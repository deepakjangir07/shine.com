package com.net.shine.utils.tracker;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.net.shine.config.URLConfig;
import com.net.shine.receivers.ReferralReceiver;
import com.net.shine.utils.db.ShineSharedPreferences;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class PostDataThread implements Runnable {

	private Context mActivity;
	String utmSource;
	String utmMedium;
	String utmContent;
	String referer = "";
	Map<String, String> map;

	public PostDataThread(Context mActivity) {
		this.mActivity = mActivity;
	}

	@Override
	public void run() {

		String deviceID = "";

		if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
			//Proceed with whatever you're doing
			TelephonyManager mytelMgr = (TelephonyManager) mActivity
					.getSystemService(Context.TELEPHONY_SERVICE);
			deviceID = mytelMgr.getDeviceId();
		}
		else
		{
			deviceID = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
			Log.d("m_cooker", "PostDataThread.....Cannot Read DeviceID!!");
		}



		SharedPreferences searcPref = mActivity.getSharedPreferences(
				ShineSharedPreferences.PREF_NAME, 0);

		try {
			map = ReferralReceiver.retrieveReferralParams1(mActivity
                    .getApplicationContext());
			utmSource = map.get("utm_source");
			if (utmSource != null) {
				utmMedium = map.get("utm_medium");
				String url = URLConfig.DYNAMIC_POSTBACK_URL;

				url = url + "utm_source="
						+ URLEncoder.encode(utmSource, "utf-8");

				if (deviceID != null) {
					url = url + "&udid=" + URLEncoder.encode(deviceID, "utf-8");
				}
				if (map.get("utm_campaign") != null) {
					url = url
							+ "&utm_campaign="
							+ URLEncoder.encode(map.get("utm_campaign"),
									"utf-8");
				}
				if (map.get("utm_medium") != null) {
					url = url + "&utm_medium="
							+ URLEncoder.encode(map.get("utm_medium"), "utf-8");
				}
				if (map.get("utm_term") != null) {

					url = url + "&utm_term="
							+ URLEncoder.encode(map.get("utm_term"), "utf-8");
				}

				if (map.get("vendorid") != null) {

					ShineSharedPreferences.saveVendorId(mActivity,map.get("vendorid"));
					Log.d("vendorid-pref",ShineSharedPreferences.getVendorId(mActivity));
					Log.d("vendorId",map.get("vendorid"));
					Log.d("vendorId_urlEncoded",URLEncoder.encode(map.get("vendorid"),"utf-8"));

					url = url + "&vendorid="
							+ URLEncoder.encode(map.get("vendorid"), "utf-8");
				}
				if (map.get("utm_content") != null) {
					url = url
							+ "&utm_content="
							+ URLEncoder
									.encode(map.get("utm_content"), "utf-8");
				}
				if (map.get("gclid") != null) {
					url = url + "&gclid="
							+ URLEncoder.encode(map.get("gclid"), "utf-8");
				}

				Log.d("post-back-url",url);

				URL callback = new URL(url);
				HttpURLConnection connection = (HttpURLConnection) callback
						.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();
				int code = connection.getResponseCode();
				Log.d("inside_post-back",code+"new");
				if (code == 200) {

					Log.d("inside_post-back",code+"");

					String result = connection.getContent().toString();
					JSONObject obj = new JSONObject(result);
					String status = obj.getString("status");
					if (status.equals("SUCCESS")) {

						ShineSharedPreferences.setRefferalFlag(mActivity, true);
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
