package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.SuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.interfaces.OTPCallback;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.SkillModel;
import com.net.shine.models.UpdateFlowsModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.VerifyUserEmailMobile;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gobletsky on 10/11/16.
 */

public class UpdateFlowsFragment extends BottomSheetDialogFragment {

    public static final int MobileVerificationWidget = 1;
    public static final int LinkedinSyncWidget = 2;
    public static final int SkillsWidget = 3;
    public static final int TotalExperienceWidget = 4;
    public static final int CurrentSalaryWidget = 5;

    public BaseActivity mActivity;

    private ArrayList<Integer> skip_flow_array=new ArrayList<>();

    private ArrayList<String> skill_tag_text=new ArrayList<>();



    public static UpdateFlowsFragment newInstance(UpdateFlowsModel model) {
        Bundle args = new Bundle();
        args.putInt("widget_type", model.next_widget);
        args.putString("update_flow_json", model.prefill_details.toString());
        UpdateFlowsFragment fragment = new UpdateFlowsFragment();
        fragment.setArguments(args);

        Log.d("PROFILE","Details "+model.prefill_details.toString());

        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        ManualScreenTracker.manualTracker("Update Flows");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    private View parentView;

    private class SkillsHolder {
         AutoCompleteTextView skillName;
         TextView skillLevel;
         ImageView skillDelete;
         TextInputLayout inputLayoutSkill;
         TextInputLayout inputLayoutExperience;
         UpdateFlowsModel.SkillsWidget.Skill_suggestions suggestion;
    }

    private final int MAX_SKILLS = 10;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        parentView = LayoutInflater.from(getContext()).inflate(R.layout.update_flow_frag, new LinearLayout(getContext()), false);
        final int type =  getArguments().getInt("widget_type");
        String json = getArguments().getString("update_flow_json");
        UpdateFlowsModel model = new UpdateFlowsModel();
        model.next_widget = type;
        model.prefill_details = new JsonParser().parse(json);
        start_update_flow_id = model.next_widget;
        initView(model);
        super.setupDialog(dialog, style);
        dialog.setContentView(parentView);

        //setMaxHeight to 87.5%
        if(parentView.getParent()!=null){
            DisplayMetrics displaymetrics = new DisplayMetrics();
            mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            View view =  ((View) parentView.getParent()).findViewById(android.support.design.R.id.design_bottom_sheet);
            view.setPadding(0, displaymetrics.heightPixels/8, 0, 0);
            view.setBackgroundColor(Color.TRANSPARENT);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateFlowsFragment.this.dismiss();

                    ShineCommons.trackShineEvents("Update Flow-Close",getUpdateFlowWidgetName(type),mActivity);
                }
            });
            parentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });


        }


        dialog.setCanceledOnTouchOutside(true);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from((View) parentView.getParent());
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(100000);

        behavior.setSkipCollapsed(true);





        MyApplication.getInstance().setIsUpdateFlowShown(true);
    }

    private void removeListeners(){
        final TextInputLayout input_layout_item = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title);
        final TextInputLayout input_layout_item2 = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title2);
        input_layout_item.getEditText().setOnFocusChangeListener(null);
        input_layout_item2.getEditText().setOnFocusChangeListener(null);
        input_layout_item.getEditText().setOnClickListener(null);
        input_layout_item2.getEditText().setOnClickListener(null);
        input_layout_item.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
        input_layout_item2.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
        input_layout_item.getEditText().setFocusableInTouchMode(true);
        input_layout_item.getEditText().setFocusable(true);
        input_layout_item2.getEditText().setFocusable(true);
        input_layout_item2.getEditText().setFocusableInTouchMode(true);
    }

    private void addListeners(){
        final TextInputLayout input_layout_item = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title);
        final TextInputLayout input_layout_item2 = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title2);
        input_layout_item.getEditText().setFocusableInTouchMode(false);
        input_layout_item.getEditText().setFocusable(false);
        input_layout_item2.getEditText().setFocusable(false);
        input_layout_item2.getEditText().setFocusableInTouchMode(false);
        input_layout_item.getEditText().setInputType(InputType.TYPE_NULL);
        input_layout_item2.getEditText().setInputType(InputType.TYPE_NULL);
        input_layout_item2.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    v.callOnClick();
            }
        });
        input_layout_item.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showNewSingleSpinnerRadioDialog(
                        getString(R.string.hint_annual_salary), input_layout_item.getEditText(),
                        URLConfig.ANNUAL_SALARYL_LIST, mActivity);
            }
        });
        input_layout_item2.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showNewSingleSpinnerRadioDialog(
                        getString(R.string.hint_annual_salary_inthousands),
                        input_layout_item2.getEditText(), URLConfig.ANNUAL_SALARYT_LIST, mActivity);
            }
        });
        input_layout_item.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    v.callOnClick();
            }
        });
    }



    private void addListenersFAandInd(){
        final TextInputLayout input_layout_item = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title);
        final TextInputLayout input_layout_item2 = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title2);
        input_layout_item.getEditText().setFocusableInTouchMode(false);
        input_layout_item.getEditText().setFocusable(false);
        input_layout_item2.getEditText().setFocusable(false);
        input_layout_item2.getEditText().setFocusableInTouchMode(false);
        input_layout_item.getEditText().setInputType(InputType.TYPE_NULL);
        input_layout_item2.getEditText().setInputType(InputType.TYPE_NULL);
        input_layout_item2.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    v.callOnClick();
            }
        });
        input_layout_item.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showNewMultiSpinnerDialogRadio(
                        getString(R.string.functional_area), input_layout_item.getEditText(),
                        URLConfig.FUNCTIONAL_AREA_LIST2, mActivity);
            }
        });
        input_layout_item2.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showNewSingleSpinnerRadioDialog(
                        getString(R.string.industry),
                        input_layout_item2.getEditText(), URLConfig.INDUSTRY_LIST3, mActivity);
            }
        });
        input_layout_item.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    v.callOnClick();
            }
        });
    }


    private void deleteSkillView(SkillsHolder holder){
        try {
            LinearLayout sub_layout = (LinearLayout) parentView.findViewById(R.id.sub_layout_skills);
            View add_more = parentView.findViewById(R.id.addMore);
            holder.skillName.setVisibility(View.GONE);
            holder.inputLayoutExperience.setVisibility(View.GONE);
            holder.inputLayoutSkill.setVisibility(View.GONE);
            holder.skillLevel.setVisibility(View.GONE);
            holder.skillDelete.setVisibility(View.GONE);
            sub_layout.removeView(holder.skillName);
            sub_layout.removeView(holder.skillLevel);
            sub_layout.removeView(holder.skillDelete);
            sub_layout.removeView(holder.inputLayoutExperience);
            sub_layout.removeView(holder.inputLayoutSkill);
            skillViewList.remove(holder);
            if (skillViewList.size() == 1) {
                skillViewList.get(0).skillDelete.setVisibility(View.GONE);
            }
            if(skillViewList.size()< MAX_SKILLS)
                add_more.setVisibility(View.VISIBLE);
            else
                add_more.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<SkillsHolder> skillViewList = new ArrayList<>();
    private void addSkillView(UpdateFlowsModel.SkillsWidget.Skill_suggestions suggestion){
        LinearLayout sub_layout = (LinearLayout) parentView.findViewById(R.id.sub_layout_skills);
        View addMore = parentView.findViewById(R.id.addMore);
        if(skillViewList.size()< MAX_SKILLS)
        {
            addMore.setVisibility(View.VISIBLE);
            View DetailTextView = mActivity.getLayoutInflater().inflate(
                    R.layout.skill_edit_sub_layout, new LinearLayout(getContext()), false);
            for (SkillsHolder holder : skillViewList) {
                holder.skillDelete.setVisibility(View.VISIBLE);
            }
            sub_layout.addView(DetailTextView);
            final SkillsHolder holder = new SkillsHolder();
            holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_skill);
            holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_experience);
            holder.skillName = (AutoCompleteTextView) DetailTextView
                    .findViewById(R.id.skill_name);
            holder.skillName.setAdapter(new SuggestionsFilterableArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.SKILL_SUGGESTION));
            if(!TextUtils.isEmpty(suggestion.value))
                holder.skillName.setText(suggestion.value);
            holder.skillLevel = (EditText) DetailTextView
                    .findViewById(R.id.skill_level_field);
            holder.skillDelete = (ImageView) DetailTextView
                    .findViewById(R.id.delete_skill);
            holder.skillDelete.setVisibility(View.VISIBLE);
            TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
            TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);
            holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View va) {
                    deleteSkillView(holder);
                }
            });
            holder.skillLevel.setTag(ShineCommons.setTagAndValue(
                    URLConfig.SKILLS_REVRSE_MAP, URLConfig.SKILLS_LIST,
                    suggestion.years_of_experience, holder.skillLevel));
            holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = (TextView) v;
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_years), tv,
                            URLConfig.newSkillList(), mActivity);
                }
            });
            holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        v.callOnClick();
                    }
                }
            });
            holder.suggestion = suggestion;
            skillViewList.add(holder);

            if(skillViewList.size()>=MAX_SKILLS){
                addMore.setVisibility(View.GONE);
            }
        }
        else {
            addMore.setVisibility(View.GONE);
            Toast.makeText(mActivity,"Max limit is " + MAX_SKILLS,Toast.LENGTH_SHORT).show();
        }
    }

    private TextInputLayout input_layout_item;
    private TextInputLayout input_layout_item2;
    private void initView(final UpdateFlowsModel model){
        TextView header_1 = (TextView) parentView.findViewById(R.id.update_flow_header_1);
        TextView header_2 = (TextView) parentView.findViewById(R.id.update_flow_header_2);


        parentView.findViewById(R.id.discard_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateFlowsFragment.this.onClick(v, model);
            }
        });
        parentView.findViewById(R.id.submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateFlowsFragment.this.onClick(v, model);
            }
        });
        parentView.findViewById(R.id.linkedin_sync_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateFlowsFragment.this.onClick(view, model);
            }
        });
        parentView.findViewById(R.id.update_flow_header_linkedin).setVisibility(View.GONE);
        parentView.findViewById(R.id.linkedin_sync_btn).setVisibility(View.GONE);
        parentView.findViewById(R.id.img_add).setVisibility(View.GONE);
        parentView.findViewById(R.id.normal_update_flow).setVisibility(View.VISIBLE);
        parentView.findViewById(R.id.skill_exp_update_flow).setVisibility(View.GONE);
        parentView.findViewById(R.id.submit_btn).setVisibility(View.VISIBLE);

        TextView submit_btn_tv = (TextView)parentView.findViewById(R.id.submit_btn_tv);
        submit_btn_tv.setText("Save");
        TextInputLayout input_layout_code = (TextInputLayout) parentView.findViewById(R.id.input_layout_code);
        input_layout_item = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title);
        input_layout_item.getEditText().setInputType(InputType.TYPE_CLASS_TEXT);
        input_layout_item.getEditText().setText("");
        input_layout_item2 = (TextInputLayout) parentView.findViewById(R.id.input_layout_item_title2);
        input_layout_item2.getEditText().setInputType(InputType.TYPE_CLASS_TEXT);
        input_layout_item2.getEditText().setText("");
        input_layout_item2.setVisibility(View.GONE);
        removeListeners();
        input_layout_code.setVisibility(View.GONE);

        final TagView skill_labels = (TagView) parentView.findViewById(R.id.skill_labels);
        skill_labels.setVisibility(View.GONE);
        skill_labels.removeAll();


        TextView tv = (TextView) parentView.findViewById(R.id.discard_btn_tv);
        tv.setText("Skip");

        Gson gson = new Gson();

        try {
            switch (model.next_widget){
                case MobileVerificationWidget:
                    header_1.setText("Should recruiter contact you at?");
                    header_2.setText("Candidates with verified mobile number, get 3x more calls");
                    input_layout_item.setHint("Mobile");
                    int maxLength = 10;
                    InputFilter[] fArray = new InputFilter[1];
                    fArray[0] = new InputFilter.LengthFilter(maxLength);
                    input_layout_item.getEditText().setFilters(fArray);
                    submit_btn_tv.setText("Verify");
                    UpdateFlowsModel.MobileVerificationWidget mobileVerificationWidget =
                            gson.fromJson(model.prefill_details, UpdateFlowsModel.MobileVerificationWidget.class);
                    input_layout_item.getEditText().setText(mobileVerificationWidget.cell_phone);
                    input_layout_item.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                    input_layout_code.getEditText().setText(mobileVerificationWidget.country_code);
                    break;
                case LinkedinSyncWidget:
                    header_1.setText("Enhance your application");
                    header_2.setText("Sync your LinkedIn to highlight your application for free !");
                    tv.setText("I don't have LinkedIn account");

                    parentView.findViewById(R.id.update_flow_header_linkedin).setVisibility(View.VISIBLE);
                    parentView.findViewById(R.id.normal_update_flow).setVisibility(View.GONE);
                    parentView.findViewById(R.id.submit_btn).setVisibility(View.GONE);
                    parentView.findViewById(R.id.linkedin_sync_btn).setVisibility(View.VISIBLE);
                    break;
                case SkillsWidget:
                    input_layout_item.getEditText().setInputType(InputType.TYPE_CLASS_TEXT);
                    header_1.setText("Do you have following Skills?");
                    header_2.setText("Update and increase your chances of getting shortlisted");
                    final UpdateFlowsModel.SkillsWidget skillsWidget =
                            gson.fromJson(model.prefill_details, UpdateFlowsModel.SkillsWidget.class);
                    if(skillsWidget.experience_required){
                        skillViewList = new ArrayList<>();
                        parentView.findViewById(R.id.normal_update_flow).setVisibility(View.GONE);
                        parentView.findViewById(R.id.skill_exp_update_flow).setVisibility(View.VISIBLE);
                        LinearLayout ll = (LinearLayout) parentView.findViewById(R.id.sub_layout_skills);
                        ll.removeAllViews();
                        for (UpdateFlowsModel.SkillsWidget.Skill_suggestions suggestion: skillsWidget.skill_suggestions)
                        {
                            addSkillView(suggestion);
                        }
                        parentView.findViewById(R.id.addMore).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addSkillView(new UpdateFlowsModel.SkillsWidget.Skill_suggestions());
                            }
                        });
                    } else {

                        int maxLength1 = 1000;
                        InputFilter[] fArray1 = new InputFilter[1];
                        fArray1[0] = new InputFilter.LengthFilter(maxLength1);
                        input_layout_item.getEditText().setFilters(fArray1);

                        skill_labels.setVisibility(View.VISIBLE);
                        input_layout_item.setHint("Skill Name");
                        parentView.findViewById(R.id.img_add).setVisibility(View.VISIBLE);
                        parentView.findViewById(R.id.img_add).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                skillAddButtonClick();
                            }
                        });
                        skill_labels.setOnTagDeleteListener(new TagView.OnTagDeleteListener() {
                            @Override
                            public void onTagDeleted(TagView tagView, Tag tag, int i) {
                                tagView.remove(i);
                                skill_tag_text.remove(tag.text);
                            }
                        });
                        skill_labels.post(new Runnable() {
                            @Override
                            public void run() {
                                for(UpdateFlowsModel.SkillsWidget.Skill_suggestions suggestion: skillsWidget.skill_suggestions){
                                    Tag tag = new Tag(suggestion.value);
                                    tag.isDeletable = true;
                                    tag.layoutBorderColor = Color.parseColor("#888888");
                                    tag.deleteIndicatorColor = Color.parseColor("#555555");
                                    tag.layoutColor = Color.WHITE;
                                    tag.tagTextColor = Color.parseColor("#555555");
                                    tag.layoutBorderSize = 1f;
                                    tag.radius=0f;
                                    skill_labels.addTag(tag);
                                    skill_tag_text.add(tag.text);
                                }
                            }
                        });
                    }
                    break;
                case TotalExperienceWidget:
                    header_1.setText("Is this your total experience?");
                    header_2.setText("Recruiter contact candidate based on experience");
                    input_layout_item.setHint("Experience (Years)");
                    input_layout_item.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                    input_layout_item2.setVisibility(View.VISIBLE);
                    input_layout_item2.setHint("Experience (Months)");
                    input_layout_item2.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
                    UpdateFlowsModel.TotalExperienceWidget totalExperienceWidget =
                            gson.fromJson(model.prefill_details, UpdateFlowsModel.TotalExperienceWidget.class);
                    input_layout_item.getEditText().setText(totalExperienceWidget.experience_in_years+"");
                    input_layout_item2.getEditText().setText(totalExperienceWidget.experience_in_months+"");

                    break;
                case CurrentSalaryWidget:
                    header_1.setText("Is this your current salary?");
                    header_2.setText("Updated salary increases relevant recruiter calls");
                    input_layout_item.setHint("Salary (Lakhs)");
                    input_layout_item2.setVisibility(View.VISIBLE);
                    input_layout_item2.setHint("Salary (Thousands)");
                    UpdateFlowsModel.CurrentSalaryWidget currentSalaryWidget =
                            gson.fromJson(model.prefill_details, UpdateFlowsModel.CurrentSalaryWidget.class);
                    input_layout_item.getEditText().setTag(ShineCommons.setTagAndValue(
                            URLConfig.ANNUAL_SALARYL_REVRSE_MAP,
                            URLConfig.ANNUAL_SALARYL_LIST,
                            currentSalaryWidget.salary_in_lakh, input_layout_item.getEditText()));
                    input_layout_item2.getEditText().setTag(ShineCommons.setTagAndValue(
                            URLConfig.ANNUAL_SALARYT_REVRSE_MAP,
                            URLConfig.ANNUAL_SALARYT_LIST,
                            currentSalaryWidget.salary_in_thousand, input_layout_item2.getEditText()));
                    addListeners();
                    break;

                default:
                    UpdateFlowsFragment.this.dismiss();
                    break;


            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }



    public void onClick(View v, UpdateFlowsModel model) {
        Gson gson = new Gson();
        switch (v.getId()){
            case R.id.discard_btn:
                //hit next update Flow

                skip_flow_array.add(model.next_widget);

                ShineCommons.trackShineEvents("Update Flow-Skip",getUpdateFlowWidgetName(model.next_widget),mActivity);


                switch (model.next_widget){
                    case LinkedinSyncWidget:
                        deleteLinkedinProfileFlow();
                        break;
                    default:
                        checkNextUpdateFlow();
                        break;
                }
                break;
            case R.id.submit_btn:
                switch (model.next_widget){
                    case MobileVerificationWidget:
                        String mobile = input_layout_item.getEditText().getText().toString();
                        Log.d("new number",mobile+"");
                        new VerifyUserEmailMobile(mActivity).verifyMobile(mobile ,new OTPCallback() {
                            @Override
                            public void finalStep(String mobile, String otp) {

                                ShineCommons.trackShineEvents("UpdateFlow",getUpdateFlowWidgetName(MobileVerificationWidget),mActivity);

                                checkNextUpdateFlow();
                            }
                        });
                        break;
                    case SkillsWidget:
                        UpdateFlowsModel.SkillsWidget skillsWidget =
                                gson.fromJson(model.prefill_details, UpdateFlowsModel.SkillsWidget.class);
                        if(skillsWidget.experience_required){
                            updateSkillWithExperienceDetails();
                        } else {
                            updateSkillWithoutExperienceDetails();
                        }
                        break;
                    case TotalExperienceWidget:
                        updateTotalExperience();
                        break;
                    case CurrentSalaryWidget:
                        updateSalary();
                        break;


                }
                break;
            case R.id.linkedin_sync_btn:
                MyApplication.isLinkedInSyncing=false;
                final Dialog alertDialog = DialogUtils.showCustomProgressDialog(getActivity(), "Please Wait...");
                LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {

                    @Override
                    public void onLinkedInConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                        if(TextUtils.isEmpty(accessToken)) {
                            ShineCommons.trackShineEvents("LinkedinLoginFail","UpdateFlow",mActivity);
                            Toast.makeText(getContext(), "Unable to access Linkedin", Toast.LENGTH_SHORT).show();
                            return;
                        }


                        MyApplication.isLinkedInSyncing=false;
                        syncLinkedinProfile(accessToken, expiry, alertDialog);
                    }
                    @Override
                    public void onLinkedInSuccess() {

                        MyApplication.isLinkedInSyncing=false;

                    }
                    @Override
                    public void onLinkedInFailed() {

                        MyApplication.isLinkedInSyncing=false;

                    }
                }, true, alertDialog);
                DialogUtils.dialogDismiss(alertDialog);

                break;
        }
    }


    private void deleteLinkedinProfileFlow(){
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                mActivity.getString(R.string.plz_wait));
        Type type = new TypeToken<UpdateFlowsModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                checkNextUpdateFlow();
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                checkNextUpdateFlow();
            }
        },
                URLConfig.LINKEDIN_UPDATE_FLOW_DELETE_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
        req.setUseDeleteMethod();
        req.execute("updateFlows");
    }


        private void syncLinkedinProfile(String accessToken, String expiry, final Dialog alertDialog){
        Type type = new TypeToken<EmptyModel>() {}.getType();
        VolleyNetworkRequest request = new VolleyNetworkRequest(getContext(), new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object empty, String tag) {
                DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(getContext(), "LinkedIn Profile Synced successfully.", Toast.LENGTH_SHORT).show();

                ShineCommons.trackShineEvents("UpdateFlow",getUpdateFlowWidgetName(LinkedinSyncWidget),mActivity);

                checkNextUpdateFlow();
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("Debug::sync", "Profile Sync failed");
                DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(getContext(), error+"", Toast.LENGTH_LONG).show();
            }
        },URLConfig.LINKEDIN_PROFILE_SYNC_API.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
        HashMap<String, String> map = new HashMap<>();
        map.put("candidate_id", ShineSharedPreferences.getCandidateId(getContext()));
        map.put("token", accessToken);
        map.put("expires_in", expiry);
        request.setUsePostMethod(map);
        request.execute("Profile-Sync");

    }


    private void updateTotalExperience() {
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                getString(R.string.plz_wait));
        HashMap<String, Object> skillMap = new HashMap<>();

        String experience_in_years = input_layout_item.getEditText().getText().toString().trim();
        String experience_in_months = input_layout_item2.getEditText().getText().toString().trim();

        boolean isError = false;
        if(TextUtils.isEmpty(experience_in_years)){
            isError = true;
            input_layout_item.setError("Enter a valid value");
        } else {
            input_layout_item.setError(null);
        }
        if(TextUtils.isEmpty(experience_in_months)){
            isError = true;
            input_layout_item2.setError("Enter a valid value");
        } else {
            input_layout_item2.setError(null);
        }
        if(isError)
            return;

        int exp = Integer.parseInt(experience_in_years);
        if (exp == 0)
            experience_in_years = URLConfig.EXPERIENCE_MAP.get("0 Yr");
        else if (exp == 1)
            experience_in_years = URLConfig.EXPERIENCE_MAP.get("1 Yr");
        else if (exp < 25)
            experience_in_years = URLConfig.EXPERIENCE_MAP.get(exp + " Yrs");
        else
            experience_in_years = URLConfig.EXPERIENCE_MAP.get("> 25 Yrs");

        //TODO: Ujjawal - Verify that lookups are sent correctly.
        skillMap.put("experience_in_years", experience_in_years);
        skillMap.put("experience_in_months", Integer.parseInt(experience_in_months));
        String URL = URLConfig.UPDATE_TOTAL_EXP_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
        Type listType = new TypeToken<EmptyModel>() {
        }.getType();


        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);

                ShineCommons.trackShineEvents("UpdateFlow",getUpdateFlowWidgetName(TotalExperienceWidget),mActivity);

                checkNextUpdateFlow();
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                Toast.makeText(mActivity,error,Toast.LENGTH_SHORT).show();

            }
        },
                URL, listType);
        downloader.setUsePutMethod(skillMap);
        downloader.execute("UPDATE_SKILL");
    }

    private void updateSalary() {
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                getString(R.string.plz_wait));
        HashMap<String, Object> skillMap = new HashMap<>();

        String salary_in_lakh = input_layout_item.getEditText().getText().toString().trim();
        String salary_in_thousand = input_layout_item2.getEditText().getText().toString().trim();
//
        boolean isError = false;
        if(TextUtils.isEmpty(salary_in_lakh)){
            isError = true;
            input_layout_item.setError("Enter a valid value");
        } else {
            input_layout_item.setError(null);
        }
        if(TextUtils.isEmpty(salary_in_thousand)){
            isError = true;
            input_layout_item2.setError("Enter a valid value");
        } else {
            input_layout_item2.setError(null);
        }
        if(isError)
            return;
//
         salary_in_lakh = URLConfig.ANNUAL_SALARYL_MAP.get(salary_in_lakh);
         salary_in_thousand =  URLConfig.ANNUAL_SALARYT_MAP.get(salary_in_thousand);

        //TODO: Ujjawal - Verify that lookups are sent correctly.
        skillMap.put("salary_in_lakh", salary_in_lakh);
        skillMap.put("salary_in_thousand", salary_in_thousand);
        String URL = URLConfig.UPDATE_TOTAL_SAL_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
        Type listType = new TypeToken<EmptyModel>() {
        }.getType();


        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);

                ShineCommons.trackShineEvents("UpdateFlow",getUpdateFlowWidgetName(CurrentSalaryWidget),mActivity);

                checkNextUpdateFlow();
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);

            }
        },
                URL, listType);
        downloader.setUsePutMethod(skillMap);
        downloader.execute("UPDATE_SALARY");

    }

    private void skillAddButtonClick(){
        try {
            final TagView skill_labels = (TagView) parentView.findViewById(R.id.skill_labels);
            if(TextUtils.isEmpty(input_layout_item.getEditText().getText().toString())){
                Toast.makeText(getContext(), "Please enter a valid skill", Toast.LENGTH_SHORT).show();
                return;
            }
            Tag tag = new Tag("");
            if(input_layout_item.getEditText().getText().toString().contains(",")){

                String str = input_layout_item.getEditText().getText().toString().trim();
                List<String> skillList = Arrays.asList(str.split(","));
                for (int i=0;i<skillList.size();i++){
                    if(!TextUtils.isEmpty(skillList.get(i).toString())){
                        tag = new Tag(skillList.get(i).toString().trim());
                    }
                    tag.isDeletable = true;
                    tag.layoutBorderColor = Color.parseColor("#888888");
                    tag.deleteIndicatorColor = Color.parseColor("#555555");
                    tag.layoutColor = Color.WHITE;
                    tag.tagTextColor = Color.parseColor("#555555");
                    tag.layoutBorderSize = 1f;
                    tag.radius=0f;
                    input_layout_item.getEditText().setText("");
                    List<Tag> labels = skill_labels.getTags();
                    Log.d("multiple_skill_add",labels+"");




                    if(!skill_tag_text.contains(tag.text)) {
                        skill_labels.addTag(tag);

                        skill_tag_text.add(tag.text);
                    }
                }
            }
            else {
                tag = new Tag(input_layout_item.getEditText().getText().toString().trim());
                tag.isDeletable = true;
                tag.layoutBorderColor = Color.parseColor("#888888");
                tag.deleteIndicatorColor = Color.parseColor("#555555");
                tag.layoutColor = Color.WHITE;
                tag.tagTextColor = Color.parseColor("#555555");
                tag.layoutBorderSize = 1f;
                tag.radius=0f;
                input_layout_item.getEditText().setText("");
                List<Tag> labels = skill_labels.getTags();
                Log.d("single_skill_add",labels+"");


                if(!skill_tag_text.contains(tag.text)) {
                    skill_labels.addTag(tag);

                    skill_tag_text.add(tag.text);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateSkillWithoutExperienceDetails() {
        try {
            final TagView skill_labels = (TagView) parentView.findViewById(R.id.skill_labels);
            if(!TextUtils.isEmpty(input_layout_item.getEditText().getText().toString())){
                skillAddButtonClick();
            }
            List<Tag> labels = skill_labels.getTags();
            JSONArray skillArray = new JSONArray();
            for (int i = 0; i < labels.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("value", labels.get(i).text);
                skillArray.put(obj);
            }

            if(skillArray.length()!=0){
                    updateSkills(skillArray);
            }
            else {
                Toast.makeText(mActivity,"Please add atleast one skill",Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }




        private void updateSkillWithExperienceDetails() {
        try {
            JSONArray skillArray = new JSONArray();
            boolean wasError = false;
            for (int i = 0; i < skillViewList.size(); i++) {
                JSONObject obj = new JSONObject();
                String skillName = skillViewList.get(i).skillName.getText().toString().trim();
                String levelD = skillViewList.get(i).skillLevel.getText().toString();
               if (skillName.isEmpty() && levelD.isEmpty() )
               {
                   wasError = true;
                   skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
                   skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
                   continue;

               }
               else if (skillName.isEmpty()) {
                   wasError = true;
                    skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
                    continue;
                }
               else if (levelD.isEmpty()) {
                   wasError = true;

                    skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
                    continue;
                }
                obj.put("value", skillName);
                obj.put("years_of_experience", URLConfig.SKILLS_MAP.get(levelD));
                skillArray.put(obj);
            }
            if (wasError) {
                Toast.makeText(getActivity(), "Please fill all required details", Toast.LENGTH_SHORT).show();
                return;
            }
            updateSkills(skillArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSkills(JSONArray skillArray){
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                getString(R.string.plz_wait));
        HashMap<String, Object> skillMap = new HashMap<>();
        skillMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
        skillMap.put("skills", skillArray);
        String URL = URLConfig.BULK_ADD_SKILL_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
        Type listType = new TypeToken<SkillModel>() {
        }.getType();


        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);

                ShineCommons.trackShineEvents("UpdateFlow",getUpdateFlowWidgetName(SkillsWidget),mActivity);

                checkNextUpdateFlow();
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                Toast.makeText(mActivity,error,Toast.LENGTH_SHORT).show();

            }
        },
                URL, listType);
        downloader.setUsePostMethod(skillMap);
        downloader.execute("UPDATE_SKILL");
    }



    private int start_update_flow_id;


    private void checkNextUpdateFlow(){
        removeListeners();
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                mActivity.getString(R.string.plz_wait));
        Type type = new TypeToken<UpdateFlowsModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                UpdateFlowsModel model= (UpdateFlowsModel) object;


                if(skip_flow_array.contains(model.next_widget)){
                    UpdateFlowsFragment.this.dismiss();
                } else {
                    initView(model);
                }
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                UpdateFlowsFragment.this.dismiss();
            }
        },
                URLConfig.UPDATE_FLOW_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
        //TODO:here we have to put also referral_code in apply if coming from link
        req.execute("updateFlows");
    }




    public static String getUpdateFlowWidgetName(int type)
    {

        String widgetName=" ";

        switch (type){
            case 1:

                widgetName= "Mobile number varification";

                break;
            case 2:

                widgetName= "LinkedIn Sync profile";
                break;
            case 3:

                widgetName="Skills";

                break;
            case 4:

                widgetName="Experience";
                break;
            case 5:
                widgetName="Salary";
                break;


        }
        return widgetName;
    }

}
