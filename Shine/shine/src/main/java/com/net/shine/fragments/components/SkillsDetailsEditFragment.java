package com.net.shine.fragments.components;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.SuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.BulkSkillsResultsModel;
import com.net.shine.models.SkillResult;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;


public class SkillsDetailsEditFragment extends BaseFragment implements VolleyRequestCompleteListener, View.OnClickListener {




    private class SkillsHolder {
        public AutoCompleteTextView skillName;
        public TextView skillLevel;
        public ImageView skillDelete;
        public TextInputLayout inputLayoutSkill;
        public TextInputLayout inputLayoutExperience;

        public SkillResult skillModel;
    }

    private ArrayList<SkillsHolder> skillViewList = new ArrayList<>();
    private Dialog alertDialog;
    private LinearLayout sub_layout;
    private UserStatusModel userProfileVO;

    private final String UPDATE_SKILL = "updateSkills";
    //String[] new_list;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.skill_edit_comp, container, false);
        view.setVisibility(View.VISIBLE);

        final TextView heading=(TextView) view.findViewById(R.id.heading);
        heading.setText("Skill Details");

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        sub_layout = (LinearLayout) view
                .findViewById(R.id.e_details);
        LinearLayout cancelBtn = (LinearLayout) view.findViewById(R.id.cancel_btn);
        TextView updateBtn = (TextView) view.findViewById(R.id.updateBtn);
        final TextView addMore = (TextView) view.findViewById(R.id.addMore);
        addMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    addMore.setVisibility(View.VISIBLE);
                View DetailTextView = mActivity.getLayoutInflater().inflate(
                        R.layout.skill_edit_sub_layout, container, false);


                for (SkillsHolder holder : skillViewList) {
                    holder.skillDelete.setVisibility(View.VISIBLE);
                }

                sub_layout.addView(DetailTextView);

                final SkillsHolder holder = new SkillsHolder();
                holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_skill);
                holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_experience);

                holder.skillName = (AutoCompleteTextView) DetailTextView
                        .findViewById(R.id.skill_name);
                    holder.skillName.setAdapter(new SuggestionsFilterableArrayAdapter<>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            URLConfig.SKILL_SUGGESTION));
                holder.skillLevel = (EditText) DetailTextView
                        .findViewById(R.id.skill_level_field);
                    holder.skillLevel.setFocusable(false);
                    holder.skillLevel.setFocusableInTouchMode(false);
                holder.skillDelete = (ImageView) DetailTextView
                        .findViewById(R.id.delete_skill);

                holder.skillDelete.setVisibility(View.VISIBLE);
                TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
                TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);

                holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        try {
                            holder.skillName.setVisibility(View.GONE);
                            holder.inputLayoutExperience.setVisibility(View.GONE);
                            holder.inputLayoutSkill.setVisibility(View.GONE);
                            holder.skillLevel.setVisibility(View.GONE);
                            holder.skillDelete.setVisibility(View.GONE);
                            sub_layout.removeView(holder.skillName);
                            sub_layout.removeView(holder.skillLevel);
                            sub_layout.removeView(holder.skillDelete);
                            sub_layout.removeView(holder.inputLayoutExperience);
                            sub_layout.removeView(holder.inputLayoutSkill);
                            skillViewList.remove(holder);
                            if (skillViewList.size() == 1) {
                                skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                            }
                            if(skillViewList.size()<=9)
                            addMore.setVisibility(View.VISIBLE);
                            else
                            addMore.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.skillLevel.setTag(ShineCommons.setTagAndValue(
                        URLConfig.SKILLS_REVRSE_MAP, URLConfig.SKILLS_LIST,
                        0, holder.skillLevel));
                holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        TextView tv = (TextView) v;
                        DialogUtils.showNewSingleSpinnerRadioDialog(
                                getString(R.string.hint_exp_years), tv,
                                URLConfig.newSkillList(), mActivity);
                    }
                });
                    holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if(hasFocus){
                                v.callOnClick();
                            }
                        }
                    });
                holder.skillModel = new SkillResult();
                holder.skillModel.setId("");
                holder.skillModel.setValue(holder.skillName.getText().toString());
                holder.skillModel.setYears_of_experience(0);
                skillViewList.add(holder);


        }
        });
        updateBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        ArrayList<SkillResult> skillModelList = new ArrayList<>();
        if(SkillDetailComp.skillsList==null || SkillDetailComp.skillsList.isEmpty())
        {
            SkillResult skillModel = new SkillResult();
            skillModel.setId("");
            skillModel.setValue("");
            skillModel.setYears_of_experience(0);
            skillModelList.add(skillModel);
        }
        else
        {
            skillModelList.addAll(SkillDetailComp.skillsList);
        }




        for (int i = 0; i < skillModelList.size(); i++) {

            final SkillsHolder holder = new SkillsHolder();
            View DetailTextView = inflater.inflate(R.layout.skill_edit_sub_layout, container, false);
            holder.skillName = (AutoCompleteTextView) DetailTextView.findViewById(R.id.skill_name);

            holder.skillName.setAdapter(new SuggestionsFilterableArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.SKILL_SUGGESTION));

            holder.skillName.setText(skillModelList.get(i).getValue());
            holder.skillLevel = (EditText) DetailTextView.findViewById(R.id.skill_level_field);
            holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_skill);
            holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_experience);
            TextChangeListener.addListener(holder.skillName,holder.inputLayoutSkill, mActivity);
            TextChangeListener.addListener(holder.skillLevel,holder.inputLayoutExperience,mActivity);


            int tag;
            System.out.println("--reverse map" + URLConfig.SKILLS_REVRSE_MAP);

            if(skillModelList.get(i).getYears_of_experience()==2) {
                tag=-1;
                holder.skillLevel.setText("");
            }
            else
            {
                  tag = ShineCommons.setTagAndValue(URLConfig.SKILLS_REVRSE_MAP,
                        URLConfig.SKILLS_LIST, skillModelList.get(i).getYears_of_experience(),
                        holder.skillLevel);
                tag=tag-1;
            }

            System.out.println("--skillllevel--" + skillModelList.get(i).getYears_of_experience() +"\n--tag"+tag);



                holder.skillLevel.setTag(tag);


            holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = (TextView) v;


                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_years), tv,
                            URLConfig.newSkillList(), mActivity);
                }
            });
            holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        v.callOnClick();
                    }
                }
            });
            holder.skillDelete = (ImageView) DetailTextView
                    .findViewById(R.id.delete_skill);

            holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try{


                            holder.skillName.setVisibility(View.GONE);
                            holder.inputLayoutExperience.setVisibility(View.GONE);
                            holder.inputLayoutSkill.setVisibility(View.GONE);
                            holder.skillLevel.setVisibility(View.GONE);
                            holder.skillDelete.setVisibility(View.GONE);
                            sub_layout.removeView(holder.skillName);
                            sub_layout.removeView(holder.skillLevel);
                            sub_layout.removeView(holder.skillDelete);
                            sub_layout.removeView(holder.inputLayoutExperience);
                            sub_layout.removeView(holder.inputLayoutSkill);
                            skillViewList.remove(holder);
                            if (skillViewList.size() == 1) {
                                skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


                if (skillModelList.size() > 1) {
                    holder.skillDelete.setVisibility(View.VISIBLE);
                }

                sub_layout.addView(DetailTextView);

                holder.skillModel = skillModelList.get(i);
                skillViewList.add(holder);
            }

        return view;
    }


    private void updateSkillDetails() {
        try {

            JSONArray skillArray = new JSONArray();
            boolean wasError = false;

            for (int i = 0; i < skillViewList.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", skillViewList.get(i).skillModel.getId());
                String skillName = skillViewList.get(i).skillName.getText().toString().trim();
                String levelD = skillViewList.get(i).skillLevel.getText().toString();

                System.out.println("--skill--" + skillViewList.get(i).skillLevel.getText().toString());

               if (skillName.isEmpty() && levelD.isEmpty() )

               {
                   wasError = true;
                   skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
                   skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
                   continue;

               }
               else if (skillName.isEmpty()) {
                   wasError = true;
                    skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
                    continue;
                }

               else if (levelD.isEmpty()) {
                   wasError = true;

                    skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
                    continue;
                }
                obj.put("value", skillName);
                obj.put("years_of_experience", URLConfig.SKILLS_MAP.get(levelD));
                skillArray.put(obj);
            }
            if (wasError) {
                Toast.makeText(getActivity(), "Please fill all required details", Toast.LENGTH_SHORT).show();
                return;
            }
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            HashMap<String, Object> skillMap = new HashMap<>();

            skillMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
            skillMap.put("skills_data", skillArray);


            String URL = URLConfig.BULK_UPDATE_SKILL_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));

            Type listType = new TypeToken<BulkSkillsResultsModel>() {
            }.getType();


            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, SkillsDetailsEditFragment.this,
                    URL, listType);

            downloader.setUsePostMethod(skillMap);
            downloader.execute(UPDATE_SKILL);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.title_edit_profile));
        mActivity.setActionBarVisible(false);
        //mActivity.updateLeftDrawer()();
        ManualScreenTracker.manualTracker("Edit-SkillDetails");
    }

    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.updateBtn:

                    updateSkillDetails();

                    break;

                case R.id.cancel_btn:


                       mActivity.popone();


                    Bundle bundle=new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_SKILL);

                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));


                    ShineCommons.hideKeyboard(mActivity);
                    break;

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

            if (object != null) {

                if (tag.equals(UPDATE_SKILL)) {

                    ShineSharedPreferences.setSkillDashFlag(mActivity, true);

                    BulkSkillsResultsModel skillModel = (BulkSkillsResultsModel) object;
                    if(SkillDetailComp.skillsList==null)
                        SkillDetailComp.skillsList = new ArrayList<>();
                    else
                        SkillDetailComp.skillsList.clear();
                    for (SkillResult model : skillModel.skills_data) {
                        SkillDetailComp.skillsList.add(model);
                    }
                    DataCaching.getInstance().cacheData(DataCaching.SKILLS_LIST_SECTION,
                            "" + DataCaching.CACHE_TIME_LIMIT, SkillDetailComp.skillsList, mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    Toast.makeText(mActivity, "Skills updated successfully", Toast.LENGTH_SHORT).show();

                    ShineCommons.trackShineEvents("Edit", "Skill", mActivity);


                      mActivity.popone();

                     Bundle bundle=new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_SKILL);

                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        DialogUtils.dialogDismiss(alertDialog);
        DialogUtils.showErrorToast(error);

    }

}
