package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by manish on 01/02/16.
 */
public class ApplywithoutResumeModel implements Serializable {

        public String job_id = "";
        public String user_id = "";
        public String name = "";
        public String email = "";
        public String mobile = "";
        public int application_source ;
        public int is_auto_apply;
        public String comp_name = "";
        public String job_title = "";
        public String photo = "";


}
