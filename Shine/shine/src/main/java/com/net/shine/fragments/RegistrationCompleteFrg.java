package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.HomeActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.models.RegistrationModel;
import com.net.shine.models.ResumeDetails;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RegistrationCompleteFrg extends BaseFragment implements
        VolleyRequestCompleteListener, OnFocusChangeListener, View.OnClickListener {
    private EditText name;
    private LinearLayout container_box;
    private TextView totalExpField, funcAreaField,
            industryField, cityField;
    private Dialog alertDialog;

    private boolean checkForToastAlert = false;
    private TextInputLayout inputLayoutName, inputLayoutExperience, inputLayoutCity,
            inputLayoutFunctionalArea, inputLayoutIndustry;

    public static RegistrationCompleteFrg newInstance(Bundle args) {

        RegistrationCompleteFrg fragment = new RegistrationCompleteFrg();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.registration_complete_view, container, false);

        try {

            container_box = (LinearLayout) view.findViewById(R.id.container);
            container_box.requestFocus();

            view.findViewById(R.id.submit_btn).setOnClickListener(this);

            int citSelectedPos = -1;    //cannot be 0 as it refers to #TopCities
            int[] indSelectedPos = new int[0];
            int[] funSelectedPos = new int[0];

            name = (EditText) view.findViewById(R.id.name);
            totalExpField = (TextView) view.findViewById(R.id.total_exp);
            cityField = (TextView) view.findViewById(R.id.city);
            cityField.setTag(citSelectedPos);
            cityField.setOnClickListener(this);
            cityField.setOnFocusChangeListener(this);
            funcAreaField = (TextView) view.findViewById(R.id.fun_area);
            funcAreaField.setTag(funSelectedPos);
            funcAreaField.setOnClickListener(this);
            funcAreaField.setOnFocusChangeListener(this);
            industryField = (TextView) view.findViewById(R.id.industry);
            industryField.setTag(indSelectedPos);
            industryField.setOnClickListener(this);
            industryField.setOnFocusChangeListener(this);
            inputLayoutName = (TextInputLayout) view.findViewById(R.id.input_layout_name);
            inputLayoutCity = (TextInputLayout) view.findViewById(R.id.input_layout_city);
            inputLayoutExperience = (TextInputLayout) view.findViewById(R.id.input_layout_exp);
            inputLayoutFunctionalArea = (TextInputLayout) view.findViewById(R.id.input_layout_fun_area);
            inputLayoutIndustry = (TextInputLayout) view.findViewById(R.id.input_layout_industry);

            TextChangeListener.addListener(name, inputLayoutName, mActivity);
            TextChangeListener.addListener(cityField, inputLayoutCity, mActivity);
            TextChangeListener.addListener(funcAreaField, inputLayoutFunctionalArea, mActivity);
            TextChangeListener.addListener(industryField, inputLayoutIndustry, mActivity);
            TextChangeListener.addListener(totalExpField, inputLayoutExperience, mActivity);

            ResumeDetails parser = ShineSharedPreferences.getParserInfo(mActivity, getArguments().getString(URLConfig.USERID));
            if(parser!=null){
                if(!TextUtils.isEmpty(parser.name))
                    name.setText(parser.name);

                cityField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.CITY_REVERSE_MAP,
                        URLConfig.CITY_LIST_EDIT, parser.can_location,
                        cityField));


                try {
                    Log.d("Debug::exp_map", parser.exp_in_yr+ " "  + URLConfig.EXPERIENCE_REVERSE_MAP.toString());
                    if(URLConfig.EXPERIENCE_REVERSE_MAP.containsKey(parser.exp_in_yr+""))
                    {

                        String exp_str = URLConfig.EXPERIENCE_REVERSE_MAP.get(parser.exp_in_yr + "");
                        String[] str = exp_str.split(" ");
                        if(str.length>0){
                            int yr = Integer.parseInt(str[0]);
                            totalExpField.setText(yr+"");
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }




        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.ok_btn:
                    DialogUtils.dialogDismiss(alertDialog);
                    break;
                case R.id.submit_btn:
                    submitAllDetails();
                    break;
                default:
                    showSpinner(v.getId());
                    ShineCommons.hideKeyboard(mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submitAllDetails() {
        try {
            String nameText = name.getText().toString().trim();
            String totalExp = totalExpField.getText().toString().trim();
            String functionalArea = funcAreaField.getText().toString().trim();
            String industry = industryField.getText().toString().trim();
            String city = cityField.getText().toString().trim();

            if (nameText.length() == 0) {
                checkForToastAlert = true;
                 inputLayoutName.setError("Please enter name");
            }

            if (city.length() == 0) {
                checkForToastAlert = true;
                 inputLayoutCity.setError("Please select city");
            }
            String exp_lookup = "";
            if (totalExp.length() == 0) {
                checkForToastAlert = true;
                 inputLayoutExperience.setError("Please enter experience");
            } else {
                int exp = Integer.parseInt(totalExp);
                if (exp == 0)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("0 Yr");
                else if (exp == 1)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("1 Yr");
                else if (exp < 25)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get(exp + " Yrs");
                else
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("> 25 Yrs");
            }

            JSONArray func_list = new JSONArray();
            if (functionalArea.length() == 0) {
                checkForToastAlert = true;
                 inputLayoutFunctionalArea.setError("Please select functional area");
            } else {
                int[] func_area;
                String[] i_func = functionalArea.split(",");
                func_area = new int[i_func.length];
                for (int i = 0; i < i_func.length; i++) {
                    if (!i_func[i].equals(""))
                        func_area[i] = Integer.parseInt(URLConfig.FUNCTIONA_AREA_MAP.get(i_func[i].trim()));
                }
                for (int aFunc_area : func_area) {
                    func_list.put(aFunc_area);
                }
            }
            JSONArray ind_list = new JSONArray();
            if (industry.length() == 0) {
                checkForToastAlert = true;
                 inputLayoutIndustry.setError("Please select industry");
            } else {
                int[] indus;
                if (industry.equals("Any")) {
                    indus = new int[0];
                } else {
                    String[] i_ind = industry.split(",");
                    indus = new int[i_ind.length];
                    for (int i = 0; i < i_ind.length; i++) {
                        if (!i_ind[i].equals(""))
                            indus[i] = Integer.parseInt(URLConfig.INDUSTRY_MAP.get(i_ind[i].trim()));
                    }
                }
                for (int indu : indus) {
                    ind_list.put(indu);
                }
            }

            if (checkForToastAlert) {
                showToastAlert(getResources().getString(
                        R.string.required_fields));
                checkForToastAlert = false;
                return;
            }

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            Bundle b = getArguments();
            String userid = b.getString(URLConfig.USERID) + "";

            Type listType = new TypeToken<RegistrationModel>() {
            }.getType();

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.REGISTRATION_COMPLETE_URL.replace(URLConfig.CANDIDATE_ID, userid), listType);

            HashMap<String, Object> registerMap = new HashMap<>();

            registerMap.put("candidate_location", URLConfig.CITY_MAP.get(city));
            registerMap.put("name", nameText);
            registerMap.put("country_code", "91");
//            registerMap.put("salary_in_thousand", "0");
            registerMap.put("industry", ind_list);
            registerMap.put("experience_in_months", "0");
            registerMap.put("experience_in_years", exp_lookup);
            registerMap.put("functional_area", func_list);
//            registerMap.put("salary_in_lakh", "1");

            downloader.setUsePutMethod(registerMap);
            downloader.execute("submitAllDetails");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showToastAlert(String text) {
        DialogUtils.showErrorToast(text);
    }


    private void showSpinner(int id) {
        try {
            switch (id) {

                case R.id.fun_area:

                    DialogUtils.showNewMultiSpinnerDialog(
                            getString(R.string.hint_func_area), funcAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST2, mActivity, true);

                    break;
                case R.id.industry:

                    DialogUtils.showNewSingleSpinnerDialog(
                            getString(R.string.hint_industry), industryField,
                            URLConfig.INDUSTRY_LIST2, mActivity, true);

                    break;
                case R.id.city:

                    DialogUtils.showNewMultiSpinnerDialogRadio(
                            getString(R.string.hint_city), cityField,
                            URLConfig.CITY_LIST_EDIT, mActivity);

                    break;
                case R.id.ok_btn:
                    DialogUtils.dialogDismiss(alertDialog);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        container_box.requestFocus();
        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
        mActivity.setTitle(getString(R.string.register_complete));
        ManualScreenTracker.manualTracker("RegistrationComplete");

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {
        try {
            final RegistrationModel registrationModel = (RegistrationModel) object;


            if(TextUtils.isEmpty(ShineSharedPreferences.getUserAccessToken(mActivity))&&!TextUtils.isEmpty(registrationModel.access_token)) {
                ShineSharedPreferences.setUserAccessToken(mActivity, registrationModel.access_token);
            }
            //TODO: If we ever implement Notification type 6, we need to handle
            //      and save userStatusModel when user first logged in
            DialogUtils.dialogDismiss(alertDialog);
            Bundle b = getArguments();
            if (b == null)
                b = new Bundle();
            UserStatusModel profile;
            profile = (UserStatusModel) b.getSerializable("userStatusModel");
            if (profile == null) {
                profile = ShineSharedPreferences.getUserInfo(mActivity);
                if (profile == null)
                    profile = new UserStatusModel();
                profile.candidate_id = (b.getString(URLConfig.USERID));
                profile.job_title = (b.getString(URLConfig.JOBTITLE));
                profile.is_mid_out = (b.getInt(URLConfig.IS_MID_OUT));
                profile.email = (b.getString(URLConfig.USEREMAIL));
                profile.full_name = (name.getText().toString());
                profile.mobile_no = (b.getString(URLConfig.USEREMOB));
                profile.set_next_phonebook_sync_on(b.getString(URLConfig.PHONEBOOK_SYNC_TIME));
                profile.set_phonebook_synced(b.getBoolean(URLConfig.PHONEBOOK_SYNC_STATUS));
            }


            ShineCommons.trackShineEvents("RegistrationConversion","WebMidoutComplete",mActivity);

            try {
                if(name!=null)
                    profile.full_name = (name.getText().toString());
                else
                    profile.full_name = registrationModel.name;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            ShineSharedPreferences.saveUserInfo(
                    mActivity.getApplicationContext(), profile);

            if (!profile.getIsPhonebookSynced()) {
                ShineCommons.sendContact();
            } else {
                ShineSharedPreferences
                        .savePhoneBookReadProgress(
                                mActivity, false);
                ShineSharedPreferences
                        .savePhoneBookDelivered(mActivity,
                                true);
                ShineSharedPreferences.saveSyncPhTime(
                        mActivity.getApplicationContext(),
                        profile.get_next_phonebook_sync_on(),
                        profile.candidate_id);
            }

//            ShineSharedPreferences.setResumeMidout(mActivity,
//                    true);
            ShineSharedPreferences.setExpDashFlag(
                    mActivity, false);
            try {
                String totalExp = totalExpField.getText().toString();
                if(!totalExp.isEmpty())
                {
                    int exp = Integer.parseInt(totalExp);
                    if (exp == 0) {
                        ShineSharedPreferences.setExpDashFlag(
                                mActivity, true);
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            ShineSharedPreferences.setEduDashFlag(mActivity,
                    profile.has_education);

            ShineSharedPreferences.setSkillDashFlag(mActivity,
                    profile.has_skills);

            Intent intent = new Intent(mActivity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(getArguments());
            intent.putExtra(ShineAuthUtils.COMPLETED_FROM, ShineAuthUtils.FROM_WEB_MIDOUT_REGISTRATION);
            startActivity(intent);
            mActivity.finish();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (alertDialog != null && alertDialog.isShowing()) {

                            alertDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    if(alertDialog != null)
                        alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                                RegistrationCompleteFrg.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
