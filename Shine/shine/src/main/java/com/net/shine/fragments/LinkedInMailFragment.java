package com.net.shine.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedinDataSendListener;
import com.net.shine.models.ActionBarState;
import com.net.shine.models.ReferralRequestModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.SendRefferalService;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.SendtoServerLinkedin;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.ReferralCipher;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

//import org.apache.http.ParseException;

public class LinkedInMailFragment extends BaseFragment implements OnClickListener {

    private EditText messageBody;
    private UserStatusModel userProfileVO;
    private ReferralRequestModel model;
    private String recipientName;
    private String accessToken;

    private String jobUrl = "";
    private String recipientProfileID = "";
    private String recipientProfileUrl = "";
    int fromWhich;


    public static LinkedInMailFragment newInstance(Bundle args) {
        LinkedInMailFragment fragment = new LinkedInMailFragment();
        fragment.setArguments(args);
        return fragment;
    }


//    public static LinkedInMailFragment newInstance(int fromWhich, String recipientProfileID,
//                                String recipientName,
//                                String jobUrl, String recipientProfileUrl) {
//
//        Bundle args = new Bundle();
//        args.putInt("fromWhich", fromWhich);
//        args.putString("recipientProfileID", recipientProfileID);
//        args.putString("recipientName", recipientName);
//        args.putString("jobUrl", jobUrl);
//        args.putString("recipientProfileUrl", recipientProfileUrl);
//        LinkedInMailFragment fragment = new LinkedInMailFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    public static LinkedInMailFragment newInstance(int fromWhich, String recipientProfileID,
//                                String recipientName,
//                                String jobUrl, ReferralRequestModel model, String recipientProfileUrl) {
//
//        Bundle args = new Bundle();
//        args.putInt("fromWhich", fromWhich);
//        args.putString("recipientProfileID", recipientProfileID);
//        args.putString("recipientName", recipientName);
//        args.putString("jobUrl", jobUrl);
//        args.putSerializable("model", model);
//        args.putString("recipientProfileUrl", recipientProfileUrl);
//        LinkedInMailFragment fragment = new LinkedInMailFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }

    public LinkedInMailFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        this.fromWhich = getArguments().getInt("fromWhich", 0);
        this.recipientName = getArguments().getString("recipientName", "");
        this.recipientProfileID = getArguments().getString("recipientProfileID", "");
        this.jobUrl = getArguments().getString("jobUrl", "");
        this.recipientProfileUrl = getArguments().getString("recipientProfileUrl", "");
        this.model = (ReferralRequestModel) getArguments().getSerializable("model");
    }

    EditText subject;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.linkedin_mail_frg, container, false);

        accessToken = ShineSharedPreferences.getLinkedinAccessToken(mActivity);

        LinearLayout submitBtn = (LinearLayout) view.findViewById(R.id.submit_btn);
        submitBtn.setOnClickListener(this);

        LinearLayout cancel_btn = (LinearLayout) view.findViewById(R.id.cancel_btn);
        cancel_btn.setOnClickListener(this);

        TextView connectedVia = (TextView) view.findViewById(R.id.connected_via_linkedin);
        connectedVia.setText("You are connected to " + recipientName + " via LinkedIn");

        subject = (EditText) view.findViewById(R.id.subject);
        if(fromWhich==1)
            subject.setText("Request for Referral");
        else if(fromWhich==3)
        {
            subject.setText("Shine.com App Invite");
        }
        else
            subject.setText("Hi " + recipientName + " , I want to connect with you.");

//        TextView sendTo = (TextView) view.findViewById(R.id.sendto);
//        sendTo.setText("To : " + recipientName);

        messageBody = (EditText) view.findViewById(R.id.feedback);

        if(fromWhich==1 && model!=null && model.getJob_id()!=null && !model.getJob_id().equals(""))
        {
                    String Ref_URL= URLConfig.VIEW_MESSAGE_URL_ENCRYPTED +
                            ReferralCipher.cipher(model.getJob_id() + "/" + userProfileVO.candidate_id
                                    + "/" + model.getReferral_id(), ReferralCipher.ENC_MODE) + "/";
            messageBody.setText("Hi, I came across the following job opportunity in your company on Shine.com\n\n" +
                                jobUrl + "\n\n I would like to request you to refer my candidature to the hiring manager. " +
                                "You can do so by clicking on the link below \n\n"
                                + Ref_URL +
                                "\n\n Regards \n"
                                + userProfileVO.full_name + "");
        }
        else if(fromWhich==3)
        {
            messageBody.setText("Download the shine.com mobile app and get the best jobs in the industry! "+jobUrl);
        }
        else if (fromWhich==2)
        {
                messageBody.setText("I would like to connect with you for possible openings in your company.\n" +
                       "It would be great if you could reply to this email or send me your contact details to discuss this further.\n" +
                        "My contact detail are as follows:\n\n" +
                        "Phone: " + userProfileVO.mobile_no + "\nEmail: "+ userProfileVO.email +
                        "\n\nLooking forward to hear from you. \nThanks and regards\n"
                        + userProfileVO.full_name);
        }

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.submit_btn:
                Log.d("LINKEDIN", LinkedinConstants.getSendMessageUrl(accessToken));
                new SendMessageAsyncTask().execute(LinkedinConstants.getSendMessageUrl(accessToken));


                break;

            case R.id.cancel_btn:
               mActivity.onBackPressed();



                break;

            default:
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Message");
        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);

    }





    private class SendMessageAsyncTask extends AsyncTask<String, Void, String> {


        ProgressDialog customDialog;
        String sub;
        String message;

        @Override
        protected void onPreExecute() {

            customDialog = new ProgressDialog(mActivity, ProgressDialog.STYLE_SPINNER);
            customDialog.setMessage("Sending Message...");
            customDialog.setCancelable(false);
            customDialog.show();
            sub = subject.getText().toString();
            message = messageBody.getText().toString();
        }

        @Override
        protected String doInBackground(String... urls) {

            try {
                JSONObject jsonObjSend = new JSONObject();
                jsonObjSend.put("subject", sub);
                jsonObjSend.put("body", message);


                JSONObject values = new JSONObject();
                JSONObject person = new JSONObject();
                JSONObject path = new JSONObject();
                path.put("_path", "/people/" + recipientProfileID);
                person.put("person", path);
                JSONArray persons = new JSONArray();
                persons.put(person);
                values.put("values", persons);

                jsonObjSend.put("recipients", values);

                Log.d("LINKEDIN", jsonObjSend.toString());

                StringBuilder content = new StringBuilder();

                try {
                    URL mUrl = new URL(urls[0]);
                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-type", "application/json");
                    connection.setRequestProperty("x-li-format", "json");
                    connection.setRequestProperty(URLConfig.LINKEDIN_HEADER_KEY, URLConfig.LINKEDIN_HEADER_VALUE);
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(jsonObjSend.toString());
                    writer.flush();
                    writer.close();

                    long t = System.currentTimeMillis();

                    Log.i("LinkedIn", "HTTPResponse " +
                            connection.getResponseCode()+
                            " received in [" + (System.currentTimeMillis() - t) + "ms]");
                    int resp = connection.getResponseCode();
                    connection.disconnect();
                    return resp+"";
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String status) {
            customDialog.dismiss();
            if(status.equals("201"))
            {
                if(fromWhich==1)
                {
                    Toast.makeText(mActivity,"Your referral request has been sent successfully.", Toast.LENGTH_SHORT).show();
                    ShineCommons.trackShineEvents("JobReferralConnect","Linkedin", mActivity);

                    Intent i = new Intent(mActivity, SendRefferalService.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("model", model);
                    i.putExtra("bundle", bundle);
                    mActivity.startService(i);
                }

                else
                {
                    ShineCommons.trackShineEvents("CompanyConnect","Linkedin", mActivity);


                    Toast.makeText(mActivity,"Your message has been sent successfully.", Toast.LENGTH_SHORT).show();
                }

                mActivity.onBackPressed();

                }
            else if(status.equals("404"))
            {
                //404 means contacts sync is Needed...
                final Dialog alertDialog = new Dialog(mActivity);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.cus_linkedin_sync_dialog);
                alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                alertDialog.findViewById(R.id.linkedin_sync_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        new GetConnectionRequestAsyncTask().execute(accessToken);
                    }
                });//LinkedInMailFragment.this);
                alertDialog.findViewById(R.id.no_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
            else if(status.equals("401"))
            {
//                Toast.makeText(mActivity,
//                        "Sorry..You  need to re-authorize our app",
//                        Toast.LENGTH_SHORT).show();
                LinkedinConnect();
            }
            else if(status.equals("403"))
            {

                final Dialog alertDialog = new Dialog(mActivity);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.cus_linkedin_limit_dialog);
                alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                alertDialog.findViewById(R.id.linkedin_limit_send_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        try {
//                                int sdk = android.os.Build.VERSION.SDK_INT;
//
//                                if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
//                                    android.text.ClipboardManager clipboard2;
//                                    clipboard2 = (android.text.ClipboardManager) mActivity
//                                            .getSystemService(Context.CLIPBOARD_SERVICE);
//                                    clipboard2.setText(messageBody.getText());
//                                } else {
                                    ClipData clip = ClipData.newPlainText("msg",messageBody.getText());
                                    ClipboardManager clipboard;
                                    clipboard = (ClipboardManager) mActivity
                                            .getSystemService(Context.CLIPBOARD_SERVICE);
                                    clipboard.setPrimaryClip(clip);
//                                }

                            ShineCommons.trackCustomEvents("AppExit", "LinkedIn", mActivity);


                            mActivity.getPackageManager().getPackageInfo(
                                    "com.linkedin.android", 0);
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));

                        } catch (PackageManager.NameNotFoundException e) {
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.findViewById(R.id.no_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
            else
            {

                final Dialog alertDialog = new Dialog(mActivity);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.cus_linkedin_limit_dialog);
                alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                alertDialog.findViewById(R.id.linkedin_limit_send_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        try {
//                            int sdk = android.os.Build.VERSION.SDK_INT;
//
//                            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
//                                android.text.ClipboardManager clipboard2;
//                                clipboard2 = (android.text.ClipboardManager) mActivity
//                                        .getSystemService(Context.CLIPBOARD_SERVICE);
//                                clipboard2.setText(messageBody.getText());
//                            } else {
                                ClipData clip = ClipData.newPlainText("msg",messageBody.getText());
                                ClipboardManager clipboard;
                                clipboard = (ClipboardManager) mActivity
                                        .getSystemService(Context.CLIPBOARD_SERVICE);
                                clipboard.setPrimaryClip(clip);
//                            }

                            ShineCommons.trackCustomEvents("AppExit", "LinkedIn", mActivity);


                            mActivity.getPackageManager().getPackageInfo(
                                    "com.linkedin.android", 0);
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));

                        } catch (PackageManager.NameNotFoundException e) {
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.findViewById(R.id.no_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();






                Toast.makeText(mActivity,"Unable to send Message.", Toast.LENGTH_SHORT).show();


                ShineCommons.trackShineEvents("LinkedinMessageFailed","JobReferralConnect",mActivity);
            }


        }

    }



    private class GetConnectionRequestAsyncTask extends
            AsyncTask<String, Void, JSONObject> implements LinkedinDataSendListener {

        Dialog customDialog;
        boolean cancel_sync_dialog = false;
        boolean newUserIdFound = false;



        @Override
        protected void onPreExecute() {

            customDialog = DialogUtils.showSyncDialog(mActivity, "linkedin");

            customDialog.findViewById(R.id.close_btn).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(customDialog!=null)
                        customDialog.dismiss();
                    cancel_sync_dialog = true;
                }
            });

            if(customDialog.findViewById(R.id.continue_button)!=null)
            {
                customDialog.findViewById(R.id.continue_button).setVisibility(View.GONE);
            }

        }

        @Override
        protected JSONObject doInBackground(String... urls) {

            Log.d("DEBUG Friends", "Come " + urls[0]);

            if (urls.length > 0) {
                String accessToken = urls[0];

                try {

                    Log.d("DEBUG_LINKEDIN", "Start Hitting Connection API");
                    JSONArray values = new JSONArray();
                    int total = 1000;
                    int count = 0;


                    while (total - count > 0) {
                        StringBuilder content = new StringBuilder();
                        URL mUrl = new URL(LinkedinConstants.getConnectionsUrl(accessToken, count));
                        Log.d("DEBUG_LINKEDIN", "Hitting Connection API: start=" + count + " " + mUrl);
                        HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                        connection.setRequestProperty("x-li-format", "json");

                        if (connection.getResponseCode() == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            String line;
                            while ((line = bufferedReader.readLine()) != null)
                                content.append(line).append("\n");
                            bufferedReader.close();
                            connection.disconnect();
                            JSONObject ans = new JSONObject(content.toString());
                            if (ans.has("_total"))    //update total from its default 1000
                            {
                                total = ans.getInt("_total");
                            }
                            if (!ans.has("values")) {
                                break;    //we have reached the end
                            }
                            JSONArray nVal = ans.getJSONArray("values");
                            if (nVal.length() == 0) {
                                break;    //we have reached the end
                            }
                            count = count + nVal.length();
                            for (int i = 0; i < nVal.length(); i++) {
                                values.put(nVal.get(i));
                            }
                            if (nVal.length() < LinkedinConstants.CONNECTION_PAGING_COUNT)    //we received less results then demanded, so end has been reached
                            {
                                break;
                            }
                        } else {
                            Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                            break;
                        }

                    }

                    for (int i = 0; i < values.length(); i++) {
                        JSONObject connection_detail = values.getJSONObject(i);
                        if (connection_detail.has("publicProfileUrl") && connection_detail.getString("publicProfileUrl").equals(recipientProfileUrl)) {
                            recipientProfileID = connection_detail.getString("id");
                            newUserIdFound = true;
                        }

                    }

                    JSONObject object = new JSONObject();
                    object.put("_total", total);
                    object.put("_start", 0);
                    object.put("_count", count);
                    object.put("values", values);

                    return LinkedinConstants.parseLinkedin(object);


                } catch (Exception e) {
                    e.printStackTrace();
                    DialogUtils.dialogDismiss(customDialog);
                }
            }
            return null;
        }




        @Override
        protected void onPostExecute(JSONObject data) {

            if (data != null && (!cancel_sync_dialog)) {
                new SendtoServerLinkedin(mActivity,data,this).sendtoServerLinkedInData();
            }
            else if (customDialog != null)
                customDialog.dismiss();
        }

        @Override
        public void onSuccessLinkdin() {

            if (customDialog != null)
                customDialog.dismiss();

            URLConfig.isRecentSync = true;
            ShineSharedPreferences.saveLinkedinImported(mActivity, true);
            if(newUserIdFound)
            {
//                Toast.makeText(
//                        mActivity,
//                        "LinkedIn contacts synced.",
//                        Toast.LENGTH_SHORT).show();
                new SendMessageAsyncTask().execute(LinkedinConstants.getSendMessageUrl(accessToken));
            }
            else
            {

                final Dialog alertDialog = new Dialog(mActivity);
                alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialog.setContentView(R.layout.cus_linkedin_limit_dialog);
                alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                alertDialog.findViewById(R.id.linkedin_limit_send_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        try {
                            int sdk = android.os.Build.VERSION.SDK_INT;

//                            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
//                                android.text.ClipboardManager clipboard2;
//                                clipboard2 = (android.text.ClipboardManager) mActivity
//                                        .getSystemService(Context.CLIPBOARD_SERVICE);
//                                clipboard2.setText(messageBody.getText());
//                            } else {
                                ClipData clip = ClipData.newPlainText("msg",messageBody.getText());
                                ClipboardManager clipboard;
                                clipboard = (ClipboardManager) mActivity
                                        .getSystemService(Context.CLIPBOARD_SERVICE);
                                clipboard.setPrimaryClip(clip);
                            ShineCommons.trackCustomEvents("AppExit", "LinkedIn", mActivity);

//                            }

                            mActivity.getPackageManager().getPackageInfo(
                                    "com.linkedin.android", 0);
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));

                        } catch (PackageManager.NameNotFoundException e) {
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                    .parse(recipientProfileUrl)));
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.findViewById(R.id.no_btn).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();

                ShineCommons.trackShineEvents("LinkedinMessageFailed","JobReferralConnect",mActivity);




                //show Dialog
//                Toast.makeText(
//                        mActivity,
//                        "LinkedIn contacts synced. But User not Found. Unable to send Message",
//                        Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailedLinkdin() {

            if (customDialog != null)
                customDialog.dismiss();

            Toast.makeText(
                    mActivity,
                    "LinkedIn sync failed. Please try again in some time.",
                    Toast.LENGTH_SHORT).show();


        }
    }



    Dialog auth_dialog;
    WebView web;

    private void LinkedinConnect() {


        auth_dialog = new Dialog(mActivity,
                android.R.style.Theme_Translucent_NoTitleBar);
        auth_dialog.setContentView(R.layout.auth_dialog);
        web = (WebView) auth_dialog.findViewById(R.id.webv);
        auth_dialog.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

        String authUrl = LinkedinConstants.getAuthorizationUrl();
        web.loadUrl(authUrl);
        web.getSettings().setJavaScriptEnabled(true);

        web.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                auth_dialog.findViewById(R.id.progress_bar).setVisibility(
                        View.GONE);
                web.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String authorizationUrl) {

                if (authorizationUrl.startsWith(LinkedinConstants.REDIRECT_URI)) {
                    Uri uri = Uri.parse(authorizationUrl);
                    Log.d("LINKED IN AUTH RESPONSE", authorizationUrl);

                    String error = uri.getQueryParameter("error");
                    if (error != null) {
                        Toast.makeText(
                                mActivity,
                                "Error occured : " + error,
                                Toast.LENGTH_SHORT).show();
                        auth_dialog.dismiss();
                        return true;
                    }

                    String stateToken = uri
                            .getQueryParameter(LinkedinConstants.STATE_PARAM);
                    if (stateToken == null
                            || !stateToken.equals(LinkedinConstants.STATE)) {
                        Toast.makeText(mActivity,"Unable to send message.", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(mActivity,
//                                "Sorry..State Token doesn't match",
//                                Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    String authorizationToken = uri
                            .getQueryParameter(LinkedinConstants.RESPONSE_TYPE_VALUE);
                    if (authorizationToken == null) {
//                        Toast.makeText(mActivity,
//                                "Sorry..You did not allow authorization",
//                                Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    String accessTokenUrl = LinkedinConstants
                            .getAccessTokenUrl(authorizationToken);

                    new GetAccessTokenAsyncTask().execute(accessTokenUrl);

                } else {
                    web.loadUrl(authorizationUrl);
                }
                return true;
            }

        });
        auth_dialog.setTitle("LinkedIn Authentication");
        if (!mActivity.isFinishing())
            auth_dialog.show();
    }


    private class GetAccessTokenAsyncTask extends AsyncTask<String, Void, Boolean> {


        ProgressDialog customDialog;

        @Override
        protected void onPreExecute() {

            customDialog = new ProgressDialog(mActivity, ProgressDialog.STYLE_SPINNER);
            customDialog.setMessage("Authorizing...");
            customDialog.setCancelable(false);
            customDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... urls) {
            if (urls.length > 0) {
                String url = urls[0];
                try {

                    StringBuilder content = new StringBuilder();
                    URL mUrl = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    if (connection.getResponseCode() == 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null)
                            content.append(line).append("\n");
                        bufferedReader.close();
                        JSONObject resultJson = new JSONObject(content.toString());

                            int expiresIn = resultJson.has("expires_in") ? resultJson
                                    .getInt("expires_in") : 0;
                            accessToken = resultJson.has("access_token") ? resultJson
                                    .getString("access_token") : null;
                            if (expiresIn > 0 && accessToken != null) {
                                Log.i("DEBUG", "This is the access Token: "
                                        + accessToken + ". It will expires in "
                                        + expiresIn + " secs");
                                Calendar calendar = Calendar.getInstance();
                                calendar.add(Calendar.SECOND, expiresIn);
                                long expireDate = calendar.getTimeInMillis();

                                ShineSharedPreferences.setLinkedinAccessToken(accessToken, mActivity);
                                ShineSharedPreferences.setLinkedinAccessExpiry(""+expireDate, mActivity);

                                return true;
                            }
                        } else {
                            return false;
                        }

                } catch (Exception e) {
                    e.printStackTrace();

                }

            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status) {
            customDialog.dismiss();
            if (status) {
                auth_dialog.hide();
                new SendMessageAsyncTask().execute(LinkedinConstants.getSendMessageUrl(accessToken));
            } else {
                Toast.makeText(mActivity,
                        "Unable to get authorization.",
                        Toast.LENGTH_SHORT).show();
             ShineCommons.trackShineEvents("LinkedInLoginFail","SendLinkedInMessage",mActivity);
            }
        }

    }




}
