package com.net.shine.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.net.shine.R;

/**
 * Created by manish on 10/10/16.
 */
public class CustomWebView extends AppCompatActivity {
    private WebView webView;
    private String shineUrl;
    private ProgressBar progressBar;


//    @Override
//    public boolean onCreateOptionsMenu (Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.share_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        super.onOptionsItemSelected(item);
//        switch (item.getItemId()) {
//            case R.id.action_share:
//                if (shineUrl != null) {
//                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
//                    shareIntent.setType("text/plain");
//                    shareIntent.putExtra(Intent.EXTRA_TEXT, shineUrl);
//
//                    Log.d("url to share",shineUrl);
//                    Intent chooserIntent = Intent.createChooser(shareIntent, "Share url");
//                    chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(chooserIntent);
//                }
//                return true;
//        }
//        return false;
//    }

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            shineUrl = extras.getString("shineurl");
            Log.d("shineurl", shineUrl);
        }

        webView = (WebView) findViewById(R.id.my_webview);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        webView.setWebViewClient(new WebViewClient());
        webView.setHapticFeedbackEnabled(false);
        WebSettings w = webView.getSettings();
        webView.getSettings().setAllowFileAccess(true);
        w.setLoadWithOverviewMode(true);
        w.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        w.setUseWideViewPort(true);
        w.setJavaScriptEnabled(true);
        w.setSupportZoom(true);
        w.setBuiltInZoomControls(true);
        w.setDisplayZoomControls(false);

        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        w.setPluginState(WebSettings.PluginState.ON);
        webView.requestFocusFromTouch();
        webView.loadUrl(shineUrl);
        webView.setEnabled(false);
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {

                progressBar.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
                super.onPageFinished(view, url);
            }

        });

    }



    @Override
    public void onBackPressed() {
        if (webView.canGoBack())
            webView.goBack();
        else
            super.onBackPressed();
    }


}
