package com.net.shine.interfaces;

import android.app.Dialog;

import com.net.shine.models.SocialApplyResponseModel;


/**
 * Created by ujjawal-work on 04/07/16.
 */

public interface SocialAppliedListener {
    void onApplied(String msg, SocialApplyResponseModel token, Dialog dialog);

}
