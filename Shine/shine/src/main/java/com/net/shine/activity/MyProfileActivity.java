package com.net.shine.activity;

import android.content.Intent;
import android.os.Bundle;

import com.net.shine.R;
import com.net.shine.fragments.MyProfileFrg;

/**
 * Created by ujjawal-work on 22/08/16.
 */

public class MyProfileActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            setBaseFrg(MyProfileFrg.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(getString(R.string.my_profile));
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(callerCompat!=null){
//            callerCompat.onActivityResult(requestCode, resultCode, data);
//        }
//
//    }
//
//
//    public GoogleResolutionOnResult callerCompat;
//
//    public interface GoogleResolutionOnResult {
//        void onActivityResult(int requestCode, int resultCode, Intent data);
//    }



}
