package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.InboxTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.NotificationModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * Created by manishpoddar on 30/06/17.
 */

public class NotificationsFrag extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener {

    private NotificationModel model;

    public static NotificationsFrag newInstance() {

        Bundle args = new Bundle();

        NotificationsFrag fragment = new NotificationsFrag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notification_fragment, container, false);
        final String notificationData = ShineSharedPreferences.getNotificationData(mActivity,
                ShineSharedPreferences.getCandidateId(mActivity) + "_noti");
        RelativeLayout errorlayout = (RelativeLayout) view.findViewById(R.id.error_layout);

        NotificationTimestampUpdate();

        if (!TextUtils.isEmpty(notificationData)) {

            Log.d("inside_notidata", notificationData);
            Gson gson = new Gson();

            model = gson.fromJson(notificationData, NotificationModel.class);

            if (model.newData != null) {

                errorlayout.setVisibility(View.GONE);
                Log.d("inside_newdata", notificationData);
                if (model.newData.jobAlert != null) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container1);
                    layout.setOnClickListener(this);
                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title1);
                    TextView description = (TextView) view.findViewById(R.id.desc1);
                    heading.setText("Job alert");
                    description.setText("You have " + model.newData.jobAlert.count + " new job alert emails");

                    String date = ShineCommons.getFormattedDate(model.newData.jobAlert.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number1);
                    TextView monthName = (TextView) view.findViewById(R.id.month1);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout1);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);

                }
                if (model.newData.recruiterMail != null) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container2);
                    layout.setOnClickListener(this);

                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title2);
                    TextView description = (TextView) view.findViewById(R.id.desc2);
                    heading.setText("Recruiter mail");
                    description.setText("You have " + model.newData.recruiterMail.count + " new recruiter emails");

                    String date = ShineCommons.getFormattedDate(model.newData.recruiterMail.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number2);
                    TextView monthName = (TextView) view.findViewById(R.id.month2);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout2);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);

                }
                if (model.newData.whoViewed != null) {
                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container3);
                    layout.setOnClickListener(this);

                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title3);
                    TextView description = (TextView) view.findViewById(R.id.desc3);
                    heading.setText("Recruiter activity");
                    description.setText(model.newData.whoViewed.count + " new recruiter activities on your profile");

                    String date = ShineCommons.getFormattedDate(model.newData.whoViewed.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number3);
                    TextView monthName = (TextView) view.findViewById(R.id.month3);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout3);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);
                }
            }

            if (model.oldData != null) {
                Log.d("indise_olddata", notificationData);
                errorlayout.setVisibility(View.GONE);
                if (model.oldData.jobAlert != null) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container4);
                    layout.setOnClickListener(this);

                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title4);
                    TextView description = (TextView) view.findViewById(R.id.desc4);
                    heading.setText("Job alert");
                    description.setText("You have " + model.oldData.jobAlert.count + " new job alert emails");

                    String date = ShineCommons.getFormattedDate(model.oldData.jobAlert.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number4);
                    TextView monthName = (TextView) view.findViewById(R.id.month4);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout4);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);
                }

                if (model.oldData.recruiterMail != null) {

                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container5);
                    layout.setOnClickListener(this);

                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title5);
                    TextView description = (TextView) view.findViewById(R.id.desc5);
                    heading.setText("Recruiter mail");
                    description.setText("You have " + model.oldData.recruiterMail.count + " new recruiter emails");

                    String date = ShineCommons.getFormattedDate(model.oldData.recruiterMail.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number5);
                    TextView monthName = (TextView) view.findViewById(R.id.month5);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout5);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);


                }
                if (model.oldData.whoViewed != null) {
                    LinearLayout layout = (LinearLayout) view.findViewById(R.id.container6);
                    layout.setOnClickListener(this);

                    layout.setVisibility(View.VISIBLE);
                    TextView heading = (TextView) view.findViewById(R.id.title6);
                    TextView description = (TextView) view.findViewById(R.id.desc6);
                    heading.setText("Recruiter activity");
                    description.setText(model.oldData.whoViewed.count + " new recruiter activities on your profile");

                    String date = ShineCommons.getFormattedDate(model.oldData.whoViewed.timestamp.substring(0, 10));
                    String date_number = date.substring(0, 2);
                    String month = "";
                    if (date_number != null && date_number.trim().length() == 1) {
                        month = date.substring(2, 6).toUpperCase();

                        date_number = "0" + date_number;
                    } else {

                        month = date.substring(3, 6).toUpperCase();
                    }
                    Log.d("date_inbox", date_number + " " + month);

                    TextView dateNumber = (TextView) view.findViewById(R.id.date_number6);
                    TextView monthName = (TextView) view.findViewById(R.id.month6);
                    LinearLayout imageView = (LinearLayout) view.findViewById(R.id.image_layout6);

                    dateNumber.setText(date_number);
                    monthName.setText(month);
                    ColorGenerator generator = ColorGenerator.MATERIAL;
                    int color = generator.getColor(date_number);
                    TextDrawable drawable = TextDrawable.builder()
                            .buildRect("", color);
                    imageView.setBackground(drawable);
                }
            }

            if (model.newData.whoViewed == null && model.newData.recruiterMail == null && model.newData.jobAlert == null
                    && model.oldData.whoViewed == null && model.oldData.recruiterMail == null && model.oldData.jobAlert == null) {

                Log.d("outside_notidata", notificationData);

                errorlayout.setVisibility(View.VISIBLE);
                DialogUtils.showErrorActionableMessage(mActivity, errorlayout,
                        "", "Presently, you have no new notifications ", "", DialogUtils.ERR_NO_NOTIFICATION, null);
            }
        } else {
            errorlayout.setVisibility(View.VISIBLE);
            DialogUtils.showErrorActionableMessage(mActivity, errorlayout,
                    "", "Presently, you have no new notifications ", "", DialogUtils.ERR_NO_NOTIFICATION, null);
        }


        return view;
    }


    public void NotificationTimestampUpdate() {

        String url = URLConfig.NOTIFICATION_TIMESTAMP_UPDATE_API.replace(URLConfig.CANDIDATE_ID,
                ShineSharedPreferences.getCandidateId(mActivity)) + "notification_bell_click";
        Type type = new TypeToken<EmptyModel>() {
        }.getType();
        VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this, url, type);

        HashMap<String, String> params = new HashMap<>();
        request.setUsePutMethod(params);
        request.execute("notification");

    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Notifications");
        mActivity.showNavBar();
        mActivity.showBackButton();
        mActivity.showSelectedNavButton(mActivity.MORE_OPTION);
        ManualScreenTracker.manualTracker("Notification");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.container1:
            case R.id.container4: {
                BaseFragment fragment = new InboxTabFrg();
                Bundle bundle = new Bundle();
                bundle.putInt(MainTabFrg.SELECTED_TAB, InboxTabFrg.ALERT_MAIL_TAB);
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                ShineCommons.trackShineEvents("Notification-Job Alert","Notification",mActivity);

                break;
            }

            case R.id.container2:
            case R.id.container5: {
                BaseFragment fragment1 = new InboxTabFrg();
                Bundle bundle1 = new Bundle();
                bundle1.putInt(MainTabFrg.SELECTED_TAB, InboxTabFrg.RECRUITER_MAIL_TAB);
                fragment1.setArguments(bundle1);
                mActivity.singleInstanceReplaceFragment(fragment1);
                ShineCommons.trackShineEvents("Notification-Recruiter Mail","Notification",mActivity);

                break;
            }

            case R.id.container3:
            case R.id.container6: {
                BaseFragment fragment2 = new MyProfileViewFrg();
                mActivity.singleInstanceReplaceFragment(fragment2);
                ShineCommons.trackShineEvents("Notification-Profile Views","Notification",mActivity);

                break;
            }
            default:
                break;


        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        Log.d("timestamp_update", object + "");

    }

    @Override
    public void OnDataResponseError(String error, String tag) {

        Log.d("timestamp_update", error);


    }
}
