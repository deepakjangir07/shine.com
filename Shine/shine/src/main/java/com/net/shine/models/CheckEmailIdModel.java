package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by dhruv on 3/8/15.
 */
public class CheckEmailIdModel implements Serializable {

    Boolean exists;

    public Boolean getExists() {
        return exists;
    }

    public void setExists(Boolean exists) {
        this.exists = exists;
    }
}
