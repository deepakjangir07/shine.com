package com.net.shine.utils.auth;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.HomeActivity;
import com.net.shine.activity.RegistrationCompleteActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.models.RegDumpModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;

/**
 * Created by ujjawal-work on 18/08/16.
 */

public class ShineAuthUtils {

    public static final int REQUESTED_TO_APPLY_JOBS = 6;
    public static final int REQUESTED_FOR_APPLIED_JOBS =11;
    public static final int REQUESTED_FOR_PROFILE =4;
    public static final int REQUESTED_FOR_WHO_VIEWED =8;
    public static final int REQUESTED_FOR_MATCHED_JOB =10;
    public static final int REQUESTED_NORMAL = 0;
    public static final int REQUESTED_TO_KONNECT = 5;
    public static final int REQUESTED_FOR_REFERRAL_RECEIVED =12;
    public static final int REQUESTED_FOR_REFERRAL_REQUESTED =13;
    public static final int REQUESTED_FOR_FEEDBACK =14;
    public static final int REQUESTED_FOR_RESUME = 15;
    public static final int REQUESTED_FOR_APP_INVITE =16;
    public static final int REQUESTED_FOR_ALERT_MAIL =17;
    public static final int REQUESTED_FOR_RECRUITER_MAIL = 21;
    public static final int REQUESTED_FOR_JOB_ALERT =18;
    public static final int REQUESTED_FOR_MESSAGES = 20;
    public static final int REQUESTED_FOR_SEARCH = 22;
    public static final int REQUESTED_FOR_CONNECTION_JOBS =19;
    public static final String REQUESTED_TYPE_KEY = "login_type";
    public static final String COMPLETED_FROM = "login_complete_source";
    public static final int FROM_SOCIAL_APPLY_REGISTRATION = 0;
    public static final int FROM_WEB_MIDOUT_REGISTRATION = 1;
    public static final int FROM_NORMAL_REGISTRATION = 3;
    public static final int REQUESTES_FOR_JOB_DETAIL = 23;
    public static final int FROM_NORMAL = 2;

    public static final int REQUESTED_FOR_EDUCATION_ADD = 25;
    public static final int REQUESTED_FOR_EMPLOYMENT_ADD = 26;
    public static final int REQUESTED_FOR_EXPERIENCE = 27;
    public static final int REQUESTED_FOR_CERTIFICATION = 28;
    public static final int REQUESTED_FOR_DESIRED_JOB_DETAIL = 29;
    public static final int REQUESTED_FOR_SKILL = 30;
    public static final int REQUESTED_FOR_NON_SKIP_REGISTERATION=24;
    public static final int REQUESTED_FOR_SAVED_JOBS = 31;
    public static final int REQUESTED_FOR_RECOMMEDED_JOBS = 32;
    public static final int REQUESTED_FOR_INBOX_TAB = 33;

    public static void LoginUser(UserStatusModel model, final BaseActivity mActivity, final Bundle bundle){

        if (model.is_mid_out != 1) {

            Log.d("SPLASh","user is not midout "+model.is_mid_out);
            ShineSharedPreferences.saveUserInfo(mActivity, model);
        }
        else
        {
            Log.d("SPLASh","user mideout "+model.is_mid_out);
        }

        //is_mid_out values= 0,1,2(0=full,1=web_midout,2=)


        MoengageTracking.setLoginAttribute(mActivity);
        MoengageTracking.setProfileAttributes(mActivity);

        ShineCommons.sendCustomDimension(model,"LoginScreen");


        if (model.profile_title != null && (model.profile_title.length() > 0)) {
            ShineSharedPreferences.setProfileFlag(mActivity, true);
        }
        if (model.is_resume_mid_out && model.is_mid_out != 1) {
            if (!model.getIsPhonebookSynced()) {
                ShineCommons.sendContact();
            } else {
                ShineSharedPreferences
                        .savePhoneBookReadProgress(
                                mActivity, false);
                ShineSharedPreferences
                        .savePhoneBookDelivered(mActivity,
                                true);
                ShineSharedPreferences.saveSyncPhTime(
                        mActivity.getApplicationContext(),
                        model.get_next_phonebook_sync_on(),
                        model.candidate_id);
            }
            ShineCommons.trackShineEvents("LoginUserInfo","ResumeMidoutUser",mActivity);

            Intent intent = new Intent(mActivity,
                    HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(bundle);
            mActivity.startActivity(intent);
            mActivity.finish();

        } else if ((!model.has_jobs
                || !model.has_education || !model.has_skills)
                && !model.is_resume_mid_out && model.is_mid_out != 1) {
            ShineSharedPreferences.setDashboard(mActivity, true);
            if (!model.getIsPhonebookSynced()) {
                ShineCommons.sendContact();
            } else {
                ShineSharedPreferences
                        .savePhoneBookReadProgress(
                                mActivity, false);
                ShineSharedPreferences
                        .savePhoneBookDelivered(mActivity,
                                true);
                ShineSharedPreferences.saveSyncPhTime(
                        mActivity.getApplicationContext(),
                        model.get_next_phonebook_sync_on(),
                        model.candidate_id);
            }
            ShineCommons.trackShineEvents("LoginUserInfo","ProfileMidoutUser",mActivity);

            final Intent intentToLaunch= new Intent(mActivity,
                    HomeActivity.class);
            intentToLaunch.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intentToLaunch.putExtras(bundle);

            final Dialog alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
            Type listType = new TypeToken<RegDumpModel>() {}.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    DialogUtils.dialogDismiss(alertDialog);
                    if(object!=null) {
                        ShineSharedPreferences.saveRegDumpInfo(mActivity, (RegDumpModel) object);
                    }
                    mActivity.startActivity(intentToLaunch);
                    mActivity.finish();
                }

                @Override
                public void OnDataResponseError(String error, String tag) {
                    DialogUtils.dialogDismiss(alertDialog);
                    mActivity.startActivity(intentToLaunch);
                    mActivity.finish();
                }
            }, URLConfig.REG_DUMP_API.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)), listType);
            downloader.execute("Reg-Dump");
        } else if (model.is_mid_out == 1) {

                //Registration
                final Bundle data = new Bundle();
                data.putAll(bundle);
                data.putBoolean(URLConfig.PHONEBOOK_SYNC_STATUS, model.getIsPhonebookSynced());
                data.putString(URLConfig.USERID, model.candidate_id);
                data.putString(URLConfig.USERNAME, model.full_name);
                data.putString(URLConfig.JOBTITLE, model.job_title);
                data.putString(URLConfig.NEXTSTATUS, String.valueOf(model.is_mid_out));
                data.putString(URLConfig.USEREMAIL, model.email);
                data.putString(URLConfig.USEREMOB, model.mobile_no);
                data.putString(URLConfig.PHONEBOOK_SYNC_TIME, model.get_next_phonebook_sync_on());
                data.putSerializable("userStatusModel", model);

                final Dialog alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                Type listType = new TypeToken<RegDumpModel>() {}.getType();
                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                    @Override
                    public void OnDataResponse(Object object, String tag) {
                        DialogUtils.dialogDismiss(alertDialog);
                        Log.d("Debug::Reg-dump", object + " ");
                        if(object!=null) {
                            ShineSharedPreferences.saveRegDumpInfo(mActivity, (RegDumpModel) object);
                        }


                        Intent intent=new Intent(mActivity,RegistrationCompleteActivity.class);
                        intent.putExtras(data);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        mActivity.startActivity(intent);
                        mActivity.finish();

                    }

                    @Override
                    public void OnDataResponseError(String error, String tag) {
                        Log.d("Debug::Reg-dump", error + " ");
                        DialogUtils.dialogDismiss(alertDialog);


                        Intent intent=new Intent(mActivity,RegistrationCompleteActivity.class);
                        intent.putExtras(data);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        mActivity.startActivity(intent);

                        mActivity.finish();

                    }
                },
                        URLConfig.REG_DUMP_API.replace(URLConfig.CANDIDATE_ID, model.candidate_id), listType);
                downloader.execute("Reg-Dump");
                ShineCommons.trackShineEvents("LoginUserInfo","WebMidoutUser",mActivity);

        } else {
            if (!model.getIsPhonebookSynced()) {
                ShineCommons.sendContact();
            } else {
                ShineSharedPreferences
                        .savePhoneBookReadProgress(
                                mActivity, false);
                ShineSharedPreferences
                        .savePhoneBookDelivered(mActivity,
                                true);
                ShineSharedPreferences.saveSyncPhTime(
                        mActivity.getApplicationContext(),
                        model.get_next_phonebook_sync_on(),
                        model.candidate_id);
            }
            ShineCommons.trackShineEvents("LoginUserInfo","ProfileCompleteUser",mActivity);


                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
                mActivity.finish();
        }

    }

}
