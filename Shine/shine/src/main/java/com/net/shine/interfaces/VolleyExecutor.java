package com.net.shine.interfaces;

public interface VolleyExecutor {

    void execute(String tag);
}
