package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SkillModel implements Serializable{

	@SerializedName("name")
	private String skillName = "";
	@SerializedName("experience")

	private int skilllevelIndex = -1;
	@SerializedName("id")

	private String skillId = "";

	public String getSkillId() {
		return skillId;
	}
	public void setSkillId(String skillId) {
		this.skillId = skillId;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public int getSkilllevelIndex() {
		return skilllevelIndex;
	}
	public void setSkilllevelIndex(int skilllevelIndex) {
		this.skilllevelIndex = skilllevelIndex;
	}

}
