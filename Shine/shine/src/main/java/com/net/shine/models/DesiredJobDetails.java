package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class DesiredJobDetails implements Serializable {
    public int[] getLocation() {
        return location;
    }

    public void setLocation(int[] location) {
        this.location = location;
    }

    public int[] getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(int[] minSalary) {
        this.minSalary = minSalary;
    }

    public int[] getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(int[] maxSalary) {
        this.maxSalary = maxSalary;
    }

    public int[] getFunctionalArea() {
        return functionalArea;
    }

    public void setFunctionalArea(int[] functionalArea) {
        this.functionalArea = functionalArea;
    }

    public int[] getIndustry() {
        return industry;
    }

    public void setIndustry(int[] industry) {
        this.industry = industry;
    }

    public int[] getShiftType() {
        return shiftType;
    }

    public void setShiftType(int[] shiftType) {
        this.shiftType = shiftType;
    }

    public int[] getJobType() {
        return jobType;
    }

    public void setJobType(int[] jobType) {
        this.jobType = jobType;
    }

    @SerializedName("candidate_location")
    private int [] location = new int[]{};

    @SerializedName("minimum_salary")
    private int [] minSalary = new int[]{};
    @SerializedName("maximum_salary")
    private int []maxSalary = new int[]{};

    @SerializedName("functional_area")
    private int [] functionalArea = new int[]{};

    @SerializedName("industry")
    private int [] industry = new int[]{};

    @SerializedName("shift_type")
    private int [] shiftType = new int[]{};

    @SerializedName("job_type")
    private int [] jobType = new int[]{};
}
