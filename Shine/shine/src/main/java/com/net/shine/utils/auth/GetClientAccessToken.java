package com.net.shine.utils.auth;

import android.content.Context;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.config.ServerConfig;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.models.AuthTokenModel;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;


public  class GetClientAccessToken implements VolleyRequestCompleteListener {





    public static String APP_API_KEY = ServerConfig.API_KEY;
    public static String APP_API_SECRET = ServerConfig.API_SECRET;



    private Context mContext;
    private VolleyNetworkRequest downloader;
    private String ApiHitTag;


    public GetClientAccessToken(Context mContext,VolleyNetworkRequest downloader,String ApiHitTag)
    {
        this.mContext=mContext;
        this.ApiHitTag=ApiHitTag;
        this.downloader=downloader;

    }
    public GetClientAccessToken(Context mContext)
    {
        this.mContext=mContext;
    }


    public  void getClientAccessToken()
    {

        Type type = new TypeToken<AuthTokenModel>(){}.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mContext, this,
                URLConfig.CLIENT_AUTH_API, type);
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put("key", APP_API_KEY);
        paramsMap.put("secret",APP_API_SECRET );
        req.setUsePostMethod(paramsMap);
        req.execute("ClientAccessToken");

    }
    @Override
    public void OnDataResponse(final Object object, String tag) {

        AuthTokenModel model = (AuthTokenModel) object;

               ShineSharedPreferences.setClientAccessToken(mContext,model.access_token);
                if(downloader!=null)
                {

                    downloader.addHeader(URLConfig.CLIENT_ACCESS_TOKEN,model.access_token);
                    downloader.execute(ApiHitTag);

                }
    }

    @Override
    public void OnDataResponseError(String error, String tag) {

          Log.d("CLIENT DEBUG ERROR ","ERROR  is "+error);
    }


}
