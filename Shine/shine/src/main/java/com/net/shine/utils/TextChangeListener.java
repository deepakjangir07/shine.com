package com.net.shine.utils;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import com.net.shine.R;

/**
 * Created by jaspreet on 6/11/15.
 */
public class TextChangeListener {


    public static void addListener(TextView tv, TextInputLayout tl, Activity a){


        final TextView tt;
        final TextInputLayout til;
        final Activity mActivity;
        tt = tv;
        til = tl;
        mActivity = a;
        tv.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try {
                    tt.setTextColor(mActivity.getResources().getColor(R.color.dark_grey));
                    tt.setHintTextColor(mActivity.getResources().getColor(R.color.labelColor));
                    til.setError(null);
                    til.setErrorEnabled(false);
//                    if(Build.VERSION.SDK_INT<16){
//                        tt.setBackgroundDrawable(tt.getBackground().getConstantState().newDrawable());
//                    } else {
//                        tt.setBackground(tt.getBackground().getConstantState().newDrawable());
//                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

}
