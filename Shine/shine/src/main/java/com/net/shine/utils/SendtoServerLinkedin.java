package com.net.shine.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedinDataSendListener;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class SendtoServerLinkedin implements VolleyRequestCompleteListener {

    private Context mContext;
    private JSONObject data;
    private LinkedinDataSendListener successCallBackListener;

    public SendtoServerLinkedin(Context context, JSONObject jsondata, LinkedinDataSendListener listener)
    {
        this.mContext=context;
        this.data=jsondata;
        this.successCallBackListener=listener;
    }


    public void sendtoServerLinkedInData()
    {
        try {
            HashMap<String,Object> paramsMap = new HashMap<>();
            paramsMap.put("shine_id", ShineSharedPreferences.getCandidateId(mContext));
            paramsMap.put("source","linkedin");
            paramsMap.put("data", data);
            paramsMap.put("access_token", ShineSharedPreferences.getLinkedinAccessToken(mContext));
//            paramsMap.put("linkedin_token_expiry", ShineSharedPreferences.getLinkedinAccessExpiry(mContext)+"");

            Log.d("DEBUG_LINKEDIN", "Dumping data to shine");

            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest request= new VolleyNetworkRequest(mContext,this, URLConfig.KONNECT_LINKEDIN_URL, type);
            request.setUsePostMethod(paramsMap);
            request.execute("DumpLinkedInData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

                successCallBackListener.onSuccessLinkdin();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        try {

            successCallBackListener.onFailedLinkdin();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
