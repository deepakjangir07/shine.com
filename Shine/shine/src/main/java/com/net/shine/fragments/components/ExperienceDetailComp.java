package com.net.shine.fragments.components;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.TotalExperience;

/**
 * Created by gobletsky on 7/7/15.
 */
public class ExperienceDetailComp implements  View.OnClickListener {


    private Activity mActivity;
    private View view;
    private TotalExperience totalExModel;
    private View personalView;


    public ExperienceDetailComp(Activity mActivity, View view, View personalView) {
        this.mActivity = mActivity;
        this.view = view;
        this.personalView = personalView;
    }

    public void showTotalExpDetails(TotalExperience model) {
        try {

            this.totalExModel = model;

            ImageView exp_edit = (ImageView) view.findViewById(R.id.exp_edit);
            exp_edit.setOnClickListener(this);

            String profileTitle = model.getResumeTitle();
            TextView profile_title_tv = (TextView) view
                    .findViewById(R.id.profile_title_tv);
            if (profileTitle == null || profileTitle.equals(""))
            {
                profileTitle = "Not Mentioned";
            }
            profile_title_tv.setText(profileTitle);


            TextView annual_salary_tv = (TextView) view
                    .findViewById(R.id.annual_salary_tv);
            if ( URLConfig.ANNUAL_SALARYL_REVRSE_MAP.get("" + model.getSalaryLIndex()) == null
                    || URLConfig.ANNUAL_SALARYL_REVRSE_MAP.get("" + model.getSalaryLIndex()).equals("null")) {

                annual_salary_tv.setText("Not Mentioned");


            } else {

                annual_salary_tv.setText("Rs."
                        + URLConfig.ANNUAL_SALARYL_REVRSE_MAP.get(""
                        + model.getSalaryLIndex())
                        + " lakh "
                        + URLConfig.ANNUAL_SALARYT_REVRSE_MAP.get(""
                        + model.getSalaryTIndex()) + " Thousands");

                String sal = ""
                        + URLConfig.ANNUAL_SALARYL_REVRSE_MAP.get(""
                        + model.getSalaryLIndex())
                        + "."
                        + URLConfig.ANNUAL_SALARYT_REVRSE_MAP.get(""
                        + model.getSalaryTIndex());
                if(sal.contains("+"))
                {
                    //shift the plus sign to end
                    sal = sal.replace("+", "") + "+ Lakh";
                }
                else
                {
                    sal = sal + " Lakh";
                }

            }

            TextView total_exp_tv = (TextView) view.findViewById(R.id.total_exp_tv);
            total_exp_tv.setText(""
                    + URLConfig.EXPERIENCE_REVERSE_MAP.get(""
                    + model.getTotalExperienceIndex())
                    + " - "
                    + URLConfig.EXPERIENCE_MONTHS_REVERSE_MAP.get(""
                    + model.getMonthExperienceIndex()));



            TextView team_handled_tv = (TextView) view.findViewById(R.id.team_handled_tv);
            String teamSize = URLConfig.TEAM_SIZE_REVRSE_MAP.get("" + model.getHandledTeamSizeIndex());
            if (teamSize == null || "".equals(teamSize)) {
                 teamSize = mActivity.getString(R.string.not_mentioned);
            }
            team_handled_tv.setText(teamSize);



            TextView notice_period_tv = (TextView) view.findViewById(R.id.notice_period_tv);
                if (model.getNoticePeriodIndex() == -1)
                {
                    notice_period_tv.setText("Not Mentioned");
                }
                else
                {
                    notice_period_tv.setText(URLConfig.NOTICE_PERIOD_REVRSE_MAP.get(""
                            + model.getNoticePeriodIndex()));
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.exp_edit:
                    Bundle bundle = new Bundle();
                    Gson gson = new Gson();
                    String json = gson.toJson(totalExModel);
                    bundle.putString(URLConfig.KEY_MODEL_NAME, json);
                    BaseFragment frg = new ExperienceDetailsEditFragment();
                    frg.setArguments(bundle);
                    ((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);



                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




}
