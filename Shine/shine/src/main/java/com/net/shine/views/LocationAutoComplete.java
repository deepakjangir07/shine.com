package com.net.shine.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Filterable;
import android.widget.ListAdapter;

import com.net.shine.adapters.RelevantSuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by manishpoddar on 06/04/16.
 */
public class LocationAutoComplete extends MyAutoCompleteTextView{

    private TextWatcher resetter = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setAdapter(mOriginalAdapter);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public LocationAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addTextChangedListener(resetter);

    }
    public LocationAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        addTextChangedListener(resetter);

    }


    public LocationAutoComplete(Context context) {
        super(context);
        addTextChangedListener(resetter);

    }

    private RelevantSuggestionsFilterableArrayAdapter mOriginalAdapter;

    @Override
    public <T extends ListAdapter & Filterable> void setAdapter(T adapter) {
        super.setAdapter(adapter);
        mOriginalAdapter = (RelevantSuggestionsFilterableArrayAdapter) adapter;
    }

    @Override
    protected void replaceText(CharSequence text) {

        String curr = getText().toString();

        if(TextUtils.isEmpty(curr)){

            setText(text+", ");
        }else {
            int in = curr.lastIndexOf(", ");
            if(in==-1){
                setText(text + ", ");
            }else{
                setText(curr.substring(0, in)+ ", " + text + ", ");
            }
        }
        try {
            performRelevantFiltering(text.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    private void performRelevantFiltering(final String text) throws UnsupportedEncodingException {

                String arr[] = URLConfig.CITY_LIST;
                ArrayList<String> list = new ArrayList<>();
                for(String s:arr)
                    list.add(s);
                for(String s:arr){
                    if(getText().toString().toLowerCase().contains(s.toLowerCase()) && list.contains(s)){
                        list.remove(s);
                    }
                }
                arr = new String[list.size()];
                for(int i=0;i<list.size();i++)
                    arr[i] = list.get(i);
                setRelevantAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        arr));
                showDropDown();

            }


    public <T extends ListAdapter & Filterable> void setRelevantAdapter(T adapter){
        super.setAdapter(adapter);

    }



}

