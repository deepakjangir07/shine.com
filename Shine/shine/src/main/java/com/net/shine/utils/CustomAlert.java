package com.net.shine.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyCustomJobAlertFragment;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.models.CustomJobAlertModel;
import com.net.shine.models.SetFacetsModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomAlert implements VolleyRequestCompleteListener {
	private BaseActivity mActivity;
	private Dialog alertDialog;
	private View parentView;
	private boolean isUpdate = false, isDelete = false, isAdd = false;
	public static final int ADD_ALERT_THROUGH_FORM = 1;
	public static final int ADD_ALERT_THROUGH_SEARCH = 2;
	private int add_alert_option = ADD_ALERT_THROUGH_FORM;
	private UserStatusModel userProfileVO;

    private MyCustomJobAlertFragment fragment;

    private  SimpleSearchVO searchVO;


    public CustomAlert(BaseActivity mActivity)
    {
        this.mActivity = mActivity;
    }

	public CustomAlert(BaseActivity mActivity, MyCustomJobAlertFragment mfragment) {
		this.mActivity = mActivity;
		userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        this.fragment=mfragment;
	}

	public void updateCustomAlert(UserStatusModel userProfileVO, SimpleSearchVO svo,
			String email, String keyword, String[] loc,
			String ind[], String area[], int sal, int exp) {
		isUpdate = true;
		isDelete = false;
		isAdd = false;
		alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                mActivity.getString(R.string.plz_wait));

        String URL="";
        String mEmail=email;
        if(userProfileVO!=null)
        {
            mEmail=userProfileVO.email;
            URL= URLConfig.UPDATE_JOB_ALERT.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity)).replace(URLConfig.JOB_ALERT_ID,svo.getJobId());
        }

        HashMap<String, Object> obj=new HashMap<>();

            obj.put("id",ShineSharedPreferences.getCandidateId(mActivity));
            obj.put("name",svo.getCustomJobAlertName());
            obj.put("email",mEmail);
            obj.put("keywords",keyword);
            if(loc!=null&&loc.length>0) {
                JSONArray jsonArray=new JSONArray();
                for (String aLoc : loc) {
                    jsonArray.put(aLoc);
                }
                obj.put("locid", jsonArray);
            }
            else  {
                List<String> blankArray=new ArrayList<>();
                obj.put("locid",new JSONArray(blankArray));
            }
                obj.put("sal",getStringValue(sal));
            if(area!=null&&(area.length>0)) {
                JSONArray areaJson=new JSONArray();
                for (String anArea : area) {
                    areaJson.put(anArea);
                }
                obj.put("area",areaJson);
            }
            else {
                List<String> areablank=new ArrayList<>();
                obj.put("area", new JSONArray(areablank));  //IF area ind,location blank then we have to send Empty [] to server
            }
            if((ind!=null)&&(ind.length>0)) {
                JSONArray indJson=new JSONArray();
                for (String anInd : ind) {
                    indJson.put(anInd);
                }
                obj.put("ind", indJson);
            }
            else {
                List<String> indblank=new ArrayList<>();
                obj.put("ind",new JSONArray(indblank));
            }
            obj.put("exp", getStringValue(exp));
            obj.put("medium", "2");


        Type type = new TypeToken<CustomJobAlertModel>(){}.getType();
        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,this,URL,type);
        downloader.setUsePutMethod(obj);
        downloader.execute("JobAlert");

	}


    public void saveCustomAlertFromFacets(UserStatusModel userProfileVO, SimpleSearchVO svo,
                                          String email, ArrayList<String> funct_area, SetFacetsModel setFacetsModel,
                                          View parentView) {
        isAdd = true;
        isUpdate = false;
        isDelete = false;
        this.parentView = parentView;
        alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                mActivity.getString(R.string.plz_wait));

        ArrayList<String> locTagArray = setFacetsModel.getLocation();
        ArrayList<String> salTagArray = setFacetsModel.getSalary();
        ArrayList<String> exTagArray = setFacetsModel.getExperience();
        ArrayList<String> indTagArray = setFacetsModel.getIndustry();
        ArrayList<String> funTagArray = setFacetsModel.getFunctionalArea();

        String URL;
        String mEmail;
        if(userProfileVO!=null)
        {
            mEmail=userProfileVO.email;
            URL= URLConfig.CUSTOM_JOB_ALERT_CREATE_USER.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity));
        }
        else {
            mEmail=email;
            URL= URLConfig.CUSTOM_ALERT_URL;
        }
        HashMap<String, Object> obj = new HashMap<>();
        try {


            if(TextUtils.isEmpty(svo.getLocation()))
            {
                obj.put("loc",new JSONArray());
            }
            else {
                obj.put("loc",new JSONArray().put(svo.getLocation()));
            }

            obj.put("locid",new JSONArray(new ArrayList<>()));
            obj.put("name",svo.getCustomJobAlertName());
            obj.put("email",mEmail);
            obj.put("keywords",svo.getKeyword());
            obj.put("sal",getStringValue(svo.getMinSalTag()));

            if((funct_area!=null)&&(funct_area.size()>0)) {
                JSONArray funcJSON=new JSONArray();
                for(int i=0;i<funct_area.size();i++) {
                    Log.d("FA ","funcation Area "+funct_area.get(i));
                    if(URLConfig.FUNCTIONA_AREA_MAP.get(funct_area.get(i))!=null)
                    funcJSON.put(URLConfig.FUNCTIONA_AREA_MAP.get(funct_area.get(i)));
                    else
                        funcJSON.put(funct_area.get(i));
                }
                obj.put("area", funcJSON);
            }
            else {
                List<String> func_area_blank=new ArrayList<>();
                obj.put("area",new JSONArray(func_area_blank));
            }
            obj.put("ind",getStringValue(svo.getIndusTypeTag()));
            obj.put("exp", getStringValue(svo.getMinExpTag()));
            obj.put("medium", "2");

            //// Send Facets params from here
            JSONObject post_params=new JSONObject();
            if((locTagArray!=null)&&(locTagArray.size()>0)) {

                JSONArray locidJson=new JSONArray();
                for(int i=0;i<locTagArray.size();i++) {
                    locidJson.put(locTagArray.get(i));
                }
                post_params.put("location", locidJson);
            }
            else {
                List<String> locationblank=new ArrayList<>();
                post_params.put("location",new JSONArray(locationblank));
            }

            if((salTagArray!=null)&&(salTagArray.size()>0)) {
                JSONArray salJSON=new JSONArray();
                for(int i=0;i<salTagArray.size();i++) {
                    salJSON.put(salTagArray.get(i));
                }
                post_params.put("fsalary", salJSON);
            }
            else {
                List<String> sal_area_blank=new ArrayList<>();
                post_params.put("fsalary",new JSONArray(sal_area_blank));
            }

            if((funTagArray!=null)&&(funTagArray.size()>0)) {
                JSONArray funcJSON=new JSONArray();
                for(int i=0;i<funTagArray.size();i++) {
                    funcJSON.put(funTagArray.get(i));
                }
                post_params.put("farea", funcJSON);
            }
            else {
                List<String> func_area_blank=new ArrayList<>();
                post_params.put("farea",new JSONArray(func_area_blank));
            }

            if((indTagArray!=null)&&(indTagArray.size()>0)) {
                JSONArray indJSON=new JSONArray();
                for(int i=0;i<indTagArray.size();i++) {
                    indJSON.put(indTagArray.get(i));
                }
                post_params.put("findustry", indJSON);
            }
            else {
                List<String> ind_area_blank=new ArrayList<>();
                post_params.put("findustry",new JSONArray(ind_area_blank));
            }

            if((exTagArray!=null)&&(exTagArray.size()>0)) {
                JSONArray expJSON=new JSONArray();
                for(int i=0;i<exTagArray.size();i++) {
                    expJSON.put(exTagArray.get(i));
                }
                post_params.put("fexp", expJSON);
            }
            else {
                List<String> exp_area_blank=new ArrayList<>();
                post_params.put("fexp",new JSONArray(exp_area_blank));
            }
            obj.put("post_params",post_params);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Type type = new TypeToken<CustomJobAlertModel>(){}.getType();

        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,CustomAlert.this, URL, type);
        downloader.setUsePostMethod(obj);
        downloader.execute("JobAlert");

    }


    public void saveCustomAlert(UserStatusModel userProfileVO,
			String email, String alertName, String keyword, String[] loc,
			String ind[], String area[], int sal, int exp,SimpleSearchVO sVO) {

        this.searchVO=sVO;

		isAdd = true;
		isUpdate = false;
		isDelete = false;
		alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
				mActivity.getString(R.string.plz_wait));

        String URL;
        String mEmail;

        if(userProfileVO!=null)
        {
            mEmail=userProfileVO.email;
            URL= URLConfig.CUSTOM_JOB_ALERT_CREATE_USER.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity));
        }
        else {
            mEmail=email;
            URL= URLConfig.CUSTOM_JOB_ALERT_CREATE;
        }

        HashMap<String, Object> obj=new HashMap<>();
            obj.put("name",alertName);
            obj.put("email",mEmail);
            obj.put("keywords",keyword);
            obj.put("sal",getStringValue(sal));


            if(loc!=null&&loc.length>0)
            {
                JSONArray locJson=new JSONArray();
                for (String aLoc : loc) {
                    locJson.put(aLoc);
                }
                obj.put("locid",locJson);
            }
            else {
                List<String> blankLoc=new ArrayList<>();
                obj.put("locid",new JSONArray(blankLoc));
            }

         obj.put("loc",new JSONArray());

            if(area!=null&&(area.length>0)) {
                JSONArray areaJson=new JSONArray();
                for (String anArea : area) {
                    areaJson.put(anArea);
                }
                obj.put("area", areaJson);
            }
            else
            {
                List<String> areablank=new ArrayList<>();
                obj.put("area", new JSONArray(areablank));  //IF area ind,location blank then we have to send Empty [] to server
            }
            if((ind!=null)&&(ind.length>0)) {
                JSONArray indJson=new JSONArray();
                for (String anInd : ind) {
                    indJson.put(anInd);
                }
                obj.put("ind", indJson);
            }
            else {
                List<String> indblank=new ArrayList<>();
                obj.put("ind",new JSONArray(indblank));
            }
            obj.put("exp", getStringValue(exp));
            obj.put("medium", "2");

            Type type = new TypeToken<CustomJobAlertModel>(){}.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,this, URL, type);
            downloader
                    .setUsePostMethod(obj);
            downloader.execute("JobAlert");


    }

	public void saveCustomAlert(UserStatusModel userProfileVO, SimpleSearchVO svo,
			 String email, ArrayList<String> funct_area,
			View parentView) {
		isAdd = true;
		isUpdate = false;
		isDelete = false;
		this.parentView = parentView;
		alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
				mActivity.getString(R.string.plz_wait));

        String URL;
        String mEmail;
        if(userProfileVO!=null)
        {
            mEmail=userProfileVO.email;
            URL= URLConfig.CUSTOM_JOB_ALERT_CREATE_USER.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity));
        }
        else {
            mEmail=email;
            URL= URLConfig.CUSTOM_ALERT_URL;
        }
        HashMap<String, Object> obj=new HashMap<>();

            if(TextUtils.isEmpty(svo.getLocation())) {
                obj.put("loc",new JSONArray());
            }
            else {
                obj.put("loc",new JSONArray().put(svo.getLocation()));
            }

            obj.put("name",svo.getCustomJobAlertName());
            obj.put("email",mEmail);
            obj.put("keywords",svo.getKeyword());


                obj.put("locid",new JSONArray(new ArrayList<>()));

        obj.put("sal",getStringValue(svo.getMinSalTag()));

            if((funct_area!=null)&&(funct_area.size()>0)) {
                JSONArray funcJSON=new JSONArray();
                for(int i=0;i<funct_area.size();i++)
                {

                    funcJSON.put(funct_area.get(i));
                }
                obj.put("area", funcJSON);
            }
            else {
                List<String> func_area_blank=new ArrayList<>();
                obj.put("area",new JSONArray(func_area_blank));
            }
            obj.put("ind",getStringValue(svo.getIndusTypeTag()));
            obj.put("exp", getStringValue(svo.getMinExpTag()));
            obj.put("medium", "2");


        Type type = new TypeToken<CustomJobAlertModel>(){}.getType();
        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,this, URL, type);
        downloader.setUsePostMethod(obj);
        downloader.execute("JobAlert");


    }

	public void deleteCustomAlert(String alertId) {

		alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
				mActivity.getString(R.string.plz_wait));

		isUpdate = false;
		isDelete = true;
		isAdd = false;

        String URL= URLConfig.DELETE_JOB_ALERT.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity)).replace(URLConfig.ALERT_ID,alertId);

        Type type = new TypeToken<CustomJobAlertModel>(){}.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity,this, URL, type);
        req.setUseDeleteMethod();
        req.execute("JobAlert");


	}

    @Override
    public void OnDataResponse(final Object object, String tag) {

        try {


            DialogUtils.dialogDismiss(alertDialog);

                        if (isUpdate) {
                            Toast.makeText(mActivity, "Custom alert updated",
                                    Toast.LENGTH_SHORT).show();
                            mActivity.popone();
                        }
                        else if (isAdd) {

                            if(ShineSharedPreferences.getUserInfo(mActivity)!=null) {
                                if(add_alert_option==CustomAlert.ADD_ALERT_THROUGH_SEARCH) {
                                    ShineCommons.trackShineEvents("SaveJobAlert", "JSRP-LoggedIn", mActivity);
                                }
                                else if(add_alert_option==CustomAlert.ADD_ALERT_THROUGH_FORM)
                                {
                                    ShineCommons.trackShineEvents("SaveJobAlert", "CJA-LoggedIn", mActivity);
                                }
                            }
                            else {
                                if(add_alert_option==CustomAlert.ADD_ALERT_THROUGH_SEARCH) {
                                    ShineCommons.trackShineEvents("SaveJobAlert", "JSRP-NonLoggedIn", mActivity);
                                }
                                else if(add_alert_option==CustomAlert.ADD_ALERT_THROUGH_FORM)
                                {
                                    ShineCommons.trackShineEvents("SaveJobAlert", "CJA-NonLoggedIn", mActivity);
                                }
                            }


                            if (add_alert_option == ADD_ALERT_THROUGH_SEARCH) {

                                TextView save_job_alert = (TextView) parentView.findViewById(R.id.save_job_alert);
                                TextView saved_job_alert = (TextView) parentView.findViewById(R.id.saved_job_alert);
                                save_job_alert.setVisibility(View.GONE);
                                saved_job_alert.setVisibility(View.VISIBLE);

                            }

                            else if (add_alert_option == ADD_ALERT_THROUGH_FORM) {

                                Toast.makeText(
                                        mActivity,
                                        "New alert has been successfully created",
                                        Toast.LENGTH_LONG).show();


                                if(searchVO!=null&&userProfileVO==null)
                                {
                                    showSearchResultFromCJA(searchVO);
                                }

                                mActivity.popone();


                            }
                        }

                        else if (isDelete) {
                            Toast.makeText(mActivity, "Custom alert deleted",
                                    Toast.LENGTH_SHORT).show();
                            BaseFragment fragment1 = new MyCustomJobAlertFragment();
                            mActivity.singleInstanceReplaceFragment(fragment1);
                            mActivity.onBackPressed();
                        }

                       if(fragment!=null)
                       fragment.updateList((CustomJobAlertModel) object);



        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public void OnDataResponseError(final String error, String tag) {

        try {
            DialogUtils.dialogDismiss(alertDialog);
            DialogUtils.showErrorToastLong(error);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public String getStringValue(int tag) {
		if (tag == 0 || tag == -1) {
			return "";
		}
		return String.valueOf(tag);
	}



    private void showSearchResultFromCJA(SimpleSearchVO sVO) {
        try {
            Bundle bundle = new Bundle();
            bundle.putBoolean(URLConfig.CUSTOM_JOB_NEW_ADD,true);
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            BaseFragment fragment = new SearchResultFrg();
            fragment.setArguments(bundle);
            mActivity.replaceFragmentWithBackStack(fragment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public void setAdd_alert_option(int add_alert_option) {
		this.add_alert_option = add_alert_option;
	}

}