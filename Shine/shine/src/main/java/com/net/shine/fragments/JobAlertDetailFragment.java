package com.net.shine.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.InboxMailDetailAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class JobAlertDetailFragment extends BaseFragment implements VolleyRequestCompleteListener,
        GetConnectionListener {

    public static final String JOB_ALERT_ID = "job_alert_id";
    private View view;
    private RecyclerView gridViewMatch, gridViewOther;
    private InboxMailDetailAdapter adapter, adapter2;
    private View errorLayout;
    private View LoadingCmp;
    private String subject;
    private TextView dateNumber,monthName;
    private LinearLayout imageContainer,topContainer;
    private ArrayList<InboxAlertMailDetailModel.InboxJobDetail> jobListMatch = new ArrayList<>();
    private ArrayList<InboxAlertMailDetailModel.InboxJobDetail> jobListOther = new ArrayList<>();
    private UserStatusModel userProfileVO;
    private Set<String> unique_companies = new HashSet<>();
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private String inbox_alert_id;
    private boolean isFromNoti = false;

    public static JobAlertDetailFragment newInstance(boolean isFromNoti) {

        Bundle args = new Bundle();
        args.putBoolean("isFromNoti", isFromNoti);
        JobAlertDetailFragment fragment = new JobAlertDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFromNoti = getArguments().getBoolean("isFromNoti", false);
        inbox_alert_id = getArguments().getString(JOB_ALERT_ID);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.job_alert_detail_frag, container, false);

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        gridViewMatch = (RecyclerView) view.findViewById(R.id.grid_view_match);
        errorLayout = view.findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        gridViewMatch.setFocusable(false);
        gridViewMatch.setLayoutManager(new LinearLayoutManager(mActivity));
        gridViewOther = (RecyclerView) view.findViewById(R.id.grid_view_other);
        gridViewOther.setFocusable(false);
        gridViewOther.setLayoutManager(new LinearLayoutManager(mActivity));
        dateNumber = (TextView) view.findViewById(R.id.date_number);
        monthName = (TextView) view.findViewById(R.id.month);
        imageContainer = (LinearLayout) view.findViewById(R.id.image_layout);
        topContainer = (LinearLayout) view.findViewById(R.id.top_container);
        LoadingCmp = view.findViewById(R.id.loading_cmp);

        unique_companies = new HashSet<>();
        try {
            if (!isFromNoti) {
                TextView sub = (TextView) view.findViewById(R.id.subject);
                if (subject != null)
                    sub.setText(subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        downloadMailDetail();


        return view;
    }

    private void downloadMailDetail() {
        try {

            DialogUtils.showLoading(mActivity, getString(R.string.loading),
                    LoadingCmp);

            Type type = new TypeToken<InboxAlertMailDetailModel>() {
            }.getType();

            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.JOB_ALERT_MAIL_DETAIL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id).replace(URLConfig.ID2, inbox_alert_id), type);


            req.execute("JobAlertMailDetials");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Job alert");
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();

        if(getUserVisibleHint())
        ManualScreenTracker.manualTracker("JobAlertMailDetail");

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void showresult() {

        try {

            if (isAdded()) {
                LoadingCmp.setVisibility(View.GONE);
                errorLayout.setVisibility(View.GONE);
                topContainer.setVisibility(View.VISIBLE);

                TextView sub = (TextView) view.findViewById(R.id.subject);
                sub.setText(subject);

                if (jobListMatch.isEmpty()&&jobListOther.isEmpty()) {
                    gridViewMatch.setVisibility(View.GONE);
                    gridViewOther.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,"Sorry, we have not found any jobs matching your preferences"
                            ,"","",DialogUtils.ERR_NO_EMAIL,null);

                } else if(!jobListMatch.isEmpty()&&jobListOther.isEmpty()){
                    gridViewMatch.setVisibility(View.VISIBLE);
                    gridViewOther.setVisibility(View.GONE);
                }
                else if(jobListMatch.isEmpty()&&!jobListOther.isEmpty()){
                    gridViewMatch.setVisibility(View.GONE);
                    gridViewOther.setVisibility(View.VISIBLE);
                }
                else {
                    gridViewMatch.setVisibility(View.VISIBLE);
                    gridViewOther.setVisibility(View.VISIBLE);
                }


                adapter = new InboxMailDetailAdapter(mActivity, jobListMatch);
                gridViewMatch.setAdapter(adapter);

                adapter2 = new InboxMailDetailAdapter(mActivity, jobListOther);
                gridViewOther.setAdapter(adapter2);

                invalidateWithFriendsData();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callGetFriend(String co_names) {
        GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
        friends.getFriendsInCompanyVolleyRequest(co_names);
    }

    private void invalidateWithFriendsData() {

        for (int m = 0; m < jobListMatch.size(); m++) {
            InboxAlertMailDetailModel.InboxJobDetail job = jobListMatch.get(m);
            if (job.getCompanyUuid() != null) {
                for (int n = 0; n < job.getCompanyUuid().size(); n++) {
                    if (friend_list.containsKey(job.getCompanyUuid().get(n))) {
                        if (friend_list.containsKey(job.getCompanyUuid().get(n))) {
                            job.setMatch_job(friend_list.get(job.getCompanyUuid().get(n)));
                        }
                    }
                }
            }

            for (int m1 = 0; m1 < jobListOther.size(); m1++) {
                InboxAlertMailDetailModel.InboxJobDetail job1 = jobListOther.get(m1);
                if (job1.getCompanyUuid() != null) {
                    for (int n = 0; n < job1.getCompanyUuid().size(); n++) {
                        if (friend_list.containsKey(job1.getCompanyUuid().get(n))) {
                            //	job.setMatch_job(friend_list.get(job.getCompanyUuid().get(n)));
                            job1.setMatch_job(friend_list.get(job1.getCompanyUuid().get(n)));
                        }
                    }
                }
            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
                gridViewMatch.invalidate();
            }
            if (adapter2 != null) {
                adapter2.notifyDataSetChanged();
                gridViewOther.invalidate();
            }

        }

    }

    @Override
    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
        if (!isAdded())
            return;
        friend_list = result;
        invalidateWithFriendsData();
    }

    @Override
    public void OnDataResponse(Object object, String tag) {
        try {

            errorLayout.setVisibility(View.GONE);
            InboxAlertMailDetailModel model = (InboxAlertMailDetailModel) object;
            if (!TextUtils.isEmpty(model.getSubject_line())) {
                subject = model.getSubject_line();
            }

            String date = ShineCommons.getFormattedDate(model.getTimestamp().substring(0, 10));
            String date_number = date.substring(0, 2);
            String month = "";
            if (date_number != null && date_number.trim().length() == 1) {
                month = date.substring(2, 6).toUpperCase();

                date_number = "0" + date_number;
            } else {

                month = date.substring(3, 6).toUpperCase();
            }
            Log.d("date_inbox", date_number + " " + month);

            dateNumber.setText(date_number);
            monthName.setText(month);
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(date_number);
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect("", color);
            imageContainer.setBackground(drawable);

            System.out.println("--inbox alrt mail model" + model);

            jobListMatch = (model.getMatched_job_details());

            for (InboxAlertMailDetailModel.InboxJobDetail detail : jobListMatch) {
                String[] locations = detail.getLocation();
                int len = locations.length;
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += locations[j];
                    } else {
                        loc += locations[j] + " / ";
                    }
                }
                detail.location_str = loc;
            }

            jobListOther = (model.getOther_job_details());

            for (InboxAlertMailDetailModel.InboxJobDetail detail : jobListOther) {
                String[] locations = detail.getLocation();
                int len = locations.length;
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += locations[j];
                    } else {
                        loc += locations[j] + " / ";
                    }
                }
                detail.location_str = loc;
            }

            showresult();    //this needs to be called before connections API as we need to
            //initialize the adapter before that API's callback

            String co_names = "";


            if (jobListMatch.size() > 0) {
                for (int i = 0; (i < jobListMatch.size()); i++) {

                    co_names = ShineCommons.getCompanyNames(jobListMatch.get(i)
                            .getCompanyUuid(), unique_companies);

                }
            }

            if (jobListOther.size() > 0) {
                for (int i = 0; (i < jobListOther.size()); i++) {

                    co_names = ShineCommons.getCompanyNames(jobListOther.get(i)
                            .getCompanyUuid(), unique_companies);
                }
            }

            if (co_names.length() != 0) {
                callGetFriend(co_names);
            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    LoadingCmp.setVisibility(View.GONE);
                    topContainer.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                    gridViewMatch.setVisibility(View.GONE);
                    gridViewOther.setVisibility(View.GONE);

                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.technical_error),
                            mActivity.getString(R.string.technical_error2),"",DialogUtils.ERR_TECHNICAL,null);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}