package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResumeDetails implements Serializable
{

//    {"id":"56dfd5ccac39211f3f838e79","candidate_id":"56dfd5a3ac39211f3d93b7ea",
// "creation_date":"2016-03-09T13:20:36.012916","resume_name":"RESUME_UjjawalGarg_09-Mar-16_13:20:36",
// "extension":"docx","is_default":1,"jobs":[{"start_month":6,"start_year":2015,"is_current":true,"company_name":
// "HT Media Ltd","job_title":"Android Developer"},{"start_month":1,"start_year":2015,"end_year":2015,"end_month":2,"
// is_current":null,"company_name":"HT Media Ltd","job_title":"Intern"},{"start_month":6,"start_year":2014,"end_year":2014,
// "end_month":7,"is_current":null,"company_name":"HT Media Ltd","job_title":"Intern"}],
// "qualifications":[{"year_of_passout":2015,"institute_name":"Kurukshetra University",
// "education_level":50,"education_specialization":402,"institute_id":119}],
// "skills":[{"name":"c++","experience":null},{"name":"java","experience":null},{"name":"python","experience":null}]}


    @Override
    public String toString() {
        return "Jobs: " + jobs.toString()
                + "Edus: " + qualifications.toString()
                + "Skills: " + skills.toString();
    }


    public String name;
    public int can_location;
    public int exp_in_yr;

    public  int salary_in_lakh;

    public int getSalary_in_lakh() {
        return salary_in_lakh;
    }

    public void setSalary_in_lakh(int salary_in_lakh) {
        this.salary_in_lakh = salary_in_lakh;
    }

    @SerializedName("id")
        String id = "";

        @SerializedName("candidate_id")
        String cId = "";

        @SerializedName("creation_date")
        String creationDate = "";

        @SerializedName("resume_name")
        String resumeName = "";

        @SerializedName("extension")
        String extension = "";

        @SerializedName("is_default")
        int isDefault;

       public ArrayList<jobs> jobs = new ArrayList<>();

       public ArrayList<Qualifications> qualifications = new ArrayList<>();

       public ArrayList<skills> skills = new ArrayList<>();



        public String getId() {
            return id;
        }

        public int getIsDefault() {
            return isDefault;
        }

        public String getResumeName() {
            return resumeName;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setIsDefault(int isDefault) {
            this.isDefault = isDefault;
        }

        public ArrayList<ResumeDetails.jobs> getJobs() {
            return jobs;
        }

        public void setJobs(ArrayList<ResumeDetails.jobs> jobs) {
            this.jobs = jobs;
        }

        public ArrayList<Qualifications> getQualifications() {
            return qualifications;
        }

       public void setQualifications(ArrayList<ResumeDetails.Qualifications> qual) {
        qualifications = qual;
    }

        public ArrayList<skills> getSkills() {
            return skills;
        }

        public void setSkills(ArrayList<skills> skills) {
            this.skills = skills;
        }


    public static class jobs implements Serializable
    {

        @Override
        public String toString() {
            return "Job:{" + company_name + ", " +job_title+" }";
        }

        int start_month;
        int start_year;
        int end_month;
        int end_year;
       public  int fuctional_area;

        public int getFuctional_area() {
            return fuctional_area;
        }

        public void setFuctional_area(int fuctional_area) {
            this.fuctional_area = fuctional_area;
        }

        public int getIndustry() {
            return industry;
        }

        public void setIndustry(int industry) {
            this.industry = industry;
        }

        public int industry;
        boolean is_current;
        String company_name = "";
        String job_title = "";

        public int getEnd_month() {
            return end_month;
        }

        public void setEnd_month(int end_month) {
            this.end_month = end_month;
        }

        public int getEnd_year() {
            return end_year;
        }

        public void setEnd_year(int end_year) {
            this.end_year = end_year;
        }

        public String getCompany_name() {
            if(company_name==null)
                return "";
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public boolean is_current() {
            return is_current;
        }

        public void setIs_current(boolean is_current) {
            this.is_current = is_current;
        }

        public int getStart_month() {
            return start_month;
        }

        public void setStart_month(int start_month) {
            this.start_month = start_month;
        }

        public String getJob_title() {
            if(job_title==null)
                return "";
            return job_title;
        }

        public void setJob_title(String job_title) {
            this.job_title = job_title;
        }

        public int getStart_year() {
            return start_year;
        }

        public void setStart_year(int start_year) {
            this.start_year = start_year;
        }
    }

    public static class Qualifications implements Serializable{

       int year_of_passout;
       String institute_name = "";
       int education_level;
       int study_field;
        int education_specialization;
        int institute_id;
        int course_type;

        @SerializedName("education_specialization_custom")

        public String other_specialization="";

        public String getOther_specialization() {
            return other_specialization;
        }

        public void setOther_specialization(String other_specialization){
            this.other_specialization = other_specialization;
        }

        public int getCourse_type() {
            return course_type;
        }

        public void setCourse_type(int course_type) {
            this.course_type = course_type;
        }

        public int getInstitute_id() {
            return institute_id;
        }

        public void setInstitute_id(int institute_id) {
            this.institute_id = institute_id;
        }

        public int getEducation_specialization() {
            return education_specialization;
        }

        public void setEducation_specialization(int education_specialization) {
            this.education_specialization = education_specialization;
        }

        @Override
        public String toString() {
            return "EDU:{" + institute_name + ", " +year_of_passout+" }";
        }

        public int getEducation_level() {
            return education_level;
        }

        public void setEducation_level(int education_level) {
            this.education_level = education_level;
        }

        public String getInstitute_name() {
            if(institute_name==null)
                return "";
            return institute_name;
        }

        public void setInstitute_name(String institute_name) {
            this.institute_name = institute_name;
        }

        public int getStudy_field() {
            return study_field;
        }

        public void setStudy_field(int study_field) {
            this.study_field = study_field;
        }

        public int getYear_of_passout() {
            return year_of_passout;
        }

        public void setYear_of_passout(int year_of_passout) {
            this.year_of_passout = year_of_passout;
        }
    }

    public static  class skills implements Serializable{

        String name = "";
        int experience;

        @Override
        public String toString() {
            return "skill:{" + name + ", " +experience+" }";
        }

        public int getExperience() {
            return experience;
        }

        public void setExperience(int experience) {
            this.experience = experience;
        }

        public String getName() {
            if(name==null)
                return "";
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
