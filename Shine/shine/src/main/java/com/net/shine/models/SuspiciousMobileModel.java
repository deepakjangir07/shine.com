package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by manishpoddar on 01/03/16.
 */
public class SuspiciousMobileModel implements Serializable {

    public boolean blocked = false;
    public String mobile = "";
    public String otp = "";
    public String verification_date = "";
}
