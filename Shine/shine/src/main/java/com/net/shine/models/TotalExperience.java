package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class TotalExperience implements Serializable {

    private int salaryIndex;

    @SerializedName("experience_in_years")
    private int totalExperienceIndex;

    @SerializedName("team_size_managed")
    private int handledTeamSizeIndex;


    @SerializedName("experience_in_months")
    private int monthExperienceIndex;

    @SerializedName("salary_in_thousand")
    private int salaryTIndex;

    @SerializedName("salary_in_lakh")
    private int salaryLIndex;

    @SerializedName("notice_period")
    private int noticePeriodIndex;

    @SerializedName("resume_title")
    private String resumeTitle="";

    private boolean isFreshser;



    public int getSalaryLIndex() {
        return salaryLIndex;
    }
    public void setSalaryLIndex(int salaryLIndex) {
        this.salaryLIndex = salaryLIndex;
    }
    public int getSalaryTIndex() {
        return salaryTIndex;
    }
    public void setSalaryTIndex(int salaryTIndex) {
        this.salaryTIndex = salaryTIndex;
    }
    public int getNoticePeriodIndex() {
        return noticePeriodIndex;
    }
    public void setNoticePeriodIndex(int noticePeriodIndex) { this.noticePeriodIndex = noticePeriodIndex; }
    public String getResumeTitle() {
        return resumeTitle;
    }
    public void setResumeTitle(String resumeTitle) {
        this.resumeTitle = resumeTitle;
    }
    public boolean isFreshser() {
        return isFreshser;
    }
    public void setFreshser(boolean isFreshser) {
        this.isFreshser = isFreshser;
    }
    public int getSalaryIndex() {
        return salaryIndex;
    }
    public void setSalaryIndex(int salaryIndex) {
        this.salaryIndex = salaryIndex;
    }
    public int getTotalExperienceIndex() {
        return totalExperienceIndex;
    }
    public void setTotalExperienceIndex(int totalExperienceIndex) {	this.totalExperienceIndex = totalExperienceIndex; }
    public int getHandledTeamSizeIndex() {
        return handledTeamSizeIndex;
    }
    public void setHandledTeamSizeIndex(int handledTeamSizeIndex) {	this.handledTeamSizeIndex = handledTeamSizeIndex;}
    public int getMonthExperienceIndex() {
        return monthExperienceIndex;
    }
    public void setMonthExperienceIndex(int monthExperienceIndex) {	this.monthExperienceIndex = monthExperienceIndex;}
}
