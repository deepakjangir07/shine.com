package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.utils.auth.ShineAuthUtils;

/**
 * Created by manishpoddar on 14/07/17.
 */

public class CustomLogoutMessageFrag extends BaseFragment {

    private ImageView imageView1;
    private TextView textView1,textView2,textView3,textView4;

    public static CustomLogoutMessageFrag newInstance() {
        Bundle bundle = new Bundle();
        CustomLogoutMessageFrag fragment = new CustomLogoutMessageFrag();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_logout_frg, container, false);

        mActivity.setTitle("Messages");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.showSelectedNavButton(mActivity.MAIL_OPTION);

        imageView1 = (ImageView) view.findViewById(R.id.imageview1);
        textView1 = (TextView) view.findViewById(R.id.textview1);
        textView2 = (TextView) view.findViewById(R.id.textview2);
        textView3 = (TextView) view.findViewById(R.id.textview3);
        textView4 = (TextView) view.findViewById(R.id.textview4);

        textView1.setText("Connect with top recruiters");
        textView2.setText("View latest recruiter messages and job openings");
        imageView1.setImageResource(R.drawable.error_no_mail_logout);
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_FOR_MESSAGES);
                BaseFragment fragment = new LoginFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);

            }
        });

        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle2 = new Bundle();
                bundle2.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_FOR_MESSAGES);
                BaseFragment fragment1 = new RegistrationFrg();
                fragment1.setArguments(bundle2);
                mActivity.singleInstanceReplaceFragment(fragment1);
            }
        });
        return view;
    }
}
