package com.net.shine.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.MyProfileViewAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.ProfileViewMainTabFrg;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class MyProfileViewFrg extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener, VolleyRequestCompleteListener {
    ListView gridView;

    MyProfileViewAdapter mAdapter;
    View LoadingCmp;
    View view;

    private int total_count;

    private boolean loadMore = false;
    private ArrayList<WhoViewMyProfileModel.Result> profileList_viewed = new ArrayList<>();
    private UserStatusModel userProfileVO;
    private View errorlayout;
    private TextView WhoViewedCount;
    private SwipeRefreshLayout swipeContainer;

    private Toast toast;



    public static MyProfileViewFrg newInstance() {
        Bundle bundle = new Bundle();
        MyProfileViewFrg fragment = new MyProfileViewFrg();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         view = inflater.inflate(R.layout.viewed_my_profile_view,
                container, false);

        mActivity.setTitle("Recruiter actions");
        mActivity.showSelectedNavButton(BaseActivity.MORE_OPTION);
        mActivity.showBackButton();
        mActivity.showNavBar();
        try {
            swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            swipeContainer.setOnRefreshListener(this);
            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
            gridView = (ListView) view.findViewById(R.id.grid_view_profile);
            gridView.setOnScrollListener(this);
            errorlayout = view.findViewById(R.id.error_layout);
            errorlayout.setVisibility(View.GONE);

            mAdapter=new MyProfileViewAdapter(mActivity,profileList_viewed);
            gridView.setAdapter(mAdapter);

            WhoViewedCount = (TextView) view.findViewById(R.id.Viewed_count);
            LoadingCmp = view.findViewById(R.id.loading_cmp);


            getViewProfileData(1,LoadingCmp,false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                try {

                    BaseFragment fragment = new ProfileViewMainTabFrg();
                    WhoViewMyProfileModel.Result data=(WhoViewMyProfileModel.Result)adapterView.getItemAtPosition(position);
                    Bundle b=new Bundle();
                    b.putSerializable("list_item",data);
                    fragment.setArguments(b);
                    mActivity.singleInstanceReplaceFragment(fragment);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        if(toast!=null)
            toast.cancel();
    }



    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Recruiter actions");
        mActivity.showSelectedNavButton(BaseActivity.MORE_OPTION);
        mActivity.showBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();

        ManualScreenTracker.manualTracker("ProfileViews");

    }




    private void loadMoreData() {

        try {
            int noofhits = (profileList_viewed.size() / URLConfig.PER_PAGE) + 1;
            int totalPage = total_count / URLConfig.PER_PAGE;
            int modtotalnoPages = totalPage % URLConfig.PER_PAGE;

            if (modtotalnoPages != 0) {
                totalPage = totalPage + 1;
            }

            if (noofhits <= totalPage && total_count > profileList_viewed.size()) {
                getViewProfileData(noofhits,view.findViewById(R.id.loading_grid), true, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    private void getViewProfileData(int pageNumber,View loadingView,boolean flag, boolean isRefresh) {

        try {
            LoadingCmp = loadingView;
            loadMore = flag;
            if (!isRefresh) {
                DialogUtils.showLoading(mActivity, getString(R.string.loading),
                        LoadingCmp);
            }

              String mURL=URLConfig.WHO_VIEW_PROFILE_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id)+"?page="+pageNumber;
                Type type = new TypeToken<WhoViewMyProfileModel>() {
                }.getType();
                VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity,this,mURL,type);
                req.execute("ProfileViewActivities");

            Log.d("prof url",URLConfig.WHO_VIEW_PROFILE_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id)+"");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showresult() {

        try {

            LoadingCmp.setVisibility(View.GONE);
            if (isAdded()) {




                if (profileList_viewed.size() > 0) {

                    gridView.setVisibility(View.VISIBLE);
                    errorlayout.setVisibility(View.GONE);
                    WhoViewedCount.setVisibility(View.VISIBLE);


                        mAdapter.rebuildListObject(profileList_viewed);
                        mAdapter.notifyDataSetChanged();
                     if (profileList_viewed.size() > 1) {
                            WhoViewedCount.setText(Html.fromHtml("<b>" + total_count + "</b>" + " "
                                    + getString(R.string.profile_view_message)));
                        } else {
                            WhoViewedCount.setText(Html.fromHtml("<b>" + total_count + "</b>" + " recruiter viewed/contacted your profile"));
                        }

                    MoengageTracking.setRecruiterContactedCount(mActivity,total_count);

                } else {

                        errorlayout.setVisibility(View.VISIBLE);
                        gridView.setVisibility(View.GONE);
                    BaseFragment fragment = new MyProfileFrg();
                        DialogUtils.showErrorActionableMessage(mActivity,errorlayout,
                                mActivity.getString(R.string.no_profile_view_list_msg),
                                mActivity.getString(R.string.no_profile_view_list_msg2),
                                mActivity.getString(R.string.update_profile),
                                DialogUtils.ERR_NO_VIEWS,fragment);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void OnDataResponse(Object object, String tag) {
        try {


            swipeContainer.setRefreshing(false);
            WhoViewMyProfileModel model=(WhoViewMyProfileModel)object;
            total_count=model.count;
            if (profileList_viewed == null || !loadMore)
                profileList_viewed = new ArrayList<>();
            profileList_viewed.addAll(model.results);

            loadMore=false;

           showresult();

            String url = URLConfig.NOTIFICATION_TIMESTAMP_UPDATE_API.replace(URLConfig.CANDIDATE_ID,
                    ShineSharedPreferences.getCandidateId(mActivity))+"who_viewed_section_visit";
            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    Log.d("timestamp","updated");
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("timestamp","error"+error);

                }
            }, url, type);

            HashMap<String,String> params = new HashMap<>();
            request.setUsePutMethod(params);
            request.execute("notification");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            swipeContainer.setRefreshing(false);


            gridView.setVisibility(View.GONE);
            WhoViewedCount.setVisibility(View.GONE);
            errorlayout.setVisibility(View.VISIBLE);

            LoadingCmp.setVisibility(View.GONE);
            DialogUtils.showErrorActionableMessage(mActivity,errorlayout,
                    mActivity.getString(R.string.technical_error),
                    mActivity.getString(R.string.technical_error2),
                    "",
                    DialogUtils.ERR_TECHNICAL,null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRefresh() {
        errorlayout.setVisibility(View.GONE);

        getViewProfileData(1,LoadingCmp,false,true);


    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {

        try {



            if (totalItemCount != 0) {
                int lastScreen = firstVisibleItem + visibleItemCount;
                if ((lastScreen == totalItemCount) && !(loadMore)) {
                    loadMoreData();
                } else if (LoadingCmp.getTag() != null) {
                    if (LoadingCmp.getTag().equals("error")) {
                        view.findViewById(R.id.loading_grid).setVisibility(
                                View.VISIBLE);
                        loadMore = false;
                    }
                }
            } else {

                if (view.findViewById(R.id.loading_grid) != null) {
                    view.findViewById(R.id.loading_grid).setVisibility(
                            View.GONE);
                }

            }

            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
}