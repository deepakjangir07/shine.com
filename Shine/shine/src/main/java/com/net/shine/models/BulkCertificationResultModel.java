package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ggnf2853 on 23/05/16.
 */
public class BulkCertificationResultModel {


    public String candidate_id = "";

    @SerializedName("certifications_data")
    public ArrayList<CertificationResult> certi_data = new ArrayList<>();

//    public static class SkillModel
//    {
//        public String id = "";
//        public String value = "";
//        public int years_of_experience;
//    }
}


