package com.net.shine.fragments.components;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.gson.Gson;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.PersonalDetail;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.VerifyUserEmailMobile;
import com.net.shine.utils.db.ShineSharedPreferences;

public class PersonalDetailComp implements
		OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

	private Activity mActivity;
	private View view;
	private View loadingView;
	private PersonalDetail pModel;
	private ImageButton mob_verify;

    ImageButton email_verify;

    Fragment parentFrg;

	public PersonalDetailComp(Activity mActivity, View view, Fragment pFrg) {
        parentFrg = pFrg;
		this.mActivity = mActivity;
		this.view = view;
	}



	public void VerifyOTP(String otp) {
		new VerifyUserEmailMobile(mActivity, mob_verify).verifyOTP(otp);
	}

	public void showDetails(PersonalDetail pModel) {
		try {

			this.pModel = pModel;
            loadingView=view.findViewById(R.id.loading_cmp);
			loadingView.setVisibility(View.GONE);
			TextView userName = (TextView) view
					.findViewById(R.id.user_name_title);
			userName.setText("" + pModel.getFirstName() + " " + pModel.getLastName());

			ImageView editDetails = (ImageView) view
					.findViewById(R.id.personal_edit);

			editDetails.setOnClickListener(this);

			final ImageView profile = (ImageView) view.findViewById(R.id.imageView1);

			String profileUrl = ShineSharedPreferences.getLinkedinPictureUrl(mActivity);
			Log.d("DEBUG::IMG", "GET: " + profileUrl);
			if (!TextUtils.isEmpty(profileUrl)) {
				ImageRequest request = new ImageRequest(profileUrl,
						new Response.Listener<Bitmap>() {
							@Override
							public void onResponse(Bitmap bitmap) {
								profile.setImageBitmap(bitmap);
							}
						}, 0, 0, ImageView.ScaleType.CENTER, null,
						new Response.ErrorListener() {
							public void onErrorResponse(VolleyError error) {
								profile.setImageResource(R.drawable.user_profile);
							}
						});
				MyApplication.getInstance().addToRequestQueue(request, "image");
			} else {

				ColorGenerator generator = ColorGenerator.MATERIAL;
				int color = generator.getRandomColor();
				TextDrawable drawable = TextDrawable.builder()
						.buildRect(pModel.getFirstName().substring(0,1).toUpperCase(),color);
			      profile.setImageDrawable(drawable);
			}


			TextView email = (TextView) view.findViewById(R.id.email_id);
			email.setText("" + pModel.getEmail());

			TextView mobile = (TextView) view.findViewById(R.id.mobile_num);
			mobile.setText( pModel.getCellPhone());

			 email_verify = (ImageButton) view
					.findViewById(R.id.email_id_verify_status);
			email_verify.setOnClickListener(this);

			if (pModel.getEmailVerified()==1) {
				email_verify.setVisibility(View.GONE);
			}

			mob_verify = (ImageButton) view
					.findViewById(R.id.mobile_num_verify_status);
			mob_verify.setOnClickListener(this);

			if (pModel.getCellPhoneVerified()==1 ) {
				mob_verify.setVisibility(View.GONE);
			}



			String locationName = URLConfig.CITY_REVERSE_MAP.get(""
					+ pModel.getLocation());


			TextView location = (TextView) view
					.findViewById(R.id.current_location);
			location.setText(locationName);

			TextView gender = (TextView) view.findViewById(R.id.gender);
			if (URLConfig.GENDER_REVERSE_MAP.get("" + pModel.getGender()) == null) {
				gender.setText("Not Mentioned");

			} else
				gender.setText(""
						+ URLConfig.GENDER_REVERSE_MAP.get(""
								+ pModel.getGender()));
			TextView dob = (TextView) view.findViewById(R.id.dob);
			if (pModel.getDateOfBirth()==null) {

				dob.setText("Not Mentioned");

			} else {


				dob.setText(ShineCommons.getFormattedDate(pModel.getDateOfBirth().substring(0, 10)));

			}
			ShineSharedPreferences.updateUserEmailAndMobile(mActivity, ""
					+ pModel.getEmail(), "" + pModel.getCellPhone(),
					pModel.getFirstName() + " " + pModel.getLastName());



		} catch (Exception e) {
			e.printStackTrace();
		}

	}



    public static final int REQUEST_SMS_PERMISSIONS = 12;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        new VerifyUserEmailMobile(mActivity).verifyMobile(this);

    }



	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.personal_edit:
                Gson gson = new Gson();
                String json = gson.toJson(pModel);
                Bundle bundle = new Bundle();
				bundle.putString(URLConfig.KEY_MODEL_NAME, json);
				BaseFragment frg = new PersonalDetailsEditFragment(mob_verify);
				frg.setArguments(bundle);
				((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);

				break;
			case R.id.mobile_num_verify_status:

				if(ContextCompat.checkSelfPermission(mActivity, Manifest.permission.RECEIVE_SMS)== PackageManager.PERMISSION_GRANTED)
				{
					new VerifyUserEmailMobile(mActivity).verifyMobile(this);
				}
				else
				{
				    parentFrg.requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_SMS_PERMISSIONS);
				}
				break;

			case R.id.email_id_verify_status:

				new VerifyUserEmailMobile(mActivity).verifyEmail(email_verify);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}