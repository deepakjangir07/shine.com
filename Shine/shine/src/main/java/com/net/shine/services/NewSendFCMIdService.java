package com.net.shine.services;

import android.app.AppOpsManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.reflect.TypeToken;
import com.moengage.push.PushManager;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.GetClientAccessToken;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;

public class NewSendFCMIdService extends IntentService {


	private static final String TAG = "RegIntentService";
	private int status = 0;

	public NewSendFCMIdService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		try {

				String token = FirebaseInstanceId.getInstance().getToken();
				Log.i(TAG, "FCM Registration Token: " + token);

				if(token!=null) {
					isNotificationEnabled();
					sendRegistrationToServer(token);
				}

		} catch (Exception e) {
			Log.d(TAG, "Failed to complete token refresh", e);
		}

	}
	//--------------------------------------------------------------------------------------------------------------------------
	//https://code.google.com/p/android/issues/detail?id=38482----reference
	//
	private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
	private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

	public boolean isNotificationEnabled() {
		try {

			if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {
				AppOpsManager mAppOps = (AppOpsManager) getApplicationContext().getSystemService(Context.APP_OPS_SERVICE);

				ApplicationInfo appInfo = getApplicationContext().getApplicationInfo();

				String pkg = getApplicationContext().getApplicationContext().getPackageName();

				int uid = appInfo.uid;

				Class appOpsClass = null; /* Context.APP_OPS_MANAGER */

				try {

					appOpsClass = Class.forName(AppOpsManager.class.getName());

					Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);

					Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
					int value = (int) opPostNotificationValue.get(Integer.class);
					System.out.println("Debug::noti" + (int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg));
					status = (int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg);
					return ((int) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);


				} catch (Exception e){
					status = 0;
					e.printStackTrace();
				}
			} else {
				status = 0;
			}
		}
		catch (Exception e){
			e.printStackTrace();
			status = 0;
		}
		return false;
	}
	//---------------------------------------------------------------------------------------------------------------------------

	/**
	 * Persist registration to third-party servers.
	 *
	 * Modify this method to associate the user's FCM registration token with any server-side account
	 * maintained by your application.
	 *
	 * @param token The new token.
	 */
	private void sendRegistrationToServer(final String token) {


		//Send to Moe
//        MoEHelper mHelper = new MoEHelper(this);







		ShineSharedPreferences.saveFCMRegistrationId(getApplicationContext(),token);

		//TODO-FCM: The recommended way is to send regId only if there is a change
		//			So, we need to save a String value(last sent FCM)
		// 			for our FCMID tracker(rocq, shine)

		//Send to shine
		Type type = new TypeToken<EmptyModel>(){}.getType();
		final VolleyNetworkRequest downloader = new VolleyNetworkRequest(getApplicationContext(), new VolleyRequestCompleteListener() {
			@Override
			public void OnDataResponse(Object object, String tag) {
				Log.d("DEBUG::FCM", "FCM id sent to Shine successfully");
			}

			@Override
			public void OnDataResponseError(String error, String tag) {
				Log.d("DEBUG::FCM", "Error sending FCM id sent to Shine : " + error);
			}
		},
				URLConfig.FCM_POST_API, type);
		final HashMap paramsMap = new HashMap<>();
		paramsMap.put(URLConfig.FCM_REG_ID, ShineSharedPreferences.getFCMRegistrationId(getApplicationContext()));
		paramsMap.put(URLConfig.VERSION_CODE, "" + ShineCommons.getAppVersionCode(getApplicationContext()));
		paramsMap.put(URLConfig.DEVICE_NOTIFICATION_STATUS,status);
		paramsMap.put("device", "1");



		new AsyncTask<Void, Void, Void>()
		{

			String gaid = "";
			@Override
			protected Void doInBackground(Void... voids) {
				try {
					AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
					gaid = info.getId()+"";
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				Log.d("DEBUG::FCM", "Sending GAID: " + gaid + ", FCMID: " + token);
				paramsMap.put("gaid", gaid);
				ShineSharedPreferences.saveGAID(getApplicationContext(),gaid);

				System.out.println(paramsMap+"post FCM params");
				downloader.setUsePostMethod(paramsMap);
				if(TextUtils.isEmpty(ShineSharedPreferences.getClientAccessToken(getApplicationContext()))
						|| ShineSharedPreferences.getClientAccessToken(getApplicationContext()).equals("shine")){
					Log.d("getting c a t for FCM", "Debug" );
					new GetClientAccessToken(getApplicationContext(), downloader, "sendFCMIdService");
				}else {
					downloader.execute("sendFCMIdService");
				}

			}
		}.execute();


		PushManager.getInstance().refreshToken(this.getApplicationContext(), token);


	}

}