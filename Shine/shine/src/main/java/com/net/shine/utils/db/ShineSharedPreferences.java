package com.net.shine.utils.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.models.AdModel;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.RegDumpModel;
import com.net.shine.models.ResumeDetails;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.enrcypt.XOREncryption;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ShineSharedPreferences {


    public static final String CLIENT_ACCESS_TOKEN = "client_access_token";
    public static final String CANDIDATE_ID = "candidate_id";
    public static final String UPDATE_FLOW_TYPE = "update_type_flow_type";
    public static final String UPDATE_FLOW_FLAG = "update_type_flow_flag";
    public static final String SIMILAR_JOBS_FLAG = "similar_jobs_flag";
    public static final String USER_ACCESS_TOKEN = "user_access_token";
    public static final String HIGHEST_EDUCATION = "highest_education";
    public static final String CURRENT_COMPANY = "current_company";
    public static final String PREF_NAME = "shine_user_info";
    public static final String PHONEBOOK_SYNC_STATUS = "phonebook_sync_status";
    public static final String APP_PREF = "shine_app_info";
    public static final String NAME = "name";
    public static final String RESUME_SKIP = "resume_skip";
    public static final String PROFILE_SKIP = "profile_skip";
    public static final String UPDATE_SKIP = "update_skip";

    public static final String SALARY_IN_LAKH="salary_in_lakh";
    public static final String UPDATE_FLOW_SKIP = "update_flow_skip";

    public static final String APP_VERSION_LATEST = "app_version_latest";


    public static final String EMAIL = "email";
    public static final String ALL_SYNCED_BY_USER = "user_all_sync";


    public static final String EXPERIENCE_IN_YEAR = "exp_in_year";

    public static final String EXP_FLAG = "exp_flag";
    public static final String EDU_FLAG = "edu_flag";
    public static final String SKILL_FLAG = "skill_flag";
    public static final String CERTI_FLAG="certi_flag";
    public static final String PROFILE_FLAG = "profile_flag";
    public static final String DASH_FLAG = "dash_flag";
    public static final String VERIFY_EMAIL_FLAG = "verify_email_flag";
    public static final String VERIFY_MOBILE_FLAG = "verify_mobile_flag";

    public static final String USER_PH_SYNC_TIME = "ph_sync_time";
    public static final String MOBILE = "mobile";
    public static final String JOB_TITLE = "job_title";
    public static final String RATE_APP_PREF = "rate_app_info";
    public static final String IS_MID_OUT = "is_mid_out";

    public static final String PASSOUT_YEAR = "passout_year";
    public static final String INSTITUTE_NAME = "institute_name";
    public static final String EDUCATION_LEVEL = "education_level";
    public static final String COURSE_TYPE = "course_type";
    public static final String EDUCATION_SPECIALIZATION = "education_specialization";
    public static final String EDUCATION_OTHER = "education_other";

    public static final String UPDATE_FLOW_JOB_TITLE = "update_flow_job_title";
    public static final String UPDATE_FLOW_COMPANY_NAME = "update_flow_company_name";
    public static final String UPDATE_FLOW_INDUSTRY_ID = "update_flow_industry_id";
    public static final String UPDATE_FLOW_IS_CURRENT = "update_flow_is_current";
    public static final String UPDATE_FLOW_START_YEAR = "update_flow_start_year";
    public static final String UPDATE_FLOW_START_MONTH = "update_flow_start_month";
    public static final String UPDATE_FLOW_END_YEAR = "update_flow_end_year";
    public static final String UPDATE_FLOW_END_MONTH = "update_flow_end_month";
    public static final String UPDATE_FLOW_SUB_FIELD = "update_flow_sub_field";

    public static final String UPDATE_FLOW_PASSOUT_YEAR = "update_flow_passout_year";
    public static final String UPDATE_FLOW_INSTITUTE_NAME = "update_flow_institute_name";
    public static final String UPDATE_FLOW_EDUCATION_LEVEL = "update_flow_education_level";
    public static final String UPDATE_FLOW_EDUCATION_SPECIALIZATION = "update_flow_education_specialization";
    public static final String UPDATE_FLOW_EDUCATION_COURSE_TYPE = "update_flow_education_course_type";
    public static final String UPDATE_FLOW_SKILL = "update_flow_skills";
    public static final String IS_CURRENT = "is_current";
    public static final String START_MONTH = "start_month";
    public static final String START_YEAR = "start_year";
    public static final String END_MONTH = "end_month";
    public static final String END_YEAR = "end_year";
    public static final String COMPANY_NAME = "company_name";
    public static final String JOB_TITLE_PARSER = "job_title";
    public static final String FUCTIONAL_AREA="fucational_area";
    public static final String INDUSTRY="industry";
    public static final String MATCHED_JOBS_COUNT = "m_j_c";
    public static final String RATE_APP_COUNT = "r_a_c";
    public static final String RATE_APP_DATE = "r_a_date";
    public static final String UNREAD_JOB_MAIL_COUNT = "unread_job_mail_count";
    public static final String UNREAD_RECRUITER_MAIL_COUNT = "unread_recruiter_mail_count";
    public static final String DONT_ASK_AGAIN = "don'task_again";
    public static final String PROFILE_VIEWDED_COUNT = "p_v_c";
    public static final String PROFILE_ALERTS_COUNT = "p_a_c";
    public static final String REFFERAL_FLAG = "seventy";
    public static final String GCM_REGISTRATION_ID = "gcm_reg_id";
    public static final String RESUME_MIDOUT_FLAG = "resumemidout";
    public static final String PH_BOOK_DELIVERY = "phonebook_delivery";
    public static final String PH_BOOK_PROGRESS = "phonebook_progress";
    public static final String PH_SYNC_TIME = "next_phonebook_sync_on";
    public static final String CONNECT_PREF = "connect_pref";
    public static final String LINKEDIN_CONNECT = "linkedin_connect";
    public static final String GOOGLE_CONNECT = "google_connect";
    public static final String FACEBOOK_CONNECT = "facebook_connect";
    public static final String LAST_MATCH_JOB_TIME = "matchjob_time";
    public static final String TUTORIAL_PAGE_INSTALLED = "tut_detail_installed";
    public static final String GMAIL_SYNC = "gmail_sync";
    public static final String PHONEBOOK_SYNC = "phonebook_sync";
    public static final String LINKEDIN_SYNC = "linkedin_sync";
    public static final String UPDATE_PROFILE = "update_profile";
    public static final String SERVER_LOOKUP_VERSION = "server_lookup_version";
    public static final String FB_PROFILE = "fb_profile";
    public static final String LINKEDIN_RESUME_STATUS = "LinkedinResumeStatus";
    public static final String UPDATE_FLOW_STATUS = "UpdateFlowStatus";
    private static final String SKILL_JSON = "skill_json";

    private static final String WIDGET_JSON_LIST = "widget_json_list";


    public static void setAllSyncedByUser(Context mContext, boolean var) {
        SharedPreferences.Editor searcPref = mContext.getSharedPreferences(
                PREF_NAME, 0).edit();
        searcPref.putBoolean(ALL_SYNCED_BY_USER, var);
        searcPref.apply();
    }

    public static boolean getAllSyncedByUser(Context mContext) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        return searcPref.getBoolean(ALL_SYNCED_BY_USER, false);
    }


    public static void saveNotificationData(Context mContext,String key,String response)
    {
        SharedPreferences.Editor preferences=mContext.getSharedPreferences(PREF_NAME,0).edit();
        if(TextUtils.isEmpty(key))
        {
            preferences.putString("notification_data", response);
        }
        else {
            preferences.putString(key, response);
        }
        preferences.apply();
    }

    public static String getNotificationData(Context mContext,String key)
    {
        SharedPreferences preferences=mContext.getSharedPreferences(PREF_NAME,0);

        if(TextUtils.isEmpty(key))
        {
            return preferences.getString("notification_data","");
        }
        return preferences.getString(key,"");
    }

    public static void saveNotificationCount(Context mContext,String key,String response)
    {
        SharedPreferences.Editor preferences=mContext.getSharedPreferences(PREF_NAME,0).edit();
        if(TextUtils.isEmpty(key))
        {
            preferences.putString("notification_count", response);
        }
        else {
            preferences.putString(key, response);
        }
        preferences.apply();
    }

    public static String getNotificationCount(Context mContext,String key)
    {
        SharedPreferences preferences=mContext.getSharedPreferences(PREF_NAME,0);

        if(TextUtils.isEmpty(key))
        {
            return preferences.getString("notification_count","");
        }
        return preferences.getString(key,"");
    }





    public static void saveCareerPlusAds(Context mContext, String key, String response)
    {
        SharedPreferences.Editor preferences=mContext.getSharedPreferences(PREF_NAME,0).edit();
        if(TextUtils.isEmpty(key))
        {
            preferences.putString("career_plus_ad", response);
        }
        else {
            preferences.putString(key+"_Ads", response);
        }
        preferences.apply();
    }



    public static String getCareerPlusAds(Context mContext, String key)
    {
        SharedPreferences preferences=mContext.getSharedPreferences(PREF_NAME,0);

        if(TextUtils.isEmpty(key))
        {
            return preferences.getString("career_plus_ad","");
        }
        return preferences.getString(key+"_Ads","");
    }



    public static boolean isCareerPlusAds(Context mContext)
    {
        SharedPreferences preferences=mContext.getSharedPreferences(PREF_NAME,0);

        String ads="";
        if(TextUtils.isEmpty(getCandidateId(mContext))) {
            ads = preferences.getString("career_plus_ad", "");
        }else {
            ads = preferences.getString(getCandidateId(mContext)+"_Ads", "");
        }

        Gson gson=new Gson();

        AdModel model=gson.fromJson(ads,AdModel.class);
        if(model!=null&&model.results.size()>0)
            return true;
        return false;
    }

    public static void saveUserInfo(Context mContext, UserStatusModel userProfileVO) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(NAME, userProfileVO.full_name);
            editor.putString(EMAIL, userProfileVO.email);

            editor.putString(MOBILE, userProfileVO.mobile_no);
            editor.putString(USER_PH_SYNC_TIME, userProfileVO.get_next_phonebook_sync_on());
            editor.putString(JOB_TITLE, userProfileVO.job_title);
            editor.putInt(IS_MID_OUT, userProfileVO.is_mid_out);
            editor.putBoolean(RESUME_MIDOUT_FLAG, userProfileVO.is_resume_mid_out);

            editor.putString(CANDIDATE_ID, userProfileVO.candidate_id);

            editor.putInt(MATCHED_JOBS_COUNT, userProfileVO.matched_jobs_count);
            editor.putInt(UNREAD_JOB_MAIL_COUNT, userProfileVO.unread_alert_mails);
            editor.putInt(UNREAD_RECRUITER_MAIL_COUNT, userProfileVO.unread_recruiter_mails);
            editor.putBoolean(EXP_FLAG, userProfileVO.has_jobs);

            editor.putBoolean(LINKEDIN_CONNECT, userProfileVO.getIsLinkedinSynced());
            editor.putBoolean(GOOGLE_CONNECT, userProfileVO.getIsGmailSynced());
            editor.putBoolean(PHONEBOOK_SYNC_STATUS, userProfileVO.getIsPhonebookSynced());


            if (!userProfileVO.get_last_gmail_sync().equals("")) {
                editor.putLong(GMAIL_SYNC, ShineCommons.convertDatetoMilis(userProfileVO.get_last_gmail_sync()));
            }
            if (!userProfileVO.get_last_linkedin_sync().equals("")) {
                editor.putLong(LINKEDIN_SYNC, ShineCommons.convertDatetoMilis(userProfileVO.get_last_linkedin_sync()));
            }
            if (!userProfileVO.get_last_phonebook_sync().equals("")) {
                editor.putLong(PHONEBOOK_SYNC, ShineCommons.convertDatetoMilis(userProfileVO.get_last_phonebook_sync()));
            }


            editor.putBoolean(EDU_FLAG, userProfileVO.has_education);
            editor.putBoolean(SKILL_FLAG, userProfileVO.has_skills);

            editor.putString(INSTITUTE_NAME, userProfileVO.institute_name);
            editor.putString(HIGHEST_EDUCATION, userProfileVO.highest_education);
            editor.putString(CURRENT_COMPANY, userProfileVO.current_company);
            editor.putInt(EXPERIENCE_IN_YEAR, userProfileVO.experience_in_years);
            if (userProfileVO.profile_title != null && userProfileVO.profile_title.length() > 0) {
                editor.putBoolean(PROFILE_FLAG, true);
            }
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static final String USER_NAME_DUMP = "user_name_dump";
    public static final String CITY_DUMP = "city_dump";
    public static final String EXP_DUMP = "exp_dump";

    public static void saveRegDumpInfo(Context mContext, RegDumpModel model){
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME + model.candidate_id, 0);
            SharedPreferences.Editor editor = searcPref.edit();

            if(model.education!=null){
                editor.putInt(PASSOUT_YEAR, model.education.year_of_passout);
                editor.putString(INSTITUTE_NAME, model.education.institute_name);
                editor.putInt(COURSE_TYPE, model.education.course_type);
                editor.putInt(EDUCATION_LEVEL, model.education.education_level);
                editor.putInt(EDUCATION_SPECIALIZATION, model.education.education_specialization);
                editor.putString(EDUCATION_OTHER,model.education.other_specialization);
            }

            if(model.job!=null){
                editor.putBoolean(IS_CURRENT, model.job.is_current);
                editor.putInt(START_MONTH, model.job.start_month);
                editor.putInt(START_YEAR, model.job.start_year);
                editor.putInt(END_MONTH, model.job.end_month);
                editor.putInt(END_YEAR, model.job.end_month);
                editor.putString(COMPANY_NAME, model.job.company_name);
                editor.putString(JOB_TITLE_PARSER, model.job.job_title);
                editor.putInt(FUCTIONAL_AREA,model.job.functional_area);
                editor.putInt(INDUSTRY,model.job.industry);
            }

            if(model.skills!=null && model.skills.size()>0){
                Gson gson = new Gson();
                String json = gson.toJson(model.skills);
                editor.putString(SKILL_JSON, json);
            }

            if(!TextUtils.isEmpty(model.name)){
                editor.putString(USER_NAME_DUMP, model.name);
            }

            editor.putInt(CITY_DUMP, model.candidate_location);
            editor.putInt(EXP_DUMP, model.experience_in_years);

            editor.putInt(SALARY_IN_LAKH,model.salary_in_lakh);

            editor.apply();

        }
        catch (Exception e){
            e.printStackTrace();
        }


    }


    public static void saveWidgetList(Context context, SimpleSearchModel result)
    {
        try {
            SharedPreferences preferences=context.getSharedPreferences(PREF_NAME,0);
            SharedPreferences.Editor editor=preferences.edit();
            if(result!=null && result.results.size()>0){
                Gson gson = new Gson();
                String json = gson.toJson(result);
                editor.putString(WIDGET_JSON_LIST, json);
               editor.apply();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public static String getWidgetList(Context context) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(PREF_NAME, 0);

            return preferences.getString(WIDGET_JSON_LIST, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }



    public static void saveResumeParser(Context mContext,
                                        ResumeDetails parser, String userId) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME + userId, 0);
            SharedPreferences.Editor editor = searcPref.edit();

            if (parser.getQualifications().size() > 0) {
                editor.putInt(PASSOUT_YEAR, parser.getQualifications().get(0).getYear_of_passout());
                editor.putInt(EDUCATION_SPECIALIZATION, parser.getQualifications().get(0).getEducation_specialization());
                editor.putString(INSTITUTE_NAME, parser.getQualifications().get(0).getInstitute_name());
                editor.putString(EDUCATION_OTHER,parser.getQualifications().get(0).getOther_specialization());
//                editor.putInt(COURSE_TYPE, parser.getQualifications().get(0).getCourse_type());
                editor.putInt(EDUCATION_LEVEL, parser.getQualifications().get(0).getEducation_level());
            }

            if (parser.getJobs().size() > 0) {
                editor.putBoolean(IS_CURRENT, parser.getJobs().get(0).is_current());
                editor.putInt(START_MONTH, parser.getJobs().get(0).getStart_month());
                editor.putInt(START_YEAR, parser.getJobs().get(0).getStart_year());
                editor.putInt(END_MONTH, parser.getJobs().get(0).getEnd_month());
                editor.putInt(END_YEAR, parser.getJobs().get(0).getEnd_year());
                editor.putString(COMPANY_NAME, parser.getJobs().get(0).getCompany_name());
                editor.putString(JOB_TITLE_PARSER, parser.getJobs().get(0).getJob_title());
            }

            if (parser.getSkills().size() > 0) {

                Gson gson = new Gson();
                String json = gson.toJson(parser.getSkills());
                editor.putString(SKILL_JSON, json);

            }

            editor.apply();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ResumeDetails getParserInfo(Context mContext,
                                              String userId) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME + userId, 0);

            ResumeDetails parser = new ResumeDetails();

            ResumeDetails.Qualifications qualifications = new ResumeDetails.Qualifications();
            qualifications.setEducation_level(searcPref.getInt(EDUCATION_LEVEL, 0));
            qualifications.setInstitute_name(searcPref.getString(INSTITUTE_NAME, null));
            qualifications.setYear_of_passout(searcPref.getInt(PASSOUT_YEAR, 0));
            qualifications.setOther_specialization(searcPref.getString(EDUCATION_OTHER,null));
            qualifications.setCourse_type(searcPref.getInt(COURSE_TYPE,0));
            qualifications.setEducation_specialization(searcPref.getInt(EDUCATION_SPECIALIZATION,-1));
            parser.getQualifications().add(qualifications);


            ResumeDetails.jobs job = new ResumeDetails.jobs();
            job.setJob_title(searcPref.getString(JOB_TITLE_PARSER, null));
            job.setCompany_name(searcPref.getString(COMPANY_NAME, null));
            job.setIs_current(searcPref.getBoolean(IS_CURRENT, true));
            job.setStart_month(searcPref.getInt(START_MONTH, 0));
            job.setStart_year(searcPref.getInt(START_YEAR, 0));
            job.setEnd_month(searcPref.getInt(END_MONTH, 0));
            job.setEnd_year(searcPref.getInt(END_YEAR, 0));

            job.setFuctional_area(searcPref.getInt(FUCTIONAL_AREA,-1));

            job.setIndustry(searcPref.getInt(INDUSTRY,-1));

            parser.getJobs().add(job);


            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<ResumeDetails.skills>>() {
            }.getType();
            ArrayList<ResumeDetails.skills> skills = gson.fromJson(searcPref.getString(SKILL_JSON, "[]"), type);
            parser.getSkills().addAll(skills);

            parser.can_location = searcPref.getInt(CITY_DUMP, 0);
            parser.name = searcPref.getString(USER_NAME_DUMP, "");
            parser.exp_in_yr = searcPref.getInt(EXP_DUMP, 0);

            parser.setSalary_in_lakh(searcPref.getInt(SALARY_IN_LAKH,0));


            return parser;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateUserPhSyncTime(Context mContext, String syncTime) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(USER_PH_SYNC_TIME, syncTime);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFCMRegistrationId(Context mContext, String id) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    CONNECT_PREF, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(GCM_REGISTRATION_ID, id);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFCMRegistrationId(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    CONNECT_PREF, 0);
            return searcPref.getString(GCM_REGISTRATION_ID, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveUserEmail(Context mContext, String emailId) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(EMAIL, emailId);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveUserPass(Context mContext, String pass) {
        try {

            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();

            editor.putString("pass", XOREncryption.encryptDecrypt(pass));
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveUserName(Context mContext, String userName) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(NAME, userName);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateUserEmailAndMobile(Context mContext,
                                                String emailId, String mobile, String name) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(EMAIL, emailId);
            editor.putString(MOBILE, mobile);
            editor.putString(NAME, name);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateJobTitle(Context mContext, String job_title) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putString(JOB_TITLE, job_title);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateRateAppCount(Context mContext, boolean ratecount,
                                          boolean dontAsk, boolean setDate, boolean resetRateCount) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    RATE_APP_PREF, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, 1);
            Date dateSet = cal.getTime();

            int rateAppHit = searcPref.getInt(RATE_APP_COUNT, 0);

            if (ratecount)
                editor.putInt(RATE_APP_COUNT, rateAppHit + 1);
            if (dontAsk)
                editor.putBoolean(DONT_ASK_AGAIN, dontAsk);
            if (setDate) {
                editor.putString(RATE_APP_DATE, dateSet.toString());

                editor.putInt(RATE_APP_COUNT, 0);
            }
            if (resetRateCount)
                editor.putInt(RATE_APP_COUNT, 0);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUserEmail(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getString(EMAIL, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUserPass(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            String pass = searcPref.getString("pass", null);
            if(!TextUtils.isEmpty(pass))
            return XOREncryption.encryptDecrypt(pass);
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UserStatusModel getUserInfo(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);

            UserStatusModel userProfileVO = new UserStatusModel();
            String userId = searcPref.getString(CANDIDATE_ID, null);
            if (userId == null) {
                return null;
            }
            userProfileVO.candidate_id = userId;
            userProfileVO.full_name = (searcPref.getString(NAME, null));
            userProfileVO.email = (searcPref.getString(EMAIL, null));
            userProfileVO.mobile_no = (searcPref.getString(MOBILE, null));
            userProfileVO.job_title = (searcPref.getString(JOB_TITLE, null));
            userProfileVO.set_next_phonebook_sync_on(searcPref.getString(USER_PH_SYNC_TIME, ""));

            userProfileVO.matched_jobs_count = (searcPref.getInt(MATCHED_JOBS_COUNT, 0));
            userProfileVO.unread_alert_mails = (searcPref.getInt(UNREAD_JOB_MAIL_COUNT,
                    0));

            userProfileVO.unread_recruiter_mails = (searcPref.getInt(
                    UNREAD_RECRUITER_MAIL_COUNT, 0));
            userProfileVO.has_education = (searcPref.getBoolean(EDU_FLAG, false));
            userProfileVO.has_jobs = (searcPref.getBoolean(EXP_FLAG, false));
            userProfileVO.has_skills = (searcPref.getBoolean(SKILL_FLAG, false));

            userProfileVO.current_company = (searcPref.getString(CURRENT_COMPANY, null));
            userProfileVO.institute_name = (searcPref.getString(INSTITUTE_NAME, null));
            userProfileVO.highest_education = (searcPref.getString(HIGHEST_EDUCATION, null));

            userProfileVO.experience_in_years = (searcPref.getInt(EXPERIENCE_IN_YEAR, 2));

            return userProfileVO;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean signoutUser(Context mContext, String message) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.clear();
            Toast.makeText(mContext, message,
                    Toast.LENGTH_SHORT).show();
            editor.apply();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void clearAlerts(Context mContext) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.remove(MATCHED_JOBS_COUNT);
        editor.remove(PROFILE_VIEWDED_COUNT);
        editor.remove(PROFILE_ALERTS_COUNT);
        editor.remove(UNREAD_JOB_MAIL_COUNT);
        editor.remove(UNREAD_RECRUITER_MAIL_COUNT);
        editor.remove(EXP_FLAG);
        editor.remove(EDU_FLAG);
        editor.remove(SKILL_FLAG);
        editor.remove(DASH_FLAG);
        editor.remove(VERIFY_EMAIL_FLAG);
        editor.remove(VERIFY_MOBILE_FLAG);
        editor.remove(GOOGLE_CONNECT);
        editor.remove(FACEBOOK_CONNECT);
        editor.remove(LINKEDIN_CONNECT);
        editor.remove(LAST_MATCH_JOB_TIME);

        editor.apply();
    }

    public static void setPhonebookSyncStatus(Context mContext, boolean status) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(PHONEBOOK_SYNC_STATUS, status);
        editor.apply();

    }


    public static void setRefferalFlag(Context mContext, boolean refferalsend) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(REFFERAL_FLAG, refferalsend);
        editor.apply();
    }

    public static boolean getRefferalFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(REFFERAL_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void setResumeMidout(Context mContext, boolean isResumeMidout) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(RESUME_MIDOUT_FLAG, isResumeMidout);
        editor.apply();
    }


    public static boolean getResumeMidout(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(RESUME_MIDOUT_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveGAID(Context mContext,String gaid){
        SharedPreferences searcPref = mContext.getSharedPreferences(
                PREF_NAME, 0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putString("gaid",gaid);
        editor.apply();
    }

    public static String getGAID(Context mContext){
        SharedPreferences searcPref = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return searcPref.getString("gaid",null);
    }

    public static void saveVendorId(Context mContext,String vendorId){
        SharedPreferences searcPref = mContext.getSharedPreferences(
                PREF_NAME, 0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putString("vendorId",vendorId);
        editor.apply();

        Log.d("NON_SKIP_TEST  ","Saved vendor id "+vendorId);



    }

    public static String getVendorId(Context mContext){
        SharedPreferences searcPref = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return searcPref.getString("vendorId",null);
    }

    public static void setDashboard(Context mContext, boolean isDashboard) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(DASH_FLAG, isDashboard);
        editor.apply();
    }

    public static boolean getDashboard(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(DASH_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setExpDashFlag(Context mContext, boolean exp) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(EXP_FLAG, exp);
        editor.apply();
    }

    public static void setEduDashFlag(Context mContext, boolean edu) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(EDU_FLAG, edu);
        editor.apply();
    }

    public static void setSkillDashFlag(Context mContext, boolean skill) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(SKILL_FLAG, skill);
        editor.apply();
    }

    public static boolean getSkillFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(SKILL_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setCertiDashFlag(Context mContext, boolean certi) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(CERTI_FLAG, certi);
        editor.apply();
    }

    public static boolean getCertiFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(CERTI_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getEduFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(EDU_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getExpFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(EXP_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getProfileFlag(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(PROFILE_FLAG, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setProfileFlag(Context mContext, boolean value) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putBoolean(PROFILE_FLAG, value);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void savePhoneBookDelivered(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(PH_BOOK_DELIVERY, Flag);
        editor.apply();
    }

    public static boolean getPhoneBookDelivered(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(PH_BOOK_DELIVERY, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveSyncPhTime(Context mContext, String datetime,
                                      String user) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putString(PH_SYNC_TIME + user, datetime);
        editor.apply();
    }

    public static String getSyncPhTime(Context mContext, String user) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getString(PH_SYNC_TIME + user, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void savePhoneBookReadProgress(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(PH_BOOK_PROGRESS, Flag);
        editor.apply();
    }

    public static boolean getPhoneBookReadProgress(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(PH_BOOK_PROGRESS, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveLinkedinImported(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(LINKEDIN_CONNECT, Flag);
        editor.apply();
    }

    public static boolean getLinkedinImported(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(LINKEDIN_CONNECT, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveGoogleImported(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(GOOGLE_CONNECT, Flag);
        editor.apply();
    }

    public static void savePhonebookImported(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(PHONEBOOK_SYNC_STATUS, Flag);
        editor.apply();
    }

    public static boolean getGoogleImported(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(GOOGLE_CONNECT, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean getPhonebookImported(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(PHONEBOOK_SYNC_STATUS, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean getTutpageVisitedFirstTime(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    TUTORIAL_PAGE_INSTALLED, 0);
            boolean isAppInstalled = searcPref.getBoolean(
                    TUTORIAL_PAGE_INSTALLED, false);
            return isAppInstalled;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    public static void saveTutpageVisitedFirstTime(Context mContext) {
        try {

            SharedPreferences searcPref = mContext.getSharedPreferences(
                    TUTORIAL_PAGE_INSTALLED, 0);
            SharedPreferences.Editor editor = searcPref.edit();
            editor.putBoolean(TUTORIAL_PAGE_INSTALLED, true);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateSynctimeGmail(Context mContext, Long TimeInmilis) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = lookupPrefs.edit();
            editor.putLong(GMAIL_SYNC, TimeInmilis);

            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateSynctimePhonebook(Context mContext, Long TimeInmilis) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = lookupPrefs.edit();
            editor.putLong(PHONEBOOK_SYNC, TimeInmilis);

            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateSynctimeLinkedIn(Context mContext, long TimeInmilis) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = lookupPrefs.edit();
            editor.putLong(LINKEDIN_SYNC, TimeInmilis);

            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static long getSyncTimeGmail(Context mContext) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return lookupPrefs.getLong(GMAIL_SYNC, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public static long getSyncTimePhonebook(Context mContext) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return lookupPrefs.getLong(PHONEBOOK_SYNC, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public static long getSyncTimeLinkedIn(Context mContext) {
        try {
            SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return lookupPrefs.getLong(LINKEDIN_SYNC, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public static void setPrefUpdateProfile(Context mContext, boolean flag) {
        try {
            SharedPreferences update = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            SharedPreferences.Editor editor = update.edit();
            editor.putBoolean(UPDATE_PROFILE, flag);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean getPrefUpdateProfile(Context mContext) {
        SharedPreferences update = mContext.getSharedPreferences(PREF_NAME, 0);
        return update.getBoolean(UPDATE_PROFILE, false);


    }

    public static void saveResumeSkip(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(RESUME_SKIP, Flag);
        editor.apply();
    }

    public static boolean getResumeSkip(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(RESUME_SKIP, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveProfileSkip(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(PROFILE_SKIP, Flag);
        editor.apply();
    }

//    public static void saveLastUpdateFlowTime(Context mContext, long time) {
//        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
//                0);
//        SharedPreferences.Editor editor = searcPref.edit();
//        editor.putLong(UPDATE_FLOW_SKIP, time);
//        editor.apply();
//    }
//
//    public static boolean getUpdateFlowSkip(Context mContext) {
//        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
//                0);
//        long lastTime = searcPref.getLong(UPDATE_FLOW_SKIP, 0);
//        if (lastTime + 24 * 60 * 60 * 1000 > System.currentTimeMillis())
//            return true;
//        return false;
//    }

    public static void saveReferralCode(Context context, String jobId, String referralCode) {
        SharedPreferences referrallPref = context.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = referrallPref.edit();
        editor.putString(jobId, referralCode);
        editor.apply();


    }

    public static String getGetReferralCode(Context context, String jobId) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(PREF_NAME, 0);
            return preferences.getString(jobId, "");
        } catch (Exception e) {
            e.printStackTrace();

        }
        return "";
    }

    public static boolean getProfileSkip(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(PROFILE_SKIP, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setUpdateSkip(Context mContext, boolean Flag) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putBoolean(UPDATE_SKIP, Flag);
        editor.apply();
    }

    public static boolean getUpdateSkip(Context mContext) {
        try {
            SharedPreferences searcPref = mContext.getSharedPreferences(
                    PREF_NAME, 0);
            return searcPref.getBoolean(UPDATE_SKIP, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveServerLookupVersion(Context mContext, int version) {
        SharedPreferences searcPref = mContext.getSharedPreferences(APP_PREF,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putInt(SERVER_LOOKUP_VERSION, version);
        editor.apply();

    }

    public static int getServerLookupVersion(Context mContext) {
        SharedPreferences searcPref = mContext.getSharedPreferences(APP_PREF,
                0);
        return searcPref.getInt(SERVER_LOOKUP_VERSION, LookupInit.CURRENT_FILE_LOOKUP_VERSION);

    }


    public static void setLatestAppVersion(Context mContext, int version) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putInt(APP_VERSION_LATEST, version);
        editor.apply();

    }

    public static int getLatestAppVersion(Context mContext) {
        SharedPreferences searcPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        return searcPref.getInt(APP_VERSION_LATEST, 0);

    }

    public static void setClientAccessToken(Context mContext, String token) {
        SharedPreferences searcPref = mContext.getSharedPreferences(APP_PREF,
                0);
        SharedPreferences.Editor editor = searcPref.edit();
        editor.putString(CLIENT_ACCESS_TOKEN, token);
        editor.apply();

    }

    public static String getClientAccessToken(Context mContext) {
        SharedPreferences searcPref = mContext.getSharedPreferences(APP_PREF,
                0);
        return searcPref.getString(CLIENT_ACCESS_TOKEN, "shine");
    }

    public static void setUserAccessToken(Context mContext, String token) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString(USER_ACCESS_TOKEN, token);
        editor.apply();

    }

    public static String getUserAccessToken(Context mContext) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        return mPref.getString(USER_ACCESS_TOKEN, "");
    }

    public static void setCandidateId(Context mContext, String candidtaeId) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(CANDIDATE_ID, candidtaeId);
        editor.apply();
    }

    public static String getCandidateId(Context mContext) {

        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        return mPref.getString(CANDIDATE_ID, "");
    }

//    public static void setUpdateFlowType(Context mContext, String type) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(UPDATE_FLOW_TYPE, type);
//        editor.apply();
//    }
//
//    public static String getUpdateFlowType(Context mContext) {
//
//        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
//                0);
//        String type = mPref.getString(UPDATE_FLOW_TYPE, "");
//
//        return type;
//    }
//
//    public static void setUpdateFlowEdu(Context mContext, EducationDetailModel model) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putInt(UPDATE_FLOW_PASSOUT_YEAR, model.passoutYearIndex);
//        editor.putInt(UPDATE_FLOW_EDUCATION_COURSE_TYPE, model.courseTypeIndex);
//        editor.putInt(UPDATE_FLOW_EDUCATION_LEVEL, model.educationNameIndex);
//        editor.putInt(UPDATE_FLOW_EDUCATION_SPECIALIZATION, model.educationStreamIndex);
//        editor.putString(UPDATE_FLOW_INSTITUTE_NAME, model.institueName);
//
//        editor.apply();
//    }
//
//    public static EducationDetailModel getUpdateFlowEdu(Context mContext) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        EducationDetailModel model = new EducationDetailModel();
//        model.passoutYearIndex = mPrefs.getInt(UPDATE_FLOW_PASSOUT_YEAR, 0);
//        model.courseTypeIndex = mPrefs.getInt(UPDATE_FLOW_EDUCATION_COURSE_TYPE, 0);
//        model.educationNameIndex = mPrefs.getInt(UPDATE_FLOW_EDUCATION_LEVEL, 0);
//        model.educationStreamIndex = mPrefs.getInt(UPDATE_FLOW_EDUCATION_SPECIALIZATION, 0);
//        model.institueName = mPrefs.getString(UPDATE_FLOW_INSTITUTE_NAME, "");
//        return model;
//    }
//
//    public static void setUpdateFlowJob(Context mContext, EmploymentDetailModel model) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(UPDATE_FLOW_JOB_TITLE, model.getJobTitle());
//        editor.putString(UPDATE_FLOW_COMPANY_NAME, model.getComName());
//        editor.putInt(UPDATE_FLOW_SUB_FIELD, model.getFunctionalIndex());
//        editor.putInt(UPDATE_FLOW_INDUSTRY_ID, model.getIndustryIndex());
//        editor.putBoolean(UPDATE_FLOW_IS_CURRENT, model.isCurrent());
//        editor.putString(UPDATE_FLOW_START_YEAR, model.getStartYear());
//        editor.putString(UPDATE_FLOW_START_MONTH, model.getStartMonth());
//        editor.putString(UPDATE_FLOW_END_MONTH, model.getEndMonth());
//        editor.putString(UPDATE_FLOW_END_YEAR, model.getEndYear());
//
//
//        editor.apply();
//    }
//
//    public static EmploymentDetailModel getUpdateFlowJob(Context mContext) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        EmploymentDetailModel model = new EmploymentDetailModel();
//        model.setJobTitle(mPrefs.getString(UPDATE_FLOW_JOB_TITLE, ""));
//        model.setComName(mPrefs.getString(UPDATE_FLOW_COMPANY_NAME, ""));
//        model.setFunctionalIndex(mPrefs.getInt(UPDATE_FLOW_SUB_FIELD, 0));
//        model.setIndustryIndex(mPrefs.getInt(UPDATE_FLOW_INDUSTRY_ID, 0));
//        model.setCurrent(mPrefs.getBoolean(UPDATE_FLOW_IS_CURRENT, false));
//        model.setStartYear(mPrefs.getString(UPDATE_FLOW_START_YEAR, ""));
//        model.setStartMonth(mPrefs.getString(UPDATE_FLOW_START_MONTH, ""));
//        model.setEndYear(mPrefs.getString(UPDATE_FLOW_END_YEAR, ""));
//        model.setEndMonth(mPrefs.getString(UPDATE_FLOW_END_MONTH, ""));
//        return model;
//    }
//
//    public static void setUpdateFlowSkills(Context mContext, JSONArray array) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putString(UPDATE_FLOW_SKILL, array.toString());
//        editor.apply();
//    }
//
//    public static List<SkillModel> getUpdateFlowSkills(Context mContext) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        Type type = new TypeToken<ArrayList<SkillModel>>() {
//        }.getType();
//
//        Gson gson = new Gson();
//        List<SkillModel> skillList = gson.fromJson(mPrefs.getString(UPDATE_FLOW_SKILL, ""), type);
//        return skillList;
//    }


    public static void setLinkedinPictureUrl(String picture_url, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        lookupPrefs.edit().putString("pictureurl", picture_url).apply();
    }

    public static String getLinkedinPictureUrl(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getString("pictureurl", "");
    }

    public static void setLinkedinAccessToken(String accessToken, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        lookupPrefs.edit().putString("accesstoken2", accessToken).apply();
    }

    public static String getLinkedinAccessToken(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getString("accesstoken2", "");
    }

    public static void setLinkedinAccessExpiry(String expiry, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        try {
            long lExpiry = Long.parseLong(expiry);
            lookupPrefs.edit().putLong("expires2", lExpiry).apply();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static long getLinkedinAccessExpiry(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getLong("expires2", 0);
    }


    public static void setGoogleAccessToken(String accessToken, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        lookupPrefs.edit().putString("accesstoken2_google", accessToken).apply();
    }

    public static String getGoogleAccessToken(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getString("accesstoken2_google", "");
    }

    public static void setGooleAccessExpiry(String expiry, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        try {
            long lExpiry = Long.parseLong(expiry);
            lookupPrefs.edit().putLong("expires2_google", lExpiry).apply();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void setFacebookAccessToken(String accessToken, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        lookupPrefs.edit().putString("accesstoken2_facebook", accessToken).apply();
    }

    public static String getFacebookAccessToken(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getString("accesstoken2_facebook", "");
    }

    public static void setFacebookAccessExpiry(String expiry, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        try {
           // long lExpiry = Long.parseLong(expiry);
            //lookupPrefs.edit().putLong("expires2_facebook", lExpiry).apply();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static long getFacebookAccessExpiry(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        return lookupPrefs.getLong("expires2_facebook", 0);
    }


    public static void setLinkedinProfile(ApplywithoutResumeModel model, Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        Gson gson = new Gson();
        String serialized_obj = gson.toJson(model);
        lookupPrefs.edit().putString("linkedin_model", serialized_obj).apply();
    }

    public static ApplywithoutResumeModel getLinkedProfile(Context mContext) {
        SharedPreferences lookupPrefs = mContext.getSharedPreferences(
                PREF_NAME, 0);
        String serialized_obj = lookupPrefs.getString("linkedin_model", "");
        try {
            Gson gson = new Gson();
            return gson.fromJson(serialized_obj, ApplywithoutResumeModel.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static void setShowUpdateFlow(Context mContext, boolean flag) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putBoolean(UPDATE_FLOW_FLAG, flag);
//        editor.apply();
//    }
//
//    public static boolean getShowUpdateflow(Context mContext) {
//
//        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
//                0);
//        boolean flag = mPref.getBoolean(UPDATE_FLOW_FLAG, false);
//
//        return flag;
//    }

//    public static void setShowSimilarJobs(Context mContext, boolean flag) {
//        SharedPreferences mPrefs = mContext.getSharedPreferences(PREF_NAME, 0);
//        SharedPreferences.Editor editor = mPrefs.edit();
//        editor.putBoolean(SIMILAR_JOBS_FLAG, flag);
//        editor.apply();
//    }
//
//    public static boolean getShowSimilarJobs(Context mContext) {
//        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
//                0);
//        boolean flag = mPref.getBoolean(SIMILAR_JOBS_FLAG, false);
//
//        return flag;
//
//    }

    public static void setLinkedInResumeEnabled(Context mContext, boolean flag) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        mPref.edit().putBoolean(LINKEDIN_RESUME_STATUS, flag).apply();
    }

    public static boolean isLinkedInResumeEnabled(Context mContext) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        boolean flag = mPref.getBoolean(LINKEDIN_RESUME_STATUS, true);

        return flag;
    }

    public static void setUpdateFlowEnabled(Context mContext, boolean flag) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        mPref.edit().putBoolean(UPDATE_FLOW_STATUS, flag).apply();
    }

    public static boolean isUpdateFlowEnabled(Context mContext) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        boolean flag = mPref.getBoolean(UPDATE_FLOW_STATUS, true);

        return flag;
    }

    public static void setFbUserData(Context mContext, ApplywithoutResumeModel model) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        Gson gson = new Gson();
        String serialized_obj = gson.toJson(model);
        mPref.edit().putString(FB_PROFILE, serialized_obj).apply();
    }

    public static ApplywithoutResumeModel getFbUserData(Context mContext) {
        SharedPreferences mPref = mContext.getSharedPreferences(PREF_NAME,
                0);
        String serialized_obj = mPref.getString(FB_PROFILE, "");
        try {
            Gson gson = new Gson();
            return gson.fromJson(serialized_obj, ApplywithoutResumeModel.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
