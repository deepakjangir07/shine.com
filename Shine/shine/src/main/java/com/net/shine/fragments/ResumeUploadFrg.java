package com.net.shine.fragments;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.chooser.android.DbxChooser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.android.gms.plus.Plus;
import com.google.gson.reflect.TypeToken;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.AuthActivity;
import com.net.shine.activity.HomeActivity;
import com.net.shine.activity.ProfileCompleteActivity;
import com.net.shine.activity.ResumeUploadActivity;
import com.net.shine.activity.SocialApplyFormActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.models.RegDumpModel;
import com.net.shine.models.ResumeDetails;
import com.net.shine.services.LinkedInSyncService;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by ujjawal-work on 19/08/16.
 */

public class ResumeUploadFrg extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener {


    private static final String APP_KEY = "3ln3ploxp6djwys";

    private static final int MOB_CHOOSER_REQUEST = 0;
    private static final int EMAIL_REQUEST = 4;
    private static final int DBX_CHOOSER_REQUEST = 2;
    private static final int GDRIVE_CHOOSER_REQUEST = 3;
    private Dialog alertDialog;
    private Dialog alertDialogMobile;
    private Dialog alertDialogDrive;
    private Dialog alertDialogDBX;

    private static final int REQUEST_CODE_OPENER = 1;
    private static final int REQUEST_CODE_RESOLUTION = 12;
    private static final int REQUEST_STORAGE_PERMISSIONS = 10;
    private static final int REQUEST_ACCOUNT_PERMISSIONS = 13;

    private DriveId driveId;
    private String fileName;
    private File final_file;
    private String Dropbox_download_url;
    private String gdrive_file_id;
    private String gd_auth_token;


    private GoogleApiClient mGoogleApiClient;
    private boolean addResume = false;

    public boolean nonSkipResume = false;
    boolean isSocialApply=false;


    public static ResumeUploadFrg newInstance(Bundle args) {

        if (args == null)
            args = new Bundle();
        ResumeUploadFrg fragment = new ResumeUploadFrg();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setTitle("Upload Resume");
        addResume = getArguments().getBoolean(URLConfig.FROM_INSIDE_ADD_RESUME, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Upload Resume");

        ManualScreenTracker.manualTracker("ResumeUpload");

        if (addResume) {
            mActivity.showNavBar();

            if (mActivity.actionBar != null)
                mActivity.setActionBarVisible(true);
        } else {
            mActivity.hideNavBar();
        }

        ShineCommons.hideKeyboard(mActivity);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.resume_upload_view, container, false);

        mActivity.setTitle("Upload Resume");
        ShineCommons.hideKeyboard(mActivity);
        setHasOptionsMenu(true);

        view.findViewById(R.id.mobile_upload).setOnClickListener(this);
        view.findViewById(R.id.google_drive).setOnClickListener(this);
        view.findViewById(R.id.dropbox).setOnClickListener(this);
        View email = view.findViewById(R.id.email);
        email.setOnClickListener(this);
        View linkedIn = view.findViewById(R.id.linkedin);
        linkedIn.setOnClickListener(this);

        if (ShineSharedPreferences.isLinkedInResumeEnabled(mActivity)) {
            linkedIn.setVisibility(View.VISIBLE);
        } else {
            linkedIn.setVisibility(View.GONE);
        }

         isSocialApply = getArguments().getBoolean(SocialApplyFragment.IS_SOCIAL_APPLY, false);

        if (isSocialApply) {
            email.setVisibility(View.GONE);
            linkedIn.setVisibility(View.GONE);
        } else {
            boolean isApply = getArguments().getInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL) == ShineAuthUtils.REQUESTED_TO_APPLY_JOBS;
            int from_screen = getArguments().getInt(ShineAuthUtils.COMPLETED_FROM, ShineAuthUtils.FROM_NORMAL);
            if (isApply && from_screen != ShineAuthUtils.FROM_NORMAL_REGISTRATION) {
                TextView t = (TextView) view.findViewById(R.id.resume_heading_text);
                t.setText("You will need a resume to apply to jobs");
                view.findViewById(R.id.resume_heading_text2).setVisibility(View.GONE);
            }
        }


//        if (!addResume && Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
//            Log.d("NON_SKIP_TEST  ", "true for vendorid " + ShineSharedPreferences.getVendorId(mActivity));
//            view.findViewById(R.id.skip).setVisibility(View.GONE);
//            nonSkipResume = true;
//        }
//        else if(isSocialApply)
//        {
//            view.findViewById(R.id.skip).setVisibility(View.GONE);
//            nonSkipResume = true;
//        }
//        else {
//            Log.d("NON_SKIP_TEST  ", "false for vendorid " + ShineSharedPreferences.getVendorId(mActivity));
//            view.findViewById(R.id.skip).setVisibility(View.VISIBLE);
//            nonSkipResume = false;
//        }


        try {
            if (getActivity() instanceof SocialApplyFormActivity) {
                SocialApplyFormActivity authActivity = (SocialApplyFormActivity) getActivity();
                authActivity.gDriveCompat = new SocialApplyFormActivity.GoogleResolutionOnResult() {
                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {

                        Log.d("DBG:: ", "code SocialApplyFormActivity " + requestCode + "  " + resultCode);

                        DialogUtils.dialogDismiss(alertDialog);
                        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_RESOLUTION) {
                            if (mGoogleApiClient != null) {
                                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

                                mGoogleApiClient.connect();
                            }
                        } else {
                            return;
                        }
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (getActivity() instanceof HomeActivity) {
                HomeActivity authActivity = (HomeActivity) getActivity();
                authActivity.gDriveCompat = new HomeActivity.GoogleResolutionOnResult() {
                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {

                        Log.d("DBG:: ", "code  HomeActivity " + requestCode + "  " + resultCode);

                        DialogUtils.dialogDismiss(alertDialog);
                        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_RESOLUTION) {
                            if (mGoogleApiClient != null) {
                                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

                                mGoogleApiClient.connect();
                            }
                        } else {
                            return;
                        }
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (getActivity() instanceof ResumeUploadActivity) {
                ResumeUploadActivity authActivity = (ResumeUploadActivity) getActivity();
                authActivity.gDriveCompat = new ResumeUploadActivity.GoogleResolutionOnResult() {
                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {

                        Log.d("DBG:: ", "code  ResumeUploadActivity " + requestCode + "  " + resultCode);

                        DialogUtils.dialogDismiss(alertDialog);
                        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_RESOLUTION) {
                            if (mGoogleApiClient != null) {
                                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

                                mGoogleApiClient.connect();
                            }
                        } else {
                            return;
                        }
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        try {
            if (getActivity() instanceof AuthActivity) {
                AuthActivity authActivity = (AuthActivity) getActivity();
                authActivity.gDriveCompat = new AuthActivity.GoogleResolutionOnResult() {
                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {

                        Log.d("DBG:: ", "code  AuthActivity " + requestCode + "  " + resultCode);

                        DialogUtils.dialogDismiss(alertDialog);
                        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_RESOLUTION) {
                            if (mGoogleApiClient != null) {
                                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

                                mGoogleApiClient.connect();
                            }
                        } else {
                            return;
                        }
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);

        Log.d("DBG: ", "onActivityResult-- " + requestCode + " " + resultCode);


        switch (requestCode) {
            case MOB_CHOOSER_REQUEST:
                DialogUtils.dialogDismiss(alertDialog);
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                        fileName = data.getStringExtra(FilePickerActivity.RESULT_FILE_NAME);
                        final_file = new File(filePath);
                        uploadResume(MOB_CHOOSER_REQUEST);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case DBX_CHOOSER_REQUEST:
                DialogUtils.dialogDismiss(alertDialog);
                if (resultCode == Activity.RESULT_OK) {
                    Log.d("DBG", "Dropbox");
                    DbxChooser.Result result = new DbxChooser.Result(data);
                    long size = result.getSize();
                    if (size != -1 && size > 1024 * 1024) {
                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "File size is too large. Please choose a smaller file.");
                        return;
                    }
                    fileName = result.getName();
                    String ext = result.getName().substring(
                            (result.getName().length()) - 4);
                    if (ext.equals(".pdf") || ext.equals(".doc")
                            || ext.equals(".docx") || ext.equals(".rtf")
                            || ext.equals(".txt")) {
                        Dropbox_download_url = result.getLink().toString();
                        uploadResume(DBX_CHOOSER_REQUEST);
                    } else {
                        Log.d("DBG", "not valid format Dropbox");

                        Toast.makeText(mActivity, "Not a valid resume format.", Toast.LENGTH_SHORT).show();

                        //alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "Not a valid resume format.");
                    }

                }
                break;

//            case REQUEST_CODE_RESOLUTION:
//                DialogUtils.dialogDismiss(alertDialog);
//                if (resultCode == Activity.RESULT_OK) {
//                    if(mGoogleApiClient!=null) {
//                        mGoogleApiClient.connect();
//                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "Please Wait...");
//                    }
//                }
//                break;
            case EMAIL_REQUEST:


                try {
                    DialogUtils.dialogDismiss(alertDialog);

                    final Dialog alertDialog1 = DialogUtils.showCustomAlertsNoTitle(getActivity(), getString(R.string.email_resume_msg));
                    alertDialog1.findViewById(R.id.ok_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog1.dismiss();
                            goToNextScreen();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case REQUEST_CODE_OPENER:

                Log.d("DBG:  ", "Request code opener");
                DialogUtils.dialogDismiss(alertDialogDrive);
                if (resultCode == Activity.RESULT_OK) {

                    driveId = data
                            .getParcelableExtra(OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);
                    if (getArguments().getBoolean(SocialApplyFragment.IS_SOCIAL_APPLY, false)) {
                        alertDialogDrive = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                        driveId.asDriveFile().getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>() {
                            @Override
                            public void onResult(@NonNull DriveResource.MetadataResult metadataResult) {

                                if (!metadataResult.getStatus().isSuccess()) {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    DialogUtils.dialogDismiss(alertDialogDrive);
                                    alertDialogDrive = DialogUtils.showCustomAlertsNoTitle(mActivity, "Unable to get File.");
                                    return;
                                }
                                if (metadataResult.getMetadata().getFileSize() > 1024 * 1024) {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    DialogUtils.dialogDismiss(alertDialogDrive);

                                    alertDialogDrive = DialogUtils.showCustomAlertsNoTitle(mActivity, "File size too large. Please select a smaller file.");
                                    return;
                                }
                                fileName = metadataResult.getMetadata().getOriginalFilename();
                                Log.d("Debug::GDRIVE_FILE", fileName + "");
                                uploadResume(GDRIVE_CHOOSER_REQUEST);
                            }
                        });
                    } else {


                        gdrive_file_id = driveId.getResourceId();

                        Log.d("DBG:  ", "Request code opener " + gdrive_file_id);

                        if(!TextUtils.isEmpty(gdrive_file_id))
                        uploadResume(GDRIVE_CHOOSER_REQUEST);
                        else
                        {
                            DialogUtils.dialogDismiss(alertDialog);
                            DialogUtils.dialogDismiss(alertDialogDrive);
                            alertDialogDrive = DialogUtils.showCustomAlertsNoTitle(mActivity, "Unable to get File.");
                            return;
                        }
                    }
                }
                break;
        }


    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.resume_skip_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.resume_skip:
                goToNextScreen();
//                ShineSharedPreferences.saveResumeSkip(mActivity, true);
                ShineCommons.trackShineEvents("Skip", "UploadResume", mActivity);
                break;

        }
        return false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (!addResume && Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
            menu.findItem(R.id.resume_skip).setVisible(false);
            nonSkipResume = true;
        }
        else if(addResume){
            menu.findItem(R.id.resume_skip).setVisible(true);
        }
        else if(isSocialApply)
        {
            menu.findItem(R.id.resume_skip).setVisible(false);
            nonSkipResume = true;
        }
        else {
            menu.findItem(R.id.resume_skip).setVisible(true);
            nonSkipResume = false;
        }

    }

    private void goToNextScreen() {

        if (addResume) {

            Log.d(mActivity.getClass().getName(), "called on backpressesd");
            DialogUtils.dialogDismiss(alertDialog);
            mActivity.onBackPressed();


        } else if (nonSkipResume && ShineSharedPreferences.getVendorId(mActivity) != null) {
            if (Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
                Intent intent = new Intent(mActivity, ProfileCompleteActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                mActivity.finish();
            }
        } else {
            fetchRegDumpAndGoBack();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linkedin:

                String accessToken = ShineSharedPreferences.getLinkedinAccessToken(mActivity);
                Long expiry = ShineSharedPreferences.getLinkedinAccessExpiry(mActivity);
                if (TextUtils.isEmpty(accessToken) || expiry < System.currentTimeMillis()) {
                    LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {
                        public void onLinkedInConnected(String accessToken, String expiry, Dialog customDialog) {
                            getResumeFromLinkedIn(accessToken, expiry);
                        }
                    }, false, alertDialog);
                } else {
                    getResumeFromLinkedIn(accessToken, expiry + "");
                }
                break;

            case R.id.dropbox:
                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                if (ShineSharedPreferences.getUserInfo(getContext()) != null) {

                }
                DbxChooser mChooser = new DbxChooser(APP_KEY);
                DbxChooser.ResultType resultType = DbxChooser.ResultType.DIRECT_LINK;
                mChooser.forResultType(resultType).launch(this, DBX_CHOOSER_REQUEST);
                DialogUtils.dialogDismiss(alertDialog);
                break;

            case R.id.google_drive:
                alertDialogDrive = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
                }
                if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.GET_ACCOUNTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    googleDriveClicked();
                } else {
                    requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_ACCOUNT_PERMISSIONS);
                }
                break;

            case R.id.mobile_upload:
                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
                }
                if (!Environment.MEDIA_MOUNTED.equals(Environment
                        .getExternalStorageState())) {
                    Toast.makeText(mActivity, "SD card not Found",
                            Toast.LENGTH_SHORT).show();
                    DialogUtils.dialogDismiss(alertDialog);
                } else {
                    if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startFilePickerActivity();
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            Log.d("m_cooker", "Shoud'ave shown rationale dialog");
                        } else {
                            Log.d("m_cooker", "meh..user is not retarded");
                        }
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS);
                    }
                }
                break;

            case R.id.email:
                alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
                    ShineCommons.trackShineEvents("AddResume", "Email", mActivity);
                }
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "register@shine.com", null));
                final PackageManager pm = mActivity.getPackageManager();
                final List<ResolveInfo> resInfo = pm.queryIntentActivities(
                        emailIntent, 0);
                if (resInfo != null && resInfo.size() > 0) {
                    String text = "register@shine.com";
                    ClipboardManager clipboard = (ClipboardManager) mActivity
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData
                            .newPlainText("User Email", text);
                    clipboard.setPrimaryClip(clip);
                    DialogUtils.showErrorToast("Please e-mail your resume to " + text
                            + ".( Copied to clipboard)");
                    Intent openInChooser = Intent.createChooser(emailIntent,
                            "Send Mail to..");
                    startActivityForResult(openInChooser, EMAIL_REQUEST);
                } else {
                    Toast.makeText(mActivity, "No email client installed",
                            Toast.LENGTH_SHORT).show();
                    DialogUtils.dialogDismiss(alertDialog);
                }
                break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
        }
    }


    private void getResumeFromLinkedIn(String accessToken, String expiry) {
        try {
            DialogUtils.dialogDismiss(alertDialog);
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));

            String URL = URLConfig.RESUME_UPLOAD_URL;

            if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
            }
            if (!addResume) {
                URL = URLConfig.RESUME_UPLOAD_PARSER_URL;

                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                }
            }


            HashMap<String, String> linkedinMap = new HashMap<>();

            linkedinMap.put("candidate_id", ShineSharedPreferences.getCandidateId(getContext()));
            linkedinMap.put("upload_source", "app");
            linkedinMap.put("upload_medium", "linkedin");
            linkedinMap.put("linkedin_token", accessToken);
            linkedinMap.put("linkedin_token_expiry", expiry + "");

            Intent intent = new Intent(mActivity, LinkedInSyncService.class);
            if (!ShineSharedPreferences.getLinkedinImported(mActivity))
                mActivity.startService(intent);

            Type type = new TypeToken<ResumeDetails>() {
            }.getType();
            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this,
                    URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
            request.setUsePostMethod(linkedinMap);
            request.execute("ResumeViaLinkedIn");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void startFilePickerActivity() {
        Intent intent = new Intent(mActivity, FilePickerActivity.class);
        startActivityForResult(intent, MOB_CHOOSER_REQUEST);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        try {
            Map<String, Integer> perms = new HashMap<>();
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);

            DialogUtils.dialogDismiss(alertDialog);

            if (requestCode == REQUEST_STORAGE_PERMISSIONS) {

                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startFilePickerActivity();
                } else if (getView() != null) {
                    Log.i("m_cooker", "STORAGE permission was NOT granted.");

                    Snackbar.make(getView(), "You need to give storage permission to download the resume.",
                            Snackbar.LENGTH_SHORT)
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                            Uri.fromParts("package", mActivity.getPackageName(), null));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mActivity.startActivity(intent);
                                }
                            })
                            .show();
                }


            } else if (requestCode == REQUEST_ACCOUNT_PERMISSIONS) {
                if (perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                    googleDriveClicked();
                } else if (getView() != null) {
                    Log.i("m_cooker", "GET_ACCOUNTS permission was NOT granted.");
                    Snackbar.make(getView(), "You need to give contacts permission to upload resume via Google Drive.",
                            Snackbar.LENGTH_SHORT)
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                            Uri.fromParts("package", mActivity.getPackageName(), null));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mActivity.startActivity(intent);
                                }
                            })
                            .show();
                }
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onGoogleClientConnected() {
        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
                .setMimeType(
                        new String[]{
                                "application/pdf",
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                "application/vnd.oasis.opendocument.text",
                                "application/msword",
                                "application/vnd.google-apps.document",
                                "application/rtf", "text/plain"})

                .build(mGoogleApiClient);

        try {
            Log.d("DBG::GDRIVE::OnConnctd", "Starting chooser");
            startIntentSenderForResult(intentSender,
                    REQUEST_CODE_OPENER, null, 0, 0, 0, null);
            getAccountToken(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAccountToken(final boolean sendToServer) {

        DialogUtils.dialogDismiss(alertDialog);
        alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

        AccountManager am = AccountManager.get(mActivity);
        Account[] ac_list = am.getAccountsByType("com.google");

        String user_Sel_Ac = Plus.AccountApi.getAccountName(mGoogleApiClient);
        Log.d("DBG::PLUS:ONCNCTD", "Getting token for: " + user_Sel_Ac + " SendToServer: " + sendToServer);
        if (user_Sel_Ac == null)
            user_Sel_Ac = "";

        int final_sel_acc_id = 0;
        for (int i = 0; i < ac_list.length; i++) {
            if (ac_list[i].name.equals(user_Sel_Ac)) {
                final_sel_acc_id = i;
                break;
            }
        }

        //Note: Using Scopes.DRIVE_FILE instead of DriveScopes.DRIVE
        //      as google recommends using restrictive scope. Also, including Drive API
        //      library caused the size of app to be increased by 1MB
        //      DriveScopes.DRIVE = "https://www.googleapis.com/auth/drive";
        //      Scopes.DRIVE_FILE = "https://www.googleapis.com/auth/drive.file";


        am.getAuthToken(
                am.getAccountsByType("com.google")[final_sel_acc_id],
                "oauth2:" + Scopes.DRIVE_FILE, new Bundle(), true,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            gd_auth_token = future.getResult().getString(
                                    AccountManager.KEY_AUTHTOKEN);
                            Log.d("DBG:: ", "auth token " + gd_auth_token + " SendToServer: " + sendToServer);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (sendToServer)
                            uploadResume(GDRIVE_CHOOSER_REQUEST);
                    }
                }, null);


    }

    @SuppressWarnings("deprecation")
    private void googleDriveClicked() {
        //  DialogUtils.dialogDismiss(alertDialog);
        //alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
        if (mGoogleApiClient == null) {


            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .setAccountName(null)
                    .addApi(Plus.API)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            onGoogleClientConnected();
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            if (!connectionResult.hasResolution()) {
                                try {
                                    Toast.makeText(mActivity, "Connection failure",
                                            Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                DialogUtils.dialogDismiss(alertDialogDrive);
                                return;
                            }

                            try {
                                connectionResult.startResolutionForResult(mActivity, REQUEST_CODE_RESOLUTION);

                            } catch (IntentSender.SendIntentException e) {
                                e.printStackTrace();
                            }
                        }
                    }).build();
        }

        if (mGoogleApiClient.isConnected()) {
            Log.d("DBG::GDRIVE::OnClick", "client already connected");


            DialogUtils.dialogDismiss(alertDialogDrive);

            AccountManager am = AccountManager.get(mActivity);
            Account[] ac_list = am.getAccountsByType("com.google");

            if (ac_list.length > 1) {
                Log.d("DBG::GDRIVE::OnClick", "Multiple accounts found, clearing default and reconnect");
                mGoogleApiClient.clearDefaultAccountAndReconnect();
            } else {
                Log.d("DBG::GDRIVE::OnClick", "Single account found, call onConnected");
                onGoogleClientConnected();
            }

        } else {
            DialogUtils.dialogDismiss(alertDialogDrive);

            Log.d("DBG::GDRIVE::OnClick", "client not connected, try connecting");
            mGoogleApiClient.connect();
        }
    }

    @SuppressWarnings("deprecation")
    private void uploadResume(int module_type) {
        try {

            DialogUtils.dialogDismiss(alertDialog);

            if (getArguments().getBoolean(SocialApplyFragment.IS_SOCIAL_APPLY, false)) {
                getBase64String(module_type);
                return;
            }

            String URL = URLConfig.RESUME_UPLOAD_URL;

            if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
            } else {
                URL = URL;
            }
            if (!addResume) {
                URL = URLConfig.RESUME_UPLOAD_PARSER_URL;

                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                } else {
                    URL = URL;
                }
            }
            if (module_type == MOB_CHOOSER_REQUEST) {

                Log.d("DBG", "Resume upload " + module_type);

                alertDialogMobile = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                try {


                    MultipartEntity entity = new MultipartEntity();
                    FileBody fileBody = new FileBody(final_file); // image should be
                    entity.addPart("resume_file", fileBody);
                    entity.addPart("upload_medium", new StringBody("direct"));
                    entity.addPart("upload_source", new StringBody("app"));
                    entity.addPart("candidate_id", new StringBody(ShineSharedPreferences.getCandidateId(getContext())));
                    Type type = new TypeToken<ResumeDetails>() {
                    }.getType();
                    VolleyNetworkRequest vm = new VolleyNetworkRequest(mActivity,
                            this, URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())),
                            type, entity);
                    vm.setMultiPartMethod();
                    vm.execute("RESUME_UPLOAD_MOBILE");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (module_type == DBX_CHOOSER_REQUEST) {

                Log.d("DBG", "DropBox link " + Dropbox_download_url);
                alertDialogDBX = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                Type type = new TypeToken<ResumeDetails>() {
                }.getType();
                VolleyNetworkRequest vdownloader = new VolleyNetworkRequest(mActivity, this,
                        URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
                HashMap<String, String> dropboxMap = new HashMap<>();
                dropboxMap.put("candidate_id", ShineSharedPreferences.getCandidateId(getContext()));
                dropboxMap.put("upload_source", "app");
                dropboxMap.put("upload_medium", "dropbox");
                dropboxMap.put("dropbox_download_link", Dropbox_download_url);
                vdownloader.setUsePostMethod(dropboxMap);
                vdownloader.execute("RESUME_UPLOAD_DROPBOX");
            } else {
                DialogUtils.dialogDismiss(alertDialogDrive);
                alertDialogDrive = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                if (gd_auth_token == null || gd_auth_token.equals("")) {
                    getAccountToken(true);
                } else {
                    Type type = new TypeToken<ResumeDetails>() {
                    }.getType();
                    VolleyNetworkRequest vdownloader = new VolleyNetworkRequest(mActivity, this,
                            URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(getContext())), type);
                    HashMap<String, String> gMap = new HashMap<>();
                    gMap.put("upload_medium", "google_drive");
                    gMap.put("upload_source", "app");
                    gMap.put("google_drive_file_id", gdrive_file_id);
                    gMap.put("google_drive_oauth_token", gd_auth_token);
                    vdownloader.setUsePostMethod(gMap);
                    vdownloader.execute("RESUME_UPLOAD_DRIVE");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getBase64String(int module_type) {
        alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
        switch (module_type) {
            case MOB_CHOOSER_REQUEST:
                new AsyncTask<Void, byte[], byte[]>() {
                    @Override
                    protected byte[] doInBackground(Void... params) {
                        try {
                            InputStream inputStream = new FileInputStream(final_file);
                            ByteArrayOutputStream output = new ByteArrayOutputStream();
                            byte[] bytes;
                            byte[] buffer = new byte[8192];
                            int bytesRead;
                            try {
                                while ((bytesRead = inputStream.read(buffer)) != -1) {
                                    output.write(buffer, 0, bytesRead);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            inputStream.close();
                            bytes = output.toByteArray();
                            output.close();
                            saveToTempFile(bytes, fileName);
                            return bytes;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(byte[] result) {
                        DialogUtils.dialogDismiss(alertDialog);
                        setResultAndPop(result, fileName);
                    }
                }.execute();

                break;
            case DBX_CHOOSER_REQUEST:
                new AsyncTask<Void, byte[], byte[]>() {
                    @Override
                    protected byte[] doInBackground(Void... params) {
                        try {
                            URL url = new URL(Dropbox_download_url);
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.connect();
                            // expect HTTP 200 OK, so we don't mistakenly save error report
                            // instead of the file
                            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                return null;
                            }
                            BufferedInputStream reader = new BufferedInputStream(connection.getInputStream());
                            ByteArrayOutputStream output = new ByteArrayOutputStream();
                            byte[] bytes;
                            byte[] buffer = new byte[8192];
                            int bytesRead;
                            try {
                                while ((bytesRead = reader.read(buffer)) != -1) {
                                    output.write(buffer, 0, bytesRead);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            reader.close();
                            bytes = output.toByteArray();
                            output.close();
                            saveToTempFile(bytes, fileName);
                            return bytes;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(byte[] result) {
                        DialogUtils.dialogDismiss(alertDialog);
                        setResultAndPop(result, fileName);
                    }
                }.execute();
                break;
            case GDRIVE_CHOOSER_REQUEST:
                DriveFile file = driveId.asDriveFile();
                file.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null)
                        .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                            @Override
                            public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                                try {
                                    if (!driveContentsResult.getStatus().isSuccess()) {
                                        DialogUtils.dialogDismiss(alertDialog);
                                        DialogUtils.dialogDismiss(alertDialogDrive);
                                        Toast.makeText(getContext(), "Unable to open file.", Toast.LENGTH_SHORT).show();
                                        Log.w("DRIVE", "Error while opening the file contents");
                                        Log.w("DRIVE", driveContentsResult.getStatus().getStatusMessage() + "");
                                        return;
                                    }
                                    Log.i("DRIVE", "File contents opened");
                                    DriveContents contents = driveContentsResult.getDriveContents();
                                    BufferedInputStream reader = new BufferedInputStream(contents.getInputStream());
                                    ByteArrayOutputStream output = new ByteArrayOutputStream();
                                    final byte[] bytes;
                                    byte[] buffer = new byte[8192];
                                    int bytesRead;
                                    try {
                                        while ((bytesRead = reader.read(buffer)) != -1) {
                                            output.write(buffer, 0, bytesRead);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    reader.close();
                                    bytes = output.toByteArray();
                                    output.close();
                                    contents.discard(mGoogleApiClient);
                                    saveToTempFile(bytes, fileName);
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {

                                            DialogUtils.dialogDismiss(alertDialog);

                                            DialogUtils.dialogDismiss(alertDialogDrive);
                                            setResultAndPop(bytes, fileName);
                                        }
                                    });
                                } catch (Exception e) {
                                    DialogUtils.dialogDismiss(alertDialogDrive);

                                    DialogUtils.dialogDismiss(alertDialog);
                                    Toast.makeText(getContext(), "Unable to open file.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        });
                break;
        }
    }

    private void setResultAndPop(byte[] result, String fileName) {
        SocialApplyFragment fragment = (SocialApplyFragment) getTargetFragment();
      if (fragment != null) {
                mActivity.popone();
                fragment.onResumeAcquired(result, fileName);
        }
    }

    private void saveToTempFile(byte[] data, String fileName) {
        try {
            File dir = new File(MyApplication.getResumeDir());
            boolean b;
            b = dir.mkdirs();
            if (b)
                Log.d("DEBUG::WALL-E", "Creating dir(s)...");
            File[] files = dir.listFiles();
            for (File f : files) {
                b = f.delete();
                if (b)
                    Log.d("DEBUG::WALL-E", "Resume file deleted");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(MyApplication.getResumeDir() + fileName));
            bos.write(data);
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void OnDataResponse(Object object, final String tag) {
        try {

            final ResumeDetails resumeDetails = (ResumeDetails) object;

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        DialogUtils.dialogDismiss(alertDialog);
                        DialogUtils.dialogDismiss(alertDialogDBX);

                        DialogUtils.dialogDismiss(alertDialogMobile);

                        DialogUtils.dialogDismiss(alertDialogDrive);

                        if (resumeDetails.getQualifications().size() > 0 || resumeDetails.getJobs().size() > 0
                                || resumeDetails.getSkills().size() > 0) {
                            ShineSharedPreferences.saveResumeParser(mActivity,
                                    resumeDetails, ShineSharedPreferences.getCandidateId(getContext()));
                            Log.d("DENUG::PARSER", "Have Parser Data to Save");

                        } else {
                            Log.d("DENUG::PARSER", "No Parser Data to Save");
                        }
                        ShineSharedPreferences.setResumeMidout(mActivity, false);
                        MoengageTracking.setProfileAttributes(mActivity);

                        try {
                            List<ResumeDetails> resumeList = (List<ResumeDetails>) DataCaching.getInstance().getObject(mActivity, DataCaching.RESUME_LIST_SECTION);
                            if (resumeList != null) {
                                for (ResumeDetails rsModel : resumeList) {
                                    rsModel.setIsDefault(0);
                                }
                                resumeList.add(resumeDetails);
                                DataCaching.getInstance().cacheData(DataCaching.RESUME_LIST_SECTION, "" + DataCaching.CACHE_TIME_LIMIT, resumeList, mActivity);
                            } else {
                                resumeList = new ArrayList<>();
                                resumeList.add(resumeDetails);
                                DataCaching.getInstance().cacheData(DataCaching.RESUME_LIST_SECTION, "" + DataCaching.CACHE_TIME_LIMIT, resumeList, mActivity);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "Resume Uploaded Successfully");
                        alertDialog.findViewById(R.id.ok_btn).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogUtils.dialogDismiss(alertDialog);
                                goToNextScreen();

                            }
                        });

                        //TODO: Deepak Decide if from LoginToApply check is needed or not
                        if (!addResume) {
                            ShineCommons.trackShineEvents("RegistrationConversion", "ResumeMidoutComplete", mActivity);
                            if (!ShineSharedPreferences.getResumeMidout(mActivity) && ShineSharedPreferences.getEduFlag(mActivity)
                                    && ShineSharedPreferences.getSkillFlag(mActivity) && ShineSharedPreferences.getExpFlag(mActivity)) {
                                if (ShineSharedPreferences.getVendorId(mActivity) != null) {
                                    ShineCommons.trackShineEvents("RegistrationComplete" + ShineSharedPreferences.getVendorId(mActivity), "", mActivity);
                                } else {
                                    ShineCommons.trackShineEvents("RegistrationComplete", "", mActivity);
                                }
                            }

                        }

                        switch (tag) {
                            case "RESUME_UPLOAD_DRIVE":
                                ShineCommons.trackShineEvents("AddResume", "GoogleDrive", mActivity);
                                break;
                            case "RESUME_UPLOAD_MOBILE":
                                ShineCommons.trackShineEvents("AddResume", "Mobile", mActivity);
                                break;
                            case "RESUME_UPLOAD_DROPBOX":
                                ShineCommons.trackShineEvents("AddResume", "Dropbox", mActivity);
                                break;
                            case "ResumeViaLinkedIn":
                                ShineCommons.trackShineEvents("AddResume", "LinkedIn", mActivity);
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchRegDumpAndGoBack() {
        DialogUtils.dialogDismiss(alertDialog);
        alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
        Type listType = new TypeToken<RegDumpModel>() {
        }.getType();
        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                DialogUtils.dialogDismiss(alertDialog);
                Log.d("Debug::Reg-dump", object + " ");
                if (object != null) {
                    ShineSharedPreferences.saveRegDumpInfo(mActivity, (RegDumpModel) object);
                }
                mActivity.onBackPressed();
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("Debug::Reg-dump", error + " ");
                DialogUtils.dialogDismiss(alertDialog);
                mActivity.onBackPressed();
            }
        },
                URLConfig.REG_DUMP_API.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)), listType);
        downloader.execute("Reg-Dump");
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {


                @Override
                public void run() {


                    DialogUtils.dialogDismiss(alertDialog);
                    DialogUtils.dialogDismiss(alertDialogDBX);

                    DialogUtils.dialogDismiss(alertDialogMobile);

                    DialogUtils.dialogDismiss(alertDialogDrive);
                    // FIXME: 10/13/2015 - String Comparison!! Bad Dev! >:(
                    if (error != null && error.toLowerCase().contains("linkedin auth")) {
                        //clear tokens
                        ShineSharedPreferences.setLinkedinAccessToken("", mActivity);
                        ShineSharedPreferences.setLinkedinAccessExpiry("0", mActivity);
                        //ask for Re-Login
                        LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {
                            public void onLinkedInConnected(String accessToken, String expiry, Dialog customDialog) {
                                DialogUtils.dialogDismiss(customDialog);
                                getResumeFromLinkedIn(accessToken, expiry);
                            }
                        }, false, null);
                    } else {
                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
