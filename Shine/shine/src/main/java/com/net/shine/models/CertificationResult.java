package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class CertificationResult implements Serializable {
    int certification_year=0;
    String certification_name="";
    String id="";

    public int getCertification_year() {
        return certification_year;
    }

    public void setCertification_year(int certification_year) {
        this.certification_year = certification_year;
    }

    public String getCertification_name() {
        return certification_name;
    }

    public void setCertification_name(String certification_name) {
        this.certification_name = certification_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CertificationResultsModel{" +
                "certification_year=" + certification_year +
                ", certification_name='" + certification_name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
