package com.net.shine.fragments.components;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.EmploymentDetailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gobletsky on 8/7/15.
 */
public class EmploymentDetailsEditFragment extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener, View.OnFocusChangeListener {

    private EmploymentDetailModel empModel;
    private View view;
    boolean isAddJob = false;
    private LinearLayout container_box;
    boolean hasCurrentJob = false;
    private String ADD_TAG = "AddProfessionalDetails";
    private String EDIT_TAG = "UpdateProfessionalDetails";
    private String GET_TAG = "getTotalJobDetails";
    ProgressDialog dialog ;
    TextInputLayout inputLayoutJobTitle, inputLayoutCompany, inputLayoutIndustry, inputLayoutFunctionalArea;// inputLayoutStartYear, inputLayoutEndYear, inputLayoutStartMonth, inputLayoutEndMonth;


   public EmploymentDetailsEditFragment(boolean hasCurrentJob)
    {
        this.hasCurrentJob = hasCurrentJob;
    }

    public EmploymentDetailsEditFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.employment_details_edit, container, false);
        container_box = (LinearLayout) view.findViewById(R.id.main_container);
        container_box.requestFocus();

        try {

            TextView tv_title = (TextView) view.findViewById(R.id.header_title);
            tv_title.setText("Employment Details");

          dialog  = new ProgressDialog(mActivity);
            Bundle bundle = getArguments();

            if (bundle != null && bundle.containsKey(URLConfig.KEY_MODEL_NAME)) {
                Gson gson = new Gson();
                String json = bundle.getString(URLConfig.KEY_MODEL_NAME);
                empModel = gson.fromJson(json, EmploymentDetailModel.class);
                isAddJob = false;
            } else {
                    System.out.println("--I M IN ELSE--");
                    empModel = new EmploymentDetailModel();
                    isAddJob = true;
                }

            addUpdateProfessional();

            TextChangeListener.addListener(jobTitleField,inputLayoutJobTitle,mActivity);
            TextChangeListener.addListener(funAreaField,inputLayoutFunctionalArea,mActivity);
            TextChangeListener.addListener(indusNameField,inputLayoutIndustry,mActivity);
            TextChangeListener.addListener(comNameField,inputLayoutCompany,mActivity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        return view;
    }







    EditText jobTitleField;
    CheckBox alertBoxImg;
    EditText comNameField;
    EditText indusNameField;
    EditText funAreaField;
    EditText expStartYearField;
    EditText expStartMonthField;
    EditText expEndYearField;
    EditText expEndMonthField;
    Dialog  alertDialog;



    private void addUpdateProfessional() {
        try {


            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);

            jobTitleField = (EditText) view
                    .findViewById(R.id.job_title_field);
            inputLayoutCompany = (TextInputLayout) view.findViewById(R.id.input_layout_comp);
            inputLayoutJobTitle = (TextInputLayout) view.findViewById(R.id.input_layout_title);
            inputLayoutFunctionalArea = (TextInputLayout) view.findViewById(R.id.input_layout_area);
            inputLayoutIndustry = (TextInputLayout) view.findViewById(R.id.input_layout_industry);
            jobTitleField.setText(empModel.getJobTitle());

            alertBoxImg = (CheckBox) view.findViewById(R.id.alert_check);
            alertBoxImg.setChecked(false);
            view.findViewById(R.id.alert_check_layout)
                    .setOnClickListener(this);

            comNameField = (EditText) view
                    .findViewById(R.id.comp_name_field);
            comNameField.setText(empModel.getComName());

            indusNameField = (EditText) view
                    .findViewById(R.id.industry_name_field);
            indusNameField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.INDUSTRY_REVERSE_MAP, URLConfig.INDUSTRY_LIST3,
                    empModel.getIndustryIndex(), indusNameField));
            indusNameField.setOnClickListener(this);
            indusNameField.setOnFocusChangeListener(this);

            funAreaField = (EditText) view
                    .findViewById(R.id.func_area_field);
            funAreaField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.FUNCTIONA_AREA_REVERSE_MAP,
                    URLConfig.FUNCTIONAL_AREA_LIST2,
                    empModel.getFunctionalIndex(), funAreaField));
            funAreaField.setOnClickListener(this);
            funAreaField.setOnFocusChangeListener(this);

            expStartYearField = (EditText) view
                    .findViewById(R.id.start_year);

            if (!empModel.getStartYear().equals("")) {
                expStartYearField.setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                        URLConfig.YEARS_NAME_LIST,
                        Integer.parseInt(empModel.getStartYear()),
                        expStartYearField));
            } else {
                expStartYearField.setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                        URLConfig.YEARS_NAME_LIST, -1, expStartYearField));
            }
            expStartYearField.setOnClickListener(this);
            expStartYearField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    EmploymentDetailsEditFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expStartMonthField = (EditText) view
                    .findViewById(R.id.start_month);
            if (!empModel.getStartMonth().equals("")) {

                expStartMonthField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.MONTH_NAME_REVERSE_MAP,
                        URLConfig.MONTH_NAME_LIST,
                        Integer.parseInt(empModel.getStartMonth()),
                        expStartMonthField));
            } else {
                expStartMonthField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.MONTH_NAME_REVERSE_MAP,
                        URLConfig.MONTH_NAME_LIST, -1, expStartMonthField));
            }
            expStartMonthField.setOnClickListener(this);
            expStartMonthField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    EmploymentDetailsEditFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expEndYearField = (EditText) view.findViewById(R.id.end_year);
            if (!empModel.getEndYear().equals("")) {

                expEndYearField
                        .setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                                URLConfig.YEARS_NAME_LIST,
                                Integer.parseInt(empModel.getEndYear()),
                                expEndYearField));
            } else {
                expEndYearField.setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                        URLConfig.YEARS_NAME_LIST, -1, expEndYearField));
            }

            expEndYearField.setOnClickListener(this);
            expEndYearField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    EmploymentDetailsEditFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expEndMonthField  = (EditText) view
                    .findViewById(R.id.end_month);
            if (!empModel.getEndMonth().equals("")) {

                expEndMonthField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.MONTH_NAME_REVERSE_MAP,
                        URLConfig.MONTH_NAME_LIST,
                        Integer.parseInt(empModel.getEndMonth()),
                        expEndMonthField));
            } else {
                expEndMonthField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.MONTH_NAME_REVERSE_MAP,
                        URLConfig.MONTH_NAME_LIST, -1, expEndMonthField));
            }

            expEndMonthField.setOnClickListener(this);
            expEndMonthField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    EmploymentDetailsEditFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            if (empModel.isCurrent()) {
                alertBoxImg.setChecked(true);
                expEndMonthField.setVisibility(View.GONE);
                expEndYearField.setVisibility(View.GONE);
                view.findViewById(R.id.toyear).setVisibility(View.GONE);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    private String editEmpID = null;
    private void updateProfessionalDetails() {
        try {


            boolean alertsCheck = alertBoxImg.isChecked();

            String jobTitleTV = "" + jobTitleField.getText();
            jobTitleTV = jobTitleTV.trim();

            boolean isEmpty = false;
            if (jobTitleTV.length() == 0) {
                isEmpty = true;
                 inputLayoutJobTitle.setError("Please enter job title");
            }

            String comNameTV = "" + comNameField.getText();
            comNameTV = comNameTV.trim();

            if (comNameTV.length() == 0) {
                isEmpty = true;
                 inputLayoutCompany.setError("Please enter company name");
            }

            String indusNameTV = indusNameField.getText().toString().trim();

            if (indusNameTV.length() == 0) {
                isEmpty = true;
                 inputLayoutIndustry.setError("Please enter industry");
            } else {
                indusNameTV = URLConfig.INDUSTRY_MAP.get("" + indusNameTV);
            }

            String funAreaTV = funAreaField.getText().toString().trim();
            if (funAreaTV.length() == 0) {
                isEmpty = true;
                inputLayoutFunctionalArea.setError("Please enter functional area");
            } else {
                funAreaTV = URLConfig.FUNCTIONA_AREA_MAP.get(funAreaTV);

            }

            String expStartYearTV = expStartYearField.getText().toString()
                    .trim();

            if (expStartYearTV.length() == 0) {
                isEmpty = true;
                changeColor(expStartYearField);
             } else {
                expStartYearTV = URLConfig.YEAR_MAP.get(expStartYearTV);
            }

            String expStartMonthsTV = expStartMonthField.getText().toString()
                    .trim();

            if (expStartMonthsTV.length() == 0) {
                isEmpty = true;
                changeColor(expStartMonthField);
             } else {
                expStartMonthsTV = URLConfig.MONTH_NAME_MAP
                        .get(expStartMonthsTV);
            }

            String expEndYearTV = expEndYearField.getText().toString().trim();

            if (expEndYearTV.length() == 0 && !alertsCheck) {
                isEmpty = true;
                changeColor(expEndYearField);
             } else {
                expEndYearTV = URLConfig.YEAR_MAP.get(expEndYearTV);
            }

            String expEndMonthsTV = expEndMonthField.getText().toString()
                    .trim();

            if (expEndMonthsTV.length() == 0 && !alertsCheck) {
                isEmpty = true;
                changeColor(expEndMonthField);
             } else {
                expEndMonthsTV = URLConfig.MONTH_NAME_MAP.get(expEndMonthsTV);
            }


            String expStart =expStartMonthsTV+expStartYearTV;

            String expEnd=expEndMonthsTV+expEndYearTV;

            if(expStart.equals(expEnd) && !alertsCheck)
            {
                changeColor(expEndYearField);

                changeColor(expEndMonthField);

                DialogUtils.showErrorToast(getResources().getString(
                        R.string.start_end));
                return;
            }


            if (isEmpty) {
                DialogUtils.showErrorToast(getResources().getString(
                        R.string.required_fields));
                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            if(isAddJob){

                Type listType = new TypeToken<EmploymentDetailModel>() {}.getType();

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                        URLConfig.MY_TOTAL_JOBS_URL.replace(URLConfig.CANDIDATE_ID,userProfileVO.candidate_id), listType);
                if(alertsCheck)
                {

                    HashMap<String, String> employmentMap = new HashMap<>();

                    employmentMap.put("start_year", expStartYearTV);
                    employmentMap.put("start_month", expStartMonthsTV);
                    employmentMap.put("industry_id", indusNameTV);
                    employmentMap.put("sub_field", funAreaTV);
                    employmentMap.put("job_title", jobTitleTV);
                    employmentMap.put("company_name", comNameTV);
                    employmentMap.put("is_current", alertsCheck+"");

                    downloader.setUsePostMethod(employmentMap);
                    downloader.execute(ADD_TAG);
                }
                else
                {

                    HashMap<String, String> employmentMap = new HashMap<>();

                    employmentMap.put("start_year", expStartYearTV);
                    employmentMap.put("start_month", expStartMonthsTV);
                    employmentMap.put("end_year", expEndYearTV);
                    employmentMap.put("end_month", expEndMonthsTV);
                    employmentMap.put("industry_id", indusNameTV);
                    employmentMap.put("sub_field", funAreaTV);
                    employmentMap.put("job_title", jobTitleTV);
                    employmentMap.put("company_name", comNameTV);
                    employmentMap.put("is_current", alertsCheck+"");


                    downloader.setUsePostMethod(employmentMap);
                    downloader.execute(ADD_TAG);
                }
            }

            else
            {


                Type listType = new TypeToken<EmploymentDetailModel>() {}.getType();

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                        URLConfig.MY_TOTAL_JOBS_URL.replace(URLConfig.CANDIDATE_ID,userProfileVO.candidate_id)+empModel.getJobId()+"/", listType);

                editEmpID = empModel.getJobId();

                if(alertsCheck)
                {
                    HashMap<String, String> employmentMap = new HashMap<>();

                    employmentMap.put("start_year", expStartYearTV);
                    employmentMap.put("start_month", expStartMonthsTV);
                    employmentMap.put("industry_id", indusNameTV);
                    employmentMap.put("sub_field", funAreaTV);
                    employmentMap.put("job_id", empModel.getJobId());
                    employmentMap.put("job_title", jobTitleTV);
                    employmentMap.put("company_name", comNameTV);
                    employmentMap.put("is_current", alertsCheck+"");


                    downloader.setUsePutMethod(employmentMap);

                    downloader.execute(EDIT_TAG);
                }
                else {

                    HashMap<String, String> employmentMap = new HashMap<>();

                    employmentMap.put("start_year", expStartYearTV);
                    employmentMap.put("start_month", expStartMonthsTV);
                    employmentMap.put("end_year", expEndYearTV);
                    employmentMap.put("end_month", expEndMonthsTV);
                    employmentMap.put("industry_id", indusNameTV);
                    employmentMap.put("sub_field", funAreaTV);
                    employmentMap.put("job_id", empModel.getJobId());
                    employmentMap.put("job_title", jobTitleTV);
                    employmentMap.put("company_name", comNameTV);
                    employmentMap.put("is_current", alertsCheck+"");

                    downloader.setUsePutMethod(employmentMap);

                    downloader.execute(EDIT_TAG);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        container_box.requestFocus();
        mActivity.setActionBarVisible(false);
        mActivity.setTitle(getString(R.string.title_edit_profile));
    }


    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void OnDataResponse(final Object object, final String tag)
    {
        mActivity.runOnUiThread(new Runnable() {


            @Override
            public void run() {
            DialogUtils.dialogDismiss(dialog);

            DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(mActivity,"Employment details updated successfully" , Toast.LENGTH_SHORT).show();

                if(isAddJob){
                    ShineCommons.trackShineEvents("Add", "Employment", mActivity);

                }
                else {
                ShineCommons.trackShineEvents("Edit", "Employment", mActivity);}

                if (object != null) {
                if (GET_TAG.equals(tag)) {
                    empModel = (EmploymentDetailModel) object;
                    addUpdateProfessional();
                }
                else {
                    List<EmploymentDetailModel> list = (List<EmploymentDetailModel>) DataCaching.getInstance().getObject(mActivity, DataCaching.JOB_LIST_SECTION);

                    EmploymentDetailModel model = (EmploymentDetailModel) object;

                    ShineSharedPreferences.setExpDashFlag(mActivity, true);

                    if (ADD_TAG.equals(tag)) {
                        list.add(model);
                    } else if (EDIT_TAG.equals(tag)) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getJobId().equals(editEmpID)) {
                                editEmpID = null;
                                list.remove(i);
                                list.add(model);
                            }
                        }
                    }

                    Collections.sort(list);
                    if (list.size() > 0) {
                        UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
                        user.current_company = (list.get(0).getComName());  //first job is latest job, as we receive sorted jobs
                        ShineSharedPreferences.saveUserInfo(mActivity, user);
                    }

                    DataCaching.getInstance().cacheData(DataCaching.JOB_LIST_SECTION,
                            "" + DataCaching.CACHE_TIME_LIMIT, list, mActivity);
                    mActivity.popone();

                    Bundle bundle=new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EMPLOYMENT);

                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));

                }

        }



            }
        });
    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(dialog);
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity,
                            error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            EmploymentDetailsEditFragment.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus){
            showSpinner(v.getId());
        container_box.requestFocus();}
    }

    private void showSpinner(int id) {
        try {
            ShineCommons.hideKeyboard(mActivity);

            switch (id) {
                case R.id.alert_check_layout:
                    boolean alertsCheck = alertBoxImg.isChecked();
                    if (alertsCheck) {
                        alertBoxImg.setChecked(false);
                        expEndMonthField.setVisibility(View.VISIBLE);
                        expEndYearField.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.toyear).setVisibility(View.VISIBLE);

                    } else {

                        if (!empModel.isCurrent() && hasCurrentJob) {
                            Toast.makeText(
                                    mActivity.getApplicationContext(),
                                    "Another job is already selected as your present job.",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            alertBoxImg.setChecked(true);
                            expEndMonthField.setVisibility(View.GONE);
                            expEndYearField.setVisibility(View.GONE);
                            view.findViewById(R.id.toyear).setVisibility(View.GONE);
                        }
                    }
                    break;
                case R.id.industry_name_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_industry), indusNameField,
                            URLConfig.INDUSTRY_LIST3, mActivity);
                    break;
                case R.id.func_area_field:
                        DialogUtils.showNewMultiSpinnerDialogRadio(
                                getString(R.string.hint_func_area), funAreaField,
                                URLConfig.FUNCTIONAL_AREA_LIST2, mActivity);
                    break;
                case R.id.start_year:
                    DialogUtils.showNewSingleSpinnerRadioDialog("Start Year", expStartYearField,
                            URLConfig.YEARS_NAME_LIST, mActivity);
                    break;
                case R.id.start_month:
                    DialogUtils.showNewSingleSpinnerRadioDialog("Start Month",
                            expStartMonthField, URLConfig.MONTH_NAME_LIST,
                            mActivity);
                    break;
                case R.id.end_year:
                    DialogUtils.showNewSingleSpinnerRadioDialog("End Year", expEndYearField,
                            URLConfig.YEARS_NAME_LIST, mActivity);
                    break;
                case R.id.end_month:
                    DialogUtils.showNewSingleSpinnerRadioDialog("End Month", expEndMonthField,
                            URLConfig.MONTH_NAME_LIST, mActivity);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void changeColor(TextView text) {
         text.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_btn:
                updateProfessionalDetails();



                break;
            case R.id.cancel_btn:

            mActivity.popone();

                Bundle bundle=new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EMPLOYMENT);

                mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));

            break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
            default:
                showSpinner(v.getId());
                break;
        }
    }
}
