package com.net.shine.fragments;

/**
 * Created by Deepak on 23/08/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.FrgGridViewAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.CustomJobAlertModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class MyCustomJobAlertFragment extends BaseFragment implements
        AdapterView.OnItemClickListener, VolleyRequestCompleteListener {

    private ListView listView;
    private UserStatusModel userProfileVO;
    private View loadingView;

    private ArrayList<SimpleSearchVO> customJobAlertList = new ArrayList<>();

    public static MyCustomJobAlertFragment newInstance(){
        MyCustomJobAlertFragment fragment = new MyCustomJobAlertFragment();
        return fragment;
    }






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        mActivity.setActionBarVisible(false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.alert_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.action_add_alert:

                Bundle  bundle = new Bundle();
                SimpleSearchVO sObj = new SimpleSearchVO();
                bundle.putSerializable(URLConfig.CUSTOM_JOB_MODIFY, sObj);
                bundle.putSerializable("add_or_edit", CustomJobAlertEditFragment.ADD_JOB_ALERT);
                Fragment fragment = CustomJobAlertEditFragment.newInstance(bundle);
                fragment.setTargetFragment(MyCustomJobAlertFragment.this, 15);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;

        }
        return false;
    }

    public void updateList(CustomJobAlertModel customJobAlertModel ){


        try {

            mActivity.getSupportActionBar().show();
            SimpleSearchVO model = new SimpleSearchVO();
            model.setCustomJobAlertName(customJobAlertModel.getName());
            model.setJobId(customJobAlertModel.getId());
            model.setKeyword(customJobAlertModel.getKeywords());

            if (customJobAlertModel.getExp() != null && !customJobAlertModel.getExp().equals(""))
                model.setMinExpTag(Integer.parseInt(customJobAlertModel.getExp()));

            String temp = customJobAlertModel.getArea().toString().replaceAll("\"", "");
            model.setCustomJobAlerMinfuncAreaTag(temp);

            if (customJobAlertModel.getSal() != null && !customJobAlertModel.getSal().equals(""))
                model.setMinSalTag(Integer.parseInt(customJobAlertModel.getSal()));

            temp = customJobAlertModel.getInd().toString().replaceAll("\"", "");
            model.setCustomJobAlerIndusTypeTag(temp);

            temp = customJobAlertModel.getLocid().toString().replaceAll("\"", "");
            model.setCustomJobAlertLocTag(temp);


            for(Iterator<SimpleSearchVO> it = customJobAlertList.iterator(); it.hasNext();) {
                SimpleSearchVO s = it.next();
                if(s.getCustomJobAlertName().equals(model.getCustomJobAlertName())) {
                    it.remove();
                }

            }
            customJobAlertList.add(model);
            Collections.reverse(customJobAlertList);
            showCustomJobAlertList(customJobAlertList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.custom_job_alert_layout, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        loadingView = view.findViewById(R.id.loading_cmp);
        loadingView.setVisibility(View.VISIBLE);
        callApi();




        return view;
    }

    private void showCustomJobAlertList(
            ArrayList<SimpleSearchVO> customJobAlertList) {
        try {
            if (customJobAlertList.size() == 0) {

                DialogUtils.showErrorMsg(loadingView,
                        mActivity.getString(R.string.no_alert_default_msg), DialogUtils.ERR_NO_CUSTOM_ALERT);
                listView.setVisibility(View.GONE);

            } else {
                loadingView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

                FrgGridViewAdapter listAdapter = new FrgGridViewAdapter(this, customJobAlertList,
                        FrgGridViewAdapter.CUSTOM_JOB_ALERT_LIST_SCREEN);
                listView.setAdapter(listAdapter);
                listView.setOnItemClickListener(this);

                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listView != null)
                            ShineCommons.setListViewHeightBasedOnChildren(listView);
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

        try {


            SimpleSearchVO sVO=customJobAlertList.get(arg2);



            Log.d("CJA ","SVO "+sVO);
            String tag_val = sVO.getCustomJobAlertLocTag()
                    .replaceAll("\\[", "").replaceAll("\\]", "")
                    .replaceAll("\"", "").replace(",", "#").replaceAll(" ","");

            if(!tag_val.equals(""))
            {


                String[] i_loc = tag_val.split("#");

                for(String i: i_loc)
                {
                    Log.d("strLoc", i);
                }

                String loc_text = "";
                for (int i = 0; i < i_loc.length; i++) {

                    loc_text = loc_text
                            + URLConfig.CITY_REVERSE_MAP.get(""
                            + Integer.parseInt(i_loc[i])) + ", ";
                }
                loc_text = loc_text.substring(0, loc_text.length() - 2);


                sVO.setLocation(loc_text);
            }



//            String tag_val_func = sVO.getCustomJobAlerMinfuncAreaTag()
//                    .replaceAll("\\[", "").replaceAll("\\]", "")
//                    .replaceAll("\"", "").replace(",", "#").replaceAll(" ", "");
//            if (!tag_val_func.equals("")) {
//
//                String[] i_func = tag_val_func.split("#");
//                String func_text = "";
//                for (int i = 0; i < i_func.length; i++) {
//                    func_text = func_text
//                            + URLConfig.FUNCTIONA_AREA_REVERSE_MAP.get(""
//                            + Integer.parseInt(i_func[i])) + ", ";
//                }
//                func_text = func_text.substring(0, func_text.length() - 2);
//
//                sVO.setFuncArea(func_text);
//            }
//
//            String tag_val_indus = sVO.getCustomJobAlerIndusTypeTag()
//                    .replaceAll("\\[", "").replaceAll("\\]", "")
//                    .replaceAll("\"", "").replace(",", "#").replaceAll(" ", "");
//            if (!tag_val_indus.equals("")) {
//
//                String[] i_indus = tag_val_indus.split("#");
//                String indus_text = "";
//                for (int i = 0; i < i_indus.length; i++) {
//                    indus_text = indus_text
//                            + URLConfig.INDUSTRY_REVERSE_MAP.get(""
//                            + Integer.parseInt(i_indus[i])) + ", ";
//                }
//                indus_text = indus_text.substring(0,
//                        indus_text.length() - 2);
//                sVO.setIndusType(indus_text);
//            }

            Log.d("CJA ","SVO "+sVO);

            showSearchResultView(sVO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showSearchResultView(SimpleSearchVO sVO) {
        try {

            Bundle bundle = new Bundle();
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            BaseFragment fragment = new SearchResultFrg();
            fragment.setArguments(bundle);
            mActivity.singleInstanceReplaceFragment(fragment,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub

        super.onResume();
        mActivity.setTitle(getString(R.string.title_custom_job_alert));
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();

        ManualScreenTracker.manualTracker("MyCustomJobAlert");


    }

    public void callApi() {

        if (userProfileVO != null) {
            String URL = URLConfig.CUSTOM_JOB_ALERT_CREATE_USER.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

            Type type = new TypeToken<List<CustomJobAlertModel>>() {
            }.getType();

            Log.e("URL", "URL is " + URL);
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, MyCustomJobAlertFragment.this,
                    URL,
                    type);

            downloader.execute("CustomJobsAlert");
        } else {
            Toast.makeText(mActivity, "Sorry! you are logged out", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

            final ArrayList<CustomJobAlertModel> customJobAlertModel = (ArrayList<CustomJobAlertModel>) object;

            for (int i = 0; i < customJobAlertModel.size(); i++) {
                Log.d("custom", customJobAlertModel.get(i).toString());

            }

            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (loadingView != null)
                        loadingView.setVisibility(View.GONE);

                    customJobAlertList.clear();

                    for (int i = 0; i < customJobAlertModel.size(); i++) {
                        SimpleSearchVO model = new SimpleSearchVO();

                        model.setCustomJobAlertName(customJobAlertModel.get(i).getName());
                        model.setJobId(customJobAlertModel.get(i).getId());
                        model.setKeyword(customJobAlertModel.get(i).getKeywords());

                        if (customJobAlertModel.get(i).getExp() != null && !customJobAlertModel.get(i).getExp().equals(""))
                            model.setMinExpTag(Integer.parseInt(customJobAlertModel.get(i).getExp()));

                        String temp = customJobAlertModel.get(i).getArea().toString().replaceAll("\"", "");
                        model.setCustomJobAlerMinfuncAreaTag(temp);

                        if (customJobAlertModel.get(i).getSal() != null && !customJobAlertModel.get(i).getSal().equals(""))
                            model.setMinSalTag(Integer.parseInt(customJobAlertModel.get(i).getSal()));

                        temp = customJobAlertModel.get(i).getInd().toString().replaceAll("\"", "");
                        model.setCustomJobAlerIndusTypeTag(temp);

                        temp = customJobAlertModel.get(i).getLocid().toString().replaceAll("\"", "");
                        model.setCustomJobAlertLocTag(temp);

                        customJobAlertList.add(model);
                    }

                    showCustomJobAlertList(customJobAlertList);

                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        DialogUtils.showErrorMsg(loadingView,
                error, DialogUtils.ERR_TECHNICAL);
    }

}