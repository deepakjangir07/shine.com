package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchVO;

/**
 * Created by manishpoddar on 28/08/17.
 */

public class TopCompaniesFrg extends BaseFragment implements View.OnClickListener{

    private ImageView ibm,tcs,infosys,genpact,deloitte,amex, capgemini,interglobe;


    public static TopCompaniesFrg newInstance(){
        Bundle bundle = new Bundle();
        TopCompaniesFrg fragment = new TopCompaniesFrg();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_companies,container,false);
        ibm = (ImageView) view.findViewById(R.id.ibm);
        ibm.setOnClickListener(this);
        tcs = (ImageView) view.findViewById(R.id.tcs);
        tcs.setOnClickListener(this);
        infosys = (ImageView) view.findViewById(R.id.infosys);
        infosys.setOnClickListener(this);

        genpact = (ImageView) view.findViewById(R.id.genpact);
        genpact.setOnClickListener(this);

        deloitte = (ImageView) view.findViewById(R.id.deloitte);
        deloitte.setOnClickListener(this);

        amex = (ImageView) view.findViewById(R.id.amex);
        amex.setOnClickListener(this);

        capgemini = (ImageView) view.findViewById(R.id.capgemini);
        capgemini.setOnClickListener(this);

        interglobe = (ImageView) view.findViewById(R.id.interglobe);
        interglobe.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ibm:
                submitSearch("ibm");
                break;
            case R.id.tcs:
                submitSearch("tata consultancy services");
                break;
            case R.id.infosys:
                submitSearch("infosys");
                break;
            case R.id.genpact:
                submitSearch("genpact");
                break;
            case R.id.deloitte:
                submitSearch("deloitte");
                break;
            case R.id.amex:
                submitSearch("american express");
                break;
            case R.id.capgemini:
                submitSearch("capgemini");
                break;
            case R.id.interglobe:
                submitSearch("interglobe");
                break;

        }

    }

    private void submitSearch(String keyword){

        SimpleSearchVO sVO = new SimpleSearchVO();
        sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
        sVO.setKeyword(keyword);

        BaseFragment fragment = new SearchResultFrg();
        Bundle bundle = new Bundle();
        bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
        fragment.setArguments(bundle);
        mActivity.replaceFragmentWithBackStack(fragment);

    }

}
