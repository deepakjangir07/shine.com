package com.net.shine.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.adapters.FrgGridViewAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.FacetsModel;
import com.net.shine.models.FacetsSubModel;
import com.net.shine.models.SetFacetsModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;

public class SearchFacetsFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private TextView freshness, relevance, location, experience, salary, industry, func_area, job_type, companies;
    private FacetsModel facetModel;
    private ArrayList<FacetsSubModel> jLocation = new ArrayList<>();
    private ArrayList<FacetsSubModel> jIndustry = new ArrayList<>();
    private ArrayList<FacetsSubModel> jFunctionalArea = new ArrayList<>();
    private ArrayList<FacetsSubModel> jSalary = new ArrayList<>();
    private ArrayList<FacetsSubModel> jExprience = new ArrayList<>();
    private ArrayList<FacetsSubModel> jJobType = new ArrayList<>();
    private ArrayList<FacetsSubModel> jTopCompanies = new ArrayList<>();
    private SetFacetsModel setFacetModel;
    private SimpleSearchVO svo;
    private String sort;
    private ListView listView;
    private TextView title;
    private String facetsTitles;
    private FrgGridViewAdapter adapter;
    private ArrayList<FacetsSubModel> itemsList = new ArrayList<>();
    private boolean Check;
    private ImageView back_button;

    private ArrayList<String> facetsTitleList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mActivity.hideNavBar();
        mActivity.setActionBarVisible(false);
        view = inflater.inflate(R.layout.search_facets_frg, container, false);
        relevance = (TextView) view.findViewById(R.id.relevence_facets);
        freshness = (TextView) view.findViewById(R.id.freshness_facets);
        listView = (ListView) view.findViewById(R.id.list_view);

        back_button = (ImageView) view.findViewById(R.id.back_button);

        back_button.setOnClickListener(this);

        location = (TextView) view.findViewById(R.id.location_tv);
        location.setOnClickListener(this);
        experience = (TextView) view.findViewById(R.id.experience_tv);
        experience.setOnClickListener(this);
        salary = (TextView) view.findViewById(R.id.salary_tv);
        salary.setOnClickListener(this);
        industry = (TextView) view.findViewById(R.id.industry_tv);
        industry.setOnClickListener(this);
        func_area = (TextView) view.findViewById(R.id.fun_area_tv);
        func_area.setOnClickListener(this);
        job_type = (TextView) view.findViewById(R.id.job_type_tv);
        job_type.setOnClickListener(this);
        companies = (TextView) view.findViewById(R.id.top_companies_tv);
        companies.setOnClickListener(this);
        relevance = (TextView) view.findViewById(R.id.relevence_facets);
        relevance.setOnClickListener(this);
        freshness = (TextView) view.findViewById(R.id.freshness_facets);
        freshness.setOnClickListener(this);
        view.findViewById(R.id.refine_text1).setOnClickListener(this);
        view.findViewById(R.id.reset_button).setOnClickListener(this);
        view.findViewById(R.id.back_button).setOnClickListener(this);

        if (jLocation.size() > 0) {
            setFacetList(jLocation, "Location",true);
        }


        Bundle bundle = getArguments();


        if (setFacetModel == null) {
            if (bundle != null && getArguments().getSerializable(URLConfig.SEARCH_VO_WITH_FACETS) != null)
                setFacetModel = (SetFacetsModel) getArguments().getSerializable(URLConfig.SEARCH_VO_WITH_FACETS);
            else
                setFacetModel = new SetFacetsModel();
        }


        if (bundle != null) {
            sort = setFacetModel.getSort();
            svo = (SimpleSearchVO) bundle
                    .getSerializable(URLConfig.SEARCH_VO_KEY);
            facetModel = (FacetsModel) bundle
                    .getSerializable(URLConfig.SEARCH_FACETS_VO_KEY);


            jLocation = (ArrayList<FacetsSubModel>) facetModel.getjLocation();
            if (jLocation.size() > 0) {
                location.setVisibility(View.VISIBLE);
            }else {
                location.setVisibility(View.GONE);
            }

            jSalary = (ArrayList<FacetsSubModel>) facetModel.getjSalary();
            if (jSalary.size() > 0) {
                salary.setVisibility(View.VISIBLE);
            }
                else {
                salary.setVisibility(View.GONE);
            }

            jExprience = (ArrayList<FacetsSubModel>) facetModel.getjExperience();
            if (jExprience.size() > 0)
                experience.setVisibility(View.VISIBLE);
            else
                experience.setVisibility(View.GONE);


            jFunctionalArea = (ArrayList<FacetsSubModel>) facetModel.getjFunctionalArea();
            if (jFunctionalArea.size() > 0)
                func_area.setVisibility(View.VISIBLE);
            else
                func_area.setVisibility(View.GONE);


            jIndustry = (ArrayList<FacetsSubModel>) facetModel.getjIndustry();
            if (jIndustry.size() > 0)
                industry.setVisibility(View.VISIBLE);
            else
                industry.setVisibility(View.GONE);


            jJobType = (ArrayList<FacetsSubModel>) facetModel.getjJobType();
            if (jJobType.size() > 0)
                job_type.setVisibility(View.VISIBLE);
            else
                job_type.setVisibility(View.GONE);


            jTopCompanies = (ArrayList<FacetsSubModel>) facetModel.getjTopCompanies();
            if (jTopCompanies.size() > 0)
               companies.setVisibility(View.VISIBLE);
            else
                companies.setVisibility(View.GONE);


            if (sort.equals("1")) {
                freshness.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.blue_rectangle_button));
                freshness.setTextColor(Color.parseColor("#ffffff"));
                relevance.setBackground(null);
                relevance.setTextColor(Color.parseColor("#555555"));
            } else {
                relevance.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.blue_rectangle_button));
                relevance.setTextColor(Color.parseColor("#ffffff"));
                freshness.setBackgroundDrawable(null);
                freshness.setTextColor(Color.parseColor("#555555"));
            }

        }
        if (jLocation.size() > 0) {
            setFacetList(jLocation, "Location",true);
        }


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.location_tv:
                setFacetList(jLocation, "Location",false);
                location.setBackgroundColor(Color.parseColor("#ffffff"));
                location.setTypeface(Typeface.DEFAULT_BOLD);
                experience.setBackground(null);
                experience.setTypeface(null);
                salary.setBackground(null);
                salary.setTypeface(null);
                industry.setBackground(null);
                industry.setTypeface(null);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.experience_tv:
                setFacetList(jExprience, "Experience",false);
                experience.setBackgroundColor(Color.parseColor("#ffffff"));
                experience.setTypeface(Typeface.DEFAULT_BOLD);
                location.setBackground(null);
                location.setTypeface(null);
                salary.setBackground(null);
                salary.setTypeface(null);
                industry.setBackground(null);
                industry.setTypeface(null);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.salary_tv:
                setFacetList(jSalary, "Salary",false);
                salary.setBackgroundColor(Color.parseColor("#ffffff"));
                salary.setTypeface(Typeface.DEFAULT_BOLD);
                experience.setBackground(null);
                experience.setTypeface(null);
                location.setBackground(null);
                location.setTypeface(null);
                industry.setBackground(null);
                industry.setTypeface(null);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.industry_tv:
                setFacetList(jIndustry, "Industry",false);
                industry.setBackgroundColor(Color.parseColor("#ffffff"));
                industry.setTypeface(Typeface.DEFAULT_BOLD);
                salary.setBackground(null);
                salary.setTypeface(null);
                experience.setBackground(null);
                experience.setTypeface(null);
                location.setBackground(null);
                location.setTypeface(null);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.fun_area_tv:
                setFacetList(jFunctionalArea, "Functional Area",false);
                func_area.setBackgroundColor(Color.parseColor("#ffffff"));
                func_area.setTypeface(Typeface.DEFAULT_BOLD);
                industry.setBackground(null);
                industry.setTypeface(null);
                salary.setBackground(null);
                salary.setTypeface(null);
                experience.setBackground(null);
                experience.setTypeface(null);
                location.setBackground(null);
                location.setTypeface(null);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.job_type_tv:
                setFacetList(jJobType, "Job Type",false);
                job_type.setBackgroundColor(Color.parseColor("#ffffff"));
                job_type.setTypeface(Typeface.DEFAULT_BOLD);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                industry.setBackground(null);
                industry.setTypeface(null);
                salary.setBackground(null);
                salary.setTypeface(null);
                experience.setBackground(null);
                experience.setTypeface(null);
                location.setBackground(null);
                location.setTypeface(null);
                companies.setBackground(null);
                companies.setTypeface(null);
                break;

            case R.id.top_companies_tv:
                setFacetList(jTopCompanies, "Top Companies",false);
                companies.setBackgroundColor(Color.parseColor("#ffffff"));
                companies.setTypeface(Typeface.DEFAULT_BOLD);
                job_type.setBackground(null);
                job_type.setTypeface(null);
                func_area.setBackground(null);
                func_area.setTypeface(null);
                industry.setBackground(null);
                industry.setTypeface(null);
                salary.setBackground(null);
                salary.setTypeface(null);
                experience.setBackground(null);
                experience.setTypeface(null);
                location.setBackground(null);
                location.setTypeface(null);
                break;

            case R.id.relevence_facets:
                setFacetModel.setSort("");
                relevance.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.blue_rectangle_button));
                relevance.setTextColor(Color.parseColor("#ffffff"));
                freshness.setBackgroundDrawable(null);
                freshness.setTextColor(Color.parseColor("#000000"));
                break;

            case R.id.freshness_facets:
                setFacetModel.setSort("1");
                freshness.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.blue_rectangle_button));
                freshness.setTextColor(Color.parseColor("#ffffff"));
                relevance.setBackground(null);
                relevance.setTextColor(Color.parseColor("#000000"));
                break;

            case R.id.refine_text1:
                ShineCommons.trackShineEvents("Search", "RefineSearch", mActivity);
                setModel();
                search();
                break;

            case R.id.reset_button:
                ClearModel();
                facetsTitleList.clear();
                adapter.notifyDataSetChanged();
                break;

            case R.id.back_button:
                ClearModel();
                mActivity.popone();
                mActivity.setActionBarVisible(true);
                mActivity.showNavBar();
                facetsTitleList.clear();
                break;

            case R.id.facets:
                Check = facetsTitleList.contains(v.findViewById(
                        R.id.facets_img_checkbox).getTag());
                if (Check) {
                    Check = false;
                    CheckBox ch = (CheckBox) v.findViewById(R.id.facets_img_checkbox);
                    ch.setChecked(false);
                    facetsTitleList.remove(v.findViewById(
                            R.id.facets_img_checkbox).getTag());

                } else {
                    Check = true;
                    facetsTitleList.add((String) v.findViewById(
                            R.id.facets_img_checkbox).getTag());
                    CheckBox ch = (CheckBox) v.findViewById(R.id.facets_img_checkbox);
                    ch.setChecked(true);
                }

                setSelectedFilterCounter(false, facetsTitleList.size());
                break;

            default:
                break;
        }

    }


    private void setSelectedFilterCounter(boolean reset, int count) {


        if (facetsTitles.equals("Location") || reset) {
            if (count != 0 && !reset)
                location.setText("Location " + "( " + count + " )");
            else location.setText("Location");

        }
        if (facetsTitles.equals("Experience") || reset) {
            if (count != 0 && !reset)
                experience.setText("Experience " + "( " + count + " )");
            else
                experience.setText("Experience");
        }
        if (facetsTitles.equals("Salary") || reset) {
            if (count != 0 && !reset)
                salary.setText("Salary " + "( " + count + " )");
            else
                salary.setText("Salary");
        }
        if (facetsTitles.equals("Industry") || reset) {
            if (count != 0 && !reset)
                industry.setText("Industry " + "( " + count + " )");
            else
                industry.setText("Industry");
        }
        if (facetsTitles.equals("Functional Area") || reset) {
            if (count != 0 && !reset)
                func_area.setText("Department " + "( " + count + " )");
            else
                func_area.setText("Department");
        }

        if (facetsTitles.equals("Job Type") || reset) {
            if (count != 0 && !reset)
                job_type.setText("Job Type " + "( " + count + " )");
            else
                job_type.setText("Job Type");
        }

        if (facetsTitles.equals("Companies") || reset) {
            if (count != 0 && !reset)
                companies.setText("Top Companies " + "( " + count + " )");
            else
                companies.setText("Companies");
        }

    }

    private void setFacetList(ArrayList<FacetsSubModel> itemList,
                              String facetsTitle,boolean firstCall) {

        this.itemsList = itemList;
        this.facetsTitles = facetsTitle;
        if (facetsTitles != null) {
            getList();
        }


        adapter = new FrgGridViewAdapter(this, itemsList,
                FrgGridViewAdapter.FACETS_LIST_SCREEN, facetsTitleList);
        listView.setAdapter(adapter);
        //this.facetsTitle = facetsTitle;

        for (int i = 0; i < itemsList.size(); i++) {
            FacetsSubModel model = itemsList.get(i);
            if (facetsTitleList.contains(model.getKey())) {
                if (i > 5) {
                    listView.setSelection(i - 2);
                }
                break;
            }
        }

    }

    private void search() {
        BaseFragment fragment = new SearchResultFrg();
        Bundle bundle = new Bundle();
        bundle.putSerializable(URLConfig.SEARCH_VO_WITH_FACETS, setFacetModel);
        bundle.putSerializable("LASTFACET", facetModel);
        bundle.putSerializable(URLConfig.SEARCH_VO_KEY, svo);
        bundle.putBoolean("FROM_FACETS", true);
        fragment.setArguments(bundle);
        mActivity.popone();
        mActivity.replaceFragmentWithBackStack(fragment);

    }

    private void ClearModel() {
        setFacetModel.setExperience(new ArrayList<String>());
        setFacetModel.setFunctionalArea(new ArrayList<String>());
        setFacetModel.setIndustry(new ArrayList<String>());
        setFacetModel.setLocation(new ArrayList<String>());
        setFacetModel.setSalary(new ArrayList<String>());
        setFacetModel.setJobType(new ArrayList<String>());
        setFacetModel.setTopCompanies(new ArrayList<String>());
        setFacetModel.setSort("");

        setSelectedFilterCounter(true, 0);

        relevance.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.blue_rectangle_button));
        relevance.setTextColor(Color.parseColor("#ffffff"));
        freshness.setBackgroundDrawable(null);
        freshness.setTextColor(Color.parseColor("#000000"));
    }

    private void getList() {



        if(setFacetModel.getLocation().size()>0)
            location.setText("Location "+"("+setFacetModel.getLocation().size()+")");

        if(setFacetModel.getExperience().size()>0)
            experience.setText("Experience "+"("+setFacetModel.getExperience().size()+")");

        if(setFacetModel.getSalary().size()>0)
            salary.setText("Salary "+"("+setFacetModel.getSalary().size()+")");

        if(setFacetModel.getIndustry().size()>0)
            industry.setText("Industry "+"("+setFacetModel.getIndustry().size()+")");

        if(setFacetModel.getFunctionalArea().size()>0)
            func_area.setText("Department "+"("+setFacetModel.getFunctionalArea().size()+")");

        if(setFacetModel.getJobType().size()>0)
            job_type.setText("Job Type "+"("+setFacetModel.getJobType().size()+")");

        if(setFacetModel.getTopCompanies().size()>0)
            companies.setText("Companies "+"("+setFacetModel.getTopCompanies().size()+")");



        if (facetsTitles.equals("Location")) {
            facetsTitleList = setFacetModel.getLocation();

        }
        if (facetsTitles.equals("Experience")) {
            facetsTitleList = setFacetModel.getExperience();

        }
        if (facetsTitles.equals("Salary")) {
            facetsTitleList = setFacetModel.getSalary();

        }
        if (facetsTitles.equals("Industry")) {
            facetsTitleList = setFacetModel.getIndustry();

        }
        if (facetsTitles.equals("Functional Area")) {
            facetsTitleList = setFacetModel.getFunctionalArea();

        }

        if (facetsTitles.equals("Job Type")) {
            facetsTitleList = setFacetModel.getJobType();


        }

        if (facetsTitles.equals("Top Companies")) {
            facetsTitleList = setFacetModel.getTopCompanies();

        }

    }

    private void setModel() {
        if (facetsTitles.equals("Location")) {
            setFacetModel.setLocation(facetsTitleList);
        }
        if (facetsTitles.equals("Experience")) {
            setFacetModel.setExperience(facetsTitleList);
        }
        if (facetsTitles.equals("Salary")) {
            setFacetModel.setSalary(facetsTitleList);
        }
        if (facetsTitles.equals("Industry")) {
            setFacetModel.setIndustry(facetsTitleList);
        }
        if (facetsTitles.equals("Functional Area")) {
            setFacetModel.setFunctionalArea(facetsTitleList);
        }
        if (facetsTitles.equals("Job Type")) {
            setFacetModel.setJobType(facetsTitleList);
        }
        if (facetsTitles.equals("Top Companies")) {
            setFacetModel.setTopCompanies(facetsTitleList);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("method-called", "pause");
        mActivity.showNavBar();
        mActivity.setActionBarVisible(true);


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("method-called", "stop");

        mActivity.showNavBar();
        mActivity.setActionBarVisible(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("method-called", "destroy");

        mActivity.showNavBar();
        mActivity.setActionBarVisible(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ManualScreenTracker.manualTracker("Search Facets");
        mActivity.hideNavBar();
        mActivity.setActionBarVisible(false);
    }


}
