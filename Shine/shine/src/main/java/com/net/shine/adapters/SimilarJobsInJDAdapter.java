package com.net.shine.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.models.SimpleSearchModel;

import java.util.ArrayList;

public class SimilarJobsInJDAdapter extends JobsListAdapter {



    public SimilarJobsInJDAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list,"SimilarJobs");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = super.getView(position, convertView, viewGroup);
        TextView tv = (TextView) view.findViewById(R.id.title);
        tv.setSingleLine();

         return view;
    }
}
