package com.net.shine.activity;

import android.os.Bundle;

import com.net.shine.fragments.LinkedInMailFragment;
import com.net.shine.fragments.ShineMailFragment;

/**
 * Created by gobletsky on 29/11/16.
 */

public class SendMailActivity extends BaseActivity {

    public static final int LINKEDIN_MAIL_SCREEN = 1;
    public static final int SHINE_MAIL_SCREEN = 0;
    public static final String TO_SCREEN = "to_screen";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if(savedInstanceState==null){
            if(bundle!=null && bundle.getInt(TO_SCREEN, LINKEDIN_MAIL_SCREEN)==LINKEDIN_MAIL_SCREEN)
                setBaseFrg(LinkedInMailFragment.newInstance(getIntent().getExtras()));
            else
                setBaseFrg(ShineMailFragment.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle("Get Referral");
    }

}
