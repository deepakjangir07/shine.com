package com.net.shine.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.CustomWebView;
import com.net.shine.activity.FriendPopupActivity;
import com.net.shine.adapters.SimilarJobsInJDAdapter;
import com.net.shine.config.ServerConfig;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.AdModel;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ExpandableTextView;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.XOREncryption;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.views.BetterPopupWindow;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import static android.widget.TextView.BufferType.SPANNABLE;

/**
 * Created by ujjawal-work on 17/08/16.
 */

public class JobDetailFrg extends BaseFragment implements VolleyRequestCompleteListener, AdapterView.OnItemClickListener, GetConnectionListener, View.OnClickListener {

    public static final String FROM_SCREEN = "from_screen";

    public String ad_link = "";


    public String ad_title = "";


    public String pixelUrl=URLConfig.JD_PIXEL_URL;

    public static final int FROM_REFERRAL_NOTI = 0;
    public static final int FROM_LOGIN_TO_APPLY = 1;
    public static final int FROM_EMAIL_LINK = 2;
    public static final int FROM_LISTING = 3;
    public static final int FROM_APPLIED_JOBS_SCREEN = 4;
    public static final int FROM_WIDGET_JOBS_SCREEN = 5;
    public static final String BUNDLE_JOB_ID_KEY = "JOBID";
    public static final String BUNDLE_JOB_DATA_KEY = "job_data";
    public static final String BUNDLE_IN_JOB_MAIL_KEY = "IN_JOB_MAIL";
    public static final String BUNDLE_IN_REC_MAIL_KEY = "IN_REC_MAIL";
    private String search_keyword = "";
    private SimpleSearchModel.Result srVo;
    //Note - We are adding JD frnd_model to friend_list & unique_companies
    // This will prevent redundant conn hits if JD & similar have same company
    // When we recieve similarJobs, we are already calling invalidateWithFirendsData,
    // which helps in showing JD conn in similar
    // Also, we are using friend_list.putAll() instead in getFriends
    private Set<String> unique_companies = new HashSet<>();
    private ArrayList<String> curr_company_uuid = null;
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private boolean applied_flag = false;
    private View view;
    private TextView apply, job_by_recuiter, functional_area, industry, other_skills, other_skills_text, save_job;
    private View applied_btn;
    private ListView gridView_similar;
    private View similarJob;
    private String shareUrl;
    private int fromScreen = 0;
    private ImageView adView;
    private int i = 0;
    private ImageView backButton, shareButton;
    private LinearLayout applySaveLayout;
    private Boolean isFirstLoad = false;


    public static JobDetailFrg newInstance(SimpleSearchModel.Result job, Bundle args) {
        Bundle bundle = new Bundle();
        bundle.putAll(args);
        bundle.putSerializable(JobDetailFrg.BUNDLE_JOB_DATA_KEY, job);
        JobDetailFrg fragment = new JobDetailFrg();
        fragment.setArguments(bundle);
        return fragment;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.share_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_share:
                if (srVo == null)
                    return true;
                try {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.setType("text/plain");
                    String msg =
                            "I came across this awesome opportunity on Shine.com and thought you’ll be a great fit.\n" +
                                    "\n" +
                                    "Job Details:\n" +
                                    "\n" +
                                    "Job Title: " + srVo.jobTitle + "\n" +
                                    "Job Location: " + srVo.job_loc_str + "\n" +
                                    "Apply Here: " + shareUrl;
                    shareIntent.putExtra(Intent.EXTRA_TEXT, msg);

                    UserStatusModel userProfileVO = ShineSharedPreferences.getUserInfo(getContext());
                    if (userProfileVO != null) {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, userProfileVO.full_name + " has shared a job opportunity with you");
                    } else {
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Job opportunity");
                    }

                    List<Intent> targetedShareIntents = new ArrayList<>();
                    List<ResolveInfo> resInfo = mActivity.getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    if (!resInfo.isEmpty()) {
                        for (ResolveInfo resolveInfo : resInfo) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);

                            if (TextUtils.equals(packageName, "com.twitter.android")) {
                                String msgTwitter =
                                        "Came across this awesome opportunity on Shine.com\n" +
                                                "Apply Here: " + shareUrl;
                                targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msgTwitter);
                            } else {
                                targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msg);
                            }

                            targetedShareIntent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));


                            targetedShareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            targetedShareIntent.setType("text/plain");

                            if (userProfileVO != null) {
                                targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, userProfileVO.full_name + " has shared a job opportunity with you");
                            } else {
                                targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, "Job opportunity");
                            }
                            targetedShareIntent.setPackage(packageName);
                            targetedShareIntents.add(targetedShareIntent);
                        }
                    }
                    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), "Share Job Via ");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                    startActivity(chooserIntent);
                    ShineCommons.trackShineEvents("ShareJob", "JD", mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
        }
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        setHasOptionsMenu(false);


        mActivity.hideNavBar();
        mActivity.setActionBarVisible(false);
        mActivity.setTitle("Job detail");
        view = inflater.inflate(R.layout.jd_view_comp, container, false);


        apply = (TextView) view.findViewById(R.id.apply_btn);

        functional_area = (TextView) view.findViewById(R.id.functional_area);




        backButton = (ImageView) view.findViewById(R.id.back_button);




        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              mActivity.onBackPressed();
            }
        });
        shareButton = (ImageView) view.findViewById(R.id.share_button);




        save_job = (TextView) view.findViewById(R.id.save_job);

        save_job.setOnClickListener(this);
        shareButton.setOnClickListener(this);
        industry = (TextView) view.findViewById(R.id.industry);

        other_skills = (TextView) view.findViewById(R.id.other_skills);
        other_skills_text = (TextView) view.findViewById(R.id.other_skills_text);

        applied_btn = view.findViewById(R.id.applied_btn);
        applySaveLayout = (LinearLayout) view.findViewById(R.id.apply_save_layout);
        gridView_similar = (ListView) view
                .findViewById(R.id.grid_view_similar);
        similarJob = view.findViewById(R.id.similiar_job);
        job_by_recuiter = (TextView) view.findViewById(R.id.jobs_by_recruiter);
        job_by_recuiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchVO svo = new SimpleSearchVO();
                svo.setFromJD(true);
                svo.setRect_id(srVo.rect_id);
                svo.setKeyword(srVo.comp_name);
                showCompanyJobs(srVo.frnd_model, svo);
            }
        });
        similarJob.setOnClickListener(this);

        adView = (ImageView) view.findViewById(R.id.imageView);

        adView.setVisibility(View.GONE);

        adView.setOnClickListener(this);


        //if(JobDetailFrg.this.isVisible()) {
        //}
        view.findViewById(R.id.frnd_row).setOnClickListener(this);

        view.findViewById(R.id.applied_similar_btn).setOnClickListener(this);

        apply.setOnClickListener(this);


        try {

            Bundle bundle = getArguments();
            boolean inJobMail = bundle.getBoolean(BUNDLE_IN_JOB_MAIL_KEY);
            boolean inRecMail = bundle.getBoolean(BUNDLE_IN_REC_MAIL_KEY);
            search_keyword = bundle.getString(URLConfig.JOB_SEARCH_KEYWORD, "");
            boolean from_ref_noti = bundle.getInt(FROM_SCREEN, FROM_LISTING) == FROM_REFERRAL_NOTI;

            if (inRecMail || inJobMail) {
                srVo = new SimpleSearchModel.Result();
                srVo.jobId = bundle.getString(BUNDLE_JOB_ID_KEY, "");
                //# is used in FrgGridViewAdapter from mail if job already applied
                if (srVo.jobId.startsWith("#")) {
                    srVo.jobId = srVo.jobId.substring(1);
                    view.findViewById(R.id.applied_btn).setVisibility(
                            View.VISIBLE);
                    view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                    applySaveLayout.setVisibility(View.GONE);

                    apply.setEnabled(false);
                    apply.setOnClickListener(null);
                }
                if (URLConfig.jobAppliedSet.contains(srVo.jobId)) {
                    view.findViewById(R.id.applied_btn).setVisibility(
                            View.VISIBLE);
                    view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                    applySaveLayout.setVisibility(View.GONE);

                    apply.setEnabled(false);
                    apply.setOnClickListener(null);
                }
            } else if (from_ref_noti) {
                srVo = (SimpleSearchModel.Result) bundle.getSerializable(BUNDLE_JOB_DATA_KEY);
                view.findViewById(R.id.applied_btn).setVisibility(
                        View.VISIBLE);
                view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                applySaveLayout.setVisibility(View.GONE);

                apply.setEnabled(false);
                apply.setOnClickListener(null);
            } else {
                srVo = (SimpleSearchModel.Result) bundle.getSerializable(BUNDLE_JOB_DATA_KEY);
                if (srVo == null)
                    srVo = new SimpleSearchModel.Result();
                if (srVo.comp_uid_list != null) {
                    curr_company_uuid = srVo.comp_uid_list;
                }
                fromScreen = bundle.getInt(FROM_SCREEN, 0);
                if (srVo.is_applied || fromScreen == FROM_APPLIED_JOBS_SCREEN
                        || URLConfig.jobAppliedSet.contains(srVo.jobId)) {
                    view.findViewById(R.id.applied_btn).setVisibility(
                            View.VISIBLE);
                    view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                    applySaveLayout.setVisibility(View.GONE);

                    similarJob.setVisibility(View.GONE);
                    apply.setEnabled(false);
                    apply.setOnClickListener(null);
                }
            }

            if (srVo.job_desc == null || srVo.job_desc.equals("")) {
                downloadJD();
            } else {
                setJobData();
            }

            if ((srVo.frnd_model != null)
                    && (srVo.frnd_model.data.friends.size() > 0)) {

                //Below Logic is to prevent redundant hits if JD & similar have same company
                //add JD companyIds to unique_companies
                unique_companies.add(srVo.frnd_model.data.company_uid);
                if (friend_list == null)
                    friend_list = new HashMap<>();
                //add JD frnd_model to friend_list
                friend_list.put(srVo.frnd_model.data.company_uid, srVo.frnd_model);


                view.findViewById(R.id.frnd_row)
                        .setVisibility(View.VISIBLE);

                final TextView frndCount = (TextView) view
                        .findViewById(R.id.frndsCount);
                String name = srVo.frnd_model.data.friends.get(0).name;
                int friend_count = srVo.frnd_model.data.friends
                        .size();
                if (friend_count > 1) {

                    frndCount
                            .setText(Html
                                    .fromHtml("<font color='#555555'>"
                                            + name
                                            + "</font> + "
                                            + "<b>"
                                            + (friend_count - 1)
                                            + "</b> connections "
                                            + "<font color='#555555'>work here</font>"));
                } else if (friend_count == 1) {
                    frndCount
                            .setText(Html
                                    .fromHtml("<font color='#3B7CBC'> "
                                            + name
                                            + "</font> "
                                            + "<font color='#555555'> works here</font>"));

                }

            }

            downloadSimilar();


            if (bundle.getInt(FROM_SCREEN, FROM_LISTING) == FROM_LOGIN_TO_APPLY || bundle.getBoolean(URLConfig.APPLY_FROM_WIDGET)) {
                if (!applied_flag && !srVo.is_applied) {
                    applied_flag = true;
                    applyToJob("AutomaticLoginApply", srVo.jobId, srVo, true);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }






        Log.d("Debug::BUG", "OncreateView called for " + srVo.jobTitle);
        return view;
    }


    private void setJobData() {
        try {
            shareUrl = ServerConfig.SERVER_IP_SHINE + "/jobs/" + srVo.title_slug + "/" +
                    srVo.company_slug + "/" + srVo.jobId;
            TextView designation = (TextView) view
                    .findViewById(R.id.designation);
            designation.setText(srVo.jobTitle);
            TextView cmpName = (TextView) view.findViewById(R.id.client);
            cmpName.setText(srVo.comp_name);
            if (!TextUtils.isEmpty(srVo.rect_id)) {
                cmpName.setPaintFlags(cmpName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cmpName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SimpleSearchVO svo = new SimpleSearchVO();
                        svo.setFromJD(true);
                        svo.setRect_id(srVo.rect_id);
                        svo.setKeyword(srVo.comp_name);
                        showCompanyJobs(srVo.frnd_model, svo);
                    }
                });
            }

            TextView date = (TextView) view.findViewById(R.id.date);
            TextView venue = (TextView) view.findViewById(R.id.venue);
            LinearLayout walkin = (LinearLayout) view.findViewById(R.id.walkin);
            View divider = view.findViewById(R.id.divider);
            TextView apply = (TextView) view.findViewById(R.id.apply_btn);

            TextView experience = (TextView) view.findViewById(R.id.experience);
            final String s1 = srVo.jobExperience.trim().replace("to", " - ");
            experience.setText(s1);
            final TextView location = (TextView) view.findViewById(R.id.location);
            final TextView more = (TextView) view.findViewById(R.id.more);
            final StringTokenizer st = new StringTokenizer(srVo.job_loc_str, "/");
            if (st.countTokens() > 1) {
                final String s = st.nextToken();
                location.setText(s + " & ");
                if (android.os.Build.VERSION.SDK_INT >= 14) {
                    final LinearLayout lin = (LinearLayout) view
                            .findViewById(R.id.jd_loc_linear);
                    ViewTreeObserver vto = more.getViewTreeObserver();
                    vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        public boolean onPreDraw() {
                            // Remove after the first run so it doesn't fire
                            // forever
                            location.getViewTreeObserver()
                                    .removeOnPreDrawListener(this);
                            if (more.getLineCount() > 1) {
                                lin.setOrientation(LinearLayout.VERTICAL);
                                location.setSingleLine(true);
                                location.setText(s);
                                SpannableStringBuilder sb_more = new SpannableStringBuilder(
                                        "& more");

                                int l = sb_more.length();
                                sb_more.setSpan(new ForegroundColorSpan(
                                                Color.parseColor("#000000")), 0, 1,
                                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                sb_more.setSpan(new ForegroundColorSpan(
                                                Color.parseColor("#3E7BBE")),
                                        l - 4, l,
                                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                more.setText(sb_more);
                            }
                            return true;
                        }
                    });
                } else {
                    final LinearLayout lin = (LinearLayout) view
                            .findViewById(R.id.jd_loc_linear);
                    lin.setOrientation(LinearLayout.VERTICAL);
                    location.setSingleLine();
                    more.setSingleLine();
                }
                more.setVisibility(View.VISIBLE);
                more.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        String location_more = "";
                        String[] arr_loc = srVo.job_loc_str.split("/");
                        for (int i = 0; i < arr_loc.length; i++) {
                            if (i == (arr_loc.length - 1)) {
                                location_more = location_more
                                        + arr_loc[i].trim();
                            } else {
                                location_more = location_more
                                        + arr_loc[i].trim() + ", ";
                            }
                        }
                        showpopup(more, location_more);
                    }

                });
            } else {
                location.setText(srVo.job_loc_str);
                more.setVisibility(View.GONE);
            }


            ExpandableTextView desc = (ExpandableTextView) view.findViewById(R.id.desc);
            desc.setFocusable(true);
            desc.setFocusableInTouchMode(true);
            ExpandableTextView compDesc = (ExpandableTextView) view
                    .findViewById(R.id.company_desc);
            compDesc.setFocusable(true);
            compDesc.setFocusableInTouchMode(true);

            TextView posted = (TextView) view.findViewById(R.id.posted);
            posted.setText(srVo.posted_date);
            TextView salary = (TextView) view.findViewById(R.id.salary);
            salary.setText(srVo.jobSalary);

            if (srVo.jJobType == 2) {
                walkin.setVisibility(View.VISIBLE);
                divider.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(srVo.getVenue())) {
                    venue.setVisibility(View.VISIBLE);
                    venue.setText(Html.fromHtml("<b>Venue: </b>" + srVo.getVenue()));
                } else {
                    venue.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(srVo.getDate())) {
                    date.setVisibility(View.VISIBLE);
                    date.setText(Html.fromHtml("<b>Date: </b>" + srVo.getDate()));
                } else {
                    date.setVisibility(View.GONE);
                }
            } else {
                walkin.setVisibility(View.GONE);
                divider.setVisibility(View.GONE);
            }


            final TagView skills = (TagView) view.findViewById(R.id.skills);
            if (!TextUtils.isEmpty(srVo.job_skills)) {
                if (srVo.job_skills.contains(",")) {
                    final List<String> items = Arrays.asList(srVo.job_skills.trim().split(","));
                    Log.d("list of skills", items.size() + "");
                    if (items.size() > 0) {

                        skills.post(new Runnable() {
                            @Override
                            public void run() {
                                for (i = 0; i < items.size(); i++) {
                                    if (!TextUtils.isEmpty(items.get(i).toString().trim())) {
                                        Tag tag = new Tag(items.get(i).toString().trim() + ", ");
                                        tag.layoutBorderColor = Color.parseColor("#ffffff");
                                        tag.layoutColor = Color.WHITE;
                                        tag.tagTextColor = Color.parseColor("#2c84cc");
                                        tag.layoutColorPress = Color.parseColor("#f6f6f6");
                                        skills.addTag(tag);
                                    }
                                }
                            }
                        });

                        skills.setOnTagClickListener(new TagView.OnTagClickListener() {
                            @Override
                            public void onTagClick(Tag tag, int i) {
                                BaseFragment fragment = new SearchResultFrg();
                                SimpleSearchVO searchVO = new SimpleSearchVO();
                                searchVO.setKeyword(tag.text);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, searchVO);
                                fragment.setArguments(bundle);
                                mActivity.replaceFragmentWithBackStack(fragment);
                            }
                        });

                    }
                } else {
                    skills.post(new Runnable() {
                        @Override
                        public void run() {
                            if (!TextUtils.isEmpty(srVo.job_skills)) {
                                Tag tag = new Tag(srVo.job_skills.toString().trim());
                                tag.layoutBorderColor = Color.parseColor("#ffffff");
                                tag.layoutColor = Color.WHITE;
                                tag.tagTextColor = Color.parseColor("#2c84cc");
                                tag.layoutColorPress = Color.parseColor("#ffffff");
                                skills.addTag(tag);
                            }
                        }
                    });

                    skills.setOnTagClickListener(new TagView.OnTagClickListener() {
                        @Override
                        public void onTagClick(Tag tag, int i) {
                            BaseFragment fragment = new SearchResultFrg();
                            SimpleSearchVO searchVO = new SimpleSearchVO();
                            searchVO.setKeyword(tag.text);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, searchVO);
                            fragment.setArguments(bundle);
                            mActivity.replaceFragmentWithBackStack(fragment);
                        }
                    });
                }

            } else {
                skills.post(new Runnable() {
                    @Override
                    public void run() {

                        Tag tag = new Tag("Not Mentioned");
                        tag.layoutBorderColor = Color.parseColor("#ffffff");
                        tag.layoutColor = Color.WHITE;
                        tag.tagTextColor = Color.parseColor("#555555");
                        skills.addTag(tag);

                    }
                });

            }


            TextView cmpname = (TextView) view
                    .findViewById(R.id.company_name);
            String sb_name = "" + srVo.comp_name;

            cmpname.setText(sb_name);

            cmpname.setTypeface(null, Typeface.BOLD);

            desc.setText(Html.fromHtml(srVo.job_desc), SPANNABLE);

            System.out.println("job_desc" + desc.getText());
            System.out.println("job_desc_raw" + srVo.job_desc);

            if (!srVo.comp_desc.trim().equals("")) {
                compDesc.setText(Html.fromHtml("" + srVo.comp_desc));
            } else {
                compDesc.setVisibility(View.GONE);
            }

            if (srVo.is_applied || fromScreen == JobDetailFrg.FROM_APPLIED_JOBS_SCREEN
                    || URLConfig.jobAppliedSet.contains(srVo.jobId)) {
                view.findViewById(R.id.applied_btn).setVisibility(
                        View.VISIBLE);
                view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                applySaveLayout.setVisibility(View.GONE);

                similarJob.setVisibility(View.GONE);
                apply.setEnabled(false);
                apply.setOnClickListener(null);
            }

            if (getArguments().getInt(FROM_SCREEN, FROM_LISTING) != FROM_REFERRAL_NOTI) {
                view.findViewById(R.id.loading_cmp).setVisibility(View.GONE);
                view.findViewById(R.id.below_layout).setVisibility(View.VISIBLE);
                view.findViewById(R.id.jd_scroll_view).setVisibility(View.VISIBLE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);

        String candId="";
        if(user!=null)
            candId=user.candidate_id;

        pixelUrl= URLConfig.JD_PIXEL_URL+URLEncoder.encode("jid="+srVo.jobId+"&cname="+srVo.comp_name+"&jd="+srVo.jobTitle+"&sid="+System.currentTimeMillis()+"&cid="+candId+"&medium=Android");


        if(!isFirstLoad) {
            isFirstLoad = true;
            if(getUserVisibleHint()) {
                ShineCommons.hitJDpixelApi(mActivity, pixelUrl);
                GetCareerPlusBelowAds();
            }

        }

    }

    private String Location;

    public void showpopup(TextView view, String loc) {
        Location = loc;
        DemoPopupWindow dw = new DemoPopupWindow(view);
        dw.showLikePopDownMenu();

    }

    @Override
    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
        if (!isAdded())
            return;
        try {
            if (result != null) {
                try {
                    if (srVo.comp_uid_list != null) {
                        for (int n = 0; n < srVo.comp_uid_list.size(); n++) {
                            if (result.containsKey(srVo.comp_uid_list.get(n))) {
                                srVo.frnd_model = (result.get(srVo.comp_uid_list.get(n)));
                                view.findViewById(R.id.frnd_row).setVisibility(
                                        View.VISIBLE);
                                final TextView frndCount = (TextView) view
                                        .findViewById(R.id.frndsCount);
                                String name = result.get((srVo.comp_uid_list.get(n)))
                                        .data.friends.get(n).name;
                                int friend_count = result
                                        .get((srVo.comp_uid_list.get(n)))
                                        .data.friends.size();
                                if (friend_count > 1) {

                                    frndCount.setText(Html
                                            .fromHtml("<font color='#555555'>"
                                                    + name
                                                    + "</font> + "
                                                    + "<b>"
                                                    + (friend_count - 1)
                                                    + "</b> connections "
                                                    + "<font color='#555555'>work here</font>"));
                                } else if (friend_count == 1) {
                                    frndCount.setText(Html
                                            .fromHtml("<font color='#3B7CBC'> "
                                                    + name
                                                    + "</font> "
                                                    + "<font color='#555555'> works here</font>"));
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (friend_list == null)
                    friend_list = new HashMap<>();
                friend_list.putAll(result);
                invalidateWithFriendsData();
                if (getArguments().getInt(FROM_SCREEN, FROM_LISTING) == FROM_REFERRAL_NOTI) {
                    view.findViewById(R.id.loading_cmp).setVisibility(
                            View.GONE);
                    view.findViewById(R.id.jd_scroll_view).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.below_layout).setVisibility(View.VISIBLE);
                    if (srVo.frnd_model != null) {

                        Bundle args = new Bundle();
                        args.putSerializable("svo", srVo);
                        args.putInt("fromScreen", fromScreen);
                        Intent intent = new Intent(mActivity, FriendPopupActivity.class);
                        intent.putExtras(args);
                        mActivity.startActivity(intent);

                    }
                }
            } else {
                Log.e("TAG", "result discover null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DemoPopupWindow extends BetterPopupWindow {

        public DemoPopupWindow(View anchor) {
            super(anchor);
        }

        @Override
        protected void onCreate() {
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup root = (ViewGroup) inflater.inflate(R.layout.location_dialog, null);
            TextView tv_locations = (TextView) root.findViewById(R.id.loc_txt);
            tv_locations.setBackgroundResource(R.drawable.more_info_jd);
            tv_locations.setText(Location);
            // set the inflated view as what we want to display
            this.setContentView(root);
        }
    }

    private void downloadJD() {
        view.findViewById(R.id.jd_scroll_view).setVisibility(View.GONE);
        view.findViewById(R.id.below_layout).setVisibility(View.GONE);
        DialogUtils.showLoading(mActivity, getString(R.string.loading), view.findViewById(R.id.loading_cmp));
        String URL;
        UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
        VolleyNetworkRequest req;
        Type type = new TypeToken<SimpleSearchModel>() {
        }.getType();
        if (user != null) {
            URL = URLConfig.JOB_DETAIL_URL_LOGGED_IN.replace(URLConfig.CANDIDATE_ID, user.candidate_id);
            req = new VolleyNetworkRequest(mActivity, this,
                    URL + srVo.jobId + "/" +
                            "?fl=id,jCID,is_shortlisted,jRUrl,jPDate,jSal,jKwds,jKwdns,jArea,jInd,jCD,jCName,jJT,jJD,jCUID,jLoc,jCType,jExp,jJT_slug,jCName_slug,is_applied,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID",
                    type);
        } else {
            URL = URLConfig.JOB_DETAIL_URL_LOGGED_OUT;
            req = new VolleyNetworkRequest(mActivity, this,
                    URL + srVo.jobId + "/" +
                            "?fl=id,jCID,is_shortlisted,jRUrl,jPDate,jSal,jKwds,jKwdns,,jArea,jInd,jCD,jCName,jJT,jJD,jCUID,jLoc,jCType,jExp,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID",
                    type);
        }
        Log.d("DEBUG::ONDTRSP:", "JobDescription VolleyExecuted");
        req.execute("JobDescription");
    }


    private SimilarJobsInJDAdapter adapter_similar;

    private void downloadSimilar() {
        View similarJob = view.findViewById(R.id.similiar_job);
        similarJob.setTag(srVo.jobId);
        view.findViewById(R.id.applied_similar_btn).setTag(srVo.jobId);
        similarJob.setOnClickListener(this);
        jobList_similar = new ArrayList<>();
        SimpleSearchVO sVO = new SimpleSearchVO();
        sVO.setJobId(srVo.jobId);
        sVO.setFromSimilarJobs(true);
        if (ShineSharedPreferences.getCandidateId(mActivity) != null) {
            String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID,
                    ShineSharedPreferences.getCandidateId(mActivity));
            sVO.setUrl(URL);
        } else {
            sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
        }

        adapter_similar = new SimilarJobsInJDAdapter(mActivity, jobList_similar);
        gridView_similar.setAdapter(adapter_similar);
        gridView_similar.setOnItemClickListener(this);
        view.findViewById(R.id.loading_cmp_similar).setVisibility(
                View.VISIBLE);
        gridView_similar.setFocusable(false);
        UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
        String URL;
        if (user != null) {
            URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, user.candidate_id);
        } else {
            URL = URLConfig.SIMILAR_JOBS_URL_LOGOUT;
        }
        int numberOfResult = 3;
        Type type = new TypeToken<SimpleSearchModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                URL + "jobid=" + sVO.getJobId() + "&page=1"
                        + "&perpage=" + numberOfResult
                        + "&fl=id,jCID,jRUrl,is_applied,jCName,jCType,jJT,jKwds,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID",
                type);
        Log.d("DEBUG::ONDTRSP:", "SearchJobResult VolleyExecuted");
        req.execute("SearchJobResult");
    }

    private void callGetFriend(String comp_names) {
        GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
        friends.getFriendsInCompanyVolleyRequest(comp_names);
    }

    private void invalidateWithFriendsData() {

        view.findViewById(R.id.loading_cmp).setVisibility(
                View.GONE);

        if (!friend_list.isEmpty()) // case of company-uuid null from server

            for (int m = 0; m < jobList_similar.size(); m++) {
                SimpleSearchModel.Result job = jobList_similar.get(m);
                if (job.comp_uid_list != null) {
                    for (int n = 0; n < job.comp_uid_list.size(); n++) {
                        if (friend_list.containsKey(job.comp_uid_list.get(n))) {
                            job.frnd_model = (friend_list.get(job.comp_uid_list.get(n)));
                        }
                    }
                }

            }
        if (adapter_similar.getCount() >= 3) {
            if (similarJob != null)
                similarJob.setVisibility(View.VISIBLE);
        } else {
            if (similarJob != null)
                similarJob.setVisibility(View.GONE);
        }

        if (adapter_similar.getCount() == 0) {
            view.findViewById(R.id.similar_title).setVisibility(View.GONE);
            view.findViewById(R.id.grid_view_similar).setVisibility(View.GONE);
        }

        gridView_similar.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        int listviewElementsheight = 0;
        // not needed now as inJD is checked in onDataDownloaded
        int l = adapter_similar.getCount() > 3 ? 3 : adapter_similar.getCount();

        for (int i = 0; i < l; i++) {
            View mView = adapter_similar.getView(i, null, gridView_similar);
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            listviewElementsheight += mView.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = gridView_similar.getLayoutParams();
        params.height = listviewElementsheight;
        gridView_similar.setLayoutParams(params);
        view.findViewById(R.id.loading_cmp_similar).setVisibility(View.GONE);
        view.findViewById(R.id.grid_view_similar).setVisibility(View.VISIBLE);

        adapter_similar.rebuildListObject(jobList_similar);
        adapter_similar.notifyDataSetChanged();
        gridView_similar.invalidate();

    }


    private void applyToJob(String GAEvent, final String jobID, final SimpleSearchModel.Result svo, final boolean showPostApply) {

        ApplyJobsNew.preApplyFlow(GAEvent, jobID, svo.comp_uid_list, svo.frnd_model, mActivity, new ApplyJobsNew.onApplied() {
            @Override
            public void onApplied(String appliedResponse, Dialog plzWaitDialog) {
                srVo.is_applied = true;
                applied_btn.setVisibility(
                        View.VISIBLE);
                applySaveLayout.setVisibility(View.GONE);
                apply.setVisibility(View.GONE);
                similarJob.setVisibility(View.GONE);
                apply.setEnabled(false);
                apply.setOnClickListener(null);
                //It will be false in case of Login to Apply
                if (showPostApply) {
                    ApplyJobsNew.postApplyFlow(svo, mActivity, plzWaitDialog);
                }
            }
        }, svo);
    }


    private ArrayList<SimpleSearchModel.Result> jobList_similar;

    @Override
    public void OnDataResponse(Object object, String tag) {
        try {

            if (tag.equals("SearchJobResult")) {
                SimpleSearchModel model = (SimpleSearchModel) object;
                if (model.count > 0 && search_keyword != null && search_keyword.equals("")) {

                    for (SimpleSearchModel.Result res : model.results) {
                        int len = res.job_loc_list.size();
                        String loc = "";
                        for (int j = 0; j < len; j++) {
                            if (j >= len - 1) {
                                loc += res.job_loc_list.get(j);
                            } else {
                                loc += res.job_loc_list.get(j) + " / ";
                            }
                        }
                        res.job_loc_str = loc;
                    }

                    adapter_similar
                            .rebuildListObject(model.results);
                    adapter_similar.notifyDataSetChanged();

                    jobList_similar = model.results;
                    // we need to call it here as we need to show
                    // list view without friends too.
                    //Note: This also helps in showing connections for Jobs with same company as JD
                    invalidateWithFriendsData();

                    String co_names_similar;
                    if (srVo != null && srVo.frnd_model == null && srVo.comp_uid_list != null) {
                        co_names_similar = ShineCommons.getCompanyNames(srVo.comp_uid_list, unique_companies);
                    } else {
                        co_names_similar = "";
                    }

                    if (model.results.size() > 0) {
                        for (int i = 0; (i < model.results.size()); i++) {
                            co_names_similar = ShineCommons.getCompanyNames(model.results
                                    .get(i).comp_uid_list, unique_companies);
                        }

                    }


                    if (!co_names_similar.equals("")
                            && ShineSharedPreferences.getUserInfo(mActivity) != null)
                        callGetFriend(co_names_similar);

                    // else // invalidateWithFriendsData needed
                    // to hide and resize similar jobs
                    // invalidateWithFriendsData();
                }
                else if(model.count==0){
                    view.findViewById(R.id.similar_title).setVisibility(View.GONE);
                    view.findViewById(R.id.grid_view_similar).setVisibility(View.GONE);
                    view.findViewById(R.id.loading_cmp_similar).setVisibility(
                            View.GONE);
                }
                else {
                    //Expired job ,Redirect to JSRP page
                    if (model.count == 0 && search_keyword != null && search_keyword.length() > 0)
                        setJSRP();
                }

            }

            else if (tag.equals("saveJob")) {
                save_job.setText("Saved");
                save_job.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_saved, 0, 0, 0);
                ShineCommons.trackShineEvents("Save jobs","JD",mActivity);
                return;

            } else if (tag.equals("unsaveJob")) {
                save_job.setText("Save");
                save_job.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_save, 0, 0, 0);
                return;

            }

            else {
                if (getArguments().getInt(FROM_SCREEN, FROM_LISTING) != FROM_REFERRAL_NOTI) {
                    view.findViewById(R.id.loading_cmp).setVisibility(
                            View.GONE);
                    view.findViewById(R.id.jd_scroll_view).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.below_layout).setVisibility(View.VISIBLE);
                }

                SimpleSearchModel.Result result = ((SimpleSearchModel) object).results.get(0);
                if (result.is_shortlisted) {
                    save_job.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_saved, 0, 0, 0);
                    save_job.setText("Saved");
                } else {
                    save_job.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_save, 0, 0, 0);
                    save_job.setText("Save");
                }


                int len = result.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += result.job_loc_list.get(j);
                    } else {
                        loc += result.job_loc_list.get(j) + " / ";
                    }
                }
                result.job_loc_str = loc;
                result.job_func_area_str = result.job_area_list.toString().replace("\\", "")
                        .replace("{", "").replace("}", "").replace("\"", "").replace("[", "").replace("]", "");
                functional_area.setText(result.job_func_area_str);
                industry.setText(result.jobIndustries.trim());
                if (!TextUtils.isEmpty(result.other_skills))
                    other_skills.setText(result.other_skills.trim());
                else {
                    other_skills_text.setVisibility(View.GONE);
                    other_skills.setVisibility(View.GONE);
                }


                if (result.posted_date.length() > 10)
                    result.posted_date = result.posted_date.substring(0, 10);

                if (result.comp_desc == null || result.comp_desc.equals(""))
                    result.comp_desc = "";
                //Preserve the company we received from list
                DiscoverModel.Company backup = srVo.frnd_model;

                srVo = result;

                //Restore the company model
                srVo.frnd_model = backup;

                if (getArguments().getInt(FROM_SCREEN, FROM_LISTING) == FROM_REFERRAL_NOTI) {
                    //Here srVo.frnd_model will be null
                    //Create a Hit for JD Connection
                    String co_names = ShineCommons.getCompanyNames(srVo.comp_uid_list, unique_companies);
                    if (!co_names.isEmpty()) {
                        callGetFriend(co_names);
                        //Empty co_names means a hit for JD Conn is already sent by similar Job Conn
                        //because they had same company
                    }
                }

                if (srVo.is_applied) {
                    view.findViewById(R.id.applied_btn).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.apply_btn).setVisibility(View.GONE);
                    applySaveLayout.setVisibility(View.GONE);
                    similarJob.setVisibility(View.VISIBLE);
                    View apply = view.findViewById(R.id.apply_btn);
                    apply.setEnabled(false);
                    apply.setOnClickListener(null);
                }
                setJobData();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        view.findViewById(R.id.jd_scroll_view).setVisibility(View.GONE);
                        view.findViewById(R.id.below_layout).setVisibility(View.GONE);
                        DialogUtils.showErrorMsg(
                                view.findViewById(R.id.loading_cmp), error, DialogUtils.ERR_TECHNICAL);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setJSRP() {
        SimpleSearchVO sVO = new SimpleSearchVO();
        sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
        sVO.setKeyword(search_keyword);
        Bundle bundle = new Bundle();
        bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
        bundle.putBoolean("fromExpiredJob", true);
        BaseFragment fragment = new SearchResultFrg();
        fragment.setArguments(bundle);
        mActivity.replaceFragment(fragment);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle2 = new Bundle();
        bundle2.putInt(URLConfig.INDEX, position);
        bundle2.putSerializable(JobDetailParentFrg.JOB_LIST, jobList_similar);
        BaseFragment fragment = new JobDetailParentFrg();
        fragment.setArguments(bundle2);
        mActivity.singleInstanceReplaceFragment(fragment,true);
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.apply_btn:
                    //apply from JD below btn
                    if (srVo.comp_uid_list != null)      //workaround to fix company array sometimes null error
                        curr_company_uuid = srVo.comp_uid_list;
                    else if (curr_company_uuid != null)
                        srVo.comp_uid_list = curr_company_uuid;
                    applyToJob("JD", srVo.jobId, srVo, true);
                    break;

                case R.id.save_job:
                    if (save_job.getText().toString().trim().equalsIgnoreCase("save")){
                        Log.d("save_job>>>>>>","saving");

                        saveJob();
                    }
                    else if(save_job.getText().toString().trim().equalsIgnoreCase("saved")){
                        Log.d("unsave_job>>>>>>","unsaving");
                        save_job.setText("Please wait...");
                        unSaveJob();
                    }
                    break;

                case R.id.similiar_job:
                    showSearchResultView("" + srVo.jobId);

                    ShineCommons.trackShineEvents("View SimilarJobs", "JD-ViewMore", mActivity);

                    break;


                case R.id.applied_similar_btn:

                    showSearchResultView("" + srVo.jobId);


                    ShineCommons.trackShineEvents("View SimilarJobs", "JD-ViewSimilarInApplyButton", mActivity);


                    break;

                case R.id.frnd_row:

                    DiscoverModel.Company dfm = srVo.frnd_model;
                    if (dfm != null) {
                        Log.d("DEBUG::TEMP", "dfm not null jd");
                        Bundle args = new Bundle();
                        args.putSerializable("svo", srVo);
                        args.putInt("fromScreen", fromScreen);
                        Intent intent = new Intent(mActivity, FriendPopupActivity.class);
                        intent.putExtras(args);
                        mActivity.startActivity(intent);
                        ShineCommons.trackShineEvents("KonnectFriendList", "JD", mActivity);
                    } else {
                        Log.d("DEBUG::TEMP", "dfm null jd");
                    }

                    break;


                case R.id.imageView:

                    try {
                        if (!TextUtils.isEmpty(ad_link)) {
                            String appendUserData = "";
                            String encrypted_text = "";
                            String finalLink = "";

                            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);


                            String date_time_value = KonnectFrag.getDate(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");


                            if (ad_link.contains("APP_SCREEN_NAME")) {
                                finalLink = ad_link.replace("APP_SCREEN_NAME", "Job_Description");

                            }

                            if (user != null) {
                                appendUserData = URLConfig.key + "|" + user.email + "|" + user.mobile_no + "|" + date_time_value;

                                encrypted_text = XOREncryption.encryptDecrypt(appendUserData);

                                Uri uri = Uri.parse(finalLink);
                                String paramValue = uri.getQueryParameter("next");
                                if(paramValue.contains("?")||paramValue.contains("3F")) {
                                    finalLink = finalLink +  URLEncoder.encode("&ad_content="+encrypted_text);
                                }
                                else {
                                    finalLink = finalLink  + URLEncoder.encode("?ad_content="+encrypted_text);
                                }
                            }

                            if (ShineCommons.appInstalledOrNot("com.android.chrome")) {
                                System.out.println("-- package install");
                                ChromeCustomTabs.openCustomTab(mActivity, finalLink);
                            } else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity, CustomWebView.class);
                                bundle.putString("shineurl", finalLink);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);

                            }

                            ShineCommons.trackShineEvents("CareerPlusAdClick", "Job_Description", mActivity);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case R.id.share_button:
                    try {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        shareIntent.setType("text/plain");
                        String msg =
                                "I came across this awesome opportunity on Shine.com and thought you’ll be a great fit.\n" +
                                        "\n" +
                                        "Job Details:\n" +
                                        "\n" +
                                        "Job Title: " + srVo.jobTitle + "\n" +
                                        "Job Location: " + srVo.job_loc_str + "\n" +
                                        "Apply Here: " + shareUrl;
                        shareIntent.putExtra(Intent.EXTRA_TEXT, msg);

                        UserStatusModel userProfileVO = ShineSharedPreferences.getUserInfo(getContext());
                        if (userProfileVO != null) {
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, userProfileVO.full_name + " has shared a job opportunity with you");
                        } else {
                            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Job opportunity");
                        }

                        List<Intent> targetedShareIntents = new ArrayList<>();
                        List<ResolveInfo> resInfo = mActivity.getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);
                        if (!resInfo.isEmpty()) {
                            for (ResolveInfo resolveInfo : resInfo) {
                                String packageName = resolveInfo.activityInfo.packageName;
                                Intent targetedShareIntent = new Intent(android.content.Intent.ACTION_SEND);

                                if (TextUtils.equals(packageName, "com.twitter.android")) {
                                    String msgTwitter =
                                            "Came across this awesome opportunity on Shine.com\n" +
                                                    "Apply Here: " + shareUrl;
                                    targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msgTwitter);
                                } else {
                                    targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msg);
                                }

                                targetedShareIntent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));


                                targetedShareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                targetedShareIntent.setType("text/plain");

                                if (userProfileVO != null) {
                                    targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, userProfileVO.full_name + " has shared a job opportunity with you");
                                } else {
                                    targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, "Job opportunity");
                                }
                                targetedShareIntent.setPackage(packageName);
                                targetedShareIntents.add(targetedShareIntent);
                            }
                        }
                        Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), "Share Job Via ");
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                        startActivity(chooserIntent);
                        ShineCommons.trackShineEvents("ShareJob", "JD", mActivity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    private void showSearchResultView(String jobId) {
        SimpleSearchVO sVO = new SimpleSearchVO();
        sVO.setJobId(jobId);
        sVO.setFromSimilarJobs(true);
        sVO.setHideModifySearchButton(true);
        if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
            String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
            sVO.setUrl(URL);
        } else {
            sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
        BaseFragment fragment = new SearchResultFrg();
        fragment.setArguments(bundle);
        mActivity.replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();

        mActivity.hideNavBar();
        mActivity.setActionBarVisible(false);
        mActivity.setTitle("Job detail");
        if(getUserVisibleHint()) {
            if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
                ManualScreenTracker.manualTracker("JobDetails-LoggedIn");
            } else {
                ManualScreenTracker.manualTracker("JobDetails-NonLoggedIn");
            }
        }

    }


    public void GetCareerPlusBelowAds() {

        final UserStatusModel userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

        String url = "";
        if (userProfileVO != null) {
            url = URLConfig.BELOW_AD_URL_LOGGEDIN.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
        } else {
            url = URLConfig.BELOW_AD_URL_LOGOUT;
        }

        Type type = new TypeToken<AdModel>() {
        }.getType();

        VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {

                final AdModel adModel = (AdModel) object;

                if (adModel != null && adModel.results.size() > 0) {
                    adView.setVisibility(View.VISIBLE);

                    if (URLConfig.BELOW_AD >= adModel.results.size()) {
                        URLConfig.BELOW_AD = 0;

                    }
                    try {
                        Glide.with(mActivity).load(adModel.results.get(URLConfig.BELOW_AD).adPath).listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model1, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model1, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {


                                try {

                                    if (adModel.results.size() > URLConfig.BELOW_AD) {
                                        String url = adModel.results.get(URLConfig.BELOW_AD).impressionUrl;



                                        if (!TextUtils.isEmpty(url)) {
                                            url = url.replace("App", "Android").replace("APP_SCREEN_NAME", "Job_Description");

                                            if (getUserVisibleHint()) {

                                                ShineCommons.hitImpressionApi(mActivity, url);

//                                                ShineCommons.trackShineEvents("CareerPlusAdImpression", "Job_Description", mActivity);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                return false;
                            }
                        }).into(adView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ad_link = adModel.results.get(URLConfig.BELOW_AD).clickUrl;
                    ad_title=adModel.results.get(URLConfig.BELOW_AD).adTitle;

                } else {
                    adView.setVisibility(View.GONE);

                }
            }

            @Override
            public void OnDataResponseError(String error, String tag) {

                Log.d("ADS", "error " + error);

            }
        }, url, type);

        request.execute("BELOWAD");

    }


    @Override
    public void onPause() {
        super.onPause();
    }

    public void saveJob() {
        if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
            save_job.setText("saving...");
            String url = URLConfig.SAVE_JOB_URL.replace(URLConfig.CANDIDATE_ID,
                    ShineSharedPreferences.getUserInfo(mActivity).candidate_id);
            Type type = new TypeToken<SimpleSearchModel>() {
            }.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, url, type);
            HashMap<String, String> saveMap = new HashMap<>();
            saveMap.put("job_id", srVo.jobId);
            saveMap.put("status", "0");
            downloader.setUsePostMethod(saveMap);
            downloader.execute("saveJob");
        } else {

           Toast.makeText(mActivity, "Please login to save job", Toast.LENGTH_SHORT).show();
        }
    }

    public void unSaveJob() {

        if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
            Log.d("unsave_job","click");
            String url = URLConfig.UNSAVE_JOB_URL.replace(URLConfig.CANDIDATE_ID,
                    ShineSharedPreferences.getUserInfo(mActivity).candidate_id).replace(URLConfig.JOB_ID, srVo.jobId);
            Type type = new TypeToken<SimpleSearchModel>() {
            }.getType();

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, url, type);
            downloader.setUseDeleteMethod();
            downloader.execute("unsaveJob");
        }

    }


    public void showCompanyJobs(DiscoverModel.Company row, SimpleSearchVO svo) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("companyModel", row);
        bundle.putSerializable("companyIdObject", svo);
        bundle.putBoolean("fromDiscoverConnect", true);
        BaseFragment fragment = new SearchResultFrg();
        fragment.setArguments(bundle);
        mActivity.replaceFragmentWithBackStack(fragment);
    }




    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.");
            }
            else
            {

                if (getUserVisibleHint()) {

                    GetCareerPlusBelowAds();
                    ShineCommons.hitJDpixelApi(mActivity, pixelUrl);
                }

            }
        }
    }

}
