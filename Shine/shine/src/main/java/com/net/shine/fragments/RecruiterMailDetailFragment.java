package com.net.shine.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.models.RecruiterMailDetailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RecruiterMailDetailFragment extends BaseFragment implements
		OnClickListener, VolleyRequestCompleteListener {
	private View view, LoadingCmp;
	private UserStatusModel userProfileVO;
	private InboxAlertMailModel.Results mailVo;
	private View apply;
	private View errorLayout;
	private Dialog alertDialog;
	private LinearLayout mainContainer;

	private String RECRUITERMAILDETAILS = "RecruiterMailDetails";
	private String RECRUITERMAILDETAILSAPPLY = "RecruiterMailDetailsApply";

	private RecruiterMailDetailModel recMailVo;
	public static final String JOB_ALERT_DATA_KEY = "job_alert_data";



	public static RecruiterMailDetailFragment newInstance(boolean isFromNoti) {

        Bundle args = new Bundle();
        args.putBoolean("isFromNoti", isFromNoti);
        RecruiterMailDetailFragment fragment = new RecruiterMailDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		try {
			view = inflater.inflate(R.layout.recruiter_mail_detail_frg, container, false);
			LoadingCmp = view.findViewById(R.id.loading_cmp);
			mainContainer = (LinearLayout) view.findViewById(R.id.main_container);
			errorLayout  = view.findViewById(R.id.error_layout);
			errorLayout.setVisibility(View.GONE);
			userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
			Bundle bundle = getArguments();
			mailVo = (InboxAlertMailModel.Results) bundle.getSerializable(JOB_ALERT_DATA_KEY);

			if(!TextUtils.isEmpty(mailVo.subject_line))
			{
				TextView subject = (TextView) view.findViewById(R.id.subject);
				subject.setText(mailVo.subject_line);

			}
			if(!TextUtils.isEmpty(mailVo.recruiter_name))
			{
				TextView sender = (TextView) view.findViewById(R.id.sender);
				sender.setText("From: " + mailVo.recruiter_name);
			}



			downloadMailDetail();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}

	private void downloadMailDetail() {
		try {
			DialogUtils.showLoading(mActivity, getString(R.string.loading),
                    LoadingCmp);

			Type listType = new TypeToken<RecruiterMailDetailModel>() {}.getType();

			VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,this,
					URLConfig.RECRUITER_MAIL_DETAIL_URL.replace(URLConfig.CANDIDATE_ID,userProfileVO.candidate_id)
							.replace(URLConfig.ID2,mailVo.hash_code), listType);

            downloader.execute(RECRUITERMAILDETAILS);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		mActivity.setTitle(getString(R.string.title_recruiter_mails));
		mActivity.setActionBarVisible(true);
		mActivity.showNavBar();
		mActivity.showBackButton();


		if(getUserVisibleHint()) {
			ManualScreenTracker.manualTracker("RecruiterMailDetail");
			setUserVisibleHint(false);
		}

	}


	private void showMailDetail() {
		try {
			LoadingCmp.setVisibility(View.GONE);
			mainContainer.setVisibility(View.VISIBLE);
			TextView jobtitle = (TextView) view.findViewById(R.id.job_title);
			TextView experience = (TextView) view.findViewById(R.id.experience);
			TextView salary = (TextView) view.findViewById(R.id.salary);
			TextView location = (TextView) view.findViewById(R.id.location);
			TextView subject = (TextView) view.findViewById(R.id.subject);
			TextView sender = (TextView) view.findViewById(R.id.sender);
			TextView mailBody = (TextView) view.findViewById(R.id.mail_body);
			TextView applyText = (TextView) view.findViewById(R.id.apply_text1);
			TextView recruiterName = (TextView) view.findViewById(R.id.recruiter_name);
			TextView dateNumber = (TextView) view.findViewById(R.id.date_number);
			TextView monthName = (TextView) view.findViewById(R.id.month);
			LinearLayout imageContainer = (LinearLayout) view.findViewById(R.id.image_layout);
			applyText.setText("Apply Now");

			apply = view.findViewById(R.id.apply_btn);
			apply.setOnClickListener(this);

			if (recMailVo.contextData.applyButton!=0) {
				apply.setVisibility(View.VISIBLE);
			}

			if (!TextUtils.isEmpty(recMailVo.contextData.jobTitle)) {
				jobtitle.setText(recMailVo.contextData.jobTitle);
			} else {
				view.findViewById(R.id.job_title).setVisibility(View.GONE);
			}


			if(!TextUtils.isEmpty(recMailVo.recName)){
				recruiterName.setText(recMailVo.recName);
			}
			else {
				recruiterName.setVisibility(View.GONE);
			}



			if(!TextUtils.isEmpty(recMailVo.contextData.maxSalary)&&
					!TextUtils.isEmpty(recMailVo.contextData.minSalary)){
				salary.setText(recMailVo.contextData.minSalary+" - "+recMailVo.contextData.maxSalary);
			}
			else if(TextUtils.isEmpty(recMailVo.contextData.maxSalary)&&
					!TextUtils.isEmpty(recMailVo.contextData.minSalary)){
				salary.setText(recMailVo.contextData.minSalary);
			}
			else if(!TextUtils.isEmpty(recMailVo.contextData.maxSalary)&&
					TextUtils.isEmpty(recMailVo.contextData.minSalary)){
				salary.setText(recMailVo.contextData.maxSalary);
			}
			else {
				salary.setVisibility(View.GONE);
			}


			if(!TextUtils.isEmpty(recMailVo.contextData.maxExp)&&
					!TextUtils.isEmpty(recMailVo.contextData.minexp)){
				experience.setText(recMailVo.contextData.minexp+" - "+recMailVo.contextData.maxExp);
			}
			else if(TextUtils.isEmpty(recMailVo.contextData.maxExp)&&
					!TextUtils.isEmpty(recMailVo.contextData.minexp)){
				experience.setText(recMailVo.contextData.minexp);
			}
			else if(!TextUtils.isEmpty(recMailVo.contextData.maxExp)&&
					TextUtils.isEmpty(recMailVo.contextData.minexp)){
				experience.setText(recMailVo.contextData.maxExp);
			}
			else {
				experience.setVisibility(View.GONE);
			}


			if (!TextUtils.isEmpty(recMailVo.contextData.location)) {
				location.setText(recMailVo.contextData.location);
			} else {
				view.findViewById(R.id.location).setVisibility(View.GONE);
			}


			String date = ShineCommons.getFormattedDate(recMailVo.timestamp.substring(0, 10));
			String date_number = date.substring(0, 2);
			String month = "";
			if (date_number != null && date_number.trim().length() == 1) {
				month = date.substring(2, 6).toUpperCase();

				date_number = "0" + date_number;
			} else {

				month = date.substring(3, 6).toUpperCase();
			}
			Log.d("date_inbox", date_number + " " + month);

			dateNumber.setText(date_number);
			monthName.setText(month);
			ColorGenerator generator = ColorGenerator.MATERIAL;
			int color = generator.getColor(date_number);
			TextDrawable drawable = TextDrawable.builder()
					.buildRect("", color);
			imageContainer.setBackground(drawable);

			subject.setText(recMailVo.subjectline);
			sender.setText("From: " + recMailVo.recName);
			mailBody.setText(Html.fromHtml(recMailVo.contextData.emailbody));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.apply_btn:
					if (recMailVo.contextData.existingjobs.equals("")) {
						apply();

					} else {
						goToJDPage();

					}

				break;
			case R.id.ok_btn:
				try {
					if (alertDialog != null && alertDialog.isShowing()) {
						alertDialog.dismiss();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			default:
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void goToJDPage() {
		try {
			Bundle bundle = new Bundle();
			
			bundle.putBoolean("IN_REC_MAIL", true);
			bundle.putString("JOBID", recMailVo.contextData.existingjobs);
			JobDetailFrg frg = new JobDetailFrg();
			frg.setArguments(bundle);
			mActivity.singleInstanceReplaceFragment(frg);

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void apply() {

		DialogUtils.dialogDismiss(alertDialog);
		alertDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");

		Type listType = new TypeToken<EmptyModel>() {}.getType();

		VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
				URLConfig.INBOX_APPLY_URL.replace(URLConfig.CANDIDATE_ID,userProfileVO.candidate_id), listType);

		HashMap mailMap = new HashMap();

		mailMap.put("candidate_id", userProfileVO.candidate_id);
		mailMap.put("subject_line", recMailVo.subjectline);
		mailMap.put("recruiter_email", recMailVo.contextData.recMail);
		mailMap.put("recruiter_name", recMailVo.recName);

		downloader.setUsePostMethod(mailMap);
		
		downloader.execute(RECRUITERMAILDETAILSAPPLY);

	}

	@Override
	public void OnDataResponse(final Object object, final String tag) {
		try {

			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {

					try {

						LoadingCmp.setVisibility(View.GONE);
						DialogUtils.dialogDismiss(alertDialog);
						errorLayout.setVisibility(View.GONE);
							if (tag.equals(RECRUITERMAILDETAILSAPPLY)) {
								alertDialog = DialogUtils
										.showCustomAlertsNoTitle(mActivity,
												"Response Sent, Your Response has been sent to the Recruiter.");

								ShineCommons.trackShineEvents("Apply","RecruiterMailDetail",mActivity);

								alertDialog
										.findViewById(R.id.ok_btn)
										.setOnClickListener(
												RecruiterMailDetailFragment.this);
							}
							if (tag.equals(RECRUITERMAILDETAILS)) {

								RecruiterMailDetailModel mailDetailModel = (RecruiterMailDetailModel) object;

								recMailVo = mailDetailModel;
								showMailDetail();
							}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void OnDataResponseError(final String error, String tag) {
		try {

			LoadingCmp.setVisibility(View.GONE);
			mainContainer.setVisibility(View.GONE);
			DialogUtils.dialogDismiss(alertDialog);
			errorLayout.setVisibility(View.VISIBLE);
			DialogUtils.showErrorActionableMessage(mActivity,errorLayout,URLConfig.PARSING_ERROR_MSG,
					"","",DialogUtils.ERR_NO_EMAIL,null);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
