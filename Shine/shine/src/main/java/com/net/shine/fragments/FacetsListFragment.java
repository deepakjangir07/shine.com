//package com.net.shine.fragments;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentManager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.net.shine.R;
//import com.net.shine.adapters.FrgGridViewAdapter;
//import com.net.shine.models.FacetsSubModel;
//import com.net.shine.models.SerializableArrayList;
//import com.net.shine.models.SetFacetsModel;
//
//import java.util.ArrayList;
//
//public class FacetsListFragment extends BaseFragment implements View.OnClickListener{
//	private View view;
//	private ListView listView;
//	private TextView title;
//	private String facetsTitle;
//	private FrgGridViewAdapter adapter;
//	private ArrayList<FacetsSubModel> itemList = new ArrayList<>();
//	private boolean Check;
//	private ArrayList<String> facetsTitleList;
//	private SetFacetsModel setFacetsModel;
//
//	public static FacetsListFragment newInstance(ArrayList<FacetsSubModel> list, String title, SetFacetsModel model) {
//
//		Bundle args = new Bundle();
//        args.putSerializable("setFacetsModel", model);
//        args.putString("facetsTitle", title);
//        args.putSerializable("itemList", new SerializableArrayList<>(list));
//		FacetsListFragment fragment = new FacetsListFragment();
//		fragment.setArguments(args);
//		return fragment;
//	}
//
//	@Override
//	public void onCreate(@Nullable Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		SerializableArrayList ser_list = (SerializableArrayList) getArguments().getSerializable("itemList");
//        if (ser_list != null) {
//            this.itemList = ser_list.getOriginal();
//        }
//        this.facetsTitle = getArguments().getString("facetsTitle");
//		this.setFacetsModel = (SetFacetsModel) getArguments().getSerializable("setFacetsModel");
//
//	}
//
//
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		view = inflater.inflate(R.layout.facet_list_fragment, container, false);
//		title = (TextView) view.findViewById(R.id.title_facets);
//		title.setText(facetsTitle);
//		if(facetsTitle!=null){
//		getList();
//		}
//
//		listView = (ListView) view.findViewById(R.id.list_view);
//		view.findViewById(R.id.done_text1).setOnClickListener(this);
//		view.findViewById(R.id.reset_text2).setOnClickListener(this);
//
//		adapter = new FrgGridViewAdapter(this, itemList,
//				FrgGridViewAdapter.FACETS_LIST_SCREEN,facetsTitleList,FacetsListFragment.this);
//		listView.setAdapter(adapter);
//
//        for(int i=0;i<itemList.size();i++)
//        {
//            FacetsSubModel model = itemList.get(i);
//            if(facetsTitleList.contains(model.getKey())) {
//                if (i > 5) {
//                    listView.setSelection(i - 2);
//                }
//                break;
//            }
//        }
//
//
//		title.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//                FragmentManager fm = mActivity.getSupportFragmentManager();
//                fm.popBackStack();
//			}
//		});
//
//		// TODO Auto-generated method stub
//		return view;
//	}
//
//	@Override
//	public void onClick(View v) {
//		// TODO Auto-generated method stub
//		switch (v.getId()) {
//		case R.id.done_text1:
//			setModel();
//			System.out.println(setFacetsModel.getSalary());
//			FragmentManager fm = mActivity.getSupportFragmentManager();
//			fm.popBackStack();
//			break;
//
//		case R.id.reset_text2:
//			facetsTitleList.clear();
//			adapter.notifyDataSetChanged();
//			break;
//		case R.id.facets:
//            Check = facetsTitleList.contains(v.findViewById(
//                    R.id.facets_img_checkbox).getTag());
//			if (Check) {
//				Check = false;
//                CheckBox ch = (CheckBox) v.findViewById(R.id.facets_img_checkbox);
//                ch.setChecked(false);
//				facetsTitleList.remove(v.findViewById(
//						R.id.facets_img_checkbox).getTag());
//
//			} else {
//				Check = true;
//				facetsTitleList.add((String) v.findViewById(
//                        R.id.facets_img_checkbox).getTag());
//                CheckBox ch = (CheckBox) v.findViewById(R.id.facets_img_checkbox);
//                ch.setChecked(true);
//			}
//			break;
//		default:
//			break;
//		}
//	}
//	private void getList() {
//		if (facetsTitle.equals("Location")) {
//			facetsTitleList= setFacetsModel.getLocation();
//		}
//		if (facetsTitle.equals("Experience")) {
//			facetsTitleList= setFacetsModel.getExperience();
//		}
//		if (facetsTitle.equals("Salary")) {
//            facetsTitleList= setFacetsModel.getSalary();
//		}
//		if (facetsTitle.equals("Industry")) {
//			facetsTitleList= setFacetsModel.getIndustry();
//		}
//		if (facetsTitle.equals("Functional Area")) {
//			facetsTitleList= setFacetsModel.getFunctionalArea();
//		}
//
//		if(facetsTitle.equals("Job Type"))
//		{
//			facetsTitleList=setFacetsModel.getJobType();
//
//		}
//
//		if(facetsTitle.equals("Top Companies"))
//		{
//			facetsTitleList=setFacetsModel.getTopCompanies();
//		}
//
//	}
//	private void setModel() {
//		if (facetsTitle.equals("Location")) {
//			setFacetsModel.setLocation(facetsTitleList);
//		}
//		if (facetsTitle.equals("Experience")) {
//			setFacetsModel.setExperience(facetsTitleList);
//		}
//		if (facetsTitle.equals("Salary")) {
//			setFacetsModel.setSalary(facetsTitleList);
//		}
//		if (facetsTitle.equals("Industry")) {
//			setFacetsModel.setIndustry(facetsTitleList);
//		}
//		if (facetsTitle.equals("Functional Area")) {
//			setFacetsModel.setFunctionalArea(facetsTitleList);
//		}
//		if (facetsTitle.equals("Job Type")) {
//			setFacetsModel.setJobType(facetsTitleList);
//		}
//		if (facetsTitle.equals("Top Companies")) {
//			setFacetsModel.setTopCompanies(facetsTitleList);
//		}
//
//	}
//}
