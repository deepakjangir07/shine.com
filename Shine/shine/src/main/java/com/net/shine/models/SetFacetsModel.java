package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

public class SetFacetsModel implements Serializable{

	public SetFacetsModel(){}

    public SetFacetsModel copy(){
        SetFacetsModel model = new SetFacetsModel();
        model.getSalary().addAll(Salary);
        model.getIndustry().addAll(Industry);
        model.getFunctionalArea().addAll(FunctionalArea);
        model.getExperience().addAll(Experience);
        model.getLocation().addAll(Location);
        model.getJobType().addAll(JobType);
        model.getTopCompanies().addAll(TopCompanies);
        model.sort = sort;
        return model;
    }



	private String sort ="";
	private ArrayList<String> Salary=new ArrayList<String>();
	private ArrayList<String> Industry=new ArrayList<String>();
	private ArrayList<String> FunctionalArea=new ArrayList<String>();
	private ArrayList<String> Experience=new ArrayList<String>();
	private ArrayList<String> Location=new ArrayList<String>() ;
	private ArrayList<String> JobType=new ArrayList<String>() ;
	private ArrayList<String> TopCompanies=new ArrayList<String>() ;

	public ArrayList<String> getJobType() {
		return JobType;
	}

	public void setJobType(ArrayList<String> jobType) {
		JobType = jobType;
	}

	public ArrayList<String> getTopCompanies() {
		return TopCompanies;
	}

	public void setTopCompanies(ArrayList<String> topCompanies) {
		TopCompanies = topCompanies;
	}

	public ArrayList<String> getLocation() {
		return Location;
	}
	public void setLocation(ArrayList<String> location) {
		Location = location;
	}
	public ArrayList<String> getSalary() {
		return Salary;
	}
	public void setSalary(ArrayList<String> salary) {
		Salary = salary;
	}
	public ArrayList<String> getIndustry() {
		return Industry;
	}
	public void setIndustry(ArrayList<String> industry) {
		Industry = industry;
	}
	public ArrayList<String> getFunctionalArea() {
		return FunctionalArea;
	}
	public void setFunctionalArea(ArrayList<String> functionalArea) {
		FunctionalArea = functionalArea;
	}
	public ArrayList<String> getExperience() {
		return Experience;
	}
	public void setExperience(ArrayList<String> experience) {
		Experience = experience;
	}
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}




}
