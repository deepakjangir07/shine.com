package com.net.shine.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.KonnectFrag;
import com.net.shine.models.AdModel;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.XOREncryption;

import java.net.URLEncoder;
import java.util.ArrayList;


public class InboxAlertsListAdapter extends BaseAdapter {


    private BaseActivity mActivity;
    private ArrayList<InboxAlertMailModel.Results> mList;
    public static int LIST_ITEM = 0;
    public static int AD_ITEM = 1;
    LayoutInflater li;
    private boolean is_impression_marked = false;
    private String add_link = "";
    boolean jobAlert = true;
    public InboxAlertsListAdapter(BaseActivity activity, ArrayList<InboxAlertMailModel.Results> list, boolean jobAlert) {

        this.mActivity = activity;
        this.mList = list;
        this.jobAlert = jobAlert;
        li = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        is_impression_marked = false;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int postition) {
        return mList.get(postition);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 3 && !jobAlert && ShineSharedPreferences.isCareerPlusAds(mActivity))
            return AD_ITEM;
        else
            return LIST_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        InboxMailViewHolder viewHolder = new InboxMailViewHolder();
        int type = getItemViewType(position);

        if (convertView == null) {
            if (type == LIST_ITEM) {
                Log.d("ADS_REC", "type " + type);
                view = li.inflate(R.layout.job_alert_grid_row, viewGroup, false);
                viewHolder.sender = (TextView) view.findViewById(R.id.sender);
                viewHolder.subject = (TextView) view.findViewById(R.id.subject);
                viewHolder.dateContainer = (LinearLayout) view.findViewById(R.id.image_layout);
                viewHolder.dateNumber = (TextView) view.findViewById(R.id.date_number);
                viewHolder.month = (TextView) view.findViewById(R.id.month);
                viewHolder.dot = (ImageView) view.findViewById(R.id.new_dot);
            } else if (type == AD_ITEM) {
                view = li.inflate(R.layout.ad_layout_messages, viewGroup, false);
                viewHolder.imageView = (ImageView) view.findViewById(R.id.imageView);
            }
            view.setTag(viewHolder);
        } else {
            viewHolder = (InboxMailViewHolder) view.getTag();
        }

        if (type == AD_ITEM) {
            final String admodel = ShineSharedPreferences.getCareerPlusAds(mActivity, ShineSharedPreferences.getCandidateId(mActivity));
            if (viewHolder.imageView != null) {
                if (!TextUtils.isEmpty(admodel)) {
                    viewHolder.imageView.setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    final AdModel model = gson.fromJson(admodel, AdModel.class);
                    if (model != null && model.results.size() > 0) {
                        if (URLConfig.AD >= model.results.size()) {
                            URLConfig.AD = 0;
                        }
                        try {
                            Glide.with(mActivity).load(model.results.get(URLConfig.AD).adPath).listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model1, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model1, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {


                                    try {
                                        String url = model.results.get(URLConfig.AD).impressionUrl;

                                        if (!TextUtils.isEmpty(url) && !is_impression_marked) {
                                            url = url.replace("App", "Android").replace("APP_SCREEN_NAME", "Recruiter_Mail_Listing");
                                            Log.d("IMPRESSION ", "URL " + url);
                                            ShineCommons.hitImpressionApi(mActivity, url);
                                            is_impression_marked = true;
//                                            ShineCommons.trackShineEvents("CareerPlusAdImpression", "Recruiter_Mail_Listing", mActivity);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    return false;
                                }
                            }).into(viewHolder.imageView);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        add_link = model.results.get(URLConfig.AD).clickUrl;
                    }
                } else {
                    viewHolder.imageView.setVisibility(View.GONE);
                }

                viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!TextUtils.isEmpty(add_link)) {
                            String appendUserData = "";
                            String encrypted_text = "";
                            String finalLink = "";

                            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
                            String date_time_value = KonnectFrag.getDate(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
                            if (add_link.contains("APP_SCREEN_NAME")) {
                                finalLink = add_link.replace("APP_SCREEN_NAME", "Recruiter_Mail_Listing");
                            }
                            if (user != null) {
                                appendUserData = URLConfig.key + "|" + user.email + "|" + user.mobile_no + "|" + date_time_value;
                                encrypted_text = XOREncryption.encryptDecrypt(appendUserData);
                                Uri uri = Uri.parse(finalLink);
                                String paramValue = uri.getQueryParameter("next");
                                if (paramValue.contains("?") || paramValue.contains("3F")) {
                                    finalLink = finalLink + URLEncoder.encode("&ad_content=" + encrypted_text);
                                } else {
                                    finalLink = finalLink + URLEncoder.encode("?ad_content=" + encrypted_text);
                                }
                            }

                            if (ShineCommons.appInstalledOrNot("com.android.chrome")) {
                                ChromeCustomTabs.openCustomTab(mActivity, finalLink);
                            } else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity, CustomWebView.class);
                                bundle.putString("shineurl", finalLink);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);
                            }

                            ShineCommons.trackShineEvents("CareerPlusAdClick", "Recruiter_Mail_Listing", mActivity);
                        }

                    }
                });
            }
        } else {

            InboxAlertMailModel.Results mailVo = mList.get(position);
            if (mailVo.has_read == 0){
                viewHolder.sender.setTypeface(Typeface.DEFAULT_BOLD);
            }
            else {
                viewHolder.sender.setTypeface(Typeface.DEFAULT);
            }

            if(mailVo.is_new_mail){
                viewHolder.dot.setVisibility(View.VISIBLE);
            }
            else {
                viewHolder.dot.setVisibility(View.GONE);
            }
            viewHolder.sender.setText(mailVo.recruiter_name);
            String date = ShineCommons.getFormattedDate(mailVo.timestamp.substring(0, 10));
            String date_number = date.substring(0, 2);
            String month = "";
            if (date_number != null && date_number.trim().length() == 1) {
                month = date.substring(2, 6).toUpperCase();

                date_number = "0" + date_number;
            } else {

                month = date.substring(3, 6).toUpperCase();
            }
            Log.d("date_inbox", date_number + " " + month);

            viewHolder.dateNumber.setText(date_number);
            viewHolder.month.setText(month);
            viewHolder.subject.setText(mailVo.subject_line);
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(date_number);
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect("", color);
            viewHolder.dateContainer.setBackground(drawable);
        }
        return view;
    }

    public void rebuildListObject(ArrayList<InboxAlertMailModel.Results> itemList) {
        this.mList = itemList;
    }
    class InboxMailViewHolder {

        TextView sender, subject, dateNumber, month;
        LinearLayout dateContainer;
        ImageView imageView,dot;
    }
}