package com.net.shine.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.net.shine.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class MainPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;
    private  ArrayList<String> names = new ArrayList<>();
    BaseActivity mActivity;
	public MainPagerAdapter(BaseActivity activity, FragmentManager fm, List<Fragment> fragments, ArrayList<String> name) {
        super(fm);
        this.fragments = fragments;
        this.mActivity=activity;
        this.names=name;
    }

    @Override
    public Fragment getItem(int i) {

    	 return this.fragments.get(i);

    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return names.get(position).toString();
    }
}