package com.net.shine.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.ExpandableListWithEditTextAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by manishpoddar on 29/03/17.
 */

public class ChildParentSpinnerWithEditTextFrag extends DialogFragment implements View.OnClickListener, ExpandableListView.OnChildClickListener {


    ExpandableListWithEditTextAdapter listAdapter;
    ExpandableListView lv;
    String title;
    public static   TextView tView;
    View view;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    String search_parent, search_child;


    List<String> listDataHeaderPID;
    HashMap<String, List<String>> listDataChildCID;

    HashMap<String, Integer> indexMap;
    HashMap<String, List<String>> newListDataChild = new HashMap<>();
    List<String> newListDataHeader = new ArrayList<>();


    List<String> completeList;
    int expandedGroupPos;
    String selectedChildText = "";
    BaseActivity mActivity;

    private boolean fromSearch=false;

    private  String other_edu="";

    private String lastText="";

    public ChildParentSpinnerWithEditTextFrag(String title, final TextView tView,
                                              final String[] list,String other_spec) {
        this.title = title;
        this.tView = tView;

        this.other_edu=other_spec;
        completeList = new ArrayList<>();
        listDataChild = new HashMap<>();
        listDataHeader = new ArrayList<>();
        indexMap = new HashMap<>();
        String currParent = "";
        Integer i = 0;
        for (String str : list) {
            completeList.add(str);
            indexMap.put(str, i);
            i++;
            if (str.startsWith("#")) {
                currParent = str.substring(1);
                listDataHeader.add(currParent);
                listDataChild.put(currParent, new ArrayList<String>());
            } else {
                listDataChild.get(currParent).add(str);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = ((BaseActivity) activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ShineBaseThemeNoOverlay);
    }

    // @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.child_parent_spinner_frag, container, false);

        try {

            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.update_btn).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);


            Integer[] info = (Integer[]) tView.getTag();
            Log.d("INFo", "info size " + info.length);

            for (int i = 0; i < info.length; i++) {
                Log.d("INFo", "info size " + info[i]);
                if (info[i] == null) {
                    info[i] = -1;
                }

            }


            //set selected only if selection is not a header
            //if (curr > 0)
            //  selectedChildText = completeList.get(curr);
            Log.d("complete-list1", completeList.size() + "");
            Log.d("complete-list2", listDataHeader.size() + "");
            Log.d("complete-list3", listDataChild.size() + "");
            //Log.d("complete-curr-value", curr + "");

            Log.d("info[0]",""+info[0]);

            Log.d("info[1]",""+info[1]);

            Log.d("listHeader ",listDataChild.toString()+"");


           ArrayList<Integer> childList=URLConfig.CHILD_CID_MAP.get(info[0]);


            lv = (ExpandableListView) view.findViewById(R.id.cus_lv_spinner);
            listAdapter = new ExpandableListWithEditTextAdapter(mActivity, listDataHeader, listDataChild,other_edu);
            // int info[] = listAdapter.getChildInfoByName(selectedChildText);
            expandedGroupPos = URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.indexOf(info[0]);

            if(childList!=null) {
                listAdapter.setSelectedChild(URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.indexOf(info[0]),
                        childList.indexOf(info[1]));
            }
            lv.setAdapter(listAdapter);
            TextView tv_title = (TextView) view.findViewById(R.id.header_title);
            tv_title.setText(title);


            lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                    if (lv.isGroupExpanded(groupPosition)) {
                        lv.collapseGroup(expandedGroupPos);
                        expandedGroupPos = -1;

                    } else {
                        lv.collapseGroup(expandedGroupPos);
                        expandedGroupPos = groupPosition;
                        lv.expandGroup(groupPosition);


                        ExpandableListWithEditTextAdapter.other_edu="";
                       // listAdapter = new ExpandableListWithEditTextAdapter(mActivity, listDataHeader, listDataChild,other_edu);

                       // listAdapter.refreshLists(listDataHeader, listDataChild,groupPosition,-1,"");

                        return true;
                    }


                    return false;
                }

            });


            lv.setOnChildClickListener(this);

            Log.d("expandgroupPos",""+expandedGroupPos);


            if (expandedGroupPos >= 0) {
                lv.expandGroup(expandedGroupPos);
                lv.setSelectedGroup(expandedGroupPos);
            }


            EditText et = (EditText) view.findViewById(R.id.et_spinner_search);


            if (completeList != null && completeList.size() < 13) {
                et.setVisibility(View.GONE);
            } else {
                et.setVisibility(View.VISIBLE);
            }


            et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        return true;
                    }
                    return false;
                }
            });

            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    try {
                        String prefix = s.toString().toLowerCase();
                        ExpandableListWithEditTextAdapter.other_edu="";


                        HashMap<String, List<String>> searchnewListDataChild = new HashMap<>();
                        List<String> searchnewListDataHeader = new ArrayList<>();


                        for (String parent : listDataHeader) {
                            List<String> childList = listDataChild.get(parent);

                            if(parent.replace(".","").toLowerCase().contains(prefix))
                            {
                                searchnewListDataHeader.add(parent);
                                searchnewListDataChild.put(parent,childList);

                                search_parent = parent;
                                Log.d("search_parent",search_parent);

                            }
                        }

                        newListDataHeader = searchnewListDataHeader;
                        newListDataChild = searchnewListDataChild;

                        //collapse any prev expanded groups
                        if (expandedGroupPos >= 0) {
                            lv.collapseGroup(expandedGroupPos);
                            expandedGroupPos = -1;
                        }


                        listAdapter.refreshLists(newListDataHeader, newListDataChild, -1, -1,other_edu);

                        if (prefix.isEmpty()) {
                            //restore users expand selection
                            for (int i = 0; i < newListDataChild.size(); i++) {
                                if (expandedGroupPos == i)
                                    lv.expandGroup(i);
                                else
                                    lv.collapseGroup(i);
                            }
                        } else {
                            //expand all
                            for (int i = 0; i < newListDataChild.size(); i++) {
                                lv.expandGroup(i);

                            }
                        }


                        fromSearch=true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                    ExpandableListWithEditTextAdapter.other_edu="";


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }


    @Override
    public void onClick(View v) {
//        super.onClick(v);

        switch (v.getId()) {
            case R.id.cancel_btn:
                mActivity.popone();
                break;
        }


    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        listAdapter.setSelectedChild(groupPosition, childPosition);






//        Log.d("GP+CP", groupPosition + " " + childPosition);
//        Log.d("GP_text", listDataHeader.get(groupPosition).toString() + "");
        selectedChildText = ((CheckedTextView) v.findViewById(R.id.text1)).getText().toString();
//        Log.d("initial_searchpa",search_parent);
//        Log.d("parent_selectedpos",parent.getSelectedPosition()+"");

       // parent.getSelectedPosition();

        if(fromSearch){
            Log.d("_group_posi",newListDataHeader.get(groupPosition)+"");
            search_parent = newListDataHeader.get(groupPosition);
            groupPosition = URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_LIST.indexOf(search_parent);
            groupPosition=groupPosition-1;
            Log.d("searchparent_childclick",search_parent+groupPosition);

            Log.d("search_group_posi",groupPosition+"");
        }






        try {

            String text;
            if (URLConfig.NO_SELECTION
                    .equals(selectedChildText)) {
                text = "";
            } else {
                text = selectedChildText;
            }
            ShineCommons.hideKeyboard(mActivity);


            Log.d("INFo", "info selectedChildText " + selectedChildText);

            Log.d("INFo", "info text " + text);


            Integer[] info = new Integer[2];
            info[0] = URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.get(groupPosition);
            info[1] = URLConfig.CHILD_CID_MAP.get(info[0]).get(childPosition);



            if(info[1]==506) {


                Log.d("Other", "other option selected ");
                Log.d("TAG1","grp "+info[0]+" child "+info[1]);


            }
            else
            {

                Log.d("TAG","grp "+info[0]+" child "+info[1]);


                tView.setTag(info);

                String parentText=listDataHeader.get(groupPosition).toString();


                tView.setText(parentText  +" ("+  text+")");


                mActivity.getSupportFragmentManager().popBackStackImmediate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
