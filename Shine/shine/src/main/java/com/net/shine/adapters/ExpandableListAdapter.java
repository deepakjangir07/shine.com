package com.net.shine.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.net.shine.R;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public int[] getChildInfoByName(String child)
    {
        int[] info = new int[2];
        info[0] = -1;
        info[1] = -1;
        for(int i=0;i< _listDataHeader.size();i++)
        {
            List<String> childList = _listDataChild.get(_listDataHeader.get(i));
            for(int j=0;j<childList.size();j++)
            {
                if(childList.get(j).equals(child))
                {
                    info[0] = i;
                    info[1] = j;
                    return info;
                }
            }
        }
        return info;
    }



    int selected_grp_pos = -1;
    int selected_child_pos = -1;

    public void setSelectedChild(int grp, int subchild)
    {
        selected_grp_pos = grp;
        selected_child_pos = subchild;
        notifyDataSetChanged();
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(android.R.layout.simple_list_item_single_choice, null);
        }

        CheckedTextView txtListChild = (CheckedTextView) convertView
                .findViewById(android.R.id.text1);
        txtListChild.setTextColor(Color.parseColor("#555555"));
        if(Build.VERSION.SDK_INT >= 16)
        {
            txtListChild.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        txtListChild.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);


        txtListChild.setText(childText);

        if(selected_grp_pos==groupPosition && selected_child_pos==childPosition)
            txtListChild.setChecked(true);
        else
            txtListChild.setChecked(false);



        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.spinner_parent_items, parent, false);
        }
        ImageView img = (ImageView) convertView.findViewById(R.id.expand_indicator);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);

        if(isExpanded){
            img.setImageResource(R.drawable.textdropup_arrow);

                lblListHeader.setTypeface(Typeface.create("sans-serif-light",Typeface.BOLD));
        }
        else{
            img.setImageResource(R.drawable.textdropdown_arrow);

                lblListHeader.setTypeface(Typeface.create("sans-serif-light",Typeface.NORMAL));

        }

        lblListHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void refreshLists(List<String> listDataHeader,
                              HashMap<String, List<String>> listChildData,
                             int selected_grp_pos, int selected_child_pos)
    {
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        setSelectedChild(selected_grp_pos, selected_child_pos);
        notifyDataSetChanged();
    }

}