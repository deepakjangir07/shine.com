package com.net.shine.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.net.shine.MyApplication;

import java.io.InputStream;
import java.util.Map;

public class CustomInsRequest extends Request<byte[]> {
    private final VolleyNetworkRequest mListener;
    public static Map<String, String> responseHeaders;

    public CustomInsRequest(int post, String mUrl,
                            VolleyNetworkRequest volleyInsDownloader,
                            VolleyNetworkRequest errorListener) {
        // TODO Auto-generated constructor stub

        super(post, mUrl, errorListener);
        // this request would never use cache.
        setShouldCache(false);
        mListener = volleyInsDownloader;

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        return MyApplication.getInstance().getHeaders();

    }

    @Override
    protected void deliverResponse(byte[] response) {
        mListener.onResponse(response);
    }

    @Override
    protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {

        responseHeaders = response.headers;

        return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
    }

}