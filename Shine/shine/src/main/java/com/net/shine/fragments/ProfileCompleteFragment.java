package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableWeightLayout;
import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.HomeActivity;
import com.net.shine.adapters.SuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.BulkSkillsResultsModel;
import com.net.shine.models.ResumeDetails;
import com.net.shine.models.SkillsResultsModel;
import com.net.shine.models.ThirdPageEducation;
import com.net.shine.models.ThirdPageModel;
import com.net.shine.models.TotalExpModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


/**
 * Created by Deepak on 10/7/15.
 */
public class ProfileCompleteFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, VolleyRequestCompleteListener {
    private boolean skillAdded = false;
    private boolean profileAdded = false;
    private boolean empAdded = false;
    private boolean eduAdded = false;
    private String job_id = "";
    private String edu_id = "";
    private String other_edu = "";

    private class SkillsHolder {
        public AutoCompleteTextView skillName;
        public TextView skillLevel;
        public ImageView skillDelete;
        public TextInputLayout inputLayoutSkill;
        public TextInputLayout inputLayoutExperience;

        public SkillsResultsModel skillModel;
    }

    private ArrayList<SkillsHolder> skillViewList = new ArrayList<>();
    private UserStatusModel userProfileVO;

    private final String UPDATE_SKILL = "updateSkills";
    private View parentView;
    private EditText jobTitleField;
    private EditText comNameField;
    private LinearLayout container_box;
    private CheckBox alertBoxImg;
    private EditText salary, profile_title;
    private boolean isEmpty = false;
    private boolean alertsCheck = false;
    private boolean isParserEmpty = false;
    private EditText indusNameField;
    private EditText funAreaField;
    private EditText expStartYearField, expStartMonthField, expEndYearField, expEndMonthField;
    private EditText qualLevelField, eduStreamField, passoutYearField, courseTypeField;
    private AutoCompleteTextView instituteNameField;
    private LinearLayout sub_Layout;
    TextInputLayout inputLayoutQualification, inputLayoutSpecialization, inputLayoutInstituteName, inputLayoutCourseType,
            inputLayoutPassoutYear, inputLayoutJobTitle, inputLayoutCompany, inputLayoutIndustry, inputLayoutFunctionalArea, inputLayoutSkill;

    private ExpandableWeightLayout emp_expandLayout;
    private ExpandableWeightLayout profile_expandLayout;
    private ExpandableWeightLayout edu_expandLayout;
    private ExpandableWeightLayout skill_expandLayout;
    private TextView profile_text_expand;
    private TextView edu_text_expand;
    private TextView emp_text_expand;
    private TextView skill_text_expand;


    public static ProfileCompleteFragment newInstance(Bundle args) {

        ProfileCompleteFragment fragment = new ProfileCompleteFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.profile_complete_fragment, container, false);

        try {


            boolean is_from_social_apply = getArguments() != null && getArguments().getBoolean("is_from_social_apply", false);
            TextView profile_comp_msg = (TextView) parentView.findViewById(R.id.text_profile_complete_msg);
            TextView post_apply_pass_msg = (TextView) parentView.findViewById(R.id.social_apply_password_msg);

            if (is_from_social_apply) {
                parentView.findViewById(R.id.thanks_social_apply).setVisibility(View.VISIBLE);
                profile_comp_msg.setText("Complete registration to help top recruiters reach you.");
                post_apply_pass_msg.setText("Your password has been sent to " + ShineSharedPreferences.getUserEmail(MyApplication.getInstance().getApplicationContext()));
            }

            emp_expandLayout = (ExpandableWeightLayout) parentView.findViewById(R.id.emp_expandableLayout);
            profile_expandLayout = (ExpandableWeightLayout) parentView.findViewById(R.id.profile_expandableLayout);
            edu_expandLayout = (ExpandableWeightLayout) parentView.findViewById(R.id.edu_expandableLayout);
            skill_expandLayout = (ExpandableWeightLayout) parentView.findViewById(R.id.expandableLayout);
            profile_text_expand = (TextView) parentView.findViewById(R.id.profile_col_exp_btn);
            edu_text_expand = (TextView) parentView.findViewById(R.id.edu_col_exp_btn);
            emp_text_expand = (TextView) parentView.findViewById(R.id.emp_col_exp_btn);
            skill_text_expand = (TextView) parentView.findViewById(R.id.skil_col_exp_btn);
            profile_title = (EditText) parentView.findViewById(R.id.job_profile);
            salary = (EditText) parentView.findViewById(R.id.salary_area_field);
            salary.setTag(0);
            salary.setOnClickListener(this);
            salary.setOnFocusChangeListener(this);


            setCollapseExpandText(emp_expandLayout, emp_text_expand);
            setCollapseExpandText(profile_expandLayout, profile_text_expand);
            setCollapseExpandText(edu_expandLayout, edu_text_expand);

            setCollapseExpandText(skill_expandLayout, skill_text_expand);


            RelativeLayout emp_block = (RelativeLayout) parentView.findViewById(R.id.emp_block);
            emp_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    emp_expandLayout.toggle();
                    Log.d("Debug ", "state " + emp_expandLayout.isExpanded());
                    if (emp_expandLayout.isExpanded()) {
                        ((TextView) parentView.findViewById(R.id.emp_col_exp_btn)).setText(" + ");
                    } else {
                        ((TextView) parentView.findViewById(R.id.emp_col_exp_btn)).setText(" _ ");

                    }

                }
            });
            final RelativeLayout profile_block = (RelativeLayout) parentView.findViewById(R.id.profile_block);
            profile_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profile_expandLayout.toggle();
                    if (profile_expandLayout.isExpanded()) {
                        ((TextView) parentView.findViewById(R.id.profile_col_exp_btn)).setText(" + ");
                    } else {
                        ((TextView) parentView.findViewById(R.id.profile_col_exp_btn)).setText(" _ ");

                    }
                }
            });

            RelativeLayout edu_block = (RelativeLayout) parentView.findViewById(R.id.edu_block);
            edu_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edu_expandLayout.toggle();
                    if (edu_expandLayout.isExpanded()) {
                        ((TextView) parentView.findViewById(R.id.edu_col_exp_btn)).setText(" + ");
                    } else {
                        ((TextView) parentView.findViewById(R.id.edu_col_exp_btn)).setText(" _ ");

                    }

                }
            });

            RelativeLayout skill_block = (RelativeLayout) parentView.findViewById(R.id.skills_block);
            skill_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    skill_expandLayout.toggle();
                    if (skill_expandLayout.isExpanded()) {
                        ((TextView) parentView.findViewById(R.id.skil_col_exp_btn)).setText(" + ");
                    } else {
                        ((TextView) parentView.findViewById(R.id.skil_col_exp_btn)).setText(" _ ");

                    }
                }
            });

            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

            container_box = (LinearLayout) parentView.findViewById(R.id.main_container);
            container_box.requestFocus();

            parentView.findViewById(R.id.skip_btn).setOnClickListener(this);

            if (Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
                parentView.findViewById(R.id.skip_btn).setVisibility(View.GONE);
                mActivity.hideNavBar();
                mActivity.showBackButton();
            } else {
                parentView.findViewById(R.id.skip_btn).setVisibility(View.VISIBLE);
                mActivity.showNavBar();
                mActivity.setActionBarVisible(true);
            }

            parentView.findViewById(R.id.submit_btn).setOnClickListener(this);

            if (!ShineSharedPreferences.getExpFlag(mActivity)) {
                addUpdateProfessional();
                TextChangeListener.addListener(jobTitleField, inputLayoutJobTitle, mActivity);
                TextChangeListener.addListener(funAreaField, inputLayoutFunctionalArea, mActivity);
                TextChangeListener.addListener(indusNameField, inputLayoutIndustry, mActivity);
                TextChangeListener.addListener(comNameField, inputLayoutCompany, mActivity);
            }
            if (!ShineSharedPreferences.getEduFlag(mActivity)) {
                addUpdateEducational();
                TextChangeListener.addListener(qualLevelField, inputLayoutQualification, mActivity);
                TextChangeListener.addListener(eduStreamField, inputLayoutSpecialization, mActivity);
                TextChangeListener.addListener(instituteNameField, inputLayoutInstituteName, mActivity);
                TextChangeListener.addListener(courseTypeField, inputLayoutCourseType, mActivity);
                TextChangeListener.addListener(passoutYearField, inputLayoutPassoutYear, mActivity);


            }
            if (!ShineSharedPreferences.getSkillFlag(mActivity)) {
                skillViewList = new ArrayList<>();
                addUpdateSkills();
                // TextChangeListener.addListener(skillNameField,inputLayoutSkill,mActivity);

            }
            if (!ShineSharedPreferences.getProfileFlag(mActivity)) {
                addUpdateProfile();
            }
            boolean isResume = ShineSharedPreferences
                    .getResumeMidout(mActivity);
            if (!isResume && !isParserEmpty) {
                parentView.findViewById(R.id.text).setVisibility(
                        View.VISIBLE);

            }

        } catch (Exception w) {
            w.printStackTrace();
        }
        return parentView;
    }

    private void addUpdateProfile() {
        try {
            parentView.findViewById(R.id.profile_layout).setVisibility(View.VISIBLE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setCollapseExpandText(ExpandableWeightLayout expandableRelativeLayout, TextView tv) {
        if (expandableRelativeLayout.isExpanded()) {
            tv.setText(" _ ");
        } else {
            tv.setText(" + ");

        }

    }

    private void addUpdateProfessional() {
        try {

            parentView.findViewById(R.id.e_details).setVisibility(View.VISIBLE);
            jobTitleField = (EditText) parentView
                    .findViewById(R.id.job_title_field);

            alertBoxImg = (CheckBox) parentView.findViewById(R.id.alert_check);
            alertBoxImg.setChecked(false);
            parentView.findViewById(R.id.alert_check_layout)
                    .setOnClickListener(this);

            comNameField = (EditText) parentView
                    .findViewById(R.id.comp_name_field);
            funAreaField = (EditText) parentView.findViewById(R.id.functional_area_field);
            indusNameField = (EditText) parentView.findViewById(R.id.industry_area_field);
            inputLayoutCompany = (TextInputLayout) parentView.findViewById(R.id.input_layout_comp_name);
            inputLayoutJobTitle = (TextInputLayout) parentView.findViewById(R.id.input_layout_job_title);
            inputLayoutFunctionalArea = (TextInputLayout) parentView.findViewById(R.id.input_layout_functional_area);
            inputLayoutIndustry = (TextInputLayout) parentView.findViewById(R.id.input_layout_industry);
            funAreaField.setOnClickListener(this);
            funAreaField.setOnFocusChangeListener(this);
            indusNameField.setOnClickListener(this);
            indusNameField.setOnFocusChangeListener(this);
            funAreaField.setTag(0);
            indusNameField.setTag(0);
            expStartYearField = (EditText) parentView
                    .findViewById(R.id.start_year);

            expStartYearField.setOnClickListener(this);
            expStartYearField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    ProfileCompleteFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expStartMonthField = (EditText) parentView
                    .findViewById(R.id.start_month);

            expStartMonthField.setOnClickListener(this);
            expStartMonthField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    ProfileCompleteFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expEndYearField = (EditText) parentView.findViewById(R.id.end_year);


            expEndYearField.setOnClickListener(this);
            expEndYearField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    ProfileCompleteFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });

            expEndMonthField = (EditText) parentView
                    .findViewById(R.id.end_month);


            expEndMonthField.setOnClickListener(this);
            expEndMonthField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    ProfileCompleteFragment.this.onFocusChange(v, hasFocus);
                    v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                }
            });


            ResumeDetails parser = ShineSharedPreferences
                    .getParserInfo(mActivity, ShineSharedPreferences.getCandidateId(mActivity));
            checkForEmtyParser(parser);


            if (parser.getJobs().size() > 0) {


                jobTitleField.setText(parser.getJobs().get(0).getJob_title());
                comNameField.setText(parser.getJobs().get(0).getCompany_name());
                expStartMonthField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.MONTH_NAME_REVERSE_MAP,
                        URLConfig.MONTH_NAME_LIST, parser.getJobs().get(0).getStart_month(),
                        expStartMonthField));
                expStartYearField.setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                        URLConfig.YEARS_NAME_LIST, parser.getJobs().get(0).getStart_year(),
                        expStartYearField));

                funAreaField.setTag(ShineCommons.setTagAndValue(URLConfig.FUNCTIONA_AREA_REVERSE_MAP, URLConfig.FUNCTIONAL_AREA_LIST2, parser.getJobs().get(0).getFuctional_area(), funAreaField));

                if (parser.getJobs().get(0).getIndustry() == 0) {
                    indusNameField.setTag(-1);
                } else {

                    indusNameField.setTag(ShineCommons.setTagAndValue(URLConfig.INDUSTRY_REVERSE_MAP, URLConfig.INDUSTRY_LIST2, parser.getJobs().get(0).getIndustry(), indusNameField));
                }

                if (parser.getJobs().get(0).is_current() == true) {
                    alertsCheck = true;
                    alertBoxImg.setChecked(true);
                    expEndMonthField.setVisibility(View.GONE);
                    expEndYearField.setVisibility(View.GONE);
                    parentView.findViewById(R.id.toyear).setVisibility(View.GONE);
                    expEndMonthField.setTag(0);
                    expEndYearField.setTag(0);
                } else {

                    expEndMonthField.setTag(ShineCommons.setTagAndValue(
                            URLConfig.MONTH_NAME_REVERSE_MAP,
                            URLConfig.MONTH_NAME_LIST, parser.getJobs().get(0).getEnd_month(),
                            expEndMonthField));

                    expEndYearField.setTag(ShineCommons.setTagAndValue(URLConfig.YEAR_MAP,
                            URLConfig.YEARS_NAME_LIST, parser.getJobs().get(0).getEnd_year(),
                            expEndYearField));


                }
            }

            if (parser.getSalary_in_lakh() > 0) {
                salary.setTag(ShineCommons.setTagAndValue(URLConfig.ANNUAL_SALARYL_REVRSE_MAP, URLConfig.ANNUAL_SALARY_LIST, parser.getSalary_in_lakh(), salary));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void addUpdateEducational() {
        try {
            parentView.findViewById(R.id.edu_details).setVisibility(View.VISIBLE);

            qualLevelField = (EditText) parentView.findViewById(R.id.qual_field);

            qualLevelField.setOnClickListener(this);
            qualLevelField.setOnFocusChangeListener(this);


            Integer[] tag = new Integer[2];
            tag[0] = -1;
            tag[1] = -1;


            qualLevelField.setTag(ShineCommons.setTagAndValueQualification(
                    URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP,
                    URLConfig.EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP,
                    tag[0], tag[1], qualLevelField, ""));


            eduStreamField = (EditText) parentView
                    .findViewById(R.id.edu_stream_field);
            eduStreamField.setTag(0);

            eduStreamField.setOnClickListener(this);
            eduStreamField.setOnFocusChangeListener(this);

            instituteNameField = (AutoCompleteTextView) parentView
                    .findViewById(R.id.ins_name_field);
            instituteNameField.setAdapter(new SuggestionsFilterableArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.INSTITUTE_SUGGESTION));
            inputLayoutQualification = (TextInputLayout) parentView.findViewById(R.id.input_layout_qual_name);
            inputLayoutSpecialization = (TextInputLayout) parentView.findViewById(R.id.input_layout_edu_name);
            inputLayoutInstituteName = (TextInputLayout) parentView.findViewById(R.id.input_layout_institute_name);
            inputLayoutCourseType = (TextInputLayout) parentView.findViewById(R.id.input_layout_course_type_name);
            inputLayoutPassoutYear = (TextInputLayout) parentView.findViewById(R.id.input_layout_passout);

            courseTypeField = (EditText) parentView
                    .findViewById(R.id.course_type_field);


            courseTypeField.setTag(0);

            courseTypeField.setOnClickListener(this);
            courseTypeField.setOnFocusChangeListener(this);

            passoutYearField = (EditText) parentView
                    .findViewById(R.id.passout_yr_field);


            passoutYearField.setOnClickListener(this);
            passoutYearField.setOnFocusChangeListener(this);

            ResumeDetails parser = ShineSharedPreferences
                    .getParserInfo(mActivity, ShineSharedPreferences.getCandidateId(mActivity));
            checkForEmtyParser(parser);

            if (parser.getQualifications().size() > 0) {

                instituteNameField.setText(parser.getQualifications().get(0).getInstitute_name());

                tag[0] = parser.getQualifications().get(0).getEducation_level();
                tag[1] = parser.getQualifications().get(0).getEducation_specialization();

                Log.d("dump-edu", tag[1] + " " + parser.getQualifications().get(0).other_specialization);


                qualLevelField.setTag(ShineCommons.setTagAndValueQualification(
                        URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP,
                        URLConfig.EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP,
                        tag[0], tag[1], qualLevelField, parser.getQualifications().get(0).other_specialization));


                passoutYearField.setTag(ShineCommons.setTagAndValue(
                        URLConfig.PASSOUT_YEAR_REVRSE_MAP,
                        URLConfig.PASSOUT_YEAR_LIST, parser.getQualifications().get(0).getYear_of_passout(),
                        passoutYearField));

                courseTypeField.setTag(ShineCommons.setTagAndValue(URLConfig.COURSE_TYPE_REVRSE_MAP, URLConfig.COURSE_TYPE_LIST, parser.getQualifications().get(0).getCourse_type(), courseTypeField));
                passoutYearField.setTag(ShineCommons.setTagAndValue(URLConfig.PASSOUT_YEAR_REVRSE_MAP, URLConfig.PASSOUT_YEAR_LIST, parser.getQualifications().get(0).getYear_of_passout(), passoutYearField));


                other_edu = parser.getQualifications().get(0).other_specialization;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public

    int curr_req = 0;
    Dialog alertDialog;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submit_btn:


                if (!ShineSharedPreferences.getProfileFlag(mActivity) || profileAdded) {
                    updateProfileDetails();
                }
                if (!ShineSharedPreferences.getExpFlag(mActivity) || empAdded) {
                    updateProfessionalDetails();
                }
                if (!ShineSharedPreferences.getEduFlag(mActivity) || eduAdded) {
                    updateEducationalDetails();
                }
                if (!ShineSharedPreferences.getSkillFlag(mActivity) || skillAdded) {
                    updateSkillDetails();
                }

                if (curr_req > 0) {
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomProgressDialog(mActivity, getString(R.string.plz_wait));
                }

                break;

            case R.id.skip_btn:

                mActivity.onBackPressed();

                if (Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
                    ShineSharedPreferences.saveProfileSkip(mActivity, false);

                } else {
//                    ShineSharedPreferences.saveProfileSkip(mActivity, true);
                }

                ShineCommons.trackShineEvents("Skip", "ProfileComplete", mActivity);

                break;

            case R.id.alert_check_layout:
                if (alertsCheck) {
                    alertsCheck = false;
                    alertBoxImg.setChecked(false);
                    expEndMonthField.setVisibility(View.VISIBLE);
                    expEndYearField.setVisibility(View.VISIBLE);
                    parentView.findViewById(R.id.toyear).setVisibility(View.VISIBLE);

                } else {

                    alertsCheck = true;
                    alertBoxImg.setChecked(true);
                    expEndMonthField.setVisibility(View.GONE);
                    expEndYearField.setVisibility(View.GONE);
                    parentView.findViewById(R.id.toyear).setVisibility(View.GONE);
                }
                break;

            default:
                showSpinner(v.getId());
                break;
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            showSpinner(v.getId());


    }

    private void showSpinner(int id) {
        try {
            switch (id) {


                case R.id.salary_area_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
                            salary, URLConfig.ANNUAL_SALARYL_LIST, mActivity);
                    break;

                case R.id.functional_area_field:
                    DialogUtils.showNewMultiSpinnerDialogRadio(getString(R.string.hint_func_area),
                            funAreaField, URLConfig.FUNCTIONAL_AREA_LIST2, mActivity);
                    break;
                case R.id.industry_area_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_industry),
                            indusNameField, URLConfig.INDUSTRY_LIST3, mActivity);
                    break;
                case R.id.start_year:

                    DialogUtils.showNewSingleSpinnerRadioDialog("Start Year", expStartYearField,
                            URLConfig.YEARS_NAME_LIST, mActivity);
                    break;
                case R.id.start_month:

                    DialogUtils.showNewSingleSpinnerRadioDialog("Start Month",
                            expStartMonthField, URLConfig.MONTH_NAME_LIST,
                            mActivity);
                    break;
                case R.id.end_year:

                    DialogUtils.showNewSingleSpinnerRadioDialog("End Year", expEndYearField,
                            URLConfig.YEARS_NAME_LIST, mActivity);
                    break;
                case R.id.end_month:

                    DialogUtils.showNewSingleSpinnerRadioDialog("End Month", expEndMonthField,
                            URLConfig.MONTH_NAME_LIST, mActivity);
                    break;


                case R.id.qual_field:

                    try {
                        if (qualLevelField.getTag() != null) {
                            Integer[] info = (Integer[]) qualLevelField.getTag();
                            if (info != null && info.length > 1 && info[1] == 506) {
                                String str = qualLevelField.getText().toString();
                                String other_specialization = str.substring(str.indexOf("(") + 1, str.indexOf(")"));
                                other_edu = other_specialization;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//
                    DialogUtils.showNewMultiSpinnerwithEdittextDialogRadio(getString(R.string.hint_qual),
                            qualLevelField, URLConfig.EDUCATIONAL_QUALIFICATION_LIST2, other_edu,
                            mActivity);


                    break;

                case R.id.course_type_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_course_type), courseTypeField,
                            URLConfig.COURSE_TYPE_LIST, mActivity);
                    break;
                case R.id.passout_yr_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_passout_year),
                            passoutYearField, URLConfig.PASSOUT_YEAR_LIST,
                            mActivity);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateProfileDetails() {
        try {
            String profileTitleTV = "" + profile_title.getText();
            profileTitleTV = profileTitleTV.trim();
            String salaryTV = "" + salary.getText();
            salaryTV = salaryTV.trim();

            if (profileTitleTV.isEmpty() && salaryTV.isEmpty()) {
                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);


            Type listType = new TypeToken<TotalExpModel>() {
            }.getType();
            String URL = URLConfig.MY_TOTAL_PROFESSIONAL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
            if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
            } else {
                URL = URL;
            }

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                    URL, listType);

            HashMap profileMap = new HashMap();

            profileMap.put("resume_title", profileTitleTV);
            profileMap.put("salary_in_lakh", URLConfig.ANNUAL_SALARYL_MAP.get("" + salaryTV));
            profileMap.put("salary_in_thousand", "0");
            profileMap.put("experience_in_years", userProfileVO.experience_in_years);

            downloader.setUsePutMethod(profileMap);

            downloader.execute("AddProfileDetails");
            curr_req++;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProfessionalDetails() {
        try {

            String indusNameTV = indusNameField.getText().toString().trim();

            if (indusNameTV.length() == 0) {
                isEmpty = true;
                inputLayoutIndustry.setError("Please enter industry");

            } else {
                indusNameTV = URLConfig.INDUSTRY_MAP.get("" + indusNameTV);
            }
            String funAreaTV = funAreaField.getText().toString().trim();
            if (funAreaTV.length() == 0) {
                isEmpty = true;
                inputLayoutFunctionalArea.setError("Please enter functional area");

            } else {
                funAreaTV = URLConfig.FUNCTIONA_AREA_MAP.get(funAreaTV);
            }

            String jobTitleTV = "" + jobTitleField.getText();
            jobTitleTV = jobTitleTV.trim();

            if (jobTitleTV.length() == 0) {
                isEmpty = true;
                inputLayoutJobTitle.setError("Please enter job title");
            }

            String comNameTV = "" + comNameField.getText();
            comNameTV = comNameTV.trim();

            if (comNameTV.length() == 0) {
                isEmpty = true;
                inputLayoutCompany.setError("Please enter company name");
            }


            String expStartYearTV = expStartYearField.getText().toString()
                    .trim();

            if (expStartYearTV.length() == 0) {
                isEmpty = true;
                changeColor(expStartYearField);
            } else {
                expStartYearTV = URLConfig.YEAR_MAP.get(expStartYearTV);
            }

            String expStartMonthsTV = expStartMonthField.getText().toString()
                    .trim();

            if (expStartMonthsTV.length() == 0) {
                isEmpty = true;
                changeColor(expStartMonthField);
            } else {
                expStartMonthsTV = URLConfig.MONTH_NAME_MAP
                        .get(expStartMonthsTV);
            }

            String expEndYearTV = expEndYearField.getText().toString().trim();

            if (expEndYearTV.length() == 0 && !alertsCheck) {
                isEmpty = true;
                changeColor(expEndYearField);
            } else {
                expEndYearTV = URLConfig.YEAR_MAP.get(expEndYearTV);
            }

            String expEndMonthsTV = expEndMonthField.getText().toString()
                    .trim();

            if (expEndMonthsTV.length() == 0 && !alertsCheck) {
                isEmpty = true;
                changeColor(expEndMonthField);
            } else {
                expEndMonthsTV = URLConfig.MONTH_NAME_MAP.get(expEndMonthsTV);
            }

            if (isEmpty) {

                isEmpty = false;

                emp_expandLayout.expand();
                emp_text_expand.setText(" _ ");
                showToastAlert(getResources().getString(
                        R.string.required_fields));

                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);

            if (alertsCheck) {
                expEndYearTV = "null";
                expEndMonthsTV = "null";
            }


            Type listType = new TypeToken<ThirdPageModel>() {
            }.getType();
            VolleyNetworkRequest downloader;

            String URL = URLConfig.MY_TOTAL_JOBS_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

            if (TextUtils.isEmpty(job_id)) {
                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                } else {
                    URL = URL;
                }
                downloader = new VolleyNetworkRequest(mActivity, this,
                        URL, listType);

            } else {

                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + job_id + "/" + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                } else {
                    URL = URL + job_id + "/";
                }
                downloader = new VolleyNetworkRequest(mActivity, this,
                        URL, listType);
            }
            if (alertsCheck) {
                HashMap profileMap = new HashMap();

                profileMap.put("start_year", expStartYearTV);
                profileMap.put("start_month", expStartMonthsTV);
                profileMap.put("job_title", jobTitleTV);
                profileMap.put("company_name", comNameTV);
                profileMap.put("industry_id", indusNameTV);
                profileMap.put("sub_field", funAreaTV);
                profileMap.put("is_current", alertsCheck + "");

                if (TextUtils.isEmpty(job_id)) {
                    downloader.setUsePostMethod(profileMap);
                } else {
                    downloader.setUsePutMethod(profileMap);
                }
                downloader.execute("AddProfessionalDetails");
                curr_req++;
            } else {

                HashMap profileMap = new HashMap();

                profileMap.put("start_year", expStartYearTV);
                profileMap.put("start_month", expStartMonthsTV);
                profileMap.put("end_year", expEndYearTV);
                profileMap.put("end_month", expEndMonthsTV);
                profileMap.put("job_title", jobTitleTV);
                profileMap.put("company_name", comNameTV);
                profileMap.put("industry_id", indusNameTV);
                profileMap.put("sub_field", funAreaTV);
                profileMap.put("is_current", alertsCheck + "");


                if (TextUtils.isEmpty(job_id)) {
                    downloader.setUsePostMethod(profileMap);
                } else {
                    downloader.setUsePutMethod(profileMap);
                }

                downloader.execute("AddProfessionalDetails");
                curr_req++;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateEducationalDetails() {
        try {

            String other_specialization = "";
            String qualLevelTV = qualLevelField.getText().toString().trim();
            String eduStreamTV = eduStreamField.getText().toString().trim();
            String instituteNameTV = instituteNameField.getText().toString()
                    .trim();
            String passoutYearTV = passoutYearField.getText().toString().trim();
            String courseTypeTV = courseTypeField.getText().toString().trim();

            if (qualLevelTV.length() == 0) {
                isEmpty = true;
                inputLayoutQualification.setError("Please select qualification");
            } else {


                Integer[] info = (Integer[]) qualLevelField.getTag();
                qualLevelTV = info[0].toString();
                eduStreamTV = info[1].toString();
                if (info[1] == 506) {
                    String str = qualLevelField.getText().toString();
                    other_specialization = str.substring(str.indexOf("(") + 1, str.indexOf(")"));
                    other_edu = other_specialization;
                }
            }


            if (instituteNameTV.length() == 0) {
                isEmpty = true;
                inputLayoutInstituteName.setError("Please enter institutes name");
            }

            if (courseTypeTV.length() == 0) {
                isEmpty = true;
                inputLayoutCourseType.setError("Please select course type");
            } else {
                courseTypeTV = URLConfig.COURSE_TYPE_MAP.get(courseTypeTV);
            }

            if (passoutYearTV.length() == 0) {
                isEmpty = true;
                inputLayoutPassoutYear.setError("Please select year of pass-out");
            } else {
                passoutYearTV = URLConfig.PASSOUT_YEAR_MAP.get(passoutYearTV);
            }

            if (isEmpty) {
                isEmpty = false;


                edu_expandLayout.expand();
                edu_text_expand.setText(" _ ");
                showToastAlert(getResources().getString(
                        R.string.required_fields));
                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);

            Type listType = new TypeToken<ThirdPageEducation>() {
            }.getType();

            HashMap eduMap = new HashMap();

            eduMap.put("candidate_id", userProfileVO.candidate_id);
            eduMap.put("education_level", qualLevelTV);
            eduMap.put("education_specialization", eduStreamTV);
            eduMap.put("institute_name", instituteNameTV);
            eduMap.put("course_type", courseTypeTV);
            eduMap.put("year_of_passout", passoutYearTV);
            eduMap.put("education_specialization_custom", other_specialization);


            String URL = "";

            if (TextUtils.isEmpty(edu_id)) {
                URL = URLConfig.UPDATE_EDU_DETAILS.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                } else {
                    URL = URL;
                }
            } else {
                URL = URLConfig.UPDATE_EDU_DETAILS.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id) + edu_id + "/";
                if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                    URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
                } else {
                    URL = URL;
                }

            }


            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                    URL, listType);


            if (TextUtils.isEmpty(edu_id)) {
                downloader.setUsePostMethod(eduMap);
            } else {
                downloader.setUsePutMethod(eduMap);

            }
            downloader.execute("AddEducationDetails");
            curr_req++;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(final Object object, final String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    curr_req--;
                    if (curr_req <= 0) {
                        DialogUtils.dialogDismiss(alertDialog);
                    }

                    if (object != null) {

                        switch (tag) {
                            case "AddProfessionalDetails":
                                ShineSharedPreferences.setExpDashFlag(
                                        mActivity, true);
                                // parentView.findViewById(R.id.e_details).setVisibility(View.GONE);


                                ThirdPageModel model = (ThirdPageModel) object;
                                job_id = model.getId();

                                empAdded = true;
                                emp_expandLayout.collapse();
                                emp_text_expand.setText(" + ");
                                break;

                            case "AddEducationDetails":
                                ShineSharedPreferences.setEduDashFlag(
                                        mActivity, true);
                                //parentView.findViewById(R.id.edu_details).setVisibility(View.GONE);

                                ThirdPageEducation edumodel = (ThirdPageEducation) object;

                                edu_id = edumodel.getId();

                                eduAdded = true;
                                edu_expandLayout.collapse();
                                edu_text_expand.setText(" + ");
                                break;

                            case "AddProfileDetails":
                                ShineSharedPreferences.setProfileFlag(
                                        mActivity, true);
                                //parentView.findViewById(R.id.profile_layout).setVisibility(View.GONE);
                                profileAdded = true;
                                profile_expandLayout.collapse();
                                profile_text_expand.setText(" + ");
                                break;

                            case UPDATE_SKILL:
                                ShineSharedPreferences.setSkillDashFlag(
                                        mActivity, true);
                                skillAdded = true;
                                skill_expandLayout.collapse();
                                skill_text_expand.setText(" + ");
                                //parentView.findViewById(R.id.skill_details).setVisibility(View.GONE);
//                                parentView.findViewById(R.id.skill_sub_layout).setVisibility(View.GONE);
//                                parentView.findViewById(R.id.addMore).setVisibility(View.GONE);

                                break;

//
//                            case "AddSkillDetails":
//                                ShineSharedPreferences.setSkillDashFlag(
//                                        mActivity, true);
//                                parentView.findViewById(R.id.skill_details).setVisibility(View.GONE);
//                                break;
                        }


                        if (ShineSharedPreferences.getExpFlag(mActivity)
                                && ShineSharedPreferences.getEduFlag(mActivity)
                                && ShineSharedPreferences.getSkillFlag(mActivity)) {
                            regPageOk();

                            ShineCommons.trackShineEvents("RegistrationConversion", "ProfileMidoutComplete", mActivity);
                            if (!ShineSharedPreferences.getResumeMidout(mActivity) && ShineSharedPreferences.getEduFlag(mActivity)
                                    && ShineSharedPreferences.getSkillFlag(mActivity) && ShineSharedPreferences.getExpFlag(mActivity)) {
                                if (ShineSharedPreferences.getVendorId(mActivity) != null) {
                                    ShineCommons.trackShineEvents("RegistrationComplete" + ShineSharedPreferences.getVendorId(mActivity), "", mActivity);
                                } else {
                                    ShineCommons.trackShineEvents("RegistrationComplete", "", mActivity);
                                }

                            }
                        }

                    }

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    curr_req--;
                    if (curr_req <= 0) {
                        DialogUtils.dialogDismiss(alertDialog);
                    }
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            container_box.requestFocus();
            if (mActivity != null) {

                mActivity.setTitle(getString(R.string.title_profile_complete));
                ManualScreenTracker.manualTracker("NewRegistrationPage3 ");
            }
            mActivity.hideNavBar();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void changeColor(TextView text) {
        text.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
    }

    private void checkForEmtyParser(ResumeDetails parser) {
        // TODO Auto-generated method stub
        try {

            isParserEmpty = true;

            if (parser == null && isParserEmpty) {
                Log.d("DEBUG::PARSER", "PARSER NULL");

                parentView.findViewById(R.id.text).setVisibility(View.GONE);
                isParserEmpty = true;

            } else {
                if (!ShineSharedPreferences.getExpFlag(mActivity)) {


                    if (parser.getJobs().size() > 0) {
                        if (parser.getJobs().get(0).getCompany_name() != null && parser.getJobs().get(0).getCompany_name().equals("")
                                && !parser.getJobs().get(0).is_current()
                                && parser.getJobs().get(0).getJob_title().equals("")
                                && parser.getJobs().get(0).getStart_month() == 0
                                && parser.getJobs().get(0).getStart_year() == 0 && parser.getJobs().get(0).getEnd_month() == 0
                                && parser.getJobs().get(0).getEnd_month() == 0) {
                            isParserEmpty = true;
                            Log.d("DEBUG::PARSER", "PARSER EXP NULL");
                        } else {
                            isParserEmpty = false;
                            Log.d("DEBUG::PARSER", "PARSER EXP PRESENT");
                        }
                    } else {
                        isParserEmpty = true;
                    }
                }
                if (isParserEmpty && !ShineSharedPreferences.getEduFlag(mActivity)) {
                    if (parser != null && parser.getQualifications().size() > 0) {

                        if (parser != null && (parser.getQualifications().get(0).getEducation_level() == 0
                                && parser.getQualifications().get(0).getInstitute_name().equals("")
                                && parser.getQualifications().get(0).getYear_of_passout() == 0)) {
                            isParserEmpty = true;
                            Log.d("DEBUG::PARSER", "PARSER EDU NULL");
                        } else {
                            isParserEmpty = false;
                            Log.d("DEBUG::PARSER", "PARSER EDU PRESENT");
                        }
                    } else {
                        isParserEmpty = true;
                    }
                }
                if (isParserEmpty && !ShineSharedPreferences.getSkillFlag(mActivity)) {
                    if (parser != null && parser.getSkills().size() > 0) {
                        if (parser.getSkills().get(0).getName().equals("") || parser.getSkills().get(0).getName().equals("null")) {
                            isParserEmpty = true;
                            Log.d("DEBUG::PARSER", "PARSER SKILL NULL");
                        } else {
                            isParserEmpty = false;
                            Log.d("DEBUG::PARSER", "PARSER SKILL PRESENT");
                        }
                    } else {
                        isParserEmpty = true;
                    }
                }
            }

            if (parser != null && isParserEmpty) {
                parentView.findViewById(R.id.text).setVisibility(View.GONE);
                Log.d("DEBUG::PARSER", "PARSER EVERYTHING NULL");

            }


        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void regPageOk() {


        if (Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity))) {
            Intent intent = new Intent(mActivity,
                    HomeActivity.class);

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ShineCommons.trackShineEvents("Add", "ProfileCompleteSuccess", mActivity);
            getActivity().finish();

        } else {
            ShineCommons.trackShineEvents("Add", "ProfileCompleteSuccess", mActivity);
            mActivity.onBackPressed();
        }
//        mActivity.popall();
//
//        if (loginRequestType == URLConfig.REQUESTED_TO_APPLY_JOBS && jobAppleBundle!=null) {
//            mActivity.showJDPageAndApply(jobAppleBundle);
//        }else {
//            mActivity.setFragment(new LoginHomeFrg(),
//                    BaseFragment.MATCHED_JOBS_FRAGMENT);
//        }

    }

    private void addUpdateSkills() {
        parentView.findViewById(R.id.skill_details).setVisibility(View.VISIBLE);
        sub_Layout = (LinearLayout) parentView.findViewById(R.id.skill_sub_layout);
        final TextView addMore = (TextView) parentView.findViewById(R.id.addMore);
        try {
            addMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        View DetailTextView = mActivity.getLayoutInflater().inflate(
                                R.layout.skill_edit_sub_layout, null);
                        for (SkillsHolder holder : skillViewList) {
                            holder.skillDelete.setVisibility(View.VISIBLE);
                        }
                        sub_Layout.setOrientation(LinearLayout.VERTICAL);
                        sub_Layout.addView(DetailTextView);
                        final SkillsHolder holder = new SkillsHolder();
                        holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_skill);
                        holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_experience);
                        holder.skillName = (AutoCompleteTextView) DetailTextView
                                .findViewById(R.id.skill_name);

                        holder.skillName.setAdapter(new SuggestionsFilterableArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                URLConfig.SKILL_SUGGESTION));


                        holder.skillLevel = (EditText) DetailTextView
                                .findViewById(R.id.skill_level_field);
                        holder.skillDelete = (ImageView) DetailTextView
                                .findViewById(R.id.delete_skill);
//                        holder.skillName.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
                        holder.skillDelete.setVisibility(View.VISIBLE);
                        TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
                        TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);
                        holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    addMore.setVisibility(View.VISIBLE);


                                    holder.skillName.setVisibility(View.GONE);
                                    holder.inputLayoutExperience.setVisibility(View.GONE);
                                    holder.inputLayoutSkill.setVisibility(View.GONE);
                                    holder.skillLevel.setVisibility(View.GONE);
                                    holder.skillDelete.setVisibility(View.GONE);
                                    sub_Layout.removeView(holder.skillName);
                                    sub_Layout.removeView(holder.skillLevel);
                                    sub_Layout.removeView(holder.skillDelete);
                                    sub_Layout.removeView(holder.inputLayoutExperience);
                                    sub_Layout.removeView(holder.inputLayoutSkill);
                                    skillViewList.remove(holder);
                                    if (skillViewList.size() == 1) {
                                        skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        holder.skillLevel.setTag(ShineCommons.setTagAndValue(
                                URLConfig.SKILLS_REVRSE_MAP, URLConfig.newSkillList(),
                                0, holder.skillLevel));
                        holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                TextView tv = (TextView) v;
                                DialogUtils.showNewSingleSpinnerRadioDialog(
                                        getString(R.string.hint_exp_years), tv,
                                        URLConfig.newSkillList(), mActivity);
                            }
                        });
                        holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    v.callOnClick();
                                }
                            }
                        });
                        holder.skillModel = new SkillsResultsModel();
                        holder.skillModel.setId("");
                        holder.skillModel.setValue(holder.skillName.getText().toString());
                        holder.skillModel.setYears_of_experience(0);
                        skillViewList.add(holder);

                        if (skillViewList.size() >= 10) {
                            addMore.setVisibility(View.GONE);
                            return;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<SkillsResultsModel> skillModelList = new ArrayList<>();
        try {
            ResumeDetails parser = ShineSharedPreferences
                    .getParserInfo(mActivity, ShineSharedPreferences.getCandidateId(mActivity));
            checkForEmtyParser(parser);

            if (isParserEmpty || parser == null || parser.getSkills() == null || parser.getSkills().size() == 0) {
                SkillsResultsModel skillModel = new SkillsResultsModel();
                skillModel.setId("");
                skillModel.setValue("");
                skillModel.setYears_of_experience(0);
                skillModelList.add(skillModel);
            } else {
                Log.d("DEBUG::UJJ", parser.getSkills().toString());
                for (int i = 0; i < parser.getSkills().size() && i < 5; i++) {
                    SkillsResultsModel skillModel = new SkillsResultsModel();
                    skillModel.setId("");
                    skillModel.setValue(parser.getSkills().get(i).getName());
                    skillModel.setYears_of_experience(parser.getSkills().get(i).getExperience());
                    skillModelList.add(skillModel);
                }
            }

        } catch (Exception e) {
            if (skillModelList.size() == 0) {
                SkillsResultsModel skillModel = new SkillsResultsModel();
                skillModel.setId("");
                skillModel.setValue("");
                skillModel.setYears_of_experience(0);
                skillModelList.add(skillModel);
            }
            e.printStackTrace();
        }


        for (int i = 0; i < skillModelList.size(); i++) {

            final SkillsHolder holder = new SkillsHolder();
            View DetailTextView = mActivity.getLayoutInflater().inflate(
                    R.layout.skill_edit_sub_layout, null);
            holder.skillName = (AutoCompleteTextView) DetailTextView.findViewById(R.id.skill_name);
            holder.skillName.setAdapter(new SuggestionsFilterableArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.SKILL_SUGGESTION));
            holder.skillName.setText(skillModelList.get(i).getValue());
            holder.skillLevel = (EditText) DetailTextView.findViewById(R.id.skill_level_field);
            holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_skill);
            holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_experience);
            TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
            TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);
//            holder.skillName.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);


            int tag;
            System.out.println("--reverse map" + URLConfig.SKILLS_REVRSE_MAP);

            if (skillModelList.get(i).getYears_of_experience() == 2) {
                tag = -1;
                holder.skillLevel.setText("");
            } else {
                tag = ShineCommons.setTagAndValue(URLConfig.SKILLS_REVRSE_MAP,
                        URLConfig.SKILLS_LIST, skillModelList.get(i).getYears_of_experience(),
                        holder.skillLevel);
                tag = tag - 1;
            }
            holder.skillLevel.setTag(tag);
            holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = (TextView) v;
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_years), tv,
                            URLConfig.newSkillList(), mActivity);
                }
            });
            holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        v.callOnClick();
                    }
                }
            });
            holder.skillDelete = (ImageView) DetailTextView
                    .findViewById(R.id.delete_skill);

            holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        addMore.setVisibility(View.VISIBLE);


                        holder.skillName.setVisibility(View.GONE);
                        holder.inputLayoutExperience.setVisibility(View.GONE);
                        holder.inputLayoutSkill.setVisibility(View.GONE);
                        holder.skillLevel.setVisibility(View.GONE);
                        holder.skillDelete.setVisibility(View.GONE);
                        sub_Layout.removeView(holder.skillName);
                        sub_Layout.removeView(holder.skillLevel);
                        sub_Layout.removeView(holder.skillDelete);
                        sub_Layout.removeView(holder.inputLayoutExperience);
                        sub_Layout.removeView(holder.inputLayoutSkill);
                        skillViewList.remove(holder);
                        if (skillViewList.size() == 1) {
                            skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            if (skillModelList.size() > 1) {
                holder.skillDelete.setVisibility(View.VISIBLE);
            }

            sub_Layout.addView(DetailTextView);

            holder.skillModel = skillModelList.get(i);
            skillViewList.add(holder);
        }

    }

    private void showToastAlert(String msg) {
        DialogUtils.showErrorToast(msg);
    }

    private void updateSkillDetails() {

        try {

            JSONArray skillArray = new JSONArray();
            boolean wasError = false;

            for (int i = 0; i < skillViewList.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", skillViewList.get(i).skillModel.getId());
                String skillName = skillViewList.get(i).skillName.getText().toString().trim();
                String levelD = skillViewList.get(i).skillLevel.getText().toString();

                if (skillName.isEmpty() && levelD.isEmpty())

                {
                    wasError = true;
                    skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
//                    skillViewList.get(i).skillName.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                    skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
//                    skillViewList.get(i).skillLevel.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                    continue;
                } else if (skillName.isEmpty()) {
                    wasError = true;
//                    ErrorsScreen.showErrorToast(mActivity, "Please enter a valid skill name");
                    skillViewList.get(i).inputLayoutSkill.setError("Enter skill");
//                    skillViewList.get(i).skillName.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                    continue;
                } else if (levelD.isEmpty()) {
                    wasError = true;
//                    ErrorsScreen.showErrorToast(mActivity, (getString(R.string.exp_year_empty_msg)));
                    skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
//                    skillViewList.get(i).skillLevel.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                    continue;
                }
                obj.put("value", skillName);
                obj.put("years_of_experience", URLConfig.SKILLS_MAP.get(levelD));
                skillArray.put(obj);
            }
            if (wasError) {

                skill_expandLayout.expand();
                skill_text_expand.setText(" _ ");

                Toast.makeText(getActivity(), "Please fill required details", Toast.LENGTH_SHORT).show();
                return;

            }
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            HashMap<String, Object> skillMap = new HashMap<>();

            skillMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
            skillMap.put("skills_data", skillArray);


            String URL = URLConfig.BULK_UPDATE_SKILL_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
            if (!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))) {
                URL = URL + "?vendorid=" + ShineSharedPreferences.getVendorId(mActivity);
            } else {
                URL = URL;
            }
            Type listType = new TypeToken<BulkSkillsResultsModel>() {
            }.getType();


            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, ProfileCompleteFragment.this,
                    URL, listType);

            downloader.setUsePostMethod(skillMap);
            downloader.execute(UPDATE_SKILL);
            curr_req++;

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
