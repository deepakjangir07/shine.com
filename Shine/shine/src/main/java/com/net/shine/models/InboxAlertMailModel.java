package com.net.shine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jaspreet on 3/8/15.
 */
public class InboxAlertMailModel implements Serializable {


    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("next")
    @Expose
    public Object next;
    @SerializedName("previous")
    @Expose
    public Object previous;
    @SerializedName("results")
    @Expose
    public ArrayList<Results> results = new ArrayList<>();



    public static class Results implements Serializable {

        public String id;

        public String candidate_id;

        public String timestamp;

        public String mail_type;

        public int has_read;


        public String alert_name;

        public ArrayList<String> matched_job_details = new ArrayList<>();

        public ArrayList<String> other_job_details = new ArrayList<>();

        public String subject_line;

        public String recruiter_name;



        public  String hash_code;



        public boolean is_new_mail;
        }


}