package com.net.shine.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.AppliedWithoutResumeConfirmation;
import com.net.shine.fragments.components.PersonalDetailComp;
import com.net.shine.interfaces.OTPCallback;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.SuspiciousMobileModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;


public class VerifyUserEmailMobile implements VolleyRequestCompleteListener, OnClickListener {
    private BaseActivity mActivity;
    private Dialog alertDialog;
    private String mobileNo = "";
    private String mode = "";
    private BroadcastReceiver mybroadcast;
    private PersonalDetailComp pdComp;
    private ImageView img;
    private View imageToHide;
    private OTPCallback mCallback;
    public VerifyUserEmailMobile(Activity mActivity) {
        this.mActivity = (BaseActivity) mActivity;
    }
    public VerifyUserEmailMobile(Activity mActivity, View imageToHide) {
        this.mActivity = (BaseActivity) mActivity;
        this.imageToHide = imageToHide;
    }

    public void verifyMobile(PersonalDetailComp comp) {
        try {
            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));


            String getUrl = URLConfig.MOBILE_VERIFY + "?shine_id=" + user.candidate_id + "&mobile_num=" + user.mobile_no;
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, getUrl, type);
            downloader.execute("VerifyUserEmail");
            pdComp = comp;
            mode = "mobile";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifyMobile(String mobile, OTPCallback mCallback) {
        try {
            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));
            mobileNo = mobile;
            Log.d("new number",mobile+"inside verify");

            this.mCallback = mCallback;
            String getUrl = URLConfig.MOBILE_VERIFY + "?shine_id=" + user.candidate_id + "&mobile_num=" + mobile;
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, getUrl, type);
            downloader.execute("VerifyUserEmail");
            mode = "mobilePostApply";


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifyMobileOTP(final SuspiciousMobileModel model, final OTPCallback callback){


        this.model1=model;
        try {
                mCallback = callback;
                alertDialog = DialogUtils.showOTPDialog(mActivity, model.mobile);
                if(mybroadcast == null && ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.RECEIVE_SMS)== PackageManager.PERMISSION_GRANTED)
                {
                    mybroadcast = new SmsReceiver();
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    mActivity.registerReceiver(mybroadcast, filter);
                }
                alertDialog.findViewById(R.id.cross)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    if (mybroadcast != null) {
                                        mActivity.unregisterReceiver(mybroadcast);
                                        mybroadcast = null;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                alertDialog.findViewById(R.id.submit_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mybroadcast != null) {
                            mActivity.unregisterReceiver(mybroadcast);
                            mybroadcast = null;
                        }



                        EditText et = (EditText) alertDialog.findViewById(R.id.otp_et);
                        if(!TextUtils.isEmpty(et.getText().toString().trim())) {
                            model.otp = et.getText().toString().trim();
                        }

                        TextInputLayout inputLayout = (TextInputLayout) alertDialog.findViewById(R.id.input_layout_mo);

                        TextChangeListener.addListener(et, inputLayout, mActivity);
                        if(!TextUtils.isEmpty(et.getText().toString().trim())) {
                            DialogUtils.dialogDismiss(alertDialog);
                            verifyOTPNew(et.getText().toString(), model, mCallback);
                        }
                        else
                        {
                            inputLayout.setError("Please enter valid OTP");


                         }
                        //// FIXME: 01/02/16 Think if we should unregister the receiver here??

                    }
                });

                alertDialog.findViewById(R.id.resend_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.dialogDismiss(alertDialog);
                       resendOTP(model);
                        Toast.makeText(mActivity, "Resend OTP", Toast.LENGTH_SHORT).show();
                    }
                });
            mode = "mobilenew";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    ApplywithoutResumeModel model;
    SuspiciousMobileModel model1;

    public void verifyMobileWithoutResume(ApplywithoutResumeModel model) {
        try {
            this.model = model;
            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));


            String getUrl = URLConfig.VERIFY_OTP_WITHOUT_RESUME.replace(URLConfig.MOBILE_NO, model.mobile)
                    .replace(URLConfig.JOB_ID, model.job_id);
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, getUrl, type);
            HashMap<String, String> paramsMap = new HashMap<>();
            downloader.setUsePutMethod(paramsMap);
            downloader.execute("VerifyUserMobileWithoutResume");
            mode = "mobileWithoutResume";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void verifyOTPNew(String otp, SuspiciousMobileModel model, OTPCallback callback) {
        try {

            this.model1 = model;
            mCallback = callback;
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    "Verifying OTP...");
            Type type = new TypeToken<SuspiciousMobileModel>() {
            }.getType();
            String getUrl = URLConfig.VERIFY_MOBILE_OTP_URL.replace(URLConfig.MOBILE_NO, model1.mobile)+"?otp_code="+otp;
            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    getUrl, type);
            req.execute("verifyMobileOTP");
            mode = "verifyMobileOTP";

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void resendOTP(SuspiciousMobileModel model){

        try {this.model1=model;
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));


            String getUrl = URLConfig.VERIFY_MOBILE_OTP_URL.replace(URLConfig.MOBILE_NO, model.mobile);
            Type type = new TypeToken<SuspiciousMobileModel>() {
            }.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, getUrl, type);
            HashMap<String, String> paramsMap = new HashMap<>();
            downloader.setUsePutMethod(paramsMap);
            downloader.execute("ResendOTP");
            mode = "ResendOTP";

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void verifyEmail(ImageView imageView) {
        try {
            this.img=imageView;
            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));

            //TODO - Ujjawal - Confirm that cadidate id is not needed in this hit
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this, URLConfig.VERIFY_EMAIL_URL, type);
            HashMap<String, String> paramsMap = new HashMap<>();
            paramsMap.put("email", user.email);
            req.setUsePostMethod(paramsMap);
            req.execute("verifyEmail");
            mode = "email";

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void verifyOTPWithoutResume(String otp, ApplywithoutResumeModel model) {
        try {

            this.model = model;
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    "Verifying OTP...");
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.APPLY_WITHOUT_RESUME_URL, type);
            HashMap<String, String> paramsMap = new HashMap<>();
            paramsMap.put("user_id", model.mobile);
            paramsMap.put("job_id", model.job_id);
            paramsMap.put("is_auto_apply", "3");
            paramsMap.put("name", model.name);
            paramsMap.put("email", model.email);
            paramsMap.put("mobile", model.mobile);
            paramsMap.put("application_source", "3");
            paramsMap.put("otp_code", otp);
            req.setUsePostMethod(paramsMap);
            req.execute("applyOTPWithoutResume");
            mode = "applyOTPwithoutResume";

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void verifyOTP(String otp) {
        try {

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    "Verifying OTP...");
            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.MOBILE_VERIFY, type);
            HashMap<String, String> paramsMap = new HashMap<>();
            paramsMap.put("shine_id", ShineSharedPreferences.getCandidateId(mActivity));
            paramsMap.put("auth_code", otp);
            req.setUsePostMethod(paramsMap);
            req.execute("verifyOTP");
            mode = "verifyOTP";

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // TODO: To be revisited once again
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
            case R.id.cancel_btn:case R.id.cross:
                try {
                    DialogUtils.dialogDismiss(alertDialog);
                    if(mybroadcast!=null)
                    {
                        mActivity.unregisterReceiver(mybroadcast);
                        mybroadcast = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }





    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

            DialogUtils.dialogDismiss(alertDialog);


            if (mode.equals("email")) {
                ShineCommons.trackShineEvents("EmailVerify","MyProfile",mActivity);
                alertDialog = DialogUtils.showCustomAlertsNoTitle(
                        mActivity, "An email with a verification link has been sent to your registered email.");
                alertDialog.findViewById(R.id.ok_btn)
                        .setOnClickListener(VerifyUserEmailMobile.this);
            }

            else if(mode.equals("mobile")) {
                alertDialog = DialogUtils.showOTPDialog(mActivity, ShineSharedPreferences.getUserInfo(mActivity)!=null?
                        ShineSharedPreferences.getUserInfo(mActivity).mobile_no:"");

                if(mybroadcast == null && ContextCompat.checkSelfPermission(mActivity, Manifest.permission.RECEIVE_SMS)==
                        PackageManager.PERMISSION_GRANTED)
                {
                    mybroadcast = new SmsReceiver();
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    mActivity.registerReceiver(mybroadcast, filter);
                }
                alertDialog.findViewById(R.id.cross)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    if(mybroadcast!=null)
                                    {
                                        mActivity.unregisterReceiver(mybroadcast);
                                        mybroadcast = null;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                alertDialog.findViewById(R.id.submit_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText et = (EditText) alertDialog.findViewById(R.id.otp_et);
                        TextInputLayout inputLayout = (TextInputLayout) alertDialog.findViewById(R.id.input_layout_mo);

                        TextChangeListener.addListener(et, inputLayout, mActivity);

                        if(!TextUtils.isEmpty(et.getText().toString().trim())) {
                            VerifyingOTP(et.getText().toString());
                        }
                        else
                        {

                            inputLayout.setError("Please enter valid OTP");

                        }
                    }
                });
                alertDialog.findViewById(R.id.resend_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.dialogDismiss(alertDialog);
                        verifyMobile(pdComp);
                        Toast.makeText(mActivity, "Resend OTP", Toast.LENGTH_SHORT).show();
                    }
                });


            }

            else if (mode.equals("applyOTPwithoutResume")){
                DialogUtils.dialogDismiss(alertDialog);
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("apply_model", model);
                AppliedWithoutResumeConfirmation frg = new AppliedWithoutResumeConfirmation();
                frg.setArguments(bundle2);
                mActivity.popone();
                mActivity.popone();
                mActivity.setFragment(frg);


                Toast.makeText(MyApplication.getInstance().getApplicationContext(),
                        "Job Applied Successfully", Toast.LENGTH_SHORT).show();

            }

            else if (mode.equals("verifyMobileOTP")){
                DialogUtils.dialogDismiss(alertDialog);
                ShineCommons.trackShineEvents("MobileVerify","MyProfile",mActivity);
                if(mCallback!=null){
                    mCallback.finalStep(model1.mobile,model1.otp);
                }
            }
            else if (mode.equals("ResendOTP"))
            {
                verifyMobileOTP(model1 ,mCallback);
            }
            else if(mode.equals("verifyOTP")){
                EmptyModel model = (EmptyModel) object;

                alertDialog = DialogUtils.showCustomAlertsNoTitle(
                        mActivity, model.detail);
                alertDialog.findViewById(R.id.ok_btn)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogUtils.dialogDismiss(alertDialog);
                                try {
                                    imageToHide.setVisibility(View.GONE);
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
            else if(mode.equals("mobileWithoutResume"))
            {
                alertDialog = DialogUtils.showOTPDialog(mActivity, model!=null?model.mobile:"");
                if(mybroadcast == null && ContextCompat.checkSelfPermission(mActivity, Manifest.permission.RECEIVE_SMS)== PackageManager.PERMISSION_GRANTED)
                {
                    mybroadcast = new SmsReceiver();
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    mActivity.registerReceiver(mybroadcast, filter);
                }
                alertDialog.findViewById(R.id.cross)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    if (mybroadcast != null) {
                                        mActivity.unregisterReceiver(mybroadcast);
                                        mybroadcast = null;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                alertDialog.findViewById(R.id.submit_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        EditText et = (EditText) alertDialog.findViewById(R.id.otp_et);
                        TextInputLayout inputLayout = (TextInputLayout) alertDialog.findViewById(R.id.input_layout_mo);

                        TextChangeListener.addListener(et, inputLayout, mActivity);
                        if(!TextUtils.isEmpty(et.getText().toString().trim())) {
                            DialogUtils.dialogDismiss(alertDialog);
                            verifyOTPWithoutResume(et.getText().toString(), model);
                        }
                        else
                        {
                            inputLayout.setError("Please enter valid OTP");

                        }
                        //// FIXME: 01/02/16 Think if we should unregister the receiver here??
                    }
                });

                alertDialog.findViewById(R.id.resend_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.dialogDismiss(alertDialog);
                        verifyMobileWithoutResume(model);
                        Toast.makeText(mActivity, "Resend OTP", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if(mode.equals("mobilePostApply")){
                DialogUtils.dialogDismiss(alertDialog);
                alertDialog = DialogUtils.showOTPDialog(mActivity, mobileNo);

                if(mybroadcast == null && ContextCompat.checkSelfPermission(mActivity, Manifest.permission.RECEIVE_SMS)==
                        PackageManager.PERMISSION_GRANTED)
                {
                    mybroadcast = new SmsReceiver();
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    mActivity.registerReceiver(mybroadcast, filter);
                }
                alertDialog.findViewById(R.id.cross)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    DialogUtils.dialogDismiss(alertDialog);
                                    if(mybroadcast!=null)
                                    {
                                        mActivity.unregisterReceiver(mybroadcast);
                                        mybroadcast = null;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                alertDialog.findViewById(R.id.submit_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText et = (EditText) alertDialog.findViewById(R.id.otp_et);
                        TextInputLayout inputLayout = (TextInputLayout) alertDialog.findViewById(R.id.input_layout_mo);

                        TextChangeListener.addListener(et, inputLayout, mActivity);

                        if(!TextUtils.isEmpty(et.getText().toString().trim())) {
                            try {
                                if(mActivity!=null && mybroadcast!=null)
                                {
                                    mActivity.unregisterReceiver(mybroadcast);
                                    mybroadcast = null;
                                }

                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                            final Dialog progressDialog = DialogUtils.showCustomProgressDialog(mActivity,
                                    "Verifying OTP...");
                            Type type = new TypeToken<EmptyModel>() {
                            }.getType();
                            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                                @Override
                                public void OnDataResponse(Object object, String tag) {
                                    DialogUtils.dialogDismiss(progressDialog);
                                    EmptyModel model = (EmptyModel) object;
                                    if (!TextUtils.isEmpty(model.detail)) {
                                        Toast.makeText(mActivity, model.detail+"", Toast.LENGTH_SHORT).show();
                                    }
                                    DialogUtils.dialogDismiss(alertDialog);
                                    mCallback.finalStep("", "");
                                }

                                @Override
                                public void OnDataResponseError(String error, String tag) {
                                    DialogUtils.dialogDismiss(progressDialog);
                                    if (!TextUtils.isEmpty(error)) {
                                        Toast.makeText(mActivity, error, Toast.LENGTH_SHORT).show();
                                    }

                                }
                            },
                                    URLConfig.MOBILE_VERIFY, type);
                            HashMap<String, String> paramsMap = new HashMap<>();
                            paramsMap.put("shine_id", ShineSharedPreferences.getCandidateId(mActivity));
                            paramsMap.put("auth_code", et.getText().toString());
                            req.setUsePostMethod(paramsMap);
                            req.execute("verifyOTPPostApply");
                        }
                        else
                        {

                            inputLayout.setError("Please enter valid OTP");

                        }
                    }
                });
                alertDialog.findViewById(R.id.resend_otp).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.dialogDismiss(alertDialog);
                        verifyMobile(mobileNo,mCallback);
                        Toast.makeText(mActivity, "Resend OTP", Toast.LENGTH_SHORT).show();
                    }
                });

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(final String error , String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    if (error.contains("Email is already verified")) {
                        if (img != null)
                            img.setVisibility(View.GONE);
                    }
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            VerifyUserEmailMobile.this);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void VerifyingOTP(String otp) {
        try {
            if(mActivity!=null && mybroadcast!=null)
            {
                mActivity.unregisterReceiver(mybroadcast);
                mybroadcast = null;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        DialogUtils.dialogDismiss(alertDialog);

        pdComp.VerifyOTP(otp);
    }


    class SmsReceiver extends BroadcastReceiver {
        private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                Bundle myBundle = intent.getExtras();
                SmsMessage[] messages = null;
                String strMessage = "";
                try {
                    if (myBundle != null) {
                        Object[] pdus = (Object[]) myBundle.get("pdus");
                        messages = new SmsMessage[pdus.length];

                        for (int i = 0; i < messages.length; i++) {
                            messages[i] = SmsMessage
                                    .createFromPdu((byte[]) pdus[i]);
                            strMessage += "SMS From: "
                                    + messages[i].getOriginatingAddress();
                            strMessage += " : >>>>>  ";
                            strMessage += messages[i].getDisplayMessageBody();
                            strMessage += "\n";
                        }
                        String sms = strMessage;

                        int otpIndex = sms
                                .indexOf("Shine.com is "); // 13
                        // letters
                        if (otpIndex > 0) {

                            int start = otpIndex + 13;

                            try {
                                if(mode.equals("mobile"))
                                {
                                    String otp = sms.substring(start, start + 4);
                                    VerifyingOTP(otp);
                                }
                                else if (mode.equals("mobilenew"))
                                {
                                    String otp = sms.substring(start,start+6);
                                    verifyOTPNew(otp,model1,mCallback);
                                    DialogUtils.dialogDismiss(alertDialog);
                                }
                                else if (mode.equals("mobileWithoutResume"))
                                {
                                    String otp = sms.substring(start, start + 6);
                                    verifyOTPWithoutResume(otp, model);
                                    DialogUtils.dialogDismiss(alertDialog);
                                }


                                if(mActivity!=null && mybroadcast!=null) {
                                    mActivity.unregisterReceiver(mybroadcast);
                                    mybroadcast = null;
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        // Post a request to server with OTP
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
