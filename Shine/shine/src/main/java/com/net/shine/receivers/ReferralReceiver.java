package com.net.shine.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.moe.pushlibrary.InstallReceiver;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.PostDataThread;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class ReferralReceiver extends BroadcastReceiver {
	private final static String PREFS_FILE_NAME = "ReferralParamsFile";

	@Override
	public void onReceive(Context context, Intent intent) {
		// Workaround for Android security issue:
		// http://code.google.com/p/android/issues/detail?id=16
		try {
			final Bundle extras = intent.getExtras();
			if (extras != null) {
				extras.containsKey(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		// Return if this is not the right intent.
		if (!intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
			return;
		}

		//Rocq tracking
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}

		try{
			InstallReceiver.registerInstallation(context, intent);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		//Shine tracking
		String referrer = intent.getStringExtra("referrer");
		if (referrer == null || referrer.length() == 0) {
			return;
		}
		try {
			referrer = URLDecoder.decode(referrer, "UTF-8"); //$NON-NLS-1$
		} catch (Exception e) {
			return;
		}
		Map<String, String> referralParams = new HashMap<>();
		String[] params = referrer.split("&"); // $NON-NLS-1$
		for (String param : params) {
			String[] pair = param.split("="); // $NON-NLS-1$
			if(pair!=null && pair.length>=2)
			referralParams.put(pair[0], pair[1]);
		}
		ReferralReceiver.storeReferralParams(context, referralParams);
		ShineSharedPreferences.setRefferalFlag(context, false);


		//GA Tracking
		try
		{
			new CampaignTrackingReceiver().onReceive(context, intent);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void storeReferralParams(Context context,
			Map<String, String> params) {
		SharedPreferences storage = context.getSharedPreferences(
				ReferralReceiver.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = storage.edit();
		for (String key : ReferralReceiver.EXPECTED_PARAMETERS) {
			String value = params.get(key);
			if (value != null) {
				editor.putString(key, value);
			}
		}
		editor.commit();


		PostDataThread commetsThread = new PostDataThread(context);
		Thread thread = new Thread(commetsThread);

		thread.start();

	}

	private final static String[] EXPECTED_PARAMETERS = { "utm_source",
			"utm_medium", "utm_term", "utm_content", "utm_campaign", "gclid", "vendorid" };

	public static Map<String, String> retrieveReferralParams1(Context context) {
		HashMap<String, String> params = new HashMap<String, String>();
		SharedPreferences storage = context.getSharedPreferences(
				ReferralReceiver.PREFS_FILE_NAME, Context.MODE_PRIVATE);
		for (String key : ReferralReceiver.EXPECTED_PARAMETERS) {
			String value = storage.getString(key, null);
			if (value != null) {
				params.put(key, value);
			}
		}
		return params;
	}

}
