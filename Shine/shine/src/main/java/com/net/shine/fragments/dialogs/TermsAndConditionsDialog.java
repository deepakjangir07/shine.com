package com.net.shine.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.tracker.ManualScreenTracker;

public class TermsAndConditionsDialog extends DialogFragment {


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {


		View dialogView = LayoutInflater.from(getActivity()).inflate(
				R.layout.terms_conditions_dialog, null);
		WebView termsContent = (WebView) dialogView.findViewById(R.id.terms_content);
		termsContent.setWebViewClient(new WebViewClient());
		termsContent.setHapticFeedbackEnabled(false);
		WebSettings w = termsContent.getSettings();
		w.setLoadWithOverviewMode(true);
		// w.setUseWideViewPort(true);
		w.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		w.setJavaScriptEnabled(true);
		w.setPluginState(PluginState.ON);
		termsContent.loadUrl(URLConfig.TERMS_AND_CONDITION);
		AlertDialog dialog = new AlertDialog.Builder(getActivity()).setView(dialogView).create();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		return dialog;

	}
@Override
public void onStart() {
	// TODO Auto-generated method stub
	super.onStart();
	ManualScreenTracker.manualTracker("T&C");

}

}
