package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.SuggestedJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.ProfileViewMainTabFrg;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.EndlessRecyclerOnScrollListener;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Deepak on 22/12/16.
 */

public class ProfileViewedSimilarJobs extends BaseFragment implements VolleyRequestCompleteListener {


    private Bundle bundle;
    private WhoViewMyProfileModel.Result result;
    private SimpleSearchVO svo;
    private RecyclerView gridView;
    private SuggestedJobsAdapter adapter;
    boolean loadMore = false;
    private View LoadingCmp, parentView;
    private LinearLayout activitylogContainer;
    private int totalJobs;
    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
    private UserStatusModel userProfileVO;
    private String nextUrl = "";
    private TextView view_jobs;


    public static ProfileViewedSimilarJobs newInstance(Bundle args) {
        ProfileViewedSimilarJobs fragment = new ProfileViewedSimilarJobs();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.profile_view_similar_layout, container, false);

        bundle= getArguments();




        result= (WhoViewMyProfileModel.Result) bundle.getSerializable("list_item");

        svo=(SimpleSearchVO)bundle.getSerializable(URLConfig.SEARCH_VO_KEY);

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        activitylogContainer = (LinearLayout) parentView.findViewById(R.id.activity_log_container);




        if(result.activityLog!=null&&result.activityLog.size()>0){

            for(int i=0; i<result.activityLog.size(); i++){
                LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                TextView tv=new TextView(mActivity);
                tv.setLayoutParams(lparams);
                tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_mark_black,0,0,0);
                tv.setCompoundDrawablePadding(15);
                tv.setTextSize(12);
                tv.setText(result.activityLog.get(i));
                activitylogContainer.addView(tv);

            }

        }
        if(result.applicationActivities>0) {


            parentView.findViewById(R.id.jobs_count_layout).setVisibility(View.VISIBLE);

            ((TextView) parentView.findViewById(R.id.jobs_count_textview)).setText(Html
                    .fromHtml("You have applied to " + "<b><font color='#333333'>"
                            + result.applicationActivities + " jobs "
                            + "</font> </b>"
                            + "<font color='#333333'> from </font><b><font color='#333333'> " + result.companyName+"</font></b>"));


        }
        else {
            parentView.findViewById(R.id.jobs_count_layout).setVisibility(View.GONE);



        }

        view_jobs=(TextView)parentView.findViewById(R.id.view_job);
        view_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseFragment fragment = new JobsTabFrg();
                Bundle bundle = new Bundle();
                bundle.putInt(JobsTabFrg.SELECTED_TAB,JobsTabFrg.APPLIED_TAB);
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);

                ShineCommons.trackShineEvents("ProfileViews","View Applied Jobs",mActivity);
            }
        });




        gridView = (RecyclerView) parentView.findViewById(R.id.grid_view);
        gridView.setLayoutManager(new LinearLayoutManager(mActivity));
        EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener() {

            @Override
            public void onLoadMore(int currentPage) {

                if(!TextUtils.isEmpty(nextUrl)&&!loadMore) {
                    loadMore=true;
                    downloadsearchresultJobs(nextUrl, parentView.findViewById(R.id.loading_grid), true);
                }
            }
        };

        gridView.addOnScrollListener(mScrollListener);

        adapter = new SuggestedJobsAdapter(mActivity, jobList);

        gridView.setAdapter(adapter);




        downloadsearchresultJobs(getFirstUrl(),
                parentView.findViewById(R.id.loading_cmp), false);



        return parentView;

    }




    public String getFirstUrl()
    {

        String finalUrl = URLConfig.SEARCH_RESULT_URL_LOGOUT;
        if (userProfileVO != null) {
            finalUrl = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
        }

        return finalUrl +
                "?page=" + "" + 1 +
                "&perpage=" + "" + URLConfig.PER_PAGE
                + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate"
                + "&rect=" + svo.getRect_id();
    }



    private void downloadsearchresultJobs(String url, View loadingView,
                                          boolean flag) {

        try {

            LoadingCmp = loadingView;
            loadMore = flag;
            DialogUtils.showLoading(mActivity, getString(R.string.loading),
                    LoadingCmp);

            if (loadingView.getId() == R.id.loading_cmp)
                gridView.setVisibility(View.GONE);

            Type type = new TypeToken<SimpleSearchModel>() {}.getType();

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, url, type);

            downloader.execute("jobList");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(final Object object, String tag) {


        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (object != null) {

                        if (!svo.isFromSimilarJobs() && !loadMore)
                            jobList = new ArrayList<>();
                        SimpleSearchModel model = (SimpleSearchModel) object;

                        nextUrl = model.next;

                        totalJobs=model.count;


                        for (SimpleSearchModel.Result res : model.results) {
                            int len = res.job_loc_list.size();
                            String loc = "";
                            for (int j = 0; j < len; j++) {
                                if (j >= len - 1) {
                                    loc += res.job_loc_list.get(j);
                                } else {
                                    loc += res.job_loc_list.get(j) + " / ";
                                }
                            }
                            res.job_loc_str = loc;
                        }
                        totalJobs = model.count;
                        jobList.addAll(model.results);
                        loadMore = false;

                        showresult();
                    }
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public  void showresult()
    {
        try {
            if (totalJobs > 0) {
                LoadingCmp.setVisibility(View.GONE);

                if (LoadingCmp.getId() == R.id.loading_cmp)
                    gridView.setVisibility(View.VISIBLE);


                parentView.findViewById(R.id.top_fragment).setVisibility(
                        View.VISIBLE);

                if(result.applicationActivities==0) {
                    ProfileViewMainTabFrg tabchager = (ProfileViewMainTabFrg) getTargetFragment();
                    tabchager.setTabTitle(0, totalJobs + "\n Jobs found ");
                  }
                adapter.rebuildListObject(jobList);
                adapter.notifyDataSetChanged();


            } else {

                    DialogUtils.showErrorMsg(LoadingCmp,
                            mActivity.getString(R.string.no_result_found_msg_profile_views),
                            DialogUtils.ERR_NO_SEARCH_RESULT);
                    gridView.setVisibility(View.GONE);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {

        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    LoadingCmp.setVisibility(View.GONE);

                    DialogUtils.showErrorMsg(LoadingCmp, error, DialogUtils.ERR_TECHNICAL);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
