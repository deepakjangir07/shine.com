package com.net.shine.models;

import com.google.gson.annotations.SerializedName;
import com.net.shine.config.URLConfig;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EmploymentDetailModel implements Serializable, Comparable<EmploymentDetailModel> {
	/**
	 * 
	 */

	@SerializedName("id")
	private String jobId = "";

	@SerializedName("job_title")
	private String jobTitle = "";

	@SerializedName("company_name")
	private String comName = "";

	@SerializedName("industry_id")
	private int industryIndex = -1;

	@SerializedName("is_current")
	private boolean isCurrent=false;

	@SerializedName("start_year")
	private String startYear = "";

	@SerializedName("start_month")
	private String startMonth = "";

	@SerializedName("end_year")
	private String endYear = "";

	@SerializedName("end_month")
	private String endMonth = "";

	@SerializedName("sub_field")
	private int functionalIndex;





	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getEndYear() { return endYear; }

	public void setEndYear(String endYear) { this.endYear = endYear; }

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public int getIndustryIndex() {
		return industryIndex;
	}

	public void setIndustryIndex(int industryIndex) {
		this.industryIndex = industryIndex;
	}

	public int getFunctionalIndex() {
		return functionalIndex;
	}

	public void setFunctionalIndex(int functionalIndex) {
		this.functionalIndex = functionalIndex;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	@Override
	public int compareTo(EmploymentDetailModel employmentDetailModel) {

		EmploymentDetailModel t1 = this;
		EmploymentDetailModel t2 = employmentDetailModel;
		if(t1.isCurrent())
			return -1;
		if(t2.isCurrent())
			return  1;
		try {

		String date1 =  URLConfig.MONTH_NAME_REVERSE_MAP.get(t1.getEndMonth())
				+ " " +t1.getEndYear();
		String date2 =  URLConfig.MONTH_NAME_REVERSE_MAP.get(t2.getEndMonth())
				+ " " +t2.getEndYear();

		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");

			Date d1 = formatter.parse(date1);

			Date d2 = formatter.parse(date2);

//			Log.d("DEBUG::DATE", date1 + " " + date2 + ": " + d1.compareTo(d2));

			return d2.compareTo(d1);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return 0;
	}
}
