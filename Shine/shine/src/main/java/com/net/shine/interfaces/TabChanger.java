package com.net.shine.interfaces;

import android.os.Bundle;

/**
 * Created by ujjawal-work on 16/03/16.
 */
public interface TabChanger {

    void goToTabAt(int pos, Bundle data, int auth_type);

}
