package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.ActionBarState;
import com.net.shine.models.ForgotPasswordModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.regex.Pattern;


public class ForgotPasswordFrg extends BaseFragment implements
        VolleyRequestCompleteListener, View.OnClickListener {
    private Dialog alertDialog;
    private EditText emailField;
    private TextInputLayout inputLayoutForgotPassword;


    public static ForgotPasswordFrg newInstance() {
        
        Bundle args = new Bundle();
        
        ForgotPasswordFrg fragment = new ForgotPasswordFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forgot_password_view, container, false);

        try {
            inputLayoutForgotPassword = (TextInputLayout) view.findViewById(R.id.input_layout_forgot_password);

            emailField = (EditText) view.findViewById(R.id.email_field);
            TextChangeListener.addListener(emailField, inputLayoutForgotPassword, mActivity);


            TextView tvSubmit = (TextView) view
                    .findViewById(R.id.submit_btn_tv);
            tvSubmit.setText("Submit");
            view.findViewById(R.id.submit_btn).setOnClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.submit_btn:
                submit();

                break;

            default:
                break;
        }
    }

    private void submit() {
        try {

            String email = "" + emailField.getText();
            email = email.trim();
            if (email.length() == 0) {
                 inputLayoutForgotPassword.setError("Email ID is required");

                return;
            }

            if (!Pattern.matches(ShineCommons.REGEX, email)) {
                emailField.setTextColor(ContextCompat.getColor(mActivity, R.color.red));
//                emailField.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                inputLayoutForgotPassword.setError("Please enter a valid Email ID");
                return;
            }

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));
            alertDialog.setCancelable(true);


            Type listType = new TypeToken<ForgotPasswordModel>() {
            }.getType();

            HashMap credentialsMap = new HashMap();

            credentialsMap.put("email", email);

            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.FORGOT_PASSWORD, listType);

            request.setUsePostMethod(credentialsMap);

            request.execute("forgotPassword");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showToastAlert(String msg) {
        DialogUtils.showErrorToast(msg);
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.for_pass));
        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
        ManualScreenTracker.manualTracker("ForgotPassword");
       // RocqAnalytics.trackScreen("forgot password screen");


    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        SharedPreferences.Editor outState = mActivity.getSharedPreferences(
                "frag", Context.MODE_APPEND).edit();
        outState.putString("fraginfo", "");

        outState.apply();

    }

    @Override
    public void OnDataResponse(final Object object, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);

                    ForgotPasswordModel fpm = (ForgotPasswordModel) object;

                    if (fpm.getEmail() != null) {

                        Toast.makeText(mActivity, getResources().getString(R.string.password_reset_email_sent), Toast.LENGTH_LONG).show();
                        mActivity.popone();

                    } else {


                        showToastAlert(getResources().getString(R.string.email_not_existing));

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);
                    showToastAlert(error);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
