package com.net.shine.utils;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GmailDataSendListener;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.HashMap;

public class SendtoServerGmail implements VolleyRequestCompleteListener {

    private Context mContext;
    private String data;
    private GmailDataSendListener successCallBackListener;

    public SendtoServerGmail(Context context, String jsondata, GmailDataSendListener listener)
    {
        this.mContext=context;
        this.data=jsondata;
        this.successCallBackListener=listener;
    }


    public void sendtoServerGmailData()
    {
        try {
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("shine_id", ShineSharedPreferences.getCandidateId(mContext));
            obj.put("source","gmail");
            obj.put("data",new JSONArray(data));

            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest request= new VolleyNetworkRequest(mContext,this,
                    URLConfig.KONNECT_GMAIL_URL,type);
            request.setUsePostMethod(obj);
            request.execute("DumpGmailData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {
                successCallBackListener.onSuccessGmail();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        successCallBackListener.onFailedGmail();
    }

}
