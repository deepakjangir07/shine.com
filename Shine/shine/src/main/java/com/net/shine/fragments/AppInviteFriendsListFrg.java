package com.net.shine.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.AppInviteFriendRecyclerViewAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.ShineNonConnectionModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.EndlessRecyclerOnScrollListener;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.EasyTracker;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AppInviteFriendsListFrg extends BaseFragment implements VolleyRequestCompleteListener {

    private RecyclerView list;
    private boolean loadMore = false;
    private boolean isDownloading=false;
    private View LoadingCmp;

    private UserStatusModel userProfileVO;
    private ArrayList<DiscoverModel.Company.CompanyData.Friends> friendLists = new ArrayList<>();

    private AppInviteFriendRecyclerViewAdapter adapter;

    private String nextUrl="";

    public static AppInviteFriendsListFrg newInstance()
    {
        Bundle bundle = new Bundle();
        AppInviteFriendsListFrg fragment = new AppInviteFriendsListFrg();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.d("CLICK","list");
        userProfileVO= ShineSharedPreferences.getUserInfo(mActivity);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.invite_friend, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {

            case R.id.action_invite_friend:
                inviteOtherFriends();
                break;

        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.title_appinvite));
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();
        ManualScreenTracker.manualTracker("App Invite");
    }

    private View parentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.app_invite_friends_list, container, false);

        list= (RecyclerView) parentView.findViewById(R.id.appInvite_friendList);

        EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener() {

            @Override
            public void onLoadMore(int currentPage) {

                if(nextUrl!=null&&!isDownloading&&!loadMore) {
                    loadMore=true;
                    LoadingCmp = parentView.findViewById(R.id.loading_grid);
                    downloadFriendsData(nextUrl, LoadingCmp, true);
                }
            }
        };

        LoadingCmp = parentView.findViewById(R.id.loading_cmp);
        list.setLayoutManager(new LinearLayoutManager(getContext()));

        list.addOnScrollListener(mScrollListener);




        adapter = new AppInviteFriendRecyclerViewAdapter(
                mActivity, friendLists);
        list.setAdapter(adapter);

        String URL = URLConfig.SHINE_NON_CONNECTIONS.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
        System.out.println("--url-my" + URL);
        downloadFriendsData(URL, LoadingCmp, false);

        return parentView;
    }

    public void downloadFriendsData(String URL,View loadingView,
                                    boolean loadmoreflag)
    {

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

        loadMore = loadmoreflag;
        LoadingCmp = loadingView;

        isDownloading=true;

        DialogUtils.showLoading(mActivity, getString(R.string.loading),
                LoadingCmp);
        if (userProfileVO != null) {

            Type listType = new TypeToken<ShineNonConnectionModel>() {
            }.getType();


            System.out.println("--url--" + URL);

            VolleyNetworkRequest volleyNetworkRequest =
                    new VolleyNetworkRequest(getContext(), this, URL, listType);

            if(loadMore){
                volleyNetworkRequest.execute("Shine_Non_Connection_load_more");
            } else {
                volleyNetworkRequest.execute("Shine_Non_Connection");
            }


        }
    }

    public void inviteOtherFriends(){
        ShineCommons.trackShineEvents("InviteApp", "InviteNonConnected", mActivity);

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Shine.com App Invite");

        String msg =  "Download the shine.com mobile app and get the best jobs in the industry! &nbsp;"
                + URLConfig.NON_SHINE_CONNECTION_LINK;

        sharingIntent.putExtra(Intent.EXTRA_TEXT, msg);

        List<Intent> targetedShareIntents = new ArrayList<>();
        List<ResolveInfo> resInfo = mActivity.getPackageManager().queryIntentActivities(sharingIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                String packageName = resolveInfo.activityInfo.packageName;
                Intent targetedShareIntent = new Intent(Intent.ACTION_SEND);

                if (TextUtils.equals(packageName, "com.twitter.android")) {
                    String msgTwitter =
                            "Download the shine.com mobile app"
                                    + URLConfig.NON_SHINE_CONNECTION_LINK;
                    targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msgTwitter);
                } else {
                    targetedShareIntent.putExtra(Intent.EXTRA_TEXT, msg);
                }

                targetedShareIntent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));


                targetedShareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                targetedShareIntent.setType("text/plain");
                targetedShareIntent.putExtra(Intent.EXTRA_SUBJECT, "Shine.com App Invite");
                targetedShareIntent.setPackage(packageName);
                targetedShareIntents.add(targetedShareIntent);

            }
        }
        Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), "Invite Friends Via ");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));

        startActivity(chooserIntent);

    }

    @Override
    public void OnDataResponse(Object object, String tag) {

       ShineNonConnectionModel map = (ShineNonConnectionModel) object;


         if(friendLists==null||!loadMore)
             friendLists=new ArrayList<>();


        LoadingCmp.setVisibility(View.GONE);


        nextUrl=map.next;

        friendLists.addAll(map.results);
        if(LoadingCmp.getVisibility()==View.VISIBLE) {
            LoadingCmp.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }

        if(friendLists.size()>0) {
            adapter.rebuildListObject(friendLists);
        }
        else {
            DialogUtils.showErrorMsg(parentView.findViewById(R.id.loading_cmp), "Currently there are no friends to invite", DialogUtils.ERR_NO_JOBS_WITH_CONNECTION);

        }

        switch (tag){
            case "Shine_Non_Connection_load_more":
                loadMore = false;
                isDownloading = false;
                break;
            case "Shine_Non_Connection":
                isDownloading = false;
                break;
        }


    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        System.out.println("--error--"+error);
        switch (tag){
            case "Shine_Non_Connection_load_more":
                DialogUtils.showErrorMsg(parentView.findViewById(R.id.loading_grid), error, DialogUtils.ERR_NO_IMG);
                loadMore = false;
                isDownloading = false;
                break;
            case "Shine_Non_Connection":
                DialogUtils.showErrorMsg(parentView.findViewById(R.id.loading_cmp), error, DialogUtils.ERR_GENERIC);
                isDownloading = false;
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try{
            EasyTracker.getInstance().sendView("InviteAppFriendList");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
