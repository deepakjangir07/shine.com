package com.net.shine.utils.auth;

import android.content.Intent;

import com.net.shine.activity.BaseActivity;

import java.io.Serializable;

public abstract class ActivityResultHandler implements Serializable {


        public abstract void handleOnActivityResult(BaseActivity mActivity, int requestCode, int resultCode, Intent data);

    }