package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

public class DiscoverModel implements Serializable{

    public int total_count = 0;

    public ArrayList<Company> results = new ArrayList<>();

    public static class Company implements Serializable
    {
        public String company = "";
        public CompanyData data = new CompanyData();

        public static class CompanyData implements Serializable
        {
            public int job_count = 0;
            public String company_uid = "";
            public ArrayList<Friends> friends = new ArrayList<>();
            public String company_image_url = "";

            public static class Friends implements Serializable
            {
                public String name = "";
                public String emails = "";
                public String facebook_url = "";
                public String linkedin_url = "";
                public String phones = "";
                public String shine_id = "";
                public String uid = "";
                public String linkedin_id = "";
                public boolean direct_connection;
                public String studied_together = "";
                public String worked_together = "";
                public String linkedin_image_url = "";

                @Override
                public String toString() {
                    return "Friends{" +
                            "name='" + name + '\'' +
                            ", emails='" + emails + '\'' +
                            ", facebook_url='" + facebook_url + '\'' +
                            ", linkedin_url='" + linkedin_url + '\'' +
                            ", phones='" + phones + '\'' +
                            ", shine_id='" + shine_id + '\'' +
                            ", uid='" + uid + '\'' +
                            ", linkedin_id='" + linkedin_id + '\'' +
                            ", direct_connection=" + direct_connection +
                            ", studied_together='" + studied_together + '\'' +
                            ", worked_together='" + worked_together + '\'' +
                            ", linkedin_image_url='" + linkedin_image_url + '\'' +
                            '}';
                }
            }

        }

    }


}
