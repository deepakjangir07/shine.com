package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gobletsky on 19/12/16.
 */

public class RecentSearchModel implements Serializable {

    public ArrayList<RecentSearchQuery> searches_list;


    public static class RecentSearchQuery implements Serializable {

        public String keyword;

    }

}
