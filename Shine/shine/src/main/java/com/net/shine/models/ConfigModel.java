package com.net.shine.models;

/**
 * Created by Deepak on 15/12/16.
 */
        import java.io.Serializable;


public class ConfigModel implements Serializable {

    public int lookup_version;
    public Android android = new Android();


    public static class Android implements Serializable
    {
        public boolean linkedin_resume_allowed = true;
        public String latest_version = "0";
        public String min_supported = "0";
        public String min_update_flows_version = "0";

    }



}
