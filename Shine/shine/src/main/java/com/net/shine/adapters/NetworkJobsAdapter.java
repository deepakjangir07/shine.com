package com.net.shine.adapters;

import android.os.Bundle;
import android.view.View;

import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.models.SimpleSearchModel;

import java.util.ArrayList;

public class NetworkJobsAdapter extends JobsListRecyclerAdapter {



    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);

        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {


                    if(inActionMode()){
                        onItemClickInActionMode(viewHolder.getAdapterPosition());
                        return;
                    }


                    Bundle bundle = new Bundle();
                    bundle.putInt(URLConfig.INDEX, position);
                    bundle.putSerializable(JobDetailParentFrg.JOB_LIST, mList);
                    BaseFragment fragment = new JobDetailParentFrg();
                    fragment.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment);




                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
    }

    public NetworkJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list,"JobsWithConnection");
    }


}
