package com.net.shine.fragments.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.FilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.ShineCommons;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class SpinnerFragment extends DialogFragment implements View.OnClickListener, AdapterView.OnItemClickListener {


    String title;
    TextView tView;
    List<String> master_list;
    HashMap<String, Integer> indexMap;
    View view;
    ListView lv;
    FilterableArrayAdapter<String> adapter;
    String selectedItemText = "";

    public SpinnerFragment(String title, final TextView tView,
                           final String[] master_list) {
        this.title = title;
        this.tView = tView;
        this.master_list = Arrays.asList(master_list);
        indexMap = new HashMap<>();
        for (Integer i = 0; i < master_list.length; i++) {
            indexMap.put(master_list[i], i);
        }
    }

    BaseActivity mActivity;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = ((BaseActivity) activity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ShineBaseThemeNoOverlay);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.spinner_fragment, container, false);


        try {

            view.findViewById(R.id.update_btn).setVisibility(View.INVISIBLE);

        view.findViewById(R.id.update_btn)
                .setOnClickListener(this);
        view.findViewById(R.id.cancel_btn).setOnClickListener(this);

        TextView tv_title = (TextView) view.findViewById(R.id.header_title);
        tv_title.setText(title);
            lv = (ListView) view.findViewById(R.id.lv_spinner);

        int currentIndex = (int) tView.getTag();
            if (currentIndex<0){
                lv.clearChoices();
                selectedItemText = "";
            }
            else {
                selectedItemText = master_list.get(currentIndex);
            }


        adapter = new FilterableArrayAdapter<>(mActivity, android.R.layout.simple_list_item_single_choice, master_list);
        lv.setAdapter(adapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        lv.setItemChecked(currentIndex, true);
        lv.setOnItemClickListener(this);

        EditText et = (EditText) view.findViewById(R.id.et_spinner_search);

            if(title==null)
                title = "";

            if(title.toLowerCase().contains("salary")
                    || title.toLowerCase().contains("experience")
                    || title.toLowerCase().contains("year"))
            {
                et.setHint("Enter value");
            }


            if(master_list!=null&&master_list.size()<13)
            {
                et.setVisibility(View.GONE);
            }
            else {
                et.setVisibility(View.VISIBLE);
            }


        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                lv.clearChoices();

                adapter.getFilter().filter(s.toString(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int count) {
                        int newSelectedIndex = adapter.getPosition(selectedItemText);
                        if (newSelectedIndex != -1) {
                            lv.setItemChecked(newSelectedIndex, true);
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

            if(currentIndex>5)
            {
                lv.setSelection(currentIndex-2);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        return view;
    }

    @Override
    public void onClick(View v) {
//        super.onClick(v);

        switch (v.getId()) {
            case R.id.update_btn:
                try {
                    mActivity.getSupportFragmentManager().popBackStackImmediate();

                    int selected_pos = indexMap.get(selectedItemText);
                    String text;
                    if (URLConfig.NO_SELECTION
                            .equals(master_list.get(selected_pos))) {
                        text = "";
                    } else {
                        text = master_list.get(selected_pos);
                    }

                    ShineCommons.hideKeyboard(mActivity);
//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
                    tView.setText(text);
                    tView.setTag(selected_pos);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_btn:
                mActivity.popone();
                break;
        }


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedItemText = ((TextView)view).getText().toString();
        lv.setItemChecked(position, true);
        try {
            mActivity.getSupportFragmentManager().popBackStackImmediate();

            int selected_pos = indexMap.get(selectedItemText);
            String text;
            if (URLConfig.NO_SELECTION
                    .equals(master_list.get(selected_pos))) {
                text = "";
            } else {
                text = master_list.get(selected_pos);
            }

            ShineCommons.hideKeyboard(mActivity);
//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
            tView.setText(text);
            tView.setTag(selected_pos);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
