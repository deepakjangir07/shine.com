package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class SkillResult implements Serializable {

    String id = "";
    String candidate_id = "";
    String value = "";
    int years_of_experience;

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(String candidate_id) {
        this.candidate_id = candidate_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getYears_of_experience() {
        return years_of_experience;
    }

    public void setYears_of_experience(int years_of_experience) {
        this.years_of_experience = years_of_experience;
    }
}
