package com.net.shine.fragments.components;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.SuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.EducationDetailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gobletsky on 8/7/15.
 */
public class EducationDetailsEditFragment extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener, View.OnFocusChangeListener {

    private EducationDetailModel eduModel;
    private View view;
    private LinearLayout main_container;
    boolean isAddEdu = false;
    private String ADD_TAG = "AddEducationDetails";
    private String EDIT_TAG = "updateEducationDetails";
    private String GET_TAG = "educationDetails";
    ProgressDialog dialog;
    TextInputLayout inputLayoutQualification, inputLayoutSpecialization, inputLayoutInstituteName, inputLayoutCourseType, inputLayoutPassoutYear;


     String other_specialization="";
    public static EducationDetailsEditFragment newInstance() {

        Bundle args = new Bundle();

        EducationDetailsEditFragment fragment = new EducationDetailsEditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.education_details_edit, container, false);

        TextView tv_title = (TextView) view.findViewById(R.id.header_title);
        tv_title.setText("Education Details");
        main_container = (LinearLayout) view.findViewById(R.id.main_container);
        main_container.requestFocus();

        try {

            dialog = new ProgressDialog(mActivity);
            Bundle bundle = getArguments();
            if(bundle!=null && bundle.containsKey(URLConfig.KEY_MODEL_NAME))
            {
                Gson gson = new Gson();
                String json = bundle.getString(URLConfig.KEY_MODEL_NAME);
                System.out.println("--Json" + json);
                eduModel = gson.fromJson(json, EducationDetailModel.class);
                isAddEdu = false;

                System.out.println("--bundle--" + json);


            }
            else
            {
                System.out.println("--I M IN ELSE--");
                        eduModel = new EducationDetailModel();
                        isAddEdu = true;
                }



                addUpdateEducational();
            TextChangeListener.addListener(qualLevelField,inputLayoutQualification,mActivity);
            TextChangeListener.addListener(eduStreamField,inputLayoutSpecialization,mActivity);
            TextChangeListener.addListener(instituteNameField,inputLayoutInstituteName,mActivity);
            TextChangeListener.addListener(courseTypeField,inputLayoutCourseType,mActivity);
            TextChangeListener.addListener(passoutYearField,inputLayoutPassoutYear,mActivity);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }



        return view;
    }




    EditText qualLevelField;
    EditText eduStreamField;
    AutoCompleteTextView instituteNameField;
    EditText courseTypeField;
    EditText passoutYearField;

    private void addUpdateEducational() {
        try {

            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);


            qualLevelField = (EditText) view.findViewById(R.id.qual_field);
            inputLayoutQualification = (TextInputLayout) view.findViewById(R.id.input_layout_qual);
            inputLayoutSpecialization = (TextInputLayout) view.findViewById(R.id.input_layout_stream);
            inputLayoutInstituteName= (TextInputLayout) view.findViewById(R.id.input_layout_institute_name);
            inputLayoutCourseType= (TextInputLayout) view.findViewById(R.id.input_layout_course_type_name);
            inputLayoutPassoutYear = (TextInputLayout) view.findViewById(R.id.input_layout_passout_year);

            inputLayoutSpecialization.setVisibility(View.GONE);

            Integer[] tag=new Integer[2];
            tag[0]=eduModel.educationNameIndex;
            tag[1]=eduModel.educationStreamIndex;



            other_specialization=eduModel.other_specialization;


            qualLevelField.setTag(ShineCommons.setTagAndValueQualification(
                    URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP,
                    URLConfig.EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP,
                    tag[0],tag[1], qualLevelField,eduModel.other_specialization));



            qualLevelField.setOnClickListener(this);
            qualLevelField.setOnFocusChangeListener(this);

            eduStreamField = (EditText) view
                    .findViewById(R.id.edu_stream_field);
            eduStreamField.setOnClickListener(this);
            eduStreamField.setOnFocusChangeListener(this);

            instituteNameField = (AutoCompleteTextView) view
                    .findViewById(R.id.ins_name_field);
            instituteNameField.setAdapter(new SuggestionsFilterableArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.INSTITUTE_SUGGESTION));
            instituteNameField.setText(eduModel.institueName);

            courseTypeField = (EditText) view
                    .findViewById(R.id.course_type_field);
            courseTypeField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.COURSE_TYPE_REVRSE_MAP,
                    URLConfig.COURSE_TYPE_LIST, eduModel.courseTypeIndex,
                    courseTypeField));
            courseTypeField.setOnClickListener(this);
            courseTypeField.setOnFocusChangeListener(this);

            passoutYearField = (EditText) view
                    .findViewById(R.id.passout_yr_field);
            passoutYearField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.PASSOUT_YEAR_REVRSE_MAP,
                    URLConfig.PASSOUT_YEAR_LIST, eduModel.passoutYearIndex,
                    passoutYearField));
            passoutYearField.setOnClickListener(this);
            passoutYearField.setOnFocusChangeListener(this);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Dialog alertDialog;



    private String editEduID = null;
    private void updateEducationalDetails() {
        try {

            String other_specialization="";
            String qualLevelTV = qualLevelField.getText().toString().trim();
            String eduStreamTV = eduStreamField.getText().toString().trim();
            String instituteNameTV = instituteNameField.getText().toString()
                    .trim();
            String passoutYearTV = passoutYearField.getText().toString().trim();
            String courseTypeTV = courseTypeField.getText().toString().trim();
            boolean isEmpty = false;
            if (qualLevelTV.length() == 0) {
                isEmpty = true;
                 inputLayoutQualification.setError("Please select qualification");
            } else {
                //qualLevelTV = URLConfig.EDUCATIONAL_QUALIFICATION_MAP.get(qualLevelTV);

                Integer[] info = (Integer[]) qualLevelField.getTag();

                Log.d("QualificationTAG","TAG1 "+info[0]);

                Log.d("SpecialiazationTAG","TAG1 "+info[1]);

                qualLevelTV=info[0].toString();
                eduStreamTV=info[1].toString();

                Log.d("DEEPAK","text "+qualLevelField.getText().toString());

                if(info[1]==506)
                {
                    String str = qualLevelField.getText().toString();
                    other_specialization = str.substring(str.indexOf("(")+1,str.indexOf(")"));
                    Log.d("other_special",other_specialization);
                }

            }




            if (instituteNameTV.length() == 0) {
                isEmpty = true;
                 inputLayoutInstituteName.setError("Please enter institutes name");
            }

            if (courseTypeTV.length() == 0) {
                isEmpty = true;
                 inputLayoutCourseType.setError("Please select course type");
            } else {
                courseTypeTV = URLConfig.COURSE_TYPE_MAP.get(courseTypeTV);
            }

            if (passoutYearTV.length() == 0) {
                isEmpty = true;
                 inputLayoutPassoutYear.setError("Please select year of pass-out");
            } else {
                passoutYearTV = URLConfig.PASSOUT_YEAR_MAP.get(passoutYearTV);
            }

            if (isEmpty) {
                DialogUtils.showErrorToast(getResources().getString(R.string.required_fields));
                return;
            }

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            if(isAddEdu){
                String URL= URLConfig.UPDATE_EDU_DETAILS.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity));


                Type listType = new TypeToken<EducationDetailModel>() {}.getType();

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                        URL, listType);


                HashMap updateEducationMap = new HashMap();

                updateEducationMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
                updateEducationMap.put("education_level", qualLevelTV);
                updateEducationMap.put("education_specialization", eduStreamTV);
                updateEducationMap.put("institute_name", instituteNameTV);
                updateEducationMap.put("course_type", courseTypeTV);
                updateEducationMap.put("year_of_passout", passoutYearTV);

                updateEducationMap.put("education_specialization_custom", other_specialization);

                downloader.setUsePostMethod(updateEducationMap);

                downloader.execute(ADD_TAG);}
            else
            {

                String URL= URLConfig.UPDATE_EDU_DETAILS.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity))+eduModel.educationId+"/";

                Type listType = new TypeToken<EducationDetailModel>() {}.getType();

                editEduID = eduModel.educationId;

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                        URL, listType);

                HashMap updateEducationMap = new HashMap();

                updateEducationMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
                updateEducationMap.put("education_level", qualLevelTV);
                updateEducationMap.put("education_specialization", eduStreamTV);
                updateEducationMap.put("institute_name", instituteNameTV);
                updateEducationMap.put("course_type", courseTypeTV);
                updateEducationMap.put("year_of_passout", passoutYearTV);
                updateEducationMap.put("education_specialization_custom", other_specialization);


                downloader.setUsePutMethod(updateEducationMap);

                downloader.execute(EDIT_TAG);}


        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onResume() {
        super.onResume();
        main_container.requestFocus();
        mActivity.setActionBarVisible(false);
        mActivity.setTitle(getString(R.string.title_edit_profile));
    }

    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void OnDataResponse(final Object object, final String tag)
    {
        mActivity.runOnUiThread(new Runnable() {


            @Override
            public void run() {

                DialogUtils.dialogDismiss(dialog);


                DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(mActivity,"Education details updated successfully" , Toast.LENGTH_SHORT).show();

                if(isAddEdu){
                    ShineCommons.trackShineEvents("Add", "Education", mActivity);
                }
                else{
                ShineCommons.trackShineEvents("Edit", "Education", mActivity);
                }
                if (object != null) {

                    if (GET_TAG.equals(tag)) {
                        eduModel = (EducationDetailModel) object;
                        addUpdateEducational();

                    } else {
                        List<EducationDetailModel> list = (List<EducationDetailModel>) DataCaching.getInstance().getObject(mActivity, DataCaching.EDUCATION_LIST_SECTION);

                        EducationDetailModel model = (EducationDetailModel) object;

                        ShineSharedPreferences.setEduDashFlag(mActivity, true);

                        if (ADD_TAG.equals(tag)) {
                            list.add(model);
                        } else if (EDIT_TAG.equals(tag)) {
                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).educationId.equals(editEduID)) {
                                    editEduID = null;
                                    list.remove(i);
                                    list.add(model);
                                }
                            }
                        }

                        Collections.sort(list);

                        if (list.size() > 0) {
                            UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);
                            user.highest_education = (URLConfig.EDUCATIONAL_QUALIFICATION_MAP.get(list.get(0).educationNameIndex + ""));
                            ShineSharedPreferences.saveUserInfo(mActivity, user);
                        }

                        DataCaching.getInstance().cacheData(DataCaching.EDUCATION_LIST_SECTION,
                                "" + DataCaching.CACHE_TIME_LIMIT, list, mActivity);

                        mActivity.popone();

                        Bundle bundle=new Bundle();

                        bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EDUCATION);

                        mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));


                    }

                }


            }
        });
    }




    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(dialog);
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            EducationDetailsEditFragment.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus) {
//            v.getBackground().setColorFilter(Color.parseColor("#72bdf9"), PorterDuff.Mode.SRC_IN);
            showSpinner(v.getId());
            main_container.requestFocus();
        }
    }

    private void showSpinner(int id) {
        try {
            ShineCommons.hideKeyboard(mActivity);

            switch (id) {
                case R.id.qual_field:

                    Integer[] info = (Integer[]) qualLevelField.getTag();

                    Log.d("QualificationTAG","TAG1 "+info[0]);

                    Log.d("SpecialiazationTAG","TAG1 "+info[1]);


                    if(info[1]==506)
                    {
                        String str = qualLevelField.getText().toString();
                        other_specialization = str.substring(str.indexOf("(")+1,str.indexOf(")"));
                    }
                    else
                    {
                        other_specialization="";
                    }
                    DialogUtils.showNewMultiSpinnerwithEdittextDialogRadio(getString(R.string.hint_qual),
                                    qualLevelField, URLConfig.EDUCATIONAL_QUALIFICATION_LIST2,other_specialization,
                                    mActivity);
                    break;

                case R.id.course_type_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_course_type), courseTypeField,
                            URLConfig.COURSE_TYPE_LIST, mActivity);
                    break;
                case R.id.passout_yr_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_passout_year),
                            passoutYearField, URLConfig.PASSOUT_YEAR_LIST,
                            mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_btn:
                updateEducationalDetails();

                break;
            case R.id.cancel_btn:

                mActivity.popone();

                Bundle bundle=new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EDUCATION);
                mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));

                break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
            default:
                showSpinner(v.getId());
                break;
        }
    }
}
