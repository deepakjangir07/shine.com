package com.net.shine.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moe.pushlibrary.MoEHelper;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.config.ServerConfig;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.JobAlertDetailParentFragment;
import com.net.shine.fragments.JobDetailFrg;
import com.net.shine.models.AdModel;
import com.net.shine.models.ConfigModel;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.models.NotificationModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.NewSendFCMIdService;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.LookupChecker;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.GetClientAccessToken;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.LookupInit;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.EasyTracker;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.utils.tracker.PostDataThread;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SplashScreen extends Activity implements
        VolleyRequestCompleteListener, View.OnClickListener {


    MoEHelper mHelper = null;
    public boolean updateDialogShown = false;
    Uri data = null;
    UserStatusModel userProfileVO;
    private Dialog alertDialog;
    public int afterLoginCondition;
    public String search_keyword = "";
    private final static String WHO_VIEWED_MY_PROPFILE = "whoviewed";
    private final static String ACCOUNT_SETTINGS = "accountsettings";
    private final static String DESIRED_JOB = "desiredjob";
    private final static String MY_PROFILE = "myprofile";
    private final static String JOB_ALERTS = "job-alerts";

    String jobs = "jobs"; //Job detail
    String home = "home";  // MatchedJob, Recommended Jobs, Applied Jobs
    String saved_jobs = "shortlisted-jobs"; //Shortlisted jobs
    String create_alerts = "job-alerts"; //CJA
    String activities = "activities"; //whoViewed List
    String recruiter_activities = "recruiter-activities"; //activity by a particular recruiter
    String myProfile = "myprofile";
    String login = "login"; //Login
    String register = "register"; //Register //simple search activity
    String registration = "registration"; // Registration complete frag //uploadresume
    String jobSearch = "job-search"; //Job search
    String inbox= "inbox";
    String apiKey = "AIzaSyBtmK_SIBfhb_hXkgLlfk7IwVlnKZxTb2I";

    private int appVersionCode;

    @Override
    public void startActivity(Intent intent) {
        if (!updateDialogShown) {
            super.startActivity(intent);
        } else {
            delayedIntent = intent;
        }
    }

    public void startActivityFromSuper(Intent intent) {
        super.startActivity(intent);
    }


    public void finish() {
        if (!updateDialogShown) {
            super.finish();
        } else {
            delayedFinish = true;
        }
    }

    private boolean delayedFinish = false;
    private Intent delayedIntent = null;

    public void handleDelayedStuff() {
        if (delayedIntent != null) {
            super.startActivity(delayedIntent);
        }
        if (delayedFinish) {
            super.finish();
        }
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TestFairy.begin(this, "4bcf3872291aeec22f9eebbc9c7ec74086e7a991");

        try {
            //Check for ClientAccessToken
            if (ShineSharedPreferences.getClientAccessToken(this).equals("shine")) {
                new GetClientAccessToken(this).getClientAccessToken();
            }
            setContentView(R.layout.splash_view);

            //update GCM info
            if (checkPlayServices()) {
                Intent intent = new Intent(this, NewSendFCMIdService.class);
                startService(intent);
            }
            Log.d("NON_SKIP_TEST  ","true for vendorid "+ShineSharedPreferences.getVendorId(this));

            //Config api hit

            appVersionCode = ShineCommons.getAppVersionCode(this);
            Type type = new TypeToken<ConfigModel>() {
            }.getType();
            VolleyNetworkRequest req = new VolleyNetworkRequest(this, this, URLConfig.CONFIG_STATUS, type);
            req.execute("SplashConfig");


            //update user Status
            userProfileVO = ShineSharedPreferences.getUserInfo(SplashScreen.this);
            if (userProfileVO != null) {
                hitUserStatusApi(userProfileVO.candidate_id);
            }


            //post third party referral tracking data
            if (!ShineSharedPreferences.getRefferalFlag(this)) {
                Thread thread = new Thread(new PostDataThread(this));
                thread.start();
            }

            mHelper = new MoEHelper(this);

            //track campaign data
            Intent i = getIntent();
            if (i.getData() != null)
                data = i.getData();
            try {
                EasyTracker.getInstance().setContext(this);
                if (data != null) {
                    if (data.getQueryParameter("utm_source") != null) {
                        // Use campaign parameters if available.
                        EasyTracker.getTracker().setCampaign(data.getPath());
                    } else if (data.getQueryParameter("referrer") != null) {
                        // Otherwise, try to find a referrer parameter.
                        EasyTracker.getTracker().setCampaign(data.getPath());
                        //EasyTracker.getTracker().setReferrer(uri.getQueryParameter("referrer"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {

                Intent intent = getIntent();
                String fromNoti = intent.getStringExtra(URLConfig.FROM_NOTIFICATION);
                if (!TextUtils.isEmpty(fromNoti) && fromNoti.equals("true")) {
                    Log.d("inside activity per", "matched job");
                    setActivityAsPerCondition(intent);
                    return;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                if (data != null && data.getHost().equals("get.ht")) {
                    System.out.println("-- i am in  get .ht");

                    new AsyncTask<String, String, String>() {

                        @Override
                        protected String doInBackground(String... params) {

                            try {
                                String dataUrl = data.toString().replaceAll("get.ht", "goo.gl");

                                String uri = "https://www.googleapis.com/urlshortener/v1/url?shortUrl=" + dataUrl
                                        + "&key=" + apiKey;

                                StringBuilder content = new StringBuilder();
                                URL mUrl = new URL(uri);
                                HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                                if (connection.getResponseCode() == 200) {
                                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                    String line;
                                    while ((line = bufferedReader.readLine()) != null)
                                        content.append(line).append("\n");
                                    bufferedReader.close();
                                    Log.d("DEBUG::goo.gl", uri);
                                    System.out.println("-- sb" + content);
                                    JSONObject jobj = new JSONObject(content.toString());
                                    String longUrl = jobj.optString("longUrl");
                                    if (URLUtil.isValidUrl(longUrl)) {
                                        return longUrl;
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            if (URLUtil.isValidUrl(s)) {
                                doStuff(Uri.parse(s));
                            } else {
                                goToHomeScreen();
                            }
                        }
                    }.execute();

                }

                else if(data!=null&&data.getHost().equals("fapp1.jobs.shine.com"))
                {
                    try {
                        ShineCommons.hitJDpixelApi(SplashScreen.this,data.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                else {

                    if (data != null && data.toString().contains("android-app-banner")) {
                        String orig_url = ServerConfig.SERVER_IP_SHINE + data.getQueryParameter("current_page");
                        orig_url = orig_url + "?" + data.getQuery();
                        Log.d("Debug::Banner", orig_url);
                        doStuff(Uri.parse(orig_url));
                    } else if (data != null) {
                        doStuff(data);
                    } else {
                        goToNextScreen();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                goToHomeScreen();
            }

            //Decide and move on to next activity
        } catch (Exception e) {
            e.printStackTrace();
        }


        GetCareerPlusAds();
        NotificationApiHit();
        MoengageTracking.setLoginAttribute(this);
        MoengageTracking.setProfileAttributes(this);


    }

    public void doStuff(Uri data) {

        if (userProfileVO != null) {
            Log.d("USERPROFILE HIT ", "Userprofile hit from spalshScreenViaLinks " + userProfileVO.candidate_id);
            hitUserStatusApi(userProfileVO.candidate_id);
        }


        boolean flag = false;

        if (data != null) {
            if (data.getQueryParameter("utm_source") != null) {
                EasyTracker.getTracker().setCampaign(data.getPath());

            } else if (data.getQueryParameter("referrer") != null) {

                EasyTracker.getTracker().setCampaign(data.getPath());
            }


            //THIS IS for handle referral code in apply
            System.out.println("data--qqq--" + data.getQueryParameter("q"));
            if (data.getQueryParameter("ref_code") != null) {

                try {
                    Log.d("Ref_code ", "ref_code " + data.getQueryParameter("ref_code"));
                    List<String> params = data.getPathSegments();
                    String referral_code = data.getQueryParameter("ref_code");
                    String[] param = params.get(3).split("/");
                    String jobId = param[param.length - 1];

                    ShineSharedPreferences.saveReferralCode(this, jobId, referral_code);

                    navigateToJD(jobId);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        if (data != null && data.getHost() != null) {

            System.out.println("Data-- shine app" + data + "data-host" + data.getHost());

            if ("shineapp".equals(data.getHost())
                    || "www.shine.com".equals(data.getHost())
                    || "m.shine.com".equals(data.getHost())
                    || "sumosc.shine.com".equals(data.getHost())
                    || "sm.shine.com".equals(data.getHost())
                    || "sumosc1.shine.com".equals(data.getHost())
                    || "sm1.shine.com".equals(data.getHost())) {


                List<String> oparams = data.getPathSegments();
                List<String> params = new ArrayList<>();
                params.addAll(oparams);
                if ("myshine".equals(params.get(0))) {
                    params.remove(0);
                }

                if (data.getQueryParameter("webview") != null) {

                    try {
                        Log.d("webview", "webview " + data.getQueryParameter("webview"));
                        Intent i = new Intent(this, CustomWebView.class);
                        i.putExtra("shineurl", data.toString());
                        startActivity(i);

                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    flag = true;

                } else if (jobs.equals(params.get(0)) && params.size() >= 4) {
                    String id = params.get(3);
                    search_keyword = params.get(1);
                    if (!TextUtils.isEmpty(id)) {

                        navigateToJD(id);
                        flag = true;
                    }
                } else if (params.size() > 0
                        && params.get(0).equals(MY_PROFILE)) {
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_PROFILE;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;
                } else if (create_alerts.equals(params.get(0))) {
                    navigateToScreen(ShineAuthUtils.REQUESTED_FOR_JOB_ALERT);
                    flag = true;
                } else if (home.equals(params.get(0))) {

                    if (data.getQueryParameter("type") != null) {
                        String nextParam = data.getQueryParameter("type");
                        if (nextParam.equals("other")) {
                            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_RECOMMEDED_JOBS;
                            if (showLoginDialog()) {
                                navigateToScreen(afterLoginCondition);
                            }
                        } else if (nextParam.equals("applied")) {
                            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_APPLIED_JOBS;
                            if (showLoginDialog()) {
                                navigateToScreen(afterLoginCondition);
                            }
                        }

                    } else {
                        afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_MATCHED_JOB;
                        if (showLoginDialog()) {
                            navigateToScreen(afterLoginCondition);
                        }

                    }
                    flag = true;

                }

                else if(saved_jobs.equals(params.get(0))){
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_SAVED_JOBS;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;

                }


                else if(activities.equals(params.get(0))|| recruiter_activities.equals(params.get(0))){
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_WHO_VIEWED;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;

                }

                else if(inbox.equals(params.get(0))){
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_INBOX_TAB;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;

                }

                else if(myProfile.equals(params.get(0))){
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_PROFILE;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;

                }

                else if (login.equals(params.get(0))) {

                    String nextParam = data.getQueryParameter("next");
                    String appParam = data.getQueryParameter("appParams");
                    Log.d("inside login", "fine");
                    Log.d("params", nextParam + " " + appParam);
                    if (TextUtils.isEmpty(nextParam) && TextUtils.isEmpty(appParam)) {
                        if (ShineSharedPreferences
                                .getUserInfo(SplashScreen.this) == null) {
                            goToLoginScreen();
                        }
                        else
                        {
                            Intent intent;

                            intent = new Intent(SplashScreen.this,
                                    HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(URLConfig.NOTIFICATION_TYPE,
                                    URLConfig.SEARCH_SIMPLE_NOTIFY);

                            startActivity(intent);
                            finish();
                            flag=true;
                        }
                    } else {
                        flag = deepLinkFromMail(nextParam.split("/"), appParam);


                        if (!flag && ShineSharedPreferences
                                .getUserInfo(SplashScreen.this) == null) {
                            goToLoginScreen();
                        }
                    }
                    flag = true;
                } else if (register.equals(params.get(0))
                        || registration.equals(params.get(0))) {
                    if (ShineSharedPreferences
                            .getUserInfo(SplashScreen.this) == null) {

                        gotoRegister();
                        flag = true;
                    }
                } else if ("refer-your-friend".equals(params.get(0))
                        ) {
                    if (ShineSharedPreferences
                            .getUserInfo(SplashScreen.this) != null) {
                        navigateToScreen(ShineAuthUtils.REQUESTED_FOR_REFERRAL_RECEIVED);
                        flag = true;
                    }
                } else if (jobSearch.equals(params.get(0))) {

                    Log.d("job_search_link","inside");

                    if (params.size() >= 2) {
                        Log.d("job_search_link","inside1");

                        if (params.get(1).equals("simple")) {
                            SimpleSearchVO sVO = new SimpleSearchVO();
                            sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);

                            if (params.size() >= 3 && params.get(1).equals("simple")) {
                                sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
                                sVO.setKeyword(params.get(2));
                                if (params.size() == 4) {
                                    sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
                                    sVO.setKeyword(params.get(2));
                                    sVO.setLocation(params.get(3));
                                }
                            }
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                            Intent intent;
                            intent = new Intent(SplashScreen.this, HomeActivity.class);
                            intent.putExtras(bundle);
                            intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY,ShineAuthUtils.REQUESTED_FOR_SEARCH);
                            startActivity(intent);

                        } else {

                            Log.d("job_search_link","inside2");


                            SimpleSearchVO sVO = new SimpleSearchVO();
                            sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
                            sVO.setKeyword(params.get(1));
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                            bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY,ShineAuthUtils.REQUESTED_FOR_SEARCH);
                            Intent intent;
                            intent = new Intent(SplashScreen.this, HomeActivity.class);
                            intent.putExtras(bundle);
                            Log.d("job_search_link","inside2"+bundle);

                            startActivity(intent);
                        }
                    } else {
                        Intent intent;

                        intent = new Intent(SplashScreen.this,
                                HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(URLConfig.NOTIFICATION_TYPE,
                                URLConfig.SEARCH_SIMPLE_NOTIFY);

                        startActivity(intent);
                    }
                    flag = true;
                    finish();

                }

                if (!flag) {
                    goToHomeScreen();
                }
            }


        }
    }


    private void setActivityAsPerCondition(Intent intent) {
        String requestCode = "";


        if (intent.getStringExtra(URLConfig.NOTIFICATION_TYPE) != null) {
            requestCode = intent.getStringExtra(URLConfig.NOTIFICATION_TYPE);


            if (intent.getStringExtra("from_moengage") != null) {

                System.out.println("MOENGAGE");

                try {
                    ShineCommons.trackShineEvents("GCMOpened", "type " + requestCode + "- Moengage", SplashScreen.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                try {
                    ShineCommons.trackShineEvents("GCMOpened", "type " + requestCode + "- GCM", SplashScreen.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("act per cond", "matched");
        if (requestCode.equals(URLConfig.MATCHED_JOBS_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_MATCHED_JOB;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }
        } else if (requestCode.equals(URLConfig.MY_PROFILE_NOTIFI)) {
            Log.d("here in profile", requestCode + "");
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_PROFILE;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }

        } else if (requestCode.equals(URLConfig.MY_PROFILE_VIEW_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_WHO_VIEWED;
            if (showLoginDialog()) {
                    navigateToScreen(afterLoginCondition);

            }
        } else if (requestCode.equals(URLConfig.APPLIED_JOBS_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_APPLIED_JOBS;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }
        } else if (requestCode.equals(URLConfig.DISCOVER_NOTIFY)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_TO_KONNECT;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }

        }

        else if (requestCode.equals(URLConfig.LOGIN_NOTIFI))
            goToLoginScreen();
        else if (requestCode.equals(URLConfig.REGISTER_NOTIFI))
            gotoRegister();

        else if (requestCode.equals(URLConfig.EDUCATION_ADD_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_EDUCATION_ADD;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }

        else if(requestCode.equals(URLConfig.EMPLOYMENT_ADD_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_EMPLOYMENT_ADD;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }

        else if(requestCode.equals(URLConfig.EXPERINCE_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_EXPERIENCE;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }

        else if(requestCode.equals(URLConfig.SKILL_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_SKILL;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }
        else if(requestCode.equals(URLConfig.CERTIFICATION_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_CERTIFICATION;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }
        else if(requestCode.equals(URLConfig.DESIRED_JOB_DETAILS_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_DESIRED_JOB_DETAIL;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }

        else if(requestCode.equals(URLConfig.SAVED_JOBS_NOTIFI)){
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_SAVED_JOBS;
            if(showLoginDialog()){
                navigateToScreen(afterLoginCondition);
            }
        }



        else if (requestCode.equals(URLConfig.REFERRAL_RECEIVED_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_REFERRAL_RECEIVED;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }

        } else if (requestCode.equals(URLConfig.FEEDBACK_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_FEEDBACK;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }
        } else if (requestCode.equals(URLConfig.RESUME_NOTIFY)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_RESUME;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);

            }
        } else if (URLConfig.APP_UPDATE.equals(requestCode)) {
            String vc = intent.getStringExtra(URLConfig.VERSION_CODE);
            if (!TextUtils.isEmpty(vc)) {
                int appVersion = Integer.parseInt(intent.getStringExtra(URLConfig.VERSION_CODE));
                if (appVersion > ShineCommons.getAppVersionCode(getApplicationContext())) {
                    String url = intent.getStringExtra("url");
                    if (!URLUtil.isValidUrl(url)) {
                        url = "market://details?id=com.net.shine";
                        Log.d("URL--gcm", url);
                    }
                    Intent notificationIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    startActivity(notificationIntent);
                    finish();
                    return;
                }
                startActivity(new Intent(this, HomeActivity.class));
                finish();
                return;

            }
            Log.d("URL--gcm", "Already latest app...Opening Home Frg");
            startActivity(new Intent(this, HomeActivity.class));
            finish();
            return;

        }
        else if(requestCode.equals(URLConfig.APP_INVITE)){

            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_APP_INVITE;
            if(showLoginDialog()){

                    navigateToScreen(afterLoginCondition);
            }
        }

        else if (requestCode.equals(URLConfig.JOB_ALERT_NOTIFI)){
            navigateToScreen(ShineAuthUtils.REQUESTED_FOR_JOB_ALERT);
        }

        else if (requestCode.equals(URLConfig.MY_CONNECTION_JOB)) {

            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_CONNECTION_JOBS;
            if (showLoginDialog()) {
                    navigateToScreen(afterLoginCondition);

            }

        } else if (requestCode.equals(URLConfig.MAIL_NOTIFY)) {

            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_RECRUITER_MAIL;
            if (showLoginDialog()) {
                InboxAlertMailModel.Results mailVo = new InboxAlertMailModel.Results();
                String mailId = "";
                if (intent.getStringExtra(URLConfig.MAIL_NOTIFY_MAIL_ID) != null)
                    mailId = intent.getStringExtra(URLConfig.MAIL_NOTIFY_MAIL_ID);
                else if (intent.getStringExtra(URLConfig.MAIL_NOTIFY_OBJ_ID) != null)
                    mailId = intent.getStringExtra(URLConfig.MAIL_NOTIFY_OBJ_ID);
                Log.d("DEBUG::REC_GCM", "MailId: " + mailId);
                mailVo.hash_code = (mailId);
                Bundle bundle = new Bundle();
                bundle.putBoolean(URLConfig.MAIL_TYPE, false);
                ArrayList<InboxAlertMailModel.Results> resultsArrayList = new ArrayList<>();
                resultsArrayList.add(mailVo);
                bundle.putSerializable(
                        JobAlertDetailParentFragment.JOB_ALERT_LIST, resultsArrayList);
                bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY,afterLoginCondition);
                intent = new Intent(this, HomeActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();


            }


        } else if (requestCode.equals(URLConfig.INBOX_JOB_ALERT_NOTIFY)) {
            Log.d("inside inbox", intent.getExtras() + "");
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_ALERT_MAIL;
            if (showLoginDialog()) {
                Bundle b = intent.getExtras();

                if (b.getString(URLConfig.INBOX_JOB_ALERT_ID) != null) {
                    System.out.println("----inbox mail alert" + requestCode + "----mail id" + b.getString(URLConfig.INBOX_JOB_ALERT_ID));
                    InboxAlertMailModel.Results mailVo = new InboxAlertMailModel.Results();
                    mailVo.id = (b.getString(URLConfig.INBOX_JOB_ALERT_ID));
                    System.out.println("jas-mail vo--" + mailVo);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(URLConfig.MAIL_TYPE, true);
                    ArrayList<InboxAlertMailModel.Results> resultsArrayList = new ArrayList<>();
                    resultsArrayList.add(mailVo);
                    bundle.putSerializable(
                            JobAlertDetailParentFragment.JOB_ALERT_LIST, resultsArrayList);
                    bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY,afterLoginCondition);
                    intent = new Intent(this, HomeActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();

                } else {
                    Log.d("DEBUG-GCM", "inbox alert mail id null");
                }
            }
        } else if (requestCode.equals(URLConfig.SEARCH_SIMPLE_NOTIFY)) {
            Bundle bundle = getIntent().getExtras();
            SimpleSearchVO sVO = (SimpleSearchVO) bundle
                    .getSerializable(URLConfig.SEARCH_VO_KEY);
            if (sVO == null) {
                sVO = new SimpleSearchVO();
            }
            String search_key = bundle.getString("search_key");
            String search_loc = bundle.getString("search_loc");
            if (!TextUtils.isEmpty(search_key))
                sVO.setKeyword(search_key);
            if (!TextUtils.isEmpty(search_loc))
                sVO.setLocation(search_loc);
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(URLConfig.SEARCH_VO_KEY,
                    sVO);
            if (!TextUtils.isEmpty(sVO.getKeyword())) {
                intent = new Intent(this, HomeActivity.class);
                intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY,ShineAuthUtils.REQUESTED_FOR_SEARCH);
                intent.putExtras(bundle1);
                startActivity(intent);
                finish();

            } else {
                goToHomeScreen();
            }
        } else if (requestCode.equals(URLConfig.JOB_DETAILS_NOTIFI)) {

            navigateToJD(intent.getStringExtra(URLConfig.JOB_ID_FROM_EMAIL_LINK));

        } else if (requestCode.equals(URLConfig.REFERRAL_REQUEST_NOTIFI)) {
            afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_REFERRAL_REQUESTED;
            if (showLoginDialog()) {
                navigateToScreen(afterLoginCondition);
            }
        }
    }

    private boolean deepLinkFromMail(String[] param, String appParams) {
        {
            Log.d("inside next--", Arrays.toString(param) + "");

            boolean flag = false;

            if ((param.length >= 3
                    && MY_PROFILE.equals(param[2]))|| param[2].equals(ACCOUNT_SETTINGS)
                    || param[2].equals(DESIRED_JOB)) {
                Log.d("inside profile", param[2]);
                afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_PROFILE;
                if (showLoginDialog()) {
                    navigateToScreen(afterLoginCondition);
                }
                flag = true;

            } else if (param[2].equals(JOB_ALERTS)) {
                if (ShineSharedPreferences
                        .getUserInfo(SplashScreen.this) != null) {
                    navigateToScreen(ShineAuthUtils.REQUESTED_FOR_JOB_ALERT);
                    flag = true;
                }
            } else if (param.length >= 3) {
                Log.d("inside_job",param.length+"");
                if (jobs.equals(param[2])) {

                    Log.d("inside_job_inside",param.length+"");

                    if (param.length >= 5) {
                        Log.d("inside_job_inside_more",param.length+"");

                        if ((param[param.length - 1] != null && !"".equals(param[param.length - 1].trim()))) {

                            Log.d("inside_job_inside_more_",param.length+"");
                            navigateToJD(param[param.length - 1]);
                            flag = true;
                        }
                    }
                } else if (jobs.equals(param[1])) {
                    Log.d("inside apply to job", param[4]);
                    String id = param[4];
                    if (!TextUtils.isEmpty(id)) {
                        navigateToJD(id);
                        flag = true;
                    }
                } else if (home.equals(param[2])
                        && appParams != null && appParams.contains("enb_block")
                        && appParams.contains(WHO_VIEWED_MY_PROPFILE)) {
                    Log.d("inside profile view", "dfsfgdhfh");
                    afterLoginCondition = ShineAuthUtils.REQUESTED_FOR_WHO_VIEWED;
                    if (showLoginDialog()) {
                        navigateToScreen(afterLoginCondition);
                    }
                    flag = true;
                }
            } else {
                Log.d("inside final", "home screen");
                goToHomeScreen();
            }
            return flag;
        }
    }

    private void sendPhoneBook() {
        if (!ShineSharedPreferences.getPhoneBookDelivered(this)
                && !ShineSharedPreferences.getPhoneBookReadProgress(this)) {
            Intent intent = new Intent(SplashScreen.this, com.net.shine.services.ContactRetrieveService.class);
            intent.putExtra("isUserAction", false);
            this.startService(intent);
        }
    }

    private void goToNextScreen() {


        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    UserStatusModel userProfileVO = ShineSharedPreferences
                            .getUserInfo(SplashScreen.this);

                    if (userProfileVO != null) {
                        callNextScreen();
                    } else {
                        if (!ShineSharedPreferences
                                .getTutpageVisitedFirstTime(SplashScreen.this)) {
                            Intent in = new Intent(SplashScreen.this,
                                    TutorialImages.class);
                            in.putExtra("TO_MATCH_JOB", 2);
                            startActivity(in);
                        } else {
                            Intent intent = new Intent(SplashScreen.this,
                                    HomeActivity.class);
                            startActivity(intent);
                        }
                        finish();
                    }
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void goToLoginScreen() {
        try {
            try {
                if (alertDialog != null && alertDialog.isShowing()) {

                    alertDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(this, AuthActivity.class);
            intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY,
                    afterLoginCondition);
            intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.LOGIN_FRAG);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoRegister() {
        try {

            Intent intent = new Intent(this, AuthActivity.class);
            intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.REGISTER_FRAG);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void navigateToScreen(int requestType) {
        try {
            Intent intent = new Intent(SplashScreen.this, HomeActivity.class);
            intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY, requestType);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Log.d("create-alert","inside2"+requestType);
            Log.d("create-alert","inside2"+getIntent().getExtras());

            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void navigateToJD(String jobId){

        Log.d("inside_JD_navigate",jobId);
        Intent intent = new Intent(SplashScreen.this,
                HomeActivity.class);
        intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY,ShineAuthUtils.REQUESTES_FOR_JOB_DETAIL);
        intent.putExtra(JobDetailFrg.FROM_SCREEN,
                JobDetailFrg.FROM_EMAIL_LINK);
        intent.putExtra(URLConfig.JOB_ID_FROM_EMAIL_LINK,
                jobId);
        intent.putExtra(URLConfig.JOB_SEARCH_KEYWORD, search_keyword);
        startActivity(intent);
        finish();
    }


    private boolean showLoginDialog() {
        try {
            if (ShineSharedPreferences.getUserInfo(SplashScreen.this) == null) {
                alertDialog = DialogUtils.showCustomLoginAlerts(
                        SplashScreen.this,
                        getString(R.string.not_logged_to_view_who_viewed));
                alertDialog.findViewById(R.id.cancel_btn).setOnClickListener(
                        SplashScreen.this);
                alertDialog.findViewById(R.id.login_btn).setOnClickListener(
                        SplashScreen.this);
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                goToLoginScreen();
                break;
            case R.id.cancel_btn:
                try {
                    if (alertDialog != null && alertDialog.isShowing()) {

                        alertDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                break;
            default:
                break;
        }

    }


    private void callNextScreen() {
        try {
            if (!ShineSharedPreferences.getTutpageVisitedFirstTime(this)) {
                Intent in = new Intent(this, TutorialImages.class);
                in.putExtra("TO_MATCH_JOB", 1);
                startActivity(in);
            }

            else if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(this))&&LoginEventHandler.isHandlingNeeded())
            {

                if(LoginEventHandler.isResumeMidout()) {
                    Intent intent = new Intent(SplashScreen.this, ResumeUploadActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                else if(LoginEventHandler.isProfileMidout())
                {
                    Intent intent = new Intent(SplashScreen.this, ProfileCompleteActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                }
            }

            else {
                Intent intent = new Intent(SplashScreen.this,
                        HomeActivity.class);
                //This line is needed for ROcQ integration
                if (getIntent() != null && getIntent().getExtras() != null)
                    intent.putExtras(getIntent().getExtras());
                startActivity(intent);
            }
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        MoEHelper.getInstance(this).onStart(this);

        ManualScreenTracker.manualTracker("Splash");
    }

    @Override
    protected void onStop() {
        super.onStop();
        MoEHelper.getInstance(this).onStop(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MoEHelper.getInstance(this).onResume(this);

    }

    @Override
    public void OnDataResponse(Object object, String tag) {
        try {
            if (tag.equals("UserStatus")) {
                UserStatusModel model = (UserStatusModel) object;
                ShineSharedPreferences.saveUserInfo(
                        SplashScreen.this, model);
                if (!model.getIsPhonebookSynced() && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    Log.d("DEBUG::PHONEBBOK", "SPLASH SCREEN");
                    sendPhoneBook();
                }

                Log.d("sending_moengage", "splash");

                ShineCommons.sendCustomDimension(model, "Splash");


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {

            if (tag.equals("SplashConfig")) {
                ConfigModel model = (ConfigModel) object;

                Log.d("DEBUG::CONFIG", "Config hit received");
                Log.d("DEBUG::LOOKUP", "Server version " + model.lookup_version);
                Log.d("DEBUG::CONFIG", "Show LinkedInResume " + model.android.linkedin_resume_allowed);
                Log.d("DEBUG::CONFIG", "Show UpdateFlow " + model.android.min_update_flows_version);
                Log.d("DEBUG::UPDATE", "Latest Version " + model.android.latest_version);


                int minimum_supported_version = appVersionCode;
                try {
                    minimum_supported_version = Integer.parseInt(model.android.min_supported);
                    // is an integer!
                } catch (Exception e) {
                    // not an integer!
                    e.printStackTrace();
                }

                int latest_app_version = appVersionCode;
                try {
                    latest_app_version = Integer.parseInt(model.android.latest_version);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ShineSharedPreferences.setLatestAppVersion(this, latest_app_version);


                try {
                    ShineSharedPreferences.setUpdateFlowEnabled(this, appVersionCode >= Integer.parseInt(model.android.min_update_flows_version));
                } catch (Exception e) {
                    ShineSharedPreferences.setUpdateFlowEnabled(this, true);
                    e.printStackTrace();
                }

                ShineSharedPreferences.setLinkedInResumeEnabled(this, model.android.linkedin_resume_allowed);
                ShineSharedPreferences.saveServerLookupVersion(MyApplication.getInstance().getApplicationContext(), model.lookup_version);
                Intent intent = new Intent(BaseActivity.UPDATE_BROADCAST_NOTI);
                Bundle bundle = new Bundle();

                if (minimum_supported_version > appVersionCode) {

                    if (isFinishing()) {
                        bundle.putString(BaseActivity.UPDATE_TYPE, BaseActivity.TYPE_FORCED);
                        intent.putExtras(bundle);
                        Log.d("inside if", "broadcast fired" + bundle);
                        LocalBroadcastManager.getInstance(MyApplication.getInstance()).sendBroadcast(intent);
                    } else {

                        Log.d("inside else", "broadcast not fired");
                        DialogUtils.showUpdateDialog(SplashScreen.this, BaseActivity.TYPE_FORCED);
                        updateDialogShown = true;
                    }

                    return;
                }
                if (latest_app_version > appVersionCode && !ShineSharedPreferences.getUpdateSkip(this)) {
                    if (isFinishing()) {
                        bundle.putString(BaseActivity.UPDATE_TYPE, BaseActivity.TYPE_OPTIONAL);
                        intent.putExtras(bundle);
                        LocalBroadcastManager.getInstance(MyApplication.getInstance()).sendBroadcast(intent);
                    } else {
                        DialogUtils.showUpdateDialog(SplashScreen.this, BaseActivity.TYPE_OPTIONAL);
                        updateDialogShown = true;
                    }
                }

                if (model.lookup_version > LookupInit.CURRENT_FILE_LOOKUP_VERSION) {
                    LookupChecker lookupChecker = new LookupChecker(
                            MyApplication.getInstance().getApplicationContext());
                    lookupChecker
                            .checkVersionTableExists(model.lookup_version);
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        Log.e("error", "Error in response " + error);
    }

    private void hitUserStatusApi(String candidateId) {
        String URL = URLConfig.USER_SERVER_STATUS_URL.replace(URLConfig.CANDIDATE_ID, candidateId) + "?nid=" + ShineSharedPreferences.getFCMRegistrationId(this) +
                "&vc=" + ShineCommons.getAppVersionCode(this) + "&dt=" + URLConfig.DEVICE_ANDROID;
        Type type = new TypeToken<UserStatusModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(this, this, URL, type);
        req.execute("UserStatus");
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        return result == ConnectionResult.SUCCESS;
    }


    private void goToHomeScreen() {
        try {

            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void NotificationApiHit() {
        String url = "";
        if (userProfileVO != null) {

            Log.d("notification_response", "inside hit");

            url = URLConfig.NOTIFICATION_API.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

            Type type = new TypeToken<NotificationModel>() {

            }.getType();

            VolleyNetworkRequest request = new VolleyNetworkRequest(this, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    Log.d("notification_response", object.toString());
                    Gson gson = new Gson();
                    String notificationData = gson.toJson(object);
                    ShineSharedPreferences.saveNotificationData(SplashScreen.this,
                            userProfileVO.candidate_id + "_noti", notificationData);
                    Log.d("notification_response1", notificationData);
                    NotificationCount();
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("Notification", "error " + error);

                }
            }, url, type);

            request.execute("notification");
        }
    }


    public void NotificationCount() {
        if (userProfileVO != null) {
            final String notificationData = ShineSharedPreferences.getNotificationData(this,
                    ShineSharedPreferences.getCandidateId(this) + "_noti");
            if (!TextUtils.isEmpty(notificationData)) {
                Gson gson = new Gson();

                NotificationModel model = gson.fromJson(notificationData, NotificationModel.class);
                if (model != null) {
                    if (model.newData != null) {
                        if (model.newData.jobAlert != null && model.newData.recruiterMail != null
                                && model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(SplashScreen.this,
                                    userProfileVO.candidate_id + "_noti_count", "3");
                        } else if ((model.newData.jobAlert != null && model.newData.recruiterMail != null) ||
                                (model.newData.recruiterMail != null && model.newData.whoViewed != null) ||
                                (model.newData.jobAlert != null && model.newData.whoViewed != null)) {

                            ShineSharedPreferences.saveNotificationCount(SplashScreen.this,
                                    userProfileVO.candidate_id + "_noti_count", "2");
                        } else if (model.newData.jobAlert != null || model.newData.recruiterMail != null || model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(SplashScreen.this,
                                    userProfileVO.candidate_id + "_noti_count", "1");
                        }
                    }
                }
            }
        }
    }


    public void GetCareerPlusAds() {

        String url = "";
        if (userProfileVO != null) {
            url = URLConfig.AD_URL_LOGGEDIN.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
        } else {
            url = URLConfig.AD_URL_LOGOUT;
        }

        Type type = new TypeToken<AdModel>() {
        }.getType();

        VolleyNetworkRequest request = new VolleyNetworkRequest(this, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {

                Gson gson = new Gson();
                String adJson = gson.toJson(object);
                if (userProfileVO != null && !TextUtils.isEmpty(userProfileVO.candidate_id))
                    ShineSharedPreferences.saveCareerPlusAds(SplashScreen.this, userProfileVO.candidate_id, adJson);
                else
                    ShineSharedPreferences.saveCareerPlusAds(SplashScreen.this, "", adJson);


            }

            @Override
            public void OnDataResponseError(String error, String tag) {

                Log.d("ADS", "error " + error);

            }
        }, url, type);

        request.execute("AD");

    }


}
