package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.PostApplyFlowSimilarJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.apache.http.util.TextUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class SimilarJobs extends BottomSheetDialogFragment implements VolleyRequestCompleteListener , GetConnectionListener,PostApplyFlowSimilarJobsAdapter.closeBottomSheet


{

    public BaseActivity mActivity;
    private LinearLayout inviteContainer;
   private TextView inviteText;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }


        private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
        private PostApplyFlowSimilarJobsAdapter adapter_similar;
        private RecyclerView similar_list;
        private String jobId = "";
        private String companyName = "";
        private Set<String> unique_companies = new HashSet<>();
        private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
        private View view;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(false);
        }




    @Override
    public void setupDialog(Dialog dialog, int style) {


            view  = LayoutInflater.from(getContext()).inflate(R.layout.similar_jobs, new LinearLayout(getContext()), false);
            similar_list = (RecyclerView) view.findViewById(R.id.grid_view_similar);
            similar_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            jobList.add(new SimpleSearchModel.Result());
            jobList.add(new SimpleSearchModel.Result());
            adapter_similar = new PostApplyFlowSimilarJobsAdapter(mActivity,jobList,this);
            similar_list.setAdapter(adapter_similar);
            similar_list.setVisibility(View.INVISIBLE);
            TextView appliedText = (TextView) view.findViewById(R.id.apply_thanks);
            View connectBar = view.findViewById(R.id.connect_layout);
            TextView connectButton = (TextView) view.findViewById(R.id.connect);
             inviteContainer = (LinearLayout) view.findViewById(R.id.invite_container);
             inviteText = (TextView) view.findViewById(R.id.link_text);
            inviteText.setText(Html.fromHtml("<font color = '#2c84cc'> Invite your friends </font>"+"<font color = '#333333'> to download the shine app and increase your chances of getting a referral in their companies </font>"));
            Bundle bundle = getArguments();
            jobId = bundle.getString("job_id","");
            companyName = bundle.getString("company_name","");
            boolean isWalkIn = bundle.getBoolean("walk_in", false);
            final String recruiter_url = bundle.getString("recruiter_url", "");

            TextView tv_recruiter_msg = (TextView) view.findViewById(R.id.recruiter_url_msg);




            if(!TextUtils.isEmpty(recruiter_url)){
                tv_recruiter_msg.setVisibility(View.VISIBLE);
                String text = "<font color = '#333333'> You are being redirected to </font> <font color = '#2c84cc'> company website </font> <font color = '#333333'> to complete the application for this job. Checkout similar jobs that you can apply to.</font>";
                tv_recruiter_msg.setText(Html.fromHtml(text));
                tv_recruiter_msg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShineCommons.trackCustomEvents("AppExit", "RedirectApply", mActivity);
                        final Uri uri = Uri.parse(recruiter_url);
                        if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                        {
                            System.out.println("-- package install");
                            ChromeCustomTabs.openCustomTab(mActivity,recruiter_url);
                        }
                        else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            mActivity.startActivity(intent);

                        }
                    }
                });
                view.findViewById(R.id.apply_thanks_layout).setVisibility(View.GONE);
            } else if(isWalkIn){
                tv_recruiter_msg.setVisibility(View.GONE);
                view.findViewById(R.id.apply_thanks_layout).setVisibility(View.VISIBLE);
                appliedText.setText("Thanks for showing your interest. We will send you a reminder 1 day before walk-in.");
            }
            else{
                tv_recruiter_msg.setVisibility(View.GONE);
                view.findViewById(R.id.apply_thanks_layout).setVisibility(View.VISIBLE);
            }


            if (bundle.getBoolean("company_posted",false)) {
                if (!ShineSharedPreferences
                        .getLinkedinImported(mActivity) && !ShineSharedPreferences
                        .getGoogleImported(mActivity)) {
                    connectBar.setVisibility(View.VISIBLE);

                    connectButton.setText("Connect your LinkedIn and Gmail accounts");

                } else if (ShineSharedPreferences.getLinkedinImported(mActivity) &&
                        !ShineSharedPreferences.getGoogleImported(mActivity)) {
                    connectBar.setVisibility(View.VISIBLE);

                    connectButton.setText("Connect your Gmail account");
                } else if (!ShineSharedPreferences.getLinkedinImported(mActivity) &&
                        ShineSharedPreferences.getGoogleImported(mActivity)) {
                    connectBar.setVisibility(View.VISIBLE);

                    connectButton.setText("Connect your LinkedIn account");

                } else {
                    connectBar.setVisibility(View.GONE);
                    if(ShineSharedPreferences.getUserInfo(mActivity)!=null)
                        inviteContainer.setVisibility(View.VISIBLE);
                    else
                        inviteContainer.setVisibility(View.GONE);
                }
            }
            else {
                connectBar.setVisibility(View.GONE);
                if(ShineSharedPreferences.getUserInfo(mActivity)!=null)
                    inviteContainer.setVisibility(View.VISIBLE);
                else
                    inviteContainer.setVisibility(View.GONE);
            }





        Type type = new TypeToken<SimpleSearchModel>() {}.getType();
        String api_url;
        if(TextUtils.isEmpty(ShineSharedPreferences.getCandidateId(getContext()))){
            api_url = URLConfig.SIMILAR_JOBS_URL_LOGOUT;
        } else {
            api_url = URLConfig.SIMILAR_JOBS_URL_LOGIN;
        }

        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                api_url.replace(URLConfig.CANDIDATE_ID,
                        ShineSharedPreferences.getCandidateId(getContext()))+ "&jobid=" + jobId+ "&page="
                        + 1 + "&perpage=2"
                        +  "&date_sort="
                        + URLConfig.NO_OF_DATE
                        + "&fl=id,jCID,jRUrl,is_applied,jCName,jCType,jKwds,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate",
                type);
        DialogUtils.showLoading(mActivity, "Please Wait...", view.findViewById(R.id.loading_cmp));
        downloader.execute("SimilarJobs");

        connectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    BaseFragment fragment = new KonnectFrag();
                    mActivity.singleInstanceReplaceFragment(fragment);
                    SimilarJobs.this.dismiss();

                }
            });

            inviteText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Bundle bundle = new Bundle();
                    bundle.putBoolean("fromInvite", true);
                    BaseFragment fragment = new AppInviteFriendsListFrg();
                    fragment.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment);
                    SimilarJobs.this.dismiss();

                }
            });

        view.findViewById(R.id.similiar_job).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimilarJobs.this.dismiss();
                SimpleSearchVO sVO = new SimpleSearchVO();
                sVO.setJobId(jobId);
                sVO.setFromSimilarJobs(true);
                sVO.setHideModifySearchButton(true);
                String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                sVO.setUrl(URL);
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                BaseFragment fragment = new SearchResultFrg();
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);

                ShineCommons.trackShineEvents("View SimilarJobs","PostApplyFlow-ViewMore",mActivity);
            }
        });







            super.setupDialog(dialog, style);
            dialog.setContentView(view);




            //setMaxHeight to 87.5%
            if(view.getParent()!=null){
                DisplayMetrics displaymetrics = new DisplayMetrics();
                mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                View bottom_sheet =  ((View) view.getParent()).findViewById(android.support.design.R.id.design_bottom_sheet);
                bottom_sheet.setPadding(0, displaymetrics.heightPixels/8, 0, 0);
                bottom_sheet.setBackgroundColor(Color.TRANSPARENT);
                bottom_sheet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SimilarJobs.this.dismiss();

                    }
                });
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                    }
                });




            }


            dialog.setCanceledOnTouchOutside(true);
            final BottomSheetBehavior behavior = BottomSheetBehavior.from((View) view.getParent());
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            behavior.setPeekHeight(100000);
            behavior.setSkipCollapsed(true);

    }


    private void callGetFriend(String comp_names) {

        if(!TextUtils.isEmpty(ShineSharedPreferences.getCandidateId(getContext()))) {
            GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
            friends.getFriendsInCompanyVolleyRequest(comp_names);
        }
    }


        @Override
        public void OnDataResponse(Object object, String tag) {
           try {
               SimpleSearchModel model = (SimpleSearchModel) object;

               for(SimpleSearchModel.Result result: model.results) {
                   result.isInsidePostApply = true;
                   int len = result.job_loc_list.size();
                   String loc = "";
                   for (int j = 0; j < len; j++) {
                       if (j >= len - 1) {
                           loc += result.job_loc_list.get(j);
                       } else {
                           loc += result.job_loc_list.get(j) + " / ";
                       }
                   }
                   result.job_loc_str = loc;
               }

               jobList.clear();
               jobList.addAll(model.results);
               View loading = view.findViewById(R.id.loading_cmp);
               loading.setVisibility(View.GONE);
               if (jobList.size() > 0) {
                   adapter_similar.notifyDataSetChanged();
                   similar_list.setVisibility(View.VISIBLE);
                   String co_names = "";
                   for (int i = 0; i < model.results.size(); i++) {
                       co_names = ShineCommons.getCompanyNames(model.results.get(i).comp_uid_list, unique_companies);
                   }

                   // only call if we have got new companies
                   if (!co_names.equals(""))
                       callGetFriend(co_names);
               } else {
                   DialogUtils.showErrorMsg(loading, "No similar Jobs", DialogUtils.ERR_NO_SEARCH_RESULT);
               }
           }
           catch (Exception e){
               e.printStackTrace();
           }

        }

        @Override
        public void OnDataResponseError(String error, String tag) {
            DialogUtils.showErrorMsg(view.findViewById(R.id.loading_cmp), error, DialogUtils.ERR_GENERIC);
        }
    private void invalidateWithFriendsData() {
        for (int m = 0; m < jobList.size(); m++) {
            SimpleSearchModel.Result job = jobList.get(m);
            if (job == null)
                continue;
            if (job.comp_uid_list != null) {
                for (int n = 0; n < job.comp_uid_list.size(); n++) {
                    if (friend_list.containsKey(job.comp_uid_list.get(n))) {
                        job.frnd_model = friend_list.get(job.comp_uid_list.get(n));
                    }
                }
            }
        }
        adapter_similar.notifyDataSetChanged();
        similar_list.invalidate();
    }


    @Override
    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
        if (!isAdded())
            return;
        try {
            if (result != null) {
                if (friend_list == null)
                    friend_list = new HashMap<>();
                friend_list.putAll(result);

                invalidateWithFriendsData();
            } else {
                Log.e("TAG", "result discover null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCloseAction() {

        SimilarJobs.this.dismiss();


    }
}


