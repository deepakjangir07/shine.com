package com.net.shine.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.CustomWebView;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.fragments.tabs.ReferralTabFrg;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by manishpoddar on 30/06/17.
 */

public class MoreFrag extends BaseFragment implements View.OnClickListener {


    private TextView profilePerformace,konnect,referrals,jobAlerts,inviteFriends,rateApp,
            careerStore,changePassword,signOut,konnect_loggedout,jobAlerts_loggedout,
            careerStrore_loggedout,login,register,badgeCount;
    private LinearLayout notifications, logged_in, logged_out;
    Bundle bundle;
    BaseFragment fragment;

    public static MoreFrag newInstance() {
        Bundle bundle = new Bundle();
        MoreFrag fragment = new MoreFrag();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.more_layout, null);

        mActivity.setTitle("More");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.showSelectedNavButton(mActivity.MORE_OPTION);
        setHasOptionsMenu(true);
        profilePerformace = (TextView) v.findViewById(R.id.profile_performance);
        profilePerformace.setOnClickListener(this);
        konnect = (TextView) v.findViewById(R.id.konnect);
        konnect.setOnClickListener(this);
        referrals = (TextView) v.findViewById(R.id.referrals);
        referrals.setOnClickListener(this);
        jobAlerts = (TextView) v.findViewById(R.id.my_job_alert);
        jobAlerts.setOnClickListener(this);
        inviteFriends = (TextView) v.findViewById(R.id.invite_friends);
        inviteFriends.setOnClickListener(this);
        rateApp = (TextView) v.findViewById(R.id.rate_app);
        rateApp.setOnClickListener(this);
        careerStore = (TextView) v.findViewById(R.id.career_store);
        careerStore.setOnClickListener(this);
        changePassword = (TextView) v.findViewById(R.id.change_password);
        changePassword.setOnClickListener(this);
        signOut = (TextView) v.findViewById(R.id.sign_out);
        signOut.setOnClickListener(this);
        notifications = (LinearLayout) v.findViewById(R.id.notifications);
        notifications.setOnClickListener(this);
        badgeCount = (TextView) v.findViewById(R.id.badge_count);
        try {
            if(ShineSharedPreferences.getUserInfo(mActivity)!=null&&!TextUtils.isEmpty(ShineSharedPreferences.getNotificationCount(mActivity,
                    ShineSharedPreferences.getUserInfo(mActivity).candidate_id+"_noti_count"))){
                badgeCount.setVisibility(View.VISIBLE);
                badgeCount.setText(ShineSharedPreferences.getNotificationCount(mActivity,
                        ShineSharedPreferences.getUserInfo(mActivity).candidate_id+"_noti_count"));
            }
            else {
                badgeCount.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logged_in = (LinearLayout) v.findViewById(R.id.logged_in_more);
        logged_out = (LinearLayout) v.findViewById(R.id.logged_out_more);
        konnect_loggedout = (TextView) v.findViewById(R.id.konnect_loggeedout);
        konnect_loggedout.setOnClickListener(this);

        jobAlerts_loggedout = (TextView) v.findViewById(R.id.my_job_alert_loggedout);
        jobAlerts_loggedout.setOnClickListener(this);

        careerStrore_loggedout = (TextView) v.findViewById(R.id.career_store_loggedout);
        careerStrore_loggedout.setOnClickListener(this);

        login = (TextView) v.findViewById(R.id.login_btn);
        login.setOnClickListener(this);

        register = (TextView) v.findViewById(R.id.register_btn);
        register.setOnClickListener(this);

        TextView version = (TextView) v.findViewById(R.id.version_text);
        TextView version1 = (TextView) v.findViewById(R.id.version_text_1);

        String copyright = "Copyright \u00a9 2017 HT Media Limited";

        version.setText(copyright+ " | V" + ShineCommons.getAppVersionName(getContext()));
        version1.setText(copyright+ " | V" + ShineCommons.getAppVersionName(getContext()));


        if(ShineSharedPreferences.getUserInfo(mActivity)!=null){
            logged_in.setVisibility(View.VISIBLE);
            logged_out.setVisibility(View.GONE);
        }
        else {
            logged_out.setVisibility(View.VISIBLE);
            logged_in.setVisibility(View.GONE);
        }




        return v;
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.profile_performance:
                fragment = MyProfileViewFrg.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.notifications:
                fragment = NotificationsFrag.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                ShineSharedPreferences.saveNotificationCount(mActivity, ShineSharedPreferences.getUserInfo(mActivity)
                        .candidate_id+"_noti_count","");
                ShineCommons.trackShineEvents("Notification","MenuDraw",mActivity);
                break;
            case R.id.konnect:

                if(ShineSharedPreferences.getGoogleImported(mActivity) && ShineSharedPreferences.getLinkedinImported(mActivity))
                {
                    fragment=DiscoverFragmentJobCount.newInstance(new Bundle());
                    mActivity.singleInstanceReplaceFragment(fragment,true);
                }
                else {
                    fragment = KonnectFrag.newInstance(2);
                    mActivity.singleInstanceReplaceFragment(fragment,true);
                }
                    break;

            case R.id.referrals:
                bundle = new Bundle();
                bundle.putInt(MainTabFrg.SELECTED_TAB,ReferralTabFrg.REQUESTED_REF_TAB);
                fragment = ReferralTabFrg.newInstance(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.my_job_alert:
                fragment = MyCustomJobAlertFragment.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.invite_friends:
                fragment = AppInviteFriendsListFrg.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.rate_app:
                fragment = RateTheApp.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.career_store:
                        if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                        {
                            System.out.println("-- package install");
                            ChromeCustomTabs.openCustomTab(mActivity,URLConfig.CARRER_PLUS_LINK);
                        }
                        else {
                            bundle = new Bundle();
                            Intent intent = new Intent(getApplicationContext(),CustomWebView.class);
                            bundle.putString("shineurl", URLConfig.CARRER_PLUS_LINK);
                            intent.putExtras(bundle);
                            startActivity(intent);;

                        }

                        ShineCommons.trackShineEvents("CareerStore", "MenuDraw",mActivity);
                break;
            case R.id.change_password:
                fragment = ChangePasswordFrag.newInstance();
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.sign_out:
                mActivity.logout(getResources().getString(R.string.logout_msg));
                break;
            case  R.id.konnect_loggeedout:
                bundle = new Bundle();
                bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_TO_KONNECT);
                fragment = new LoginFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.my_job_alert_loggedout:
                bundle = new Bundle();
                bundle.putInt("add_or_edit", CustomJobAlertEditFragment.ADD_JOB_ALERT);
                fragment = CustomJobAlertEditFragment.newInstance(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;
            case R.id.career_store_loggedout:
                if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                {
                    System.out.println("-- package install");
                    ChromeCustomTabs.openCustomTab(mActivity,URLConfig.CARRER_PLUS_LINK);
                }
                else {
                    bundle = new Bundle();
                    Intent intent = new Intent(getApplicationContext(),CustomWebView.class);
                    bundle.putString("shineurl", URLConfig.CARRER_PLUS_LINK);
                    intent.putExtras(bundle);
                    startActivity(intent);;

                }

                ShineCommons.trackShineEvents("CareerStore", "MenuDraw",mActivity);
                break;
            case R.id.login_btn:
                bundle = new Bundle();
                bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                fragment = new LoginFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;

            case R.id.register_btn:
                bundle = new Bundle();
                bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                fragment = new RegistrationFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
                break;


        }

    }

    @Override
    public void onResume() {
        super.onResume();

        mActivity.setTitle("More");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showSelectedNavButton(mActivity.MORE_OPTION);
        ManualScreenTracker.manualTracker("More");
    }
}

