package com.net.shine.utils.enrcypt;

/**
 * Created by Deepak on 16/03/17.
 */

public class XOREncryption {



    public static String encryptDecrypt(String input) {

        char[] key = {'e','l', '!','b','o','m','e','n','!','h','$'}; //Can be any chars, and any length array
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < input.length(); i++) {
            output.append((char) (input.charAt(i) ^ key[i % key.length]));
        }

        return output.toString();
    }



}
