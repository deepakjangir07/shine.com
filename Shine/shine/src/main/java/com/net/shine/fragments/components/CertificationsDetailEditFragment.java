package com.net.shine.fragments.components;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.BulkCertificationResultModel;
import com.net.shine.models.CertificationResult;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by ggnf2853 on 18/05/16.
 */
public class CertificationsDetailEditFragment extends BaseFragment implements VolleyRequestCompleteListener, View.OnClickListener {


    private class SkillsHolder {
        public AutoCompleteTextView skillName;
        public TextView skillLevel;
        public ImageView skillDelete;
        public TextInputLayout inputLayoutSkill;
        public TextInputLayout inputLayoutExperience;

        public CertificationResult skillModel;
    }

    private ArrayList<SkillsHolder> skillViewList = new ArrayList<>();
    private Dialog alertDialog;
    private LinearLayout sub_layout;
    private UserStatusModel userProfileVO;

    private final String UPDATE_CERTIFICATION = "updatecertifications";
    //String[] new_list;
    String[] cyear;
    int year = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.skill_edit_comp, container, false);
        view.setVisibility(View.VISIBLE);
        year = Calendar.getInstance().get(Calendar.YEAR);
        System.out.println("--year--" + year);


        int startYear = 1975;
        int yearPlus = 4;
        int yearDiff = year - startYear;
        yearDiff = yearDiff + yearPlus;
        year = year + yearPlus;
        System.out.println("--yeardiff--" + yearDiff);
        cyear = new String[yearDiff];
        System.out.println("--cyearlength--" + cyear.length);


        int j = 0;
        while (startYear < year) {
            cyear[j] = String.valueOf(startYear);
            startYear++;
            j++;
        }

        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        sub_layout = (LinearLayout) view
                .findViewById(R.id.e_details);
        LinearLayout cancelBtn = (LinearLayout) view.findViewById(R.id.cancel_btn);
        TextView heading = (TextView) view.findViewById(R.id.heading);
        heading.setText("Certification Details");
        TextView updateBtn = (TextView) view.findViewById(R.id.updateBtn);
        final TextView addMore = (TextView) view.findViewById(R.id.addMore);
        addMore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                addMore.setVisibility(View.VISIBLE);
                View DetailTextView = mActivity.getLayoutInflater().inflate(
                        R.layout.certification_edit_sub_layout, container, false);


                for (SkillsHolder holder : skillViewList) {
                    holder.skillDelete.setVisibility(View.VISIBLE);
                }

                sub_layout.addView(DetailTextView);

                final SkillsHolder holder = new SkillsHolder();
                holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_certification);
                holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_certification_year);

                holder.skillName = (AutoCompleteTextView) DetailTextView
                        .findViewById(R.id.certification_name);

                holder.skillLevel = (EditText) DetailTextView
                        .findViewById(R.id.certification_year_field);
                holder.skillDelete = (ImageView) DetailTextView.findViewById(R.id.delete_certification);


                holder.skillDelete.setVisibility(View.VISIBLE);
                TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
                TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);

                holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        try {
                            holder.skillName.setVisibility(View.GONE);
                            holder.inputLayoutExperience.setVisibility(View.GONE);
                            holder.inputLayoutSkill.setVisibility(View.GONE);
                            holder.skillLevel.setVisibility(View.GONE);
                            holder.skillDelete.setVisibility(View.GONE);
                            sub_layout.removeView(holder.skillName);
                            sub_layout.removeView(holder.skillLevel);
                            sub_layout.removeView(holder.skillDelete);
                            sub_layout.removeView(holder.inputLayoutExperience);
                            sub_layout.removeView(holder.inputLayoutSkill);
                            skillViewList.remove(holder);
                            if (skillViewList.size() == 1) {
                                skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                            }
                            if (skillViewList.size() <= 9)
                                addMore.setVisibility(View.VISIBLE);
                            else
                                addMore.setVisibility(View.GONE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


                holder.skillLevel.setTag(ShineCommons.setTagAndValue(
                        URLConfig.SKILLS_REVRSE_MAP, URLConfig.SKILLS_LIST,
                        0, holder.skillLevel));

                //  holder.skillLevel.setTag(-1);

                holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        TextView tv = (TextView) v;
                        DialogUtils.showNewSingleSpinnerRadioDialog(
                                getString(R.string.hint_exp_years), tv,
                                cyear, mActivity);
                    }
                });
                holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            v.callOnClick();
                        }
                    }
                });
                holder.skillModel = new CertificationResult();
                holder.skillModel.setId("");
                holder.skillModel.setCertification_name(holder.skillName.getText().toString());
                holder.skillModel.setCertification_year(0);
                skillViewList.add(holder);

            }
        });
        updateBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        ArrayList<CertificationResult> skillModelList = new ArrayList<>();
        if (CertificationDetailComp.skillsList == null || CertificationDetailComp.skillsList.isEmpty()) {
            CertificationResult skillModel = new CertificationResult();
            skillModel.setId("");
            skillModel.setCertification_name("");
            skillModel.setCertification_year(0);
            skillModelList.add(skillModel);
        } else {
            skillModelList.addAll(CertificationDetailComp.skillsList);
        }


        for (int i = 0; i < skillModelList.size(); i++) {

            final SkillsHolder holder = new SkillsHolder();
            View DetailTextView = inflater.inflate(R.layout.certification_edit_sub_layout, container, false);
            holder.skillName = (AutoCompleteTextView) DetailTextView.findViewById(R.id.certification_name);


            holder.skillName.setText(skillModelList.get(i).getCertification_name());
            holder.skillLevel = (EditText) DetailTextView.findViewById(R.id.certification_year_field);
            holder.inputLayoutSkill = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_certification);
            holder.inputLayoutExperience = (TextInputLayout) DetailTextView.findViewById(R.id.input_layout_certification_year);
            TextChangeListener.addListener(holder.skillName, holder.inputLayoutSkill, mActivity);
            TextChangeListener.addListener(holder.skillLevel, holder.inputLayoutExperience, mActivity);
            if (skillModelList.get(i).getCertification_year() == 0) {
                holder.skillLevel.setText("");
            } else {
                holder.skillLevel.setText(skillModelList.get(i).getCertification_year() + "");
            }
            int tag;
            tag = -1;
            holder.skillLevel.setTag(tag);

            holder.skillLevel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = (TextView) v;


                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_years), tv,
                            cyear, mActivity);
                }
            });
            holder.skillLevel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        v.callOnClick();
                    }
                }
            });
            holder.skillDelete = (ImageView) DetailTextView
                    .findViewById(R.id.delete_certification);

            holder.skillDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {


                        holder.skillName.setVisibility(View.GONE);
                        holder.inputLayoutExperience.setVisibility(View.GONE);
                        holder.inputLayoutSkill.setVisibility(View.GONE);
                        holder.skillLevel.setVisibility(View.GONE);
                        holder.skillDelete.setVisibility(View.GONE);
                        sub_layout.removeView(holder.skillName);
                        sub_layout.removeView(holder.skillLevel);
                        sub_layout.removeView(holder.skillDelete);
                        sub_layout.removeView(holder.inputLayoutExperience);
                        sub_layout.removeView(holder.inputLayoutSkill);
                        skillViewList.remove(holder);
                        if (skillViewList.size() == 1) {
                            skillViewList.get(0).skillDelete.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            if (skillModelList.size() > 1) {
                holder.skillDelete.setVisibility(View.VISIBLE);
            }

            sub_layout.addView(DetailTextView);

            holder.skillModel = skillModelList.get(i);
            skillViewList.add(holder);
        }

        return view;
    }


    private void updateCertificationsDetails() {
        try {

            JSONArray skillArray = new JSONArray();
            boolean wasError = false;

            for (int i = 0; i < skillViewList.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("id", skillViewList.get(i).skillModel.getId());
                String skillName = skillViewList.get(i).skillName.getText().toString().trim();
                String levelD = skillViewList.get(i).skillLevel.getText().toString();

                System.out.println("--skill--" + skillViewList.get(i).skillLevel.getText().toString());

                if (skillName.isEmpty() && levelD.isEmpty())

                {
                    wasError = true;
                    skillViewList.get(i).inputLayoutSkill.setError("Enter certification");
                    skillViewList.get(i).inputLayoutExperience.setError("Enter experience");
                    continue;

                } else if (skillName.isEmpty()) {
                    wasError = true;
                    skillViewList.get(i).inputLayoutSkill.setError("Enter Certification");
                    continue;
                } else if (levelD.isEmpty()) {
                    wasError = true;

                    skillViewList.get(i).inputLayoutExperience.setError("Enter year");
                    continue;
                }
                obj.put("certification_name", skillName);
                obj.put("certification_year", levelD);
                skillArray.put(obj);
            }
            if (wasError) {
                Toast.makeText(getActivity(), "Please fill all required details", Toast.LENGTH_SHORT).show();
                return;
            }
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            HashMap<String, Object> skillMap = new HashMap<>();

            skillMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
            skillMap.put("certifications_data", skillArray);


            String URL = URLConfig.BULK_UPDATE_CERTIFICATION.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));

            Type listType = new TypeToken<BulkCertificationResultModel>() {
            }.getType();

            System.out.println("--certification--" + skillMap.toString());

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, CertificationsDetailEditFragment.this,
                    URL, listType);

            downloader.setUsePostMethod(skillMap);
            downloader.execute("UpdateCertification");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.title_edit_profile));
        mActivity.setActionBarVisible(false);
        //mActivity.updateLeftDrawer()();
        ManualScreenTracker.manualTracker("Edit-CertificationDetails");
    }

    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.updateBtn:

                    updateCertificationsDetails();


                    break;

                case R.id.cancel_btn:

                    mActivity.popone();

                    Bundle bundle = new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO, MyProfileFrg.SCROLL_TO_CERTIFICATION);
                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));


                    ShineCommons.hideKeyboard(mActivity);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

            if (object != null) {

                if (tag.equals("UpdateCertification")) {

                    System.out.println("--object--" + object);

                    ShineSharedPreferences.setCertiDashFlag(mActivity, true);

                    BulkCertificationResultModel skillModel = (BulkCertificationResultModel) object;
                    if (CertificationDetailComp.skillsList == null)
                        CertificationDetailComp.skillsList = new ArrayList<>();
                    else
                        CertificationDetailComp.skillsList.clear();
                    for (CertificationResult model : skillModel.certi_data) {
                        CertificationDetailComp.skillsList.add(model);
                    }
                    DataCaching.getInstance().cacheData(DataCaching.CERTIFICATION_LIST_SECTION,
                            "" + DataCaching.CACHE_TIME_LIMIT, CertificationDetailComp.skillsList, mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    Toast.makeText(mActivity, "Certifications updated successfully", Toast.LENGTH_SHORT).show();
                    mActivity.popone();

                    ShineCommons.trackShineEvents("Edit", "Certificate", mActivity);


                    Bundle bundle = new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO, MyProfileFrg.SCROLL_TO_CERTIFICATION);
                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        DialogUtils.dialogDismiss(alertDialog);
        DialogUtils.showErrorToast(error);

    }

}

