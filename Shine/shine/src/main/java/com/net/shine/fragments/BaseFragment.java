package com.net.shine.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.net.shine.activity.BaseActivity;

/**
 * Created by ujjawal-work on 12/08/16.
 */

public abstract class BaseFragment extends Fragment {

    public BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }
}
