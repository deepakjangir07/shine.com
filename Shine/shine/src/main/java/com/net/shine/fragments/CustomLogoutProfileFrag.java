package com.net.shine.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.AuthActivity;
import com.net.shine.utils.auth.ShineAuthUtils;

/**
 * Created by manishpoddar on 28/08/17.
 */

public class CustomLogoutProfileFrag extends BaseFragment {
    private ImageView imageView1;
    private TextView textView1,textView2,textView3,textView4;

    public static CustomLogoutProfileFrag newInstance() {
        Bundle bundle = new Bundle();
        CustomLogoutProfileFrag fragment = new CustomLogoutProfileFrag();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_logout_frg, container, false);
        mActivity.setTitle("Profile");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.showSelectedNavButton(mActivity.PROFILE_OPTION);

        imageView1 = (ImageView) view.findViewById(R.id.imageview1);
        textView1 = (TextView) view.findViewById(R.id.textview1);
        textView2 = (TextView) view.findViewById(R.id.textview2);
        textView3 = (TextView) view.findViewById(R.id.textview3);
        textView4 = (TextView) view.findViewById(R.id.textview4);
        textView1.setText("Keep your profile updated");
        textView2.setText("Increase your chances of getting hired by 3x");
        imageView1.setImageResource(R.drawable.error_profile_logout);

        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(mActivity, AuthActivity.class);
                intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_FOR_PROFILE);
                intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.LOGIN_FRAG);
                mActivity.startActivity(intent);

            }
        });

        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, AuthActivity.class);
                intent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_FOR_PROFILE);
                intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.REGISTER_FRAG);
                mActivity.startActivity(intent);
            }
        });

        return view;
    }
}
