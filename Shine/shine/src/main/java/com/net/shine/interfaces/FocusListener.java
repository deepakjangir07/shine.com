package com.net.shine.interfaces;

import android.support.design.widget.TextInputLayout;

/**
 * Created by ujjawal-work on 07/04/16.
 */
public interface FocusListener {

    void setTILFocusClearer(TextInputLayout parentTextInputLayout);

}
