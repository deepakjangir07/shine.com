package com.net.shine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.net.shine.R;
import com.net.shine.adapters.ProfileViewEmailAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.List;

import static com.net.shine.fragments.JobAlertDetailParentFragment.JOB_ALERT_LIST;

/**
 * Created by Deepak on 21/12/16.
 */

public class ProfileViewEmailFragment extends BaseFragment implements AdapterView.OnItemClickListener {


    private ListView gridView;
    private ProfileViewEmailAdapter adapter;
    private View errorLayout;
    private View view;

    private WhoViewMyProfileModel.Result emailList;


    public static ProfileViewEmailFragment newInstance(Bundle bundle) {
        Bundle args = new Bundle();
        args.putAll(bundle);
        ProfileViewEmailFragment fragment = new ProfileViewEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile_view_email_layout, container, false);
        try {

            Bundle b=getArguments();

            emailList= (WhoViewMyProfileModel.Result) b.getSerializable("list_item");

            List<WhoViewMyProfileModel.MailActivity> mailActivities=emailList.mailActivities;
            gridView = (ListView) view.findViewById(R.id.grid_view);
            errorLayout = view.findViewById(R.id.error_layout);
            errorLayout.setVisibility(View.GONE);


            if(mailActivities.size()>0) {

                adapter = new ProfileViewEmailAdapter(mActivity, mailActivities);
                gridView.setAdapter(adapter);

            }

                else if(emailList.mail_in_past){
                errorLayout.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                BaseFragment fragment = new MyProfileFrg();
                DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                        mActivity.getString(R.string.old_email),
                        mActivity.getString(R.string.no_result_for_email_msg2),
                        mActivity.getString(R.string.update_profile),
                        DialogUtils.ERR_NO_EMAIL,fragment);


                gridView.setVisibility(View.GONE);

            }
            else
                {
                    errorLayout.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.GONE);
                    BaseFragment fragment = new MyProfileFrg();
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.no_result_for_email),
                            mActivity.getString(R.string.no_result_for_email_msg2),
                            mActivity.getString(R.string.update_profile),
                            DialogUtils.ERR_NO_EMAIL,fragment);


                    gridView.setVisibility(View.GONE);


            }
            gridView.setOnItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        ArrayList<InboxAlertMailModel.Results> rec_list=new ArrayList<>();

        InboxAlertMailModel.Results model=new InboxAlertMailModel.Results();

        model.hash_code = emailList.mailActivities.get(i).hashCode;
        model.subject_line = emailList.mailActivities.get(i).subjectLine;

        model.recruiter_name = emailList.mailActivities.get(i).recruiterName;

        rec_list.add(model);

        Bundle bundle = new Bundle();
        bundle.putBoolean(URLConfig.MAIL_TYPE,false);

        bundle.putSerializable(JOB_ALERT_LIST,
                rec_list);

        BaseFragment fragment = new JobAlertDetailParentFragment();
        fragment.setArguments(bundle);
        mActivity.singleInstanceReplaceFragment(fragment);


        ShineCommons.trackShineEvents("ProfileViews","EmailListClick",mActivity);


    }
}
