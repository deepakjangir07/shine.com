package com.net.shine.adapters;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.net.shine.R;
import com.net.shine.activity.CustomWebView;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.KonnectPopupWindow;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;

public class AppInviteFriendRecyclerViewAdapter extends RecyclerView.Adapter<AppInviteFriendRecyclerViewAdapter.CustomViewHolder> {


    public Activity activity;
    public ArrayList<DiscoverModel.Company.CompanyData.Friends> friendLists;


    public AppInviteFriendRecyclerViewAdapter(Activity context, ArrayList<DiscoverModel.Company.CompanyData.Friends> mList)
    {
        this.activity=context;
        this.friendLists=mList;
    }

    public void rebuildListObject(ArrayList<DiscoverModel.Company.CompanyData.Friends> friendLists){
        this.friendLists = friendLists;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        if(friendLists==null)
            return 0;
        return this.friendLists.size();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(activity).inflate(R.layout.phbook_row, parent, false);

        try {
            RecyclerView recyclerView = (RecyclerView) parent;
            return new CustomViewHolder(convertView, recyclerView);
        }catch (Exception e){
            e.printStackTrace();
            return new CustomViewHolder(convertView);
        }

    }

    public  Spanned getMessage()
    {

        return  Html
                .fromHtml("<html><head><title></title></head><body><p>" +
                        "Download the shine.com mobile app and get the best jobs in the industry! &nbsp;"
                        + URLConfig.NON_SHINE_CONNECTION_LINK
                        + "</body></html>");
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        holder.worked_with.setVisibility(View.GONE);
        holder.study_with.setVisibility(View.GONE);

        final DiscoverModel.Company.CompanyData.Friends friends = friendLists.get(position);

        int source = check_connection_source(friends.linkedin_url,
                friends.facebook_url, friends.emails, friends.phones,
                friends.shine_id);

        if (source == 1) {
            holder.worked_with.setText("Connected via LinkedIn");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 2) {
            holder.worked_with.setText("Connected via Facebook");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 3) {
            holder.worked_with.setText("Connected via Email");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 4) {
            holder.worked_with.setText("Connected via Phonebook");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else{
            holder.worked_with.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(friends.linkedin_image_url)) {
            try {

                Glide.with(activity).load(friends.linkedin_image_url).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                        Log.d("glide","exception"+e);
                        ColorGenerator generator = ColorGenerator.MATERIAL;
                        int color = generator.getRandomColor();
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRect(friends.name.substring(0,1).toUpperCase(), color);
                        holder.friendImage.setImageDrawable(drawable);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        Log.d("glide","resource"+resource+"");

                        return false;
                    }
                }).into(holder.friendImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(friends.name.substring(0,1).toUpperCase(), color);
            holder.friendImage.setImageDrawable(drawable);
        }
        holder.btnEmail.setFocusable(false);
        holder.btnEmail.setFocusableInTouchMode(false);
        holder.btnEmail.setDuplicateParentStateEnabled(false);
        String strName = friends.name;
        holder.contactName.setText(strName);

        holder.konnect_text.setText("Invite");
        holder.btnPhone.setVisibility(View.GONE);
        holder.btnShine.setVisibility(View.GONE);



        holder.konnect_text.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {



                v.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View v) {

                    }

                    @Override
                    public void onViewDetachedFromWindow(View v) {
                        v.removeOnAttachStateChangeListener(this);
                        KonnectPopupWindow.dismissInstance();
                    }
                });

                if(holder.recyclerView!=null)
                    holder.recyclerView.smoothScrollToPosition(position);
                try {

                    if(position != friendLists.size()-1)
                    {
                        KonnectPopupWindow.getInstance(activity).show(v, holder.rlmore, false);
                    }
                    else
                    {
                        KonnectPopupWindow.getInstance(activity).show(v, holder.rlmore, true);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });


        if (TextUtils.isEmpty(friends.emails)) {
            holder.rlmore.findViewById(R.id.btnEmail_layout).setVisibility(
                    View.GONE);
        }  else  {
            holder.rlmore.findViewById(R.id.btnEmail_layout).setVisibility(
                    View.VISIBLE);
        }


        if (TextUtils.isEmpty(friends.phones)) {
            holder.rlmore.findViewById(R.id.btnPhone_layout).setVisibility(
                    View.GONE);
            holder.rlmore.findViewById(R.id.btnWhatsapp_layout).setVisibility(
                    View.GONE);
        } else {
            holder.rlmore.findViewById(R.id.btnPhone_layout).setVisibility(
                    View.VISIBLE);
            holder.rlmore.findViewById(R.id.btnWhatsapp_layout).setVisibility(
                    View.VISIBLE);
        }

        if (TextUtils.isEmpty(friends.facebook_url)) {
            holder.rlmore.findViewById(R.id.btnFacebook_layout).setVisibility(
                    View.GONE);
        } else  {
            holder.rlmore.findViewById(R.id.btnFacebook_layout).setVisibility(
                    View.VISIBLE);
        }


        if (TextUtils.isEmpty(friends.linkedin_url)) {
            holder.rlmore.findViewById(R.id.btnLinkdin_layout).setVisibility(
                    View.GONE);
        } else {
            holder.rlmore.findViewById(R.id.btnLinkdin_layout).setVisibility(
                    View.VISIBLE);
        }

        if (TextUtils.isEmpty(friends.shine_id)
                || friends.direct_connection) {
            holder.rlmore.findViewById(R.id.btnShine_layout).setVisibility(
                    View.GONE);
        } else {
            holder.rlmore.findViewById(R.id.btnShine_layout).setVisibility(
                    View.VISIBLE);
        }

        holder.btnEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                ShineCommons.trackShineEvents("InviteApp", "InviteConnectedFriendsViaEmail", activity);

                String emailStr = friends.emails;
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                String subject = "Shine.com App Invite";
                Spanned mailText= getMessage();
                String textToSend = "mailto:" + emailStr + "?subject="
                        + subject + "&body=" + mailText;
                intent.setType("text/html");
                intent.setData(Uri.parse(textToSend));
                KonnectPopupWindow.dismissInstance();
                activity.startActivity(intent);
            }
        });

        holder.btnwhatsapp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                ShineCommons.trackShineEvents("InviteApp", "InviteConnectedFriendsViaWhatsapp", activity);

                if (ShineCommons.appInstalledOrNot("com.whatsapp")) {

                    System.out.println("-- package install");
                    Uri uri = Uri.parse("smsto:" + friends.phones);
                    Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                    i.setPackage("com.whatsapp");
                    ClipboardManager clipboard;
                    ClipData clip;
                    clip = ClipData.newPlainText(
                            "msg",
                            getMessage());

                    clipboard = (ClipboardManager) activity
                            .getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboard.setPrimaryClip(clip);
                    try {
                        KonnectPopupWindow.dismissInstance();
                        activity.startActivity(i);
                        Toast.makeText(activity,
                                "Your message is copied to clipboard",
                                Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        Toast.makeText(activity, "Sorry whatsapp not installed",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {


                        System.out.println("-- package not install "+friends.phones);
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:" +friends.phones));
                        Spanned smsText= getMessage();
                        Log.d("message body",smsText+"");
                        intent.putExtra("sms_body", smsText.toString());
                        activity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        holder.btnLinkdin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                ShineCommons.trackShineEvents("InviteApp", "InviteConnectedFriendsViaLinkedIn", activity);


                {

                    String messageBody = "Download the shine.com mobile app and get the best jobs in the industry!"+ URLConfig.NON_SHINE_CONNECTION_LINK;
                    try{
                        ClipData clip = ClipData.newPlainText("msg",messageBody);
                        ClipboardManager clipboard;
                        clipboard = (ClipboardManager) (activity)
                                .getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setPrimaryClip(clip);
                        ShineCommons.trackCustomEvents("AppExit", "LinkedIn", activity);
                        Toast.makeText(activity,"Message copied to clipboard",Toast.LENGTH_SHORT).show();

                        System.out.println("--linkedin-url"+friends.linkedin_url);


                        if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                        {
                            System.out.println("-- package install");
                            ChromeCustomTabs.openCustomTab(activity,friends.linkedin_url);
                        }
                        else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(activity,CustomWebView.class);
                            bundle.putString("shineurl",friends.linkedin_url);
                            intent.putExtras(bundle);
                            activity.startActivity(intent);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });


    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{

        public LinearLayout ll;
        public ImageView friendImage;
        public TextView worked_with, study_with;
        public TextView contactName;
        public View rlmore;
        public View btnPhone;
        public View btnwhatsapp;
        public ImageButton btnLinkdin;
        public ImageButton btnShine;
        public View btnEmail;
        public View konnect;
        public TextView konnect_text;
        public RecyclerView recyclerView;

        public CustomViewHolder(View convertView, RecyclerView recyclerView){

            this(convertView);
            this.recyclerView = recyclerView;

        }



        public CustomViewHolder(View convertView) {
            super(convertView);
            ll = (LinearLayout) convertView
                    .findViewById(R.id.contact_info);
            friendImage = (ImageView) convertView
                    .findViewById(R.id.image_thumbnail);
            worked_with = (TextView) ll.findViewById(R.id.work_connection);
            study_with = (TextView) ll.findViewById(R.id.study_connection);
            contactName = (TextView) ll.findViewById(R.id.ContactName);
            rlmore = LayoutInflater.from(activity).inflate(R.layout.konnect_more_layout, new LinearLayout(activity), false);
            btnPhone = rlmore.findViewById(R.id.btnPhone);
            btnwhatsapp = rlmore.findViewById(R.id.btnWhatsapp);
            btnLinkdin = (ImageButton) rlmore
                    .findViewById(R.id.btnLinkdin);
            btnShine = (ImageButton) rlmore
                    .findViewById(R.id.btnShine);
            btnEmail = rlmore.findViewById(R.id.btnEmail);
            konnect = convertView.findViewById(R.id.konnect);
            konnect_text = (TextView) convertView.findViewById(R.id.konnect_text);
        }
    }


    private int check_connection_source(String linkedin, String facebook,
                                        String email, String phone_number, String shine_id) {
        if (linkedin != null && (!linkedin.trim().equals("")))
            return 1;
        else if (facebook != null && (!facebook.trim().equals("")))
            return 2;
        else if (email != null && (!email.trim().equals("")))
            return 3;
        else if (phone_number != null && (!phone_number.trim().equals("")))
            return 4;
        else if (shine_id != null && (!shine_id.trim().equals("")))
            return 5;
        else
            return 6;
    }



}