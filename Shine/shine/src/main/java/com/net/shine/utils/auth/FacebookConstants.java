package com.net.shine.utils.auth;

import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.net.shine.MyApplication;
import com.net.shine.activity.BaseActivity;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.utils.DialogUtils;

import java.util.Collections;

/**
 * Created by ujjawal-work on 25/05/16.
 */

public class FacebookConstants {



    public static ActivityResultHandler FacebookConnect(final BaseFragment mFragment, final FacebookCallBack mCallback){


        final CallbackManager callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().logOut();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        final Dialog plzWait = DialogUtils.showCustomProgressDialog(mFragment.getActivity(), "Fetching Profile....");

                        Log.d("DEBUG::FB", loginResult.getAccessToken().getToken());

                        mCallback.onFacebookConnected(loginResult.getAccessToken().getToken(),loginResult.getAccessToken().getExpires().toString(), plzWait);


                    }

                    @Override
                    public void onCancel() {
                        Log.d("DEBUG::FB", "Cancel Operation");
//                            Toast.makeText(MyApplication.getInstance().getApplicationContext(), "Login Cancel", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (exception != null) {
                            Log.d("DEBUG::FB", exception.getMessage());
                        }
                        Toast.makeText(MyApplication.getInstance().getApplicationContext(),
                                "Some Error occurred. Please try again or use another source to Login",
                                Toast.LENGTH_LONG).show();

                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(mFragment, Collections.singletonList("public_profile,email"));





        return new ActivityResultHandler() {
            @Override
            public void handleOnActivityResult(final BaseActivity mActivity, final int requestCode, int resultCode, Intent data) {

                callbackManager.onActivityResult(requestCode, resultCode, data);

            }
        };

    }




    public interface FacebookCallBack {

        void onFacebookConnected(String accessToken, String expiry, Dialog customDialog);

    }

}
