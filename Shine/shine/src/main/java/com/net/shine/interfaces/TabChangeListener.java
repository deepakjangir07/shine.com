package com.net.shine.interfaces;

import android.os.Bundle;

/**
 * Created by ujjawal-work on 16/03/16.
 */
public interface TabChangeListener {

    void onTabChanged(Bundle data, int type);
}
