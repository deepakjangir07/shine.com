package com.net.shine.models;

import java.io.Serializable;

public class RegistrationModel implements Serializable {

//    {"id":"55c1c4ea2c84cd136e82e1ea","name":"fftest","experience_in_years":8,
// "experience_in_months":0,"salary_in_lakh":4,"salary_in_thousand":0,"user_type":2,
// "mid_out":2,"email":"fftest_dfg@gmail.com","cell_phone":"1234567890","country_code":"91",
// "sms_alert_flag":0,


    public String cell_phone = "";
    public String name = "";
    public String email = "";
    public String id = "";
    public String access_token = "";
    public int experience_in_years;

}
