package com.net.shine.fragments.components;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.DesiredJobDetails;


public class DesiredJobComp implements
		OnClickListener {

	private Activity mActivity;
	private View parent;
	private View loadingView;
	private DesiredJobDetails model;

	public DesiredJobComp(Activity mActivity, View parent) {
		this.mActivity = mActivity;
		this.parent = parent;

    }



	public void showDetails(DesiredJobDetails model) {
		try {
			loadingView = parent.findViewById(R.id.loading_cmp);

			this.model = model;
			loadingView.setVisibility(View.GONE);

			if (model != null) {
				TextView jobLocation = (TextView) parent
						.findViewById(R.id.desired_location);
                int [] location= model.getLocation();
				if (location.length!=0) {
					jobLocation.setTag(location);
					String loc_text = "";
					for (int i = 0; i < location.length; i++) {
						loc_text = loc_text
								+ URLConfig.CITY_REVERSE_MAP.get(""
										+ (location[i])) + ", ";
					}
					loc_text = loc_text.substring(0, loc_text.length() - 2);
					jobLocation.setText(loc_text);
				} else {
					jobLocation.setText("Any");
					jobLocation.setTag(-1);
				}

				// EDIT DESIRED JOB
				ImageView editDetails = (ImageView) parent
						.findViewById(R.id.desired_edit);

				editDetails.setOnClickListener(this);

				// FUNCTIONAL AREA
				TextView funArea = (TextView) parent
						.findViewById(R.id.desired_functional_area);

                int [] fun_Area=model.getFunctionalArea();
                if(fun_Area.length!=0) {
                    funArea.setTag(fun_Area);


                    String func_text = "";
                    for (int i = 0; i < fun_Area.length; i++) {
                        func_text = func_text
                                + URLConfig.FUNCTIONA_AREA_REVERSE_MAP.get(""
                                + fun_Area[i]) + ", ";
                    }

                    if (func_text.length() > 0) {
                        func_text = func_text.substring(0, func_text.length() - 2);
                        funArea.setText(func_text);
                    }
                }
                else
                {
                    funArea.setText("Any");
                    funArea.setTag(-1);
                }
				// DESIRED INDUSTRY

				TextView industry = (TextView) parent
						.findViewById(R.id.desired_industry);

				int [] industryValues= model.getIndustry();

				if (industryValues.length!=0) {
                    industry.setTag(industryValues);

					String indus_text = "";
                    System.out.println("ind"+industryValues.toString());
					for (int i = 0; i < industryValues.length; i++) {


						indus_text = indus_text
								+ URLConfig.INDUSTRY_REVERSE_MAP.get(""
										+ industryValues[i]) + ", ";
					}
                    if(indus_text.length()>0) {
                        indus_text = indus_text.substring(0,
                                indus_text.length() - 2);
                    }
					industry.setText(indus_text);
				} else {
					industry.setText("Any");
					industry.setTag(-1);
				}

				// DESIRED JOB TYPE
				TextView jobtype = (TextView) parent
						.findViewById(R.id.desired_job_type);


                int [] jobType=model.getJobType();
				int index = 0;
                if(jobType.length!=0)
                {
				for (int i = 0; i < URLConfig.jOB_TYPE_LIST_VAL.length; i++) {
					if ((""+jobType[0]).equals(URLConfig.jOB_TYPE_LIST_VAL[i])) {
						index = i;
					}
				}}
				jobtype.setTag(index);
				if (URLConfig.JOB_TYPE_LIST[index]==null) {
					jobtype.setText("");

				}
				jobtype.setText(URLConfig.JOB_TYPE_LIST[index]);

				// DESIRED SHIFT TYPE
				TextView shifttype = (TextView) parent
						.findViewById(R.id.desired_shift_type);
                int [] shiftType=model.getShiftType();
				if (model.getShiftType().length==0) {
					shifttype.setText("Not Interested in Shift Based Job");
					shifttype.setTag(0);
				} else {

					shifttype.setTag(shiftType);


					String shift_text = "";
					for (int i = 0; i < shiftType.length; i++) {
						shift_text = shift_text
								+ URLConfig.SHIFT_TYPE_LIST[
										(shiftType[i]) - 1] + ", ";
					}
                    if(shift_text.length()>0) {
                        shift_text = shift_text.substring(0,
                                shift_text.length() - 2);
                    }

					shifttype.setText(shift_text);

				}

				// DESIRED SALARY
				TextView salary = (TextView) parent
						.findViewById(R.id.desired_salary);


                if(model.getMinSalary().length>0) {
                    int int_tag_sal = model.getMinSalary()[0];
                    salary.setTag(int_tag_sal);

                    salary.setText(URLConfig.ANNUAL_SALARY_REVRSE_MAP.get(int_tag_sal + ""));
                }

				

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.desired_edit:
				Gson gson = new Gson();
				String json = gson.toJson(model);
				Bundle bundle = new Bundle();
				bundle.putSerializable(URLConfig.KEY_MODEL_NAME, json);

				BaseFragment frg = new DesiredJobDetailsEditFragment();
				frg.setArguments(bundle);
				((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);


				break;


			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}





}