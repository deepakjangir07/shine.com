package com.net.shine.fragments.components;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.interfaces.OTPCallback;
import com.net.shine.models.PersonalDetail;
import com.net.shine.models.SuspiciousMobileModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.VerifyUserEmailMobile;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by gaurav on 2/7/15.
 */
public class PersonalDetailsEditFragment extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener, View.OnFocusChangeListener, OTPCallback {

    public static final String PHONECHECK = "";
    TextInputLayout inputLayoutFirstname, inputLayoutEmail, inputLayoutMobile, inputLayoutCityName, inputLayoutDob, inputLayoutGender;
    private PersonalDetail pModel;
    private View view;
    private LinearLayout container_box;
    private EditText firstName;
    private EditText emailField;
    private EditText mobileField;
    private EditText dobfield;
    private EditText cityName;
    private Dialog alertDialog;
    private EditText gender;
    private String updatedName;
    String genderName;
    private String VERIFIED = "cellphoneVerified";
    String ciName ;
    private int monthOfYear, dayOfMonth, year;
    String dob;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            year = arg1;
            monthOfYear = arg2 + 1;
            dayOfMonth = arg3;
            dobfield.setText(dayOfMonth
                    + " "
                    + URLConfig.MONTH_NAME_REVERSE_MAP.get(""
                    + (monthOfYear)) + " " + year);
            System.out.println("AA" + arg1 + "q" + arg2 + "q" + arg3);

        }
    };


    private View mImageView;

    // Need to find out new instance method for passing imageview
    public PersonalDetailsEditFragment(View img){
        mImageView = img;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.personal_details_edit, container, false);
        container_box = (LinearLayout) view.findViewById(R.id.personal_profile_comp);
        container_box.requestFocus();

        TextView tv_title = (TextView) view.findViewById(R.id.header_title);
        tv_title.setText("Personal Details");


        Bundle bundle = getArguments();

        Gson gson = new Gson();

        String json = bundle.getString(URLConfig.KEY_MODEL_NAME);
        pModel = gson.fromJson(json, PersonalDetail.class);
        addUpdatePersonal();

        TextChangeListener.addListener(firstName, inputLayoutFirstname, mActivity);
        TextChangeListener.addListener(cityName, inputLayoutCityName, mActivity);
        TextChangeListener.addListener(gender, inputLayoutGender, mActivity);
        TextChangeListener.addListener(emailField, inputLayoutEmail, mActivity);
        TextChangeListener.addListener(dobfield, inputLayoutDob, mActivity);

        return view;
    }

    private void addUpdatePersonal() {
        try {

            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);

            firstName = (EditText) view.findViewById(R.id.first_name);
            firstName.setText(pModel.getFirstName() + " " + pModel.getLastName());
            emailField = (EditText) view.findViewById(R.id.email);
            emailField.setText(pModel.getEmail());
            mobileField = (EditText) view.findViewById(R.id.mobile_field);
            mobileField.setText(pModel.getCellPhone());
            dobfield = (EditText) view.findViewById(R.id.dob);
            inputLayoutCityName = (TextInputLayout) view.findViewById(R.id.input_layout_cityname);
            inputLayoutFirstname = (TextInputLayout) view.findViewById(R.id.input_layout_firstname);
            inputLayoutEmail = (TextInputLayout) view.findViewById(R.id.input_layout_email);
            inputLayoutMobile = (TextInputLayout) view.findViewById(R.id.input_layout_mobile);
            inputLayoutGender = (TextInputLayout) view.findViewById(R.id.input_layout_gender);
            inputLayoutDob = (TextInputLayout) view.findViewById(R.id.input_layout_dob);

            dobfield.setOnClickListener(this);
            dobfield.setOnFocusChangeListener(this);


            cityName = (EditText) view.findViewById(R.id.city_name);

            int index = ShineCommons.setTagAndValue(URLConfig.CITY_REVERSE_MAP,
                    URLConfig.CITY_LIST_EDIT, pModel.getLocation(),
                    cityName);

            cityName.setTag(index);
            ciName = cityName.getText().toString().trim();


            cityName.setOnClickListener(this);
            cityName.setOnFocusChangeListener(this);

            gender = (EditText) view.findViewById(R.id.gender);

            gender.setTag(ShineCommons.setTagAndValue(URLConfig.GENDER_REVERSE_MAP,
                    URLConfig.GENDER_LIST, pModel.getGender(), gender));
            if (gender.getText().toString().isEmpty()) {
                gender.setTag(-1);
            }
            genderName = gender.getText().toString().trim();
            gender.setOnClickListener(this);
            gender.setOnFocusChangeListener(this);
            if (pModel.getDateOfBirth() == null || pModel.getDateOfBirth().isEmpty()) {


            } else {

                dobfield.setText(ShineCommons.getFormattedDate(pModel.getDateOfBirth().substring(0, 10)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private SuspiciousMobileModel model;
    private void suspiciousPhone(){
        String getUrl = URLConfig.SUSPICIOUS_PHONE_NUMBER_URL.replace(URLConfig.MOBILE_NO,
                mobileField.getText().toString().trim())+"?"+ URLConfig.CANDIDATE_ID+"="+ ShineSharedPreferences.getCandidateId(getContext().getApplicationContext());
        Type type = new TypeToken<SuspiciousMobileModel>(){}.getType();
        VolleyNetworkRequest downloader1 = new VolleyNetworkRequest(mActivity,this,
                getUrl,type);
        HashMap mobileMap = new HashMap();
        downloader1.setUsePutMethod(mobileMap);
        downloader1.execute(PHONECHECK);
    }

    private void cellphoneVerified(SuspiciousMobileModel model){

        if(model==null || TextUtils.isEmpty(model.mobile) || TextUtils.isEmpty(model.otp)){
            return;
        }

        String getUrl = URLConfig.CELLPHONE_VERIFIED.replace(URLConfig.CANDIDATE_ID,
                ShineSharedPreferences.getCandidateId(getContext().getApplicationContext()));
        Type type = new TypeToken<SuspiciousMobileModel>(){}.getType();
        VolleyNetworkRequest downloader1 = new VolleyNetworkRequest(mActivity.getApplicationContext(),this,
                getUrl,type);
        HashMap verifyMap = new HashMap();
        verifyMap.put("cell_phone", model.mobile);
        verifyMap.put("otp_code", model.otp);
        downloader1.setUsePutMethod(verifyMap);
        downloader1.execute(VERIFIED);
    }
        @Override
        public void finalStep(String Mobile, String OTP){
            try {

                if(model==null)
                    model = new SuspiciousMobileModel();
                model.mobile = Mobile;
                model.otp = OTP;

                Type listType = new TypeToken<PersonalDetail>() {
                }.getType();


                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                        URLConfig.MY_PROFILE_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)), listType);


                HashMap personalMap = new HashMap();

                personalMap.put("email", emailField.getText().toString().trim());
                personalMap.put("country_code", "91");
                personalMap.put("cell_phone", mobileField.getText().toString().trim());
                personalMap.put("first_name", updatedName);
                personalMap.put("last_name", "");
                personalMap.put("candidate_location", "" + ciName);
                personalMap.put("gender", genderName);

                if (dob.equals("null")) {
                    downloader.setUsePutMethod(personalMap);
                    downloader.execute("UpdatePersonalDetails");

                } else {
                    personalMap.put("date_of_birth", dob);
                    downloader.setUsePutMethod(personalMap);
                    downloader.execute("UpdatePersonalDetails");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
    }

    private void updatePersonalDetails() {
        try {
            boolean isEmpty = false;
            String email = emailField.getText().toString().trim();

            if (TextUtils.isEmpty(email)) {
                isEmpty = true;
                 inputLayoutEmail.setError(mActivity.getString(R.string.email_error));
            }

            String mobileText = mobileField.getText().toString().trim();
            if (TextUtils.isEmpty(mobileText)) {
                isEmpty = true;
                 inputLayoutMobile.setError(mActivity.getString(R.string.mobile_error));
            }
            String genderText = gender.getText().toString().trim();
            if (TextUtils.isEmpty(genderText)) {
                isEmpty = true;
                 inputLayoutGender.setError(mActivity.getString(R.string.gender_error));
            }

            String firstText = firstName.getText().toString().trim();

            if (TextUtils.isEmpty(firstText)) {
                isEmpty = true;
                 inputLayoutFirstname.setError(mActivity.getString(R.string.name_error));
            } else if (!(Pattern.matches(ShineCommons.NAME_REGEX, firstText))) {
                isEmpty = true;
                 inputLayoutFirstname.setError(mActivity.getString(R.string.valid_name_error));
            }
            updatedName = firstName.getText().toString().trim();
            if (ciName.length() == 0) {
                isEmpty = true;
                 inputLayoutCityName.setError(mActivity.getString(R.string.city_error));
            } else {
                ciName = URLConfig.CITY_MAP.get("" + cityName.getText());

                if (ciName == null) {
                    ciName = URLConfig.COUNTRY_MAP.get("" + cityName.getText());
                }

            }

            if (isEmpty) {

                DialogUtils.showErrorToast(getResources().getString(R.string.required_fields));
                return;
            }

            if (!Pattern.matches(ShineCommons.REGEX, email)) {
                emailField.setTextColor(getResources().getColor(R.color.red));
                inputLayoutEmail.setError(mActivity.getString(R.string.valid_email_error));
                return;
            }

            if (mobileText.length() < 10) {
                mobileField.setTextColor(getResources().getColor(R.color.red));
                inputLayoutMobile.setError(mActivity.getString(R.string.valid_mobile_error));
                return;
            } else if ("0".equals("" + mobileText.charAt(0))) {
                mobileField.setTextColor(getResources().getColor(R.color.red));
                inputLayoutMobile.setError(mActivity.getString(R.string.startwith_zero_error));

                return;
            } else {
                boolean flag = false;
                int n = mobileText.length();
                for (int i = 0; i < n; i++) {
                    if (mobileText.charAt(i) < 48 || mobileText.charAt(i) > 57) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {

                    inputLayoutMobile.setError(mActivity.getString(R.string.mobile_error_numeric));
                    mobileField.setTextColor(getResources().getColor(
                            R.color.red));
                    return;
                }
            }

            if (year == 0) {
                dob = "null";
            } else {
                dob = year + "-" + (monthOfYear) + "-" + (dayOfMonth);
            }

                genderName = URLConfig.GENDER_MAP.get("" + gender.getText());


            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            if (pModel.getCellPhone().equals(mobileText)){
                finalStep(null,null);
            }
            else {
                suspiciousPhone();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        container_box.requestFocus();
        mActivity.setTitle(getString(R.string.title_edit_profile));
        //mActivity.updateLeftDrawer()();
        mActivity.setActionBarVisible(false);
        ManualScreenTracker.manualTracker("Edit-PersonalDetails");
    }


    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void OnDataResponse(final Object object, final String tag) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.dialogDismiss(alertDialog);
                if (tag.equals(PHONECHECK)) {

                    model = (SuspiciousMobileModel) object;

                    if (model.blocked) {

                        DialogUtils.showCustomAlertsNoTitle(mActivity, mActivity.getString(R.string.suspicious_number_msg));

                    } else {


                        model.mobile = mobileField.getText().toString().trim();
                        new VerifyUserEmailMobile(mActivity).verifyMobileOTP(model, PersonalDetailsEditFragment.this);


                    }
                }

                else if (tag.equals("UpdatePersonalDetails")){
                    ShineCommons.trackShineEvents("Edit", "Personal", mActivity);


                    Toast.makeText(mActivity, "Personal details updated successfully", Toast.LENGTH_SHORT).show();

                if (object != null) {


                        if (!emailField.getText().toString().equals(ShineSharedPreferences.getUserEmail(mActivity))) {

                            ShineSharedPreferences.saveUserEmail(mActivity, emailField.getText().toString());

                            alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "Your Email CANDIDATE_ID has been successfully changed. " +
                                    "Please use " + emailField.getText().toString() + " to login in to Shine from next time.");
                            alertDialog.findViewById(R.id.ok_btn).setOnClickListener(PersonalDetailsEditFragment.this);


                        }



                        ShineSharedPreferences.saveUserName(mActivity, updatedName);

                        //mActivity.updateLeftDrawer()();

                        PersonalDetail personalDetails = (PersonalDetail) object;

                        DataCaching.getInstance().cacheData(DataCaching.PERSONAL_SECTION,
                                "" + DataCaching.CACHE_TIME_LIMIT, personalDetails, mActivity);
                        cellphoneVerified(model);
                    if (mImageView!=null){
                        mImageView.setVisibility(View.GONE);
                    }

                    mActivity.popone();

                    Bundle bundle=new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_PERSONAL);

                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));


                    }
                }

            }
        });


    }

    @Override
    public void OnDataResponseError(final String error, final String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);

                    if (tag.equals(PHONECHECK)) {
                        if (error.contains("404")) {
                            finalStep(null, null);
                        }
                        return;

                    }
                    else if (tag.equals(VERIFIED)) {
                        Log.d("Verified error", error + "");
                    } else {
                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity,error);
                        alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                                PersonalDetailsEditFragment.this);
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDate() {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int nowYear = c.get(Calendar.YEAR);
        
        int mYear = nowYear-10;
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        c.set(mYear,mMonth,mDay);

        DatePickerDialog dialog;

        if (pModel.getDateOfBirth() == null || pModel.getDateOfBirth().isEmpty()) {
            dialog = new DatePickerDialog(mActivity, myDateListener, mYear, mMonth, mDay);
            dialog.updateDate(mYear, mMonth, mDay);
        } else {
            dialog = new DatePickerDialog(mActivity, myDateListener, year,
                    monthOfYear - 1, dayOfMonth);
        }
        dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        dialog.show();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            showSpinner(v.getId());
    }

    private void showSpinner(int id) {
        try {
            ShineCommons.hideKeyboard(mActivity);

            switch (id) {
                case R.id.dob:
                    showDate();
                    break;
                case R.id.city_name:

                    DialogUtils.showNewMultiSpinnerDialogRadio(
                            getString(R.string.hint_select_city), cityName,
                            URLConfig.CITY_LIST_EDIT, mActivity);


                    break;
                case R.id.gender:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_select_gender), gender,
                            URLConfig.GENDER_LIST, mActivity);


                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.update_btn:
                updatePersonalDetails();


                break;
            case R.id.cancel_btn:

                mActivity.popone();
                Bundle bundle=new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_PERSONAL);

                mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));

                break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
            default:
                showSpinner(v.getId());
                break;
        }
    }


}
