package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.ActionBarState;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.ReferralRequestModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.SendRefferalService;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.ReferralCipher;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

public class ShineMailFragment extends BaseFragment implements OnClickListener,
        VolleyRequestCompleteListener {
    Dialog dialog;
    private View view;
    private EditText comments;
    private View button;
    private String feedback;
    private UserStatusModel userProfileVO;
    private TextView sendTo, connectedViaWork, connectedViaStudy;
    private ReferralRequestModel model;


    public static ShineMailFragment newInstance(Bundle b) {
        ShineMailFragment fragment = new ShineMailFragment();
        fragment.setArguments(b);
        return fragment;
    }

    public static ShineMailFragment newInstance(int fromWhich, String userID, String friendId,
                                                String recipientName, String jobTitle, String organization,
                                                String connectedViaStudy, String connectedViaWork, String mailCopy, String JobID,
                                                ReferralRequestModel model, String jobExp, String jobLoc) {

        Bundle b = new Bundle();
        b.putString("user_id", userID);
        b.putString("friend_id", friendId);
        b.putString("recipient", recipientName);
        b.putString("job_title", jobTitle);
        b.putString("organization", organization);
        b.putInt("fromWhich", fromWhich);
        b.putString("connectedViaStudy", connectedViaStudy);
        b.putSerializable("model", model);
        if (jobExp == null || jobExp.isEmpty())
            jobExp = "";
        if (jobLoc == null || jobLoc.isEmpty())
            jobLoc = "";
        b.putString("location", jobLoc);
        b.putString("experience", jobExp);
        b.putString("connectedViaWork", connectedViaWork);
        b.putString("mailCopy", mailCopy);
        b.putString("JobID", JobID);

        ShineMailFragment fragment = new ShineMailFragment();
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.model = (ReferralRequestModel) getArguments().getSerializable("model");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.shine_mail_frg, container, false);


        connectedViaStudy = (TextView) view.findViewById(R.id.connected_via_study);
        connectedViaWork = (TextView) view.findViewById(R.id.connected_via_work);
        comments = (EditText) view.findViewById(R.id.feedback);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        sendTo = (TextView) view.findViewById(R.id.sendto);


        sendTo.setText("To : " + getArguments().getString("recipient"));
        button = view.findViewById(R.id.submit_btn);
        button.setOnClickListener(this);
        String connectedVia = "";
        String mailBody = "";


        if (getArguments().getString("connectedViaStudy") != null && !getArguments().getString("connectedViaStudy").trim().equals("")) {
            connectedViaStudy.setVisibility(View.VISIBLE);
            connectedViaStudy.setText(getArguments().getString("connectedViaStudy"));
            connectedVia = connectedViaStudy.getText().toString();
        }
        if (getArguments().getString("connectedViaWork") != null && !getArguments().getString("connectedViaWork").trim().equals("")) {
            connectedViaWork.setVisibility(View.VISIBLE);
            connectedViaWork.setText(getArguments().getString("connectedViaWork"));
            connectedVia = connectedViaWork.getText().toString();
        }
        if (!connectedVia.equals("")) {
            mailBody = "Hope you are doing fine. We " + connectedVia + ".";
        } else {
            mailBody = "";
        }

        String experience = getArguments().getString("experience");
        String location = getArguments().getString("location");
        String str = "";

        if (experience != null && !experience.isEmpty() && location != null && !location.isEmpty()) {
            str = "Experience: " + experience + " (" + location + ")" + "<br/> ";
        }

        Log.d("DEBUG::UJJ", "  " + str);


        if (getArguments().getInt("fromWhich") == 1) {

            String Ref_URL = URLConfig.VIEW_MESSAGE_URL_ENCRYPTED +
                    ReferralCipher.cipher(model.getJob_id() + "/" + userProfileVO.candidate_id
                            + "/" + model.getReferral_id(), ReferralCipher.ENC_MODE) + "/";

            comments.setText(Html
                    .fromHtml("<html><head><title></title></head><body><p>Hi&nbsp;"
                            + getArguments().getString("recipient")
                            + "&#44 <br/>"
                            + "<br/>"
                            + mailBody + "<br/>"
                            + "</p><p>I need a favor from you. I came across a job opportunity in "
                            + getArguments().getString("organization") + " "
                            + "and want to apply for it through Shine.com. I will really appreciate, if you can refer me for this job. </p><p><br/>"
                            + "You don’t have to send my CV to the HR. All you need to do is just a click on the referral link and your referral will reach the HR automatically.<br/>"
                            + "<br/>"
                            + "Please click, on the below link to refer me: <br/>"
                            + "<b>" + " Refer" + " " + userProfileVO.full_name + " : " + "</b>" + Ref_URL + "<br/>"
                            + "<br/>"
                            + "<b>" + "To view my current profile you can" + " {profile_url} " + "</b>" + "<br/>"
                            + "<br/>"
                            + "<b>" + "Job Details:" + "</b><br/>"
                            + "{" + getArguments().getString("job_title") + "}" + "<br/>"
                            + str
                            + "Company: " + getArguments().getString("organization") + "<br/>"
                            + "<br/>"
                            + "Want to know more about the Power of Referral? {power_of_referral_url} " + "</br>"
                            + "<br/>"
                            + "Regards<br/>"
                            + userProfileVO.full_name
                            + "</p><p>" + "</font></p></body></html>"));
        } else if (getArguments().getInt("fromWhich") == 2) {
            comments.setText(Html
                    .fromHtml("<html><head><title></title></head><body><p>Hi&nbsp;" +
                            getArguments().getString("recipient")
                            + "&#44 <br />"
                            + "<br/>"
                            + mailBody + "<br/>"
                            + "</p><p>I would like to connect with you for possible openings in your company.</p><p>It would be great if you could reply to this email or send me your contact details to discuss this further.</p><p>My contact detail are as follows:<br /></p>"
                            + "Phone:" + userProfileVO.mobile_no + " / Email: " + userProfileVO.email
                            + "<br /><p>Looking forward to hear from you.</p><p>Thanks and regards</p><p>"
                            + userProfileVO.full_name
                            + "<font></p></body></html>"));
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_btn:
                try {
                    if (comments.getText().length() > 0) {

                        submitFeedback();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;


            default:
                break;
        }
    }

    private void submitFeedback() {


        feedback = comments.getText().toString();


        dialog = DialogUtils.showCustomProgressDialog(mActivity, "Please wait..");


        Type type = new TypeToken<EmptyModel>() {
        }.getType();

        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                URLConfig.CANDIDATE_CONNECT_MAIL, type);
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("shine_id", ShineSharedPreferences.getCandidateId(mActivity));
        postParams.put("friend_id", getArguments().getString("friend_id"));
        postParams.put("message", feedback);
        req.setUsePostMethod(postParams);
        req.execute("shineMailFrag");

    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Shine Mail");
        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
        ManualScreenTracker.manualTracker("ShineMail");

    }

    @Override
    public void OnDataResponse(Object object, String tag) {
        try {


            DialogUtils.dialogDismiss(dialog);

            if (object != null) {

                Toast.makeText(mActivity.getApplicationContext(),
                        "Your connection request has been sent successfully.", Toast.LENGTH_SHORT).show();

                if (getArguments().getInt("fromWhich") == 1) {
                    ShineCommons.trackShineEvents("JobReferralConnect", "ShineMail", mActivity);

                } else if (getArguments().getInt("fromWhich") == 2) {
                    ShineCommons.trackShineEvents("CompanyConnect", "ShineMail", mActivity);


                }
                if (model != null) {
                    Intent i = new Intent(mActivity, SendRefferalService.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("model", model);
                    i.putExtra("bundle", bundle);
                    mActivity.startService(i);
                }
                mActivity.finish();


            } else {
                dialog = DialogUtils.showCustomAlertsNoTitle(mActivity, "Some Error Occurred");
//                DialogUtils.showErrorMsg(
//                        view.findViewById(R.id.loading_cmp),
//                        "Some Error Occurred", DialogUtils.ERR_NO_IMG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(dialog);
                    dialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
//                    DialogUtils.showErrorMsg(LoadingCmp, error,
//                            DialogUtils.ERR_NO_IMG);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
