package com.net.shine.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deepak on 21/12/16.
 */



public class ProfileViewEmailAdapter extends BaseAdapter {


    private BaseActivity mActivity;
    private List<WhoViewMyProfileModel.MailActivity> mList;

    public ProfileViewEmailAdapter(BaseActivity activity, List<WhoViewMyProfileModel.MailActivity> list) {
        this.mActivity = activity;
        this.mList = list;

    }
    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int postition) {
        return mList.get(postition);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;

        InboxMailViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.email_list_adapter_layout, viewGroup, false);
            viewHolder = new InboxMailViewHolder();

            viewHolder.sender = (TextView) view.findViewById(R.id.sender);
            viewHolder.date = (TextView) view.findViewById(R.id.date);
            viewHolder.subject = (TextView) view.findViewById(R.id.subject);

            view.setTag(viewHolder);
        } else {
            viewHolder = (InboxMailViewHolder) view.getTag();
        }

            viewHolder.sender.setText(mList.get(position).recruiterName);
//            if (mList.get(position).hasRead == 0) {
//                view.findViewById(R.id.job_alert_list).setBackgroundColor(
//                        Color.parseColor("#eeeeee"));
////            viewHolder.sender.setTypeface(null, Typeface.BOLD);
//            } else {
//                view.findViewById(R.id.job_alert_list).setBackgroundColor(
//                        Color.parseColor("#ffffff"));
//            }

            viewHolder.date.setText(ShineCommons.getFormattedDate(mList.get(position).timestamp.substring(0, 10)));
            viewHolder.subject.setText(mList.get(position).subjectLine);

            return view;
    }



    public void rebuildListObject(ArrayList<WhoViewMyProfileModel.MailActivity> itemList) {

        this.mList = itemList;
    }


    class InboxMailViewHolder {

        TextView sender;
        TextView date;
        TextView subject;

    }

}




