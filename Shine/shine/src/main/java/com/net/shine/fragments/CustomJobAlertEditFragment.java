package com.net.shine.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.adapters.RelevantSuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.CustomAlert;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.views.RelevantAutoComplete;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CustomJobAlertEditFragment extends BaseFragment implements
		OnFocusChangeListener, View.OnClickListener {
	private UserStatusModel userProfileVO;
	private SimpleSearchVO sVo;
	private TextView alertName;
	private EditText location, email_id;
	private RelevantAutoComplete keywords;
	public static final int ADD_JOB_ALERT = 1;
	public static final int EDIT_JOB_ALERT = 2;
	private int add_or_edit = EDIT_JOB_ALERT;
	private View view = null;

	private TextView minExpField, minSalaryField, funcAreaField;
	private EditText industryField;
    private TextInputLayout inputLayoutAlertName, inputLayoutEmail;

    public static CustomJobAlertEditFragment newInstance(Bundle args) {
        CustomJobAlertEditFragment fragment = new CustomJobAlertEditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		if(getArguments()!=null){
        add_or_edit = getArguments().getInt("add_or_edit", EDIT_JOB_ALERT);
		}
		else {
			add_or_edit = ADD_JOB_ALERT;
		}

    }





	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		if (add_or_edit == EDIT_JOB_ALERT) {
			view = inflater
					.inflate(R.layout.custom_job_alert_edit_layout, container, false);
		} else if (add_or_edit == ADD_JOB_ALERT) {
			view = inflater.inflate(R.layout.add_custom_job, container, false);
		}

		try {

			userProfileVO=ShineSharedPreferences.getUserInfo(mActivity);
			Bundle bundle = getArguments();
			if (bundle != null)
			{
				//sVo=bundle.getCha
				sVo= (SimpleSearchVO) bundle.getSerializable(URLConfig.CUSTOM_JOB_MODIFY);
			}


			if(sVo==null)
			{
				sVo=new SimpleSearchVO();
			}

			inputLayoutAlertName =(TextInputLayout) view.findViewById(R.id.input_layout_alertname);
			inputLayoutEmail=(TextInputLayout) view.findViewById(R.id.input_layout_email);


			View cancel_button = view.findViewById(R.id.cancel_btn);
			cancel_button.setOnClickListener(this);
			View save_button = view.findViewById(R.id.cj_update_add_btn);
			save_button.setOnClickListener(this);

			alertName = (TextView) view.findViewById(R.id.alert_name);

			keywords = (RelevantAutoComplete) view.findViewById(R.id.keywords);
			keywords.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
					android.R.layout.simple_spinner_dropdown_item,
					URLConfig.KEYWORD_SUGGESTION));
			keywords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Log.d("item_clicked", keywords.getText() + "");
					keywords.setSelection(keywords.getText().length());
				}
			});

			location = (EditText) view.findViewById(R.id.location);

			String tag_val = sVo.getCustomJobAlertLocTag()
					.replaceAll("\\[", "").replaceAll("\\]", "")
					.replaceAll("\"", "").replace(",", "#").replaceAll(" ","");

			if (!tag_val.equals("")) {
				String[] i_loc = tag_val.split("#");
				String loc_text = "";

				String[] city_array = URLConfig.CITY_LIST;
				String[] array_without_first_item = Arrays.copyOfRange(
						city_array, 2, city_array.length);

				int[] tag_index = new int[i_loc.length];
				for (int i=0;i<i_loc.length;i++) {

					int index = ShineCommons.setTagAndValue(URLConfig.CITY_REVERSE_MAP,
							array_without_first_item, Integer.parseInt(i_loc[i]),
							location);
					loc_text = loc_text + array_without_first_item[index] + ",";
					tag_index[i] = index;
				}

				location.setTag(tag_index);
				loc_text = loc_text.substring(0, loc_text.length() - 1);
				location.setText(loc_text);
			} else {
				location.setTag(new int[0]);
			}

			location.setOnClickListener(this);
			location.setOnFocusChangeListener(this);

			minExpField = (TextView) view.findViewById(R.id.min_exp_field);
			minExpField.setTag(0);
			minExpField.setOnClickListener(this);
			minExpField.setOnFocusChangeListener(this);

			minSalaryField = (TextView) view.findViewById(R.id.min_sal_field);
			minSalaryField.setTag(0);
			minSalaryField.setOnClickListener(this);
			minSalaryField.setOnFocusChangeListener(this);

			funcAreaField = (TextView) view.findViewById(R.id.func_area_field);
			funcAreaField.setTag(new int[0]);
			funcAreaField.setOnClickListener(this);
			funcAreaField.setOnFocusChangeListener(this);

			String tag_val_func = sVo.getCustomJobAlerMinfuncAreaTag()
					.replaceAll("\\[", "").replaceAll("\\]", "")
					.replaceAll("\"", "").replace(",", "#").replaceAll(" ","");
			if (!tag_val_func.equals("")) {
				String[] i_func = tag_val_func.split("#");
				String func_text = "";

				List<String> excludedFuncList = new ArrayList<>();
				String[] tempFuncList = URLConfig.FUNCTIONAL_AREA_LIST2;

				for (String aTempFuncList : tempFuncList) {
					if (!aTempFuncList.startsWith("#")) {
						excludedFuncList.add(aTempFuncList);
					}
				}
				final String[] excludedFunc = excludedFuncList
						.toArray(new String[excludedFuncList.size()]);
				int[] func_indixes = new int[i_func.length];
				for (int i = 0; i < i_func.length; i++) {

					int index = ShineCommons.setTagAndValue(
							URLConfig.FUNCTIONA_AREA_REVERSE_MAP, excludedFunc,
							Integer.parseInt(i_func[i]), funcAreaField);
					func_text = func_text + excludedFunc[index] + ",";
					func_indixes[i] = index;
				}

				funcAreaField.setTag(func_indixes);

				func_text = func_text.substring(0, func_text.length() - 1);
				funcAreaField.setText(func_text);
			}
			else
				funcAreaField.setTag(new int[0]);

			industryField = (EditText) view
					.findViewById(R.id.industry_type_field);
			industryField.setTag(new int[0]);

			String tag_val_indus = sVo.getCustomJobAlerIndusTypeTag()
					.replaceAll("\\[", "").replaceAll("\"", "")
					.replaceAll("\\]", "").replace(",", "#").replaceAll(" ","");

			if (!tag_val_indus.equals("")) {
				String[] i_indus = tag_val_indus.split("#");
				String indus_text = "";

				List<String> excludedHashIndusList = new ArrayList<>();
				String[] tempIndusList = URLConfig.INDUSTRY_LIST3;

				for (String aTempIndusList : tempIndusList) {
					if (!aTempIndusList.startsWith("#")) {
						excludedHashIndusList.add(aTempIndusList);
					}
				}
				final String[] excludedHashIndus = excludedHashIndusList
						.toArray(new String[excludedHashIndusList.size()]);

				int[] ind_indixes = new int[i_indus.length];

				for (int i = 0; i < i_indus.length; i++) {

					int index = ShineCommons.setTagAndValue(URLConfig.INDUSTRY_REVERSE_MAP,
							excludedHashIndus, Integer.parseInt(i_indus[i]),
							industryField);
					indus_text = indus_text + excludedHashIndus[index] + ",";
					ind_indixes[i] = index;


				}
				industryField.setTag(ind_indixes);
				indus_text = indus_text.substring(0, indus_text.length() - 1);
				industryField.setText(indus_text);
			} else {
				industryField.setTag(new int[0]);
			}

			industryField.setOnClickListener(this);
			industryField.setOnFocusChangeListener(this);

			userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

			email_id = (EditText) view.findViewById(R.id.email_id);
			if (userProfileVO == null) {
				email_id.setVisibility(View.VISIBLE);
			}
            else {
                email_id.setVisibility(View.GONE);
            }

			alertName.setText(sVo.getCustomJobAlertName());
			if (sVo.getMinExpTag() > 0) {
			
				minExpField.setText(URLConfig.EXPERIENCE_REVERSE_MAP.get(""
						+ sVo.getMinExpTag()));

				List<String> arr = Arrays.asList(URLConfig.EXPERIENCE_LIST2);
				int index = arr
						.indexOf(minExpField.getText().toString().trim());

				minExpField.setTag(index+1);
			}
			if (sVo.getMinSalTag() > 0) {
				minSalaryField.setText(URLConfig.ANNUAL_SALARY_REVRSE_MAP
						.get("" + sVo.getMinSalTag()));
				minSalaryField.setTag(sVo.getMinSalTag()+1);

			}

			keywords.setText(sVo.getKeyword());

			if(email_id.isShown()){
			if (!Pattern.matches(ShineCommons.REGEX, email_id.getText().toString())) {
				email_id.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
				email_id.requestFocus();
				inputLayoutEmail.setError(getResources().getString(R.string.email_invalid_address_msg));
			}
				email_id.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {

						if (!hasFocus) {
							String emailText = email_id.getText().toString().trim();
							if (!Pattern.matches(ShineCommons.REGEX, emailText)) {
 								email_id.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
								inputLayoutEmail.setError(getResources().getString(R.string.email_invalid_address_msg));
							}
						}
					}
				});
			}
			if (add_or_edit == EDIT_JOB_ALERT) {
				TextChangeListener.addListener(email_id,inputLayoutEmail,mActivity);
			} else if (add_or_edit == ADD_JOB_ALERT) {
				TextChangeListener.addListener(alertName,inputLayoutAlertName,mActivity);
				TextChangeListener.addListener(email_id,inputLayoutEmail,mActivity);
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}


	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.cancel_btn:
			try {

				mActivity.onBackPressed();
				ShineCommons.hideKeyboard(mActivity);

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		case R.id.cj_update_add_btn:

			if (add_or_edit == EDIT_JOB_ALERT) {
				sVo.setCustomJobAlertName(alertName.getText().toString());
				if (userProfileVO != null) {
					String keywordStr = keywords.getText().toString();
					String locText = location.getText().toString();
					String locTextArray[];

					locTextArray = locText.split(",");
					int locTagArrayLen = locTextArray.length;
					String locIdArray[] = new String[locTagArrayLen];
                    if (locText.trim().length()!=0) {

						for (int i = 0; i < locTagArrayLen; i++) {
							locIdArray[i] = URLConfig.CITY_MAP
									.get(locTextArray[i].trim());
                            }
					}
                    else
                    {
                        locIdArray=new String[]{};
                    }

					String induText = industryField.getText().toString();
					String indTextArray[];
					indTextArray = induText.split(",");
					int indTagArrayLen = indTextArray.length;
					String indIdArray[] = new String[indTagArrayLen];
                    if (induText.trim().length()!=0) {
						for (int i = 0; i < indTagArrayLen; i++) {
							indIdArray[i] = URLConfig.INDUSTRY_MAP
									.get(indTextArray[i].trim());
						}
					}
                    else
                    {
                        indIdArray=new String[]{};
                    }

					if (!minSalaryField.getText().toString().trim()
									.equals("")) {

						minSalaryField.setTag(URLConfig.ANNUAL_SALARY_MAP
								.get(minSalaryField.getText().toString()));
					}

					String areaText = funcAreaField.getText().toString();
					int[] areaTag = (int[]) funcAreaField.getTag();
					String areaTextArray[];
					areaTextArray = areaText.split(",");
					int areaTagArrayLen = areaTextArray.length;
					String areaIdArray[] = new String[areaTagArrayLen];
                   if (areaTag.length!=0 && areaText.trim().length()!=0) {

						for (int i = 0; i < areaTagArrayLen; i++) {
							areaIdArray[i] = URLConfig.FUNCTIONA_AREA_MAP
									.get(areaTextArray[i].trim());
						}
					}
                    else{
                        areaIdArray=new String[]{};
                    }
					if ((!minExpField
							.getText().toString().trim().equals(""))) {
						List<String> arr = Arrays
								.asList(URLConfig.EXPERIENCE_LIST2);
						int index = arr.indexOf(minExpField.getText()
								.toString().trim());

						minExpField.setTag(URLConfig.EXPERIENCE_MAP.get(String
								.valueOf(URLConfig.EXPERIENCE_LIST2[index])));
					} else {
						minExpField.setTag(-1);
					}
                    Log.d("DEBUG :","Keywords "+keywordStr);
					new CustomAlert(mActivity, (MyCustomJobAlertFragment) getTargetFragment()).updateCustomAlert(userProfileVO,
							sVo, null, keywordStr, locIdArray,
							indIdArray, areaIdArray, Integer
									.parseInt(minSalaryField.getTag()
											.toString()), Integer
									.parseInt(minExpField.getTag().toString()));


				}
			}
			else if (add_or_edit == ADD_JOB_ALERT) {
				EditText alert_name = (EditText) view
						.findViewById(R.id.alert_name);
				String user_alert_name = alert_name.getText().toString();
				if(userProfileVO!=null) {
					if (user_alert_name.trim().equals("")) {
						inputLayoutAlertName.setError("Please enter the alert name");
 						break;
					}
				}
				else {
					if (email_id.getText().toString().trim().equals("")
							&&  (user_alert_name.trim().equals(""))) {
						inputLayoutEmail.setError("Please enter email address");
						inputLayoutAlertName.setError("Please enter the alert name");
						break;
					}
					if(!Pattern.matches(ShineCommons.REGEX, email_id.getText().toString()) && (user_alert_name.trim().equals("")))
					{
						inputLayoutEmail.setError("Please enter valid email address");
						inputLayoutAlertName.setError("Please enter the alert name");
						break;
					}
					if (email_id.getText().toString().trim().equals("")) {
						inputLayoutEmail.setError("Please enter email address");
						break;
					}
					if(!Pattern.matches(ShineCommons.REGEX, email_id.getText().toString())){
						inputLayoutEmail.setError("Please enter valid email address");
						break;
					}
					if ((user_alert_name.trim().equals(""))) {
						inputLayoutAlertName.setError("Please enter the alert name");
						break;
					}
				}
				String alertNameStr = alertName.getText().toString();
				String keywordStr = keywords.getText().toString();
				String locText = location.getText().toString();
				String locTextArray[];
				locTextArray = locText.split(",");
				int locTagArrayLen = locTextArray.length;
				String locIdArray[] = new String[locTagArrayLen];
				if (locText.trim().length()!=0) {
					for (int i = 0; i < locTagArrayLen; i++) {
						locIdArray[i] = URLConfig.CITY_MAP.get(locTextArray[i].trim());
					}
				}
                else {
                    locIdArray=new String[]{};
                }
                String induText = industryField.getText().toString();
				String indTextArray[];
				indTextArray = induText.split(",");
				int indTagArrayLen = indTextArray.length;
				String indIdArray[] = new String[indTagArrayLen];
				if (induText.trim().length()!=0) {
					for (int i = 0; i < indTagArrayLen; i++) {
						indIdArray[i] = URLConfig.INDUSTRY_MAP
								.get(indTextArray[i].trim());

					}
				}
                else {
                    indIdArray=new String[]{};
                }
				String areaText = funcAreaField.getText().toString();
				String areaTextArray[];
				areaTextArray = areaText.split(",");
				int areaTagArrayLen = areaTextArray.length;
				String areaIdArray[] = new String[areaTagArrayLen];
				if (areaText.trim().length()!=0) {

					for (int i = 0; i < areaTagArrayLen; i++) {
						areaIdArray[i] = URLConfig.FUNCTIONA_AREA_MAP
								.get(areaTextArray[i].trim());

					}
				}
                else {
                    areaIdArray=new String[]{};
                }
				if ((!minExpField
						.getText().toString().trim().equals(""))) {

					minExpField.setTag(URLConfig.EXPERIENCE_MAP.get(minExpField
							.getText().toString().trim()));

				}
				if (!minSalaryField.getText().toString().trim()
								.equals("")) {

					minSalaryField.setTag(URLConfig.ANNUAL_SALARY_MAP
							.get(minSalaryField.getText().toString()));
				}


				sVo.setCustomJobAlertName(alertNameStr);
				if (userProfileVO != null) {
					CustomAlert ca = new CustomAlert(mActivity,(MyCustomJobAlertFragment) getTargetFragment());
					ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_FORM);
					ca.saveCustomAlert(
							userProfileVO,
							null,
							alertNameStr,
							keywordStr,
							locIdArray,
							indIdArray,
							areaIdArray,
							Integer.parseInt(minSalaryField.getTag().toString()),
							Integer.parseInt(minExpField.getTag().toString()),sVo);
				} else {
					CustomAlert ca = new CustomAlert(mActivity,(MyCustomJobAlertFragment) getTargetFragment());
					ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_FORM);
					if(!TextUtils.isEmpty(keywordStr))
						sVo.setKeyword(keywordStr);
					if(!TextUtils.isEmpty(location.getText().toString()))
						sVo.setLocation(location.getText().toString());
					if(!TextUtils.isEmpty(minExpField.getText().toString()))
						sVo.setMinExp(minExpField.getText().toString());
					if (!TextUtils.isEmpty(minSalaryField.getText().toString()))
						sVo.setMinSal(minSalaryField.getText().toString());
					if (!TextUtils.isEmpty(areaText))
						sVo.setFuncArea(areaText);
					if(!TextUtils.isEmpty(induText))
						sVo.setIndusType(induText);
					ca.saveCustomAlert(userProfileVO, email_id.getText()
									.toString(), alertNameStr, keywordStr, locIdArray,
							indIdArray, areaIdArray, Integer
									.parseInt(minSalaryField.getTag()
											.toString()), Integer
									.parseInt(minExpField.getTag().toString()),sVo);
				}
			}
			break;

		default:
			showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
			break;
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (hasFocus) {
			showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
		}

	}

	@SuppressLint("NewApi")
	private void showSpinner(int id) {
		try {
			switch (id) {
			case R.id.min_exp_field:

                String[] expList=new String[URLConfig.EXPERIENCE_LIST2.length+1];
                expList[0]="None";
				System.arraycopy(URLConfig.EXPERIENCE_LIST2, 0, expList, 1, URLConfig.EXPERIENCE_LIST2.length);

				DialogUtils.showNewSingleSpinnerRadioDialog(
						getString(R.string.hint_experience_2), minExpField,
                        expList, mActivity);

				break;
			case R.id.min_sal_field:

                String[] salList=new String[URLConfig.ANNUAL_SALARY_LIST.length+1];
                salList[0]="None";
				System.arraycopy(URLConfig.ANNUAL_SALARY_LIST, 0, salList, 1, URLConfig.ANNUAL_SALARY_LIST.length);
                DialogUtils
						.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
								minSalaryField, salList,
								mActivity);
				break;

			case R.id.location:

				String[] city_array = URLConfig.CITY_LIST;
				String[] array_without_first_item = Arrays.copyOfRange(
						city_array, 2, city_array.length);
                DialogUtils.showNewMultiSpinnerDialog(
						getString(R.string.hint_select_city), location,
						array_without_first_item, mActivity, false);

				break;
			case R.id.func_area_field:

				List<String> funcnumlist = new ArrayList<>();
				for (int i = 0; i <  URLConfig.FUNCTIONAL_AREA_LIST2.length; i++) {
					if (!URLConfig.FUNCTIONAL_AREA_LIST2[i].startsWith("#")) {
						funcnumlist.add( URLConfig.FUNCTIONAL_AREA_LIST2[i]);
					}
				}
				final String[] funcitemsNew = funcnumlist
						.toArray(new String[funcnumlist.size()]);
                DialogUtils.showNewSingleSpinnerDialog(
						getString(R.string.hint_func_area), funcAreaField,
						funcitemsNew, mActivity,true);

				break;

			case R.id.industry_type_field:


				List<String> indnumlist = new ArrayList<>();
				for (int i = 0; i <  URLConfig.INDUSTRY_LIST3.length; i++) {
					if (!URLConfig.INDUSTRY_LIST3[i].startsWith("#")) {
						indnumlist.add( URLConfig.INDUSTRY_LIST3[i]);
					}
				}
				final String[] inditemsNew = indnumlist
						.toArray(new String[indnumlist.size()]);
                DialogUtils.showNewSingleSpinnerDialog(
						getString(R.string.hint_industry), industryField,
						inditemsNew, mActivity, true);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (add_or_edit == EDIT_JOB_ALERT) {
			ManualScreenTracker.manualTracker("EditCJA");
			mActivity.setTitle("Edit alert");
		} else if (add_or_edit == ADD_JOB_ALERT) {
			mActivity.setTitle("Create alert");
			if (userProfileVO != null) {
				ManualScreenTracker.manualTracker("CreateCJA-LoggedIn");

			} else {
				ManualScreenTracker.manualTracker("CreateCJA-NonLoggedIn");

			}
		}
		mActivity.setActionBarVisible(false);
	}

	@Override
	public void onStop() {

		mActivity.setActionBarVisible(true);
		super.onStop();
	}
}
