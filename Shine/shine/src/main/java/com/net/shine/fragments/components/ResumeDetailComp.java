package com.net.shine.fragments.components;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.ResumeUploadFrg;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.ResumeDetails;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.CustomInsRequest;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class ResumeDetailComp implements
		OnClickListener, OnMenuItemClickListener, VolleyRequestCompleteListener, ActivityCompat.OnRequestPermissionsResultCallback {

	private Activity mActivity;
	private View parent;
	public Dialog alertDialog;
	private View loadingView;
	public String id="";
	public static List<ResumeDetails> resumesList;
	private LayoutInflater inflater;
	private final String RESUME_DOWNLOAD = "resumeDownload";
	private final String UPDATE_RESUME_DETAILS = "updateResumeDetails";
	private final String UPDATE_RESUME_DETAILS_DEFAULT = "updateResumeDetailsDefault";


	Fragment parentFrg;
	public ResumeDetailComp(Activity mActivity, View parent, Fragment pFrg) {
		parentFrg = pFrg;
		this.mActivity = mActivity;
		this.parent = parent;
		this.inflater = mActivity.getLayoutInflater();
		loadingView = parent.findViewById(R.id.loading_cmp);
	}

	public void showDetails(List<ResumeDetails> resumesList) {
		try {
			ResumeDetailComp.resumesList = resumesList;
			loadingView.setVisibility(View.GONE);
			LinearLayout resumesDetailView = (LinearLayout) parent
					.findViewById(R.id.resume_detail_view);

			resumesDetailView.removeAllViews();



			TextView addResumeBtn = (TextView) parent
					.findViewById(R.id.add_new_resumes);

			int size = resumesList.size();
			for (int i = 0; i < size; i++) {
				ResumeDetails model = resumesList.get(i);
				View resumeDetailTextView = inflater.inflate(
						R.layout.resume_details_textview_detail, null);

				TextView tView = (TextView) resumeDetailTextView
						.findViewById(R.id.field_name);
				ImageView dotView = (ImageView) resumeDetailTextView
						.findViewById(R.id.menu_resume);
				ImageView defaultView = (ImageView) resumeDetailTextView
						.findViewById(R.id.default_resume);
				if (model.getIsDefault() == 1) {
					defaultView.setVisibility(View.VISIBLE);
					resumeDetailTextView.findViewById(R.id.view_for_space).setVisibility(View.GONE);
					tView.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
				}
				dotView.setTag(model.getId() + "$"
						+ model.getResumeName());
				dotView.setOnClickListener(this);

				tView.setText("" + model.getResumeName());

				resumesDetailView.addView(resumeDetailTextView);

				if(i!=(size-1)){

					View oneLineView = inflater.inflate(
							R.layout.one_line_view, null);

					resumesDetailView.addView(oneLineView);

				}


			}

			addResumeBtn.setVisibility(View.VISIBLE);
			addResumeBtn.setOnClickListener(this);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {

				case R.id.add_new_resumes:
					if (resumesList.size() >= 5) {
						DialogUtils.showErrorToast("You can upload upto 5 Resume.If you want to upload a new Resume Please Delete a Resume");
					} else {
						Bundle bundle2 = new Bundle();
						bundle2.putInt(URLConfig.KEY_MY_PROFILE_EDIT,
								URLConfig.UPDATE_RESUMES);
						bundle2.putString(URLConfig.KEY_ACTION,
								URLConfig.ADD_OPERATION);
						bundle2.putString(URLConfig.KEY_PROFILE_TYPE,
								URLConfig.RESUMES_PROFILE);

						bundle2.putString(URLConfig.USERID, ShineSharedPreferences.getCandidateId(mActivity));

						bundle2.putBoolean(URLConfig.FROM_INSIDE_ADD_RESUME, true);
						ResumeUploadFrg frg = new ResumeUploadFrg();
						frg.setArguments(bundle2);
						((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);
//
//						Intent intent=new Intent(mActivity, ResumeUploadActivity.class);
//						intent.putExtras(bundle2);
//						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//						mActivity.startActivity(intent);

					}


					break;
				case R.id.menu_resume:

					showMenu(v);
					break;
				case R.id.ok_btn:
					DialogUtils.dialogDismiss(alertDialog);
					break;

				default:
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void showMenu(View v) {
		PopupMenu popup = new PopupMenu(mActivity, v);
		popup.setOnMenuItemClickListener(this);
		popup.inflate(R.menu.resume_view_popup);
		if(resumesList.size()<=1)
		{
			popup.getMenu().findItem(R.id.action_delete).setVisible(false);
		}

		Log.d("name "," "+ v.getTag());


		try {
			StringTokenizer stringTokenizer=new StringTokenizer((String) v.getTag(),"$");

			id = stringTokenizer.nextToken();
			Log.d("qwerty ",id+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(ResumeDetails model : resumesList)
		{
			if(model.getId().equals(id) && model.getIsDefault() == 1)
			{
				popup.getMenu().findItem(R.id.action_default).setVisible(false);
				popup.getMenu().findItem(R.id.action_delete).setVisible(false);
				break;
			}
		}
		popup.show();

	}

	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_default:
				setDefaultResume();

				return true;
			case R.id.action_delete:
				deleteResume();
				return true;
			case R.id.action_download:

				if(ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					downloadResume();
				}
				else
				{
					if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity,
							Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						Log.d("m_cooker", "Shoud'ave shown rationale dialog");
					}
					else
					{
						Log.d("m_cooker", "meh..user is not retarded");

					}
					parentFrg.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS);
				}



				return true;
			default:
				return false;
		}
	}


	String deleteResumeRequestId = null;
	private void deleteResume()
	{

		try {

			parent.findViewById(R.id.resume_detail_view).setVisibility(View.GONE);
			loadingView.setVisibility(View.VISIBLE);

			String URL = URLConfig.RESUME_EDIT_URL;

			Type listType = new TypeToken<EmptyModel>() {}.getType();

			URL = URL.replace(URLConfig.RESUME_ID, id).replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));

			deleteResumeRequestId = id;
			VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
					URL, listType);
			downloader.setUseDeleteMethod();

			downloader.execute(UPDATE_RESUME_DETAILS);

		}catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	String setDefaultResumeRequestId  = null;
	private void setDefaultResume()
	{
		try {

			parent.findViewById(R.id.resume_detail_view).setVisibility(View.GONE);
			loadingView.setVisibility(View.VISIBLE);

			String URL = URLConfig.RESUME_EDIT_URL;

			URL = URL.replace(URLConfig.RESUME_ID, id).replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));

			Type listType = new TypeToken<EmptyModel>(){}.getType();

			VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
					URL, listType);

			HashMap resumeMap = new HashMap();

			resumeMap.put("is_default", "1");

			downloader.setUsePutMethod(resumeMap);

			downloader.execute(UPDATE_RESUME_DETAILS_DEFAULT);
			setDefaultResumeRequestId = id;

		}catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public static final int REQUEST_STORAGE_PERMISSIONS = 10;

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

		try{

			Map<String, Integer> perms = new HashMap<>();
			for (int i = 0; i < permissions.length; i++)
				perms.put(permissions[i], grantResults[i]);


			if (requestCode == REQUEST_STORAGE_PERMISSIONS) {

				if(loadingView!=null)
					loadingView.setVisibility(View.GONE);
				if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED
						&& perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED) {
					downloadResume();
				} else {
					Log.i("m_cooker", "STORAGE permission was NOT granted.");
					Snackbar.make(parent, "You need give storage permission to download the resume.",
							Snackbar.LENGTH_SHORT)
							.setAction("Settings", new OnClickListener() {
								@Override
								public void onClick(View v) {
									Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
											Uri.fromParts("package", mActivity.getPackageName(), null));
									intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									mActivity.startActivity(intent);
								}
							})
							.show();

					//				Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show();
				}

			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private void downloadResume()
	{
		try{

			parent.findViewById(R.id.resume_detail_view).setVisibility(View.GONE);
			loadingView.setVisibility(View.VISIBLE);

			String URL = URLConfig.RESUME_DOWNLOAD_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
			URL = URL.replace(URLConfig.RESUME_ID, id);

			VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
					URL);

			downloader.setUseInsRequest();
			downloader.execute(RESUME_DOWNLOAD);

		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void OnDataResponse(final Object object, final String tag) {

		try {
			mActivity.runOnUiThread(new Runnable() {



				@Override
				public void run() {
					if (object != null) {
						try {
							loadingView.setVisibility(View.GONE);
							parent.findViewById(R.id.resume_detail_view).setVisibility(View.VISIBLE);

							String lTAG= tag;

							if (lTAG
									.equals(UPDATE_RESUME_DETAILS)) {
								for(int i=0;i<resumesList.size();i++)
								{
									if(resumesList.get(i).getId().equals(deleteResumeRequestId))
									{

										Log.d("inside_delete",resumesList.get(i).getId()+"");
										resumesList.remove(i);
										deleteResumeRequestId = null;
										ShineCommons.trackShineEvents("Delete", "Resume", mActivity);
										break;
									}
								}
								//deleteResumeRequestId should be null here
								DataCaching.getInstance().cacheData(DataCaching.RESUME_LIST_SECTION,
										"" + DataCaching.CACHE_TIME_LIMIT, resumesList, mActivity);
								showDetails(resumesList);
								DialogUtils.showErrorToast("Resume Deleted Successfully");

							} else if (lTAG
									.equals(UPDATE_RESUME_DETAILS_DEFAULT)) {
								for(int i=0;i<resumesList.size(); i++)
								{
									if(resumesList.get(i).getId().equals(setDefaultResumeRequestId))
									{
										setDefaultResumeRequestId = null;
										resumesList.get(i).setIsDefault(1);
									}
									else
									{
										resumesList.get(i).setIsDefault(0);
									}
								}
								//setDefaultResumeRequestId should be null here
								DataCaching.getInstance().cacheData(DataCaching.RESUME_LIST_SECTION,
										"" + DataCaching.CACHE_TIME_LIMIT, resumesList, mActivity);
								showDetails(resumesList);
								DialogUtils.showErrorToast("Default Resume Changed Successfully");

							} else if (lTAG.equals(RESUME_DOWNLOAD)) {

								try {
									String f_name="";
									int count;
									byte[] response =(byte[])object;
									String content = CustomInsRequest.responseHeaders.get("Content-Disposition");
									String[] arrTag = content.split("=");

									String filename = arrTag[1];
									filename = filename.replace(":", ".");

									try{

										InputStream input = new ByteArrayInputStream(response);
										File path = Environment.getExternalStorageDirectory();
										File file1 = new File(path+"/"+filename);
                                        if (!file1.exists()) {
                                            file1.createNewFile();
                                        }
										f_name=file1.toString();
//                                            map.put("resume_path", file.toString());
										BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file1));
										byte data[] = new byte[1024];

										long total = 0;

										while ((count = input.read(data)) != -1) {
											total += count;
											output.write(data, 0, count);
										}

										output.flush();

										output.close();
										input.close();



									}catch(IOException e){
										e.printStackTrace();
//                                            map.put(URLConfig.KEY_ERROR, "Download Failed. Memory card not present/ No space available on Memory card.");
									}

									File file = new File(f_name);
									Uri selectedUri = Uri.fromFile(file);
									String fileExtension = MimeTypeMap
											.getFileExtensionFromUrl(selectedUri
													.toString());

									String mimeType = MimeTypeMap
											.getSingleton()
											.getMimeTypeFromExtension(fileExtension);

									Log.d("RESUME DOWNLOAD ","file name "+filename+" mime type "+mimeType+" f_name "+f_name+" extension "+fileExtension+" selectedURI "+selectedUri.toString());
									if (mimeType != null) {
										DownloadManager dm = (DownloadManager) mActivity
												.getSystemService(Context.DOWNLOAD_SERVICE);
										dm.addCompletedDownload(filename,
												"Resume Download Completed", true, mimeType,
												f_name, file.length(), true);
//                                            getResumeDetails(true);
										Toast.makeText(mActivity,
												"Resume downloaded successfully.",
												Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(mActivity,
												"Unable to download resume on your device",
												Toast.LENGTH_SHORT).show();
									}

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}


						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			});

		}catch (Exception e) {
			e.printStackTrace();
		}

	}


	@Override
	public void OnDataResponseError(final String error, String tag) {
		try {
			Log.d("inside_error",error+"");

			if(loadingView.getVisibility() == View.VISIBLE)
				loadingView.setVisibility(View.GONE);

			if(parent.findViewById(R.id.resume_detail_view).getVisibility() == View.GONE)
				parent.findViewById(R.id.resume_detail_view).setVisibility(View.VISIBLE);

			mActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					DialogUtils.dialogDismiss(alertDialog);
					DialogUtils.showErrorMsg(loadingView, error,
							DialogUtils.ERR_NO_IMG);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}







}
