package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.SimilarJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by ujjawal-work on 19/08/16.
 */

public class AppliedWithoutResumeConfirmation extends BaseFragment {

    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
    private SimilarJobsAdapter adapter_similar;
    private RecyclerView similar_list;
    private ApplywithoutResumeModel awr;
    private TextView appliedText;
    private View view;

    @Override
    public void onResume() {
        super.onResume();
        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.applied_without_resume_confirmation, container, false);
        similar_list = (RecyclerView) view.findViewById(R.id.grid_view_similar);
        similar_list.setLayoutManager(new LinearLayoutManager(mActivity));
        adapter_similar = new SimilarJobsAdapter(mActivity,jobList);
        appliedText = (TextView) view.findViewById(R.id.apply_thanks);
        mActivity.setTitle("Confirmation");
        Bundle bundle = getArguments();

        if (bundle.getBoolean("already_applied")){
            appliedText.setText("You have already applied");
        }
        awr = (ApplywithoutResumeModel) bundle.getSerializable("apply_model");
        if(awr==null)
            awr = new ApplywithoutResumeModel();


        similarJobs();
        return view;
    }

    private void similarJobs(){

        Type type = new TypeToken<SimpleSearchModel>() {}.getType();
        final View loadingView =  view.findViewById(R.id.loading_cmp);
        DialogUtils.showLoading(mActivity, getString(R.string.loading), loadingView);
        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                SimpleSearchModel model = (SimpleSearchModel) object;
                jobList.clear();
                jobList.addAll(model.results);
                if (jobList.size()>0) {
                    adapter_similar.notifyDataSetChanged();
                    similar_list.setAdapter(adapter_similar);
                    loadingView.setVisibility(View.GONE);
                    similar_list.setVisibility(View.VISIBLE);
                }
                else {
                    DialogUtils.showErrorMsg(loadingView, "No similar Jobs", DialogUtils.ERR_NO_SEARCH_RESULT);
                }
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.showErrorMsg(loadingView, error, DialogUtils.ERR_GENERIC);
            }
        },
                URLConfig.SIMILAR_JOBS_URL_LOGOUT + "&jobid=" + awr.job_id + "&page="
                        + 1 + "&perpage=" + URLConfig.PER_PAGE
                        + "&date_sort="
                        + URLConfig.NO_OF_DATE
                        + "&fl=id,,JCID,jRUrl,is_applied,jCName,jCType,jKwds,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate",
                type);

        downloader.execute("SimilarJobs");
    }




}
