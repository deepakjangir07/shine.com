package com.net.shine.utils.auth;

import android.content.Context;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetUserStatusListener;
import com.net.shine.models.AuthTokenModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

public class GetUserAccessToken implements VolleyRequestCompleteListener {
    private Context mContext;
    private String mEmail;
    private String mPassword;
    private String mApiHitTag;

    private VolleyNetworkRequest downloader;
    private GetUserStatusListener callbackListener;

    public GetUserAccessToken(Context mContext,String email,String password,GetUserStatusListener callbackListener)
    {
        this.mContext=mContext;
        this.mEmail=email;
        this.mPassword=password;
        ShineSharedPreferences.saveUserEmail(mContext,email);
        ShineSharedPreferences.saveUserPass(mContext, password);
        this.callbackListener=callbackListener;
    }


    public GetUserAccessToken(Context mContext,VolleyNetworkRequest downloader,String ApiHitTag)
    {
        this.mContext=mContext;
        this.mApiHitTag=ApiHitTag;
        this.downloader=downloader;
        this.mEmail=ShineSharedPreferences.getUserEmail(mContext);
        this.mPassword=ShineSharedPreferences.getUserPass(mContext);
    }

    public void getUserAccessToken()
    {
        Type type = new TypeToken<AuthTokenModel>(){}.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mContext, this,
                URLConfig.USER_AUTH_API, type);
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put("email", mEmail);
        paramsMap.put("password", mPassword);
        req.setUsePostMethod(paramsMap);
        req.execute("UserAccessToken");
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        AuthTokenModel model = (AuthTokenModel) object;

        Log.d("DEBUG::PROG", model.candidate_id+" is the can_id");

                    ShineSharedPreferences.setUserAccessToken(mContext, model.access_token);
                    if(callbackListener!=null)
                        callbackListener.hitUserStatusApi(model.candidate_id);
                    else
                    {
                        downloader.addHeader("User-Access-Token", model.access_token);
                        downloader.execute(mApiHitTag);
                    }
            }

    @Override
    public void OnDataResponseError(String error, String tag) {

        if(callbackListener!=null)
            callbackListener.onUserAuthError(error);
        else //if(error.contains("redentials"))   //password may have been changed
        {
                if(mContext instanceof BaseActivity)
                {
                    ((BaseActivity) mContext).logout("Your login credentials has been updated, please login with your new credentials");
                }
        }

    }


}
