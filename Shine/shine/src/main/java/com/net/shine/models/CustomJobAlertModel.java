package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Deepak on 23/08/16.
 */
public class CustomJobAlertModel implements Serializable {

    String id="";
    String email="";
    String name="";
    String keywords="";
    ArrayList<String> locid= new ArrayList<>();
    String exp = "";
    ArrayList<String> area = new ArrayList<>();
    ArrayList<String> ind = new ArrayList<>();
    String sal = "";
    int medium;

    public ArrayList<String> getArea() {
        if(area==null)
            return new ArrayList<>();
        return area;
    }

    public void setArea(ArrayList<String> area) {
        this.area = area;
    }

    public String getExp() {
        if(exp==null)
            return "";
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getInd() {
        if(ind==null)
            return new ArrayList<>();
        return ind;
    }

    public void setInd(ArrayList<String> ind) {
        this.ind = ind;
    }

    public ArrayList<String> getLocid() {
        if(locid==null)
            return new ArrayList<>();
        return locid;
    }

    public void setLocid(ArrayList<String> locid) {
        this.locid = locid;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public int getMedium() {
        return medium;
    }

    public void setMedium(int medium) {
        this.medium = medium;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }
}
