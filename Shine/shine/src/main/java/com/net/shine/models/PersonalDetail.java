package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class PersonalDetail implements Serializable {
    @SerializedName("first_name")
    private String firstName = "";

    @SerializedName("last_name")
    private String lastName = "";

    @SerializedName("cell_phone")
    private String cellPhone = "";

    @SerializedName("date_of_birth")
    private String dateOfBirth = "";

    @SerializedName("email")
    private String email = "";

    @SerializedName("candidate_location")
    private int location;
    @SerializedName("gender")
    private int gender;

    @SerializedName("is_email_verified")
    private int emailVerified;

    @SerializedName("is_cell_phone_verified")
    private int cellPhoneVerified;
    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(int emailVerified) {
        this.emailVerified = emailVerified;
    }

    public int getCellPhoneVerified() {
        return cellPhoneVerified;
    }

    public void setCellPhoneVerified(int cellPhoneVerified) {
        this.cellPhoneVerified = cellPhoneVerified;
    }


}
