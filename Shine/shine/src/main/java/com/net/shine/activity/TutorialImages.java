package com.net.shine.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.fragments.Tutorial;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.io.InputStream;
import java.util.ArrayList;

public class TutorialImages extends BaseActivity {

    private ArrayList<Tutorial> frags = new ArrayList<>();
    private ViewPager viewPager;
    private LinearLayout mHelpScreenDotsLayout;
    private ImageView cross;
    private TextView login,register;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_images);

        mHelpScreenDotsLayout = (LinearLayout) findViewById(R.id.js_area_dots_layout);
        cross = (ImageView) findViewById(R.id.cross);
        login = (TextView) findViewById(R.id.login_btn);
        register = (TextView) findViewById(R.id.register_btn);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        frags.add(Tutorial.newInstance(R.drawable.tutorial_a));
        frags.add(Tutorial.newInstance(R.drawable.tutorial_b));
        frags.add(Tutorial.newInstance(R.drawable.tutorial_c));
        frags.add(Tutorial.newInstance(R.drawable.tutorial_d));


        final MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);


        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ShineSharedPreferences.getTutpageVisitedFirstTime(TutorialImages.this))    //true means it has been visited before
                {
                    TutorialImages.this.finish();
                    return;
                }

                ShineSharedPreferences.saveTutpageVisitedFirstTime(TutorialImages.this);
                Intent intent = new Intent(TutorialImages.this,
                        HomeActivity.class);
                startActivity(intent);
                TutorialImages.this.finish();

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShineSharedPreferences.saveTutpageVisitedFirstTime(TutorialImages.this);
                Intent intent = new Intent(TutorialImages.this, AuthActivity.class);
                intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.LOGIN_FRAG);
                startActivity(intent);
                TutorialImages.this.finish();

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShineSharedPreferences.saveTutpageVisitedFirstTime(TutorialImages.this);
                Intent intent = new Intent(TutorialImages.this, AuthActivity.class);
                intent.putExtra(AuthActivity.SELECTED_FRAG, AuthActivity.REGISTER_FRAG);
                startActivity(intent);
                TutorialImages.this.finish();

            }
        });


        final ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(final int i) {
                showSelectedDotOnHelpScreen(i, false);

            }

            @Override
            public void onPageScrollStateChanged(int i) {


            }
        };

        viewPager.addOnPageChangeListener(listener);
        viewPager.post(new Runnable() {
            @Override
            public void run() {
                listener.onPageSelected(0);
            }
        });
        showSelectedDotOnHelpScreen(0, true);


    }

    public void showSelectedDotOnHelpScreen(int pos, boolean createView) {
        try {

            for (int tempC = 0; tempC < 4; tempC++) {
                ImageView imageV;
                int tempId = tempC + 1;
                if (createView) {
                    imageV = new ImageView(this);
                    imageV.setId(tempId);
                    imageV.setPadding(5, 5, 5, 5);
                    ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    imageV.setLayoutParams(layoutParams);
                    mHelpScreenDotsLayout.addView(imageV);
                } else {
                    imageV = (ImageView) mHelpScreenDotsLayout.findViewById(tempId);
                }
                InputStream is;
                if (tempC == pos) {
                    is = getResources().openRawResource(+R.drawable.slider_dot_selected);

                } else {
                    is = getResources().openRawResource(+R.drawable.slider_dot);

                }
                Bitmap imageBitmap = BitmapFactory.decodeStream(is);
                imageV.setImageBitmap(imageBitmap);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {

        if (ShineSharedPreferences.getTutpageVisitedFirstTime(TutorialImages.this))    //true means it has been visited before
        {

            TutorialImages.this.finish();
            return;
        }


        ShineSharedPreferences.saveTutpageVisitedFirstTime(TutorialImages.this);
        Intent intent = new Intent(TutorialImages.this,
                HomeActivity.class);
        startActivity(intent);
        TutorialImages.this.finish();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {

            return frags.get(pos);
        }

        @Override
        public int getCount() {
            return frags.size();
        }

    }
}



