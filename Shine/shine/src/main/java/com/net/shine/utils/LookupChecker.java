package com.net.shine.utils;

/**
 * Created by Deepak on 15/12/16.
 */


        import android.content.Context;
        import android.content.Intent;
        import android.database.Cursor;
        import android.util.Log;

        import com.google.gson.JsonElement;
        import com.google.gson.reflect.TypeToken;
        import com.net.shine.MyApplication;

        import com.net.shine.config.URLConfig;
        import com.net.shine.services.LookupIntentService;
        import com.net.shine.utils.db.LookupInit;
        import com.net.shine.volley.VolleyNetworkRequest;
        import com.net.shine.volley.VolleyRequestCompleteListener;

        import org.json.JSONObject;

        import java.lang.reflect.Type;

   public class LookupChecker {

    private Context mContext;

    public static String CREATE_LOOKUP_FUNCTIONALAREA_TABLE = "lookup_functionalarea_table";

//	public static String CREATE_LOOKUP_SKILL_TABLE = "lookup_skill_table";

    public static String CREATE_LOOKUP_INDUSTRY_TABLE = "lookup_industry_table";

    public static String CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE = "lookup_teamsizemanaged_table";
    public static String CREATE_LOOKUP_ANNUALSALARY_TABLE = "lookup_annualsalary_table";

    public static String CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE = "lookup_edu_qualification_level_table";

    public static String CREATE_LOOKUP_EDU_INSTITUTE_TABLE = "lookup_edu_institute_table";
    public static String CREATE_LOOKUP_CITY_TABLE = "lookup_city_table";
    public static String CREATE_LOOKUP_EXPERIENCE = "lookup_experience_table";
    public static String CREATE_LOOKUP_COURSETYPE_TABLE = "lookup_coursetype_table";

    public final String[] mLookupTables = new String[] {
            CREATE_LOOKUP_CITY_TABLE, CREATE_LOOKUP_FUNCTIONALAREA_TABLE,
            CREATE_LOOKUP_INDUSTRY_TABLE, CREATE_LOOKUP_EXPERIENCE,
            CREATE_LOOKUP_ANNUALSALARY_TABLE,
            CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE,
            CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE,
            CREATE_LOOKUP_EDU_INSTITUTE_TABLE,

            CREATE_LOOKUP_COURSETYPE_TABLE };

    public final String[] mLookupFileUrls = new String[] {
            URLConfig.LOOKUP_CITY, URLConfig.LOOKUP_FUNCTIONALAREA,
            URLConfig.LOOKUP_INDUSTRY, URLConfig.LOOKUP_EXPERIENCE,
            URLConfig.LOOKUP_ANNUALSALARY, URLConfig.LOOKUP_TEAMSIZEMANAGED,

//            URLConfig.LOOKUP_EDUCATIONQUALIFICATIONLEVEL,
            URLConfig.LOOKUP_EDUCATIONSTREAM,
            URLConfig.LOOKUP_EDUCATIONINSTITUTE, URLConfig.LOOKUP_COURSETYPE };

    public LookupChecker(Context context) {
        // TODO Auto-generated constructor stub
        this.mContext = context;
    }

//	private void CheckLookUpdateCondition(int lookup_version_new) {
//		// TODO Auto-generated method stub
//
//		UpdateLookupsFiles(lookup_version_new);
//
//	}

    private void UpdateLookupsFiles(int lookup_version_new) {
        // TODO Auto-generated method stub

        try {

            Cursor cursor = MyApplication.getDataBase()
                    .getLookupVersionFromDatabase();

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        int lookup_version_db = cursor.getInt(cursor
                                .getColumnIndex("version"));

                        String tablename = cursor.getString(cursor
                                .getColumnIndex("tablename"));
                        String url = cursor.getString(cursor
                                .getColumnIndex("lookup_url"));

                        Log.d("DEBUG::LOOKUP", "UpdateLookupsFiles:  DB Lookup version " + lookup_version_db
                                + " Server lookup version " + lookup_version_new
                                + " table  " + tablename);

                        if (lookup_version_new > lookup_version_db) {

                            datadownloader(tablename, lookup_version_new, url);

                        }
                    }
                }
                cursor.close();
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void datadownloader(final String tableName, final int lookup_version_new,
                               String url) {
        Log.d("DEBUG::LOOKUP", "datadownloader hit: " + url);
        //TODO: Ujjawal - Create a model for Lookups
        Type type = new TypeToken<JsonElement>(){}.getType();

        //We need inner listener here as multiple hits are being sent in parallel from this class
        VolleyNetworkRequest req = new VolleyNetworkRequest(mContext, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                try {

                    JSONObject obj = new JSONObject(object.toString());
                    Intent intent = new Intent(mContext, LookupIntentService.class);
                    intent.putExtra("tableName", tableName);
                    intent.putExtra("version", lookup_version_new);
                    Log.d("DEBUG::LOOKUP", "datadownloader response in lookupchecker: Lookup version" + lookup_version_new
                            + "for table: " + tableName);
                    intent.putExtra("result", obj.getString("results"));
                    mContext.startService(intent);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("DEBUG::LOOKUP", "datadownloader error in lookupchecker: " + error + " for table: " + tableName);
            }
        }, url, type);
        req.execute("lookupDataDownloader");

    }

    public void checkVersionTableExists(int lookup_version_server) {
        if (!MyApplication.getDataBase().isLookupTableFilledOrExists()) {
            Log.d("DEBUG::LOOKUP", "checkVersionTableExists: Lookup table empty. Inserting version " + LookupInit.CURRENT_FILE_LOOKUP_VERSION);
            //setting current lookup version in db as 0, so that it will be updated
            for (int i = 0; i < mLookupTables.length; i++) {
                MyApplication.getDataBase().insertLookupVersion(
                        mLookupTables[i], LookupInit.CURRENT_FILE_LOOKUP_VERSION, mLookupFileUrls[i]);
            }
        }
        UpdateLookupsFiles(lookup_version_server);
    }

}