package com.net.shine.models;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.net.shine.config.URLConfig;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Deepak on 15/12/16.
 */


public class WhoViewMyProfileModel implements Serializable {

    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("next")
    @Expose
    public Object next;
    @SerializedName("previous")
    @Expose
    public Object previous;
    @SerializedName("results")
    @Expose
    public ArrayList<Result> results = new ArrayList<>();


    public class Result implements Serializable {

        @SerializedName("rect_id")
        @Expose
        public Integer rectId;
        @SerializedName("company_name")
        @Expose
        public String companyName;
        @SerializedName("automatch_jobs")
        @Expose
        public List<AutomatchJob> automatchJobs = new ArrayList<>();
        @SerializedName("automatch_jobs_count")
        @Expose
        public Integer automatchJobsCount;
        @SerializedName("application_activities")
        @Expose
        public Integer applicationActivities;
        @SerializedName("mail_activities")
        @Expose
        public List<MailActivity> mailActivities = new ArrayList<>();
        @SerializedName("mail_count")
        @Expose
        public Integer mailCount;
        @SerializedName("latest_timestamp")
        @Expose
        public String latestTimestamp;
        @SerializedName("activity_log")
        @Expose
        public List<String> activityLog = new ArrayList<>();
        @SerializedName("found_via")
        @Expose
        public String foundVia;

        @SerializedName("avg_rank")
        public Integer avg_rank;

        @SerializedName("viewed")
        public boolean viewed;

        @SerializedName("contacted")
        public boolean contacted;

        @SerializedName("mail_in_past")
        public boolean mail_in_past;

        @SerializedName("is_new_activity")
        public boolean is_new_activity;



    }

    public class MailActivity implements Serializable {

        @SerializedName("subject_line")
        @Expose
        public String subjectLine;
        @SerializedName("obj_id")
        @Expose
        public String objId;
        @SerializedName("timestamp")
        @Expose
        public String timestamp;
        @SerializedName("company_id")
        @Expose
        public Integer companyId;
        @SerializedName("recruiter_name")
        @Expose
        public String recruiterName;
        @SerializedName("recruiter_id")
        @Expose
        public Integer recruiterId;
        @SerializedName("has_read")
        @Expose
        public Integer hasRead;
        @SerializedName("hash_code")
        @Expose
        public String hashCode;


    }

    public class AutomatchJob implements Serializable {



        @SerializedName("jRR")
        public int resume_req = 1;

        public String jRUrl = "";

        @SerializedName("id")
        public String jobId = "";

        @SerializedName("jJobType")
        public int jJobType = 0;


        @SerializedName("jWLC")
        public String jWLC = "";

        @SerializedName("jWLocID")
        public int jWLocID;

        @SerializedName("jWSD")
        public String jWSD;

        @SerializedName("jExpDate")
        public String jExpDate;
        @SerializedName("jJT")
        public String jobTitle = "";
        public boolean is_applied = false;
        @SerializedName("jCUID")
        public ArrayList<String> comp_uid_list = new ArrayList<>();
        @SerializedName("jCName")
        public String comp_name = "";
        @SerializedName("jCID")
        public String rect_id = "";
        @SerializedName("jLoc")
        public List<String> job_loc_list = new ArrayList<>();
        public String job_loc_str = "";
        @SerializedName("jCName_slug")
        public String company_slug = "";
        @SerializedName("jJT_slug")
        public String title_slug = "";
        //Gson will NOT fill this var, we fill it on getting the connections
        //Do not add a default value for this var, as
        //  - null means we need to make a conn hit
        //  - empty model means 0 connections in comp
        public DiscoverModel.Company frnd_model;
        @SerializedName("jExp")
        public String jobExperience = "";
        /**
         * Job Detail Extra Fields
         **/

        @SerializedName("jKwd")
        public String total_skills = "";
        //TODO Ujjawal: We can try using JsonElement instead of using two variables,
        //              its getAsString() method works good but needs testing.
        @SerializedName("jArea")
        public ArrayList<String> job_area_list = new ArrayList<>();
        public String job_func_area_str = "";
        @SerializedName("jInd")
        public String jobIndustries = "";
        @SerializedName("jSal")
        public String jobSalary = "";
        @SerializedName("jJD")
        public String job_desc = "";
        @SerializedName("jPDate")
        public String posted_date = "";
        @SerializedName("jCD")
        public String comp_desc = "";
        String walkInDate = null;
        String walkInVenue = null;
        @SerializedName("jCType")
        boolean is_company;

        public String getVenue() {

            if (!TextUtils.isEmpty(walkInVenue))
                return walkInVenue;

            try {
                if (!TextUtils.isEmpty(URLConfig.CITY_REVERSE_MAP.get(jWLocID + ""))) {
                    walkInVenue = jWLC + ", " + URLConfig.CITY_REVERSE_MAP.get(jWLocID + "");
                } else {
                    walkInVenue = jWLC;
                }
                return walkInVenue;
            } catch (Exception e) {
                e.printStackTrace();
            }
            walkInVenue = jWLC;
            return walkInVenue;
        }

        public String getDate() {

            if (!TextUtils.isEmpty(walkInDate))
                return walkInDate;

            try {

                Date startDate = null;
//            Date startTime = null;
                SimpleDateFormat serverDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                SimpleDateFormat clientDateFormatter = new SimpleDateFormat("dd MMM", Locale.getDefault());

//                SimpleDateFormat serverTimeFormatter = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
//                SimpleDateFormat clientTimeFormatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());


                if (jWSD != null && jWSD.length() >= 10) {
                    String StrstartDate = jWSD.substring(0, 10);
                    startDate = serverDateFormatter.parse(StrstartDate);
//                String StrstartTime = jWSD.substring(11,19);
//                startTime = serverTimeFormatter.parse(StrstartTime);
                }
                Date expDate = null;
//            Date expTime = null;
                if (jExpDate.length() >= 10) {
                    String StrexpDate = jExpDate.substring(0, 10);
                    expDate = serverDateFormatter.parse(StrexpDate);
//                String StrexpTime = jExpDate.substring(11,19);
//                expTime = serverTimeFormatter.parse(StrexpTime);
                }

                walkInDate = clientDateFormatter.format(startDate) + " - " + clientDateFormatter.format(expDate);
//                +
//                    " | " + clientTimeFormatter.format(startTime) + " - " + clientTimeFormatter.format(expTime);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return walkInDate;

        }

        @Override
        public String toString() {
            return "Result{" +
                    "resume_req=" + resume_req +
                    ", jRUrl='" + jRUrl + '\'' +
                    ", jobId='" + jobId + '\'' +
                    ", jobTitle='" + jobTitle + '\'' +
                    ", is_applied=" + is_applied +
                    ", comp_uid_list=" + comp_uid_list +
                    ", comp_name='" + comp_name + '\'' +
                    ", rect_id='" + rect_id + '\'' +
                    ", is_company=" + is_company +
                    ", job_loc_list=" + job_loc_list +
                    ", job_loc_str='" + job_loc_str + '\'' +
                    ", company_slug='" + company_slug + '\'' +
                    ", title_slug='" + title_slug + '\'' +
                    ", frnd_model=" + frnd_model +
                    ", jobExperience='" + jobExperience + '\'' +
                    ", job_skills='" + total_skills + '\'' +
                    ", job_area_list=" + job_area_list +
                    ", job_func_area_str='" + job_func_area_str + '\'' +
                    ", jobIndustries='" + jobIndustries + '\'' +
                    ", jobSalary='" + jobSalary + '\'' +
                    ", job_desc='" + job_desc + '\'' +
                    ", posted_date='" + posted_date + '\'' +
                    ", comp_desc='" + comp_desc + '\'' +
                    '}';
        }


    }


}