package com.net.shine.views;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.net.shine.R;
import com.net.shine.interfaces.FocusListener;

/**
 * Created by ujjawal-work on 07/04/16.
 */
public class MyAwesomeEditText extends TextInputEditText implements FocusListener {

    public MyAwesomeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
        setEditable(attrs);
    }
    public MyAwesomeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        setEditable(attrs);
        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
    }
    public MyAwesomeEditText(Context context) {
        super(context);
        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
    }

    private void setEditable(AttributeSet attrs){

        try {

            boolean isEditable = true;
            try {
                if(attrs!=null)
                    isEditable = attrs.getAttributeBooleanValue("http://schemas.android.com/apk/res/android", "editable",true);
            }catch (Exception e){
                e.printStackTrace();
            }

            if(isEditable)
                super.setInputType(getInputType()| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            else
                super.setInputType(InputType.TYPE_NULL);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private OnFocusChangeListener myListener;

    public void setTILFocusClearer(final TextInputLayout parentTextInputLayout){
        myListener = new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus|| !TextUtils.isEmpty(getText())) {
                    parentTextInputLayout.setHintAnimationEnabled(true);
                    parentTextInputLayout.setHintEnabled(true);
                }
                else if(!TextUtils.isEmpty(parentTextInputLayout.getError())){
                    parentTextInputLayout.setHintEnabled(false);
                }
            }
        };
        super.setOnFocusChangeListener(myListener);
    }

    @Override
    public void setOnFocusChangeListener(final OnFocusChangeListener l) {
        super.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                l.onFocusChange(v, hasFocus);
                if(myListener!=null)
                    myListener.onFocusChange(v, hasFocus);
            }
        });
    }


    @Override
    public void setInputType(int type) {
        super.setInputType(type| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

    }
}
