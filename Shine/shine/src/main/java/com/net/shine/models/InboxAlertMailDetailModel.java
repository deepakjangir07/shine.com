package com.net.shine.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.net.shine.config.URLConfig;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jaspreet on 3/8/15.
 */
public class InboxAlertMailDetailModel implements Serializable {

    String id="",candidate_id="",timestamp="",mail_type="",has_read="",alert_name="",subject_line="";

   ArrayList<InboxJobDetail> matched_job_details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(String candidate_id) {
        this.candidate_id = candidate_id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMail_type() {
        return mail_type;
    }

    public void setMail_type(String mail_type) {
        this.mail_type = mail_type;
    }

    public String getHas_read() {
        return has_read;
    }

    public void setHas_read(String has_read) {
        this.has_read = has_read;
    }

    public String getAlert_name() {
        return alert_name;
    }


    public void setSubject_line(String subject_line) {
        this.subject_line = subject_line;
    }

    public String getSubject_line() {
        return subject_line;
    }


    public void setAlert_name(String alert_name) {
        this.alert_name = alert_name;
    }

    public ArrayList<InboxJobDetail> getMatched_job_details() {
        return matched_job_details;
    }

    public void setMatched_job_details(ArrayList<InboxJobDetail> matched_job_details) {
        this.matched_job_details = matched_job_details;
    }
    public  ArrayList<InboxJobDetail> other_job_details;

    public ArrayList<InboxJobDetail> getOther_job_details() {
        return other_job_details;
    }

    public void setOther_job_details(ArrayList<InboxJobDetail> other_job_details) {
        this.other_job_details = other_job_details;
    }

    @Override
    public String toString() {
        return "InboxAlertMailDetailModel{" +
                "id='" + id + '\'' +
                ", candidate_id='" + candidate_id + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", mail_type='" + mail_type + '\'' +
                ", has_read='" + has_read + '\'' +
                ", alert_name='" + alert_name + '\'' +
                ", subject_line='" + subject_line + '\'' +
                ", matched_job_details=" + matched_job_details +
                '}';
    }

    public static class InboxJobDetail implements Serializable
    {

        public DiscoverModel.Company frnd_model;

        public String jRUrl;

        @SerializedName("jRR")
        public int resume_req = 1;

        boolean is_applied;
        String company="",date,salary="",job_id="",title="",industry="",experience="",functional_area="";
        @SerializedName("company-uuid")
        ArrayList<String> companyUuid = new ArrayList<>();

        @SerializedName("title-slug")
        String titleSlug="";

         String[] location = new String[]{};

        public String location_str = "";

        public String skills =  "";


        @SerializedName("jLoc")
        public List<String> job_loc_list = new ArrayList<>();
        public String job_loc_str = "";

//        @SerializedName("jKwd")
//        public String job_skills = "";

        @SerializedName("company-slug")
        String companySlug="";
        boolean is_company;

        private DiscoverModel.Company match_job;

        public boolean is_applied() {
            return is_applied;
        }

        public void setIs_applied(boolean is_applied) {
            this.is_applied = is_applied;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getSalary() {
            return salary;
        }

        public void setSalary(String salary) {
            this.salary = salary;
        }

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIndustry() {
            return industry;
        }

        public void setIndustry(String industry) {
            this.industry = industry;
        }

        public String getExperience() {
            return experience;
        }

        public void setExperience(String experience) {
            this.experience = experience;
        }

        public String getFunctional_area() {
            return functional_area;
        }

        public void setFunctional_area(String functional_area) {
            this.functional_area = functional_area;
        }

        public ArrayList<String> getCompanyUuid() {
            return companyUuid;
        }

        public void setCompanyUuid(ArrayList<String> companyUuid) {
            this.companyUuid = companyUuid;
        }

        public String getTitleSlug() {
            return titleSlug;
        }

        public void setTitleSlug(String titleSlug) {
            this.titleSlug = titleSlug;
        }

        public String[] getLocation() {
            return location;
        }

        public void setLocation(String[] location) {
            this.location = location;
        }


        public String getCompanySlug() {
            return companySlug;
        }

        public void setCompanySlug(String companySlug) {
            this.companySlug = companySlug;
        }

        public boolean is_company() {
            return is_company;
        }

        public void setIs_company(boolean is_company) {
            this.is_company = is_company;
        }

        public DiscoverModel.Company getMatch_job() {
            return match_job;
        }

        public void setMatch_job(DiscoverModel.Company match_job) {
            this.match_job = match_job;
        }


        @SerializedName("jJobType")
        public int jJobType = 0;


        @SerializedName("jWLC")
        private String jWLC = "";

        @SerializedName("jWLocID")
        private int jWLocID = 0;

        @SerializedName("jWSD")
        private String jWSD;

        @SerializedName("jExpDate")
        private String jExpDate;

        String walkInDate = null;
        String walkInVenue = null;

        public String getVenue(){

            if(!TextUtils.isEmpty(walkInVenue))
                return walkInVenue;

            try {
                if(!TextUtils.isEmpty(URLConfig.CITY_REVERSE_MAP.get(jWLocID+""))){
                    walkInVenue = jWLC + ", "  + URLConfig.CITY_REVERSE_MAP.get(jWLocID+"");
                } else {
                    walkInVenue = jWLC;
                }
                return walkInVenue;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            walkInVenue = jWLC;
            return walkInVenue;
        }

        public String getWalkInDate() {

            if(!TextUtils.isEmpty(walkInDate))
                return walkInDate;

            try{

                Date startDate = null;
                Date startTime = null;
                SimpleDateFormat serverDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                SimpleDateFormat clientDateFormatter = new SimpleDateFormat("dd MMM", Locale.getDefault());

                SimpleDateFormat serverTimeFormatter = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                SimpleDateFormat clientTimeFormatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());


                if(jWSD.length()>18){
                    String StrstartDate = jWSD.substring(0,10);
                    startDate = serverDateFormatter.parse(StrstartDate);
                    String StrstartTime = jWSD.substring(11,19);
                    startTime = serverTimeFormatter.parse(StrstartTime);
                }
                Date expDate = null;
                Date expTime = null;
                if(jExpDate.length()>18){
                    String StrexpDate = jExpDate.substring(0,10);
                    expDate = serverDateFormatter.parse(StrexpDate);
                    String StrexpTime = jExpDate.substring(11,19);
                    expTime = serverTimeFormatter.parse(StrexpTime);
                }

                walkInDate =  clientDateFormatter.format(startDate) + " - " + clientDateFormatter.format(expDate) +
                        " | " + clientTimeFormatter.format(startTime) + " - " + clientTimeFormatter.format(expTime);

            }catch (Exception e){
                e.printStackTrace();
            }

            return walkInDate;

        }



        @Override
        public String toString() {
            return "InboxJobDetail{" +
                    "is_applied=" + is_applied +
                    ", company='" + company + '\'' +
                    ", date='" + date + '\'' +
                    ", salary='" + salary + '\'' +
                    ", job_id='" + job_id + '\'' +
                    ", title='" + title + '\'' +
                    ", industry='" + industry + '\'' +
                    ", experience='" + experience + '\'' +
                    ", functional_area='" + functional_area + '\'' +
                    ", companyUuid=" + companyUuid +
                    ", skills='" + skills +
                    ", titleSlug='" + titleSlug + '\'' +
                    ", location=" + Arrays.toString(location) +
                    ", companySlug='" + companySlug + '\'' +
                    ", is_company=" + is_company +
                    ", match_job=" + match_job +
                    '}';
        }
    }




}



