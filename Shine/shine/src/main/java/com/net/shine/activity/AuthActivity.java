package com.net.shine.activity;

import android.content.Intent;
import android.os.Bundle;

import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.LoginFrg;
import com.net.shine.fragments.RegistrationFrg;
import com.net.shine.utils.db.ShineSharedPreferences;

/**
 * Created by ujjawal-work on 18/08/16.
 */

public class AuthActivity extends BaseActivity {

    public static final int LOGIN_FRAG = 0;
    public static final int REGISTER_FRAG = 1;
    public static final String SELECTED_FRAG = "selected_frag";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null) {
            if (ShineSharedPreferences.getUserInfo(getApplicationContext()) == null) {

                Bundle bun = getIntent().getExtras();
                if (bun != null && bun.getInt(SELECTED_FRAG) == LOGIN_FRAG) {
                    BaseFragment fragment = new LoginFrg();
                    fragment.setArguments(bun);
                    setBaseFrg(fragment);
                } else {
                    BaseFragment fragment = new RegistrationFrg();
                    fragment.setArguments(bun);
                    setBaseFrg(fragment);
                }

            }
            else {
                Intent intent = new Intent(this,HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callerCompat!=null){
            callerCompat.onActivityResult(requestCode, resultCode, data);
        }
        if(gDriveCompat!=null)
        {
            gDriveCompat.onActivityResult(requestCode,resultCode,data);
        }

    }



    public AuthActivity.GoogleResolutionOnResult gDriveCompat;


    public interface GoogleResolutionOnResult {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    public TrueCallerCompat callerCompat;


    public interface TrueCallerCompat {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

}
