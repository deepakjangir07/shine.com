package com.net.shine.interfaces;

/**
 * Created by F2722 on 03-06-2015.
 */
public interface GetUserStatusListener {

    void hitUserStatusApi(String candidateId);

    void onUserAuthError(String error);
}
