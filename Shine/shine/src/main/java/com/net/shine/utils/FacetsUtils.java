package com.net.shine.utils;

import com.net.shine.config.URLConfig;
import com.net.shine.models.FacetsModel;
import com.net.shine.models.FacetsSubModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FacetsUtils {


    public static HashMap<String, Object> getFacetsModels(JSONObject obj) {
        try {
            HashMap<String, Object> map = new HashMap<>();


                JSONObject fields = obj.getJSONObject("fields");

                ArrayList<FacetsSubModel> jIndustry = new ArrayList<>();
                ArrayList<FacetsSubModel> jLocation = new ArrayList<>();
                ArrayList<FacetsSubModel> jSalary = new ArrayList<>();
                ArrayList<FacetsSubModel> jExperience = new ArrayList<>();
                ArrayList<FacetsSubModel> jFunctionalArea = new ArrayList<>();

                ArrayList<FacetsSubModel> jJobType=new ArrayList<>();


                ArrayList<FacetsSubModel> jTopCompanies=new ArrayList<>();

                JSONArray jIndArray = fields.getJSONArray("jIndID");
                for (int i = 0; i < jIndArray.length(); i++) {
                    FacetsSubModel facetsSubModel = new FacetsSubModel();
                    JSONArray jInd = (JSONArray) jIndArray.get(i);
                    facetsSubModel.setKey((String) jInd.get(0));
                    facetsSubModel.setNumber((Integer) jInd.get(1));
                    facetsSubModel.setValue((String) jInd.get(2));
                    jIndustry.add(facetsSubModel);
                }
                JSONArray jLocArray = fields.getJSONArray("jFLoc");
                for (int i = 0; i < jLocArray.length(); i++) {
                    FacetsSubModel facetsSubModel = new FacetsSubModel();
                    JSONArray jLoc = (JSONArray) jLocArray.get(i);
                    facetsSubModel.setKey((String) jLoc.get(0));
                    facetsSubModel.setNumber((Integer) jLoc.get(1));
                    facetsSubModel.setValue((String) jLoc.get(2));
                    jLocation.add(facetsSubModel);
                }
                JSONArray jExpArray = fields.getJSONArray("jFEx");
                for (int i = 0; i < jExpArray.length(); i++) {
                    FacetsSubModel facetsSubModel = new FacetsSubModel();
                    JSONArray jEx = (JSONArray) jExpArray.get(i);
                    facetsSubModel.setKey((String) jEx.get(0));
                    facetsSubModel.setNumber((Integer) jEx.get(1));
                    facetsSubModel.setValue((String) jEx.get(2));
                    jExperience.add(facetsSubModel);
                }
                JSONArray jSalArray = fields.getJSONArray("jFSal");
                for (int i = 0; i < jSalArray.length(); i++) {
                    FacetsSubModel facetsSubModel = new FacetsSubModel();
                    JSONArray jSal = (JSONArray) jSalArray.get(i);
                    facetsSubModel.setKey((String) jSal.get(0));
                    facetsSubModel.setNumber((Integer) jSal.get(1));
                    facetsSubModel.setValue((String) jSal.get(2));
                    jSalary.add(facetsSubModel);
                }
                JSONArray jFuncArray = fields.getJSONArray("jFArea");
                for (int i = 0; i < jFuncArray.length(); i++) {
                    FacetsSubModel facetsSubModel = new FacetsSubModel();
                    JSONArray jFun = (JSONArray) jFuncArray.get(i);
                    facetsSubModel.setKey((String) jFun.get(0));
                    facetsSubModel.setNumber((Integer) jFun.get(1));
                    facetsSubModel.setValue((String) jFun.get(2));
                    jFunctionalArea.add(facetsSubModel);
                }

                JSONArray jJobArray = fields.optJSONArray("jJobType");

                if(jJobArray!=null&&jJobArray.length()>0) {
                    for (int i = 0; i < jJobArray.length(); i++) {
                        FacetsSubModel facetsSubModel = new FacetsSubModel();
                        JSONArray jJobTypes = (JSONArray) jJobArray.get(i);
                        facetsSubModel.setKey((String) jJobTypes.get(0));
                        facetsSubModel.setNumber((Integer) jJobTypes.get(1));
                        facetsSubModel.setValue((String) jJobTypes.get(2));
                        jJobType.add(facetsSubModel);
                    }
                }


                JSONArray jTopCompaniesArr = fields.optJSONArray("jCIDF");
                if(jTopCompaniesArr!=null&&jTopCompaniesArr.length()>0) {
                    for (int i = 0; i < jTopCompaniesArr.length(); i++) {
                        FacetsSubModel facetsSubModel = new FacetsSubModel();
                        JSONArray jTopCompniesAr = (JSONArray) jTopCompaniesArr.get(i);
                        facetsSubModel.setKey((String) jTopCompniesAr.get(0));
                        facetsSubModel.setNumber((Integer) jTopCompniesAr.get(1));
                        facetsSubModel.setValue((String) jTopCompniesAr.get(2));
                        jTopCompanies.add(facetsSubModel);
                    }
                }





                FacetsModel facetModel = new FacetsModel();
                facetModel.setjExperience(jExperience);
                facetModel.setjIndustry(jIndustry);
                facetModel.setjSalary(jSalary);
                facetModel.setjFunctionalArea(jFunctionalArea);
                facetModel.setjLocation(jLocation);
                facetModel.setjJobType(jJobType);
                facetModel.setjTopCompanies(jTopCompanies);

                map.put(URLConfig.KEY_SEARCH_FACETS, facetModel);

            return map;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
