package com.net.shine.models;

import java.io.Serializable;

public class UserStatusModel implements Serializable {

    public  String highest_education="";

    public String last_name="";

    public int unread_alert_mails;

    public String current_company="";

    public String institute_name="";

    public String profile_title="";

    public int unread_recruiter_mails;

    public String mobile_no="";

    public String full_name="";

    public boolean  has_jobs;

    public boolean has_education;

    public int experience_in_years;

    //TODO - Ujjawal Find why we are not using this var
    public boolean is_fresher;

    public boolean has_skills;

    public boolean is_resume_mid_out;

    public String first_name="";

    public int matched_jobs_count;

    public int is_mid_out;

    public String candidate_id="";

    //TODO - Ujjawal Find why we are not using this var
    public int other_matched_jobs_count;

    public String email="";

    public String job_title="";

    private SyncStatus sync_status = new SyncStatus();


    //TODO - Ujjawal Try creating default constructors instead of getter setters
    private static class SyncStatus implements Serializable
    {
        private PhoneBook phonebook = new PhoneBook();
        private Linkedin linkedin = new Linkedin();
        private Gmail  gmail = new Gmail();


        public static class PhoneBook implements Serializable
        {
            public boolean synced;
            public String next_phonebook_sync_on = "";
            public String last_synced="";
        }

        public static class Linkedin implements Serializable
        {
            public boolean  synced;
            public String last_synced="";
        }
        public static class Gmail implements Serializable
        {
            public boolean  synced;
            public String last_synced  ="";

        }


    }



    public boolean getIsPhonebookSynced()
    {
        if(sync_status!=null&&sync_status.phonebook!=null)
            return sync_status.phonebook.synced;
        return false;
    }

    public boolean getIsLinkedinSynced()
    {
        if(sync_status!=null&&sync_status.linkedin!=null)
            return sync_status.linkedin.synced;
        return false;
    }

    public boolean getIsGmailSynced()
    {
        if(sync_status!=null&&sync_status.gmail!=null)
            return sync_status.gmail.synced;
        return false;
    }

    public String get_next_phonebook_sync_on() {
        if(sync_status!=null&&sync_status.phonebook!=null)
            return sync_status.phonebook.next_phonebook_sync_on;
        return "";
    }

    public String get_last_phonebook_sync() {
        if(sync_status!=null&&sync_status.phonebook!=null)
            return sync_status.phonebook.last_synced;
        return "";
    }

    public String get_last_linkedin_sync() {
        if(sync_status!=null&&sync_status.linkedin!=null&&!sync_status.linkedin.last_synced.equals("null"))
            return sync_status.linkedin.last_synced;
        return "";
    }

    public String get_last_gmail_sync() {
        if(sync_status!=null&&sync_status.gmail!=null&&!sync_status.gmail.last_synced.equals("null"))
            return sync_status.gmail.last_synced;
        return "";
    }

    public void set_next_phonebook_sync_on(String time)
    {
        if(sync_status==null)
            sync_status = new SyncStatus();
        if(sync_status.phonebook==null)
        sync_status.phonebook = new SyncStatus.PhoneBook();

        sync_status.phonebook.next_phonebook_sync_on = time;

    }

    public void set_phonebook_synced(boolean flag)
    {
        if(sync_status==null)
            sync_status = new SyncStatus();
        if(sync_status.phonebook==null)
            sync_status.phonebook = new SyncStatus.PhoneBook();

        sync_status.phonebook.synced = flag;

    }

}

