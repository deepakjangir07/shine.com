package com.net.shine.fragments.components;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.TotalExperience;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;


public class ExperienceDetailsEditFragment extends BaseFragment implements View.OnClickListener, VolleyRequestCompleteListener, View.OnFocusChangeListener {

    private TotalExperience totalExModel;
    private View view;
    private LinearLayout container_box;
    TextInputLayout inputLayoutSalary,inputLayoutSalaryInThousands, inputLayoutExperience,inputLayoutExperienceInMonths, inputLayoutTeam, inputLayoutNoticePeriod, inputLayoutProfileTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.experience_details_edit, container, false);
        container_box = (LinearLayout) view.findViewById(R.id.e_details);
        container_box.requestFocus();

        TextView tv_title = (TextView) view.findViewById(R.id.header_title);
        tv_title.setText("Experience Details");

        Bundle bundle = getArguments();

        Gson gson = new Gson();

        String json = bundle.getString(URLConfig.KEY_MODEL_NAME);
        totalExModel = gson.fromJson(json, TotalExperience.class);
        addUpdateTotal();
        TextChangeListener.addListener(salaryField,inputLayoutSalary,mActivity);
        TextChangeListener.addListener(salaryTField,inputLayoutSalaryInThousands,mActivity);
        TextChangeListener.addListener(noticePeriodField,inputLayoutNoticePeriod,mActivity);
        TextChangeListener.addListener(expField,inputLayoutExperience,mActivity);
        TextChangeListener.addListener(expMonthsField,inputLayoutExperienceInMonths,mActivity);
        TextChangeListener.addListener(teamSizeField,inputLayoutTeam,mActivity);
        TextChangeListener.addListener(resumeTitleField,inputLayoutProfileTitle,mActivity);




        return view;
    }


    EditText salaryField;
    EditText salaryTField;
    EditText noticePeriodField;
    EditText resumeTitleField;
    EditText expField;
    EditText expMonthsField;
    EditText teamSizeField;
    Dialog alertDialog;

    private void addUpdateTotal() {
        try {

            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);
            inputLayoutSalary = (TextInputLayout) view.findViewById(R.id.input_layout_salary);
            inputLayoutSalaryInThousands = (TextInputLayout) view.findViewById(R.id.input_layout_salaryt);
            inputLayoutExperience = (TextInputLayout) view.findViewById(R.id.input_layout_duration);
            inputLayoutProfileTitle= (TextInputLayout) view.findViewById(R.id.input_layout_title);
            inputLayoutExperienceInMonths = (TextInputLayout) view.findViewById(R.id.input_layout_expm);
            inputLayoutTeam = (TextInputLayout) view.findViewById(R.id.input_layout_team);
            inputLayoutNoticePeriod = (TextInputLayout) view.findViewById(R.id.input_layout_period);


            salaryField = (EditText) view
                    .findViewById(R.id.annual_salary_field);
            salaryField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.ANNUAL_SALARYL_REVRSE_MAP,
                    URLConfig.ANNUAL_SALARYL_LIST,
                    totalExModel.getSalaryLIndex(), salaryField));
            salaryField.setOnClickListener(this);
            salaryField.setOnFocusChangeListener(this);

            salaryTField = (EditText) view
                    .findViewById(R.id.annual_salaryt_field);
            salaryTField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.ANNUAL_SALARYT_REVRSE_MAP,
                    URLConfig.ANNUAL_SALARYT_LIST,
                    totalExModel.getSalaryTIndex(), salaryTField));
            salaryTField.setOnClickListener(this);
            salaryTField.setOnFocusChangeListener(this);

            noticePeriodField = (EditText) view
                    .findViewById(R.id.notice_period_field);
            noticePeriodField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.NOTICE_PERIOD_REVRSE_MAP,
                    URLConfig.NOTICE_PERIOD_LIST,
                    totalExModel.getNoticePeriodIndex(), noticePeriodField));

            noticePeriodField.setOnClickListener(this);
            noticePeriodField.setOnFocusChangeListener(this);

            resumeTitleField = (EditText) view
                    .findViewById(R.id.resume_title_field);

            if (totalExModel.getResumeTitle().equals("null")) {
                resumeTitleField.setText("");

            } else {
                resumeTitleField.setText(totalExModel.getResumeTitle());
            }

            expField = (EditText) view.findViewById(R.id.duration_field);
            expField.setTag(ShineCommons.setTagAndValue(URLConfig.EXPERIENCE_REVERSE_MAP,
                    URLConfig.WORK_EXPERIENCE_LIST2,
                    totalExModel.getTotalExperienceIndex(), expField));
            expField.setOnClickListener(this);
            expField.setOnFocusChangeListener(this);

            expMonthsField = (EditText) view.findViewById(R.id.exp_month_field);

            expMonthsField.setTag(ShineCommons.setTagAndValue(
                    URLConfig.EXPERIENCE_MONTHS_REVERSE_MAP,
                    URLConfig.EXPERIENCE_MONTHS_LIST,
                    totalExModel.getMonthExperienceIndex(), expMonthsField));
            expMonthsField.setOnClickListener(this);
            expMonthsField.setOnFocusChangeListener(this);

            teamSizeField = (EditText) view.findViewById(R.id.team_size_field);
            teamSizeField.setTag(ShineCommons.setTagAndValue(URLConfig.TEAM_SIZE_REVRSE_MAP,
                    URLConfig.TEAM_SIZE_LIST,
                    totalExModel.getHandledTeamSizeIndex(), teamSizeField));
            teamSizeField.setOnClickListener(this);
            teamSizeField.setOnFocusChangeListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private void updateTotalDetails() {
        try {

            String salaryTV = salaryField.getText().toString().trim();

            Boolean isEmpty = false;
            if (salaryTV.length() == 0) {
                isEmpty = true;

                inputLayoutSalary.setError("Please enter your salary");

            } else {
                salaryTV = URLConfig.ANNUAL_SALARYL_MAP.get("" + salaryTV);
            }

            String salaryTTV = salaryTField.getText().toString().trim();

            salaryTTV = URLConfig.ANNUAL_SALARYT_MAP.get("" + salaryTTV);

            String noticePeriodTV = noticePeriodField.getText().toString()
                    .trim();

            if (noticePeriodTV.length() == 0) {
                isEmpty = true;
                 inputLayoutNoticePeriod.setError("Please select notice period");
            } else {
                noticePeriodTV = URLConfig.NOTICE_PERIOD_MAP.get(""
                        + noticePeriodTV);
            }

            String expTV = expField.getText().toString().trim();

            if (expTV.length() == 0) {
                isEmpty = true;
                 inputLayoutExperience.setError("Please select experience");
            } else {
                expTV = URLConfig.EXPERIENCE_MAP.get(expTV);
            }

            String expMonthsTV = expMonthsField.getText().toString().trim();

            if (expMonthsTV.length() == 0) {
                isEmpty = true;
                 inputLayoutExperienceInMonths.setError("Please select experience");
            } else {
                expMonthsTV = URLConfig.EXPERIENCE_MONTHS_MAP.get(expMonthsTV);
            }

            String resumeTV = resumeTitleField.getText().toString().trim();

            if (resumeTV.length() == 0) {
                isEmpty = true;
                 inputLayoutProfileTitle.setError("Please enter profile title");
            }
            String teamSizeTV = teamSizeField.getText().toString().trim();

            if (teamSizeTV.length() == 0) {
                isEmpty = true;
                 inputLayoutTeam.setError("Please enter largest team size handled");
            } else {
                teamSizeTV = URLConfig.TEAM_SIZE_MAP.get(teamSizeTV);
            }

            if (isEmpty) {
                DialogUtils.showErrorToast(getResources().getString(
                        R.string.required_fields));
                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);


            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));

            Type listType = new TypeToken<TotalExperience>(){}.getType();

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.MY_TOTAL_PROFESSIONAL_URL.replace(URLConfig.CANDIDATE_ID,userProfileVO.candidate_id), listType);


            HashMap<String, String> experienceMap = new HashMap<>();

            experienceMap.put("experience_in_years", expTV);
            experienceMap.put("experience_in_months", expMonthsTV);
            experienceMap.put("salary_in_lakh", salaryTV);
            experienceMap.put("salary_in_thousand", salaryTTV);
            experienceMap.put("team_size_managed", teamSizeTV);
            experienceMap.put("resume_title", resumeTV);
            experienceMap.put("notice_period", noticePeriodTV);
            downloader.setUsePutMethod(experienceMap);
            downloader.execute("updateTotalDetails");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    @Override
    public void onResume() {
        super.onResume();
        container_box.requestFocus();
        mActivity.setActionBarVisible(false);
        mActivity.setTitle(getString(R.string.title_edit_profile));
        //mActivity.updateLeftDrawer()();
        ManualScreenTracker.manualTracker("Edit-WorkSummary");
    }

    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void OnDataResponse(final Object object, final String tag) {

        mActivity.runOnUiThread(new Runnable() {


            @Override
            public void run() {
                DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(mActivity, "Experience details updated successfully", Toast.LENGTH_SHORT).show();
                ShineCommons.trackShineEvents("Edit", "Experience", mActivity);


                if (object != null) {

                    TotalExperience totalExpModel = (TotalExperience) object;
                    DataCaching.getInstance().cacheData(DataCaching.TOTAL_EXP_SECTION,
                            "" + DataCaching.CACHE_TIME_LIMIT, totalExpModel, mActivity);


                    //Send updated salary to Moengage
                    try {
                        Double salary= Double.parseDouble(totalExpModel.getSalaryLIndex()+"."+totalExpModel.getSalaryTIndex());
                        MoengageTracking.setSalaryAttribute(mActivity,salary);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    } catch (Exception e)
                    {
                      e.printStackTrace();
                    }

                    mActivity.popone();

                    Bundle bundle=new Bundle();

                    bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EXPERIENCE);

                    mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));



                }
            }
        });


    }


    @Override
    public void OnDataResponseError(final String error, String tag) {

        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity,
                             error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            ExperienceDetailsEditFragment.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus)
            showSpinner(v.getId());
    }

    private void showSpinner(int id) {
        try {
            ShineCommons.hideKeyboard(mActivity);

            switch (id) {

                case R.id.team_size_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_team_size), teamSizeField,
                            URLConfig.TEAM_SIZE_LIST, mActivity);
                    break;
                case R.id.notice_period_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_notice_period),
                            noticePeriodField, URLConfig.NOTICE_PERIOD_LIST,
                            mActivity);
                    break;

                case R.id.annual_salary_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_annual_salary), salaryField,
                            URLConfig.ANNUAL_SALARYL_LIST, mActivity);
                    break;
                case R.id.annual_salaryt_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_annual_salary_inthousands),
                            salaryTField, URLConfig.ANNUAL_SALARYT_LIST, mActivity);
                    break;

                case R.id.exp_month_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_months), expMonthsField,
                            URLConfig.EXPERIENCE_MONTHS_LIST, mActivity);
                    break;

                case R.id.duration_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp_years), expField,
                            URLConfig.WORK_EXPERIENCE_LIST2, mActivity);
                    break;


                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.update_btn:
                updateTotalDetails();



                break;
            case R.id.cancel_btn:

                mActivity.popone();
                Bundle bundle=new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EXPERIENCE);

                mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));
                break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
            default:
                showSpinner(v.getId());
                break;
        }
    }
}
