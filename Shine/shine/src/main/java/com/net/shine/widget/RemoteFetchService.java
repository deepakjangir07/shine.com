package com.net.shine.widget;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by manishpoddar on 09/12/16.
 */

public class RemoteFetchService extends Service {

    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;


    public static ArrayList<SimpleSearchModel.Result> listItemList=new ArrayList<>();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (intent!=null&&intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID)) {
                appWidgetId = intent.getIntExtra(
                        AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);

                Log.d("Widget","onStart "+appWidgetId);
                fetchDataFromWeb();
            }
            return super.onStartCommand(intent, flags, startId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  super.onStartCommand(intent, flags, startId);

    }


    private void fetchDataFromWeb() {


        UserStatusModel userStatusModel= ShineSharedPreferences.getUserInfo(getApplicationContext());

        if(userStatusModel!=null&& !TextUtils.isEmpty(userStatusModel.candidate_id)) {


            VolleyNetworkRequest req;
            Type type = new TypeToken<SimpleSearchModel>() {
            }.getType();

            String URL = URLConfig.MATCH_JOB_URL.replace(URLConfig.CANDIDATE_ID, userStatusModel.candidate_id) + "&perpage="+URLConfig.PER_PAGE + "&sort=null"
                    + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID";
            req = new VolleyNetworkRequest(RemoteFetchService.this, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    SimpleSearchModel model = (SimpleSearchModel) object;
                    ShineSharedPreferences.saveWidgetList(RemoteFetchService.this,model);

                    processResult((SimpleSearchModel) object);
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                }

            },
                    URL,
                    type);

            Log.d("DEBUG::ONDTRSP:", "widget VolleyExecuted");
            req.execute("widget");
        }
        else
        {
            Log.d("DEBUG","Logout mode");
            populateWidget();
        }

    }


    private void processResult(SimpleSearchModel result) {
        Log.i("Resutl-==",result.results.toString());


        try {


            listItemList=new ArrayList<>();

            for (SimpleSearchModel.Result res : result.results) {
                int len = res.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += res.job_loc_list.get(j);
                    } else {
                        loc += res.job_loc_list.get(j) + " / ";
                    }
                }
                res.job_loc_str = loc;
            }


            listItemList.addAll(result.results);

            Log.d("Widget ","list size in create "+listItemList.size());



        } catch (Exception e) {
            e.printStackTrace();
        }
        populateWidget();
    }


    private void populateWidget() {

        Intent widgetUpdateIntent = new Intent();

        Bundle b=new Bundle();

        b.putSerializable("list",listItemList);
        widgetUpdateIntent.setAction(WidgetProvider.DATA_FETCHED);
        widgetUpdateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                appWidgetId);

        Log.d("Widget ","list size in send "+listItemList.size());

        widgetUpdateIntent.putExtras(b);

        sendBroadcast(widgetUpdateIntent);

        this.stopSelf();
    }
}

