package com.net.shine.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.JobListRecyclerAdapterForAds;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.components.CertificationDetailComp;
import com.net.shine.fragments.components.DesiredJobComp;
import com.net.shine.fragments.components.EducationDetailComp;
import com.net.shine.fragments.components.EmploymentDetailComp;
import com.net.shine.fragments.components.ExperienceDetailComp;
import com.net.shine.fragments.components.PersonalDetailComp;
import com.net.shine.fragments.components.ResumeDetailComp;
import com.net.shine.fragments.components.SkillDetailComp;
import com.net.shine.models.CertificationResult;
import com.net.shine.models.DesiredJobDetails;
import com.net.shine.models.EducationDetailModel;
import com.net.shine.models.EmploymentDetailModel;
import com.net.shine.models.MyProfile;
import com.net.shine.models.PersonalDetail;
import com.net.shine.models.ResumeDetails;
import com.net.shine.models.SkillResult;
import com.net.shine.models.TotalExperience;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by manishpoddar on 22/08/16.
 */
public class MyProfileFrg extends BaseFragment implements VolleyRequestCompleteListener, SwipeRefreshLayout.OnRefreshListener {


    private PersonalDetailComp pdComp;
    private EducationDetailComp edComp;
    private SkillDetailComp sdComp;
    private CertificationDetailComp cdComp;
    private DesiredJobComp djComp;
    private ResumeDetailComp rdComp;
    private View errorLayout;
    private View view;
    private ExperienceDetailComp exdComp;
    private EmploymentDetailComp emdComp;
    private LinearLayout personalLayout, expLayout, certiLayout, desiredLayout, skillLayout, eduLayout, empLayout, resumeLayout;
    private SwipeRefreshLayout swipeContainer;
    private ScrollView scrollView;
    private int scrollTo;

    public static final int SCROLL_TO_EXPERIENCE = 1;
    public static final int SCROLL_TO_DESIRED = 2;
    public static final int SCROLL_TO_CERTIFICATION = 3;
    public static final int SCROLL_TO_SKILL = 4;
    public static final int SCROLL_TO_EDUCATION = 5;
    public static final int SCROLL_TO_EMPLOYMENT = 6;
    public static final int SCROLL_TO_RESUME = 7;

    public static final int SCROLL_TO_PERSONAL = 0;


    public static final String SCROLL_TO = "scroll_to";


    private boolean firstInstance = false;

    public static MyProfileFrg newInstance(Bundle args) {
        MyProfileFrg frg = new MyProfileFrg();
        frg.setArguments(args);
        return frg;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_profile, container, false);
        mActivity.showNavBar();
        mActivity.setActionBarVisible(false);
        mActivity.hideBackButton();
        mActivity.showSelectedNavButton(mActivity.PROFILE_OPTION);
        mActivity.setTitle("Profile");

        if (JobListRecyclerAdapterForAds.actionMode != null) {
            JobListRecyclerAdapterForAds.actionMode.finish();

        }

        if (JobsListRecyclerAdapter.actionMode != null) {
            JobsListRecyclerAdapter.actionMode.finish();
        }
        scrollView = (ScrollView) view.findViewById(R.id.scroll);

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        ShineCommons.setSwipeRefeshColor(swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        errorLayout = view.findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        personalLayout = (LinearLayout) view.findViewById(R.id.my_profile_comp);
        expLayout = (LinearLayout) view.findViewById(R.id.experience_profile_comp);
        skillLayout = (LinearLayout) view.findViewById(R.id.skill_profile_comp);
        desiredLayout = (LinearLayout) view.findViewById(R.id.desired_job_details_comp);
        certiLayout = (LinearLayout) view.findViewById(R.id.certi_profile_comp);
        eduLayout = (LinearLayout) view.findViewById(R.id.education_profile_comp);
        empLayout = (LinearLayout) view.findViewById(R.id.employment_profile_comp);
        resumeLayout = (LinearLayout) view.findViewById(R.id.resume_detail_comp);

        if (getArguments() != null) {

            if (getArguments().get(SCROLL_TO) != null) {
                int scrollTo = getArguments().getInt(SCROLL_TO);
                focusOnView(scrollTo);
            } else
                focusOnView(SCROLL_TO_PERSONAL);
        }


        ShineCommons.hideKeyboard(mActivity);
        try {

            Log.d("DEBUG", "onCreate Called");

            Object objectPersonal = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.PERSONAL_SECTION);
            Object objectDesired = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.DESIRED_DETAILS_SECTION);
            Object objectEducation = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.EDUCATION_LIST_SECTION);
            Object objectSkills = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.SKILLS_LIST_SECTION);
            Object objectCerti = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.CERTIFICATION_LIST_SECTION);
            Object objectResumes = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.RESUME_LIST_SECTION);
            Object objectTotalExp = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.TOTAL_EXP_SECTION);
            Object objectTotalJobs = DataCaching.getInstance().getObject(mActivity,
                    DataCaching.JOB_LIST_SECTION);

            if (objectPersonal != null && objectDesired != null && objectEducation != null && objectSkills != null
                    && objectCerti != null && objectResumes != null && objectTotalExp != null && objectTotalJobs != null) {
                pdComp = new PersonalDetailComp(mActivity,
                        view.findViewById(R.id.my_profile_comp), this);
                pdComp.showDetails((PersonalDetail) objectPersonal);

                edComp = new EducationDetailComp(mActivity, view.findViewById(R.id.education_profile_comp));
                edComp.showEducationDetails((List<EducationDetailModel>) objectEducation);


                rdComp = new ResumeDetailComp(mActivity,
                        view.findViewById(R.id.resume_detail_comp), this);
                rdComp.showDetails((List<ResumeDetails>) objectResumes);

                sdComp = new SkillDetailComp(mActivity, view.findViewById(R.id.skill_profile_comp));
                sdComp.showDetails((List<SkillResult>) objectSkills);

                cdComp = new CertificationDetailComp(mActivity, view.findViewById(R.id.certi_profile_comp));
                cdComp.showDetails((List<CertificationResult>) objectCerti);


                djComp = new DesiredJobComp(mActivity, view.findViewById(R.id.desired_job_details_comp));
                djComp.showDetails((DesiredJobDetails) objectDesired);

                exdComp = new ExperienceDetailComp(mActivity, view.findViewById(R.id.experience_profile_comp),
                        view.findViewById(R.id.my_profile_comp));
                exdComp.showTotalExpDetails((TotalExperience) objectTotalExp);

                emdComp = new EmploymentDetailComp(mActivity, view.findViewById(R.id.employment_profile_comp),
                        view.findViewById(R.id.my_profile_comp));
                emdComp.showJobsDetails((List<EmploymentDetailModel>) objectTotalJobs);
            } else {
                getProfileModel(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void getProfileModel(boolean isRefresh) {

        if (!isRefresh)
            view.findViewById(R.id.loading_cmp_myprofile_main).setVisibility(View.VISIBLE);

        UserStatusModel profile = ShineSharedPreferences.getUserInfo(mActivity);

        Type listType = new TypeToken<MyProfile>() {
        }.getType();

        if (profile != null) {
            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this, URLConfig.PROFILE_DETAIL_URL.replace(URLConfig.CANDIDATE_ID, profile.candidate_id), listType);
            request.execute("CompleteProfileDetails");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (rdComp != null && requestCode == ResumeDetailComp.REQUEST_STORAGE_PERMISSIONS) {
            rdComp.onRequestPermissionsResult(requestCode, permissions, grantResults);
        } else if (pdComp != null && requestCode == PersonalDetailComp.REQUEST_SMS_PERMISSIONS) {
            pdComp.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.title_my_profile));
        mActivity.setActionBarVisible(false);
        ShineCommons.hideKeyboard(mActivity);
        mActivity.showNavBar();
        mActivity.showSelectedNavButton(mActivity.PROFILE_OPTION);
        mActivity.hideBackButton();
        if (JobListRecyclerAdapterForAds.actionMode != null) {
            JobListRecyclerAdapterForAds.actionMode.finish();

        }

        if (JobsListRecyclerAdapter.actionMode != null) {
            JobsListRecyclerAdapter.actionMode.finish();
        }


        if (getUserVisibleHint()) {
            ManualScreenTracker.manualTracker("MyProfile");
            setUserVisibleHint(false);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor outState = mActivity.getSharedPreferences(
                "frag", Context.MODE_APPEND).edit();
        outState.putString("fraginfo", "");

        outState.apply();
    }

    @Override
    public void OnDataResponse(final Object object, String tag) {


        try {

            swipeContainer.setRefreshing(false);
            if (object != null) {


                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            System.out.println("--object" + object + "\n--object--" + object.toString());

                            view.findViewById(R.id.loading_cmp_myprofile_main).setVisibility(View.GONE);


                            MyProfile profileModel = (MyProfile) object;

                            System.out.println("--profile model--" + profileModel);


                            pdComp = new PersonalDetailComp(mActivity,
                                    view.findViewById(R.id.my_profile_comp), MyProfileFrg.this);
                            pdComp.showDetails(profileModel.getPersonalDetails().get(0));
                            DataCaching.getInstance().cacheData(DataCaching.PERSONAL_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getPersonalDetails().get(0), mActivity);

                            edComp = new EducationDetailComp(mActivity, view.findViewById(R.id.education_profile_comp));
                            edComp.showEducationDetails(profileModel.getEducations());
                            DataCaching.getInstance().cacheData(DataCaching.EDUCATION_LIST_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getEducations(), mActivity);

                            rdComp = new ResumeDetailComp(mActivity,
                                    view.findViewById(R.id.resume_detail_comp), MyProfileFrg.this);
                            rdComp.showDetails(profileModel.getResumes());
                            DataCaching.getInstance().cacheData(DataCaching.RESUME_LIST_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getResumes(), mActivity);
                            sdComp = new SkillDetailComp(mActivity, view.findViewById(R.id.skill_profile_comp));
                            sdComp.showDetails(profileModel.getSkills());

                            Log.d("SKILL_COUNT ", "No of skills " + profileModel.getSkills().size());

                            MoengageTracking.setSkillCount(mActivity, profileModel.getSkills().size());
                            MoengageTracking.setProfileAttributes(mActivity);

                            DataCaching.getInstance().cacheData(DataCaching.SKILLS_LIST_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getSkills(), mActivity);

                            cdComp = new CertificationDetailComp(mActivity, view.findViewById(R.id.certi_profile_comp));
                            cdComp.showDetails(profileModel.getCertificationModel());
                            DataCaching.getInstance().cacheData(DataCaching.CERTIFICATION_LIST_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getCertificationModel(), mActivity);

                            djComp = new DesiredJobComp(mActivity, view.findViewById(R.id.desired_job_details_comp));
                            djComp.showDetails(profileModel.getDesiredJobs().get(0));
                            DataCaching.getInstance().cacheData(DataCaching.DESIRED_DETAILS_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getDesiredJobs().get(0), mActivity);

                            exdComp = new ExperienceDetailComp(mActivity, view.findViewById(R.id.experience_profile_comp),
                                    view.findViewById(R.id.my_profile_comp));
                            exdComp.showTotalExpDetails(profileModel.getWorkex().get(0));


                            try {
                                Double salary = Double.parseDouble(profileModel.getWorkex().get(0).getSalaryLIndex() + "." + profileModel.getWorkex().get(0).getSalaryTIndex());
                                MoengageTracking.setSalaryAttribute(mActivity, salary);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            DataCaching.getInstance().cacheData(DataCaching.TOTAL_EXP_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getWorkex().get(0), mActivity);


                            emdComp = new EmploymentDetailComp(mActivity, view.findViewById(R.id.employment_profile_comp),
                                    view.findViewById(R.id.my_profile_comp));
                            emdComp.showJobsDetails(profileModel.getJobs());
                            DataCaching.getInstance().cacheData(DataCaching.JOB_LIST_SECTION,
                                    "" + DataCaching.CACHE_TIME_LIMIT, profileModel.getJobs(), mActivity);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        swipeContainer.setRefreshing(false);
        errorLayout.setVisibility(View.VISIBLE);
        view.findViewById(R.id.loading_cmp_myprofile_main).setVisibility(View.GONE);
        DialogUtils.showErrorActionableMessage(mActivity, errorLayout,
                mActivity.getString(R.string.technical_error),
                mActivity.getString(R.string.technical_error2),
                "",
                DialogUtils.ERR_TECHNICAL, null);
    }


    @Override
    public void onRefresh() {
        errorLayout.setVisibility(View.GONE);
        getProfileModel(true);
    }


    private final void focusOnView(final int scrollTo) {
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {


                if (scrollTo == SCROLL_TO_DESIRED)
                    scrollView.scrollTo(0, desiredLayout.getTop());
                else if (scrollTo == SCROLL_TO_EXPERIENCE)
                    scrollView.scrollTo(0, expLayout.getTop());
                else if (scrollTo == SCROLL_TO_CERTIFICATION)
                    scrollView.scrollTo(0, certiLayout.getTop());
                else if (scrollTo == SCROLL_TO_SKILL)
                    scrollView.scrollTo(0, skillLayout.getTop());
                else if (scrollTo == SCROLL_TO_EDUCATION)
                    scrollView.scrollTo(0, eduLayout.getTop());
                else if (scrollTo == SCROLL_TO_EMPLOYMENT)
                    scrollView.scrollTo(0, empLayout.getTop());
                else if (scrollTo == SCROLL_TO_RESUME)
                    scrollView.scrollTo(0, resumeLayout.getTop());
                else if (scrollTo == SCROLL_TO_PERSONAL)
                    scrollView.scrollTo(0, personalLayout.getTop());


            }
        }, 10);
    }


}
