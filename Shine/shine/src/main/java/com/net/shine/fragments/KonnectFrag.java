package com.net.shine.fragments;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.AccountPicker;
import com.google.gson.Gson;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.interfaces.GmailDataSendListener;
import com.net.shine.interfaces.LinkedinDataSendListener;
import com.net.shine.models.AuthPreferencesModel;
import com.net.shine.models.GmailProfileModel;
import com.net.shine.models.MyProfile;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.ContactRetrieveService;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.SendtoServerGmail;
import com.net.shine.utils.SendtoServerLinkedin;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ujjawal-work on 12/08/16.
 */

public class KonnectFrag extends BaseFragment implements View.OnClickListener, GmailDataSendListener, LinkedinDataSendListener {

    public static String Location = "";
    private CheckBox allSynced;
    private TextView text_sync_now;
    private View view;
    private View other_layout;
    private UserStatusModel userProfileVO;
    private int mbacknum = 0;
    // 4 means commit sepukku ( we came from res-sync btn in DiscoverFrg)
    // 1 means go to DiscoverFrg   (we came from "Discover" in Drawer)
    // 2 means commit sepukku  (we come from network job screen)l
    // 2 means commit sepukku  (we come from network job screen)
    // 3 means commit sepukku ( we came here after applying to a Job)
    // 0 is default value, should never occur, we commit sepukku.
    private TextView headline;
    private int gmail_total = 0;
    private TextView textview_sync_gmail, last_sync_linkedin, last_sync_gmail, textview_sync_phonebook, last_sync_phonebook;
    private LinearLayout update_profile_layout;
    private View sync_finish_gmail, sync_finish_linkedin, sync_finish_phonebook;
    private TextView currnt_comp, higher_edu;
    private boolean cancel_sync_dialog = false;
    private WebView web;
    private Dialog auth_dialog;
    private TextView skipText;

    private List<GmailProfileModel> contact_list;
    private AuthPreferencesModel AuthPreferencesModel;
    private AccountManager am;
    private static final String SCOPE = "https://www.google.com/m8/feeds/";

    private static final int AUTHORIZATION_CODE = 1993;
    private static final int ACCOUNT_CODE = 1601;

    private static final String OAUTH_ACCESS_TOKEN = "access_token";
    private static final String QUESTION_MARK = "?";
    private static final String EQUALS = "=";
    private static final String END_POINT = "https://www.google.com/m8/feeds/contacts/default/full/";
    private String companyName = "this company";

    private ProgressBar LinkedIncustomLoader;
    private ProgressBar GmailcustomLoader;
    private ProgressBar PhonebookcustomLoader;


    //Local Broadcast Receiver to get Contact sync events

    private BroadcastReceiver syncReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.d("DEBUG", "Receiver Fired");


            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView tv = (TextView) view.findViewById(R.id.text_sync_now_phonebook);
                    TextView lTv = (TextView) view.findViewById(R.id.last_sync_phonebook);
                    switch (intent.getAction()) {
                        case "CONTACT_SYNC_SUCCESS":

                            Log.d("DEBUG", "Sync success");
                            MyApplication.isPhonebookSyncing = false;
                            if (PhonebookcustomLoader != null) {
                                PhonebookcustomLoader.setVisibility(View.GONE);
                            }
                            if (tv != null) {
                                tv.setVisibility(View.GONE);
                            }
                            if (lTv != null) {
                                lTv.setVisibility(View.VISIBLE);
                            }
                            if (view != null) {
                                view.findViewById(R.id.sync_complete_phonebook)
                                        .setVisibility(View.VISIBLE);
                            }
                            if (lTv != null) {
                                lTv.setText("Last synced now");
                            }

                            break;
                        case "CONTACT_SYNC_FAILED":
                            Log.d("DEBUG", "Sync failed");
                            MyApplication.isPhonebookSyncing = false;
                            PhonebookcustomLoader.setVisibility(View.GONE);
                            String error = intent.getStringExtra("ERROR_MSG");
                            if (error == null || error.isEmpty()) {
                                error = URLConfig.PARSING_ERROR_MSG;
                            }
                            Toast.makeText(MyApplication.getInstance().getApplicationContext(),
                                    error, Toast.LENGTH_LONG).show();

                            if (tv != null) {
                                tv.setVisibility(View.VISIBLE);
                                tv.setText("CONNECT");
                            }
                            if (lTv != null) {
                                lTv.setVisibility(View.GONE);
                            }
                            if (view != null) {
                                view.findViewById(R.id.sync_complete_phonebook)
                                        .setVisibility(View.GONE);
                            }
                            break;
                        case "CONTACT_SYNC_STARTED":
                            Log.d("DEBUG", "Sync started");
                            MyApplication.isPhonebookSyncing = true;
                            PhonebookcustomLoader.setVisibility(View.VISIBLE);
                            if (tv != null) {
                                tv.setVisibility(View.VISIBLE);
                                tv.setText("Syncing...");
                            }
                            if (lTv != null) {
                                lTv.setVisibility(View.GONE);
                            }
                            if (view != null) {
                                view.findViewById(R.id.sync_complete_phonebook)
                                        .setVisibility(View.GONE);
                            }
                            break;

                    }
                }
            });


        }
    };


    public static KonnectFrag newInstance(Bundle args){
        mInstance = new KonnectFrag();
        mInstance.setArguments(args);
        return mInstance;
    }

    public static  KonnectFrag newInstance(int backnum){
        Bundle args = new Bundle();
        args.putInt("backnum",backnum);
        mInstance = new KonnectFrag();
        mInstance.setArguments(args);
        return mInstance;
    }



    public static KonnectFrag newInstance(int backnum, String compName)
    {
        Bundle args = new Bundle();
        args.putInt("backnum", backnum);
        args.putString("compName", compName);
        mInstance = new KonnectFrag();
        mInstance.setArguments(args);
        return mInstance;
        }


    private static KonnectFrag mInstance;

    public static KonnectFrag getInstance() {
        return mInstance;
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        try {
            mActivity.setActionBarVisible(true);
            mActivity.showNavBar();
            mActivity.showBackButton();
            mActivity.showSelectedNavButton(BaseActivity.MORE_OPTION);
            setHasOptionsMenu(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("CONTACT_SYNC_SUCCESS");
        filter.addAction("CONTACT_SYNC_FAILED");
        filter.addAction("CONTACT_SYNC_STARTED");
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(syncReceiver, filter);

        mbacknum = getArguments() != null ? getArguments().getInt("num") : 1;
        if (getArguments() != null && getArguments().getString("name") != null)
            companyName = getArguments().getString("name");

        System.out.println("-- on create-mback num" + mbacknum);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.konnect_frag, container, false);

        LinkedIncustomLoader = (ProgressBar) view.findViewById(R.id.linkedin_syncBar);

        LinkedIncustomLoader.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFF000000,
                Color.parseColor("#72bdf9")));
        LinkedIncustomLoader.setIndeterminate(true);

        GmailcustomLoader = (ProgressBar) view.findViewById(R.id.gmail_syncBar);
        GmailcustomLoader.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFF000000,
                Color.parseColor("#72bdf9")));
        GmailcustomLoader.setIndeterminate(true);

        PhonebookcustomLoader = (ProgressBar) view.findViewById(R.id.phonebook_syncBar);
        PhonebookcustomLoader.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFF000000,
                Color.parseColor("#72bdf9")));
        PhonebookcustomLoader.setIndeterminate(true);


        View linkedin = view.findViewById(R.id.linkedin_btn);
        View google = view.findViewById(R.id.google_btn);
        View phonebook = view.findViewById(R.id.phonebook_btn);

        if (ShineCommons.isAndroidM()) {
            phonebook.setVisibility(View.VISIBLE);
        } else {
            phonebook.setVisibility(View.GONE);
        }


        headline = (TextView) view.findViewById(R.id.headline);
        skipText = (TextView) view.findViewById(R.id.skip_text);
        allSynced = (CheckBox) view.findViewById(R.id.apply_synced_checkbox);

        if (mbacknum == 2) {
            skipText.setText("Continue to my matched jobs");
        } else if (mbacknum == 3) {
            skipText.setText(" Skip ");

        } else if (mbacknum == 5) //mbacknum = 5 from konnect from requestReferralFragment
        {
            skipText.setText(" Cancel ");
        } else {
            skipText.setText("Continue to my connections ");
        }

        text_sync_now = (TextView) view.findViewById(R.id.text_sync_now);

        linkedin.setOnClickListener(this);
        google.setOnClickListener(this);
        phonebook.setOnClickListener(this);

        if (mbacknum == 3) {
            headline.setText("Increase your chances of getting this job by 3x");
            view.findViewById(R.id.text_reffral).setVisibility(View.GONE);
        } else {
            headline.setText("Get referred and increase your chances of getting hired by 3x");
            view.findViewById(R.id.text_reffral).setVisibility(View.VISIBLE);
        }
        update_profile_layout = (LinearLayout) view
                .findViewById(R.id.update_layout);
        TextView update_profile_btn = (TextView) view
                .findViewById(R.id.update_profile_btn);
        update_profile_btn.setOnClickListener(this);
        ImageView close_update = (ImageView) view.findViewById(R.id.close_update);
        close_update.setOnClickListener(this);
        currnt_comp = (TextView) view.findViewById(R.id.currnt_comp);
        higher_edu = (TextView) view.findViewById(R.id.higher_edu);
        other_layout = view.findViewById(R.id.layout_other_than_loading);
        other_layout.setVisibility(View.VISIBLE);
        update_profile_layout.setVisibility(View.GONE);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);





        View skip = view.findViewById(R.id.skip_btn);
        skip.setOnClickListener(this);
        show_result();

        if (!ShineSharedPreferences.getPrefUpdateProfile(mActivity)) {
            showHighEduDetails();
            showJobsDetails();
        }
        return view;
    }



    long lastsynctimelinkedin;

    public void init() {

        textview_sync_gmail = (TextView) view.findViewById(R.id.text_sync_gmail);
        textview_sync_phonebook = (TextView) view.findViewById(R.id.text_sync_now_phonebook);
        last_sync_phonebook = (TextView) view.findViewById(R.id.last_sync_phonebook);
        last_sync_gmail = (TextView) view.findViewById(R.id.last_sync_gmail);
        last_sync_linkedin = (TextView) view
                .findViewById(R.id.last_sync_linkedin);

        sync_finish_gmail = view.findViewById(R.id.sync_complete_gmail);
        sync_finish_phonebook = view.findViewById(R.id.sync_complete_phonebook);
        sync_finish_linkedin = view
                .findViewById(R.id.sync_complete_linkedin);

        long lastsynctimegmail = ShineSharedPreferences
                .getSyncTimeGmail(mActivity);
        lastsynctimelinkedin = ShineSharedPreferences
                .getSyncTimeLinkedIn(mActivity);
        long lastSyncTimePhonebook = ShineSharedPreferences.getSyncTimePhonebook(mActivity);

        Log.e("Debug ", "last sync time " + lastsynctimegmail);
        Log.d("Debug", "last sync time" + lastSyncTimePhonebook);
        Log.e("Debug ", "last sync time linked " + lastsynctimelinkedin);
        if (ShineSharedPreferences.getLinkedinImported(mActivity)) {
            sync_finish_linkedin.setVisibility(View.VISIBLE);
            text_sync_now.setVisibility(View.GONE);
        }
        if (ShineSharedPreferences.getGoogleImported(mActivity)) {
            sync_finish_gmail.setVisibility(View.VISIBLE);
            textview_sync_gmail.setVisibility(View.GONE);
        }
        if (ShineSharedPreferences.getPhonebookImported(mActivity)) {
            sync_finish_phonebook.setVisibility(View.VISIBLE);
            textview_sync_phonebook.setVisibility(View.GONE);
        }

        if (lastsynctimelinkedin > 0) {
            last_sync_linkedin.setVisibility(View.VISIBLE);
            last_sync_linkedin.setText("Last synced on "
                    + getDate(lastsynctimelinkedin, "dd/MM/yyyy"));
        }

        if (lastsynctimegmail > 0) {
            last_sync_gmail.setVisibility(View.VISIBLE);
            last_sync_gmail.setText("Last synced on "
                    + getDate(lastsynctimegmail, "dd/MM/yyyy"));
        }

        if (lastSyncTimePhonebook > 0) {
            last_sync_phonebook.setVisibility(View.VISIBLE);
            last_sync_phonebook.setText("Last synced on "
                    + getDate(lastSyncTimePhonebook, "dd/MM/yyyy"));
        }

    }


    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.phonebook_btn:
                if (MyApplication.isPhonebookSyncing) {
                    Toast.makeText(mActivity, "Phonebook Connection Syncing already in progress.", Toast.LENGTH_SHORT).show();
                    return;
                }
                PhonebookConnect();
                break;

            case R.id.linkedin_btn:

                if (MyApplication. isLinkedInSyncing) {
                    Toast.makeText(mActivity, "LinkedIn connection syncing already in progress."
                            , Toast.LENGTH_SHORT).show();
                    return;
                }
                String accessToken = ShineSharedPreferences.getLinkedinAccessToken(mActivity);
                Long expiry = ShineSharedPreferences.getLinkedinAccessExpiry(mActivity);
                if (accessToken.isEmpty() || expiry < System.currentTimeMillis()) {
                    LinkedinConnect();
                } else {
                    new GetConnectionRequestAsyncTask().execute(accessToken);
                }

                break;
            case R.id.google_btn:

                if (MyApplication.isGmailSyncing) {
                    Toast.makeText(mActivity, "Gmail connection syncing already in progress.", Toast.LENGTH_SHORT).show();
                    return;
                }

                GmailConnect();

                break;

            case R.id.skip_btn:


                if (allSynced != null) {
                    Log.d("DEBUG::KONNECT", allSynced.isChecked() + " allSynced checked");
                    ShineSharedPreferences.setAllSyncedByUser(mActivity,
                            allSynced.isChecked());
                }
                if(skipText.getText().toString().trim().equals("Continue to my matched jobs")){
                    BaseFragment fragment = new JobsTabFrg();
                    mActivity.singleInstanceReplaceFragment(fragment);

                }
                else if(skipText.getText().toString().trim().equals("Continue to my connections")) {
                    BaseFragment fragment = new DiscoverFragment();
                    mActivity.singleInstanceReplaceFragment(fragment);
                    Log.d("skip text", skipText.getText().toString());
                }
                else if(skipText.getText().equals("Skip")||skipText.getText().equals("cancel")){
                    mActivity.onBackPressed();
                }

        ShineCommons.trackShineEvents("Skip", "ImportContacts", mActivity);
                break;
            case R.id.close_update:
                if (update_profile_layout.getVisibility() == View.VISIBLE) {
                    update_profile_layout.setVisibility(View.GONE);
                }
                ShineSharedPreferences.setPrefUpdateProfile(mActivity, true);
                break;

            case R.id.update_profile_btn:

                Bundle bundle=new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_PERSONAL);

                BaseFragment fragment = new MyProfileFrg().newInstance(bundle);
                mActivity.replaceFragment(fragment);


                break;

            case R.id.close_btn:
                dismissDialog();
                cancel_sync_dialog = false;
                break;
            case R.id.continue_button:
                dismissDialog();
                cancel_sync_dialog = false;
                break;

            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            mActivity.setActionBarVisible(true);
            cancel_sync_dialog = false;
            if (mbacknum != 3) {
                mActivity.setTitle(getString(R.string.title_connect));
                if (KonnectFrag.this.isVisible()) {
                    ManualScreenTracker.manualTracker("Konnect");
                }
            } else {
                mActivity.setTitle(ThanksAppliedFrg.THANKS_FRG_TITLE);
                if (KonnectFrag.this.isVisible()) {
                    ManualScreenTracker.manualTracker("PostApplyKonnect");
                }
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(syncReceiver);
        super.onDestroy();
        SharedPreferences.Editor outState = mActivity.getSharedPreferences(
                "frag", Context.MODE_APPEND).edit();
        outState.putString("fraginfo", "");

        outState.apply();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (mActivity != null) {

                if (resultCode == Activity.RESULT_OK) {

                    if (requestCode == AUTHORIZATION_CODE) {
                        requestToken();
                    } else if (requestCode == ACCOUNT_CODE) {
                        String accountName = data
                                .getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                        AuthPreferencesModel.setUser(accountName);
                        invalidateToken();
                        requestToken();
                    }

                } else {
                    Toast.makeText(
                            mActivity,
                            "Gmail sync failed. Please try again in some time.",
                            Toast.LENGTH_SHORT).show();


                }
            }
        } catch (Exception e) { // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void LinkedinConnect() {
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        auth_dialog = new Dialog(mActivity,
                android.R.style.Theme_Translucent_NoTitleBar);
        auth_dialog.setContentView(R.layout.auth_dialog);
        web = (WebView) auth_dialog.findViewById(R.id.webv);
        auth_dialog.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        auth_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        String authUrl = LinkedinConstants.getAuthorizationUrl();
        web.loadUrl(authUrl);
        web.getSettings().setJavaScriptEnabled(true);

        web.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                auth_dialog.findViewById(R.id.progress_bar).setVisibility(
                        View.GONE);
                web.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String authorizationUrl) {

                if (authorizationUrl.startsWith(LinkedinConstants.REDIRECT_URI)) {
                    Uri uri = Uri.parse(authorizationUrl);
                    Log.d("LINKED IN AUTH RESPONSE", authorizationUrl);

                    String error = uri.getQueryParameter("error");
                    if (error != null) {
                        Toast.makeText(
                                mActivity,
                                "LinkedIn sync pending. Continue importing from other sources or skip to browse your network.",
                                Toast.LENGTH_SHORT).show();
                        auth_dialog.dismiss();
                        return true;
                    }

                    String stateToken = uri
                            .getQueryParameter(LinkedinConstants.STATE_PARAM);
                    if (stateToken == null
                            || !stateToken.equals(LinkedinConstants.STATE)) {
                        Toast.makeText(mActivity,
                                "Sorry..state token doesn't match",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    String authorizationToken = uri
                            .getQueryParameter(LinkedinConstants.RESPONSE_TYPE_VALUE);
                    if (authorizationToken == null) {
                        Log.i("DEBUG", "The user doesn't allow authorization.");
                        return true;
                    }
                    Log.i("DEBUG Friends", "Auth token received: "
                            + authorizationToken);
                    String accessTokenUrl = LinkedinConstants
                            .getAccessTokenUrl(authorizationToken);

                    new PostRequestAsyncTask().execute(accessTokenUrl);

                } else {

                    Log.i("Authorize", "Redirecting to: " + authorizationUrl);
                    web.loadUrl(authorizationUrl);
                }
                return true;
            }

        });
        auth_dialog.setTitle("LinkedIn Authentication");
        if (!mActivity.isFinishing())
            auth_dialog.show();
    }

    private void holdCaching() {
        try {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    URLConfig.isRecentSync = false;
                }
            }, (5 * 60 * 1000));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onInstanceSuccessGmail() {
        try {

            ((TextView)view.findViewById(R.id.text_sync_gmail)).setText("CONNECT");
            dismissDialog();
            if (!cancel_sync_dialog) {

                //  sync_gmail.setVisibility(View.GONE);

                (view.findViewById(R.id.text_sync_gmail)).setVisibility(View.GONE);

                (view.findViewById(R.id.last_sync_gmail)).setVisibility(View.VISIBLE);
                //textview_sync_gmail.setVisibility(View.GONE);
                //last_sync_gmail.setVisibility(View.VISIBLE);
                ShineSharedPreferences
                        .updateSynctimeGmail(
                                mActivity,
                                System.currentTimeMillis());
                view.findViewById(R.id.sync_complete_gmail).setVisibility(
                        View.VISIBLE);
                ((TextView)view.findViewById(R.id.last_sync_gmail)).setText("Last synced now");

                Toast.makeText(mActivity, " Your gmail friends are now ready to refer you for " +
                        "jobs in their companies, start applying now.", Toast.LENGTH_LONG).show();

                DialogUtils.showNotification(mActivity,"Your gmail friends are now ready to refer you for jobs in" +
                        " their companies, start applying now");

                URLConfig.isRecentSync = true;
                holdCaching();
                ShineSharedPreferences.saveGoogleImported(mActivity, true);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Called when Any Gmail and linkedin data send to server on Succss or failed
    @Override
    public void onSuccessGmail() {
        MyApplication.isGmailSyncing = false;
        if (getInstance() != null) {
            getInstance().onInstanceSuccessGmail();
        } else {
            onInstanceSuccessGmail();
        }

        ShineCommons.trackShineEvents("Import Contact Success","Gmail",mActivity);

    }


    public void onInstanceFailedGmail() {
        try {
            ((TextView) view.findViewById(R.id.text_sync_gmail)).setText("CONNECT");
            dismissDialog();
            Toast.makeText(
                    mActivity,
                    "Gmail sync failed. Please try again in some time.",
                    Toast.LENGTH_SHORT).show();

            ShineCommons.trackShineEvents("Import Contact Failed","Gmail",mActivity);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onFailedGmail() {
        MyApplication.isGmailSyncing = false;
        if (getInstance() != null) {
            getInstance().onInstanceFailedGmail();
        } else {
            onInstanceFailedGmail();
        }
    }


    public void onInstanceSuccesLinkedIn() {


        try {
            if (text_sync_now != null) {
                text_sync_now.setText("CONNECT");
            }
            dismissDialog();


            if (!cancel_sync_dialog) {
                ShineSharedPreferences
                        .updateSynctimeLinkedIn(
                                mActivity,
                                System.currentTimeMillis());
                ShineSharedPreferences.saveLinkedinImported(mActivity,
                        true);


                Toast.makeText(mActivity, "Your linkedIn friends are now ready to refer you for " +
                        "jobs in their companies, start applying now.", Toast.LENGTH_LONG).show();

                if (text_sync_now != null) {
                    text_sync_now.setVisibility(View.GONE);
                }
                if (last_sync_linkedin != null) {
                    last_sync_linkedin.setVisibility(View.VISIBLE);
                }
                if (view != null) {
                    view.findViewById(R.id.sync_complete_linkedin)
                            .setVisibility(View.VISIBLE);
                }
                if (last_sync_linkedin != null) {
                    last_sync_linkedin.setText("Last synced now");
                }


                DialogUtils.showNotification(mActivity,"Your LinkedIn friends are now ready to refer you for jobs in" +
                        " their companies, start applying now");
                URLConfig.isRecentSync = true;
                holdCaching();

            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessLinkdin() {

        MyApplication.isLinkedInSyncing = false;
        if (getInstance() != null) {
            getInstance().onInstanceSuccesLinkedIn();
        } else {
            onInstanceSuccesLinkedIn();
        }

        ShineCommons.trackShineEvents("Import Contact Success","LinkedIn",mActivity);


    }

    public void onInstanceFailedLinkedIn() {
        try {
            if (text_sync_now != null) {
                text_sync_now.setText("CONNECT");
            }

            dismissDialog();
            Toast.makeText(
                    mActivity,
                    "LinkedIn sync failed. Please try again in some time.",
                    Toast.LENGTH_SHORT).show();
            ShineCommons.trackShineEvents("Import Contact Failed","LinkedIn",mActivity);
            ShineCommons.trackShineEvents("LinkedinLoginFail","ImportContacts",mActivity);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailedLinkdin() {
        MyApplication.isLinkedInSyncing = false;
        if (getInstance() != null) {
            getInstance().onInstanceFailedLinkedIn();
        } else {
            onInstanceFailedLinkedIn();
        }


    }


    private class PostRequestAsyncTask extends AsyncTask<String, Void, Boolean> {

        ProgressDialog plzWaitDialog;

        @Override
        protected void onPreExecute() {

            Log.i("DEBUG Friends", "In PostRequestAsyncTask: ");
            plzWaitDialog = new ProgressDialog(mActivity, ProgressDialog.STYLE_SPINNER);
            plzWaitDialog.setMessage("Authorizing...");
            plzWaitDialog.setCancelable(false);
            plzWaitDialog.show();

        }

        @Override
        protected Boolean doInBackground(String... urls) {
            Log.i("DEBUG Friends", "In PostRequestAsyncTask: ");
            if (urls.length > 0) {
                String url = urls[0];

                try {
                    StringBuilder content = new StringBuilder();
                    URL mUrl = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    if (connection.getResponseCode() == 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null)
                            content.append(line).append("\n");
                        bufferedReader.close();
                        JSONObject resultJson = new JSONObject(content.toString());
                        int expiresIn = resultJson.has("expires_in") ? resultJson
                                .getInt("expires_in") : 0;
                        String accessToken = resultJson.has("access_token") ? resultJson
                                .getString("access_token") : null;
                        if (expiresIn > 0 && accessToken != null) {
                            Log.i("DEBUG", "This is the access Token: "
                                    + accessToken + ". It will expires in "
                                    + expiresIn + " secs");
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.SECOND, expiresIn);
                            String expireDate = String.valueOf(calendar.getTimeInMillis());

                            ShineSharedPreferences.setLinkedinAccessToken(accessToken, mActivity);
                            ShineSharedPreferences.setLinkedinAccessExpiry(expireDate, mActivity);

                            return true;
                        }
                    } else {
                        DialogUtils.dialogDismiss(plzWaitDialog);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    DialogUtils.dialogDismiss(plzWaitDialog);
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status) {
            try {
                DialogUtils.dialogDismiss(plzWaitDialog);
                Log.d("LINKEDIN ", "on post");

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (status) {
                auth_dialog.hide();

                new GetConnectionRequestAsyncTask().execute(ShineSharedPreferences.getLinkedinAccessToken(mActivity));
            } else {
                Toast.makeText(
                        mActivity,
                        "Sorry we are not able to fetch your linkedin contacts at the moment. Please try after some time",
                        Toast.LENGTH_SHORT).show();
                auth_dialog.hide();
            }
        }
    }

    int linkedin_count = 0;

    private class GetConnectionRequestAsyncTask extends
            AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {

            if(LinkedIncustomLoader!=null)
            LinkedIncustomLoader.setVisibility(View.VISIBLE);
            if(last_sync_linkedin!=null)
            last_sync_linkedin.setVisibility(View.GONE);
            if(text_sync_now!=null) {
                text_sync_now.setText("Syncing...");
                text_sync_now.setVisibility(View.VISIBLE);
            }

            if(sync_finish_linkedin!=null)
            sync_finish_linkedin.setVisibility(View.GONE);


            if (lastsynctimelinkedin > 0) {
                Toast.makeText
                        (getActivity(),
                                "We are updating your friends information from LinkedIn, till then you can continue searching for jobs",
                                Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText
                        (getActivity(),
                                "We are syncing your LinkedIn connection and will show you friends who can refer to your jobs soon. Till then continue browsing",
                                Toast.LENGTH_LONG).show();
            }
        }

        boolean isRetry = false;

        @Override
        protected JSONObject doInBackground(String... urls) {
            MyApplication.isLinkedInSyncing = true;
            Log.d("DEBUG Friends", "Come " + urls[0]);

            if (urls.length > 0) {
                String accessToken = urls[0];

                try {

                    Log.d("DEBUG_LINKEDIN", "Start Hitting Connection API");
                    JSONArray values = new JSONArray();
                    int total = 1000;
                    int count = 0;


                    while (total - count > 0) {
                        StringBuilder content = new StringBuilder();
                        URL mUrl = new URL(LinkedinConstants.getConnectionsUrl(accessToken, count));
                        Log.d("DEBUG_LINKEDIN", "Hitting Connection API: start=" + count + " " + mUrl);
                        HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                        connection.setRequestProperty("x-li-format", "json");

                        int response_code = connection.getResponseCode();

                        if (response_code == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            String line;
                            while ((line = bufferedReader.readLine()) != null)
                                content.append(line).append("\n");
                            bufferedReader.close();
                            connection.disconnect();
                            JSONObject ans = new JSONObject(content.toString());
                            if (ans.has("_total"))    //update total from its default 1000
                            {
                                total = ans.getInt("_total");
                            }
                            if (!ans.has("values")) {
                                break;    //we have reached the end
                            }
                            JSONArray nVal = ans.getJSONArray("values");
                            if (nVal.length() == 0) {
                                break;    //we have reached the end
                            }
                            count = count + nVal.length();
                            for (int i = 0; i < nVal.length(); i++) {
                                values.put(nVal.get(i));
                            }

                        } else if (response_code == 401) {
                            isRetry = true;
                            Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                            return null;    //return null as we should discard partial data, if any.
                        } else {
                            Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                            break;  //break, don't return null, as we should dump partial data, if any.
                        }

                    }
                    if (values.length() == 0)  //return null if we were not able to get any connections
                        return null;
                    JSONObject object = new JSONObject();
                    object.put("_total", total);
                    object.put("_start", 0);
                    object.put("_count", count);
                    object.put("values", values);

                    JSONObject ans = LinkedinConstants.parseLinkedin(object);

                    if (ans.has("t"))
                        linkedin_count = ans.getInt("t");
                    return ans;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject data) {
            MyApplication.isLinkedInSyncing = false;

            if (cancel_sync_dialog) {
                if(text_sync_now!=null)
                text_sync_now.setText("CONNECT");
                return;
            }

            if (data != null) {
                MyApplication.isLinkedInSyncing = true;
                sendDataToserver(data);
            } else if (isRetry) {

                if(text_sync_now!=null)
                text_sync_now.setText("CONNECT");
                //clear tokens
                ShineSharedPreferences.setLinkedinAccessToken("", mActivity);
                ShineSharedPreferences.setLinkedinAccessExpiry("0", mActivity);
                //ask for Re-Login
                LinkedinConnect();
                //dismiss prev loader
                dismissDialog();

                Toast.makeText(
                        mActivity,
                        "Please login to sync your connections",
                        Toast.LENGTH_SHORT).show();
            } else {
                if(text_sync_now!=null)
                text_sync_now.setText("CONNECT");
                dismissDialog();
                Toast.makeText(
                        mActivity,
                        "Sorry we are not able to fetch your linkedin contacts at the moment. Please try after some time",
                        Toast.LENGTH_LONG).show();
            }


        }

    }

    public void sendDataToserver(JSONObject data) {
        new SendtoServerLinkedin(mActivity, data, this).sendtoServerLinkedInData();
    }


    /************************************************************************/
    // GMAIL RELATED FUNCTIONS

    /**
     * *******************************************************************
     */

    private final int REQUEST_ACCOUNT_PERMISSIONS = 13;


    private void GmailConnect() {
        am = AccountManager.get(mActivity);
        AuthPreferencesModel = new AuthPreferencesModel(mActivity);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        contact_list = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            chooseAccount();
        } else {
            requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS}, REQUEST_ACCOUNT_PERMISSIONS);
        }


    }

    private int REQUEST_CONTACT_PERMISSIONS = 11;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Map<String, Integer> perms = new HashMap<>();
        for (int i = 0; i < permissions.length; i++)
            perms.put(permissions[i], grantResults[i]);


        if (requestCode == REQUEST_CONTACT_PERMISSIONS) {

            if (perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

                Intent intent = new Intent(mActivity, ContactRetrieveService.class);
                intent.putExtra("isUserAction", true);
                mActivity.startService(intent);

            } else {
                Log.i("m_cooker", "contact permission was NOT granted.");
                Snackbar.make(view, "You need to give contacts permission to Sync your Phonebook",
                        Snackbar.LENGTH_SHORT)
                        .setAction("Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", mActivity.getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mActivity.startActivity(intent);
                            }
                        })
                        .show();
            }

        } else if (requestCode == REQUEST_ACCOUNT_PERMISSIONS) {
            if (perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                chooseAccount();
            } else {
                Log.i("m_cooker", "GET_ACCOUNTS permission was NOT granted.");
                Snackbar.make(view, "You need to give contacts permission to Sync your Google Account",
                        Snackbar.LENGTH_SHORT)
                        .setAction("Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", mActivity.getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mActivity.startActivity(intent);
                            }
                        })
                        .show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void PhonebookConnect() {

        if (ContextCompat.checkSelfPermission(mActivity, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(mActivity, ContactRetrieveService.class);
            intent.putExtra("isUserAction", true);
            mActivity.startService(intent);
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CONTACT_PERMISSIONS);
        }

    }

    private void doCoolAuthenticatedStuff() {
        String token = AuthPreferencesModel.getToken();
        String url = getProfileUrl(token);

        new FetchContact().execute(url);
    }

    private void chooseAccount() {

        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                new String[]{"com.google"}, false, null, null, null, null);
        try {
            startActivityForResult(intent, ACCOUNT_CODE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "Google service not avaiable!",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private static String getProfileUrl(String accessToken) {
        return END_POINT + QUESTION_MARK + OAUTH_ACCESS_TOKEN + EQUALS
                + accessToken + "&&alt=json&&max-results=1000";

    }

    private void requestToken() {
        Account userAccount = null;
        String user = AuthPreferencesModel.getUser();
        // Log.d("Authenticated", "User = " + user);
        for (Account account : am.getAccountsByType("com.google")) {
            if (account.name.equals(user)) {
                userAccount = account;
                break;
            }
        }
        am.getAuthToken(userAccount, "oauth2:" + SCOPE, null, mActivity,
                new OnTokenAcquired(), null);


        if(GmailcustomLoader!=null)
        GmailcustomLoader.setVisibility(View.VISIBLE);

        if(last_sync_gmail!=null)
        last_sync_gmail.setVisibility(View.GONE);
        if(textview_sync_gmail!=null)
        textview_sync_gmail.setText("Syncing...");
    }

    private void invalidateToken() {
        AccountManager accountManager = AccountManager.get(mActivity);
        accountManager.invalidateAuthToken("com.google",
                AuthPreferencesModel.getToken());
        AuthPreferencesModel.setToken(null);
    }

    private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            try {

                String token = result.getResult().getString(
                        AccountManager.KEY_AUTHTOKEN);

                AuthPreferencesModel.setToken(token);
                doCoolAuthenticatedStuff();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(GmailcustomLoader!=null)
            GmailcustomLoader.setVisibility(View.GONE);
            if(last_sync_gmail!=null)
            last_sync_gmail.setVisibility(View.VISIBLE);
            if(textview_sync_gmail!=null)
            textview_sync_gmail.setText("CONNECT");
        }
    }



    private class FetchContact extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            GmailcustomLoader.setVisibility(View.VISIBLE);
            last_sync_gmail.setVisibility(View.GONE);
            textview_sync_gmail.setText("Syncing...");
            textview_sync_gmail.setVisibility(View.VISIBLE);
            sync_finish_gmail.setVisibility(View.GONE);


        }

        @Override
        protected String doInBackground(String... args) {
            MyApplication.isGmailSyncing = true;
            if (args.length > 0 && !cancel_sync_dialog) {
                String url = args[0];

                try {
                    StringBuilder content = new StringBuilder();
                    URL mUrl = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    if (connection.getResponseCode() == 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null)
                            content.append(line).append("\n");
                        bufferedReader.close();
                        return content.toString();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;

        }

        @Override
        protected void onPostExecute(String json) {
            MyApplication.isGmailSyncing = false;
            if (json != null) {

                JSONObject jsonObj;
                try {

                    jsonObj = new JSONObject(json);
                    JSONObject feed = jsonObj.getJSONObject("feed");
                    JSONArray contacts = new JSONArray();
                    if (feed.has("entry")) {
                        contacts = feed.getJSONArray("entry");
                    }

                    for (int i = 0; i < contacts.length(); i++) {
                        gmail_total += 1;
                        String name = "";
                        String email = "";
                        String phone_number = "";
                        JSONObject contact = contacts.getJSONObject(i);
                        if (contact.has("title")) {
                            name = contact.getJSONObject("title").getString(
                                    "$t");
                        }
                        if (contact.has("gd$email")) {
                            JSONArray email_list = contact
                                    .getJSONArray("gd$email");
                            for (int j = 0; j < email_list.length(); j++) {
                                JSONObject temp = email_list.getJSONObject(j);
                                if (temp.has("address")) {
                                    email = temp.getString("address");
                                    // Log.e("FINAL", "" + email);
                                }
                            }
                        }

                        if (contact.has("gd$phoneNumber")) {
                            JSONArray phone_list = contact
                                    .getJSONArray("gd$phoneNumber");
                            for (int j = 0; j < phone_list.length(); j++)
                                phone_number = phone_list.getJSONObject(j)
                                        .getString("$t");
                        }
                        GmailProfileModel c = new GmailProfileModel(name,
                                email, phone_number);
                        contact_list.add(c);
                    }

                    Gson gson = new Gson();
                    String data = gson.toJson(contact_list);
                    if (!cancel_sync_dialog) {
                        MyApplication.isGmailSyncing = true;
                        sendDataToserverGmail(data);
                    } else {
                        dismissDialog();
                        textview_sync_gmail.setText("CONNECT");
                    }

                } catch (JSONException e) {
                    dismissDialog();
                    textview_sync_gmail.setText("CONNECT");
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                textview_sync_gmail.setText("CONNECT");
            }
        }
    }


    public void sendDataToserverGmail(String data) {
        new SendtoServerGmail(mActivity, data, this).sendtoServerGmailData();
    }

    public void show_result() {
        init();

        if (mbacknum == 3) {
            allSynced.setVisibility(View.VISIBLE);
            view.findViewById(R.id.dont_show_text).setVisibility(View.VISIBLE);
            update_profile_layout.setVisibility(View.GONE);

            view.findViewById(R.id.apply_thanks_heading).setVisibility(
                    View.VISIBLE);
            TextView tv = (TextView) view
                    .findViewById(R.id.apply_no_connection_heading);
            if (companyName == null || companyName.equals("")) {
                companyName = "this company.";
            }
            tv.setText("Connect to find out if someone can refer you at " + companyName);
            tv.setVisibility(View.VISIBLE);
            headline.setTextSize(11);
        }


        if (mbacknum == 4 || mbacknum == 3 || mbacknum == 2) {
            other_layout.setVisibility(View.VISIBLE);
        } else {
            if ((!ShineSharedPreferences.getGoogleImported(mActivity)

                    || !ShineSharedPreferences.getLinkedinImported(mActivity))) {
                other_layout.setVisibility(View.VISIBLE);
            }
        }

        if (mbacknum == 4) {

            if (ShineSharedPreferences.getPrefUpdateProfile(mActivity))

                update_profile_layout.setVisibility(View.GONE);
            else
                update_profile_layout.setVisibility(View.VISIBLE);
        } else if (mbacknum != 3) {

                if (ShineSharedPreferences.getPrefUpdateProfile(mActivity))

                    update_profile_layout.setVisibility(View.GONE);
                else
                    update_profile_layout.setVisibility(View.VISIBLE);
        }



        if (MyApplication.isLinkedInSyncing) {

            if(LinkedIncustomLoader!=null)
            LinkedIncustomLoader.setVisibility(View.VISIBLE);
            if(last_sync_linkedin!=null)
            last_sync_linkedin.setVisibility(View.GONE);

            if(text_sync_now!=null) {
                text_sync_now.setText("Syncing...");
                text_sync_now.setVisibility(View.VISIBLE);
            }

            if(sync_finish_linkedin!=null)
                sync_finish_linkedin.setVisibility(View.GONE);
        }
        if (MyApplication.isGmailSyncing) {


            if(GmailcustomLoader!=null)
            GmailcustomLoader.setVisibility(View.VISIBLE);
            if(last_sync_gmail!=null)
            last_sync_gmail.setVisibility(View.GONE);
           if(textview_sync_gmail!=null) {
               textview_sync_gmail.setText("Syncing...");
               textview_sync_gmail.setVisibility(View.VISIBLE);
           }

           if(sync_finish_gmail!=null)
               sync_finish_gmail.setVisibility(View.GONE);
        }

        if (MyApplication.isPhonebookSyncing) {
            if(PhonebookcustomLoader!=null)
            PhonebookcustomLoader.setVisibility(View.VISIBLE);
            if(last_sync_phonebook!=null)
            last_sync_phonebook.setVisibility(View.GONE);
            if(textview_sync_phonebook!=null)
            textview_sync_phonebook.setText("Syncing...");
            textview_sync_phonebook.setVisibility(View.VISIBLE);
            if(sync_finish_phonebook!=null)
            sync_finish_phonebook.setVisibility(View.GONE);
        }


    }


    private void showJobsDetails() {

        try {

            if (userProfileVO.current_company != null && !userProfileVO.current_company.isEmpty()) {
                String comp = "Current company - ";
                String full = comp + userProfileVO.current_company;
                SpannableString sb = new SpannableString(full);
                sb.setSpan(new StyleSpan(Typeface.BOLD), comp.length(), full.length()
                        , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                );
                currnt_comp.setText(sb);
            } else {
                currnt_comp.setText("Current company - Not Mentioned");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void showHighEduDetails() {

        try {

            if (URLConfig.EDUCATIONAL_QUALIFICATION_REVERSE_MAP.get(userProfileVO.highest_education) != null) {

                String edu = "Highest Education -";
                String full;

                if (userProfileVO.institute_name != null && !userProfileVO.institute_name.equals("null")) {

                    String degree = URLConfig.EDUCATIONAL_QUALIFICATION_REVERSE_MAP
                            .get(userProfileVO.highest_education).replace("#","");

                    full = edu + degree
                            + " from " + userProfileVO.institute_name;
                    SpannableString sb = new SpannableString(full);
                    sb.setSpan(new StyleSpan(Typeface.BOLD), edu.length(), full.length()
                            , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    higher_edu.setText(sb);
                } else {
                    full = edu + URLConfig.EDUCATIONAL_QUALIFICATION_REVERSE_MAP.get(userProfileVO.highest_education);
                    SpannableString sb = new SpannableString(full);
                    sb.setSpan(new StyleSpan(Typeface.BOLD), edu.length(), full.length()
                            , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                    higher_edu.setText(sb);
                }
            } else {
                higher_edu.setText("Highest Education - Not Mentioned");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private void dismissDialog() {

        if (!MyApplication.isGmailSyncing && GmailcustomLoader != null&&last_sync_gmail!=null) {
            GmailcustomLoader.setVisibility(View.GONE);
            last_sync_gmail.setVisibility(View.VISIBLE);
        }
        if (!MyApplication.isLinkedInSyncing && LinkedIncustomLoader != null&&last_sync_linkedin!=null) {
            LinkedIncustomLoader.setVisibility(View.GONE);
            last_sync_linkedin.setVisibility(View.VISIBLE);
        }


    }


}
