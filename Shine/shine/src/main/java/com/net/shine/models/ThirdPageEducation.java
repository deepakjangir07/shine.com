package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by dhruv on 3/8/15.
 */
public class ThirdPageEducation implements Serializable {

        String id = "";
        String candidate_id = "";
        String education_level = "";
        String education_specialization = "";
        String institute_name = "";
        String year_of_passout = "";
        String course_type = "";

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(String candidate_id) {
        this.candidate_id = candidate_id;
    }

    public String getCourse_type() {
        return course_type;
    }

    public void setCourse_type(String course_type) {
        this.course_type = course_type;
    }

    public String getEducation_level() {
        return education_level;
    }

    public void setEducation_level(String education_level) {
        this.education_level = education_level;
    }

    public String getEducation_specialization() {
        return education_specialization;
    }

    public void setEducation_specialization(String education_specialization) {
        this.education_specialization = education_specialization;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitute_name() {
        return institute_name;
    }

    public void setInstitute_name(String institute_name) {
        this.institute_name = institute_name;
    }

    public String getYear_of_passout() {
        return year_of_passout;
    }

    public void setYear_of_passout(String year_of_passout) {
        this.year_of_passout = year_of_passout;
    }
}
