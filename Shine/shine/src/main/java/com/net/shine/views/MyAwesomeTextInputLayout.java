package com.net.shine.views;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.net.shine.interfaces.FocusListener;

/**
 * Created by ujjawal-work on 07/04/16.
 */
public class MyAwesomeTextInputLayout extends TextInputLayout {
    public MyAwesomeTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public MyAwesomeTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public MyAwesomeTextInputLayout(Context context) {
        super(context);
    }


    @Override
    public void setError(CharSequence error) {

        if((getEditText()!=null && getEditText().getBackground()!=null) &&
                (Build.VERSION.SDK_INT == 21 || Build.VERSION.SDK_INT == 22)) {

            getEditText().setBackgroundDrawable(getEditText().getBackground().getConstantState().newDrawable());
        }


        if(getEditText()!=null && getEditText().getBackground()!=null) {
            getEditText().getBackground().mutate();
            Log.d("Debug::TIL", "Mutating " + getEditText().getText());
        }
        if(!TextUtils.isEmpty(error) && getEditText()!=null && getEditText().getText().length()==0 && !getEditText().hasFocus()) {
            setHintEnabled(false);
        }


        super.setError(error);
    }

//    @Override
//    public void setErrorEnabled(boolean enabled) {
//        super.setErrorEnabled(true);
//    }

    @Override
    public void setHintEnabled(boolean enabled) {
        super.setHintEnabled(true);
        if(!enabled){

            if(getEditText()!=null && getHint()!=null){
                String hint = (String) getHint();
                setHint(" ");
                if(!hint.equals(" "))
                    getEditText().setHint(hint);
                return;
            }
        } else {
            if(getEditText()!=null && getEditText().getHint()!=null){
                String hint = (String) getEditText().getHint();
                if(!hint.equals(" "))
                    setHint(hint);
                if(TextUtils.isEmpty(getEditText().getText()))
                    getEditText().setHint(" ");
            }
        }
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        if(child instanceof FocusListener)
            ((FocusListener)child).setTILFocusClearer(this);
        if(child instanceof TextView && child.getBackground()!=null) {
            child.getBackground().mutate();
            Log.d("Debug::TIL", "Mutating child: " +index  +" text:" + (((TextView) child).getText()));
        }
    }
}
