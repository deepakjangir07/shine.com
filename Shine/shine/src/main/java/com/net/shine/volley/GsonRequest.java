package com.net.shine.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.net.shine.config.URLConfig;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

public class GsonRequest<T> extends JsonRequest<T> {

    private Listener<T> listener;

    private final Gson gson = new Gson();
    private Map<String,String> headers;

    private Type listType;

    public GsonRequest(int method, String url, Map<String, String> params, Map<String, String> headers,
                       Listener<T> listener, ErrorListener errorListener, Type listType) {
        super(method, url, new JSONObject(params).toString(), listener, errorListener);
        this.headers = headers;
        this.listener = listener;
        this.listType = listType;
    }

    public GsonRequest(int method, String url, Map<String, String> headers,
                       Listener<T> listener, ErrorListener errorListener, Type listType) {
        super(method, url, null,listener, errorListener);
        this.headers = headers;
        this.listener = listener;
        this.listType = listType;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {

            // Volley does not handle null properly, so implement null response
            // check

            Log.d("RESPONSE code ","res "+response.statusCode);
            if (response.data.length == 0) {
                byte[] responseData = "{}".getBytes("UTF8");
                response = new NetworkResponse(response.statusCode,
                        responseData, response.headers, response.notModified);
            }


            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            Log.d("Volley::GsonReq", "JsonResponse: " + jsonString);


            T parseObject = gson.fromJson(jsonString, listType);


            return Response.success(
                    parseObject,
                    parseIgnoreCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            System.out.println("VolleyQueue: Encoding Error for " + getTag()
                    + " (" + getSequence() + ")");
            return Response.error(new ParseError(e));
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("VolleyQueue: Fatal Error for " + getTag()
                    + " (" + getSequence() + ")");
            return Response.error(new VolleyError(e));
        }
    }


    /**
     * Extracts a {@link Cache.Entry} from a {@link NetworkResponse}.
     * Cache-control headers are ignored. SoftTtl == 15 mins, ttl == 24 hours.
     *
     * @param response
     *            The network response to parse headers from
     * @return a cache entry for the given response, or null if the response is
     *         not cacheable.
     */
    public Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
        long now = System.currentTimeMillis();

        Map<String, String> headers = response.headers;
        long serverDate = 0;
        String serverEtag = null;
        String headerValue;

        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        serverEtag = headers.get("ETag");

        long cacheHitButRefreshed = 0;
        long cacheExpired = 0;
        if(shouldCache())
        {


                cacheExpired = VolleyNetworkRequest.FIFTEEN_MINUTES;
                cacheHitButRefreshed = VolleyNetworkRequest.FIFTEEN_MINUTES;
                if (getTag().equals("rel-sugg")
                        || getTag().equals("recent-search")
                        || getTag().equals(URLConfig.KEYWORD_HIT_TAG)
                        || getTag().equals(URLConfig.INSTITUTE_HIT_TAG)
                        || getTag().equals(URLConfig.SKILL_HIT_TAG)) {
                    cacheExpired = Long.MAX_VALUE - now - 100;
                    cacheHitButRefreshed = VolleyNetworkRequest.ONE_WEEK;
                }

        }
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheExpired;

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        entry.ttl = ttl;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        return entry;
    }

    @Override
    public void deliverError(VolleyError error) {

        super.deliverError(error);

    }

    @Override
    protected void deliverResponse(T response) {

        listener.onResponse(response);
    }

}