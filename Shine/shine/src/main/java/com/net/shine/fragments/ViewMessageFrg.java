package com.net.shine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.net.shine.R;
import com.net.shine.models.ActionBarState;

/**
 * Created by F2722 on 08-06-2015.
 */


public class ViewMessageFrg extends BaseFragment {
    WebView mWebView;
    LinearLayout loading;
    String mUrl;

    public static ViewMessageFrg newInstance(String url) {
        Bundle args = new Bundle();
        args.putString("url", url);
        ViewMessageFrg fragment = new ViewMessageFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getArguments().getString("url", "");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_message_layout, container, false);

        try {
            loading = (LinearLayout) view.findViewById(R.id.loading_cmp);
            loading.setVisibility(View.VISIBLE);
            mWebView = (WebView) view.findViewById(R.id.webview);
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(mUrl);
            mWebView.setWebViewClient(new myWebClient());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mActivity.setTitle("Message");
            //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class myWebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            loading.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

    }

}
