package com.net.shine.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.ChildParentSpinnerWithEditTextFrag;

import java.util.HashMap;
import java.util.List;

import static com.net.shine.fragments.ChildParentSpinnerWithEditTextFrag.tView;

/**
 * Created by manishpoddar on 29/03/17.
 */

public class ExpandableListWithEditTextAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    public static   String other_edu;


    public ExpandableListWithEditTextAdapter(Context context, List<String> listDataHeader,
                                             HashMap<String, List<String>> listChildData,String other_edu) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.other_edu=other_edu;



    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public Integer[] getChildInfoByName(String child) {
        Integer[] info = new Integer[2];
        info[0] = -1;
        info[1] = -1;
        for (int i = 0; i < _listDataHeader.size(); i++) {
            List<String> childList = _listDataChild.get(_listDataHeader.get(i));
            for (int j = 0; j < childList.size(); j++) {
                if (childList.get(j).equals(child)) {
                    info[0] = i;
                    info[1] = j;
                    return info;
                }
            }
        }
        return info;
    }


    int selected_grp_pos = -1;
    int selected_child_pos = -1;

    public void setSelectedChild(int grp, int subchild) {

        Log.d("group_pos main",""+grp);
        Log.d("child_pos main",""+subchild);

        selected_grp_pos = grp;
        selected_child_pos = subchild;



        Log.d("group_pos ",""+selected_grp_pos);
        Log.d("child_pos ",""+selected_child_pos);
        notifyDataSetChanged();
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {



        final String childText = (String) getChild(groupPosition, childPosition);

        final String parentText= (String) getGroup(groupPosition);


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_single_choice_with_textview, null);

        }

        final CheckedTextView txtListChild = (CheckedTextView) convertView.findViewById(R.id.text1);
        final EditText edt_other=(EditText)convertView.findViewById(R.id.other_textbox);


        RelativeLayout otherLayout=(RelativeLayout)convertView.findViewById(R.id.other_layout);

        TextView save_other=(TextView)convertView.findViewById(R.id.save_other);


        edt_other.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                other_edu=charSequence.toString();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        save_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!TextUtils.isEmpty(edt_other.getText()))
                {
                    try {
                        tView.setText(parentText+" ("+edt_other.getText()+")");
                        ((BaseActivity)_context).getSupportFragmentManager().popBackStackImmediate();
                        other_edu="";

                        int groupPosition1 = URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_LIST.indexOf(_listDataHeader.get(groupPosition));
                        Integer[] info = new Integer[2];
                        info[0] = URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.get(groupPosition1-1);
                        info[1] = URLConfig.CHILD_CID_MAP.get(info[0]).get(childPosition);
                        tView.setTag(info);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        });


        txtListChild.setTextColor(Color.parseColor("#555555"));
        if (Build.VERSION.SDK_INT >= 16) {
            txtListChild.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        }

        txtListChild.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);


        txtListChild.setText(childText);

        if (selected_grp_pos == groupPosition && selected_child_pos == childPosition) {
            txtListChild.setChecked(true);


        }
        else {
            txtListChild.setChecked(false);

        }

            if (childText.equalsIgnoreCase("Other") && txtListChild.isChecked()==true) {
                edt_other.requestFocus();


                otherLayout.setVisibility(View.VISIBLE);
                edt_other.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(other_edu)) {
                    edt_other.setText(other_edu);
                    edt_other.setSelection(edt_other.getText().length());

                }

            } else {

               // edt_other.setText("");

                otherLayout.setVisibility(View.GONE);

                edt_other.setVisibility(View.GONE);
            }





      //  Integer[] info = new Integer[2];
        //info[0] = groupPosition;
        //info[1] = childPosition;


        //convertView.setTag(edt_other);





//        if(childText.equalsIgnoreCase("other"))
//        {
//            edt_other.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            edt_other.setVisibility(View.GONE);
//
//        }


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.spinner_parent_items, parent, false);
        }
        ImageView img = (ImageView) convertView.findViewById(R.id.expand_indicator);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);

        if (isExpanded) {
            img.setImageResource(R.drawable.textdropup_arrow);
            if (Build.VERSION.SDK_INT < 16) {
                lblListHeader.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
            } else {
                lblListHeader.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));
            }


        } else {
            img.setImageResource(R.drawable.textdropdown_arrow);
            if (Build.VERSION.SDK_INT < 16) {
                lblListHeader.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
            } else {
                lblListHeader.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
            }
        }

        lblListHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void refreshLists(List<String> listDataHeader,
                             HashMap<String, List<String>> listChildData,
                             int selected_grp_pos, int selected_child_pos,String other_edu) {
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.other_edu=other_edu;
        setSelectedChild(selected_grp_pos, selected_child_pos);
        notifyDataSetChanged();
    }



}


