package com.net.shine.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class DataCaching {
	public final static String PERSONAL_SECTION = "per_details";
	public final static String DESIRED_DETAILS_SECTION = "desired_job_details";
	public final static String JOB_LIST_SECTION = "prof_details";
	public final static String TOTAL_EXP_SECTION = "total_exp_details";
	public final static String EDUCATION_LIST_SECTION = "educ_details";
	public final static String SKILLS_LIST_SECTION = "skills_details";
	public final static String CERTIFICATION_LIST_SECTION="certification_details";
	public final static String RESUME_LIST_SECTION = "resume_details";
	public final static String APPLIED_JOB_SET = "applied_jobs_set";
	public final static int MINUTS = 30;

	public final static int CACHE_TIME_LIMIT = MINUTS * (60 * 1000);
	public final static int CACHE_TIME_LIMIT_CONNECTION = 15 * (60 * 1000);
	private static DataCaching caching;
	public static final String DATA = "data";
	public static final String TIME = "time";
	public static final String CACHE_TIME = "cache";

	private static HashMap<String, Object> cachedData;


	public synchronized static DataCaching getInstance() {
		if (caching == null) {
			caching = new DataCaching();
		}

		if (cachedData == null)
			cachedData = new HashMap<String, Object>();
		return caching;
	}

	public synchronized Object getObject(Context context, String key) {
		Object object = null;
		FileInputStream fis = null;
		ObjectInputStream is = null;
		try {
			File file = context.getFileStreamPath(key);
			if(file.exists()) {

				fis = context.openFileInput(key);
				is = new ObjectInputStream(fis);
				HashMap<String, Object> data = (HashMap<String, Object>) is
						.readObject();

				if (data != null) {
					data = (HashMap<String, Object>) data.get(key);
					if (data != null) {

						long actualTime = (Long) data.get(TIME);
						long cacheLimit = Integer.parseInt((String) data
								.get(CACHE_TIME));

						long currentTime = System.currentTimeMillis();

						if ((currentTime - actualTime) > cacheLimit) {
							return null;
						}

						object = data.get(DATA);

					}
				}
			}
			else
			{
				Log.d("DataCache:FileNotFound", key+" not found");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
				if (is != null)
					is.close();
			} catch (Exception e) {
			}
		}

		return object;
	}

	public synchronized void cacheData(String key, String cacheTime,
			Object data, Context context) {
		if (!cacheTime.equals("")) {
			HashMap<String, Object> cachedData = new HashMap<String, Object>();
			cachedData.put(DataCaching.DATA, data);
			cachedData.put(DataCaching.CACHE_TIME, cacheTime);
			cachedData.put(TIME, System.currentTimeMillis());
			savedata(context, key, cachedData);
		}
	}

	private void savedata(Context context, String key, Object object) {
		cachedData.clear();
		cachedData.put(key, object);
		saveObject(cachedData, context, key);
	}

	public static boolean saveObject(Object obj, Context context, String key) {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		boolean keep = true;
		try {
			fos = context.openFileOutput(key, Context.MODE_PRIVATE);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (oos != null)
					oos.close();
				if (fos != null)
					fos.close();
			} catch (Exception e) { /* do nothing */
			}
		}

		return keep;
	}


	public static void removeAllObjects(Context context) {
		try {
			HashMap<String, Object> cachedData = new HashMap<String, Object>();
			saveObject(cachedData, context, DataCaching.PERSONAL_SECTION);
			saveObject(cachedData, context, DataCaching.DESIRED_DETAILS_SECTION);
			saveObject(cachedData, context, DataCaching.JOB_LIST_SECTION);
			saveObject(cachedData, context, DataCaching.TOTAL_EXP_SECTION);
			saveObject(cachedData, context, DataCaching.EDUCATION_LIST_SECTION);
			saveObject(cachedData, context, DataCaching.SKILLS_LIST_SECTION);
			saveObject(cachedData, context, DataCaching.RESUME_LIST_SECTION);
			saveObject(cachedData,context, DataCaching.CERTIFICATION_LIST_SECTION);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
