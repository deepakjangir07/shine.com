package com.net.shine.fragments.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.net.shine.adapters.JobListRecyclerAdapterForAds;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.fragments.AppliedJobsFrg;
import com.net.shine.fragments.MatchedJobsFrg;
import com.net.shine.fragments.NetworkJobsFrg;
import com.net.shine.fragments.RecommendedJobsFrg;
import com.net.shine.fragments.SavedJobsFrag;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ujjawal-work on 22/08/16.
 */

public class JobsTabFrg extends MainTabFrg {

    public static final int MATCHED_TAB = 0;
    public static final int RECOMMENDED_TAB = 1;
    public static final int SAVED_TAB = 2;
    public static final int APPLIED_TAB = 3;
    public static final int NETWORK_TAB = 4;



    public static JobsTabFrg newInstance(Bundle args) {
        JobsTabFrg fragment = new JobsTabFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setTitle("Jobs for you");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        setHasOptionsMenu(true);
        mActivity.showSelectedNavButton(mActivity.JOBS_OPTION);
        if(getArguments()!=null)
        {
        currentSelected = getArguments().getInt(SELECTED_TAB, MATCHED_TAB);
        }
        else {
            currentSelected=MATCHED_TAB;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Jobs for you");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showSelectedNavButton(mActivity.JOBS_OPTION);

    }

    @Override
    public List<Fragment> getFragments() {
        List<Fragment> frgList = new ArrayList<>();

        MatchedJobsFrg f1 = MatchedJobsFrg.newInstance(getArguments());
        RecommendedJobsFrg f2 = RecommendedJobsFrg.newInstance(getArguments());
        SavedJobsFrag f3 = SavedJobsFrag.newInstance(getArguments());
        AppliedJobsFrg f4 = AppliedJobsFrg.newInstance();
        NetworkJobsFrg f5 = NetworkJobsFrg.newInstance(getArguments());
        frgList.add(f1);
        frgList.add(f2);
        frgList.add(f3);
        frgList.add(f4);
        frgList.add(f5);
        return frgList;
    }



    @Override
    public ArrayList<String> getNames() {
        ArrayList<String> tabName = new ArrayList<>();
        tabName.add("Matched");
        tabName.add("Recommended");
        tabName.add("Saved");
        tabName.add("Applied");
        tabName.add("Jobs with connections");
        return tabName;
    }

    @Override
    protected void PageSelected(final int position) {
        super.PageSelected(position);
        try {

            if(position==0) {
                ManualScreenTracker.manualTracker("MatchedJobs");
            }
            else if(position==1){
                ManualScreenTracker.manualTracker("RecommendedJobs");
            }
            else if(position==2)
            {
                ManualScreenTracker.manualTracker("SavedJobs");
            }
            else if(position==3)
            {
                ManualScreenTracker.manualTracker("JobsApplied");
            }
            else if(position==4)
            {
                ManualScreenTracker.manualTracker("JobsWithConnection");
            }

            if(JobListRecyclerAdapterForAds.actionMode!=null) {
                JobListRecyclerAdapterForAds.actionMode.finish();
            }
            if(JobsListRecyclerAdapter.actionMode!=null)
            {
                JobsListRecyclerAdapter.actionMode.finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



}
