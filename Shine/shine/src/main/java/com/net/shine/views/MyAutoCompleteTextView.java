package com.net.shine.views;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.net.shine.R;
import com.net.shine.interfaces.FocusListener;

/**
 * Created by ujjawal-work on 14/04/16.
 */
public class MyAutoCompleteTextView extends AppCompatAutoCompleteTextView implements FocusListener {

    public MyAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
        if(getBackground()!=null)
            getBackground().mutate();
        super.setInputType(getInputType()| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

    }
    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
        if(getBackground()!=null)
            getBackground().mutate();
        super.setInputType(getInputType()| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }


    public MyAutoCompleteTextView(Context context) {
        super(context);
        setHintTextColor(ContextCompat.getColor(getContext(), R.color.labelColor));
        if(getBackground()!=null)
            getBackground().mutate();
        super.setInputType(getInputType()| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    private OnFocusChangeListener myListener;

    public void setTILFocusClearer(final TextInputLayout parentTextInputLayout){
        myListener = new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus|| !TextUtils.isEmpty(getText())) {
                    parentTextInputLayout.setHintEnabled(true);
                    parentTextInputLayout.setHintAnimationEnabled(true);
                }
                else if(!TextUtils.isEmpty(parentTextInputLayout.getError())){
                    parentTextInputLayout.setHintEnabled(false);
                }
            }
        };
        super.setOnFocusChangeListener(myListener);
    }

    @Override
    public void setOnFocusChangeListener(final OnFocusChangeListener l) {
        super.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                l.onFocusChange(v, hasFocus);
                if (myListener != null)
                    myListener.onFocusChange(v, hasFocus);
            }
        });
    }

}
