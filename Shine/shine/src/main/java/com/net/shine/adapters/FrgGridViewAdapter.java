package com.net.shine.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.CustomJobAlertEditFragment;
import com.net.shine.fragments.MyCustomJobAlertFragment;
import com.net.shine.models.FacetsSubModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.CustomAlert;

import java.util.ArrayList;

public class FrgGridViewAdapter extends BaseAdapter {

	private BaseFragment baseFragment;
	private ArrayList itemList;
	private LayoutInflater inflater;
	private int screenId;
	private ArrayList<String> facetsTitleList;

	public static final int CUSTOM_JOB_ALERT_LIST_SCREEN = 11;
	public static final int FACETS_LIST_SCREEN = 13;

	private BaseActivity mActivity;


	public FrgGridViewAdapter(BaseFragment baseFragment, ArrayList itemList,
							  int screenId) {
		try {

			this.baseFragment = baseFragment;
			this.itemList = itemList;
			this.mActivity = baseFragment.mActivity;
			inflater = baseFragment.mActivity.getLayoutInflater();
			this.screenId = screenId;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FrgGridViewAdapter(BaseFragment baseFragment, ArrayList itemList,
							  int screenId, ArrayList<String> facetsTitleList) {
		try {

			this.baseFragment = baseFragment;
			this.itemList = itemList;
			this.facetsTitleList = facetsTitleList;
			this.mActivity = baseFragment.mActivity;
			this.screenId = screenId;
			inflater = baseFragment.mActivity.getLayoutInflater();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public int getCount() {
		if (itemList != null) {
			return itemList.size();
		}
		return 0;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public Object getItem(int position) {
		return itemList.get(position);
	}

	public void rebuildListObject(ArrayList itemList) {

		this.itemList = itemList;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		try {

             if (screenId == CUSTOM_JOB_ALERT_LIST_SCREEN) {

				view = inflater.inflate(R.layout.custom_job_alert_row, parent, false);

				final SimpleSearchVO sObj = (SimpleSearchVO) itemList
						.get(position);

				TextView keyword = (TextView) view.findViewById(R.id.keyword);
				TextView keyword_text = (TextView) view.findViewById(R.id.keyword_text);
				TextView exp = (TextView) view.findViewById(R.id.experience);
				TextView experience_text = (TextView) view.findViewById(R.id.exp_text);
				TextView salary = (TextView) view.findViewById(R.id.salary);
				TextView salary_text = (TextView) view.findViewById(R.id.sal_text);
				TextView location = (TextView) view.findViewById(R.id.location);
				TextView location_text = (TextView) view.findViewById(R.id.location_text);
				TextView functionalarea = (TextView) view.findViewById(R.id.functional_area);
				TextView functional_area_text = (TextView) view.findViewById(R.id.functional_area_text);
				TextView industry = (TextView) view.findViewById(R.id.industry);
				TextView industry_text = (TextView) view.findViewById(R.id.industry_text);
				TextView alert_name = (TextView) view
						.findViewById(R.id.alert_name);


				ImageView edit = (ImageView) view.findViewById(R.id.edit_alert);
				edit.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle  bundle = new Bundle();
						bundle.putSerializable(URLConfig.CUSTOM_JOB_MODIFY, sObj);
						bundle.putSerializable("add_or_edit", CustomJobAlertEditFragment.EDIT_JOB_ALERT);
						Fragment fragment = CustomJobAlertEditFragment.newInstance(bundle);
						fragment.setTargetFragment(baseFragment, 15);
						mActivity.singleInstanceReplaceFragment(fragment);
					}
				});

				ImageView delete = (ImageView) view.findViewById(R.id.delete_alert) ;
				delete.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String alertId = sObj.getJobId();
						CustomAlert ca = new CustomAlert(
								FrgGridViewAdapter.this.mActivity, (MyCustomJobAlertFragment) baseFragment.getTargetFragment());

						ca.deleteCustomAlert(alertId);
					}
				});

				String keywordStr = sObj.getKeyword();
				if (!keywordStr.equals("")) {
					keyword.setVisibility(View.VISIBLE);
					keyword_text.setVisibility(View.VISIBLE);
					keyword.setText(sObj.getKeyword());
				} else {
					keyword.setVisibility(View.GONE);
					keyword_text.setVisibility(View.GONE);
				}
				alert_name.setText(sObj.getCustomJobAlertName());

				String tag_val = sObj.getCustomJobAlertLocTag()
						.replaceAll("\\[", "").replaceAll("\\]", "")
						.replaceAll("\"", "").replace(",", "#").replaceAll(" ","");


			     if(!sObj.getCustomJobAlertLocText().equals("")) {
					 location.setVisibility(View.VISIBLE);
					 location_text.setVisibility(View.VISIBLE);
                    location.setText(sObj.getCustomJobAlertLocText());
                    tag_val="";
                }
                else if (!tag_val.equals("")) {
					 location.setVisibility(View.VISIBLE);
					 location_text.setVisibility(View.VISIBLE);
					location.setTag(tag_val);

					String[] i_loc = tag_val.split("#");

					 for(String i: i_loc)
					 {
						 Log.d("strLoc", i);
					 }

					String loc_text = "";
					for (int i = 0; i < i_loc.length; i++) {

						loc_text = loc_text
								+ URLConfig.CITY_REVERSE_MAP.get(""
										+ Integer.parseInt(i_loc[i])) + ", ";
					}
					loc_text = loc_text.substring(0, loc_text.length() - 2);
					location.setText(loc_text);
				}

                else {
					location.setVisibility(View.GONE);
					 location_text.setVisibility(View.GONE);
				}

				String tag_val_func = sObj.getCustomJobAlerMinfuncAreaTag()
						.replaceAll("\\[", "").replaceAll("\\]", "")
						.replaceAll("\"", "").replace(",", "#").replaceAll(" ", "");
				if (!tag_val_func.equals("")) {
					functionalarea.setTag(tag_val_func);
					functionalarea.setVisibility(View.VISIBLE);
					functional_area_text.setVisibility(View.VISIBLE);

					String[] i_func = tag_val_func.split("#");
					String func_text = "";
					for (int i = 0; i < i_func.length; i++) {
						func_text = func_text
								+ URLConfig.FUNCTIONA_AREA_REVERSE_MAP.get(""
										+ Integer.parseInt(i_func[i])) + ", ";
					}
					func_text = func_text.substring(0, func_text.length() - 2);
					functionalarea.setText(func_text);
				} else {
					functionalarea.setVisibility(View.GONE);
					functional_area_text.setVisibility(View.GONE);
				}

				String tag_val_indus = sObj.getCustomJobAlerIndusTypeTag()
						.replaceAll("\\[", "").replaceAll("\\]", "")
						.replaceAll("\"", "").replace(",", "#").replaceAll(" ", "");
				if (!tag_val_indus.equals("")) {
					industry.setVisibility(View.VISIBLE);
					industry_text.setVisibility(View.VISIBLE);
					industry.setTag(tag_val_indus);

					String[] i_indus = tag_val_indus.split("#");
					String indus_text = "";
					for (int i = 0; i < i_indus.length; i++) {
						indus_text = indus_text
								+ URLConfig.INDUSTRY_REVERSE_MAP.get(""
										+ Integer.parseInt(i_indus[i])) + ", ";
					}
					indus_text = indus_text.substring(0,
							indus_text.length() - 2);
					industry.setText(indus_text);
				} else {
					industry.setVisibility(View.GONE);
					industry_text.setVisibility(View.GONE);
				}

				if (sObj.getMinExpTag() == 0) {
					exp.setVisibility(View.GONE);
					experience_text.setVisibility(View.GONE);

				} else {

					
					exp.setVisibility(View.VISIBLE);
					experience_text.setVisibility(View.VISIBLE);
					exp.setText(URLConfig.EXPERIENCE_REVERSE_MAP.get(String.valueOf(sObj.getMinExpTag())));
				
				}
				if (sObj.getMinSalTag() == 0) {
					salary.setVisibility(View.GONE);
					salary_text.setVisibility(View.GONE);

				} else {
					salary.setVisibility(View.VISIBLE);
					salary_text.setVisibility(View.VISIBLE);
					salary.setText(URLConfig.ANNUAL_SALARY_REVRSE_MAP.get("" + sObj.getMinSalTag()));
				}

			}
        else if (screenId == FACETS_LIST_SCREEN) {

				view = inflater.inflate(R.layout.facets_row, parent, false);

				FacetsSubModel fvo = (FacetsSubModel) itemList.get(position);

                CheckBox check = (CheckBox) view
						.findViewById(R.id.facets_img_checkbox);

				View facets_check = view.findViewById(R.id.facets);
				facets_check.setOnClickListener((OnClickListener) baseFragment);

				if (facetsTitleList.contains(fvo.getKey())) {
					check.setChecked(true);
				} else {
					check.setChecked(false);
				}

				check.setTag(fvo.getKey());
				TextView value = (TextView) view.findViewById(R.id.title_tv);
				value.setText(fvo.getValue());
				TextView numbers = (TextView) view
						.findViewById(R.id.title_tv_number);
				numbers.setText("(" + fvo.getNumber() + ")");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;

	}


	}

