package com.net.shine.volley;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.net.shine.MyApplication;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.SplashScreen;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.VolleyExecutor;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.auth.GetClientAccessToken;
import com.net.shine.utils.auth.GetUserAccessToken;

import org.apache.http.HttpStatus;
import org.apache.http.entity.mime.MultipartEntity;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.TreeMap;


public class VolleyNetworkRequest implements Response.Listener,
        Response.ErrorListener, VolleyExecutor  {

    public static long FIFTEEN_MINUTES = 15 * 60 * 1000;
    public static long ONE_DAY = 24 * 60 * 60 * 1000;

    public static long ONE_WEEK = 7 * 24 * 60 * 60 * 1000;


    public static long ONE_MONTH = 30 * 24 * 60 * 60 * 1000;

    private VolleyRequestCompleteListener callBack;
    private boolean usePostMethod = false;
    private boolean usePutMethod = false;
    private boolean useDeleteMethod = false;
    private boolean useMultiPartMethod = false;
    private boolean useInsRequest = false;


    private String mUrl;
    private HashMap params = new HashMap();

    private HashMap<String, String> headers = new HashMap<String, String>();
    private MultipartEntity entity;
    private Context mContext;
    private Type listType;
    private String tag ;

    public VolleyNetworkRequest(Context context, VolleyRequestCompleteListener callBack,
                                String mUrl, Type listType) {
        mContext = context;
        this.callBack = callBack;
        this.mUrl = mUrl;
        this.listType = listType;

        createHeaders();
    }

    //INS; for downloading resume
    public VolleyNetworkRequest(Context context, VolleyRequestCompleteListener callBack,
                                String mUrl) {
        mContext = context;
        this.callBack = callBack;
        this.mUrl = mUrl;

        createHeaders();
    }



    //for multipart request
    public VolleyNetworkRequest(Context context, VolleyRequestCompleteListener callBack,
                                String mUrl, Type listType, MultipartEntity entity) {
        this.callBack = callBack;
        this.mUrl = mUrl;
        this.entity=entity;
        mContext = context;
        this.listType = listType;

    }

    public void addHeader(String key, String value)
    {
        if(headers==null)
            headers = new HashMap<>();
        headers.put(key,value);
    }



    public void createHeaders()
    {
        try {
            headers = MyApplication.getInstance().getHeaders();

        }catch (AuthFailureError e)
        {
            e.printStackTrace();
        }
    }


    public void setUsePostMethod(HashMap params) {
        usePostMethod = true;
        this.params = params;
    }


    public void setUsePutMethod(HashMap params) {
        usePutMethod = true;
        this.params = params;
    }

    public void setUseDeleteMethod() {
        useDeleteMethod = true;

    }

    public void setMultiPartMethod(){

        useMultiPartMethod = true;

    }

    public void setUseInsRequest(){

        useInsRequest = true;
    }

    public void execute(String tag) {

        this.tag = tag;

        Log.d("URL", mUrl);

        if (URLUtil.isValidUrl(mUrl)) {

            if (usePostMethod) {
                Log.d("REF","params "+params.toString());

                GsonRequest req = new GsonRequest(
                        Request.Method.POST, mUrl, params, headers, this, this, listType
                );


                req.getTimeoutMs();
                req.setShouldCache(false);
                MyApplication.getInstance().addToRequestQueue(req, tag);
            }

            else if (usePutMethod) {

               Log.d("REF","params "+params.toString());
                GsonRequest req = new GsonRequest(
                        Request.Method.PUT, mUrl, params, headers, this, this, listType
                );

                req.setShouldCache(false);
                MyApplication.getInstance().addToRequestQueue(req, tag);

            } else if (useDeleteMethod) {
                Log.d("URL", mUrl);

                GsonRequest req = new GsonRequest(
                        Request.Method.DELETE, mUrl, params, headers, this, this, listType
                );

                req.setShouldCache(false);
                MyApplication.getInstance().addToRequestQueue(req, tag);
            } else if(useMultiPartMethod)
            {

                CustomMultipartRequest req = new CustomMultipartRequest(Request.Method.POST,mUrl, this, this, listType, entity);
                req.setShouldCache(false);
                MyApplication.getInstance().addToRequestQueue(req, tag);

            }
            else if(useInsRequest)
            {
                CustomInsRequest req = new CustomInsRequest(Request.Method.GET, mUrl, this, this);
                req.setShouldCache(false);
                MyApplication.getInstance().addToRequestQueue(req, tag);
            }
            else {

                GsonRequest req = new GsonRequest(

                        Request.Method.GET, mUrl, headers, this, this, listType
                );

                if(tag.equals(URLConfig.SKILL_HIT_TAG) || tag.equals("rel-sugg") || tag.equals("matchedJobsFrag")
                        || tag.equals(URLConfig.KEYWORD_HIT_TAG) || tag.equals(URLConfig.INSTITUTE_HIT_TAG)
                        || tag.equals("Shine_Non_Connection") || tag.equals("DiscoverFriends") || tag.equals("DiscoverFragJobCount")
                        || tag.equals("recent-search")||tag.equals("AD")||tag.equals("BELOWAD"))
                {
                    req.setShouldCache(true);
                }

                else {
                    req.setShouldCache(false);
                }


                Log.d("REF","TIMEOUT "+req.getTimeoutMs());

                MyApplication.getInstance().addToRequestQueue(req, tag);

            }
        }
    }



    @Override
    public void onResponse(Object result) {

        try {

            callBack.OnDataResponse(result, tag);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        try{
            if (error.networkResponse != null && (error.networkResponse.statusCode == 403
                    || error.networkResponse.statusCode == 401)) {
                String s = new String(error.networkResponse.data);

                Log.d("TOKEN", "Server Response: " + s);
                if (s.contains("Client")) {
                    Log.d("TOKEN", "Gettting Client Token");
                    new GetClientAccessToken(mContext, this, tag).getClientAccessToken();
                } else if (s.contains("User") && !tag.equals("UserAccessToken"))    //to check infinite looping
                {
                    Log.d("TOKEN", "Gettting User Token");
                    new GetUserAccessToken(mContext, this, tag).getUserAccessToken();
                }

                else {

                    Log.d("403ERROR", "ALert!! " + s);

                    if (mContext instanceof BaseActivity && ShineSharedPreferences.getUserInfo(mContext)!=null)
                    {
                        Log.d("403ERROR", "ALert!!ALert!!ALert!!ALert!! " + s);
                        ((BaseActivity) mContext).logout("Your login credentials has been updated, please login with your new credentials");
                    }
                    else
                    {
                        callBack.OnDataResponseError(ErrorHandler.getErrorResponse(error, tag),tag);
                    }
                }


            }

            else if(error.networkResponse!=null&&(error.networkResponse.statusCode==HttpStatus.SC_MOVED_TEMPORARILY||error.networkResponse.statusCode==HttpStatus.SC_MOVED_PERMANENTLY)){


                try {
                    TreeMap<String, String> headers = (TreeMap<String, String>) error.networkResponse.headers;
                    String redirectUrl = headers.get("Location");
                    Log.i("Redirect Url", redirectUrl);

                    if(redirectUrl!=null)
                    {
                        Intent intent = new Intent(mContext, SplashScreen.class);
                        intent.setData(Uri.parse(redirectUrl));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                    }
                    else {

                        Intent intent = new Intent(mContext, SplashScreen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(intent);
                        callBack.OnDataResponseError(ErrorHandler.getErrorResponse(error, tag),tag);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callBack.OnDataResponseError(ErrorHandler.getErrorResponse(error, tag),tag);

                }
            }

            else {
                callBack.OnDataResponseError(ErrorHandler.getErrorResponse(error, tag),tag);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
