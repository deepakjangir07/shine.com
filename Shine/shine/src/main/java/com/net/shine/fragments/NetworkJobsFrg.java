package com.net.shine.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.adapters.NetworkJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.EndlessRecyclerOnScrollListener;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class NetworkJobsFrg extends BaseFragment implements
		VolleyRequestCompleteListener, GetConnectionListener,SwipeRefreshLayout.OnRefreshListener {

	private RecyclerView gridView;
	private NetworkJobsAdapter adapter;
	private boolean loadMore = false;
	private View LoadingCmp;
	private int noofhits = 1;
	private int totalJobs;
	private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
	private String sortDate;
	private View errorlayout;
	private View view;
	private Set<String> unique_companies;
	private String co_names = "";
	private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private SwipeRefreshLayout swipeContainer;
	private Dialog dialog;
	private RadioButton btnRelevance, btnFreshness;

	private String nextUrl = "";




	public static NetworkJobsFrg newInstance(Bundle args) {
		NetworkJobsFrg fragment = new NetworkJobsFrg();
		fragment.setArguments(args);
		return fragment;
	}


	@Override
	public void onResume() {
		super.onResume();

		if(adapter.actionMode != null)
			adapter.actionMode.finish();
	}


	@Override
	public void onPause() {
		super.onPause();
		if(adapter.actionMode != null)
			adapter.actionMode.finish();

	}

	@Override
	public void onStop() {
		super.onStop();
		if(adapter.actionMode != null)
			adapter.actionMode.finish();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		setHasOptionsMenu(true);

		view = inflater.inflate(R.layout.matched_jobs_view, container, false);
		LoadingCmp=view.findViewById(R.id.loading_cmp);
		try {
            swipeContainer=(SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            swipeContainer.setOnRefreshListener(this);
			Bundle bundle = getArguments();
            if(bundle!=null)
			sortDate = bundle.getString(URLConfig.SORT_DATE);
			gridView = (RecyclerView) view.findViewById(R.id.grid_view);
			errorlayout = view.findViewById(R.id.error_layout);
			errorlayout.setVisibility(View.GONE);
			gridView.setLayoutManager(new LinearLayoutManager(mActivity));


			adapter = new NetworkJobsAdapter(mActivity, jobList);
			gridView.setAdapter(adapter);
			EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener() {

				@Override
				public void onLoadMore(int currentPage) {

					if(!TextUtils.isEmpty(nextUrl)&&!loadMore) {
						loadMore=true;
						downloadsearchresultJobs(nextUrl,view.findViewById(R.id.loading_grid), true,false);
					}
				}
			};

			gridView.addOnScrollListener(mScrollListener);

			String url= URLConfig.MATCH_JOB_CONNECT_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)) +"?konnect=1"+"&perpage="+ URLConfig.PER_PAGE+"&sort="+sortDate +"&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";


			downloadsearchresultJobs(url, LoadingCmp, false,false);
			unique_companies = new HashSet<>();


		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}




	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		try {

			inflater.inflate(R.menu.sort_multi, menu);

			final MenuItem alertMenuItem = menu.findItem(R.id.notification);
			alertMenuItem.setActionView(R.layout.notification_badge_layout);
			RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
			final TextView notification_count = (TextView) rootView.findViewById(R.id.txtCount);
			if (!TextUtils.isEmpty(ShineSharedPreferences.getNotificationCount(mActivity,
					ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"))) {
				notification_count.setVisibility(View.VISIBLE);
				notification_count.setText(ShineSharedPreferences.getNotificationCount(mActivity,
						ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"));
			} else {
				notification_count.setVisibility(View.GONE);
			}
			rootView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					notification_count.setVisibility(View.GONE);
					BaseFragment fragment = new NotificationsFrag();
					mActivity.singleInstanceReplaceFragment(fragment);
					ShineCommons.trackShineEvents("Notification","NetworkJobs",mActivity);

					ShineSharedPreferences.saveNotificationCount(mActivity, ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count", "");

				}
			});

		}catch (Exception e){
			e.printStackTrace();
		}

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()){
			case R.id.action_multi_apply:
				adapter.startActionMode(view.findViewById(R.id.multi_apply_button));
				break;

			case R.id.action_sort:

				showDialogForSorting();
				break;
		}
		return false;
	}

	public void showDialogForSorting() {
		dialog = null;
		dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setContentView(R.layout.sort_option_dialog);

		dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		btnRelevance = (RadioButton) dialog.findViewById(R.id.relevance);
		btnFreshness = (RadioButton) dialog.findViewById(R.id.freshness);

		btnRelevance.getBackground().setColorFilter(Color.parseColor("#009688"), PorterDuff.Mode.SRC_IN);
		btnFreshness.getBackground().setColorFilter(Color.parseColor("#009688"), PorterDuff.Mode.SRC_IN);



		btnFreshness.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mActivity.selected = btnFreshness.isChecked();
				mActivity.popone();
				JobsTabFrg frg = new JobsTabFrg();
				Bundle bundle = new Bundle();
				bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.NETWORK_TAB);
				bundle.putString(URLConfig.SORT_DATE, "1");
				frg.setArguments(bundle);
				mActivity.singleInstanceReplaceFragment(frg,true);
				DialogUtils.dialogDismiss(dialog);

			}
		});
		btnRelevance.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mActivity.selected = btnFreshness.isChecked();
				mActivity.popone();
				JobsTabFrg frg = new JobsTabFrg();
				Bundle bundle = new Bundle();
				bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.NETWORK_TAB);
				bundle.putString(URLConfig.SORT_DATE, "");
				frg.setArguments(bundle);
				mActivity.singleInstanceReplaceFragment(frg,true);
				DialogUtils.dialogDismiss(dialog);


			}
		});
		if (mActivity.selected) {
			btnFreshness.setChecked(true);
		} else {
			btnRelevance.setChecked(true);
		}
		if (!mActivity.isFinishing())
			dialog.show();

	}
	private void downloadsearchresultJobs(String url, View loadingView,
										  boolean flag,boolean isRefresh) {

		try {


			LoadingCmp = loadingView;
			loadMore = flag;


			if(!isRefresh)
			DialogUtils.showLoading(mActivity, getString(R.string.loading),
					LoadingCmp);


			if (loadingView.getId() == R.id.loading_cmp&&!isRefresh)
				gridView.setVisibility(View.GONE);

                Type type = new TypeToken<SimpleSearchModel>(){}.getType();

                VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,url,type);

                req.execute("JobsInNetworkJobResult");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showresult() {
		try {

			LoadingCmp.setVisibility(View.GONE);
			if (totalJobs > 0) {

				view.findViewById(R.id.grid_view).setVisibility(View.VISIBLE);
				adapter.rebuildListObject(jobList);
				errorlayout.setVisibility(View.GONE);
				gridView.setVisibility(View.VISIBLE);

				adapter.notifyDataSetChanged();

			} else {

				if (mActivity != null) {
					errorlayout.setVisibility(View.VISIBLE);
					gridView.setVisibility(View.GONE);
					view.findViewById(R.id.grid_view).setVisibility(View.GONE);

					if(JobsListRecyclerAdapter.actionMode!=null)
						JobsListRecyclerAdapter.actionMode.finish();

					if (!ShineSharedPreferences.getLinkedinImported(mActivity) ||
							!ShineSharedPreferences.getGoogleImported(mActivity)) {

						BaseFragment konnectFrag = KonnectFrag.newInstance(2);

						DialogUtils.showErrorActionableMessage(mActivity,errorlayout,
								mActivity.getString(R.string.no_jobs_with_connection_msg),
								mActivity.getString(R.string.no_jobs_with_connection_msg2),
								mActivity.getString(R.string.sync_now),
								DialogUtils.ERR_NO_JOBS_WITH_CONNECTION,konnectFrag);
					}
					else {
						DialogUtils.showErrorActionableMessage(mActivity,errorlayout,
								mActivity.getString(R.string.no_jobs_with_connection_msg),
								mActivity.getString(R.string.try_later),
								"",
								DialogUtils.ERR_NO_JOBS_WITH_CONNECTION,null);


					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}





	private void callGetFriend() {

		GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
		friends.getFriendsInCompanyVolleyRequest(co_names);
	}

	private void invalidateWithFriendsData() {
		for (int m = 0; m < jobList.size(); m++) {
			SimpleSearchModel.Result job = jobList.get(m);
			if (job.comp_uid_list != null) {
				for (int n = 0; n < job.comp_uid_list.size(); n++) {
					if (friend_list.containsKey(job.comp_uid_list.get(n))) {
						job.frnd_model = (friend_list.get(job.comp_uid_list.get(n)));
					}
				}
			}
		}
		adapter.notifyDataSetChanged();
		gridView.invalidate();
	}

	@Override
	public void getFriends(HashMap<String, DiscoverModel.Company> result) {
		if(!isAdded())
			return;
		friend_list = result;
		invalidateWithFriendsData();
	}

	@Override
	public void OnDataResponse(Object object, String tag) {

		try {

			Log.d("we are here", object.toString() + "");


            if(jobList==null||!loadMore)
                jobList=new ArrayList<>();

            swipeContainer.setRefreshing(false);

            SimpleSearchModel model = (SimpleSearchModel) object;

			nextUrl = model.next;

            for(SimpleSearchModel.Result res:model.results)
            {
                int len = res.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += res.job_loc_list.get(j);
                    } else {
                        loc += res.job_loc_list.get(j) + " / ";
                    }
                }
                res.job_loc_str = loc;
            }

            co_names = "";
            totalJobs = model.count;
            jobList.addAll(model.results);

						adapter.notifyDataSetChanged();
						loadMore = false;
						
						co_names="";
						if(jobList.size() >0)
						{
							for (int i = (noofhits-1)* URLConfig.PER_PAGE; (i <jobList.size()) ; i++) {
								co_names=ShineCommons.getCompanyNames(jobList.get(i).comp_uid_list, unique_companies);
							}
						}
						if (!co_names.equals(""))
							callGetFriend();
						showresult();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void OnDataResponseError(final String error, String tag) {

		try {
            swipeContainer.setRefreshing(false);
			errorlayout.setVisibility(View.VISIBLE);
			gridView.setVisibility(View.GONE);
			mActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					LoadingCmp.setVisibility(View.GONE);
					DialogUtils.showErrorActionableMessage(mActivity,errorlayout,
							mActivity.getString(R.string.technical_error),
							mActivity.getString(R.string.technical_error2),
							"",
							DialogUtils.ERR_TECHNICAL,null);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

    @Override
    public void onRefresh() {
        try {

			errorlayout.setVisibility(View.GONE);
			view.findViewById(R.id.grid_view).setVisibility(View.VISIBLE);
			String url= URLConfig.MATCH_JOB_CONNECT_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)) +"?konnect=1"+"&perpage="+ URLConfig.PER_PAGE+"&sort="+sortDate +"&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
			downloadsearchresultJobs(url, view.findViewById(R.id.loading_cmp),
                    false,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
