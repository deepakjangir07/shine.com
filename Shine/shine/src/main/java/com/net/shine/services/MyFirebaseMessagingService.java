package com.net.shine.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.webkit.URLUtil;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.moengage.push.PushManager;
import com.moengage.pushbase.push.MoEngageNotificationUtils;
import com.net.shine.R;
import com.net.shine.activity.SplashScreen;
import com.net.shine.config.URLConfig;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.Map;


/**
 * Created by manishpoddar on 12/10/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static  int APP_ID = 0;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        int id = (int) System.currentTimeMillis();
        APP_ID=id;


        Log.d("FCM", "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("FCM", "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("FCM", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        try {
            // TODO Ujjawal please check the value of isEmailResumed.
            Bundle bundle = new Bundle();
            for(Map.Entry<String, String> entry: remoteMessage.getData().entrySet()){
                bundle.putString(entry.getKey(), entry.getValue());
            }

            if (MoEngageNotificationUtils.isFromMoEngagePlatform(bundle)){
                Log.d("inside Mode",bundle.toString());
                bundle.putString(URLConfig.FROM_NOTIFICATION,"true");
                bundle.putString("from_moengage","true");
                PushManager.getInstance().getPushHandler().handlePushPayload(getApplicationContext(), bundle);

            }
            else
            {

                showNotification(bundle);
                Log.d("inside", "gcm-in-Shine");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotification(Bundle bundle) {
        try {
            CharSequence message = bundle.getString("message");
            String type = bundle.getString("type");

            Log.d("Debug::GCM", "New GCM Recived: " + message + " " + type);

            if(type == null)
            {
                Log.e("DEBUG::NOTI", "type not present in notification bundle");
                return;
            }

            NotificationCompat.Builder mBuilder;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mBuilder = new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_noti)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle(getString(R.string.app_name))
                                .bigText(message));
            }
            else
            {

                mBuilder = new NotificationCompat.Builder(getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle(getString(R.string.app_name))
                                .bigText(message));

            }

            mBuilder.setPriority(1);
            mBuilder.setAutoCancel(true);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(getApplicationContext());
            Intent notificationIntent;


            //TODO - Ujjawal - type is null sometimes which throws exception
            System.out.println("----type"+type);
            if (type.equals(URLConfig.MATCHED_JOBS_NOTIFI)
                    || type.equals(URLConfig.MY_PROFILE_NOTIFI)
                    || type.equals(URLConfig.MY_PROFILE_VIEW_NOTIFI)
                    || type.equals(URLConfig.JOB_DETAILS_NOTIFI)
                    || type.equals(URLConfig.MAIL_NOTIFY)
                    || type.equals(URLConfig.DISCOVER_NOTIFY)
                    || type.equals(URLConfig.JOB_ALERT_NOTIFI)
                    || type.equals(URLConfig.SEARCH_SIMPLE_NOTIFY)
                    || type.equals(URLConfig.FEEDBACK_NOTIFI)

                    //11-12 left
                    || type.equals(URLConfig.REFERRAL_RECEIVED_NOTIFI)
                    || type.equals(URLConfig.REFERRAL_REQUEST_NOTIFI)

                    //15-21 left
                    || type.equals(URLConfig.INBOX_JOB_ALERT_NOTIFY)
                    || type.equals(URLConfig.APPLIED_JOBS_NOTIFI)

                    //24-29 left
                    || type.equals(URLConfig.APP_UPDATE)
                    || type.equals(URLConfig.APP_INVITE)

                    //32-33 left
                    || type.equals(URLConfig.MY_CONNECTION_JOB)
                    || type.equals(URLConfig.EDUCATION_ADD_NOTIFI)
                    || type.equals(URLConfig.EMPLOYMENT_ADD_NOTIFI)
                    || type.equals(URLConfig.EXPERINCE_NOTIFI)
                    || type.equals(URLConfig.CERTIFICATION_NOTIFI)
                    || type.equals(URLConfig.DESIRED_JOB_DETAILS_NOTIFI)
                    || type.equals(URLConfig.SKILL_NOTIFI)
                    || type.equals(URLConfig.SAVED_JOBS_NOTIFI)
                    || type.equals(URLConfig.LOGIN_NOTIFI)
                    || type.equals(URLConfig.REGISTER_NOTIFI)
                    || type.equals(URLConfig.RESUME_NOTIFY)

                    )

            {

                System.out.println("----type"+type);
                Log.d("getting", "Notification type --");
                notificationIntent = new Intent(getApplicationContext(),
                        SplashScreen.class);
                notificationIntent.putExtra(URLConfig.FROM_NOTIFICATION, "true");
                notificationIntent.putExtra(URLConfig.NOTIFICATION_TYPE, type);
                notificationIntent.putExtras(bundle);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }else {
                String url = bundle.getString("url");

                if(!URLUtil.isValidUrl(url))
                    return;

                notificationIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(url));

                stackBuilder.addNextIntent(notificationIntent);

                PendingIntent resultPendingIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(APP_ID, mBuilder.build());
                return;

            }
            if (userProfileVO != null ) {

                String userId = bundle.getString(URLConfig.MAIL_NOTIFY_USER_ID,"");

                if(!userProfileVO.candidate_id.equals(userId) && !userId.isEmpty())
                    return;

                switch (type) {

                    case URLConfig.MAIL_NOTIFY:


                        String objId = bundle.getString(URLConfig.MAIL_NOTIFY_OBJ_ID);
                        String mailId = bundle.getString(URLConfig.MAIL_NOTIFY_MAIL_ID);
                        notificationIntent.putExtra(URLConfig.MAIL_NOTIFY_OBJ_ID, objId);
                        notificationIntent.putExtra(URLConfig.MAIL_NOTIFY_MAIL_ID, mailId);
                        break;

                    case URLConfig.SEARCH_SIMPLE_NOTIFY:
                        String search_key=bundle.getString("search_key");
                        String search_loc=bundle.getString("search_loc");
                        notificationIntent.putExtra("search_key",search_key);
                        notificationIntent.putExtra("search_loc",search_loc);
                        break;

                    case URLConfig.INBOX_JOB_ALERT_NOTIFY:


                        String alertID = bundle.getString(URLConfig.INBOX_JOB_ALERT_ID);
                        notificationIntent.putExtra(URLConfig.INBOX_JOB_ALERT_ID, alertID);
                        break;


                    case URLConfig.JOB_DETAILS_NOTIFI: {
                        String jobId = bundle.getString(URLConfig.JOB_ID_FROM_EMAIL_LINK);
                        notificationIntent.putExtra(URLConfig.JOB_ID_FROM_EMAIL_LINK, jobId);
                        break;
                    }

                    case URLConfig.APP_UPDATE:
                        int appVersion = Integer.parseInt(bundle.getString(URLConfig.VERSION_CODE,
                                ShineCommons.getAppVersionCode(getApplicationContext())+""));
                        Log.d("appVersion",appVersion+"");
                        if (appVersion<=ShineCommons.getAppVersionCode(getApplicationContext())){

                            Log.d("currentversion",ShineCommons.getAppVersionCode(getApplicationContext())+"");


                            return;
                        }
                        else
                        {
                            Log.d("in gcm else","url");
                            String url = bundle.getString("url");

                            if(!URLUtil.isValidUrl(url))
                            {

                                url = "market://details?id=com.net.shine";
                                Log.d("URL--gcm", url);
                            }

                            notificationIntent = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(url));
                        }
                        break;


                    case URLConfig.REGISTER_COMPLETE_NOTIFY: {
                        if(userProfileVO.is_mid_out != 1){
                            return;
                        }
                    }
                }


                stackBuilder.addNextIntent(notificationIntent);

                PendingIntent resultPendingIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(APP_ID, mBuilder.build());

            }
            else{
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                stackBuilder.addNextIntent(notificationIntent);

                PendingIntent resultPendingIntent = stackBuilder
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(APP_ID, mBuilder.build());
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
