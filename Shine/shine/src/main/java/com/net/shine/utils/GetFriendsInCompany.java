package com.net.shine.utils;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class GetFriendsInCompany implements VolleyRequestCompleteListener {

	private Context context;
	private GetConnectionListener listener;
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();

    public GetFriendsInCompany(Context context, GetConnectionListener listener){
		this.context = context;
		this.listener = listener;
	}




    public void getFriendsInCompanyVolleyRequest(String companies)
    {

        String[] comp = companies.split("#");

        String secondPartUrl = "?shine_id=" + ShineSharedPreferences.getCandidateId(context) ;

        for (String aComp : comp) {
            secondPartUrl = secondPartUrl + "&companies=" + aComp;
        }

        Type type = new TypeToken<DiscoverModel>(){}.getType();

        VolleyNetworkRequest req = new VolleyNetworkRequest(context, this,
                URLConfig.KONNECT_CONNECTIONS_URL+secondPartUrl, type);
        req.execute("GetFriendsInCompany");

    }

    @Override
    public void OnDataResponse(Object object, String tag) {
        friend_list = new HashMap<>();
        try {
            DiscoverModel model = (DiscoverModel) object;
            if(model.total_count>0) {
                ArrayList<DiscoverModel.Company> list = model.results;
                for (DiscoverModel.Company comp : list) {
                    friend_list.put(comp.data.company_uid, comp);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        try {
            if(listener!=null)
                listener.getFriends(friend_list);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        friend_list = new HashMap<>();
        if(listener!=null)
            listener.getFriends(friend_list);
    }

}
