package com.net.shine.utils.auth;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;

public class LinkedinConstants {
    //	public static final String API_KEY = "753920c2cua2ms";
    public static final String API_KEY = "757gbstpwa6dqp";
    //	public static final String SECRET_KEY = "wc5dO1LPiNSeBLnn";
    public static final String SECRET_KEY = "creqezZ0kPJnJWRk";

    //This is any string we want to use. This will be used for avoid CSRF attacks. You can generate one here: http://strongpasswordgenerator.com/
    public static final String STATE = "E3ZYKC1T6H2yP4z";
    public static final String REDIRECT_URI = "https://www.shine.com";

    private static final String SCOPES = "r_contactinfo%20r_network%20w_messages%20r_fullprofile%20r_emailaddress%20r_basicprofile";
    /*********************************************/

    public static final String AUTHORIZATION_URL = "https://www.linkedin.com/oauth/v2/authorization";
    public static final String ACCESS_TOKEN_URL = "https://www.linkedin.com/oauth/v2/accessToken";
    public static final String SECRET_KEY_PARAM = "client_secret";
    public static final String RESPONSE_TYPE_PARAM = "response_type";
    public static final String GRANT_TYPE_PARAM = "grant_type";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String RESPONSE_TYPE_VALUE = "code";
    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String SCOPE_PARAM = "scope";
    public static final String STATE_PARAM = "state";
    public static final String REDIRECT_URI_PARAM = "redirect_uri";
    public static final String OAUTH_ACCESS_TOKEN_PARAM = "oauth2_access_token";

    private static final String CONNECTION_URL = "https://api.linkedin.com/v1/people/~/connections:(id,public-profile-url,pictureUrl,firstName,lastName,positions:(company:(name),startDate,endDate))";

    //TODO: Add the following fields, if in future we start getting profile ourselves instead of backend.
    //EXPECTED_FIELDS_FROM_LINKEDIN = ['id', 'first-name', 'last-name', 'headline', 'location', 'industry', 'current-share', 'summary', 'specialties', 'positions', 'picture-urls', 'public-profile-url', 'last-modified-timestamp', 'proposal-comments', 'associations', 'interests', 'publications', 'patents', 'languages', 'skills', 'certifications', 'educations', 'courses', 'volunteer', 'three-current-positions', 'three-past-positions', 'following', 'date-of-birth', 'honors-awards', 'email-address', 'phone-numbers']
//    private static final String PROFILE_URL = "https://api.linkedin.com/v1/people/~:(educations,picture-url,positions)?format=json";
    private static final String PROFILE_URL = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,phone-numbers,email-address,picture-url)?format=json";
    private static final String SEND_MESSAGE_URL = "https://api.linkedin.com/v1/people/~/mailbox";

    /*---------------------------------------*/
    public static final String QUESTION_MARK = "?";
    public static final String AMPERSAND = "&";
    public static final String EQUALS = "=";


    public static String getAuthorizationUrl() {
        return AUTHORIZATION_URL
                + QUESTION_MARK + RESPONSE_TYPE_PARAM + EQUALS + RESPONSE_TYPE_VALUE
                + AMPERSAND + CLIENT_ID_PARAM + EQUALS + API_KEY
                + AMPERSAND + SCOPE_PARAM + EQUALS + SCOPES
                + AMPERSAND + STATE_PARAM + EQUALS + STATE
                + AMPERSAND + REDIRECT_URI_PARAM + EQUALS + REDIRECT_URI;
    }

    /**
     * Method that generates the url for get the access token from the Service
     *
     * @return Url
     */
    public static String getAccessTokenUrl(String authorizationToken) {
        return ACCESS_TOKEN_URL
                + QUESTION_MARK
                + GRANT_TYPE_PARAM + EQUALS + GRANT_TYPE
                + AMPERSAND
                + RESPONSE_TYPE_VALUE + EQUALS + authorizationToken
                + AMPERSAND
                + CLIENT_ID_PARAM + EQUALS + API_KEY
                + AMPERSAND
                + REDIRECT_URI_PARAM + EQUALS + REDIRECT_URI
                + AMPERSAND
                + SECRET_KEY_PARAM + EQUALS + SECRET_KEY;
    }

    public static final int CONNECTION_PAGING_COUNT = 500;

    public static String getConnectionsUrl(String accessToken, int start, int count) {
        return CONNECTION_URL
                + '?'
                + "start=" + start
                + '&'
                + "count=" + count
                + '&'
                + OAUTH_ACCESS_TOKEN_PARAM + EQUALS + accessToken + "&format=json";

    }

    public static String getConnectionsUrl(String accessToken, int start) {

        return CONNECTION_URL
                + '?'
                + "start=" + start
                + '&'
                + "count=" + CONNECTION_PAGING_COUNT
                + '&'
                + OAUTH_ACCESS_TOKEN_PARAM + EQUALS + accessToken + "&format=json";

    }

    public static String getProfileUrl(String accessToken) {
        return PROFILE_URL
                + '&'
                + OAUTH_ACCESS_TOKEN_PARAM + EQUALS + accessToken + "&format=json";

    }


    public static String getSendMessageUrl(String accessToken) {
        return SEND_MESSAGE_URL
                + QUESTION_MARK
                + OAUTH_ACCESS_TOKEN_PARAM + EQUALS + accessToken;
    }

    public static JSONObject parseLinkedin(JSONObject data) throws JSONException {

        JSONObject main = new JSONObject();
        main.put("co", 0);
        main.put("t", 0);
        main.put("s", 0);
        if (data.has("_total")) {
            main.put("t", data.getInt("_total"));
            main.put("co", data.getInt("_total"));
        }
        if (data.has("_count")) {
            main.put("co", data.getInt("_count"));
        }
        if (data.has("_start")) {
            main.put("s", data.getInt("_start"));
        }

        if (data.has("values")) {
            JSONArray data_array = data.getJSONArray("values");
            JSONArray data_array_short = new JSONArray();

            for (int i = 0; i < data_array.length(); i++) {
                JSONObject connection_detail = data_array.getJSONObject(i);
                JSONObject connection_detail_short = new JSONObject();
                if (connection_detail.has("firstName"))
                    connection_detail_short.put("f", connection_detail.getString("firstName"));

                if (connection_detail.has("id"))
                    connection_detail_short.put("id", connection_detail.getString("id"));

                if (connection_detail.has("lastName"))
                    connection_detail_short.put("l", connection_detail.getString("lastName"));

                if (connection_detail.has("pictureUrl"))
                    connection_detail_short.put("i", connection_detail.getString("pictureUrl"));

                if (connection_detail.has("publicProfileUrl"))
                    connection_detail_short.put("pp", connection_detail.getString("publicProfileUrl"));

                if (connection_detail.has("positions")) {
                    JSONObject comp_header = connection_detail.getJSONObject("positions");
                    JSONObject comp_header_short = new JSONObject();

                    if (comp_header.has("values")) {
                        JSONArray company_detail = comp_header.getJSONArray("values");

                        JSONArray company_detail_short = new JSONArray();

                        for (int j = 0; j < company_detail.length(); j++) {
                            JSONObject temp = company_detail.getJSONObject(j);
                            JSONObject position_detail = temp.getJSONObject("company");
                            JSONObject temp_short = new JSONObject();
                            JSONObject position_detail_short = new JSONObject();

                            if (position_detail.has("name"))
                                position_detail_short.put("n", position_detail.getString("name"));

                            if (temp.has("startDate"))
                                temp_short.put("startdate", temp.getJSONObject("startDate"));

                            if (temp.has("endDate"))
                                temp_short.put("enddate", temp.getJSONObject("endDate"));

                            if (position_detail.has("id"))
                                position_detail_short.put("id", position_detail.getLong("id"));

                            temp_short.put("c", position_detail_short);
                            company_detail_short.put(j, temp_short);
                        }
                        comp_header_short.put("t", comp_header.getInt("_total"));
                        comp_header_short.put("v", company_detail_short);
                        connection_detail_short.put("p", comp_header_short);
                    }
                }

                data_array_short.put(i, connection_detail_short);

            }
            main.put("v", data_array_short);
        }

        //Log.e("Final Linkedin Shortened Array",main.toString());
        return main;
    }

    public static Dialog getFethingProfileDialog(Activity mActivity){
        Dialog customDialog = getSyncingDialog(mActivity);
        customDialog.findViewById(R.id.textView2).setVisibility(View.GONE);
        customDialog.findViewById(R.id.textView3).setVisibility(View.GONE);
        customDialog.findViewById(R.id.continue_button).setVisibility(View.GONE);
        TextView tv = (TextView) customDialog.findViewById(R.id.textView1);
        tv.setText("Fetching Profile...");
        return customDialog;

    }

    public static Dialog getSyncingDialog(Activity mActivity){
        final Dialog customDialog = new Dialog(mActivity);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(R.layout.cus_import_contact_dialog);
        customDialog.setCancelable(false);
        ImageView imageView = (ImageView) customDialog
                .findViewById(R.id.sync_icon);
        ProgressBar bar = (ProgressBar) customDialog
                .findViewById(R.id.syncBar);
        bar.setIndeterminate(true);
        imageView.setBackgroundResource(R.drawable.ic_linkedin);
        customDialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.setTitle("");
        return customDialog;
    }


    @SuppressLint("SetJavaScriptEnabled")
    public static void LinkedInConnect(final Activity mActivity, final LinkedInCallBack mCallback, final boolean shouldCache, final Dialog customDialog) {

        final Dialog auth_dialog = new Dialog(mActivity,
                android.R.style.Theme_Translucent_NoTitleBar);
        auth_dialog.setContentView(R.layout.auth_dialog);
        final WebView web = (WebView) auth_dialog.findViewById(R.id.webv);
        auth_dialog.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        String authUrl = LinkedinConstants.getAuthorizationUrl();
        web.loadUrl(authUrl);
        web.getSettings().setJavaScriptEnabled(true);

        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                auth_dialog.findViewById(R.id.progress_bar).setVisibility(
                        View.GONE);
                web.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String authorizationUrl) {

                if (authorizationUrl.startsWith(REDIRECT_URI)) {
                    Uri uri = Uri.parse(authorizationUrl);
                    Log.d("LINKED IN AUTH RESPONSE", authorizationUrl);
                    String error = uri.getQueryParameter("error");
                    Log.i("DEBUG Friends", "error received: " + error);
                    if (error != null) {

                        if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                            Toast.makeText(
                                    mActivity,
                                    "LinkedIn sync pending",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(
                                    mActivity,
                                    "Some Error Occurred. Please try again or use another source.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        auth_dialog.dismiss();
                        if(mCallback!=null)
                            mCallback.onLinkedInFailed();
                        return true;
                    }
                    final String stateToken = uri
                            .getQueryParameter(STATE_PARAM);
                    Log.i("DEBUG Friends", "stateToken received: " + stateToken);
                    if (stateToken == null
                            || !stateToken.equals(STATE)) {
                        Toast.makeText(mActivity,
                                "Sorry..state token doesn't match",
                                Toast.LENGTH_SHORT).show();
                        if(mCallback!=null)
                            mCallback.onLinkedInFailed();
                        return true;
                    }
                    String authorizationToken = uri
                            .getQueryParameter(RESPONSE_TYPE_VALUE);
                    Log.i("DEBUG Friends", "Auth token received: " + authorizationToken);
                    if (authorizationToken == null) {
                        Toast.makeText(mActivity,
                                "The user doesn't allow authorization.",
                                Toast.LENGTH_SHORT).show();
                        if(mCallback!=null)
                            mCallback.onLinkedInFailed();
                        return true;
                    }
                    String accessTokenUrl = getAccessTokenUrl(authorizationToken);
                    new AsyncTask<String, Void, String[]>() {

                        boolean cancel_sync_dialog = false;

                        @Override
                        protected void onPreExecute() {

                            if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                                MyApplication.isLinkedInSyncing = true;
                            }

                            if(customDialog!=null&&customDialog.findViewById(R.id.close_btn)!=null){
                                customDialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cancel_sync_dialog = true;
                                        customDialog.dismiss();
                                    }
                                });
                            }

                            DialogUtils.showDialog(customDialog);

                            auth_dialog.dismiss();
                        }

                        @Override
                        protected String[] doInBackground(String... params) {
                            String url = params[0];
                            try {
                                StringBuilder content = new StringBuilder();
                                URL mUrl = new URL(url);
                                HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                                if (connection.getResponseCode() == 200) {
                                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                    String line;
                                    while ((line = bufferedReader.readLine()) != null)
                                        content.append(line).append("\n");
                                    bufferedReader.close();
                                    JSONObject result = new JSONObject(content.toString());
                                    int expiresIn = result.has("expires_in") ? result
                                            .getInt("expires_in") : 0;
                                    String accessToken = result.has("access_token") ? result
                                            .getString("access_token") : null;
                                    if (expiresIn > 0 && accessToken != null) {
                                        Log.i("DEBUG", "This is the access Token: "
                                                + accessToken + ". It will expire in "
                                                + expiresIn + " secs");
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.add(Calendar.SECOND, expiresIn);
                                        long expireDate = calendar.getTimeInMillis();
                                        if (ShineSharedPreferences.getUserInfo(mActivity) != null || shouldCache) {
                                            ShineSharedPreferences.setLinkedinAccessExpiry(expireDate+"", mActivity);
                                            ShineSharedPreferences.setLinkedinAccessToken(accessToken, mActivity);
                                        }
                                        String []str = new String[2];
                                        str[0] = accessToken;
                                        str[1] = expireDate+"";
                                        return str;
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(String []accessToken) {
                            Log.d("LINKEDIN ", "on post");
                            if (cancel_sync_dialog)
                                return;
                            if(accessToken!=null && accessToken.length==2){
                                mCallback.onLinkedInConnected(accessToken[0], accessToken[1], customDialog);
                            }
                            else{
                                mCallback.onLinkedInConnected(null, null, customDialog);
                            }
                        }
                    }.execute(accessTokenUrl);

                } else {
                    Log.i("Authorize", "Redirecting to: " + authorizationUrl);
                    web.loadUrl(authorizationUrl);
                }
                return true;
            }
        });
        auth_dialog.setTitle("LinkedIn Authentication");
        auth_dialog.show();
    }



    public static void FetchAndDumpConnectionsToServer(final Activity mActivity, String accessToken,
                                                       final LinkedInCallBack mCallBack, final Dialog myCustomDialog) {

        new AsyncTask<String, Void, JSONObject>() {

            Dialog customDialog;
            boolean cancel_sync_dialog = false;



            @Override
            protected void onPreExecute() {

                if (myCustomDialog == null) {
                    customDialog = new Dialog(mActivity);
                    customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    customDialog.setContentView(R.layout.cus_import_contact_dialog);
                    customDialog.setCancelable(false);
                    ImageView imageView = (ImageView) customDialog
                            .findViewById(R.id.sync_icon);
                    ProgressBar bar = (ProgressBar) customDialog
                            .findViewById(R.id.syncBar);
                    bar.setIndeterminate(true);

                    imageView.setBackgroundResource(R.drawable.ic_linkedin);

                    customDialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancel_sync_dialog = true;
                            customDialog.dismiss();
                        }
                    });

                    customDialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog.setTitle("");
                    customDialog.show();
                } else {
                    customDialog = myCustomDialog;
                    customDialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancel_sync_dialog = true;
                            customDialog.dismiss();
                        }
                    });

                    customDialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            customDialog.dismiss();
                        }
                    });
                }

            }

            boolean isRetry = false;

            @Override
            protected JSONObject doInBackground(String... params) {


                if (params.length > 0) {
                    String accessToken = params[0];

                    try {
                        Log.d("DEBUG_LINKEDIN", "Start Hitting Connection API");
                        JSONArray values = new JSONArray();
                        int total = 1000;
                        int count = 0;
                        while (total - count > 0) {
                            StringBuilder content = new StringBuilder();
                            URL mUrl = new URL(getConnectionsUrl(accessToken, count, CONNECTION_PAGING_COUNT));

                            Log.d("DEBUG::LINEDIN", mUrl.toString() + "   url");
                            Log.d("DEBUG_LINKEDIN", "Hitting Connection API: start=" + count + " " + mUrl);

                            HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                            connection.setRequestProperty("x-li-format", "json");

                            System.out.println("-- connection res code" + connection.getResponseCode());
                            if (connection.getResponseCode() == 200) {
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                String line;
                                while ((line = bufferedReader.readLine()) != null)
                                    content.append(line).append("\n");
                                bufferedReader.close();
                                connection.disconnect();
                                JSONObject ans = new JSONObject(content.toString());
                                if (ans.has("_total"))    //update total from its default 1000
                                {
                                    total = ans.getInt("_total");
                                }
                                if (!ans.has("values")) {
                                    break;    //we have reached the end
                                }
                                JSONArray nVal = ans.getJSONArray("values");
                                if (nVal.length() == 0) {
                                    break;    //we have reached the end
                                }
                                count = count + nVal.length();
                                for (int i = 0; i < nVal.length(); i++) {
                                    values.put(nVal.get(i));
                                }
                                Log.d("DEBUG::LINEDIN", nVal.length() + " " + CONNECTION_PAGING_COUNT);
//								if(nVal.length()<CONNECTION_PAGING_COUNT)	//we received less results then demanded, so end has been reached
//								{
//									break;
//								}
                            } else if (connection.getResponseCode() == 401) {
                                isRetry = true;
                                Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                                return null;    //return null as we should discard partial data, if any.
                            } else {
                                Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                                break;
                            }
                        }
                        JSONObject object = new JSONObject();
                        object.put("_total", total);
                        object.put("_start", 0);
                        object.put("_count", count);
                        object.put("values", values);
                        return parseLinkedin(object);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject data) {
                if (cancel_sync_dialog)
                    return;
                if (data != null) {
                        Log.d("DEBUG::LINKEDIN_CONN", "Linedin Received....Dumping to SHine");
                        dumpConnectionsToServer(data, mCallBack, customDialog, mActivity);
                } else if (isRetry) {



                    System.out.println("--i am in isretry");
                    if(ShineSharedPreferences.getUserInfo(mActivity)==null)
                    {
                        ShineSharedPreferences.setLinkedinProfile(null, mActivity);
                        Toast.makeText(
                                mActivity,
                                "Please login to apply",
                                Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        MyApplication.isLinkedInSyncing = false;
                        ShineSharedPreferences.setLinkedinAccessToken("", mActivity);
                        ShineSharedPreferences.setLinkedinAccessExpiry("0", mActivity);
                        Toast.makeText(
                                mActivity,
                                "Please login to sync your connections",
                                Toast.LENGTH_SHORT).show();
                    }
                    LinkedInConnect(mActivity, mCallBack, true, customDialog);
                    DialogUtils.dialogDismiss(customDialog);


                } else {

                    if(ShineSharedPreferences.getUserInfo(mActivity)!=null)
                    {
                        MyApplication.isLinkedInSyncing = false;
                    }

                    DialogUtils.dialogDismiss(customDialog);
                    mCallBack.onLinkedInFailed();
                }
            }
        }.execute(accessToken);
    }


    private static void dumpConnectionsToServer(JSONObject json, final LinkedInCallBack mCallBack, final Dialog customDialog, final Activity mActivity) {


        try {
            JSONObject data = parseLinkedin(json);
            HashMap<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("shine_id", ShineSharedPreferences.getCandidateId(mActivity));
            paramsMap.put("source", "linkedin");
            paramsMap.put("data", data);
            paramsMap.put("access_token", ShineSharedPreferences.getLinkedinAccessToken(mActivity));
//            paramsMap.put("linkedin_token_expiry", ShineSharedPreferences.getLinkedinAccessExpiry(mContext)+"");

            Log.d("DEBUG_LINKEDIN", "Dumping data to shine");

            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    DialogUtils.dialogDismiss(customDialog);
                    mCallBack.onLinkedInSuccess();
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    DialogUtils.dialogDismiss(customDialog);
                    mCallBack.onLinkedInFailed();
                }
            }, URLConfig.KONNECT_LINKEDIN_URL, type);
            request.setUsePostMethod(paramsMap);
            request.execute("DumpLinkedInData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void FetchAndStoreUserProfile(final Activity mActivity, String accessToken,
                                                       final LinkedInCallBack mCallBack, final Dialog myCustomDialog) {
        new AsyncTask<String, Void, ApplywithoutResumeModel>() {
            Dialog customDialog;
            boolean cancel_sync_dialog = false;
            boolean isRetry = false;

            @Override
            protected void onPreExecute() {

                if (myCustomDialog == null) {
                    customDialog = LinkedinConstants.getFethingProfileDialog(mActivity);
                } else {
                    customDialog = myCustomDialog;
                }

                customDialog.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cancel_sync_dialog = true;
                        customDialog.dismiss();
                    }
                });

                customDialog.findViewById(R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });


                if(!customDialog.isShowing())
                    customDialog.show();

            }

            @Override
            protected ApplywithoutResumeModel doInBackground(String... params) {

                if (params.length > 0) {
                    String accessToken = params[0];

                    try {
                        Log.d("DEBUG_LINKEDIN", "Start Hitting Connection API");



                            StringBuilder content = new StringBuilder();
                            URL mUrl = new URL(getProfileUrl(accessToken));

                            Log.d("DEBUG::LINEDIN", mUrl.toString() + "   url");
                            Log.d("DEBUG_LINKEDIN", "Hitting Profile API: " + mUrl);

                            HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                            connection.setRequestProperty("x-li-format", "json");

                            System.out.println("-- connection res code" + connection.getResponseCode());
                            if (connection.getResponseCode() == 200) {
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                String line;
                                while ((line = bufferedReader.readLine()) != null)
                                    content.append(line).append("\n");
                                bufferedReader.close();
                                connection.disconnect();
                                JSONObject ans = new JSONObject(content.toString());
                                ApplywithoutResumeModel model = ShineSharedPreferences.getLinkedProfile(mActivity);
                                if(model==null)
                                    model = new ApplywithoutResumeModel();
                                model.name = ans.optString("firstName", "") + " " + ans.optString("lastName", "");
                                model.email = ans.optString("emailAddress", "");
                                model.user_id = ans.optString("id", "");
                                String url = ans.optString("pictureUrl", "");
                                if(URLUtil.isValidUrl(url))
                                {
                                   model.photo = url;
                                }

                                JSONObject phoneNumbers = ans.optJSONObject("phoneNumbers");
                                if(phoneNumbers!=null && phoneNumbers.optInt("_total", 0) > 0)
                                {
                                    JSONArray array = phoneNumbers.optJSONArray("values");
                                    if(array!=null && array.length()!=0)
                                    {
                                        for(int i=0;i<array.length();i++)
                                        {
                                            JSONObject phone = (JSONObject) array.get(i);
                                            if(phone.optString("phoneType", "").equals("work"))
                                            {
                                                model.mobile = phone.optString("phoneNumber", "");
                                                break;
                                            }
                                        }
                                        if(model.mobile==null || model.mobile.isEmpty())
                                        {
                                            JSONObject phone = (JSONObject) array.get(0);
                                            model.mobile = phone.optString("phoneNumber", "");
                                        }
                                    }
                                }
//                                model.mobile = ans.optString("", "");

                                Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives response: " + ans);
                                return model;
                            } else if (connection.getResponseCode() == 401) {
                                isRetry = true;
                                Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                            } else {
                                Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());

                            }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(ApplywithoutResumeModel data) {
                if (cancel_sync_dialog)
                    return;
                if (data != null) {
                    ShineSharedPreferences.setLinkedinProfile(data, mActivity);
                    DialogUtils.dialogDismiss(customDialog);
                    mCallBack.onLinkedInSuccess();
                } else if (isRetry) {

                    ShineSharedPreferences.setLinkedinProfile(null, mActivity);
                    LinkedInConnect(mActivity, mCallBack, false, customDialog);
                    DialogUtils.hideDialog(customDialog);
                    Toast.makeText(
                            mActivity,
                            "Please login to apply",
                            Toast.LENGTH_SHORT).show();

                } else {
                    DialogUtils.dialogDismiss(customDialog);
                    mCallBack.onLinkedInFailed();
                }
            }

        }.execute(accessToken);

    }

}
