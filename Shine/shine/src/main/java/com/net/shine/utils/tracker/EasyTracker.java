package com.net.shine.utils.tracker;

/**
 * Created by ht2 on 6/4/2015.
 */

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by android on 1/20/2015.
 */
public class EasyTracker {
    private static EasyTracker tracker;
    private  Activity   context ;
    private  Context   mContext ;
    private static final String PROPERTY_ID = "UA-3537905-39";



    public static EasyTracker getTracker() {
        return getInstance();
    }

    public static EasyTracker getInstance() {
        if(tracker == null)
            tracker = new EasyTracker();
        return tracker;
    }
    public void setContext(Context context) {
        this.mContext =  context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    synchronized public  Tracker getGoogleTracker( ) {
        GoogleAnalytics analytics;
        if(context!=null) {
            analytics = GoogleAnalytics.getInstance(context);
        }else{
            analytics = GoogleAnalytics.getInstance(mContext);
        }
        analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        Tracker t =  analytics.newTracker(PROPERTY_ID) ;
        t.enableAdvertisingIdCollection(true);
        t.enableExceptionReporting(true);


        return t;
    }

    public void sendEvent(String category, String event, String label, long l) {
        // Get tracker.
        Tracker t = getGoogleTracker();

// Set screen name.
        // t.setScreenName(screenName);

// Send a screen view.
//        t.send(new HitBuilders.AppViewBuilder().build());


            t.send(new HitBuilders.EventBuilder().setCategory(category)
                    .setAction(event)
                    .setLabel(label)
                    .setValue(l)
                    .build());



    }

    public void sendView(String screenName) {
        Tracker t = getGoogleTracker( );
        t.setScreenName(screenName);
        t.send( new HitBuilders.ScreenViewBuilder().build());

    }



    public void sendCustomDimensions(int id,String custom_dimension_string,String Screen_name)
    {
        Tracker t =getGoogleTracker();
        t.setScreenName(Screen_name);
        t.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(id, custom_dimension_string).build());
    }

    public void activityStart(Activity activity) {
        try {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            analytics.reportActivityStart(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void activityStop(Activity activity) {
        try {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
            analytics.reportActivityStop(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCampaign(String path) {
        // Get tracker.
        Tracker t = getGoogleTracker();
// Set screen name.
//        t.setScreenName(screenName);

// In this example, campaign information is set using
// a url string with Google Analytics campaign parameters.
// Note: This is for illustrative purposes. In most cases campaign
//       information would come from an incoming Intent.


// Campaign data sent with this hit.
        t.send(new HitBuilders.ScreenViewBuilder()
                        .setCampaignParamsFromUrl(path)
                        .build()
        );

    }
}
