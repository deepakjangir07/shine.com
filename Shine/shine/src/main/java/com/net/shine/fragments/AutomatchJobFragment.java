package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.adapters.AutomatchJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Deepak on 21/12/16.
 */

public class AutomatchJobFragment extends BaseFragment implements AdapterView.OnItemClickListener {


    private RecyclerView gridView;
    private AutomatchJobsAdapter adapter;
    private boolean loadMore = false;
    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
    private String sortDate;
    private View errorLayout;
    private View view;
    private Set<String> unique_companies = new HashSet<>();
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private String co_names = "";

    private WhoViewMyProfileModel.Result mainResult;
    private ArrayList<WhoViewMyProfileModel.AutomatchJob> automatchJobs=new ArrayList<>();

    String nextUrl;
    TextView jobs_count;

    public static AutomatchJobFragment newInstance(Bundle args) {
        AutomatchJobFragment fragment = new AutomatchJobFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.automatch_job_layout, container, false);
        try {
            Bundle bundle = getArguments();

            mainResult=(WhoViewMyProfileModel.Result)bundle.getSerializable("list_item");

            if(mainResult!=null)
                createJobListforAdapter();

            sortDate = bundle.getString(URLConfig.SORT_DATE);
            gridView = (RecyclerView) view.findViewById(R.id.grid_view);
            gridView.setLayoutManager(new LinearLayoutManager(mActivity));
            errorLayout = view.findViewById(R.id.error_layout);
            errorLayout.setVisibility(View.GONE);
            adapter = new AutomatchJobsAdapter(mActivity, jobList);

            gridView.setAdapter(adapter);
             jobs_count = (TextView) view
                    .findViewById(R.id.jobs_count_textview);

            showresult();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        try {
            Bundle bundle = new Bundle();
            bundle.putInt(URLConfig.INDEX, i);
            bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jobList);
            BaseFragment fragment = new JobDetailParentFrg();
            fragment.setArguments(bundle);
            mActivity.singleInstanceReplaceFragment(fragment);


            ShineCommons.trackShineEvents("ProfileViews","JobDetails-Automatch",mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showresult() {
        try {


            if (jobList.size() > 0) {
                errorLayout.setVisibility(View.GONE);
                view.findViewById(R.id.grid_view).setVisibility(View.VISIBLE);
                adapter.rebuildListObject(jobList);
                adapter.notifyDataSetChanged();
                jobs_count.setText("Your profile is an auto-match for "+jobList.size()+" jobs");

            }

            else
            {
                gridView.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
                BaseFragment fragment = new MyProfileFrg();
                DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                        mActivity.getString(R.string.no_result_for_automatch_jobs),
                        mActivity.getString(R.string.no_result_for_automatch_jobs_msg2),
                        mActivity.getString(R.string.update_profile),
                        DialogUtils.ERR_NO_SEARCH_RESULT,fragment);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void createJobListforAdapter()
    {

        automatchJobs=(ArrayList<WhoViewMyProfileModel.AutomatchJob>) mainResult.automatchJobs;

        jobList=new ArrayList<>();

        for(WhoViewMyProfileModel.AutomatchJob automatchModel:automatchJobs)

        {
            try {

                SimpleSearchModel.Result model=new SimpleSearchModel.Result();
                model.rect_id=automatchModel.rect_id;
                model.jobTitle=automatchModel.jobTitle;
                model.jJobType=automatchModel.jJobType;
                model.comp_name=automatchModel.comp_name;
                model.job_area_list=automatchModel.job_area_list;
                model.jobExperience=automatchModel.jobExperience;
                model.job_skills=automatchModel.total_skills;
                model.job_loc_list=automatchModel.job_loc_list;
                model.jExpDate=automatchModel.jExpDate;
                model.jWLC=automatchModel.jWLC;
                model.jobId=automatchModel.jobId;
                model.jWSD=automatchModel.jWSD;

                int len = automatchModel.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += automatchModel.job_loc_list.get(j);
                    } else {
                        loc += automatchModel.job_loc_list.get(j) + " / ";
                    }
                }

                model.job_loc_str = loc;


                jobList.add(model);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }
}
