package com.net.shine.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.net.shine.R;
import com.net.shine.models.ReferralModel;

import java.util.ArrayList;

public class RequestReferralAdapter extends BaseAdapter {
    private Activity mContext;
    private ArrayList<ReferralModel.RefrelResults> mList;
    public ImageLoader.ImageContainer imageContainer;


    public RequestReferralAdapter(Activity context, ArrayList<ReferralModel.RefrelResults> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater li = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = li.inflate(R.layout.request_referral_row, parent, false);
        TextView mCompanyName = (TextView) v.findViewById(R.id.company_name);
        TextView mJobTitle = (TextView) v.findViewById(R.id.jobtitle);
        final TextView mName = (TextView) v.findViewById(R.id.name);
        final ImageView mImg = (ImageView) v.findViewById(R.id.imageView1);

        TextView expired_job=(TextView)v.findViewById(R.id.expired);

        LinearLayout requestRow = (LinearLayout) v.findViewById(R.id.requestAcceptLayout);

        TextView request_accepted_msg = (TextView) v.findViewById(R.id.request_accepted_msg);
        TextView connected_via = (TextView) v.findViewById(R.id.connected_via);


        mCompanyName.setText(mList.get(position).getCompany_name());

        mJobTitle.setText(mList.get(position).getJob_title());
        mName.setText(mList.get(position).getReferrals().get(0).getReferrer_name());
        int connected_mode = mList.get(position).getReferrals().get(0).getMode();

        if(mList.get(position).job_expired)
        {
            mJobTitle.setTextColor(mContext.getResources().getColor(R.color.black_555555));
            expired_job.setVisibility(View.VISIBLE);
        }
        else
        {
            mJobTitle.setTextColor(mContext.getResources().getColor(R.color.linkcolor));
            expired_job.setVisibility(View.GONE);

        }

        if (connected_mode == 1) {
            connected_via.setText("Connected via Phonebook");
        } else if (connected_mode == 2) {
            connected_via.setText("Connected via Email");
        } else if (connected_mode == 3) {
            connected_via.setText("Connected via LinkedIn");
        } else if (connected_mode == 4) {
            if (mList.get(position).getReferrals().get(0).getOrganization() != null && !mList.get(position).getReferrals().get(0).getOrganization().equals("null")) {
                connected_via.setText("Worked together at " + mList.get(position).getReferrals().get(0).getOrganization());

            } else {
                connected_via.setText("Worked together");
            }
        } else if (connected_mode == 5) {
            if (mList.get(position).getReferrals().get(0).getOrganization() != null && !mList.get(position).getReferrals().get(0).getOrganization().equals("null")) {

                connected_via.setText("Studied together at " + mList.get(position).getReferrals().get(0).getOrganization());
            } else {
                connected_via.setText("Studied together");
            }
        }


        if (!TextUtils.isEmpty(mList.get(position).getReferrals().get(0).getReferrer_image_url())&&
                !(mList.get(position).getReferrals().get(0).getReferrer_image_url().equals("null"))) {
            try {

                Glide.with(mContext).load(mList.get(position).getReferrals().get(0).getReferrer_image_url())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                ColorGenerator generator = ColorGenerator.MATERIAL;
                                int color = generator.getRandomColor();
                                TextDrawable drawable = TextDrawable.builder()
                                        .buildRect(mName.getText().toString().substring(0,1).toUpperCase(), color);
                                mImg.setImageDrawable(drawable);

                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        }).into(mImg);

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(mName.getText().toString().substring(0,1).toUpperCase(), color);
            mImg.setImageDrawable(drawable);
        }
        if (mList.get(position).getReferrals().get(0).getStatus() == 2) {
            requestRow.setVisibility(View.VISIBLE);
            request_accepted_msg.setText("Request Accepted by " + mList.get(position).getReferrals().get(0).getReferrer_name());
        } else {
            requestRow.setVisibility(View.GONE);
        }

        return v;
    }

    public void rebuildListObject(ArrayList itemList) {

        this.mList = itemList;
    }
}
