package com.net.shine.widget;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by manishpoddar on 09/12/16.
 */

public class WidgetService extends RemoteViewsService {

/*
* So pretty simple just defining the Adapter of the listview
* here Adapter is ListProvider
* */


    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        int appWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);


        return (new ListProvider(this.getApplicationContext(), intent));
    }

}

