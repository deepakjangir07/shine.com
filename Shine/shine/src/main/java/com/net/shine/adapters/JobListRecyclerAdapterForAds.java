package com.net.shine.adapters;

/**
 * Created by Deepak on 01/03/17.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.activity.FriendPopupActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.models.ApplyJobModel;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.views.BetterPopupWindow;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;



public abstract class JobListRecyclerAdapterForAds extends RecyclerView.Adapter<JobListRecyclerAdapterForAds.JobsViewHolder>  {


    protected BaseActivity mActivity;
    int position=0;
    private View actionButton;
    protected ArrayList<SimpleSearchModel.Result> mList;


    private static String mLocations = "";
    private String GAEvent="";


    public JobListRecyclerAdapterForAds(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list, String GAEvent) {
        this.mActivity = activity;
        this.mList = list;
        this.GAEvent=GAEvent;


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public JobListRecyclerAdapterForAds.JobsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView;
        if(viewType==JOB_ITEM)

            convertView = LayoutInflater.from(mActivity).inflate(R.layout.grid_table_row, parent, false);
        else if(viewType==AD_ITEM)
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.ad_layout, parent, false);
        else
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.match_error, parent, false);

        try {

            RecyclerView recyclerView = (RecyclerView) parent;
            return new JobListRecyclerAdapterForAds.JobsViewHolder(convertView, recyclerView, viewType);
        }catch (Exception e){
            e.printStackTrace();
            return new JobListRecyclerAdapterForAds.JobsViewHolder(convertView, viewType);
        }

    }

    @Override
    public void onBindViewHolder(final JobListRecyclerAdapterForAds.JobsViewHolder viewHolder, final int position) {


        if(getItemViewType(position)==AD_ITEM)
        {
            //Here we add logic of ads - 4 (as visible - on plus rest gone)
            Log.d("DEBUG","null");

            return;
        }

        if(getItemViewType(position)==OTHER_JOBS_HEADING)
        {
            //Here we add logic of other suggested job
            Log.d("DEBUG","null");

            return;

        }

        viewHolder.contentView.setVisibility(View.VISIBLE);
        if (mList.get(position)!=null&&mList.get(position).is_applied || URLConfig.jobAppliedSet.contains(mList.get(position).jobId)) {
            viewHolder.apply_job.setVisibility(View.GONE);
            viewHolder.posted_date.setVisibility(View.GONE);
            viewHolder.checkbox_linear.setVisibility(View.GONE);
            viewHolder.webicon_linear.setVisibility(View.GONE);
            viewHolder.applied_job.setVisibility(View.VISIBLE);
            viewHolder.applied_similar_btn.setVisibility(View.VISIBLE);
        } else {

            viewHolder.applied_job.setVisibility(View.GONE);
            viewHolder.applied_similar_btn.setVisibility(View.GONE);
            if(inActionMode())
            {
                viewHolder.apply_job.setVisibility(View.GONE);
                viewHolder.posted_date.setVisibility(View.GONE);
                if(isMultiSupported(position)){
                    viewHolder.checkbox_linear.setVisibility(View.VISIBLE);
                    viewHolder.webicon_linear.setVisibility(View.GONE);
                    viewHolder.multi_apply_checkbox.setChecked(selectedItems.get(position));
                } else {
                    viewHolder.checkbox_linear.setVisibility(View.GONE);
                    if(!TextUtils.isEmpty(mList.get(position).jRUrl)){
                        viewHolder.webicon_linear.setVisibility(View.VISIBLE);
                        viewHolder.recruiter_multi.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    final Dialog alertDialog = new Dialog(mActivity);
                                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    alertDialog.setContentView(R.layout.cus_apply_redirect_dialog);

                                    alertDialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DialogUtils.dialogDismiss(alertDialog);
                                        }
                                    });
                                    alertDialog.findViewById(R.id.go_btn).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DialogUtils.dialogDismiss(alertDialog);
                                            if (mList.get(position) != null) {
                                                final String urlToHit = URLConfig.SUDO_APPLY_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                                                        .replace(URLConfig.JOB_ID, mList.get(position).jobId);
                                                final Dialog dialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                                                Type type = new TypeToken<ApplyJobModel>() {
                                                }.getType();
                                                VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                                                    @Override
                                                    public void OnDataResponse(Object object, String tag) {

                                                        DialogUtils.dialogDismiss(dialog);

                                                        if (ShineCommons.appInstalledOrNot("com.android.chrome")) {
                                                            System.out.println("-- package install");
                                                            ChromeCustomTabs.openCustomTab(mActivity, mList.get(position).jRUrl);
                                                        } else {
                                                            Bundle bundle = new Bundle();
                                                            Intent intent = new Intent(mActivity, CustomWebView.class);
                                                            bundle.putString("shineurl", mList.get(position).jRUrl);
                                                            intent.putExtras(bundle);
                                                            mActivity.startActivity(intent);

                                                        }
                                                    }


                                                    @Override
                                                    public void OnDataResponseError(String error, String tag) {
                                                        DialogUtils.dialogDismiss(dialog);
                                                        final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                                                        errorDialog.findViewById(R.id.ok_btn).setOnClickListener(
                                                                new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View view) {
                                                                        DialogUtils.dialogDismiss(errorDialog);
                                                                    }
                                                                });
                                                    }

                                                },
                                                        urlToHit, type);
                                                req.setUsePutMethod(new HashMap());
                                                req.execute("applyjobs");
                                            }
                                        }
                                    });

                                    alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT);
                                    if (!mActivity.isFinishing())
                                        alertDialog.show();

                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            } else {
                viewHolder.checkbox_linear.setVisibility(View.GONE);
                viewHolder.webicon_linear.setVisibility(View.GONE);
                viewHolder.apply_job.setVisibility(View.VISIBLE);
                viewHolder.posted_date.setVisibility(View.VISIBLE);

                viewHolder.apply_job.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String jobID = (String) v.getTag();
                        SimpleSearchModel.Result svo = null;
                        for (int i = 0; i < mList.size(); i++) {
                            if (mList.get(i) == null)
                                continue;
                            if (mList.get(i).jobId.equals(jobID)) {
                                svo = mList.get(i);
                                break;
                            }
                        }
                        applyToJob((String) v.getTag(), svo, viewHolder.contentView);



                    }
                });
                viewHolder.apply_job.setTag(mList.get(position).jobId);
            }
        }

        viewHolder.applied_similar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchVO sVO = new SimpleSearchVO();
                sVO.setJobId(mList.get(viewHolder.getAdapterPosition()).jobId);
                sVO.setFromSimilarJobs(true);
                sVO.setHideModifySearchButton(true);
                if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                    String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                    sVO.setUrl(URL);

                } else {
                    sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                BaseFragment fragment = new SearchResultFrg();
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);

                ShineCommons.trackShineEvents("View SimilarJobs","JobListing",mActivity);
            }
        });



        viewHolder.title.setText(mList.get(position).jobTitle);
        viewHolder.company.setText(mList.get(position).comp_name);
        viewHolder.posted_date.setText(mList.get(position).getPostedDate());


        if (mList.get(position).job_skills==null||TextUtils.isEmpty(mList.get(position).job_skills.toString().trim())) {
            viewHolder.skills.setVisibility(View.GONE);
        } else {
            viewHolder.skills.setVisibility(View.VISIBLE);
        }

        if (mList.get(position).jJobType==2){

            viewHolder.walkin.setVisibility(View.VISIBLE);
            viewHolder.divider.setVisibility(View.VISIBLE);

            viewHolder.frndCountRow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mList.get(position).getVenue())){
                viewHolder.venue.setVisibility(View.VISIBLE);
                viewHolder.venue.setText(Html.fromHtml("<b>Venue </b> " + mList.get(position).getVenue()));
            }
            else {
                viewHolder.venue.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(mList.get(position).getDate())){
                viewHolder.date.setVisibility(View.VISIBLE);
                viewHolder.date.setText(Html.fromHtml("<b>Date </b>"+mList.get(position).getDate()));
            }
            else {
                viewHolder.date.setVisibility(View.GONE);
            }
        } else {
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.venue.setVisibility(View.GONE);
            viewHolder.walkin.setVisibility(View.GONE);
            viewHolder.divider.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(mList.get(position).jRUrl)){
            viewHolder.frndCountRow.setVisibility(View.GONE);
        }

        DiscoverModel.Company mDiscoverFriendsModel = mList.get(position).frnd_model;
        if (mDiscoverFriendsModel != null && mDiscoverFriendsModel.data.friends.size() > 0) {
            setConnection(mDiscoverFriendsModel, mList.get(position), viewHolder);
        } else
            viewHolder.frndCountRow.setVisibility(View.GONE);

        showMoreInLocation(viewHolder, mList.get(position).job_loc_str, mList.get(position).jobExperience, mList.get(position).job_skills);


    }



    @Override
    public long getItemId(int position) {
        return position;
    }



    private void setConnection(DiscoverModel.Company dfm, final SimpleSearchModel.Result searchResultVO, JobsViewHolder viewHolder) {
        {

            viewHolder.frndCountRow.setVisibility(View.VISIBLE);
            final int friend_count = dfm.data.friends.size();
            if (friend_count == 2) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#555555'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connection "
                                        + "<font color='#555555'>work here</font>"));
            } else if (friend_count > 1) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#555555'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connections "
                                        + "<font color='#555555'>work here</font>"));
            } else {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#2c84cc'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> "
                                        + "<font color='#555555'> works here</font>"));
            }
            viewHolder.frndCountRow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if(inActionMode())
                        return;

                    Bundle args = new Bundle();
                    args.putSerializable("svo", searchResultVO);
                    args.putInt("fromScreen", 0);
                    Intent intent = new Intent(mActivity, FriendPopupActivity.class);
                    intent.putExtras(args);
                    mActivity.startActivity(intent);

                    ShineCommons.trackShineEvents("KonnectFriendList",GAEvent,mActivity);


                }
            });
        }

    }

    public void rebuildListObject(ArrayList<SimpleSearchModel.Result> itemList) {

        this.mList = itemList;
    }


    private void showMoreInLocation(final JobsViewHolder viewHolder, final String location,
                                    final String exp, final String skill) {


        if (skill != null && !skill.isEmpty()) {

            viewHolder.skills.setText(skill);


        }

        if (exp == null || exp.equals("")) {
            viewHolder.tv_experience.setVisibility(View.GONE);
        } else {
            viewHolder.tv_experience.setVisibility(View.VISIBLE);
            viewHolder.tv_experience.setText(exp + "  ");
        }





        final StringTokenizer st = new StringTokenizer(location, "/");
        if (st.countTokens() > 1) {
            final String s2;
            {
                s2 = st.nextToken()+"";
            }

            if(s2.toString().length()>14){
                viewHolder.tv_location.setText(s2.substring(0, 11) + "...");

            }
            else
            {viewHolder.tv_location.setText(s2.toString());}
            viewHolder.tv_more.setVisibility(View.VISIBLE);

            viewHolder.tv_more.setText(Html.fromHtml("<font color = '#777777'>"
                    +" & "
                    +"</font>"
                    +"<font color = '#2c84cc'>"
                    +(st.countTokens())
                    + " more"
                    +"</font>"));

        }
        else
        {
            viewHolder.tv_location.setText(location);
            viewHolder.tv_more.setVisibility(View.GONE);
        }





        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location_more = "";
                String[] arr_loc = location.split("/");
                for (int i = 0; i < arr_loc.length; i++) {
                    if (i == (arr_loc.length - 1)) {
                        location_more = location_more + arr_loc[i].trim();
                    } else {
                        location_more = location_more + arr_loc[i].trim() + ", ";
                    }
                }
                mLocations = location_more;
                DemoPopupWindow demoPopupWindow = new DemoPopupWindow(viewHolder.tv_more);
                demoPopupWindow.showLikePopDownMenu();
            }
        });


    }




    private void applyToJob(final String jobID, final SimpleSearchModel.Result svo, final View apply_view) {


        ApplyJobsNew.preApplyFlow(GAEvent,jobID, svo.comp_uid_list, svo.frnd_model, mActivity, new ApplyJobsNew.onApplied() {
            @Override
            public void onApplied(String appliedResponse, Dialog plzWaitDialog) {

                svo.is_applied = true;
                Toast.makeText(mActivity, appliedResponse, Toast.LENGTH_SHORT).show();
                if (apply_view.findViewById(R.id.applied_job) != null) {
                    apply_view.findViewById(R.id.apply_job)
                            .setVisibility(View.GONE);
                    apply_view.findViewById(R.id.applied_job)
                            .setVisibility(View.VISIBLE);
                    notifyDataSetChanged();
                }

                if(!svo.isInsidePostApply)
                    ApplyJobsNew.postApplyFlow(svo, mActivity, plzWaitDialog);
                else
                    DialogUtils.dialogDismiss(plzWaitDialog);
            }
        }, svo);
    }


    private static class DemoPopupWindow extends BetterPopupWindow {

        DemoPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView tv_locations = (TextView) root.findViewById(R.id.loc_txt);
            tv_locations.setText(mLocations);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }


    public static final int OTHER_JOBS_HEADING = 1;
    public static final int JOB_ITEM = 0;
    public static final int AD_ITEM = 2;

    @Override
    public int getItemViewType(int position) {
        if(mList.get(position)==null)
            return OTHER_JOBS_HEADING;
        else if(position==3&&ShineSharedPreferences.isCareerPlusAds(mActivity))
            return AD_ITEM;
        return JOB_ITEM;
    }


    public static class JobsViewHolder extends RecyclerView.ViewHolder{

        View apply_job;
        View applied_job;
        TextView frndCount;
        RelativeLayout frndCountRow;
        TextView company;
        TextView title;
        TextView posted_date;
        TextView tv_more;
        TextView tv_experience;
        TextView tv_location;
        TextView skills;
        LinearLayout checkbox_linear;
        TextView date,venue;
        RecyclerView recyclerView;
        View contentView;
        CheckBox multi_apply_checkbox;
        ImageView recruiter_multi;
        LinearLayout webicon_linear;
        View applied_similar_btn;

        TextView msg_tv_spannable;
        View error_no_match;

        LinearLayout walkin;
        View divider;



        ImageView imageView;

        JobsViewHolder(View convertView, RecyclerView recyclerView, int viewType){
            this(convertView, viewType);
            this.recyclerView = recyclerView;
        }
        JobsViewHolder(View v, int viewType) {
            super(v);
            contentView = v;
            if(viewType==JOB_ITEM){
                multi_apply_checkbox = (CheckBox) v.findViewById(R.id.apply_job_multi);
                checkbox_linear = (LinearLayout) v.findViewById(R.id.checkbox_container);
                title = (TextView) v.findViewById(R.id.title);
                company = (TextView) v.findViewById(R.id.company);
                frndCount = (TextView) v
                        .findViewById(R.id.frndsCount);

                walkin = (LinearLayout) v.findViewById(R.id.walkin);
                divider = (View) v.findViewById(R.id.divider);


                date = (TextView) v.findViewById(R.id.date);
                venue = (TextView) v.findViewById(R.id.venue);
                posted_date = (TextView) v.findViewById(R.id.posted);
                frndCountRow = (RelativeLayout) v
                        .findViewById(R.id.frnd_row);
                apply_job = v.findViewById(R.id.apply_job);
                applied_job = v.findViewById(R.id.applied_job);
                recruiter_multi = (ImageView) v.findViewById(R.id.recruiter_job_multi);
                webicon_linear = (LinearLayout) v.findViewById(R.id.webicon_container);
                tv_more = (TextView) v.findViewById(R.id.more);
                tv_experience = (TextView) v
                        .findViewById(R.id.experience);
                skills = (TextView) v.findViewById(R.id.skills);
                tv_location = (TextView) v.findViewById(R.id.location);
                applied_similar_btn = v.findViewById(R.id.applied_similar_btn_list);
            }
            else if(viewType==AD_ITEM)
            {
                imageView=(ImageView)v.findViewById(R.id.imageView);

            }

            else {
                msg_tv_spannable = (TextView) v.findViewById(R.id.msg_tv_spannable);
                error_no_match = v.findViewById(R.id.error_no_match);

            }


        }
    }






    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    public static ActionMode actionMode;
    private void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        actionMode.setTitle(getSelectedItemCount() + " Jobs selected");

        if(getSelectedItemCount()>0)
            actionButton.setVisibility(View.VISIBLE);
        else
            actionButton.setVisibility(View.GONE);
        notifyItemChanged(pos);
    }


    private int getSelectedItemCount() {
        return selectedItems.size();
    }

    public void startActionMode(View actionButtona){

        this.actionButton=actionButtona;

        actionMode = mActivity.startSupportActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle("0 Jobs selected");

                actionMode=mode;

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionButton.setVisibility(View.GONE);
                actionMode = null;
                selectedItems.clear();
                notifyDataSetChanged();
            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectedItems.size()==0) {
                    Toast.makeText(mActivity, "Please select at least one job to apply.", Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    final ArrayList<String> job_ids = new ArrayList<>();
                    for(int i=0;i<selectedItems.size();i++)
                        job_ids.add(mList.get(selectedItems.keyAt(i)).jobId);
                    ApplyJobsNew.multiApply(mActivity, job_ids, new ApplyJobsNew.MultiAppliedListener() {
                        @Override
                        public void onMultiApplied(String msg) {
                            Toast.makeText(mActivity, msg+"", Toast.LENGTH_SHORT).show();
                            for(int i=0;i<selectedItems.size();i++)
                                mList.get(selectedItems.keyAt(i)).is_applied = true;
                            if(actionMode!=null)
                                actionMode.finish();
                        }
                    });
                }

            }
        });
        notifyDataSetChanged();
    }


    boolean inActionMode(){
        return actionMode!=null;
    }

    void onItemClickInActionMode(int position){
        if(isMultiSupported(position))
            toggleSelection(position);
    }

    private boolean isMultiSupported(int position){

        if(mList.get(position).is_applied || URLConfig.jobAppliedSet.contains(mList.get(position).jobId))
            return false;
        if(!TextUtils.isEmpty(mList.get(position).jRUrl) || mList.get(position).jJobType==2)
            return false;
        return true;
    }




}




