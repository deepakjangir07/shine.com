package com.net.shine.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.URLUtil;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.AuthActivity;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.activity.ResumeUploadActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.JobDetailFrg;
import com.net.shine.fragments.SimilarJobs;
import com.net.shine.fragments.ThanksAppliedFrg;
import com.net.shine.fragments.UpdateFlowsFragment;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.interfaces.SocialAppliedListener;
import com.net.shine.models.ApplyJobModel;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SocialApplyProfileModel;
import com.net.shine.models.SocialApplyResponseModel;
import com.net.shine.models.UpdateFlowsModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.apache.http.util.TextUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;


public class ApplyJobsNew {

    public static final String JOB_ID = "job_id";
    public static final String APPLY_REDIRECT = "isApplyRedirect";
    public static final String APPLY_WALKIN = "isWalkIn";
    public static final String APPLY_BUNDLE = "apply_bundle";
    public static final String APPLY_FROM_DIALOG = "apply_from_dialog";

    public static final String APPLY_WITHOUT_RESUME="apply_without_resume";


    //here we definitely have friend list, only check for resume and apply
    public static void dialogApplyFlow(final String GAEvent, final String jobId, final BaseActivity mActivity, final onApplied listener) {

        String userId = ShineSharedPreferences.getCandidateId(mActivity);
        final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                mActivity.getString(R.string.plz_wait));
        Type type = new TypeToken<ApplyJobModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                URLConfig.jobAppliedSet.add(jobId);
                ApplyJobModel model = (ApplyJobModel) object;
                // // FIXME: 10/27/2015 Added an else which at least dismisses the dialog?
                if (model.job_id.equals(jobId))   //in case of error, jobid will not be present in response
                {
                    listener.onApplied(" 1 job applied successfully.", plzWaitDialog);
                    ShineCommons.trackShineEvents("Apply", GAEvent, mActivity);
                }
                DataCaching.getInstance().cacheData(DataCaching.APPLIED_JOB_SET, "" + DataCaching.CACHE_TIME_LIMIT_CONNECTION,
                        URLConfig.jobAppliedSet, mActivity);
                Log.d("DEBUG::APPLIED_SET", jobId + " added to Applied Set and saved");
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(plzWaitDialog);
                showResumeOrError(error, mActivity, jobId);
            }

        },
                URLConfig.APPLY_JOBS_URL.replace(URLConfig.CANDIDATE_ID, userId), type);
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put("candidate_id", userId);
        paramsMap.put("job_id", jobId);
        paramsMap.put("is_auto_apply", "3");

        //TODO:here we have to put also referral_code in apply if coming from link

        if (!ShineSharedPreferences.getGetReferralCode(mActivity, jobId).equals(""))
            paramsMap.put("referral_code", ShineSharedPreferences.getGetReferralCode(mActivity, jobId));
        req.setUsePostMethod(paramsMap);
        req.execute("applyjobs");
    }



    public static void preApplyFlow(final String GAEvent, final String jobID, final ArrayList<String> cuid, final DiscoverModel.Company frnd,
                                    final BaseActivity mActivity, final onApplied listener, SimpleSearchModel.Result argResult) {

        MyApplication.getInstance().getRequestQueue().cancelAll("updateFlows");
        UserStatusModel userProfileVO = ShineSharedPreferences
                .getUserInfo(mActivity);
        if (argResult == null)
            argResult = new SimpleSearchModel.Result();
        final SimpleSearchModel.Result result = argResult;
        if (userProfileVO != null) {     //user is logged in

            if (URLUtil.isValidUrl(result.jRUrl)) {
                showCustomApplyRedirectAlerts(mActivity, jobID, result);
                return;
            }


            String urlToHit;
            HashMap<String, String> paramsMap = new HashMap<>();

            if (result.jJobType == 2) {
                urlToHit = URLConfig.SUDO_APPLY_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                        .replace(URLConfig.JOB_ID, jobID);
            } else {
                if (result.resume_req == 0) {
                    urlToHit = URLConfig.APPLY_WITHOUT_RESUME_URL;
                    paramsMap.put("user_id", ShineSharedPreferences.getCandidateId(mActivity));
                    paramsMap.put("job_id", jobID);
                    paramsMap.put("is_auto_apply", "3");
                    paramsMap.put("name", userProfileVO.full_name);
                    paramsMap.put("email", userProfileVO.email);
                    paramsMap.put("mobile", userProfileVO.mobile_no);
                    paramsMap.put("application_source", "2");   //2 is for Shine
                } else {
                    urlToHit = URLConfig.APPLY_JOBS_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                    paramsMap.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
                    paramsMap.put("job_id", jobID);
                    paramsMap.put("is_auto_apply", "3");
                }
            }

            final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));
            Type type = new TypeToken<ApplyJobModel>() {
            }.getType();
            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    URLConfig.jobAppliedSet.add(jobID);

                    ApplyJobModel model = (ApplyJobModel) object;

                    if (result.jJobType == 2) {
                        DialogUtils.dialogDismiss(plzWaitDialog);
                        Bundle bundle1  = new Bundle();
                        bundle1.putString("job_id",jobID);
                        bundle1.putString("company_name",result.comp_name);
                        bundle1.putBoolean("company_posted",false);
                        bundle1.putBoolean("walk_in",true);
                        SimilarJobs sJ = new SimilarJobs();
                        sJ.setArguments(bundle1);
                        sJ.show(mActivity.getSupportFragmentManager(), sJ.getTag());
                        ShineCommons.trackShineEvents("Apply", "Walk-in", mActivity);
                    }
                    else  if (model.job_id.equals(jobID))   //in case of error, jobid will not be present in response
                    {
                        ShineCommons.trackShineEvents("Apply", GAEvent, mActivity);
                        listener.onApplied(" 1 job applied successfully.", plzWaitDialog);
                    } else {
                        DialogUtils.dialogDismiss(plzWaitDialog);
                    }

                    Log.d("DEBUG::APPLIED_SET", jobID + " added to Applied Set and saved");
                }

                @Override
                public void OnDataResponseError(String error, String tag) {
                    DialogUtils.dialogDismiss(plzWaitDialog);
                    showResumeOrError(error, mActivity, jobID);
                }

            },
                    urlToHit, type);

            //TODO:here we have to put also referral_code in apply if coming from link

            if (!ShineSharedPreferences.getGetReferralCode(mActivity, jobID).equals(""))
                paramsMap.put("referral_code", ShineSharedPreferences.getGetReferralCode(mActivity, jobID));

            if (result.jJobType == 2) {
                req.setUsePutMethod(paramsMap);
            } else {
                req.setUsePostMethod(paramsMap);
            }
            req.execute("applyjobs");

        } else {  //user is not logged in, show login Page

            Bundle apply_bundle = new Bundle();
            apply_bundle.putString(JOB_ID, jobID);
            apply_bundle.putBoolean(APPLY_REDIRECT, !TextUtils.isEmpty(result.jRUrl));
            apply_bundle.putBoolean(APPLY_WALKIN, result.jJobType==2);
            apply_bundle.putInt(APPLY_WITHOUT_RESUME,result.resume_req);
            result.jobId = (jobID);
            if (cuid != null)
                result.comp_uid_list = cuid;
            if (frnd != null)
                result.frnd_model = frnd;
            apply_bundle.putSerializable(JobDetailFrg.BUNDLE_JOB_DATA_KEY, result);

            Bundle bundle = new Bundle();
            bundle.putBundle(APPLY_BUNDLE, apply_bundle);
            bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_TO_APPLY_JOBS);
            bundle.putInt(AuthActivity.SELECTED_FRAG, AuthActivity.LOGIN_FRAG);

            Intent intent = new Intent(mActivity, AuthActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            intent.putExtras(bundle);
            mActivity.startActivity(intent);

        }
    }

        public static void socialApplyUser(final BaseActivity mActivity, final String jobId, final SocialApplyProfileModel model, final SocialAppliedListener listener, final boolean isRegister) {
            final Dialog plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    mActivity.getString(R.string.plz_wait));
            Type type = new TypeToken<SocialApplyResponseModel>() {
            }.getType();
            HashMap<String, Object> paramsMap = new HashMap<>();

            String url;
            if (isRegister)
                url = URLConfig.SOCIAL_APPLY_AND_REGISTER_API;
            else
                url = URLConfig.SOCIAL_APPLY_API;

            final HashMap<String, Object> profile = new HashMap<>();
//        if (isRegister) {
            switch (model.profile_source) {
                case 1: //linkedin
                    profile.put("user_type", 5);
                    break;
                case 4: //google
                    profile.put("user_type", 10);
                    break;
                case 0: //facebook
                    profile.put("user_type", 13);
                    break;
            }
//        }
            profile.put("candidate_location", model.candidate_location);
            profile.put("cell_phone", model.cell_phone);
            profile.put("country_code", model.country_code);
            profile.put("current_company_name", model.current_company_name);
            profile.put("current_job_title", model.current_job_title);
            profile.put("experience_in_months", "0");
            profile.put("experience_in_years", model.experience_in_years);
            profile.put("functional_area", model.functional_area);
            profile.put("industry", model.industry);
            profile.put("name", model.name);
            profile.put("email", model.email);
            profile.put("source_id", model.source_id);
            profile.put("country_code", 91);
            profile.put("profile_source", model.profile_source);
            profile.put("salary_in_lakh", model.salary_in_lakh);
            profile.put("salary_in_thousand", 0);
            profile.put("source_id", model.source_id);
            paramsMap.put("profile", profile);

            HashMap resume = new HashMap();
            if (!TextUtils.isEmpty(model.resume.encoded_string)) {
                resume.put("encoded_string", model.resume.encoded_string);
                resume.put("resume_name", model.resume.resume_name);
            } else { //case where we expect backend to pick previous uploaded resume
                resume.put("encoded_string", "");
                resume.put("resume_name", "");
            }

            paramsMap.put("resume", resume);

            paramsMap.put("job_id", jobId);

            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    switch (model.profile_source) {
                        case 1:
                            if(isRegister)
                                ShineCommons.trackShineEvents("Apply", "DirectApply-Linkedin", mActivity);
                            else
                                ShineCommons.trackShineEvents("Apply", "SocialApply-Linkedin", mActivity);
                            break;
                        case 4:
                            if(isRegister)
                                ShineCommons.trackShineEvents("Apply", "DirectApply-Google", mActivity);
                            else
                                ShineCommons.trackShineEvents("Apply", "SocialApply-Google", mActivity);
                            break;
                        case 0:
                            if(isRegister)
                                ShineCommons.trackShineEvents("Apply", "DirectApply-Facebook", mActivity);
                            else
                                ShineCommons.trackShineEvents("Apply", "SocialApply-Facebook", mActivity);
                            break;
                    }

                    SocialApplyResponseModel model = (SocialApplyResponseModel) object;
                    if (model.job_id.equals(jobId)) {
                        listener.onApplied("1 job applied successfully.", model, plzWaitDialog);
                    } else {
                        DialogUtils.dialogDismiss(plzWaitDialog);
                    }

                }

                @Override
                public void OnDataResponseError(String error, String tag) {
                    DialogUtils.dialogDismiss(plzWaitDialog);
                    final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity,error);
                    errorDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogUtils.dialogDismiss(errorDialog);
                                }
                            });
                }

            }, url, type);
            req.setUsePostMethod(paramsMap);
            req.execute("socialApply");
    }

    private static void showResumeOrError(String error, final BaseActivity mActivity, final String jobID) {
        if (error.contains("upload resume")) {
            final Dialog alertDialog = DialogUtils.showCustomResumeAlerts(mActivity);
            alertDialog.findViewById(R.id.resume_upload_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtils.dialogDismiss(alertDialog);

                    Intent intent = new Intent(mActivity, ResumeUploadActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(APPLY_FROM_DIALOG, true);
                    bundle.putString(JOB_ID, jobID);
                    intent.putExtras(bundle);
                    mActivity.startActivity(intent);
                    //category, action, label

                }
            });
            alertDialog.findViewById(R.id.no_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtils.dialogDismiss(alertDialog);
                }
            });
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    DialogUtils.dialogDismiss(alertDialog);
                }
            });
        } else {
            final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
            errorDialog.findViewById(R.id.ok_btn).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dialogDismiss(errorDialog);
                        }
                    });
        }
    }


        public interface onApplied {
        void onApplied(String appliedResponse, Dialog plzWaitDialog);
    }










    public static void postApplyFlow(final SimpleSearchModel.Result srvo, final BaseActivity mActivity, final Dialog alertDialog) {

        Log.d("DEBUG::APPLY", "PostApply started for " + srvo.jobId + " ");
        //increment rate count
        ShineSharedPreferences.updateRateAppCount(mActivity, true,
                false, false, false);

        DiscoverModel.Company dfm = srvo.frnd_model;
        if (dfm == null) {
            Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " Dfm is Null");
            // TODO: create a new Hit Here...
            if (srvo.comp_uid_list == null) {

                if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                    Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " comp_uid_list is Null....Starting UpdateFlow");
                    updateFlow(mActivity, alertDialog, srvo.comp_name, false, srvo.jobId);
                } else {
                    similarJobs(mActivity, srvo.jobId, srvo.comp_name, alertDialog, false);
                }

                return;
            } else {
                String company = null;
                ArrayList<String> company_id = srvo.comp_uid_list;
                for (int k = 0; k < company_id.size(); k++) {
                    company = company_id.get(k);
                    if (company != null && !company.trim().equals("")) {
                        break;
                    }
                }
                if (company == null || company.trim().equals("")) {

                    if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                        Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " comp_uid_list does not have any company....Starting UpdateFlow");
                        updateFlow(mActivity, alertDialog, srvo.comp_name, false, srvo.jobId);
                    } else {
                        similarJobs(mActivity, srvo.jobId, srvo.comp_name, alertDialog, false);
                    }
                    return;
                }


                final String finalCompany = company;
                Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " Valid Company found: " + finalCompany + "....Starting GetFriendsInCompany");

                GetFriendsInCompany friends = new GetFriendsInCompany(
                        mActivity, new GetConnectionListener() {
                    @Override
                    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
                        if (result == null) {

                            if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                                Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " GetFriendsInCompany sent null!!....Starting UpdateFlow");
                                updateFlow(mActivity, alertDialog, srvo.comp_name, true, srvo.jobId);
                            } else {
                                similarJobs(mActivity, srvo.jobId, srvo.comp_name, alertDialog, true);
                            }

                            return;
                        }
                        if (result.containsKey(finalCompany)) {
                            DialogUtils.dialogDismiss(alertDialog);
                            srvo.frnd_model = (result.get(finalCompany));
                            Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " GetFriendsInCompany sent valid dfm....Starting ThanksAppliedFrg");

                            if (mActivity != null && result.get(finalCompany) != null) {
                                ThanksAppliedFrg frg = ThanksAppliedFrg.newInstance(result.get(finalCompany), srvo, false);
                                frg.show(mActivity.getSupportFragmentManager(), frg.getTag());

                            }
                        } else {
                            if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                                updateFlow(mActivity, alertDialog, srvo.comp_name, true, srvo.jobId);
                            } else {
                                similarJobs(mActivity, srvo.jobId, srvo.comp_name, alertDialog, true);
                            }
                        }
                    }
                });
                friends.getFriendsInCompanyVolleyRequest(company);
                return;
            }
        }

        Log.d("DEBUG::APPLY", "PostApply Info: " + srvo.jobId + " dfm is not null....Starting ThanksAppliedFrg");

        DialogUtils.dialogDismiss(alertDialog);
        if (mActivity != null) {
            ThanksAppliedFrg frg = ThanksAppliedFrg.newInstance(dfm, srvo, false);
            frg.show(mActivity.getSupportFragmentManager(), frg.getTag());

        }
    }

    public static void postApplyFlow(final InboxAlertMailDetailModel.InboxJobDetail srvo, final BaseActivity mActivity, final Dialog alertDialog) {

        //increment rate count
        ShineSharedPreferences.updateRateAppCount(mActivity, true,
                false, false, false);

        DiscoverModel.Company dfm = srvo.getMatch_job();
        if (dfm == null) {
            Log.d("ERR", "dfm is null");
            // TODO: create a new Hit Here...
            if (srvo.getCompanyUuid() == null) {
                if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                    updateFlow(mActivity, alertDialog, srvo.getCompany(), false, srvo.getJob_id());
                } else {
                    similarJobs(mActivity, srvo.getJob_id(), srvo.getCompany(), alertDialog, false);
                }

                return;
            } else {
                String company = null;
                ArrayList<String> company_id = srvo.getCompanyUuid();
                for (int k = 0; k < company_id.size(); k++) {
                    company = company_id.get(k);
                    if (company != null && !company.trim().equals("")) {
                        break;
                    }
                }
                if (company == null) {
                    if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                        updateFlow(mActivity, alertDialog, srvo.getCompany(), false, srvo.getJob_id());
                    } else {
                        similarJobs(mActivity, srvo.getJob_id(), srvo.getCompany(), alertDialog, false);
                    }
                    return;
                }


                final String finalCompany = company;
                GetFriendsInCompany friends = new GetFriendsInCompany(
                        mActivity, new GetConnectionListener() {
                    @Override
                    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
                        if (result == null) {
                            if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                                updateFlow(mActivity, alertDialog, srvo.getCompany(), true, srvo.getJob_id());
                            } else {
                                similarJobs(mActivity, srvo.getJob_id(), srvo.getCompany(), alertDialog, true);
                            }
                            return;
                        }
                        if (result.containsKey(finalCompany)) {
                            DialogUtils.dialogDismiss(alertDialog);
                            srvo.setMatch_job(result.get(finalCompany));

                            if (mActivity != null && result.get(finalCompany) != null) {
                                ThanksAppliedFrg frg = ThanksAppliedFrg.newInstance(result.get(finalCompany), srvo, false);
                                frg.show(mActivity.getSupportFragmentManager(), frg.getTag());
                            }
                        } else {
                            // TODO: check connection sync status and show apt window

                            if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                                updateFlow(mActivity, alertDialog, srvo.getCompany(), true, srvo.getJob_id());
                            } else {
                                similarJobs(mActivity, srvo.getJob_id(), srvo.getCompany(), alertDialog, true);
                            }
                        }
                    }
                });
                friends.getFriendsInCompanyVolleyRequest(company);
                return;
            }
        }
        DialogUtils.dialogDismiss(alertDialog);
        if (mActivity != null) {
            ThanksAppliedFrg frg = ThanksAppliedFrg.newInstance(dfm, srvo, false);
            frg.show(mActivity.getSupportFragmentManager(), frg.getTag());
        }

    }


    private static void similarJobs(final BaseActivity mActivity, final String jobId, final String compNme,
                                    final Dialog alertDialog, final boolean isCompanyPosted) {
        DialogUtils.dialogDismiss(alertDialog);
        if(DialogUtils.showRateIfNeeded(mActivity))
            return;
        Bundle bundle1 = new Bundle();
        bundle1.putString("job_id", jobId);
        bundle1.putString("company_name", compNme);
        bundle1.putBoolean("company_posted", isCompanyPosted);
        SimilarJobs sJ = new SimilarJobs();
        sJ.setArguments(bundle1);
        sJ.show(mActivity.getSupportFragmentManager(), sJ.getTag());
    }

    private static void updateFlow(final BaseActivity mActivity, final Dialog alertDialog, final String compName, final boolean isCompanyposted, final String jobId) {

        String userId = ShineSharedPreferences.getCandidateId(mActivity);

        Type type = new TypeToken<UpdateFlowsModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                UpdateFlowsModel model= (UpdateFlowsModel) object;
                DialogUtils.dialogDismiss(alertDialog);
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: Show Update Flow: " + model.next_widget);
                if(model.next_widget==UpdateFlowsFragment.MobileVerificationWidget ||
                        model.next_widget==UpdateFlowsFragment.SkillsWidget ||
                        model.next_widget==UpdateFlowsFragment.CurrentSalaryWidget ||
                        model.next_widget==UpdateFlowsFragment.LinkedinSyncWidget||
                        model.next_widget==UpdateFlowsFragment.TotalExperienceWidget){
                UpdateFlowsFragment frg = UpdateFlowsFragment.newInstance(model);
                frg.show(mActivity.getSupportFragmentManager(), frg.getTag());
                }
                else {
                    return;
                }
            }


            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: OnDataResponseError: " + error);
                similarJobs(mActivity, jobId, compName, alertDialog, isCompanyposted);
            }

        },
                URLConfig.UPDATE_FLOW_URL.replace(URLConfig.CANDIDATE_ID, userId), type);
        //TODO:here we have to put also referral_code in apply if coming from link
        req.execute("updateFlows");


    }

    private static void updateFlowFromRedirectJobs(final BaseActivity mActivity, final Dialog alertDialog, final String jobId, final SimpleSearchModel.Result result) {

        String userId = ShineSharedPreferences.getCandidateId(mActivity);

        Type type = new TypeToken<UpdateFlowsModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                UpdateFlowsModel model= (UpdateFlowsModel) object;
                DialogUtils.dialogDismiss(alertDialog);
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: Show Update Flow: " + model.next_widget);
                if(model.next_widget==UpdateFlowsFragment.MobileVerificationWidget ||
                        model.next_widget==UpdateFlowsFragment.SkillsWidget ||
                        model.next_widget==UpdateFlowsFragment.CurrentSalaryWidget ||
                        model.next_widget==UpdateFlowsFragment.LinkedinSyncWidget||
                        model.next_widget==UpdateFlowsFragment.TotalExperienceWidget) {
                    UpdateFlowsFragment frg = UpdateFlowsFragment.newInstance(model);
                    frg.show(mActivity.getSupportFragmentManager(), frg.getTag());
                }
                else
                    return;
            }


            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: OnDataResponseError: " + error);
                similarJobsFromRedirectJobs(mActivity, jobId, result, alertDialog);
            }

        },
                URLConfig.UPDATE_FLOW_URL.replace(URLConfig.CANDIDATE_ID, userId), type);
        //TODO:here we have to put also referral_code in apply if coming from link
        req.execute("updateFlows");
    }

    private static void updateFlowFromMultiJobs(final BaseActivity mActivity, final Dialog alertDialog){
        String userId = ShineSharedPreferences.getCandidateId(mActivity);
        Type type = new TypeToken<UpdateFlowsModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                UpdateFlowsModel model= (UpdateFlowsModel) object;
                DialogUtils.dialogDismiss(alertDialog);
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: Show Update Flow: " + model.next_widget);
                if(model.next_widget==UpdateFlowsFragment.MobileVerificationWidget ||
                        model.next_widget==UpdateFlowsFragment.SkillsWidget ||
                        model.next_widget==UpdateFlowsFragment.CurrentSalaryWidget ||
                        model.next_widget==UpdateFlowsFragment.LinkedinSyncWidget||
                        model.next_widget==UpdateFlowsFragment.TotalExperienceWidget){
                UpdateFlowsFragment frg = UpdateFlowsFragment.newInstance(model);
                frg.show(mActivity.getSupportFragmentManager(), frg.getTag());
                }
                else
                    return;
            }


            @Override
            public void OnDataResponseError(String error, String tag) {
                Log.d("DEBUG::APPLY", "PostApply Info: Inside Update flow: OnDataResponseError: " + error);
                DialogUtils.dialogDismiss(alertDialog);

            }

        },
                URLConfig.UPDATE_FLOW_URL.replace(URLConfig.CANDIDATE_ID, userId), type);
        //TODO:here we have to put also referral_code in apply if coming from link
        req.execute("updateFlows");
    }


    private static void similarJobsFromRedirectJobs(final BaseActivity mActivity, String jobId, final SimpleSearchModel.Result result,
                                                    final Dialog alertDialog) {
        DialogUtils.dialogDismiss(alertDialog);
        if(DialogUtils.showRateIfNeeded(mActivity))
            return;
        Bundle bundle1 = new Bundle();
        bundle1.putString("job_id", jobId);
        bundle1.putString("company_name", result.comp_name);
        bundle1.putBoolean("company_posted", false);
        bundle1.putString("recruiter_url", result.jRUrl);
        SimilarJobs sJ = new SimilarJobs();
        sJ.setArguments(bundle1);
        sJ.show(mActivity.getSupportFragmentManager(), sJ.getTag());
    }


    public static void showCustomApplyRedirectAlerts(final BaseActivity mActivity, final String jobId,  final SimpleSearchModel.Result result) {

        try {
            final Dialog alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_apply_redirect_dialog);

            alertDialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.dialogDismiss(alertDialog);
                }
            });
            alertDialog.findViewById(R.id.go_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.dialogDismiss(alertDialog);
                    final String urlToHit = URLConfig.SUDO_APPLY_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                            .replace(URLConfig.JOB_ID, jobId);
                    final Dialog dialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                    Type type = new TypeToken<ApplyJobModel>() {
                    }.getType();
                    VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                        @Override
                        public void OnDataResponse(Object object, String tag) {
                            if(!result.isInsidePostApply){

                                Log.d("RECRUITER","redirect true");
                                    similarJobsFromRedirectJobs(mActivity, jobId, result, dialog);

                                DialogUtils.dialogDismiss(dialog);

//                                }
                            } else {

                                Log.d("RECRUITER","redirect false");
                                DialogUtils.dialogDismiss(dialog);
                            }
                            ShineCommons.trackShineEvents("Apply", "RecruiterRedirect", mActivity);

                            if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                            {
                                System.out.println("-- package install");
                                ChromeCustomTabs.openCustomTab(mActivity,result.jRUrl);
                            }
                            else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity,CustomWebView.class);
                                bundle.putString("shineurl",result.jRUrl);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);

                            }

                        }

                        @Override
                        public void OnDataResponseError(String error, String tag) {
                            DialogUtils.dialogDismiss(dialog);


                            if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                            {
                                System.out.println("-- package install");
                                ChromeCustomTabs.openCustomTab(mActivity,result.jRUrl);
                            }
                            else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity,CustomWebView.class);
                                bundle.putString("shineurl",result.jRUrl);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);

                            }
                        }

                    },
                            urlToHit, type);
                    req.setUsePutMethod(new HashMap());
                    req.execute("applyjobs");
                }
            });

            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public interface MultiAppliedListener{
        void onMultiApplied(String msg);
    }



    public static void multiApply(final BaseActivity mActivity, final ArrayList<String> mList, final MultiAppliedListener onAppliedListener){

        final Dialog dialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
        Type type = new TypeToken<ApplyJobModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                URLConfig.jobAppliedSet.addAll(mList);
                onAppliedListener.onMultiApplied(mList.size()+" jobs applied successfully.");
                if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
                    ApplyJobsNew.updateFlowFromMultiJobs(mActivity, dialog);
                } else {
                    DialogUtils.showRateIfNeeded(mActivity);
                    DialogUtils.dialogDismiss(dialog);
                }

                ShineCommons.trackShineMultipleAppliesEvents("Apply","Multiple Apply Success",Long.parseLong(mList.size()+""),mActivity);
            }
            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(dialog);
                final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                errorDialog.findViewById(R.id.ok_btn).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogUtils.dialogDismiss(errorDialog);
                            }
                        });

                ShineCommons.trackShineMultipleAppliesEvents("Multiple Apply Failed","Multiple Apply Failed",Long.parseLong(mList.size()+""),mActivity);

            }
        },
                URLConfig.BULK_JOBS_APPLY_API.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)), type);
        HashMap<String, Object> params = new HashMap<>();
        params.put("candidate_id", ShineSharedPreferences.getCandidateId(mActivity));
        params.put("is_auto_apply", "3");
        params.put("job_ids", mList);
        req.setUsePostMethod(params);
        req.execute("applyMultijobs");
    }



}
