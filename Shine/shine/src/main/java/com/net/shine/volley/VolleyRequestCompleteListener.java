package com.net.shine.volley;

import com.android.volley.VolleyError;

public interface VolleyRequestCompleteListener {

    void OnDataResponse(Object object, String tag);

    void OnDataResponseError(String error, String tag);


}