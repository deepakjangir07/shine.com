package com.net.shine.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;

public class JobAlertDetailParentFragment extends BaseFragment implements
        OnPageChangeListener {
    private MyAdapter mAdapter;
    private ViewPager viewPager;
    private int currentIndex;
    private boolean mailType;
    public static String JOB_ALERT_LIST = "job_alert_list";

    private InboxAlertMailModel.Results alertVo;


    public static JobAlertDetailParentFragment newInstance(Bundle args) {

        JobAlertDetailParentFragment fragment = new JobAlertDetailParentFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_detail_view, container, false);

        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();
        mActivity.showSelectedNavButton(mActivity.MAIL_OPTION);

        Bundle intent = getArguments();
        mailType = intent.getBoolean(URLConfig.MAIL_TYPE);
        currentIndex = intent.getInt(URLConfig.INDEX_MAIL, 0);
        ArrayList<InboxAlertMailModel.Results> jobAlertList = (ArrayList<InboxAlertMailModel.Results>) intent
                .getSerializable(JOB_ALERT_LIST);
        if(jobAlertList==null)
            jobAlertList = new ArrayList<>();
        if (jobAlertList.size() > currentIndex)
            alertVo = jobAlertList.get(currentIndex);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);

        mAdapter = new MyAdapter(getChildFragmentManager(), jobAlertList);
        viewPager.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(currentIndex);

        return view;
    }


    public class MyAdapter extends FragmentPagerAdapter {
        ArrayList<InboxAlertMailModel.Results> jobAlertList;
        int count = 0;

        public MyAdapter(FragmentManager fm, ArrayList<InboxAlertMailModel.Results> jobLst) {
            super(fm);
            this.jobAlertList = jobLst;
        }

        @Override
        public int getCount() {

            if (count != jobAlertList.size()) {
                count = jobAlertList.size();
                notifyDataSetChanged();
            }
            return jobAlertList.size();
        }


        @Override
        public Fragment getItem(final int position) {
            if (mailType) {


                alertVo = jobAlertList.get(position);

                Fragment frg = new JobAlertDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(JobAlertDetailFragment.JOB_ALERT_ID,
                        jobAlertList.get(position).id);


                frg.setArguments(bundle);
                return frg;
            } else {


                if(position!=4)
                alertVo = jobAlertList.get(position);
                Fragment frg = new RecruiterMailDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(RecruiterMailDetailFragment.JOB_ALERT_DATA_KEY,
                        jobAlertList.get(position));

                frg.setArguments(bundle);



                return frg;
            }
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();

        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();
        mActivity.showSelectedNavButton(mActivity.MAIL_OPTION);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        SharedPreferences.Editor outState = mActivity.getSharedPreferences(
                "frag", Context.MODE_APPEND).edit();
        outState.putString("fraginfo", "");

        outState.apply();

    }

    public void onPageScrolled(int i, float v, int i1) {
    }

    @Override
    public void onPageSelected(int arg0) {

        if(mailType)
            ManualScreenTracker.manualTracker("JobAlertMailDetail");
        else
            ManualScreenTracker.manualTracker("RecruiterMailDetail");
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            currentIndex = viewPager.getCurrentItem();
        }
    }


}
