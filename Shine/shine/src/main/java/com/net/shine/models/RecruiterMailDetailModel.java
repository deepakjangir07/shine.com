package com.net.shine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RecruiterMailDetailModel implements Serializable {


	@SerializedName("recruiter_name")
	@Expose
	public String recName;

	@SerializedName("hash_code")
	@Expose
	public String hashCode;

	@SerializedName("subject_line")
	@Expose
	public String subjectline;

	@SerializedName("timestamp")
	@Expose
	public String timestamp;


	@SerializedName("context_data")
	@Expose
	public ContextData contextData;

	public class ContextData implements Serializable {

		@SerializedName("minimum_experience")
		public String minexp;

		@SerializedName("maximum_experience")
		public String maxExp;

		@SerializedName("minimum_salary")
		public String minSalary;

		@SerializedName("maximum_salary")
		public String maxSalary ;

		@SerializedName("existing_jobs")
		public String existingjobs;

		@SerializedName("apply_button")
		public int applyButton;

		@SerializedName("job_location")
		public String location;

		@SerializedName("job_title")
		public String jobTitle;

		@SerializedName("email_id")
		public String recMail;

		@SerializedName("email_body")
		public String emailbody;

		@SerializedName("rec_id")
		public int recId;

	}
   
}
