package com.net.shine.fragments.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.ExpandableListAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChildParentSpinnerFrag extends DialogFragment implements View.OnClickListener, ExpandableListView.OnChildClickListener {


    ExpandableListAdapter listAdapter;
    ExpandableListView lv;
    String title;
    TextView tView;
    View view;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, Integer> indexMap;
    List<String> completeList;
    int expandedGroupPos ;
    String selectedChildText = "";



    public ChildParentSpinnerFrag(String title, final TextView tView,
                                  final String[] list) {
        this.title = title;
        this.tView = tView;
        completeList = new ArrayList<>();
        listDataChild = new HashMap<>();
        listDataHeader = new ArrayList<>();
        indexMap = new HashMap<>();
        String currParent = "";
        Integer i =0;
        for (String str : list) {
            completeList.add(str);
            indexMap.put(str, i);
            i++;
            if (str.startsWith("#")) {
                currParent = str.substring(1);
                listDataHeader.add(currParent);
                listDataChild.put(currParent, new ArrayList<String>());
            } else {
                listDataChild.get(currParent).add(str);
            }
        }
    }

    BaseActivity mActivity;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity  = ((BaseActivity) activity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ShineBaseThemeNoOverlay);
    }

    // @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.child_parent_spinner_frag, container, false);

        try {

            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.update_btn).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);

            int curr = (int) tView.getTag();
            //set selected only if selection is not a header
            if(curr>0)
                selectedChildText = completeList.get(curr);


            lv = (ExpandableListView) view.findViewById(R.id.cus_lv_spinner);
            listAdapter = new ExpandableListAdapter(mActivity, listDataHeader, listDataChild);
            int info[] = listAdapter.getChildInfoByName(selectedChildText);
            expandedGroupPos = info[0];
            listAdapter.setSelectedChild(info[0], info[1]);
            lv.setAdapter(listAdapter);
            TextView tv_title = (TextView) view.findViewById(R.id.header_title);
            tv_title.setText(title);



            lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                    if (lv.isGroupExpanded(groupPosition)) {
                        lv.collapseGroup(expandedGroupPos);
                        expandedGroupPos = -1;
                    } else {
                        lv.collapseGroup(expandedGroupPos);
                        expandedGroupPos = groupPosition;
                        lv.expandGroup(groupPosition);
                        return true;
                    }
                    return false;
                }

            });


            lv.setOnChildClickListener(this);

          if(expandedGroupPos>=0)
            {
                lv.expandGroup(expandedGroupPos);
                lv.setSelectedGroup(expandedGroupPos);
            }



            EditText et = (EditText) view.findViewById(R.id.et_spinner_search);



            if(completeList!=null&&completeList.size()<13)
            {
                et.setVisibility(View.GONE);
            }
            else {
                et.setVisibility(View.VISIBLE);
            }


            et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        return true;
                    }
                    return false;
                }
            });

            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    String prefix = s.toString().toLowerCase();

                    HashMap<String, List<String>> newListDataChild = new HashMap<>();
                    List<String> newListDataHeader = new ArrayList<>();
                    for(String parent: listDataHeader)
                    {
                        ArrayList<String> newChildList = new ArrayList<>();
                        List<String> childList = listDataChild.get(parent);
                        for(String child: childList)
                        {
                            if(child.toLowerCase().contains(prefix)) {
                                newChildList.add(child);
                            }
                        }
                        if(newChildList.size()>0)
                        {
                            newListDataHeader.add(parent);
                            newListDataChild.put(parent, newChildList);
                        }
                    }



                    //collapse any prev expanded groups
                    if(expandedGroupPos>=0)
                    {
                        lv.collapseGroup(expandedGroupPos);
                        expandedGroupPos = -1;
                    }


                    listAdapter.refreshLists(newListDataHeader, newListDataChild, -1, -1);
                    //listAdapter.refreshLists(newListDataHeader, newListDataChild);

                    if(prefix.isEmpty())
                    {
                        //restore users expand selection
                        for(int i=0;i<newListDataChild.size();i++)
                        {
                            if(expandedGroupPos==i)
                                lv.expandGroup(i);
                            else
                                lv.collapseGroup(i);
                        }
                    }
                    else
                    {
                        //expand all
                        for(int i=0;i<newListDataChild.size();i++)
                        {
                            lv.expandGroup(i);
                        }
                    }
                    int info[] = listAdapter.getChildInfoByName(selectedChildText);
                    int newExpandedGroupPos = info[0];
                    if(newExpandedGroupPos>=0)
                    {
                        expandedGroupPos = newExpandedGroupPos;
                        listAdapter.setSelectedChild(info[0], info[1]);
                        lv.expandGroup(expandedGroupPos);
                        lv.setSelectedGroup(expandedGroupPos);
                    }


                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        return view;
    }


    @Override
    public void onClick(View v) {
//        super.onClick(v);

        switch (v.getId()) {
            case R.id.update_btn:
                try {
                    mActivity.getSupportFragmentManager().popBackStackImmediate();

                    String text;
                    if (URLConfig.NO_SELECTION
                            .equals(selectedChildText)) {
                        text = "";
                    } else {
                        text = selectedChildText;
                    }
                    ShineCommons.hideKeyboard(mActivity);
                    int selected_pos;
                    if(!selectedChildText.isEmpty())
                    {
                         selected_pos = indexMap.get(selectedChildText);
                    }
                    else
                    {
                        selected_pos = -1;
                    }

//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
                    tView.setText(text);
                    tView.setTag(selected_pos);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case R.id.cancel_btn:
                mActivity.popone();
                break;
        }


    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        listAdapter.setSelectedChild(groupPosition, childPosition);
        selectedChildText = ((TextView)v).getText().toString();

        try {
            mActivity.getSupportFragmentManager().popBackStackImmediate();

            String text;
            if (URLConfig.NO_SELECTION
                    .equals(selectedChildText)) {
                text = "";
            } else {
                text = selectedChildText;
            }
            ShineCommons.hideKeyboard(mActivity);
            int selected_pos;
            if(!selectedChildText.isEmpty())
            {
                selected_pos = indexMap.get(selectedChildText);
            }
            else
            {
                selected_pos = -1;
            }

//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
            tView.setText(text);
            tView.setTag(selected_pos);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
