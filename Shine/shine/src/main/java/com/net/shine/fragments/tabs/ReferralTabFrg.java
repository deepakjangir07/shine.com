package com.net.shine.fragments.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.net.shine.fragments.ReceiveReferralsFrg;
import com.net.shine.fragments.RequestReferralsFrg;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ujjawal-work on 22/08/16.
 */

public class ReferralTabFrg extends TwoTabFrg {


    public static final int REQUESTED_REF_TAB = 0;
    public static final int RECEIVED_REF_TAB = 1;


    public static ReferralTabFrg newInstance(Bundle args) {
        ReferralTabFrg fragment = new ReferralTabFrg();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentSelected = getArguments().getInt(SELECTED_TAB, REQUESTED_REF_TAB);
    }

    @Override
    public ArrayList<String> getNames() {
        ArrayList<String> tabName = new ArrayList<>();
        tabName.add("Requested");
        tabName.add("Received");
        return tabName;
    }


    @Override
    protected void PageSelected(int position) {
        super.PageSelected(position);
        if(position==0) {
            ManualScreenTracker.manualTracker("ReferralInbox-Requested");
        }
        else if(position==1) {
            ManualScreenTracker.manualTracker("ReferralInbox-Received");
        }

    }

    @Override
    public List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();
        RequestReferralsFrg f1 = RequestReferralsFrg.newInstance();
        ReceiveReferralsFrg f2 = ReceiveReferralsFrg.newInstance();
        fList.add(f1);
        fList.add(f2);
        return fList;
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Referrals");
        mActivity.showNavBar();
        mActivity.showBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showSelectedNavButton(mActivity.MORE_OPTION);
    }
}
