package com.net.shine.models;

import java.io.Serializable;

public class ActionBarState implements Serializable {

    public static final String PREV_ACTION_STATE_KEY = "prevActionBarState";

    public static final int ACTION_BAR_STATE_UP = 1;

    public static final int ACTION_BAR_STATE_BURGER = 2;

    public String title="";

    public int state = 0;


}
