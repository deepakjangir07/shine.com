package com.net.shine.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.JobListRecyclerAdapterForAds;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.adapters.RelevantSuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.RecentSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.views.LocationAutoComplete;
import com.net.shine.views.RelevantAutoComplete;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ujjawal-work on 16/08/16.
 */

public class LoginHomeFrg extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener {

    LinearLayout recent_search_layout, expandedSearch;
    TagView recent_search;
    Bundle bundle = null;
    Intent intent = null;
    private View view, searchBtn;
    private UserStatusModel userProfileVO;
    private RelevantAutoComplete keywordText;
    private LocationAutoComplete locationText;
    private TextView experienceChoice, minSalaryField, funcAreaField, industryField, moreBtn, clearAll;
    private TextInputLayout inputLayoutKeyword;
    private ArrayList<String> recentTagList = new ArrayList<String>();
    private ArrayList<SimpleSearchVO> mList = new ArrayList<>();

    public static LoginHomeFrg newInstance() {
        Bundle args = new Bundle();
        LoginHomeFrg fragment = new LoginHomeFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_screen_loggedin, container, false);

        try {
            initialize();

            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
            mActivity.hideBackButton();
            mActivity.showNavBar();
            mActivity.setTitle("Search");
            mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    void initialize() {


        recent_search = (TagView) view.findViewById(R.id.recent_textview1);
        recent_search_layout = (LinearLayout) view.findViewById(R.id.recent_search_layout);
        recent_search_layout.setVisibility(View.GONE);

        inputLayoutKeyword = (TextInputLayout) view.findViewById(R.id.input_layout_keyword);
        keywordText = (RelevantAutoComplete) view.findViewById(R.id.keyword);
        keywordText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                URLConfig.KEYWORD_SUGGESTION));

        keywordText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("item_clicked", keywordText.getText() + "");
                keywordText.setSelection(keywordText.getText().length());

            }
        });
        keywordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    submitSearch();
                    return true;
                }

                return false;
            }
        });


        locationText = (LocationAutoComplete) view.findViewById(R.id.location);
        locationText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                URLConfig.CITY_LIST));
        locationText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("item_clicked", locationText.getText() + "");
                locationText.setSelection(locationText.getText().length());
            }
        });
        locationText
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            submitSearch();
                            return true;
                        }
                        return false;
                    }
                });


        experienceChoice = (TextView) view.findViewById(R.id.min_exp_field);
        int experienceTag = 0;
        try {
            if (experienceChoice != null && experienceChoice.getTag() != null)
                experienceTag = (int) experienceChoice.getTag();
        } catch (Exception e) {
            e.printStackTrace();
        }
        experienceChoice.setTag(experienceTag);
        TextChangeListener.addListener(keywordText, inputLayoutKeyword, mActivity);
        experienceChoice.setOnClickListener(this);
        experienceChoice.setOnFocusChangeListener(this);


        int minSalaryTag = 0;
        minSalaryField = (TextView) view.findViewById(R.id.min_sal_field);
        minSalaryField.setTag(minSalaryTag);
        minSalaryField.setOnClickListener(this);
        minSalaryField.setOnFocusChangeListener(this);

        int funcAreaTag = 0;
        funcAreaField = (TextView) view.findViewById(R.id.func_area_field);
        funcAreaField.setTag(funcAreaTag);
        funcAreaField.setOnClickListener(this);
        funcAreaField.setOnFocusChangeListener(this);

        int industryTag = 0;
        industryField = (TextView) view
                .findViewById(R.id.industry_type_field);
        industryField.setTag(industryTag);
        industryField.setOnClickListener(this);
        industryField.setOnFocusChangeListener(this);

        moreBtn = (TextView) view.findViewById(R.id.more);
        moreBtn.setOnClickListener(this);

        searchBtn = view.findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(this);

        clearAll = (TextView) view.findViewById(R.id.clearall);
        clearAll.setOnClickListener(this);


        expandedSearch = (LinearLayout) view.findViewById(R.id.adv_search_fields);
        mList = (ArrayList<SimpleSearchVO>) MyApplication
                .getDataBase().getRecentSearchList();

        Log.d("Mlist ", "size OnResume " + mList.size());
        if (recentTagList.size() > 0) {
            recentTagList.clear();
            recent_search.removeAll();

        }
        showRecentSearch(mList);

    }

    @Override
    public void onResume() {
        super.onResume();

        mActivity.hideBackButton();
        mActivity.showNavBar();
        mActivity.setTitle("Search");
        mActivity.setActionBarVisible(true);
        if(JobListRecyclerAdapterForAds.actionMode!=null)
            JobListRecyclerAdapterForAds.actionMode.finish();

        if(JobsListRecyclerAdapter.actionMode!=null)
            JobsListRecyclerAdapter.actionMode.finish();

        mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);


        if (MyApplication.getDataBase().getRecentSearchList().isEmpty()) {

            if (!TextUtils.isEmpty(ShineSharedPreferences.getUserAccessToken(mActivity))) {
                recent_search_layout.setVisibility(View.GONE);
                Type listType = new TypeToken<RecentSearchModel[]>() {
                }.getType();

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                    @Override
                    public void OnDataResponse(Object object, String tag) {

                        mList = (ArrayList<SimpleSearchVO>) MyApplication
                                .getDataBase().getRecentSearchList();
                        RecentSearchModel[] models = (RecentSearchModel[]) object;
                        if (models != null && models.length > 0) {
                            RecentSearchModel model = models[0];
                            if (model.searches_list != null) {
                                for (RecentSearchModel.RecentSearchQuery query : model.searches_list) {
                                    SimpleSearchVO simpleSearchVO = new SimpleSearchVO();
                                    simpleSearchVO.setKeyword(query.keyword);
                                    simpleSearchVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID,
                                            userProfileVO.candidate_id));
                                    mList.add(simpleSearchVO);
                                    recentSearch(simpleSearchVO);
                                }
                            }
                        }
                        if (recentTagList.size() > 0) {
                            recentTagList.clear();
                            recent_search.removeAll();
                        }
                        showRecentSearch(mList);
                    }

                    @Override
                    public void OnDataResponseError(String error, String tag) {
                        mList = (ArrayList<SimpleSearchVO>) MyApplication
                                .getDataBase().getRecentSearchList();
                        if (recentTagList.size() > 0) {
                            recentTagList.clear();
                            recent_search.removeAll();
                        }
                        showRecentSearch(mList);

                    }
                }, URLConfig.RECENT_SEARCHES_API.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)), listType);
                downloader.execute("recent-search");
            }
        } else {


            mList = (ArrayList<SimpleSearchVO>) MyApplication
                    .getDataBase().getRecentSearchList();

            if (recentTagList.size() > 0) {
                recentTagList.clear();
                recent_search.removeAll();

            }
            showRecentSearch(mList);
        }

        ManualScreenTracker.manualTracker("HomeMenu-LoggedIn");
    }

    private void showRecentSearch(final ArrayList<SimpleSearchVO> list) {


        final ArrayList<String> recentSearchList = new ArrayList<>();
        for (SimpleSearchVO svo : list) {
            String str = svo.getKeyword().toString().trim();
            if (str.charAt(str.length() - 1) == ',') {
                str = str.substring(0, str.length() - 1);
            }


            recentSearchList.add(str);
        }


        recent_search.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(Tag tag, int i) {

                try {
                    String str = tag.text;

                    ShineCommons.trackShineEvents("Search", "RecentSearch", mActivity);

                    Log.d("RECENT_DB", "svo " + mList.get(recentSearchList.indexOf(str)));
                    showSearchResultView(mList.get(recentSearchList.indexOf(str)));


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        for (int i = 0; i < recentSearchList.size(); i++) {
            Log.d("inside tag", recentSearchList.size() + "");
            if (!TextUtils.isEmpty(recentSearchList.get(i))) {
                String str = recentSearchList.get(i);
                if (str.charAt(str.length() - 1) == ',') {
                    str = str.substring(0, str.length() - 1);
                }

                final String finalStr = str;
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRecentTag(finalStr);

                    }
                });
            }

        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn:
                ShineCommons.hideKeyboard(mActivity);
                submitSearch();
                break;

            case R.id.more:
                if (moreBtn.getText().toString().trim().equalsIgnoreCase("more")) {
                    expandedSearch.setVisibility(View.VISIBLE);
                    moreBtn.setText("less");
                } else {
                    expandedSearch.setVisibility(View.GONE);
                    moreBtn.setText("more");
                }
                break;

            case R.id.clearall:
                keywordText.setText("");
                locationText.setText("");
                experienceChoice.setTag(0);
                experienceChoice.setText("");
                minSalaryField.setTag(0);
                minSalaryField.setText("");
                funcAreaField.setTag(0);
                funcAreaField.setText("");
                industryField.setTag(0);
                industryField.setText("");
                break;

            default:
                showSpinner(v.getId());
                ShineCommons.hideKeyboard(mActivity);
                break;

        }
    }

    private void submitSearch() {
        try {
            String keyword = keywordText.getText().toString().trim();
            String location = locationText.getText().toString().trim();
            String minExp = experienceChoice.getText().toString().trim();
            String minSal = minSalaryField.getText().toString().trim();
            String funcArea = funcAreaField.getText().toString().trim();
            String indusType = industryField.getText().toString().trim();
            if (keyword.length() == 0) {
                inputLayoutKeyword.setError(getString(R.string.sim_search_keyword_empty_msg));
                return;
            }

            if (location.trim().length() == 0) {
                location = "";
            }

            String minExpTag = "";
            if (minExp.length() == 0) {
                minExpTag = "0";
            } else {
                minExpTag = URLConfig.EXPERIENCE_MAP.get(minExp);
            }

            String minSalTag = "";
            if (minSal.length() == 0) {
                minSal = "None";
                minSalTag = "-1";
            } else {
                minSalTag = URLConfig.ANNUAL_SALARY_MAP.get(minSal);
            }

            if (funcArea.length() == 0) {
                funcArea = "";
            }

            if (indusType.length() == 0) {
                indusType = "";
            }

            SimpleSearchVO sVO = new SimpleSearchVO();
            if (userProfileVO != null) {
                String URL = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
                sVO.setUrl(URL);

            } else {
                sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);

            }
            sVO.setKeyword(keyword);
            sVO.setLocation(location);
            sVO.setMinExp(minExp);
            sVO.setMinExpTag(Integer.parseInt(minExpTag));

            sVO.setFuncArea(funcArea);
            sVO.setFuncAreaTag((Integer) funcAreaField.getTag());

            sVO.setIndusType(indusType);
            sVO.setIndusTypeTag((Integer) industryField.getTag());

            sVO.setMinSal(minSal);
            if (minSal.equals(URLConfig.NO_SELECTION)) {
                sVO.setMinSalTag(-1);
            } else {
                sVO.setMinSalTag(Integer.parseInt(minSalTag));

            }

            BaseFragment fragment = new SearchResultFrg();
            Bundle bundle = new Bundle();
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            fragment.setArguments(bundle);
            recentSearch(sVO);
            mActivity.replaceFragmentWithBackStack(fragment);
            mList.add(sVO);
            ShineCommons.trackShineEvents("Search", "Home", mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void recentSearch(final SimpleSearchVO sVO) {
        try {
            Runnable runnable = new Runnable() {

                @Override
                public void run() {

                    MyApplication.getDataBase().addRecentSearch(sVO);
                }
            };
            new Thread(runnable).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setRecentTag(final String myRecentSearch) {

        Log.d("inside recent tag", myRecentSearch);
        Tag tag = new Tag(myRecentSearch);
        tag.layoutBorderColor = Color.parseColor("#f6f6f6");
        tag.layoutColor = Color.WHITE;
        tag.tagTextColor = Color.parseColor("#3E7BBE");
        tag.tagTextSize = 12f;
        tag.layoutColorPress = Color.parseColor("#d3d3d3");
        tag.layoutBorderSize = 1f;
        tag.radius = 2f;
        recent_search.addTag(tag);
        recentTagList.add(myRecentSearch);


        recent_search_layout.setVisibility(View.VISIBLE);


    }


    private void showSearchResultView(SimpleSearchVO sVO) {
        try {


            BaseFragment fragment = new SearchResultFrg();
            Bundle bundle = new Bundle();
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            fragment.setArguments(bundle);
            mActivity.replaceFragmentWithBackStack(fragment);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }

    }

    private void showSpinner(int id) {
        try {
            switch (id) {
                case R.id.min_exp_field:

                    String[] expList = new String[URLConfig.EXPERIENCE_LIST2.length + 1];
                    expList[0] = "None";
                    for (int i = 0; i < URLConfig.EXPERIENCE_LIST2.length; i++) {
                        expList[i + 1] = URLConfig.EXPERIENCE_LIST2[i];
                    }

                    List<String> mainExpList=Arrays.asList(expList);

                    if(!TextUtils.isEmpty(experienceChoice.getText()))
                        experienceChoice.setTag(mainExpList.indexOf(experienceChoice.getText().toString()));
                    else
                        experienceChoice.setTag(-1);

                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp), experienceChoice,
                            expList, mActivity);

                    break;
                case R.id.min_sal_field:

                    String[] salList = new String[URLConfig.ANNUAL_SALARY_LIST.length + 1];
                    salList[0] = "None";
                    for (int i = 0; i < URLConfig.ANNUAL_SALARY_LIST.length; i++) {
                        salList[i + 1] = URLConfig.ANNUAL_SALARY_LIST[i];
                    }


                    List<String> s = Arrays.asList(salList);
                    if(minSalaryField.getText()!=null)
                        minSalaryField.setTag(s.indexOf(minSalaryField.getText().toString()));
                    else
                        minSalaryField.setTag(-1);



                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
                            minSalaryField, salList, mActivity);
                    break;
                case R.id.func_area_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_func_area), funcAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST3, mActivity);

                    break;
                case R.id.industry_type_field:

                    String[] indList = new String[URLConfig.INDUSTRY_LIST3.length + 1];
                    indList[0] = "None";
                    for (int i = 0; i < URLConfig.INDUSTRY_LIST3.length; i++) {
                        indList[i + 1] = URLConfig.INDUSTRY_LIST3[i];
                    }
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_industry), industryField,
                            indList, mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

