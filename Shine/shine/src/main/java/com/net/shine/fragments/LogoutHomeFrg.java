package com.net.shine.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cunoraz.tagview.Tag;
import com.cunoraz.tagview.TagView;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.RelevantSuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.views.LocationAutoComplete;
import com.net.shine.views.RelevantAutoComplete;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LogoutHomeFrg extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener {

    RelevantAutoComplete keywordText;
    LocationAutoComplete locationText;
    LinearLayout recent_search_layout, bottomBar, expandedSearch;
    TextView experienceChoice, moreBtn, login, register, minSalaryField, funcAreaField, industryField, clearAll;
    TagView recent_search;
    ArrayList<SimpleSearchVO> mList;
    TextInputLayout inputLayoutKeyword;
    View searchBtn, view;
    private ArrayList<String> recentTagList = new ArrayList<String>();

    public static LogoutHomeFrg newInstance() {

        Bundle args = new Bundle();

        LogoutHomeFrg fragment = new LogoutHomeFrg();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            mActivity.hideBackButton();
            mActivity.showNavBar();
            mActivity.setTitle("Search");
            mActivity.setActionBarVisible(true);

            mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);
            ManualScreenTracker.manualTracker("HomeMenu-Non Logged");
            mList = (ArrayList<SimpleSearchVO>) MyApplication
                    .getDataBase().getRecentSearchList();
            if (recentTagList.size() > 0) {
                recentTagList.clear();
                recent_search.removeAll();
            }

            for (int i = 0; i < mList.size(); i++) {
                Log.d("Mlist ", "keyword  " + mList.get(i).getKeyword());
            }

            showRecentSearch(mList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);


    }

    void initialize() {

        bottomBar = (LinearLayout) view.findViewById(R.id.LL_bottom);
        login = (TextView) view.findViewById(R.id.login_btn);
        register = (TextView) view.findViewById(R.id.register_btn);
        recent_search = (TagView) view.findViewById(R.id.recent_textview1);
        recent_search_layout = (LinearLayout) view.findViewById(R.id.recent_search_layout);
        recent_search_layout.setVisibility(View.INVISIBLE);
        login.setOnClickListener(this);
        register.setOnClickListener(this);


        inputLayoutKeyword = (TextInputLayout) view.findViewById(R.id.input_layout_keyword);
        keywordText = (RelevantAutoComplete) view.findViewById(R.id.keyword);
        keywordText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                URLConfig.KEYWORD_SUGGESTION));

        keywordText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("item_clicked", keywordText.getText() + "");
                keywordText.setSelection(keywordText.getText().length());

            }
        });
        keywordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    submitSearch();
                    return true;
                }

                return false;
            }
        });

        locationText = (LocationAutoComplete) view.findViewById(R.id.location);
        locationText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                URLConfig.CITY_LIST));
        locationText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("item_clicked", locationText.getText() + "");
                locationText.setSelection(locationText.getText().length());
            }
        });
        locationText
                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId,
                                                  KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            submitSearch();
                            return true;
                        }
                        return false;
                    }
                });

        experienceChoice = (TextView) view.findViewById(R.id.min_exp_field);
        int experienceTag = 0;
        try {
            if (experienceChoice != null && experienceChoice.getTag() != null)
                experienceTag = (int) experienceChoice.getTag();
        } catch (Exception e) {
            e.printStackTrace();
        }
        experienceChoice.setTag(experienceTag);
        TextChangeListener.addListener(keywordText, inputLayoutKeyword, mActivity);
        experienceChoice.setOnClickListener(this);
        experienceChoice.setOnFocusChangeListener(this);


        int minSalaryTag = 0;
        minSalaryField = (TextView) view.findViewById(R.id.min_sal_field);
        minSalaryField.setTag(minSalaryTag);
        minSalaryField.setOnClickListener(this);
        minSalaryField.setOnFocusChangeListener(this);

        int funcAreaTag = 0;
        funcAreaField = (TextView) view.findViewById(R.id.func_area_field);
        funcAreaField.setTag(funcAreaTag);
        funcAreaField.setOnClickListener(this);
        funcAreaField.setOnFocusChangeListener(this);

        int industryTag = 0;
        industryField = (TextView) view
                .findViewById(R.id.industry_type_field);
        industryField.setTag(industryTag);
        industryField.setOnClickListener(this);
        industryField.setOnFocusChangeListener(this);

        moreBtn = (TextView) view.findViewById(R.id.more);
        moreBtn.setOnClickListener(this);

        searchBtn = view.findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(this);

        clearAll = (TextView) view.findViewById(R.id.clearall);
        clearAll.setOnClickListener(this);

        expandedSearch = (LinearLayout) view.findViewById(R.id.adv_search_fields);
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = inflater.inflate(R.layout.home_screen_loggedout, container, false);
        try {
            initialize();

            mActivity.hideBackButton();
            mActivity.showNavBar();
            mActivity.setTitle("Search");
            mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    Bundle bundle = null;
    Intent intent = null;

    @Override
    public void onClick(View v) {
        try {

            switch (v.getId()) {

                case R.id.search_btn:

                    ShineCommons.hideKeyboard(mActivity);

                    submitSearch();

                    break;
                case R.id.login_btn:
                    bundle = new Bundle();
                    bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                    BaseFragment fragment = new LoginFrg();
                    fragment.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment);
                    break;

                case R.id.register_btn:
                    bundle = new Bundle();
                    bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                    BaseFragment fragment1 = new RegistrationFrg();
                    fragment1.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment1);
                    break;

                case R.id.more:
                    if (moreBtn.getText().toString().trim().equalsIgnoreCase("more")) {
                        expandedSearch.setVisibility(View.VISIBLE);
                        moreBtn.setText("less");
                    } else {
                        expandedSearch.setVisibility(View.GONE);
                        moreBtn.setText("more");
                    }
                    break;

                case R.id.clearall:
                    keywordText.setText("");
                    locationText.setText("");
                    experienceChoice.setTag(0);
                    experienceChoice.setText("");
                    minSalaryField.setTag(0);
                    minSalaryField.setText("");
                    funcAreaField.setTag(0);
                    funcAreaField.setText("");
                    industryField.setTag(0);
                    industryField.setText("");
                    break;
                default:
                    showSpinner(v.getId());
                    ShineCommons.hideKeyboard(mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void submitSearch() {
        try {
            String keyword = keywordText.getText().toString().trim();
            String location = locationText.getText().toString().trim();
            String minExp = experienceChoice.getText().toString().trim();
            String minSal = minSalaryField.getText().toString().trim();
            String funcArea = funcAreaField.getText().toString().trim();
            String indusType = industryField.getText().toString().trim();
            if (keyword.length() == 0) {
                inputLayoutKeyword.setError(getString(R.string.sim_search_keyword_empty_msg));
                return;
            }

            if (location.trim().length() == 0) {
                location = "";
            }

            String minExpTag = "";
            if (minExp.length() == 0) {
                minExpTag = "0";
            } else {
                minExpTag = URLConfig.EXPERIENCE_MAP.get(minExp);
            }

            String minSalTag = "";
            if (minSal.length() == 0) {
                minSal = "None";
                minSalTag = "-1";
            } else {
                minSalTag = URLConfig.ANNUAL_SALARY_MAP.get(minSal);
            }

            if (funcArea.length() == 0) {
                funcArea = "";
            }

            if (indusType.length() == 0) {
                indusType = "";
            }

            SimpleSearchVO sVO = new SimpleSearchVO();
            sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
            sVO.setKeyword(keyword);
            sVO.setLocation(location);
            sVO.setMinExp(minExp);
            sVO.setMinExpTag(Integer.parseInt(minExpTag));

            sVO.setFuncArea(funcArea);
            sVO.setFuncAreaTag((Integer) funcAreaField.getTag());

            sVO.setIndusType(indusType);
            sVO.setIndusTypeTag((Integer) industryField.getTag());

            sVO.setMinSal(minSal);
            if (minSal.equals(URLConfig.NO_SELECTION)) {
                sVO.setMinSalTag(-1);
            } else {
                sVO.setMinSalTag(Integer.parseInt(minSalTag));

            }

            BaseFragment fragment = new SearchResultFrg();
            Bundle bundle = new Bundle();
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            fragment.setArguments(bundle);
            recentSearch(sVO);
            mActivity.replaceFragmentWithBackStack(fragment);
            Log.d("Mlist Size ", "size " + mList.size());
            mList.add(sVO);
            Log.d("Mlist Size ", "size after add " + mList.size());
            ShineCommons.trackShineEvents("Search", "Home", mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void recentSearch(final SimpleSearchVO sVO) {
        try {
            Runnable runnable = new Runnable() {

                @Override
                public void run() {

                    MyApplication.getDataBase().addRecentSearch(sVO);
                }
            };
            new Thread(runnable).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showRecentSearch(final ArrayList<SimpleSearchVO> list) {


        final ArrayList<String> recentSearchList = new ArrayList<>();
        for (SimpleSearchVO svo : list) {

            String str = svo.getKeyword().toString().trim();
            if (str.charAt(str.length() - 1) == ',') {
                str = str.substring(0, str.length() - 1);
            }


            recentSearchList.add(str);
        }


        recent_search.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(Tag tag, int i) {
                try {
                    String str = tag.text;

                    ShineCommons.trackShineEvents("Search", "RecentSearch", mActivity);


                    showSearchResultView(mList.get(recentSearchList.indexOf(str)));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        for (int i = 0; i < recentSearchList.size(); i++) {
            if (!TextUtils.isEmpty(recentSearchList.get(i).toString().trim())) {

                String str = recentSearchList.get(i).toString().trim();
                if (str.charAt(str.length() - 1) == ',') {
                    str = str.substring(0, str.length() - 1);
                }

                setRecentTag(str);


            }

        }
    }

    private void setRecentTag(final String myRecentSearch) {
        Log.d("display tag", myRecentSearch + "");
        recent_search_layout.setVisibility(View.VISIBLE);


        recent_search.postDelayed(new Runnable() {
            @Override
            public void run() {
                Tag tag = new Tag(myRecentSearch);
                tag.layoutBorderColor = Color.parseColor("#f6f6f6");
                tag.layoutColor = Color.WHITE;
                tag.tagTextColor = Color.parseColor("#3E7BBE");
                tag.tagTextSize = 12f;
                tag.layoutColorPress = Color.parseColor("#d3d3d3");
                tag.layoutBorderSize = 1f;
                tag.radius = 2f;
                recent_search.addTag(tag);
                recentTagList.add(myRecentSearch);


            }
        }, 0);
    }

    private void showSearchResultView(SimpleSearchVO sVO) {
        try {

            Bundle bundle = new Bundle();

            Log.d("Mlist ", "key word for search " + sVO.getKeyword());
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            Fragment fragment = SearchResultFrg.newInstance(bundle);
            mActivity.replaceFragmentWithBackStack(fragment);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }

    }

    private void showSpinner(int id) {
        try {
            switch (id) {
                case R.id.min_exp_field:

                    String[] expList = new String[URLConfig.EXPERIENCE_LIST2.length + 1];
                    expList[0] = "None";
                    for (int i = 0; i < URLConfig.EXPERIENCE_LIST2.length; i++) {
                        expList[i + 1] = URLConfig.EXPERIENCE_LIST2[i];
                    }

                    List<String> mainExpList=Arrays.asList(expList);

                    if(!TextUtils.isEmpty(experienceChoice.getText()))
                        experienceChoice.setTag(mainExpList.indexOf(experienceChoice.getText().toString()));
                    else
                        experienceChoice.setTag(-1);

                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp), experienceChoice,
                            expList, mActivity);

                    break;
                case R.id.min_sal_field:

                    String[] salList = new String[URLConfig.ANNUAL_SALARY_LIST.length + 1];
                    salList[0] = "None";
                    for (int i = 0; i < URLConfig.ANNUAL_SALARY_LIST.length; i++) {
                        salList[i + 1] = URLConfig.ANNUAL_SALARY_LIST[i];
                    }

                    List<String> s = Arrays.asList(salList);

                    if(minSalaryField.getText()!=null)
                        minSalaryField.setTag(s.indexOf(minSalaryField.getText().toString()));
                    else
                        minSalaryField.setTag(-1);



                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
                            minSalaryField, salList, mActivity);
                    break;
                case R.id.func_area_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_func_area), funcAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST3, mActivity);

                    break;
                case R.id.industry_type_field:

                    String[] indList = new String[URLConfig.INDUSTRY_LIST3.length + 1];
                    indList[0] = "None";
                    for (int i = 0; i < URLConfig.INDUSTRY_LIST3.length; i++) {
                        indList[i + 1] = URLConfig.INDUSTRY_LIST3[i];
                    }
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_industry), industryField,
                            indList, mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
