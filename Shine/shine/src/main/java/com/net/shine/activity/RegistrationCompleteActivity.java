package com.net.shine.activity;

import android.os.Bundle;

import com.net.shine.R;
import com.net.shine.fragments.RegistrationCompleteFrg;
import com.net.shine.models.ActionBarState;

/**
 * Created by Deepak on 11/01/17.
 */

public class RegistrationCompleteActivity extends BaseActivity {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setLeftDrawer(DRAWER_REFERRAL);
        if(savedInstanceState==null){
            setBaseFrg(RegistrationCompleteFrg.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(getString(R.string.register_complete));
        //syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
    }


}
