package com.net.shine.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.DiscoverFriendRecyclerViewAdapter;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.models.ActionBarState;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

/**
 * Created by manishpoddar on 06/01/17.
 */

public class ThanksAppliedFullFrg extends BaseFragment{

    public static String THANKS_FRG_TITLE = "Confirmation";
    public static String REF_FRG_TITLE = "Get Referrals";
    DiscoverModel.Company dfm;
    SimpleSearchModel.Result srVo;
    InboxAlertMailDetailModel.InboxJobDetail javo;
    DiscoverFriendRecyclerViewAdapter adapter;
    View inflatedView;
    private boolean wasAlreadyApplied = false;
    Bundle bundle;

    public ThanksAppliedFullFrg() {

    }

    public static ThanksAppliedFullFrg newInstance(Bundle bundle)
    {
        ThanksAppliedFullFrg fragment = new ThanksAppliedFullFrg();

        fragment.setArguments(bundle);

        return fragment;
    }

//    public static ThanksAppliedFrg newInstance(DiscoverModel.Company dfm, SimpleSearchModel.Result srvo, boolean wasAlreadyApplied) {
//
//        Bundle args = new Bundle();
//        args.putSerializable("dfm", dfm);
//        args.putSerializable("srvo", srvo);
//        args.putSerializable("wasAlreadyApplied", wasAlreadyApplied);
//        ThanksAppliedFrg fragment = new ThanksAppliedFrg();
//        fragment.setArguments(args);
//        return fragment;
//    }

//    public static ThanksAppliedFrg newInstance(DiscoverModel.Company dfm, InboxAlertMailDetailModel.InboxJobDetail jamp, boolean wasAlreadyApplied) {
//
//        Bundle args = new Bundle();
//        args.putSerializable("dfm", dfm);
//        args.putSerializable("jamp", jamp);
//        args.putSerializable("wasAlreadyApplied", wasAlreadyApplied);
//        ThanksAppliedFrg fragment = new ThanksAppliedFrg();
//        fragment.setArguments(args);
//        return fragment;
//    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        inflatedView = inflater.inflate(
                R.layout.apply_frnd_list_frg, container, false);
        setHasOptionsMenu(true);

        inflatedView.findViewById(R.id.popup_image).setVisibility(View.GONE);

        try {
            bundle=getArguments();

            dfm= (DiscoverModel.Company) bundle.getSerializable("dfm");
            srVo= (SimpleSearchModel.Result) bundle.getSerializable("srvo");

            javo= (InboxAlertMailDetailModel.InboxJobDetail) bundle.getSerializable("javo");

            wasAlreadyApplied=bundle.getBoolean("wasAlreadyApplied");
        } catch (Exception e) {
            e.printStackTrace();
          //  mActivity.finish();
        }

        if (!wasAlreadyApplied)
            mActivity.setTitle(THANKS_FRG_TITLE);
        else
            mActivity.setTitle(REF_FRG_TITLE);
        RecyclerView list = (RecyclerView) inflatedView.findViewById(R.id.frnd_list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));


        if (wasAlreadyApplied) {
            RelativeLayout thanks = (RelativeLayout) inflatedView.findViewById(R.id.apply_thanks_layout);
            thanks.setVisibility(View.GONE);
        }

        TextView title = (TextView) inflatedView.findViewById(R.id.headline);
        title.setText(Html.fromHtml("<b>" + dfm.data.friends.size() + "</b>" + " connections can refer you in "
                + "<b>" + dfm.company + "</b>"));

        if (srVo != null) {
            adapter = new DiscoverFriendRecyclerViewAdapter(mActivity, dfm, 4, srVo);
        } else {
            adapter = new DiscoverFriendRecyclerViewAdapter(mActivity, dfm, 4, javo);
        }
        adapter.setConnected_through(DiscoverFriendRecyclerViewAdapter.MATCH_JOB);
        list.setAdapter(adapter);

        if (!ShineSharedPreferences.getLinkedinImported(mActivity)) {
            TextView tv = (TextView) inflatedView.findViewById(R.id.linkedin_after_applied);
            tv.setVisibility(View.VISIBLE);

            String text_before_linkedIn = "To discover more friends for referral, connect your ";
            String text_after_linkedIn = "LinkedIn account";
            SpannableString ss = new SpannableString(text_before_linkedIn + text_after_linkedIn);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {




                    UserStatusModel userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);


                    LinkedInCallBack mCallback = new LinkedInCallBack() {
                        @Override
                        public void onLinkedInConnected(String accessToken, String expiry, Dialog customDialog) {
                            super.onLinkedInConnected(accessToken, expiry, customDialog);
                            if (accessToken == null ||  TextUtils.isEmpty(accessToken)) {
                                return;
                            }
                            LinkedinConstants.FetchAndDumpConnectionsToServer(mActivity, accessToken, this, customDialog);
                        }
                        @Override
                        public void onLinkedInSuccess() {
                            super.onLinkedInSuccess();
                            inflatedView.findViewById(R.id.linkedin_after_applied).setVisibility(View.GONE);
                            ShineSharedPreferences.updateSynctimeLinkedIn(mActivity, System.currentTimeMillis());

                            //ToDO: Remove JOb and discover caching??
                            //        new DiscoverFragmentCaching().removeObject(mActivity);
                            //        new JobListCaching().removeObject(mActivity);
                            URLConfig.isRecentSync = true;
                            try {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        try {
                                            URLConfig.isRecentSync = false;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, (5 * 60 * 1000));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            ShineSharedPreferences.saveLinkedinImported(mActivity,
                                    true);
                            MyApplication.isLinkedInSyncing = false;
                            if (KonnectFrag.getInstance() != null) {
                                KonnectFrag.getInstance().onInstanceSuccesLinkedIn();
                            } else {
                                Toast.makeText(
                                        mActivity,
                                        // "LinkedIn  contacts synced. Continue importing from other sources OR Skip to browse your network.",
                                        "Your linkedIn friends are now ready to refer you for jobs in their companies, start applying now",
                                        Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onLinkedInFailed() {
                            super.onLinkedInFailed();
                            MyApplication.isLinkedInSyncing = false;
                            if (isAdded() && isVisible())

                                if (KonnectFrag.getInstance() != null) {
                                    KonnectFrag.getInstance().onInstanceFailedLinkedIn();
                                } else {
                                    Toast.makeText(
                                            mActivity,
                                            "LinkedIn sync failed. Please try again in some time.",
                                            Toast.LENGTH_SHORT).show();
                                }
                        }
                    };

                    String accessToken = ShineSharedPreferences.getLinkedinAccessToken(mActivity);
                    if (accessToken.equals("") || ShineSharedPreferences.getLinkedinAccessExpiry(mActivity) < System.currentTimeMillis()) {
                        LinkedinConstants.LinkedInConnect(getActivity(), mCallback, true, LinkedinConstants.getSyncingDialog(mActivity));
                    } else {
                        mCallback.onLinkedInConnected(accessToken, ShineSharedPreferences.getLinkedinAccessExpiry(mActivity)+"",null);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(mActivity.getResources().getColor(R.color.link));
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, text_before_linkedIn.length(),
                    text_before_linkedIn.length() + text_after_linkedIn.length()
                    , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv.setText(ss);
            tv.setMovementMethod(LinkMovementMethod.getInstance());
            tv.setHighlightColor(Color.TRANSPARENT);


        } else {
            inflatedView.findViewById(R.id.linkedin_after_applied).setVisibility(View.GONE);
        }

        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();

        //mActivity.syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
        ManualScreenTracker.manualTracker("PostApplyConnections");

    }






}

