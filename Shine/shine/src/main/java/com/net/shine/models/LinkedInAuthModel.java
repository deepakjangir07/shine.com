package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by ujjawal-work on 10/03/16.
 */


//{"user_details":{"user_token":"EXxtvZlAVI8yFRXs0Vqgw4hb1aBMzgg5TFKQl444eSln3GXc44W4KVf8rif4QyTonli8PZFGhLneoaURoRKzRw==",
// "candidate_id":"5396bddcd9bb3c2eb7abfbcc"},"prefill_details":{}}
// {"user_details":{},"prefill_details":{"name":"Ujjawal Garg","industry":27,"email":"ujjawal.1224@gmail.com",
// "cell_phone":null,"experience_in_years":4,"candidate_location":423}}

public class LinkedInAuthModel implements Serializable {

    public UserDetail user_details;
    public ProfileDetail prefill_details;
    public String accessToken;
    public String job_id;
    public String expiry;
    public int source_type;



    public static class UserDetail implements Serializable {

        public String user_token;
        public String candidate_id;
        public String email;
        public String profile_picture_url;
        public int mid_out;  // 0, 2
        public int resume_midout; //0 - i will get user_access_token, apply & go to JD


    }

    public static class ProfileDetail implements Serializable{

        public SocialApplyProfileModel profile;
        public Resume resume;
        public String source_id;
        public int profile_source;
        public String name;
//        public int industry; //ignoring this as we need desired & this is current ind
        public String email;
        public String cell_phone;
        public String experience_in_years;
        public String profile_picture_url ="";
        public int candidate_location;

    }

    public static class Resume implements Serializable{
        public String encoded_string;
        public String resume_name;
        public String url;
    }




}
