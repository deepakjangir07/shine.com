package com.net.shine.fragments.components;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.CertificationResult;
import com.net.shine.utils.DataCaching;

import java.util.List;

/**
 * Created by ggnf2853 on 18/05/16.
 */
public class CertificationDetailComp implements View.OnClickListener {

    private Activity mActivity;
    private View parent;
    private View loadingView;
    public static List<CertificationResult> skillsList;
    private LayoutInflater inflater;
    private LinearLayout skillsDetailView;


    public CertificationDetailComp(Activity mActivity, View parent) {
        this.mActivity = mActivity;
        this.parent = parent;
        this.inflater = mActivity.getLayoutInflater();
        loadingView = parent.findViewById(R.id.loading_cmp);
    }



    public void showDetails(List<CertificationResult> skillsList) {
        try {

            System.out.println("--skill-list"+skillsList.size() +"\n--"+skillsList );

            CertificationDetailComp.skillsList = skillsList;
            loadingView.setVisibility(View.GONE);
            skillsDetailView = (LinearLayout) parent
                    .findViewById(R.id.certi_detail_view);
            skillsDetailView.removeAllViews();

            parent.findViewById(R.id.c_details).setVisibility(View.VISIBLE);

            ImageView editView = (ImageView) parent
                    .findViewById(R.id.editcerti);
            editView.setVisibility(View.VISIBLE);



            editView.setOnClickListener(this);

            if (skillsList != null && skillsList.size() > 0) {

                int size = skillsList.size();
                for (int i = 0; i < size; i++) {

                    CertificationResult model = skillsList.get(i);
                    View experienceDetailTextView = inflater.inflate(
                            R.layout.skills_details_textview, null);
                    TextView labeltext = (TextView) experienceDetailTextView
                            .findViewById(R.id.label);

                    labeltext.setVisibility(View.GONE);
                    TextView tView = (TextView) experienceDetailTextView
                            .findViewById(R.id.field_name);

                    tView.setText(""
                            + model.getCertification_name()
                            + " ( "
                            + model.getCertification_year()
                            + " )"
                            );

                    skillsDetailView.addView(experienceDetailTextView);

                }

                DataCaching.getInstance().cacheData(DataCaching.CERTIFICATION_LIST_SECTION,
                        "" + DataCaching.CACHE_TIME_LIMIT, skillsList, mActivity);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onClick(View v) {
        try {
            System.out.println("--id--"+v.getId());
            switch (v.getId()) {
                case R.id.editcerti:

                    Bundle bundle = new Bundle();
                    bundle.putString(URLConfig.KEY_ACTION,
                            URLConfig.UPDATE_ALL_OPERATION);
                    BaseFragment frg = new CertificationsDetailEditFragment();
                    frg.setArguments(bundle);
                    ((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);

                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }
