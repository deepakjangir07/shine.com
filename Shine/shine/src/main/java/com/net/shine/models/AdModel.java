package com.net.shine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Deepak on 01/03/17.
 */

public class AdModel implements Serializable {

//    {
//        "count": 2,
//            "next": null,
//            "previous": null,
//            "results": [
//        {
//            "ad_title": "1",
//                "ad_path": "https://sumosc.shine.com/media/images/shine_ad/example__2017-03-02%2018%3A45%3A25.325584.png",
//                "click_url": "http://www.google1.com"
//        },
//        {
//            "ad_title": "xyz certification",
//                "ad_path": "https://sumosc.shine.com/media/images/shine_ad/listing%20ad__2017-03-06%2011%3A25%3A32.908897.png",
//                "click_url": "http://careerplus.shine.com/skill-courses/javase-8-programmer/pd4457.html"
//        }
//    ]
//    }


    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("next")
    @Expose
    public Object next;
    @SerializedName("previous")
    @Expose
    public Object previous;
    @SerializedName("results")
    @Expose
    public List<Result> results = null;

    public class Result implements Serializable{

        @SerializedName("ad_title")
        @Expose
        public String adTitle;
        @SerializedName("ad_path")
        @Expose
        public String adPath;
        @SerializedName("click_url")
        @Expose
        public String clickUrl;
        @SerializedName("impression_url")
        @Expose
        public String impressionUrl;

    }


}
