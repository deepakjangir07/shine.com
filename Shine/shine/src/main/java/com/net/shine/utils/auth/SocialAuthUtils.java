package com.net.shine.utils.auth;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.AppliedWithoutResumeConfirmation;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.LoginFrg;
import com.net.shine.fragments.RegistrationFrg;
import com.net.shine.fragments.SocialApplyFragment;
import com.net.shine.interfaces.SocialAppliedListener;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.LinkedInAuthModel;
import com.net.shine.models.NotificationModel;
import com.net.shine.models.SocialApplyProfileModel;
import com.net.shine.models.SocialApplyResponseModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

import static com.net.shine.config.URLConfig.FACEBOOK_APPLY_API;
import static com.net.shine.config.URLConfig.FACEBOOK_LOGIN_API;
import static com.net.shine.config.URLConfig.FACEBOOK_REGISTER_API;
import static com.net.shine.config.URLConfig.GOOGLE_APPLY_API;
import static com.net.shine.config.URLConfig.GOOGLE_LOGIN_API;
import static com.net.shine.config.URLConfig.GOOGLE_REGISTER_API;
import static com.net.shine.config.URLConfig.LINKEDIN_APPLY_API;
import static com.net.shine.config.URLConfig.LINKEDIN_LOGIN_API;
import static com.net.shine.config.URLConfig.LINKEDIN_REGISTER_API;


/**
 * Created by ujjawal-work on 25/05/16.
 */

public class SocialAuthUtils {

    public static final int LINKEDIN_AUTH = 0;
    public static final int GOOGLE_AUTH = 1;
    public static final int FACEBOOK_AUTH = 2;

    public static void registerUser(final String accessToken, final String expiry, final int auth_type, final Bundle bundle, final BaseActivity mActivity, final Dialog alertDialog, final AuthPrefillCallback mCallback ){
        String reg_url = "";
        switch (auth_type){
            case LINKEDIN_AUTH:
                reg_url = LINKEDIN_REGISTER_API;
                break;
            case GOOGLE_AUTH:
                reg_url = GOOGLE_REGISTER_API;
                break;
            case FACEBOOK_AUTH:
                reg_url = FACEBOOK_REGISTER_API;
                break;
        }

        authorizeUser(reg_url, accessToken, expiry, auth_type, bundle, mActivity, alertDialog, mCallback, false);


    }

    public static void loginUser(final String accessToken, final String expiry, final int auth_type, final Bundle bundle, final BaseActivity mActivity, final Dialog alertDialog, final AuthDialogCallback mCallback){
        String reg_url = "";
        switch (auth_type){
            case LINKEDIN_AUTH:
                reg_url = LINKEDIN_LOGIN_API;
                break;
            case GOOGLE_AUTH:
                reg_url = GOOGLE_LOGIN_API;
                break;
            case FACEBOOK_AUTH:
                reg_url = FACEBOOK_LOGIN_API;
                break;
        }


        authorizeUser(reg_url, accessToken, expiry, auth_type, bundle, mActivity, alertDialog, mCallback, false);


    }

    public static void socialApplyUser(final String accessToken, final String expiry, final int auth_type, final Bundle bundle,
                                       final BaseActivity mActivity, final Dialog alertDialog, final String comp_name, final String job_title){
        String reg_url = "";
        switch (auth_type){

            case GOOGLE_AUTH:
                reg_url = GOOGLE_APPLY_API;
                break;
            case FACEBOOK_AUTH:
                reg_url = FACEBOOK_APPLY_API;
                break;
            case LINKEDIN_AUTH:
                reg_url = LINKEDIN_APPLY_API;
        }

        authorizeUser(reg_url, accessToken, expiry, auth_type, bundle, mActivity, alertDialog, new AuthPrefillCallback() {
            @Override
            public void setPrefillData(LinkedInAuthModel model, int type) {

                SocialApplyProfileModel profileModel = new SocialApplyProfileModel();

                profileModel.userDetail = model.user_details;




                if(model.prefill_details!=null){
                    if(model.prefill_details.profile!=null){
                        profileModel = model.prefill_details.profile;
                        profileModel.resume = model.prefill_details.resume;
                        if(!profileModel.isDataStale()){
                            //Hit Social Application API
                            Bundle apply_bundle = bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE);
                            final String jobId;
                            if(apply_bundle!=null && !TextUtils.isEmpty(apply_bundle.getString(ApplyJobsNew.JOB_ID, "")))
                                jobId = apply_bundle.getString(ApplyJobsNew.JOB_ID);
                            else
                                jobId = "";
                            ApplyJobsNew.socialApplyUser(mActivity, jobId, profileModel, new SocialAppliedListener() {
                                @Override
                                public void onApplied(String msg, SocialApplyResponseModel token, Dialog dialog) {
                                    DialogUtils.dialogDismiss(dialog);

                                    ApplywithoutResumeModel awr = new ApplywithoutResumeModel();
                                    awr.job_id = jobId;
                                    Bundle bundle2 = new Bundle();
                                    bundle2.putSerializable("apply_model", awr);
                                    AppliedWithoutResumeConfirmation frg = new AppliedWithoutResumeConfirmation();
                                    frg.setArguments(bundle2);
                                    mActivity.replaceFragment(frg);
                                    Toast.makeText( mActivity, msg+"",Toast.LENGTH_SHORT).show();
                                }
                            } , false);
                            return;
                        }
                    } else {
                        profileModel.candidate_location = model.prefill_details.candidate_location;
                        profileModel.cell_phone = model.prefill_details.cell_phone;
                        profileModel.email = model.prefill_details.email;
                        profileModel.experience_in_years = model.prefill_details.experience_in_years;
                        profileModel.name = model.prefill_details.name;
                        profileModel.profile_source = model.prefill_details.profile_source;
                        profileModel.source_id = model.prefill_details.source_id;
                        profileModel.profile_picture_url = model.prefill_details.profile_picture_url;
                    }
                }
                Bundle bun = new Bundle();
                Bundle apply_bundle = bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE);
                String jobId;
                if(apply_bundle!=null && !TextUtils.isEmpty(apply_bundle.getString(ApplyJobsNew.JOB_ID, "")))
                    jobId = apply_bundle.getString(ApplyJobsNew.JOB_ID);
                else
                    jobId = model.job_id+"";
                bun.putSerializable("model", profileModel);
                bun.putString("job_id", jobId);
                bun.putString("comp_name", comp_name);
                bun.putString("job_title", job_title);
                bun.putString("social_access_token", accessToken);
                bun.putString("social_expiry", expiry);
                bun.putInt("auth_type", auth_type);



                BaseFragment fragment=new SocialApplyFragment();
                fragment.setArguments(bun);
                mActivity.replaceFragment(fragment);


            }
        }, true);
    }







    private static void authorizeUser(final String auth_url, final String accessToken, final String expiry, final int auth_type, final Bundle bundle,
                                      final BaseActivity mActivity, final Dialog alertDialog, final Object mCallback,final boolean isApply){

        Bundle apply_bundle = bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE);
        final String jobId;
        if(apply_bundle!=null && !TextUtils.isEmpty(apply_bundle.getString(ApplyJobsNew.JOB_ID, "")))
            jobId = apply_bundle.getString(ApplyJobsNew.JOB_ID, "");
        else
            jobId = "";

        Type listType = new TypeToken<LinkedInAuthModel>() {
        }.getType();

        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                final LinkedInAuthModel model = (LinkedInAuthModel) object;
                model.accessToken = accessToken;
                model.expiry = expiry;
                String profile_pic = null;
                if(model.prefill_details!=null && !TextUtils.isEmpty(model.prefill_details.profile_picture_url)){
                    profile_pic = model.prefill_details.profile_picture_url;
                }

                if (model.user_details != null && !TextUtils.isEmpty(model.user_details.user_token) &&
                        (!isApply || (model.user_details.resume_midout==0 && (model.user_details.mid_out==0 || model.user_details.mid_out==2)))) {
                    getUserStatus(mActivity, model.user_details.candidate_id, model.user_details.user_token, model.user_details.email,
                            accessToken, expiry, profile_pic, bundle, alertDialog, auth_type, mCallback);


                } else if(mCallback instanceof AuthPrefillCallback){
                    DialogUtils.dialogDismiss(alertDialog);
                    ((AuthPrefillCallback)mCallback).setPrefillData(model, auth_type);
                } else if(mCallback instanceof AuthDialogCallback){
                    DialogUtils.dialogDismiss(alertDialog);
                    ((AuthDialogCallback)mCallback).showAuthDialog(model, auth_type);
                }
            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(alertDialog);
                Toast.makeText(mActivity, error + " ", Toast.LENGTH_SHORT).show();

                if(isApply && !TextUtils.isEmpty(error) && error.toLowerCase().contains("already applied")) {
                    ApplywithoutResumeModel awr = new ApplywithoutResumeModel();
                    awr.job_id = jobId;
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable("apply_model", awr);
                    bundle2.putBoolean("already_applied", true);
                    AppliedWithoutResumeConfirmation frg = new AppliedWithoutResumeConfirmation();
                    frg.setArguments(bundle2);
                    mActivity.replaceFragment(frg);
                }
                Log.d("Debug::Error", error + " ");
            }
        },
                auth_url, listType);
        HashMap<String, String> map = new HashMap<>();
        switch (auth_type) {
            case LINKEDIN_AUTH:
                if(isApply)
                    map.put("access_token", accessToken);
                else
                    map.put("token", accessToken);
                map.put("expires_in", expiry);
                break;
            case GOOGLE_AUTH:
                map.put("access_token", accessToken);
                map.put("expires_in", expiry);
                break;
            case FACEBOOK_AUTH:
                map.put("access_token", accessToken);
                map.put("expires_in", expiry);
                break;
        }
        if(!TextUtils.isEmpty(jobId))
            map.put("job_id",  jobId);
        downloader.setUsePostMethod(map);
        downloader.execute("Auth_Login");

    }



    public static void getUserStatus(final BaseActivity mActivity, final String candidateId, final String user_token, final String email,
                                     final String socialAccessTokem, final String socialExpiry, final String profile_picture_url,
                                     final Bundle bundle, final Dialog alertDialog,
                                     final int auth_type, final Object mCallback){

        ShineSharedPreferences.setCandidateId(mActivity, candidateId);
        ShineSharedPreferences.setUserAccessToken(mActivity,user_token);
        ShineSharedPreferences.saveUserEmail(mActivity, email);
        Log.d("DEBUG::PROG", candidateId + " is the can_id in Login");

        String URL = URLConfig.USER_SERVER_STATUS_URL.replace(URLConfig.CANDIDATE_ID, candidateId) + "?nid=" + ShineSharedPreferences.getFCMRegistrationId(mActivity) +
                "&vc=" + ShineCommons.getAppVersionCode(mActivity) + "&dt=" + URLConfig.DEVICE_ANDROID;

        Type type = new TypeToken<UserStatusModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {

                DialogUtils.dialogDismiss(alertDialog);

                switch (auth_type){
                    case LINKEDIN_AUTH:
                        if(!TextUtils.isEmpty(socialAccessTokem)) {
                            ShineSharedPreferences.setLinkedinAccessToken(socialAccessTokem, mActivity);
                        }
                        if(!TextUtils.isEmpty(socialExpiry)) {
                                ShineSharedPreferences.setLinkedinAccessExpiry(socialExpiry, mActivity);
                        }
                        if(mCallback instanceof LoginFrg) {
                            ShineCommons.trackShineEvents("LoginSuccess", "LinkedIn", mActivity);
                        }
                        else if(mCallback instanceof RegistrationFrg) {
                            ShineCommons.trackShineEvents("RegistrationSuccess", "LinkedIn", mActivity);
                        }
                        break;
                    case GOOGLE_AUTH:
                        if(!TextUtils.isEmpty(socialAccessTokem)) {
                            ShineSharedPreferences.setGoogleAccessToken(socialAccessTokem, mActivity);
                        }
                        if(!TextUtils.isEmpty(socialExpiry)) {
                                ShineSharedPreferences.setGooleAccessExpiry(socialExpiry, mActivity);
                        }
                        if(mCallback instanceof LoginFrg) {
                            ShineCommons.trackShineEvents("LoginSuccess", "Google", mActivity);
                        }
                        else if(mCallback instanceof RegistrationFrg) {
                            ShineCommons.trackShineEvents("RegistrationSuccess", "Google", mActivity);
                        }
                        break;
                    case FACEBOOK_AUTH:
                        if(!TextUtils.isEmpty(socialAccessTokem)) {
                            ShineSharedPreferences.setFacebookAccessToken(socialAccessTokem, mActivity);
                        }
                        if(!TextUtils.isEmpty(socialExpiry)) {
                            ShineSharedPreferences.setFacebookAccessExpiry(socialExpiry, mActivity);
                        }
                        if(mCallback instanceof LoginFrg) {
                            ShineCommons.trackShineEvents("LoginSuccess", "Facebook", mActivity);
                        }
                        else if(mCallback instanceof RegistrationFrg) {
                            ShineCommons.trackShineEvents("RegistrationSuccess", "Facebook", mActivity);
                        }
                        break;
                }

                ShineAuthUtils.LoginUser((UserStatusModel)object, mActivity, bundle);
                //TODO: change this to UserPictureUrl
                if(!TextUtils.isEmpty(profile_picture_url)) {
                    try {
                        ShineSharedPreferences.setLinkedinPictureUrl(
                                profile_picture_url, mActivity);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                if(!TextUtils.isEmpty(candidateId)){
                    NotificationApiHit(candidateId,mActivity);
                }
                MoengageTracking.setLoginAttribute(mActivity);
                MoengageTracking.setProfileAttributes(mActivity);

            }

            @Override
            public void OnDataResponseError(String error, String tag) {
                DialogUtils.dialogDismiss(alertDialog);
                final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                errorDialog.findViewById(R.id.ok_btn)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogUtils.dialogDismiss(errorDialog);
                            }
                        });
            }
        }, URL, type);

        req.execute("UserStatus");

    }



    public static void NotificationApiHit(final String candidate_id, final BaseActivity mActivity){
        String url ="";
        if(!TextUtils.isEmpty(candidate_id)){

            Log.d("notification_response","inside hit");

            url = URLConfig.NOTIFICATION_API.replace(URLConfig.CANDIDATE_ID,candidate_id);

            Type type = new TypeToken<NotificationModel>(){

            }.getType();

            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    Log.d("notification_response",object.toString());
                    Gson gson = new Gson();
                    String notificationData = gson.toJson(object);
                    ShineSharedPreferences.saveNotificationData(mActivity,
                            candidate_id+"_noti",notificationData);
                    NotificationCount(candidate_id,mActivity);
                    Log.d("notification_response1",notificationData);
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("Notification","error "+error);

                }
            },url,type);

            request.execute("notification");
        }
    }

    public static void NotificationCount(String candidate_id, BaseActivity mActivity) {
        if (!TextUtils.isEmpty(candidate_id)) {
            final String notificationData = ShineSharedPreferences.getNotificationData(mActivity,
                    candidate_id + "_noti");
            if (!TextUtils.isEmpty(notificationData)) {
                Gson gson = new Gson();

                NotificationModel model = gson.fromJson(notificationData, NotificationModel.class);
                if (model != null) {
                    if (model.newData != null) {
                        if (model.newData.jobAlert != null && model.newData.recruiterMail != null
                                && model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                    candidate_id+"_noti_count","3");
                        } else if ((model.newData.jobAlert != null && model.newData.recruiterMail != null) ||
                                (model.newData.recruiterMail != null && model.newData.whoViewed != null) ||
                                (model.newData.jobAlert != null && model.newData.whoViewed != null)) {

                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                    candidate_id+"_noti_count","2");
                        } else if (model.newData.jobAlert != null || model.newData.recruiterMail != null
                                || model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                    candidate_id+"_noti_count","1");
                        }
                    }
                }
            }
        }
    }




    public interface AuthPrefillCallback{
        void setPrefillData(LinkedInAuthModel model, int type);
    }

    public interface AuthDialogCallback{
        void showAuthDialog(LinkedInAuthModel model, int type);
    }

}
