package com.net.shine.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.InboxAlertsListAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.InboxAlertMailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class JobAlertFragment extends BaseFragment implements AdapterView.OnItemClickListener,
        OnScrollListener, VolleyRequestCompleteListener, SwipeRefreshLayout.OnRefreshListener {
    private ListView gridView;
    private InboxAlertsListAdapter adapter;
    private UserStatusModel userProfileVO;
    private boolean loadMore = false;
    private int totalMails;
    private View LoadingCmp;
    private View errorScreen;
    private boolean jobAlert;
    private View view;

    private SwipeRefreshLayout swipeContainer;

    public static JobAlertFragment newInstance(boolean jobAlert) {
        Bundle args = new Bundle();
        args.putBoolean("jobAlert", jobAlert);
        JobAlertFragment fragment = new JobAlertFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<InboxAlertMailModel.Results> jobAlertList = new ArrayList<>();


    @Override
    public void onPause() {
        super.onPause();
        if(!jobAlert)
            URLConfig.AD++;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        jobAlert = getArguments().getBoolean("jobAlert", jobAlert);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.job_alert_frg, container, false);
        try {
            swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            swipeContainer.setOnRefreshListener(this);
            gridView = (ListView) view.findViewById(R.id.grid_view);
            gridView.setOnScrollListener(this);
            errorScreen = view.findViewById(R.id.error_layout);
            errorScreen.setVisibility(View.GONE);
            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
            adapter = new InboxAlertsListAdapter(mActivity, jobAlertList,jobAlert);
            gridView.setAdapter(adapter);
            gridView.setOnItemClickListener(this);
            downloadJobAlertMail(1, view.findViewById(R.id.loading_cmp), false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downloadJobAlertMail(int pageNumber, View loadingView,
                                      boolean flag, boolean isRefesh) {
        try {
            LoadingCmp = loadingView;
            if (!isRefesh)
                DialogUtils.showLoading(mActivity, getString(R.string.loading),
                        LoadingCmp);

            loadMore = flag;
            if(!loadMore){
                gridView.setVisibility(View.GONE);
            }

            if (jobAlert) {


                    Type type = new TypeToken<InboxAlertMailModel>() {
                    }.getType();

                    VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                            URLConfig.INBOX_LIST_JOB_ALERT_MAIL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id)
                                    + "?page=" + pageNumber + "&perpage=" + URLConfig.PER_PAGE_JOB_ALERT_MAIL, type);

                    req.execute("JobAlertMails");



            } else {

                    Type type = new TypeToken<InboxAlertMailModel>() {
                    }.getType();

                    VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                            URLConfig.INBOX_LIST_RECRUITER_MAIL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id)
                                    + "?page=" + pageNumber + "&perpage=" + URLConfig.PER_PAGE_JOB_ALERT_MAIL, type);

                    req.execute("RecruiterAlertMails");



            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        try {

            if (totalItemCount != 0) {
                int lastScreen = firstVisibleItem + visibleItemCount;
                if ((lastScreen == totalItemCount) && !(loadMore)) {

                    loadMoreData();
                } else if (LoadingCmp.getTag() != null) {
                    if (LoadingCmp.getTag().equals("error")) {
                        view.findViewById(R.id.loading_grid).setVisibility(
                                View.VISIBLE);
                        loadMore = false;
                    }
                }
            } else {

                if (view.findViewById(R.id.loading_grid) != null) {
                    view.findViewById(R.id.loading_grid).setVisibility(
                            View.GONE);
                }

            }
            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMoreData() {
        try {
            int noofhits = (jobAlertList.size() / URLConfig.PER_PAGE_JOB_ALERT_MAIL) + 1;
            int totalPage = totalMails / URLConfig.PER_PAGE_JOB_ALERT_MAIL;

            int modtotalnoPages = totalPage % URLConfig.PER_PAGE_JOB_ALERT_MAIL;
            if (modtotalnoPages != 0) {
                totalPage = totalPage + 1;
            }

            if (noofhits <= totalPage && totalMails > jobAlertList.size()) {

                downloadJobAlertMail(noofhits,
                        view.findViewById(R.id.loading_grid), true, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

    }


    private void showResult() {
        try {

            LoadingCmp.setVisibility(View.GONE);
            if (totalMails > 0) {

                view.findViewById(R.id.grid_view).setVisibility(View.VISIBLE);
                adapter.rebuildListObject(jobAlertList);
                adapter.notifyDataSetChanged();
                errorScreen.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);

            } else {
                gridView.setVisibility(View.GONE);
                errorScreen.setVisibility(View.VISIBLE);


                if (mActivity != null) {
                    if (jobAlert) {

                        BaseFragment fragment = new MyCustomJobAlertFragment();

                        DialogUtils.showErrorActionableMessage(mActivity,errorScreen,
                                mActivity.getString(R.string.no_job_alert_mail_msg),
                                mActivity.getString(R.string.no_job_alert_mail_msg2),
                                mActivity.getString(R.string.manage_alerts),
                                DialogUtils.ERR_NO_EMAIL,fragment);

                    } else {

                        BaseFragment fragment = new MyProfileFrg();
                        DialogUtils.showErrorActionableMessage(mActivity,errorScreen,
                                mActivity.getString(R.string.no_recruiter_mail_msg),
                                mActivity.getString(R.string.no_recruiter_mail_msg2),
                                mActivity.getString(R.string.update_profile),
                                DialogUtils.ERR_NO_EMAIL,fragment);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void OnDataResponse(Object object, String tag) {
        try {

            LoadingCmp.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            InboxAlertMailModel model = (InboxAlertMailModel) object;
            swipeContainer.setRefreshing(false);
            totalMails = model.count;

            if (jobAlertList == null || !loadMore)
                jobAlertList = new ArrayList<>();

            int prevSize=jobAlertList.size();

            /**
             *
             * This is for add an AD element in listview at position 3
             */
            if(prevSize==0&&!jobAlert&&ShineSharedPreferences.isCareerPlusAds(mActivity)) {
                if(model.results!=null&&model.results.size()>3)
                model.results.add(3,new InboxAlertMailModel.Results());
            }

            jobAlertList.addAll(model.results);
            adapter.notifyDataSetChanged();
            loadMore = false;
            showResult();

            String url="";

            if(jobAlert){

                url = URLConfig.NOTIFICATION_TIMESTAMP_UPDATE_API.replace(URLConfig.CANDIDATE_ID,
                        ShineSharedPreferences.getCandidateId(mActivity))+"job_alert_section_visit";

            }
            else {
                url = URLConfig.NOTIFICATION_TIMESTAMP_UPDATE_API.replace(URLConfig.CANDIDATE_ID,
                        ShineSharedPreferences.getCandidateId(mActivity))+"recruiter_mail_section_visit";
            }
            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    Log.d("timestamp","updated");
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("timestamp","error"+error);

                }
            }, url, type);

            HashMap<String,String> params = new HashMap<>();
            request.setUsePutMethod(params);
            request.execute("notification");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            System.out.println(" --error>>>--" + error);
            swipeContainer.setRefreshing(false);
            LoadingCmp.setVisibility(View.GONE);
            gridView.setVisibility(View.GONE);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    LoadingCmp.setVisibility(View.GONE);
                    DialogUtils.showErrorActionableMessage(mActivity,errorScreen,
                            mActivity.getString(R.string.technical_error),
                            mActivity.getString(R.string.technical_error2),
                            "",
                            DialogUtils.ERR_TECHNICAL,null);                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRefresh() {
        try {
            errorScreen.setVisibility(View.GONE);
            downloadJobAlertMail(1,
                    view.findViewById(R.id.loading_cmp), false, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


        ArrayList<InboxAlertMailModel.Results> jList = new ArrayList<>();


        for (int i = 0; i < jobAlertList.size(); i++)
        {
               if(!(!jobAlert&&ShineSharedPreferences.isCareerPlusAds(mActivity)&&i==3)) {
                   jList.add(jobAlertList.get(i));

            }
        }

        if(!jobAlert&&ShineSharedPreferences.isCareerPlusAds(mActivity)&&position>3)
            position=position-1;

        Bundle bundle = new Bundle();
        bundle.putInt(URLConfig.INDEX_MAIL, position);
        bundle.putBoolean(URLConfig.MAIL_TYPE, jobAlert);
        bundle.putSerializable(JobAlertDetailParentFragment.JOB_ALERT_LIST, jList);
        BaseFragment fragment = new JobAlertDetailParentFragment();
        fragment.setArguments(bundle);
        mActivity.singleInstanceReplaceFragment(fragment);

        }



    }
