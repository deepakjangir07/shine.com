package com.net.shine.fragments.tabs;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.net.shine.R;
import com.net.shine.adapters.MainPagerAdapter;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.interfaces.TabChangeListener;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.List;

public abstract class ProfileViewsCustomTabFrg extends BaseFragment {

    public static final String SELECTED_TAB = "selected_tab";
    private ViewPager mViewPager;
    public TabLayout tabLayout;
    protected int currentSelected;
    public int count;
    public String text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShineCommons.hideKeyboard(mActivity);
        System.out.println("on create-tab");
    }


    public abstract List<Fragment> getFragments();

    private List<TabChangeListener> listeners;

    public abstract ArrayList<String> getNames();

    ArrayList<String> tabName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.tab_layout_new, container, false);
        try {
            List<Fragment> fragments = getFragments();

            listeners = new ArrayList<>();

            for(Fragment frg: fragments)
                if(frg instanceof TabChangeListener)
                    listeners.add((TabChangeListener) frg);

            tabName = getNames();

            mViewPager = (ViewPager) view.findViewById(R.id.pager);

            tabLayout = (TabLayout) view.findViewById(R.id.tabs);

            tabLayout.addTab(tabLayout.newTab().setText(""));




            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                RelativeLayout relativeLayout = (RelativeLayout)
                        LayoutInflater.from(mActivity).inflate(R.layout.tab_layout, tabLayout, false);


                tab.setCustomView(relativeLayout);
                tab.select();
                LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
                linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
                GradientDrawable drawable = new GradientDrawable();
                drawable.setColor(Color.TRANSPARENT);
                drawable.setSize(15, 15);

                linearLayout.setDividerDrawable(drawable);
                Log.d("no. of tabs",i+"");
            }




            MainPagerAdapter mSearchPagerAdapter = new MainPagerAdapter(mActivity, getChildFragmentManager(), fragments, tabName);

            mViewPager.setAdapter(mSearchPagerAdapter);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    currentSelected = position;
                    PageSelected(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            try {
                tabLayout.setupWithViewPager(mViewPager);
                if (tabLayout.getSelectedTabPosition() == 1) {
                    currentSelected = 1;
                }
                mViewPager.setCurrentItem(currentSelected);
                Log.d("CURRENT TAB ", "selected tab " + currentSelected + " page " + mViewPager.getCurrentItem());
            } catch (Exception e) {
                e.printStackTrace();
            }
            PageSelected(currentSelected);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    protected void PageSelected(int position) {

    }

    public void setTabTitle(int position, String Title)
    {
        try {
            tabLayout.getTabAt(position).setText(Title);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }






}



