package com.net.shine.fragments.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.FilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.ShineCommons;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


public class CheckBoxSpinnerFragment extends DialogFragment implements View.OnClickListener, AdapterView.OnItemClickListener {


    String title;
    TextView tView;
    List<String> master_list;
    HashMap<String, Integer> indexMap;
    View view;
    ListView lv;
    FilterableArrayAdapter<String> adapter;
    private HashSet<String> selectedStringSet = new HashSet<>();

    private boolean shoudlLimit;

    public CheckBoxSpinnerFragment(){




    }

    @SuppressLint("ValidFragment")
    public CheckBoxSpinnerFragment(String title, final TextView tView,
                                   final String[] master_list, boolean check) {
        this.title = title;
        this.tView = tView;
        shoudlLimit = check;
        this.master_list = Arrays.asList(master_list);
        indexMap = new HashMap<>();
        for (Integer i = 0; i < master_list.length; i++) {
            indexMap.put(master_list[i], i);
        }
    }


    BaseActivity mActivity;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = ((BaseActivity) activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ShineBaseThemeNoOverlay);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.spinner_fragment, container, false);

        try {
            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(this);

            TextView tv_title = (TextView) view.findViewById(R.id.header_title);
            tv_title.setText(title);


            lv = (ListView) view.findViewById(R.id.lv_spinner);

            adapter = new FilterableArrayAdapter<>(mActivity, R.layout.checkbox_textview, master_list);
            lv.setAdapter(adapter);
            lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            int[] currentIndex = (int[]) tView.getTag();
            int scrollIndex = -1;
            if(currentIndex!=null)
            {
                Log.d("DEBUG::UJJ", Arrays.toString(currentIndex));
                for (int i = 0; i < currentIndex.length; i++) {
                    selectedStringSet.add(master_list.get(currentIndex[i]));
                    lv.setItemChecked(currentIndex[i], true);
                    if(scrollIndex==-1 || scrollIndex > currentIndex[i])
                    {
                        scrollIndex = currentIndex[i];
                    }
                }
            }

            lv.setOnItemClickListener(this);


                if(scrollIndex>5)
                {
                    lv.setSelection(scrollIndex-2);
                }


            EditText et = (EditText) view.findViewById(R.id.et_spinner_search);

            if(master_list!=null&&master_list.size()<13)
            {
                et.setVisibility(View.GONE);
            }
            else {
                et.setVisibility(View.VISIBLE);
            }


            et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                        return true;
                    }
                    return false;
                }
            });
            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    lv.clearChoices();

                    adapter.getFilter().filter(s.toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            int newSelectedIndex = adapter.getPosition("");
                            if (newSelectedIndex != -1) {
                                lv.setItemChecked(newSelectedIndex, true);
                            }
                        }
                    });
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onClick(View v) {
//        super.onClick(v);

        switch (v.getId()) {
            case R.id.update_btn:
                try {
                    mActivity.getSupportFragmentManager().popBackStackImmediate();

                    int selected_pos[] = new int[selectedStringSet.size()];
                    int i = 0;
                    for (String s : selectedStringSet) {
                        selected_pos[i] = indexMap.get(s);
                        i++;
                    }
                    String text;
                    if (i == 1 && URLConfig.NO_SELECTION
                            .equals(master_list.get(selected_pos[0]))) {
                        text = "";
                    } else {
                        text = selectedStringSet.toString();
                    }
                    text = text.replace("]", "").replace("[", "");
                    text = text.replace(",", ", ");
                    ShineCommons.hideKeyboard(mActivity);
//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
                    tView.setText(text);
                    tView.setTag(selected_pos);
                    Log.d("DEBUG::UJJ", Arrays.toString(selected_pos));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.cancel_btn:
                try {
                    mActivity.popone();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String ClickedItemText = ((TextView) view).getText().toString();

        System.out.println("---chkbox click"+selectedStringSet.size() +"limit--"+shoudlLimit);
        if (lv.isItemChecked(position)) {

//            lv.setItemChecked(position, false);
            if(selectedStringSet.size()>=10 && shoudlLimit)
            {
                Toast.makeText(mActivity, "Number of items should be less than 10.", Toast.LENGTH_SHORT).show();
                lv.setItemChecked(position, false);
            }
            else
            {
                selectedStringSet.add(ClickedItemText);
            }

        } else {
//            lv.setItemChecked(position, true);
            selectedStringSet.remove(ClickedItemText);
        }

    }


}
