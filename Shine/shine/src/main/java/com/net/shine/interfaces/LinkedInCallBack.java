package com.net.shine.interfaces;

import android.app.Dialog;

public abstract class LinkedInCallBack {

    public void onLinkedInConnected(String accessToken, String expiry, Dialog customDialog){};

    public void onLinkedInSuccess(){};

    public void onLinkedInFailed(){};
}
