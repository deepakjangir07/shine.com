package com.net.shine.fragments.components;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.SkillResult;
import com.net.shine.utils.DataCaching;

import java.util.List;


public class SkillDetailComp implements OnClickListener{

	private Activity mActivity;
	private View parent;
	private View loadingView;
	public static List<SkillResult> skillsList;
	private LayoutInflater inflater;
	private LinearLayout skillsDetailView;

	/**
	 * @param mActivity
	 * @param parent
	 */
	public SkillDetailComp(Activity mActivity, View parent) {
		this.mActivity = mActivity;
		this.parent = parent;
		this.inflater = mActivity.getLayoutInflater();
        loadingView = parent.findViewById(R.id.loading_cmp);
	}

	/**
	 * @param skillsList
	 */
	public void showDetails(List<SkillResult> skillsList) {
		try {

			SkillDetailComp.skillsList = skillsList;
			loadingView.setVisibility(View.GONE);
			skillsDetailView = (LinearLayout) parent
					.findViewById(R.id.skill_detail_view);
			skillsDetailView.removeAllViews();

			parent.findViewById(R.id.s_details).setVisibility(View.VISIBLE);
//			TextView addSkillBtn = (TextView) parent.findViewById(R.id.add_new_skills);

			ImageView editView = (ImageView) parent
					.findViewById(R.id.editskill);
			editView.setVisibility(View.VISIBLE);



			editView.setOnClickListener(this);

			if (skillsList != null && skillsList.size() > 0) {

				int size = skillsList.size();
				for (int i = 0; i < size; i++) {

					SkillResult model = skillsList.get(i);
					View experienceDetailTextView = inflater.inflate(
							R.layout.skills_details_textview, null);
					TextView labeltext = (TextView) experienceDetailTextView
							.findViewById(R.id.label);

						labeltext.setVisibility(View.GONE);
					TextView tView = (TextView) experienceDetailTextView
							.findViewById(R.id.field_name);

					String text = ""
							+ model.getValue();
					String yrs = URLConfig.SKILLS_REVRSE_MAP.get(""
							+ model.getYears_of_experience());
					if(!TextUtils.isEmpty(yrs)){
						text = text + " ( " + yrs + " )";
					}
					tView.setText(text);

					skillsDetailView.addView(experienceDetailTextView);

				}

				DataCaching.getInstance().cacheData(DataCaching.SKILLS_LIST_SECTION,
						"" + DataCaching.CACHE_TIME_LIMIT, skillsList, mActivity);

			}
//            else {
//
//				View editLine = parent.findViewById(R.id.ic_line);
//				editLine.setVisibility(View.GONE);
//				editView.setVisibility(View.GONE);
//
//			}
//			addSkillBtn.setVisibility(View.VISIBLE);
//			addSkillBtn.setOnClickListener(this);


		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.editskill:
				Bundle bundle = new Bundle();
				bundle.putString(URLConfig.KEY_ACTION,
						URLConfig.UPDATE_ALL_OPERATION);
				BaseFragment frg = new SkillsDetailsEditFragment();
				frg.setArguments(bundle);
				((BaseActivity)mActivity).replaceFragmentWithBackStack(frg);


				break;
		}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
