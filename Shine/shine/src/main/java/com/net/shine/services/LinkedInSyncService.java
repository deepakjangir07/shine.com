package com.net.shine.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.KonnectFrag;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class LinkedInSyncService extends IntentService  {

    private LinkedInTask mTask;
    private Context mContext;
    public LinkedInSyncService() {
        super("LinkedInSyncService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mContext = getApplicationContext();
        mTask = new LinkedInTask(ShineSharedPreferences.getLinkedinAccessToken(mContext));
        mTask.execute();

        return super.onStartCommand(intent,flags,startId);
    }

    @Override
	public void onDestroy() {
		super.onDestroy();
        stopSelf();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    protected void onHandleIntent(Intent intent) {

    }


    private class LinkedInTask extends AsyncTask<String, Void, JSONObject>
    {
        String accessToken = "";
        public LinkedInTask(String token)
        {
            accessToken = token;
        }

        @Override
        protected JSONObject doInBackground(String... strings) {

            try {
                MyApplication.isLinkedInSyncing = true;
                JSONArray values = new JSONArray();
                int total = 1000;
                int count = 0;
                while (total - count > 0) {
                    StringBuilder content = new StringBuilder();
                    URL mUrl = new URL(LinkedinConstants.getConnectionsUrl(accessToken, count, LinkedinConstants.CONNECTION_PAGING_COUNT));

                    Log.d("DEBUG_LINKEDIN", "Hitting Connection API: start=" + count + " " + mUrl);

                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    connection.setRequestProperty("x-li-format", "json");

                    if (connection.getResponseCode() == 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null)
                            content.append(line).append("\n");
                        bufferedReader.close();
                        connection.disconnect();
                        JSONObject ans = new JSONObject(content.toString());
                        if (ans.has("_total"))    //update total from its default 1000
                        {
                            total = ans.getInt("_total");
                        }
                        if (!ans.has("values")) {
                            break;    //we have reached the end
                        }
                        JSONArray nVal = ans.getJSONArray("values");
                        if (nVal.length() == 0) {
                            break;    //we have reached the end
                        }
                        count = count + nVal.length();
                        for (int i = 0; i < nVal.length(); i++) {
                            values.put(nVal.get(i));
                        }

                 } else {
                        Log.d("DEBUG::LINKEDIN_CONN", "Linkedin API gives error code: " + connection.getResponseCode());
                        break;
                    }
                }
                JSONObject object = new JSONObject();
                object.put("_total", total);
                object.put("_start", 0);
                object.put("_count", count);
                object.put("values", values);


                JSONObject data = LinkedinConstants.parseLinkedin(object);

                return data;


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if(jsonObject==null)
            {
                MyApplication.isLinkedInSyncing = false;
                stopSelf();
                return;
            }

            HashMap<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("shine_id", ShineSharedPreferences.getCandidateId(mContext));
            paramsMap.put("source", "linkedin");
            paramsMap.put("data", jsonObject);
            paramsMap.put("access_token", ShineSharedPreferences.getLinkedinAccessToken(mContext));

            Type type = new TypeToken<EmptyModel>() {
            }.getType();
            VolleyNetworkRequest request = new VolleyNetworkRequest(mContext, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    MyApplication.isLinkedInSyncing = false;
                    ShineSharedPreferences
                            .updateSynctimeLinkedIn(
                                    mContext,
                                    System.currentTimeMillis());
                    ShineSharedPreferences.saveLinkedinImported(mContext,
                            true);
                    if(KonnectFrag.getInstance()!=null)
                    {
                        Log.d("DEBUG::UJJ", "KonnectFrg is not null");
                        KonnectFrag.getInstance().onInstanceSuccesLinkedIn();
                    }
                    else
                    {
                        Log.d("DEBUG::UJJ", "KonnectFrg is null");
                    }
                }

                @Override
                public void OnDataResponseError(String error, String tag) {
                    MyApplication.isLinkedInSyncing = false;
                    if(KonnectFrag.getInstance()!=null)
                    {
                        Log.d("DEBUG::UJJ", "KonnectFrg is not null");
                        KonnectFrag.getInstance().onInstanceFailedLinkedIn();
                    }
                    else
                    {
                        Log.d("DEBUG::UJJ", "KonnectFrg is null");
                    }
                    stopSelf();
                }
            }, URLConfig.KONNECT_LINKEDIN_URL, type);
            request.setUsePostMethod(paramsMap);
            request.execute("DumpLinkedInData");


        }
    }



}
