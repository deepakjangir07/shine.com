package com.net.shine.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.text.Html;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.FriendPopupActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.views.BetterPopupWindow;

import java.util.ArrayList;
import java.util.StringTokenizer;


public abstract class JobsListAdapter extends BaseAdapter implements View.OnClickListener {


    protected BaseActivity mActivity;
    protected final int ACTION_NOTHING = 0;
    protected final int ACTION_APPLY_LOGGEDIN = 1;
    protected final int ACTION_APPLY_LOGGEDOUT = 2;
    protected final int ACTION_CONNECTION = 3;

    protected ArrayList<SimpleSearchModel.Result> mList;
    private static String mLocations = "";
    private static String mskill = "";
    String GAEvent="",frndRowGA="";

    public JobsListAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list, String GAEvent) {
        this.mActivity = activity;
        this.mList = list;
        this.GAEvent=GAEvent;

    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int postition) {
        return mList.get(postition);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;


        JobsViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            LayoutInflater li = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.grid_table_row, viewGroup, false);
            viewHolder = new JobsViewHolder();
            viewHolder.title = (TextView) v.findViewById(R.id.title);
            viewHolder.company = (TextView) v.findViewById(R.id.company);
            viewHolder.frndCount = (TextView) v
                    .findViewById(R.id.frndsCount);
            viewHolder.posted_date = (TextView) v.findViewById(R.id.posted);
            viewHolder.walkin = (LinearLayout) v.findViewById(R.id.walkin);
            viewHolder.divider = v.findViewById(R.id.divider);
            viewHolder.date = (TextView) v.findViewById(R.id.date);
            viewHolder.venue = (TextView) v.findViewById(R.id.venue);


            viewHolder.frndCountRow = (RelativeLayout) v
                    .findViewById(R.id.frnd_row);
            viewHolder.apply_job = v.findViewById(R.id.apply_job);
            viewHolder.applied_job = v.findViewById(R.id.applied_job);
            viewHolder.tv_more = (TextView) v.findViewById(R.id.more);
            viewHolder.tv_experience = (TextView) v
                    .findViewById(R.id.experience);
            viewHolder.skills = (TextView) v.findViewById(R.id.skills);
            viewHolder.tv_location = (TextView) v.findViewById(R.id.location);
            v.setTag(viewHolder);
        } else {
            viewHolder = (JobsViewHolder) v.getTag();
        }

        if (mList.get(position).is_applied || URLConfig.jobAppliedSet.contains(mList.get(position).jobId)) {
            viewHolder.apply_job.setVisibility(View.GONE);
            viewHolder.posted_date.setVisibility(View.GONE);
            viewHolder.applied_job.setVisibility(View.VISIBLE);
        } else {
            viewHolder.apply_job.setVisibility(View.VISIBLE);
            viewHolder.posted_date.setVisibility(View.VISIBLE);
            viewHolder.applied_job.setVisibility(View.GONE);
            viewHolder.apply_job.setOnClickListener(this);
            viewHolder.apply_job.setTag(mList.get(position).jobId);
        }

        viewHolder.title.setText(mList.get(position).jobTitle);
        viewHolder.company.setText(mList.get(position).comp_name);
        viewHolder.posted_date.setText(mList.get(position).getPostedDate());



        if (TextUtils.isEmpty(mList.get(position).job_skills)) {
            viewHolder.skills.setVisibility(View.GONE);
        } else {
            viewHolder.skills.setVisibility(View.VISIBLE);
        }

        if (mList.get(position).jJobType==2){

            viewHolder.walkin.setVisibility(View.VISIBLE);
            viewHolder.divider.setVisibility(View.VISIBLE);
            viewHolder.frndCountRow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mList.get(position).getVenue())){
                viewHolder.venue.setVisibility(View.VISIBLE);
                viewHolder.venue.setText(Html.fromHtml("<b>Venue </b> " + mList.get(position).getVenue()));
            }
            else {
                viewHolder.venue.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(mList.get(position).getDate())){
                viewHolder.date.setVisibility(View.VISIBLE);
                viewHolder.date.setText(Html.fromHtml("<b>Date </b>"+mList.get(position).getDate()));
            }
            else {
                viewHolder.date.setVisibility(View.GONE);
            }
        } else {
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.venue.setVisibility(View.GONE);
            viewHolder.walkin.setVisibility(View.GONE);
            viewHolder.divider.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(mList.get(position).jRUrl)){
            viewHolder.frndCountRow.setVisibility(View.GONE);
        }

        DiscoverModel.Company mDiscoverFriendsModel = mList.get(position).frnd_model;
        if (mDiscoverFriendsModel != null && mDiscoverFriendsModel.data.friends.size() > 0) {
            setConnection(mDiscoverFriendsModel, mList.get(position), viewHolder);
        } else
            viewHolder.frndCountRow.setVisibility(View.GONE);

        showMoreInLocation(viewHolder, mList.get(position).job_loc_str, mList.get(position).jobExperience, mList.get(position).job_skills);
        return v;
    }

    public void setConnection(DiscoverModel.Company dfm, final SimpleSearchModel.Result searchResultVO, JobsViewHolder viewHolder) {
        {

            viewHolder.frndCountRow.setVisibility(View.VISIBLE);
            final int friend_count = dfm.data.friends.size();
            if (friend_count == 2) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#333333'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connection "
                                        + "<font color='#333333'>work here</font>"));
            } else if (friend_count > 1) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#333333'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connections "
                                        + "<font color='#333333'>work here</font>"));
            } else {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#2c84cc'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> "
                                        + "<font color='#333333'> works here</font>"));
            }
            viewHolder.frndCountRow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Bundle args = new Bundle();
                    args.putSerializable("svo", searchResultVO);
                    args.putInt("fromScreen", 0);
                    Intent intent = new Intent(mActivity, FriendPopupActivity.class);
                    intent.putExtras(args);
                    mActivity.startActivity(intent);

                    ShineCommons.trackShineEvents("KonnectFriendList",GAEvent,mActivity);

                }
            });
        }

    }

    public void rebuildListObject(ArrayList<SimpleSearchModel.Result> itemList) {

        this.mList = itemList;
    }


    public void showMoreInLocation(final JobsViewHolder viewHolder, final String location,
                                   final String exp, final String skill) {


        if (skill != null && !skill.isEmpty()) {

            viewHolder.skills.setText(skill);
        }

        if (exp == null || exp.equals("")) {
            viewHolder.tv_experience.setVisibility(View.GONE);
        } else {
            viewHolder.tv_experience.setVisibility(View.VISIBLE);
            viewHolder.tv_experience.setText(exp + "  ");
        }



        final StringTokenizer st = new StringTokenizer(location, "/");
        if (st.countTokens() > 1) {
            final String s2;
            {
                s2 = st.nextToken()+"";
            }

            viewHolder.tv_location.setText(s2.toString());
            viewHolder.tv_more.setVisibility(View.VISIBLE);
            viewHolder.tv_more.setText(Html.fromHtml("& "+(st.countTokens())+" more "));
            viewHolder.tv_more.setText(Html.fromHtml("<font color = '#333333'>"
                    +" & "
                    +"</font>"
                    +"<font color = '#2c84cc'>"
                    +(st.countTokens())
                    + " more"
                    +"</font>"));

        }
        else
        {
            viewHolder.tv_location.setText(location);
            viewHolder.tv_more.setVisibility(View.GONE);
        }

        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location_more = "";
                String[] arr_loc = location.split("/");
                for (int i = 0; i < arr_loc.length; i++) {
                    if (i == (arr_loc.length - 1)) {
                        location_more = location_more + arr_loc[i].trim();
                    } else {
                        location_more = location_more + arr_loc[i].trim() + ", ";
                    }
                }
                mLocations = location_more;
                DemoPopupWindow demoPopupWindow = new DemoPopupWindow(viewHolder.tv_more);
                demoPopupWindow.showLikePopDownMenu();
            }
        });

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.apply_job:

                String jobID = (String) v.getTag();
                SimpleSearchModel.Result svo = null;
                for (int i = 0; i < mList.size(); i++) {
                    if (mList.get(i) == null)
                        continue;
                    if (mList.get(i).jobId.equals(jobID)) {
                        svo = mList.get(i);
                        break;
                    }
                }
                applyToJob((String) v.getTag(), svo, (View) v.getParent().getParent());




                break;

            default:
                break;
        }

    }

    private void applyToJob(final String jobID, final SimpleSearchModel.Result svo, final View apply_view) {


        ApplyJobsNew.preApplyFlow(GAEvent,jobID, svo.comp_uid_list, svo.frnd_model, mActivity, new ApplyJobsNew.onApplied() {
            @Override
            public void onApplied(String appliedResponse, Dialog plzWaitDialog) {

                svo.is_applied = true;
                Toast.makeText(mActivity, appliedResponse, Toast.LENGTH_SHORT).show();
                if (apply_view.findViewById(R.id.applied_job) != null) {
                    apply_view.findViewById(R.id.apply_job)
                            .setVisibility(View.GONE);
                    apply_view.findViewById(R.id.applied_job)
                            .setVisibility(View.VISIBLE);
                }

                if(!svo.isInsidePostApply)
                    ApplyJobsNew.postApplyFlow(svo, mActivity, plzWaitDialog);
                else
                    DialogUtils.dialogDismiss(plzWaitDialog);

                notifyDataSetChanged();
            }
        }, svo);
    }


    private static class DemoPopupWindow extends BetterPopupWindow {

        public DemoPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView tv_locations = (TextView) root.findViewById(R.id.loc_txt);
            tv_locations.setText(mLocations);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }

    private static class skillPopupWindow extends BetterPopupWindow {

        public skillPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView skills = (TextView) root.findViewById(R.id.loc_txt);
            skills.setText(mskill);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }

    private class JobsViewHolder {

        View apply_job;
        View applied_job;
        TextView frndCount;
        LinearLayout walkin;
        View divider;
        RelativeLayout frndCountRow;
        TextView company;
        TextView title;
        TextView tv_more;
        TextView tv_experience;
        TextView tv_location;
        TextView skills;
        TextView date,venue;
        TextView posted_date;


    }


    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    private ActionMode actionMode;
    private void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        actionMode.setTitle(getSelectedItemCount() + " Jobs selected");
        notify();
    }


    private int getSelectedItemCount() {
        return selectedItems.size();
    }

    public void startActionMode(final View actionButton){
        actionMode = mActivity.startSupportActionMode(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.setTitle("0 Jobs selected");
                actionButton.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionButton.setVisibility(View.GONE);
                actionMode = null;
                selectedItems.clear();
                notifyDataSetChanged();
            }
        });
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectedItems.size()==0) {
                    Toast.makeText(mActivity, "Please select at least one job to apply.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    final ArrayList<String> job_ids = new ArrayList<>();
                    for(int i=0;i<selectedItems.size();i++)
                        job_ids.add(mList.get(selectedItems.keyAt(i)).jobId);
                    ApplyJobsNew.multiApply(mActivity, job_ids, new ApplyJobsNew.MultiAppliedListener() {
                        @Override
                        public void onMultiApplied(String msg) {
                            Toast.makeText(mActivity, msg+"", Toast.LENGTH_SHORT).show();
                            for(int i=0;i<selectedItems.size();i++)
                                mList.get(selectedItems.keyAt(i)).is_applied = true;
                            if(actionMode!=null)
                                actionMode.finish();
                        }
                    });
                }

            }
        });
        notifyDataSetChanged();
    }


    boolean inActionMode(){
        return actionMode!=null;
    }

    void onItemClickInActionMode(int position){
        if(isMultiSupported(position))
            toggleSelection(position);
    }

    private boolean isMultiSupported(int position){

        if(mList.get(position).is_applied || URLConfig.jobAppliedSet.contains(mList.get(position).jobId))
            return false;
        if(!TextUtils.isEmpty(mList.get(position).jRUrl) || mList.get(position).jJobType==2)
            return false;
        return true;
    }

}



