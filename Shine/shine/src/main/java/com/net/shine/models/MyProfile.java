package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by manishpoddar on 22/08/16.
 */
public class MyProfile implements Serializable {
    // @SerializedName("desired_job")
    private ArrayList<DesiredJobDetails> desired_job = new ArrayList<>();



    @SerializedName("certifications")
    private ArrayList<CertificationResult>  certificationModel =new ArrayList<>();

    //@SerializedName("education")
    private ArrayList<EducationDetailModel> education = new ArrayList<>();

    //@SerializedName("jobs")
    private ArrayList<EmploymentDetailModel> jobs = new ArrayList<>();

    //@SerializedName("workex")
    private ArrayList<TotalExperience> workex = new ArrayList<>();

    //@SerializedName("resumes")
    private ArrayList<ResumeDetails> resumes = new ArrayList<>();

    //@SerializedName("skills")
    private ArrayList<SkillResult> skills = new ArrayList<>();

    //@SerializedName("personal_detail")
    private ArrayList<PersonalDetail> personal_detail = new ArrayList<>();


    public ArrayList<DesiredJobDetails> getDesiredJobs() {
        return desired_job;
    }

    public void setDesiredJobs(ArrayList<DesiredJobDetails> desiredJobs) {
        this.desired_job = desiredJobs;
    }

    public ArrayList<EducationDetailModel> getEducations() {
        return education;
    }

    public void setEducations(ArrayList<EducationDetailModel> educations) {
        this.education = educations;
    }

    public ArrayList<EmploymentDetailModel> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<EmploymentDetailModel> jobs) {
        this.jobs = jobs;
    }

    public ArrayList<PersonalDetail> getPersonalDetails() {
        return personal_detail;
    }

    public void setPersonalDetails(ArrayList<PersonalDetail> personalDetails) {
        this.personal_detail = personalDetails;
    }

    public ArrayList<ResumeDetails> getResumes() {
        return resumes;
    }

    public void setResumes(ArrayList<ResumeDetails> resumes) {
        this.resumes = resumes;
    }

    public ArrayList<CertificationResult> getCertificationModel() {
        return certificationModel;
    }

    public void setCertificationModel(ArrayList<CertificationResult> certificationModel) {
        this.certificationModel = certificationModel;
    }

    public ArrayList<SkillResult> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<SkillResult> skills) {
        this.skills = skills;
    }

    public ArrayList<TotalExperience> getWorkex() {
        return workex;
    }

    public void setWorkex(ArrayList<TotalExperience> workex) {
        this.workex = workex;
    }


    @Override
    public String toString() {
        return "MyProfileModel{" +
                "desired_job=" + desired_job +
                ", certificationModel=" + certificationModel +
                ", education=" + education +
                ", jobs=" + jobs +
                ", workex=" + workex +
                ", resumes=" + resumes +
                ", skills=" + skills +
                ", personal_detail=" + personal_detail +
                '}';
    }
}
