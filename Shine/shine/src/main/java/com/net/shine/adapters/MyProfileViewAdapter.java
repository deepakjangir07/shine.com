package com.net.shine.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;


public class MyProfileViewAdapter extends BaseAdapter {
    private Activity mContext;
    private ArrayList<WhoViewMyProfileModel.Result> mList;


    public MyProfileViewAdapter(Activity context, ArrayList<WhoViewMyProfileModel.Result> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        MyProfileViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.my_profile_view_row, parent, false);
            viewHolder = new MyProfileViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (MyProfileViewHolder) v.getTag();
        }
        viewHolder.mCompanyName.setText(mList.get(position).companyName);
        String date=(mList.get(position).latestTimestamp).substring(0,10);
        viewHolder.mDate.setText(ShineCommons.getFormattedDate(date));

        String foundVia=mList.get(position).foundVia;
        if(TextUtils.isEmpty(foundVia))
        {
            foundVia=" | Found via Search";
        }
        else {
            foundVia=" | Found via "+foundVia;
        }
        viewHolder.mFoundVia.setText(foundVia);

        if(mList.get(position).is_new_activity){
            viewHolder.dot.setVisibility(View.VISIBLE);
        }
        else {
            viewHolder.dot.setVisibility(View.GONE);
        }


        if(mList.get(position).contacted && mList.get(position).viewed){
            viewHolder.text1.setVisibility(View.VISIBLE);
            viewHolder.text2.setVisibility(View.VISIBLE);
        }
        else if(mList.get(position).contacted && !mList.get(position).viewed) {
            viewHolder.text1.setVisibility(View.GONE);
            viewHolder.text2.setVisibility(View.VISIBLE);
        }
        else if(!mList.get(position).contacted && mList.get(position).viewed){
            viewHolder.text1.setVisibility(View.VISIBLE);
            viewHolder.text2.setVisibility(View.GONE);
        }
        else {
            viewHolder.text1.setVisibility(View.GONE);
            viewHolder.text2.setVisibility(View.GONE);
        }

        return v;
    }

    public void rebuildListObject(ArrayList itemList) {

        this.mList = itemList;
    }

    class MyProfileViewHolder {
        public TextView mCompanyName;
        public TextView mDate;
        public TextView mFoundVia;

        public  TextView text1,text2;
        public ImageView dot;

        public MyProfileViewHolder(View base) {

            mCompanyName = (TextView) base
                    .findViewById(R.id.company);
            mDate = (TextView) base.findViewById(R.id.date);
            mFoundVia=(TextView)base.findViewById(R.id.foundvia);
            text1=(TextView)base.findViewById(R.id.text1);
            text2=(TextView)base.findViewById(R.id.text2);
            dot = (ImageView)base.findViewById(R.id.new_dot);


        }
    }
}
