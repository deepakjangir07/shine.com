package com.net.shine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.net.shine.MyApplication;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.ProfileCompleteFragment;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.Arrays;

/**
 * Created by ujjawal-work on 19/08/16.
 */

public class LoginEventHandler extends BaseActivity {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            if(ShineSharedPreferences.getResumeMidout(this) && !ShineSharedPreferences.getResumeSkip(this)) {
                Intent intent = new Intent(this,ResumeUploadActivity.class);
                if(getIntent().getExtras()!=null){

                    intent.putExtras(getIntent().getExtras());
                    startActivityForResult(intent, 123);
                }
                else
                {
                    startActivityForResult(intent, 123);
                }
            } else {
                setBaseFrg(ProfileCompleteFragment.newInstance(getIntent().getExtras()));
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Context context = MyApplication.getInstance().getApplicationContext();
        boolean isProfileMidout = ((!ShineSharedPreferences.getExpFlag(context)
                || !ShineSharedPreferences.getEduFlag(context)
                || !ShineSharedPreferences.getSkillFlag(context))
                && !ShineSharedPreferences.getProfileSkip(context));
        if(isProfileMidout){
            setBaseFrg(ProfileCompleteFragment.newInstance(getIntent().getExtras()));
        } else {
            onBackPressed();
        }

    }


    public static boolean isHandlingNeeded(){
        Context context = MyApplication.getInstance().getApplicationContext();
        boolean isProfileMidout = ((!ShineSharedPreferences.getExpFlag(context)
                || !ShineSharedPreferences.getEduFlag(context)
                || !ShineSharedPreferences.getSkillFlag(context))
                && !ShineSharedPreferences.getProfileSkip(context));
        boolean isResumeMidout = ShineSharedPreferences
                .getResumeMidout(context) && !ShineSharedPreferences.getResumeSkip(context);
        return isProfileMidout || isResumeMidout;
    }


    public static boolean isResumeMidout()
    {
        Context context = MyApplication.getInstance().getApplicationContext();
        boolean isResumeMidout = ShineSharedPreferences
                .getResumeMidout(context) && !ShineSharedPreferences.getResumeSkip(context);

        return isResumeMidout;
    }

    public static boolean isProfileMidout()
    {
        Context context = MyApplication.getInstance().getApplicationContext();
        boolean isProfileMidout = ((!ShineSharedPreferences.getExpFlag(context)
                || !ShineSharedPreferences.getEduFlag(context)
                || !ShineSharedPreferences.getSkillFlag(context))
                && !ShineSharedPreferences.getProfileSkip(context));

        return isProfileMidout;
    }

    @Override
    public void onBackPressed() {

        if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(this)))
        {
            finish();

        }
        else
        {
            super.onBackPressed();

        }
    }
}
