package com.net.shine.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ujjawal-work on 09/03/16.
 */
public class RegDumpModel implements Serializable{

    //    Reg-Dump{"id":"56dd60d7ac39214cb28c3d62","candidate_id":"56dd60d7ac39214cb28c3d62","name":"Hakampreet Pandher",
// "candidate_location":243,"profile_title":"Assistant Manager","gender":0,"experience_in_years":2,
// "experience_in_months":0,"is_fresher":true,"functional_areas":[1],"salary_in_lakh":0,"salary_in_thousand":0,
// "education":{"education_specialization":1,"course_type":1,"year_of_passout":2014,"education_level":20,"id":1203,
// "institute_id":7},"job":{"start_month":1,"start_year":2015,"industry":33,"is_current":true,
// "company_name":"HT Media Ltd.","id":2350,"job_title":"Assistant Manager, Software Engineer"},
// "skills":[{"name":"Python","experience":4,"id":4096},{"name":"Django","experience":4,"id":4097},
// {"name":"MongoDB","experience":4,"id":4098},{"name":"SQL","experience":4,"id":4099},{"name":"PHP",
// "experience":4,"id":4100}]}



   // {"id":"58777241d9bb3c3dc677f9ff","candidate_id":"58777241d9bb3c3dc677f9ff","name":"Divya Mittal","candidate_location":243,"profile_title":null,"gender":2,"experience_in_years":2,"experience_in_months":0,"is_fresher":true,"functional_areas":[],"salary_in_lakh":null,"salary_in_thousand":null,"education":{"course_type":1,"institute_name":"werwer","id":1,"education_specialization":2},"job":{"industry":55,"is_current":false,"company_name":"werwe","functional_area":4556,"id":1,"job_title":"werwer"},"skills":[{"name":"werwe","experience":17,"id":1},{"name":"","id":2}]

    public String candidate_id;
    public String name;
    public int candidate_location;
    public String profile_title;
    public int gender;
    public int experience_in_years;
    public int experience_in_months;
    public boolean is_fresher;
    public int[] functional_areas;
    public int salary_in_lakh;
    public int salary_in_thousand;
    public Education education;
    public Job job;
    public ArrayList<Skill> skills = new ArrayList<>();


    public static class Education implements Serializable{
        public int education_specialization;
        public int course_type;
        public int year_of_passout;
        public int education_level;
        public int id;
        public int institute_id;
        public String institute_name;
        @SerializedName("education_specialization_custom")

        public String other_specialization="";

    }

    public static class Job implements Serializable{
        public int start_month;
        public int start_year;
        public int end_month;
        public int end_year;
        public int industry;
        public int functional_area;
        public boolean is_current;
        public String company_name;
        public int id;
        public String job_title;
    }

    public static class Skill implements Serializable{
        public String name;
        public int experience;
        public int id;
    }




}
