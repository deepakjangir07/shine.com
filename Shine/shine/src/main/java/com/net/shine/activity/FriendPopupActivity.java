package com.net.shine.activity;

import android.os.Bundle;

import com.net.shine.fragments.JobApplyFriendFragment;

/**
 * Created by ujjawal-work on 22/08/16.
 */
public class FriendPopupActivity extends BaseActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setLeftDrawer(DRAWER_NO_SELECTION);
        if(savedInstanceState==null){
            setBaseFrg(JobApplyFriendFragment.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setActionBarVisible(false);
    }


}
