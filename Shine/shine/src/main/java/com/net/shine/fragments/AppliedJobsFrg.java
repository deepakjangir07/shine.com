package com.net.shine.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.AppliedJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class AppliedJobsFrg extends BaseFragment implements
		OnItemClickListener, GetConnectionListener,VolleyRequestCompleteListener,AppliedJobsAdapter.PopupJobcountClickListener,SwipeRefreshLayout.OnRefreshListener,AbsListView.OnScrollListener {
	private ListView gridView;
    private AppliedJobsAdapter mAdapter;
	private View loadingView;
	private Set<String> unique_companies = new HashSet<>();
	private View errorLayout;
	private String co_names = "";
	private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private SwipeRefreshLayout swipeContainer;
    public int totalJobs;

    public boolean pulltoRefresh=false;


    public static AppliedJobsFrg newInstance() {
        Bundle args = new Bundle();
        AppliedJobsFrg fragment = new AppliedJobsFrg();
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<SimpleSearchModel.Result>();


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		try {

			inflater.inflate(R.menu.notification_menu, menu);

			final MenuItem alertMenuItem = menu.findItem(R.id.notification);
			alertMenuItem.setActionView(R.layout.notification_badge_layout);
			RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
			final TextView notification_count = (TextView) rootView.findViewById(R.id.txtCount);
			if (!TextUtils.isEmpty(ShineSharedPreferences.getNotificationCount(mActivity,
					ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"))) {
				notification_count.setVisibility(View.VISIBLE);
				notification_count.setText(ShineSharedPreferences.getNotificationCount(mActivity,
						ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"));
			} else {
				notification_count.setVisibility(View.GONE);
			}
			rootView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					notification_count.setVisibility(View.GONE);
					BaseFragment fragment = new NotificationsFrag();
					mActivity.singleInstanceReplaceFragment(fragment);
					ShineCommons.trackShineEvents("Notification","AppliedJobs",mActivity);

					ShineSharedPreferences.saveNotificationCount(mActivity, ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count", "");

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.applied_jobs_scroll_view,
				container, false);

        try {
           swipeContainer=(SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            swipeContainer.setOnRefreshListener(this);
			loadingView = view.findViewById(R.id.loading_cmp);
			gridView = (ListView) view.findViewById(R.id.grid_view);
			errorLayout = view.findViewById(R.id.error_layout);
			errorLayout.setVisibility(View.GONE);
            gridView.setOnScrollListener(this);
			jobList = new ArrayList<>();
            mAdapter=new AppliedJobsAdapter(mActivity,jobList,this);
            gridView.setAdapter(mAdapter);
			gridView.setOnItemClickListener(this);
            downloadAppliedJobs(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
	}

	private void downloadAppliedJobs(boolean isRefresh) {

        if(!isRefresh) {
			DialogUtils.showLoading(mActivity, getString(R.string.loading),
					loadingView);
		}



        Type type = new TypeToken<SimpleSearchModel>(){}.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                URLConfig.JOBS_APPLIED_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                        + "?fl=id,jCID,jRUrl,jCName,jCType,jJT,jKwds,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jJobType,jWLC,jWSD,jExpDate,jWLocID", type);

		req.execute("AppliedJobs");
	}



	private void showresult() {
		{
			try {

				loadingView.setVisibility(View.GONE);


				if (jobList.size() > 0) {
					gridView.setVisibility(View.VISIBLE);
                    mAdapter.rebuildListObject(jobList);

                    mAdapter.notifyDataSetChanged();

				} else {

					String text_before_matched_jobs = "Please check your ";
					String text_matched_jobs = "matched jobs";
					String text_after_matched_jobs = " and apply to get shortlisted";
					String text_final = text_before_matched_jobs+text_matched_jobs+text_after_matched_jobs;

					gridView.setVisibility(View.GONE);
					errorLayout.setVisibility(View.VISIBLE);

					BaseFragment fragment = new JobsTabFrg();
					Bundle bundle = new Bundle();
					bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.MATCHED_TAB);
					fragment.setArguments(bundle);
					DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
							mActivity.getString(R.string.no_applied_jobs_msg),
							text_final,mActivity.getString(R.string.match_jobs),DialogUtils.ERR_NO_SEARCH_RESULT,fragment);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		Bundle bundle = new Bundle();
		bundle.putInt(URLConfig.INDEX, position);
		bundle.putInt(JobDetailFrg.FROM_SCREEN,
				JobDetailFrg.FROM_APPLIED_JOBS_SCREEN);
		bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jobList);
		BaseFragment fragment = new JobDetailParentFrg();
		fragment.setArguments(bundle);
		mActivity.singleInstanceReplaceFragment(fragment);
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SharedPreferences.Editor outState = mActivity.getSharedPreferences(
				"frag", Context.MODE_APPEND).edit();
		outState.putString("fraginfo", "");

		outState.commit();

	}

	private void callGetFriend() {
		GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
		friends.getFriendsInCompanyVolleyRequest(co_names);
	}

    private void invalidateWithFriendsData() {
        for (int m = 0; m < jobList.size(); m++) {
            SimpleSearchModel.Result job = jobList.get(m);
            if (job.comp_uid_list != null) {
                for (int n = 0; n < job.comp_uid_list.size(); n++) {
                    if (friend_list.containsKey(job.comp_uid_list.get(n))) {
                        job.frnd_model = friend_list.get(job.comp_uid_list.get(n));
                    }
                }
            }
        }
        mAdapter.rebuildListObject(jobList);
        mAdapter.notifyDataSetChanged();
        gridView.invalidate();
    }

	@Override
	public void getFriends(HashMap<String, DiscoverModel.Company> result) {
		if (!isAdded())
			return;
		friend_list = result;
		invalidateWithFriendsData();

	}

	@Override
	public void OnDataResponse(Object object,String tag) {


		try {
            if(jobList!=null&&pulltoRefresh)
                jobList.clear();

            pulltoRefresh=false;
            swipeContainer.setRefreshing(false);
			errorLayout.setVisibility(View.GONE);
            SimpleSearchModel model = (SimpleSearchModel) object;

            for(SimpleSearchModel.Result res:model.results)
            {
                int len = res.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += res.job_loc_list.get(j);
                    } else {
                        loc += res.job_loc_list.get(j) + " / ";
                    }
                }
                res.job_loc_str = loc;
            }
            co_names = "";
            totalJobs = model.count;
            jobList.addAll(model.results);

            mAdapter.notifyDataSetChanged();

            if (jobList.size() > 0) {
                for (int i = 0; (i < jobList.size()); i++) {

					co_names = ShineCommons.getCompanyNames(jobList.get(i).comp_uid_list, unique_companies);

                }

            }
            if(co_names.length()>0)
                callGetFriend();
            if (isAdded()) {

                showresult();
            }


		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	@Override
	public void OnDataResponseError(final String error, String tag) {
		try {
			loadingView.setVisibility(View.GONE);
			errorLayout.setVisibility(View.VISIBLE);
			gridView.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);
			mActivity.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
							mActivity.getString(R.string.technical_error),
							mActivity.getString(R.string.technical_error2),"",DialogUtils.ERR_TECHNICAL,null);


				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}



    @Override
    public void JobCountOnClick(DiscoverModel.Company dfm) {


        Bundle bundle = new Bundle();
        SimpleSearchVO svo = new SimpleSearchVO();
        bundle.putSerializable("companyModel", dfm);
        bundle.putSerializable("companyIdObject", svo);
        bundle.putBoolean("fromDiscoverConnect", true);
		BaseFragment fragment = new SearchResultFrg();
		fragment.setArguments(bundle);
		mActivity.replaceFragmentWithBackStack(fragment);
    }

    @Override
    public void onRefresh() {
		errorLayout.setVisibility(View.GONE);
        pulltoRefresh=true;
        downloadAppliedJobs(true);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        try {
            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
