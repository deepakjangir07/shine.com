package com.net.shine.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.FriendPopupActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailFrg;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.views.BetterPopupWindow;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class InboxMailDetailAdapter extends RecyclerView.Adapter<InboxMailDetailAdapter.InboxJobsViewHolder> {


    private BaseActivity mActivity;
    private ArrayList<InboxAlertMailDetailModel.InboxJobDetail> mList;
    private static String mLocations = "";
    private static String mSkill = "";

    public InboxMailDetailAdapter(BaseActivity activity, ArrayList<InboxAlertMailDetailModel.InboxJobDetail> list) {
        this.mActivity = activity;
        this.mList = list;
    }




    @Override
    public InboxJobsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return new InboxJobsViewHolder(li.inflate(R.layout.grid_table_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final InboxJobsViewHolder viewHolder, int position) {
        if (mList.get(position).is_applied() || URLConfig.jobAppliedSet.contains(mList.get(position).getJob_id())) {
            viewHolder.apply_job.setVisibility(View.GONE);
            viewHolder.applied_job.setVisibility(View.VISIBLE);
            viewHolder.similar_jobs.setVisibility(View.VISIBLE);
        } else {
            viewHolder.apply_job.setVisibility(View.VISIBLE);
            viewHolder.applied_job.setVisibility(View.GONE);
            viewHolder.similar_jobs.setVisibility(View.GONE);
            viewHolder.apply_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String jobID = (String) view.getTag();
                    InboxAlertMailDetailModel.InboxJobDetail svo = null;
                    for (int i = 0; i < mList.size(); i++) {
                        if (mList.get(i) == null)
                            continue;
                        if (mList.get(i).getJob_id().equals(jobID)) {
                            svo = mList.get(i);
                            break;
                        }
                    }
                    applyToJob("JobAlertMail", (String) view.getTag(), svo, viewHolder.contentView);


                }
            });
            viewHolder.apply_job.setTag(mList.get(position).getJob_id());
        }

        viewHolder.title.setText(mList.get(position).getTitle());
        viewHolder.company.setText(mList.get(position).getCompany());

        if (mList.get(position).skills == null || mList.get(position).skills.trim().isEmpty()) {
            viewHolder.skills.setVisibility(View.GONE);
        } else {
            viewHolder.skills.setVisibility(View.VISIBLE);
        }


        if (mList.get(position).jJobType == 2) {

            viewHolder.walkin.setVisibility(View.VISIBLE);
            viewHolder.divider.setVisibility(View.VISIBLE);
            viewHolder.frndCountRow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mList.get(position).getVenue())) {
                viewHolder.venue.setVisibility(View.VISIBLE);
                viewHolder.venue.setText(Html.fromHtml("<b>Venue </b> " + mList.get(position).getVenue()));
            } else {
                viewHolder.venue.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(mList.get(position).getDate())) {
                viewHolder.date.setVisibility(View.VISIBLE);
                viewHolder.date.setText(Html.fromHtml("<b>Date </b>" + mList.get(position).getDate()));
            } else {
                viewHolder.date.setVisibility(View.GONE);
            }
        } else {
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.venue.setVisibility(View.GONE);
            viewHolder.walkin.setVisibility(View.GONE);
            viewHolder.walkin.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(mList.get(position).jRUrl)) {
            viewHolder.frndCountRow.setVisibility(View.GONE);
        }


        DiscoverModel.Company mDiscoverFriendsModel = mList.get(position).frnd_model;
        if (mDiscoverFriendsModel != null && mDiscoverFriendsModel.data.friends.size() > 0) {
            setConnection(mDiscoverFriendsModel, mList.get(position), viewHolder);
        } else
            viewHolder.frndCountRow.setVisibility(View.GONE);


        //showMoreInLocation(viewHolder, mList.get(position).location_str, mList.get(position).getExperience());


        StringBuilder location = new StringBuilder();

        if(mList.get(position).getLocation()!=null&&mList.get(position).getLocation().length>0) {
            if (mList.get(position).getLocation().length > 1) {

                for (String locations : mList.get(position).getLocation()) {
                    location = location.append(locations).append("/");
                }
            } else {
                location = location.append(mList.get(position).getLocation()[0]);
            }
        }
        showMoreInLocation(viewHolder, location.toString(), mList.get(position).getExperience(), mList.get(position).skills);

        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InboxAlertMailDetailModel.InboxJobDetail model = mList.get(viewHolder.getAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("IN_JOB_MAIL", true);
                    if (model.is_applied()) {
                        bundle.putString("JOBID", "#" + model.getJob_id());
                    } else {
                        bundle.putString("JOBID", model.getJob_id());
                    }
                    JobDetailFrg frg = new JobDetailFrg();
                    frg.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(frg);



                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        viewHolder.similar_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchVO sVO = new SimpleSearchVO();
                sVO.setJobId(mList.get(viewHolder.getAdapterPosition()).getJob_id());
                sVO.setFromSimilarJobs(true);
                sVO.setHideModifySearchButton(true);
                if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                    String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                    sVO.setUrl(URL);

                } else {
                    sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                BaseFragment fragment = new SearchResultFrg();
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }



    private void setConnection(DiscoverModel.Company dfm, final InboxAlertMailDetailModel.InboxJobDetail searchResultVO, InboxJobsViewHolder viewHolder) {
        {
            viewHolder.frndCountRow.setVisibility(View.VISIBLE);
            int friend_count = dfm.data.friends.size();
            if (friend_count > 1) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#555555'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connections "
                                        + "<font color='#555555'>work here</font>"));
            } else {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#2c84cc'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> "
                                        + "<font color='#555555'> works here</font>"));
            }
            viewHolder.frndCountRow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

//                    showDialog(searchResultVO);

                    Bundle args = new Bundle();
                    args.putSerializable("javo", searchResultVO);
                    args.putInt("fromScreen", 0);
                    Intent intent = new Intent(mActivity, FriendPopupActivity.class);
                    intent.putExtras(args);
                    mActivity.startActivity(intent);

                    ShineCommons.trackShineEvents("KonnectFriendList","JobAlertMail",mActivity);




                }
            });
        }

    }

    public void rebuildListObject(ArrayList<InboxAlertMailDetailModel.InboxJobDetail> itemList) {
        this.mList = itemList;
    }


    private void showMoreInLocation(final InboxJobsViewHolder viewHolder, final String location,
                                   final String exp, final String skill) {


        if (skill != null && !skill.isEmpty()) {

            viewHolder.skills.setText(skill);



            final ViewTreeObserver vto1 = viewHolder.skills.getViewTreeObserver();
            vto1.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {

                    try {
                        if(!vto1.isAlive())
                            return true;

                        vto1.removeOnPreDrawListener(this);
                        if (viewHolder.skills.getLayout() != null) {
                            int count = viewHolder.skills.getLayout().getEllipsisCount(0);
                            Log.d("DEBUG::MORE", "Count for " + skill + " is: " + count);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHolder.skills.getLayoutParams();

                                if (params.getRules()[RelativeLayout.LEFT_OF] != 0) {
                                    params.addRule(RelativeLayout.LEFT_OF, 0);
                                    return false;
                            }
                            return false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return true;
                }


            });

        }


        if (exp == null || exp.equals("")) {
            viewHolder.tv_experience.setVisibility(View.GONE);
        } else {
            viewHolder.tv_experience.setVisibility(View.VISIBLE);
            viewHolder.tv_experience.setText(exp + "  ");
        }

       // viewHolder.tv_location.setText(location);


        final StringTokenizer st = new StringTokenizer(location, "/");
        if (st.countTokens() > 1) {
            final String s2;
            {
                s2 = st.nextToken()+"";
            }

            if(s2.toString().length()>14){
                viewHolder.tv_location.setText(s2.substring(0, 11) + "...");

            }
            else
            {
                viewHolder.tv_location.setText(s2 + " ");
            }
            viewHolder.tv_more.setVisibility(View.VISIBLE);

            viewHolder.tv_more.setText(Html.fromHtml("<font color = '#333333'>"
                    +"  & "
                    +"</font>"
                    +"<font color = '#2c84cc'>"
                    +(st.countTokens())
                    + " more"
                    +"</font>"));

        }
        else
        {
            viewHolder.tv_location.setText(location);
            viewHolder.tv_more.setVisibility(View.GONE);
        }



        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location_more = "";
                String[] arr_loc = location.split("/");
                for (int i = 0; i < arr_loc.length; i++) {
                    if (i == (arr_loc.length - 1)) {
                        location_more = location_more + arr_loc[i].trim();
                    } else {
                        location_more = location_more + arr_loc[i].trim()
                                + ", ";
                    }
                }
                mLocations = location_more;
                DemoPopupWindow demoPopupWindow = new DemoPopupWindow(viewHolder.tv_more);
                demoPopupWindow.showLikePopDownMenu();
            }
        });

        final ViewTreeObserver Vto_loc = viewHolder.tv_location.getViewTreeObserver();
        Vto_loc.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                try {
                    if(!Vto_loc.isAlive())
                        return true;

                    Vto_loc.removeOnPreDrawListener(this);
                    if (viewHolder.tv_location.getLayout() != null) {
                        int count = viewHolder.tv_location.getLayout().getEllipsisCount(0);
                        Log.d("DEBUG::MORE", "Count for " + location + " is: " + count);
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHolder.tv_location.getLayoutParams();
                        if (count > 0) {
                            viewHolder.tv_more.setVisibility(View.VISIBLE);
                            if (params.getRules()[RelativeLayout.LEFT_OF] != R.id.more) {
                                params.addRule(RelativeLayout.LEFT_OF, R.id.more);
                                return false;
                            }
                        } else {
                            viewHolder.tv_more.setVisibility(View.GONE);
                            if (params.getRules()[RelativeLayout.LEFT_OF] != 0) {
                                params.addRule(RelativeLayout.LEFT_OF, 0);
                                return false;
                            }
                        }
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

    }




    private void applyToJob(String GAEvent,final String jobID, final InboxAlertMailDetailModel.InboxJobDetail svo, final View apply_view) {


        SimpleSearchModel.Result sv= new SimpleSearchModel.Result();
        sv.jobId = svo.getJob_id();
        sv.resume_req = svo.resume_req;
        sv.frnd_model = svo.frnd_model;
        sv.comp_name = svo.getCompany();
        sv.jobTitle = svo.getTitle();
        sv.jRUrl = svo.jRUrl;

        ApplyJobsNew.preApplyFlow(GAEvent,jobID, svo.getCompanyUuid(), svo.frnd_model,
                mActivity, new ApplyJobsNew.onApplied() {
                    @Override
                    public void onApplied(String appliedResponse, Dialog plzWaitDialog) {

                        svo.setIs_applied(true);

                        Toast.makeText(mActivity, appliedResponse, Toast.LENGTH_SHORT).show();
                        if (apply_view.findViewById(R.id.applied_job) != null) {

                            apply_view.findViewById(R.id.apply_job)
                                    .setVisibility(View.GONE);
                            apply_view.findViewById(R.id.applied_job)
                                    .setVisibility(View.VISIBLE);
                        }
                        notifyDataSetChanged();

                        ApplyJobsNew.postApplyFlow(svo, mActivity, plzWaitDialog);

                    }
                }, sv);
    }


    private static class DemoPopupWindow extends BetterPopupWindow {

         DemoPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView tv_locations = (TextView) root.findViewById(R.id.loc_txt);
            tv_locations.setText(mLocations);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }

    private static class skillPopupWindow extends BetterPopupWindow {

         skillPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView skills = (TextView) root.findViewById(R.id.loc_txt);
            skills.setText(mSkill);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }


     static class InboxJobsViewHolder extends RecyclerView.ViewHolder{
        View apply_job;
         TextView similar_jobs;
        View applied_job;
        TextView frndCount;
        RelativeLayout frndCountRow;
        TextView company;
        TextView title;
        TextView tv_more;
        TextView tv_experience;
        LinearLayout lin_more;
        TextView skills;
        TextView tv_location;
        TextView date,venue;
        LinearLayout walkin;
         View divider;
        View contentView;


        InboxJobsViewHolder(View v) {
            super(v);
            contentView = v;
            title = (TextView) v.findViewById(R.id.title);
            company = (TextView) v.findViewById(R.id.company);
            frndCount = (TextView) v
                    .findViewById(R.id.frndsCount);
            frndCountRow = (RelativeLayout) v
                    .findViewById(R.id.frnd_row);
            walkin = (LinearLayout) v.findViewById(R.id.walkin);
            divider =  v.findViewById(R.id.divider);
            date = (TextView) v.findViewById(R.id.date);
            venue = (TextView) v.findViewById(R.id.venue);
            apply_job = v.findViewById(R.id.apply_job);
            applied_job = v.findViewById(R.id.applied_job);
            tv_more = (TextView) v.findViewById(R.id.more);
            tv_experience = (TextView) v
                    .findViewById(R.id.experience);
            skills = (TextView) v.findViewById(R.id.skills);
            lin_more = (LinearLayout) v
                    .findViewById(R.id.loc_linear);
            tv_location = (TextView) v.findViewById(R.id.location);
            similar_jobs = (TextView) v.findViewById(R.id.applied_similar_btn_list);
        }
    }

}



