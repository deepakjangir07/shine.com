package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ujjawal-work on 16/08/16.
 */

public class SerializableArrayList<T> implements Serializable {


    private static final long serialVersionUID = 1L;
    private ArrayList<T> original_list;

    public SerializableArrayList(ArrayList<T> items) {
        this.original_list = items;
    }

    public ArrayList<T> getOriginal() {
        return original_list;
    }

}
