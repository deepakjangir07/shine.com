package com.net.shine.fragments;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.dialogs.TermsAndConditionsDialog;
import com.net.shine.interfaces.SocialAppliedListener;
import com.net.shine.models.ApplywithoutResumeModel;
import com.net.shine.models.LinkedInAuthModel;
import com.net.shine.models.SocialApplyProfileModel;
import com.net.shine.models.SocialApplyResponseModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.auth.SocialAuthUtils;

import org.apache.http.util.TextUtils;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Created by ujjawal-work on 19/08/16.
 */

public class SocialApplyFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener {


    public static String IS_SOCIAL_APPLY = "is_social_apply";


    public static SocialApplyFragment newInstance( Bundle args) {
        SocialApplyFragment fragment = new SocialApplyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        mActivity.getMenuInflater().inflate(R.menu.social_apply_menu, menu);


        if(getArguments()!=null && menu!=null) {
            MenuItem item = menu.findItem(R.id.icon_social_apply);
            if(item==null)
                return;
            item.setVisible(true);
            int auth_type = getArguments().getInt("auth_type", 0);
            switch (auth_type){
                case SocialAuthUtils.LINKEDIN_AUTH:
                    item.setIcon(R.drawable.linkedin_menu);
                    break;
                case SocialAuthUtils.GOOGLE_AUTH:
                    item.setIcon(R.drawable.google_menu);
                    break;
                case SocialAuthUtils.FACEBOOK_AUTH:
                    item.setIcon(R.drawable.facebook_menu);
                    break;
            }
        }
    }




    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Complete form and apply");

    }

    private CheckBox checkBox;
    private EditText name, email, mobile, city, functionalArea, industry, experience, salary,
            currentCompany, currentDesignation;
    private TextInputLayout inputLayoutEmail, inputLayoutName, inputLayoutMobile, inputLayoutCity,
            inputLayoutFunctionalArea, inputLayoutIndustry, inputLayoutExperience, inputLayoutSalary,
            inputLayoutCurrentCompany, inputLayoutCurrentDesignation;
    private String jobId;
    private TextView resume_file_name;
    private TextView upload_btn_txt;
//    private View containerView;
    private boolean alertsCheck = true;
    private View registerLayout;
    private String social_access_token;
    private String social_expiry;
    private int auth_type;
    private SocialApplyProfileModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.social_apply_fragment, container, false);

        resume_file_name = (TextView) view.findViewById(R.id.resume_file_name);
        upload_btn_txt = (TextView) view.findViewById(R.id.upload_btn_txt);


        View upload_btn = view.findViewById(R.id.upload_resume_btn);
        upload_btn.setOnClickListener(this);

        registerLayout = view.findViewById(R.id.register_layout);
        registerLayout.setOnClickListener(this);
        view.findViewById(R.id.terms).setOnClickListener(this);
//        containerView = view.findViewById(R.id.linear_social_apply_container);
        checkBox = (CheckBox) view.findViewById(R.id.register_check);
        checkBox.setSelected(alertsCheck);
        checkBox.setChecked(alertsCheck);
        name = (EditText) view.findViewById(R.id.name);
        email = (EditText) view.findViewById(R.id.email);
        mobile = (EditText) view.findViewById(R.id.mobile_field);
        city = (EditText) view.findViewById(R.id.city);
        functionalArea = (EditText) view.findViewById(R.id.fun_area);
        industry = (EditText) view.findViewById(R.id.industry);
        experience = (EditText) view.findViewById(R.id.total_exp);
        salary = (EditText) view.findViewById(R.id.salary);
        currentCompany = (EditText) view.findViewById(R.id.current_company);
        currentDesignation = (EditText) view.findViewById(R.id.job_title);

        inputLayoutName = (TextInputLayout) view.findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        inputLayoutMobile = (TextInputLayout) view.findViewById(R.id.input_layout_mobile);
        inputLayoutCity = (TextInputLayout) view.findViewById(R.id.input_layout_city);
        inputLayoutFunctionalArea = (TextInputLayout) view.findViewById(R.id.input_layout_fun_area);
        inputLayoutIndustry = (TextInputLayout) view.findViewById(R.id.input_layout_industry);
        inputLayoutExperience = (TextInputLayout) view.findViewById(R.id.input_layout_exp);
        inputLayoutSalary = (TextInputLayout) view.findViewById(R.id.input_layout_salary);
        inputLayoutCurrentCompany = (TextInputLayout) view.findViewById(R.id.input_layout_company);
        inputLayoutCurrentDesignation = (TextInputLayout) view.findViewById(R.id.input_layout_jon_title);

        TextChangeListener.addListener(name, inputLayoutName, mActivity);
//        TextChangeListener.addListener(email, inputLayoutEmail, mActivity);
        TextChangeListener.addListener(mobile, inputLayoutMobile, mActivity);
        TextChangeListener.addListener(city, inputLayoutCity, mActivity);
        TextChangeListener.addListener(functionalArea, inputLayoutFunctionalArea, mActivity);
        TextChangeListener.addListener(industry, inputLayoutIndustry, mActivity);
        TextChangeListener.addListener(experience, inputLayoutExperience, mActivity);
        TextChangeListener.addListener(salary, inputLayoutSalary, mActivity);
        TextChangeListener.addListener(currentCompany, inputLayoutCurrentCompany, mActivity);
        TextChangeListener.addListener(currentDesignation, inputLayoutCurrentDesignation, mActivity);


        int citSelectedPos = -1;    //cannot be 0 as it refers to #TopCities
        int salaryPos = -1;
        int indSelectedPos = -1;
        int funSelectedPos = -1;

        try {
            if (city != null && city.getTag() != null)
                citSelectedPos = (int) city.getTag();
            if (industry != null && industry.getTag() != null)
                indSelectedPos = (int) industry.getTag();
            if (functionalArea != null && functionalArea.getTag() != null)
                funSelectedPos = (int) functionalArea.getTag();
            if (salary != null && salary.getTag() != null)
                salaryPos = (int) salary.getTag();
        } catch (Exception e) {
            e.printStackTrace();
        }

        city.setTag(citSelectedPos);
        city.setOnClickListener(this);
        city.setOnFocusChangeListener(this);
        functionalArea.setTag(funSelectedPos);
        functionalArea.setOnClickListener(this);
        functionalArea.setOnFocusChangeListener(this);
        industry.setTag(indSelectedPos);
        industry.setOnClickListener(this);
        industry.setOnFocusChangeListener(this);
        salary.setTag(salaryPos);
        salary.setOnClickListener(this);
        salary.setOnFocusChangeListener(this);


        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String nameText = name.getText().toString().trim();
                    if (name.length() == 0 || !(Pattern.matches(ShineCommons.NAME_REGEX, nameText))) {
                        name.setTextColor(getResources().getColor(
                                R.color.red));
                        inputLayoutName.setError(getResources().getString(
                                R.string.name_invalid_msg));
                    }
                }
            }
        });

        mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String mobileText = mobile.getText().toString().trim();
                    if (mobileText.length() < 10) {
                        mobile.setTextColor(getResources().getColor(
                                R.color.red));
                        inputLayoutMobile.setError("Enter your 10-digit Mobile No");
                    } else if ("0".equals("" + mobileText.charAt(0))) {
                        mobile.setTextColor(getResources().getColor(
                                R.color.red));
                        inputLayoutMobile.setError("Mobile number should not start with 0");
                    } else {
                        boolean flag = false;
                        int n = mobileText.length();
                        for (int i = 0; i < n; i++) {
                            if (mobileText.charAt(i) < 48
                                    || mobileText.charAt(i) > 57) {
                                flag = true;
                                break;
                            }
                        }
                        if (flag) {
                            mobile.setTextColor(getResources().getColor(
                                    R.color.red));
                            inputLayoutMobile.setError("" + R.string.MOBILE_NO_INVALID_CHAR_MSG);
                        }
                    }

                }

            }
        });

        Bundle bundle = getArguments();
        try {

            Log.d(">>>>>>>>","  "+bundle);


            Log.d(">>>>>>>>","  "+bundle.getString("comp_name", ""));

            Log.d(">>>>>>>>","  "+bundle.getString("job_title", ""));
            model = (SocialApplyProfileModel) bundle.getSerializable("model");
            jobId = bundle.getString("job_id", "");
            social_access_token = bundle.getString("social_access_token", "");
            social_expiry = bundle.getString("social_expiry", "");
            auth_type = bundle.getInt("auth_type", 0);
            String comp_name = bundle.getString("comp_name", "");
            String job_title = bundle.getString("job_title", "");
            TextView tv_comp_name = (TextView) view.findViewById(R.id.company);
            TextView tv_designation = (TextView) view.findViewById(R.id.designation);
            tv_comp_name.setText(comp_name);
            tv_designation.setText(job_title);
            fillDataFromModel(model);
            View btn = view.findViewById(R.id.apply_btn);
            btn.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.terms:
                TermsAndConditionsDialog termsDialog = new TermsAndConditionsDialog();
                FragmentManager fm = getChildFragmentManager();
                termsDialog.show(fm, "terms_check");
                break;

            case R.id.apply_btn:
                final SocialApplyProfileModel newModel = getDataFromViews();
                if (newModel == null) //null means we got validation error
                    return;
                ApplyJobsNew.socialApplyUser(mActivity, jobId, newModel, new SocialAppliedListener() {
                    @Override
                    public void onApplied(String msg, SocialApplyResponseModel token, Dialog dialog) {
                        Toast.makeText(mActivity, msg + "", Toast.LENGTH_SHORT).show();
                        Bundle bundle = new Bundle();
                        if(model.userDetail!=null && !TextUtils.isEmpty(model.userDetail.candidate_id) && !TextUtils.isEmpty(model.userDetail.user_token))
                        {
                            //user was a resume_midout user
                            bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                            bundle.putInt(ShineAuthUtils.COMPLETED_FROM, ShineAuthUtils.FROM_SOCIAL_APPLY_REGISTRATION);
                            SocialAuthUtils.getUserStatus(mActivity, model.userDetail.candidate_id, model.userDetail.user_token, model.userDetail.email,
                                    social_access_token, social_expiry, model.profile_picture_url, bundle, dialog,auth_type, null);
                        } else if(alertsCheck){
                            //user chose register with shine
                            bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                            bundle.putInt(ShineAuthUtils.COMPLETED_FROM, ShineAuthUtils.FROM_SOCIAL_APPLY_REGISTRATION);
                            SocialAuthUtils.loginUser(social_access_token, social_expiry, auth_type, bundle, mActivity, dialog, null);
                            //Hit Social Login API, and show apt. flow....
                        }
                        else {
                            DialogUtils.dialogDismiss(dialog);
                            ApplywithoutResumeModel awr = new ApplywithoutResumeModel();
                            awr.job_id = jobId;
                            Bundle bundle2 = new Bundle();
                            bundle2.putSerializable("apply_model", awr);
                            AppliedWithoutResumeConfirmation frg = new AppliedWithoutResumeConfirmation();
                            frg.setArguments(bundle2);
                            mActivity.replaceFragment(frg);
                        }
                    }
                } , alertsCheck);
                break;
            case R.id.register_layout:
                if (alertsCheck) {
                    alertsCheck = false;
                    checkBox.setChecked(false);
                } else {
                    alertsCheck = true;
                    checkBox.setChecked(true);
                }
                break;

            case R.id.upload_resume_btn:
                Bundle bundle = new Bundle();
                bundle.putBoolean(IS_SOCIAL_APPLY, true);
                ResumeUploadFrg rup = ResumeUploadFrg.newInstance(bundle);
                rup.setTargetFragment(this, 0);
                mActivity.singleInstanceReplaceFragment(rup);

                break;

            default:
                showSpinner(v.getId());
                ShineCommons.hideKeyboard(mActivity);
        }
    }

    private LinkedInAuthModel.Resume resume;

    public void onResumeAcquired(final byte[] base64Resume, final String fileName){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {

                    mActivity.showNavBar();


                    Log.d("onResumeAcquired ","base64  "+fileName);


                    Log.d("onResumeAcquired ","base64  "+base64Resume.toString());

                    if (base64Resume!=null && base64Resume.length!=0) {
                        resume = new LinkedInAuthModel.Resume();
                        resume.encoded_string = Base64.encodeToString(base64Resume, Base64.DEFAULT);
                        resume.resume_name = fileName;
                        if (!TextUtils.isEmpty(resume.resume_name)) {
                            resume_file_name.setText(resume.resume_name);
                            resume_file_name.setVisibility(View.VISIBLE);
                            resume_file_name.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        File file = new File(MyApplication.getResumeDir(), fileName);
                                        Uri uri = FileProvider.getUriForFile(getContext(), "com.net.shine", file);
                                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        intent.setData(uri);
                                        intent.putExtra(Intent.EXTRA_STREAM, uri);
                                        startActivity(intent);
                                    }catch (ActivityNotFoundException e){
                                        Toast.makeText(getContext(), "No document viewer available", Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                    catch (Exception e){
                                        Toast.makeText(getContext(), "Unable to open document", Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            upload_btn_txt.setText("Change Resume");
                        } else {
                            resume_file_name.setVisibility(View.GONE);
                            upload_btn_txt.setText("Upload Resume");
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Invalid resume", Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showSpinner(int id) {
        try {

            switch (id) {


                case R.id.fun_area:

                    DialogUtils.showNewMultiSpinnerDialogRadio(
                            getString(R.string.hint_func_area), functionalArea,
                            URLConfig.FUNCTIONAL_AREA_LIST2, mActivity);
                    break;
                case R.id.industry:

                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_industry), industry,
                            URLConfig.INDUSTRY_LIST3, mActivity);
                    break;
                case R.id.city:

                    DialogUtils.showNewMultiSpinnerDialogRadio(
                            getString(R.string.hint_city), city,
                            URLConfig.CITY_LIST_EDIT, mActivity);

                    break;

                case R.id.salary:
                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
                            salary, URLConfig.ANNUAL_SALARYL_LIST, mActivity);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void fillDataFromModel(final SocialApplyProfileModel model) {

        resume = model.resume;
        if(resume!=null && !TextUtils.isEmpty(resume.resume_name)){
            resume_file_name.setText(resume.resume_name);
            resume_file_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String resume_url = URLConfig.SOCIAL_APPLY_RESUME_URL.replace(URLConfig.CANDIDATE_ID, model.id);
                    Log.d("resume url", resume_url);
                    intent.setData(Uri.parse(resume_url));
                    startActivity(intent);
                }
            });
            resume_file_name.setVisibility(View.VISIBLE);
            upload_btn_txt.setText("Change Resume");
        } else {
            resume_file_name.setVisibility(View.GONE);
            upload_btn_txt.setText("Upload Resume");
        }


        if(model.userDetail!=null && !TextUtils.isEmpty(model.userDetail.candidate_id) && !TextUtils.isEmpty(model.userDetail.user_token))
            registerLayout.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(model.name))
            name.setText(model.name);
        if (!TextUtils.isEmpty(model.email))
            email.setText(model.email);
        if (!TextUtils.isEmpty(model.cell_phone))
            mobile.setText(model.cell_phone);

        if (model.candidate_location != 0)
            city.setTag(ShineCommons.setTagAndValue(
                    URLConfig.CITY_REVERSE_MAP,
                    URLConfig.CITY_LIST_EDIT, model.candidate_location,
                    city));

        if (model.functional_area != 0)
            functionalArea.setTag(ShineCommons.setTagAndValue(
                    URLConfig.FUNCTIONA_AREA_REVERSE_MAP, URLConfig.FUNCTIONAL_AREA_LIST2, model.functional_area,
                    functionalArea));

        if (model.industry != 0)
            industry.setTag(ShineCommons.setTagAndValue(
                    URLConfig.INDUSTRY_REVERSE_MAP,
                    URLConfig.INDUSTRY_LIST3, model.industry,
                    industry));


        if (model.salary_in_lakh != 0)
            salary.setTag(ShineCommons.setTagAndValue(
                    URLConfig.ANNUAL_SALARYL_REVRSE_MAP,
                    URLConfig.ANNUAL_SALARYL_LIST, model.salary_in_lakh,
                    salary));

        if (!TextUtils.isEmpty(model.current_company_name))
            currentCompany.setText(model.current_company_name);
        if (!TextUtils.isEmpty(model.current_job_title))
            currentDesignation.setText(model.current_job_title);
        if (!TextUtils.isEmpty(model.experience_in_years))
            experience.setText(model.experience_in_years);


    }


    private SocialApplyProfileModel getDataFromViews() {

        try {

            String nameText = name.getText().toString().trim();
            String mobileText = mobile.getText().toString().trim();
            String totalExp = experience.getText().toString().trim();
            String emailText = email.getText().toString().trim();
            String functionalAreaText = functionalArea.getText().toString().trim();
            String industryText = industry.getText().toString().trim();
            String cityText = city.getText().toString().trim();
            String salaryText = salary.getText().toString().trim();
            String compName = currentCompany.getText().toString().trim();
            String designation = currentDesignation.getText().toString().trim();
            boolean checkForToastAlert = false;

            if (nameText.length() == 0 || !(Pattern.matches(ShineCommons.NAME_REGEX, nameText))) {
                checkForToastAlert = true;
                inputLayoutName.setError("Please enter valid name");
            }
            if (emailText.trim().length() == 0) {
                checkForToastAlert = true;
                inputLayoutEmail.setError("Please enter valid email");
            }
            if (mobileText.trim().length() == 0) {
                checkForToastAlert = true;
                inputLayoutMobile.setError("Please enter valid mobile number");
            }
            int cityIndex = -1;
            if (cityText.length() == 0) {
                checkForToastAlert = true;
                inputLayoutCity.setError("Please enter valid city");
            } else {
                cityIndex = Integer.parseInt(URLConfig.CITY_MAP.get(cityText));
            }
            if (designation.length() == 0) {
                checkForToastAlert = true;
                inputLayoutCurrentDesignation.setError("Please enter job title");
            }
            if (compName.length() == 0) {
                checkForToastAlert = true;
                inputLayoutCurrentCompany.setError("Please enter company name");
            }
            int salaryIndex = -1;
            if (salaryText.length() == 0) {
                checkForToastAlert = true;
                inputLayoutSalary.setError("Please enter current salary");
            } else {
                salaryIndex = Integer.parseInt(URLConfig.ANNUAL_SALARYL_REVRSE_MAP.get(salaryText));
            }
            int indIndex = -1;
            if (industryText.length() == 0) {
                checkForToastAlert = true;
                inputLayoutIndustry.setError("Please enter industry");
            } else {
                indIndex = Integer.parseInt(URLConfig.INDUSTRY_MAP.get(industryText));
            }
            int funcIndex = -1;
            if (functionalAreaText.length() == 0) {
                checkForToastAlert = true;
                inputLayoutFunctionalArea.setError("Please enter functional area");
            } else {
                funcIndex = Integer.parseInt(URLConfig.FUNCTIONA_AREA_MAP.get(functionalAreaText));
            }

            String exp_lookup = "";
            if (totalExp.length() == 0) {
                checkForToastAlert = true;
                inputLayoutExperience.setError("Please enter valid experience");
            } else {
                int exp = Integer.parseInt(totalExp);
                if (exp == 0)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("0 Yr");
                else if (exp == 1)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("1 Yr");
                else if (exp < 25)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get(exp + " Yrs");
                else
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("> 25 Yrs");
                if (exp_lookup == null || exp_lookup.isEmpty()) {
                    checkForToastAlert = true;
                    inputLayoutExperience.setError("Please enter valid experience");
                }
            }


            if (!Pattern.matches(ShineCommons.REGEX, emailText)) {
                email.setTextColor(getResources().getColor(R.color.red));
                inputLayoutEmail.setError("Please enter valid email");
                email.requestFocus();
                return null;
            }

            if (mobileText.length() < 10) {
                mobile.setTextColor(getResources().getColor(R.color.red));
                inputLayoutMobile.setError("Please enter  10 digit mobile number");
                mobile.requestFocus();
                return null;
            } else if ("0".equals("" + mobileText.charAt(0))) {
                mobile.setTextColor(getResources().getColor(R.color.red));
                inputLayoutMobile.setError("Mobile number should not start with 0.");
                mobile.requestFocus();
                return null;
            } else {
                boolean flag = false;
                int n = mobileText.length();
                for (int i = 0; i < n; i++) {
                    if (mobileText.charAt(i) < 48 || mobileText.charAt(i) > 57) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    mobile.setTextColor(getResources().getColor(R.color.red));
                    inputLayoutMobile.setError("Mobile number should be numeric");
                    mobile.requestFocus();
                    return null;
                }
            }

            if (checkForToastAlert)
                return null;

            if (indIndex == -1 || cityIndex == -1 || funcIndex == -1 || salaryIndex == -1)
                return null;
            model.email = emailText;
            model.salary_in_lakh = salaryIndex;
            model.cell_phone = mobileText;
            model.industry = indIndex;
            model.candidate_location = cityIndex;
            model.name = nameText;
            model.functional_area = funcIndex;
            model.current_company_name = compName;
            model.current_job_title = designation;
            model.experience_in_years = exp_lookup;
            model.resume = resume;

            if (model.resume==null || TextUtils.isEmpty(model.resume.resume_name)) {
                Toast.makeText(getContext(), "Please upload a resume.", Toast.LENGTH_SHORT).show();
                return null;
            }

            return model;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }
    }

}
