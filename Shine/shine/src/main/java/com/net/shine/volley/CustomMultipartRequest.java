package com.net.shine.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.net.shine.MyApplication;

import org.apache.http.entity.mime.MultipartEntity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;
public class CustomMultipartRequest<T> extends JsonRequest<T> {

	private Response.Listener<T> listener;
//	private Response.ErrorListener mEListener;

	private final Gson gson = new Gson();

	private long cacheTimeToLive = 0;

//	private Map < String, String > header;
	private MultipartEntity entity;
	private Type type;


	public CustomMultipartRequest(int method, String url, Response.Listener<T> listener, Response.ErrorListener errorListener, Type type, MultipartEntity entity ) {
		super(method, url, null,listener, errorListener);
		this.listener = listener;
		this.entity = entity;
		this.type = type;
	}

	@Override
	public Map < String, String > getHeaders() throws AuthFailureError {

		return MyApplication.getInstance().getMultiHeaders();
	}

	@Override  
	public String getBodyContentType() {  
		return entity.getContentType().getValue();  
	}

	@Override  
	public byte[] getBody() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();  
		try {  
			entity.writeTo(bos);  
//			String entityContentAsString = new String(bos.toByteArray());
//			Log.e("volley", entityContentAsString);
		} catch (IOException e) {  
			VolleyLog.e("IOException writing to ByteArrayOutputStream");  
		}  
		return bos.toByteArray();  
	}  

	@Override  
	protected Response <T> parseNetworkResponse(NetworkResponse response) {
		// TODO Auto-generated method stub  
		try {

			// Volley does not handle null properly, so implement null response
			// check
			if (response.data.length == 0) {
				byte[] responseData = "{}".getBytes("UTF8");
				response = new NetworkResponse(response.statusCode,
						responseData, response.headers, response.notModified);
				}

			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));

			Log.d("Volley::MULTIPART", "JsonResponse: " + jsonString);


			T parseObject = gson.fromJson(jsonString, type);



			return Response.success(parseObject,
					parseIgnoreCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			System.out.println("VolleyQueue: Encoding Error for " + getTag()
					+ " (" + getSequence() + ")");
			return Response.error(new ParseError(e));
		} 
	}  

	public Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
		long now = System.currentTimeMillis();

		Map<String, String> headers = response.headers;
		long serverDate = 0;
		String serverEtag;
		String headerValue;

		headerValue = headers.get("Date");
		if (headerValue != null) {
			serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
		}
		serverEtag = headers.get("ETag");

		final long cacheHitButRefreshed = VolleyNetworkRequest.FIFTEEN_MINUTES;
		final long cacheExpired = cacheTimeToLive;
		final long softExpire = now + cacheHitButRefreshed;
		final long ttl = now + cacheExpired;

		Cache.Entry entry = new Cache.Entry();
		entry.data = response.data;
		entry.etag = serverEtag;
		entry.softTtl = softExpire;
		entry.ttl = ttl;
		entry.serverDate = serverDate;
		entry.responseHeaders = headers;

		return entry;
	}

	@Override
	protected void deliverResponse(T response) {

		listener.onResponse(response);
		// TODO Auto-generated method stub
	}

	@Override
	public void deliverError(VolleyError error) {

		super.deliverError(error);

	}


}