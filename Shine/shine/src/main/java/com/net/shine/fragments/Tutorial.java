package com.net.shine.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;


public class Tutorial extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private int img1;
    private ImageView tutorial_a;




    public static Tutorial newInstance(int param1) {
        Tutorial fragment = new Tutorial();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Bundle bun = getArguments();
        img1 = bun.getInt(ARG_PARAM1);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tutorial, container, false);
        tutorial_a = (ImageView) v.findViewById(R.id.tutorial_a);
        tutorial_a.setImageResource(img1);
        return v;
    }

}