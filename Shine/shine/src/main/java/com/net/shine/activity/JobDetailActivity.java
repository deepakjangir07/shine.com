package com.net.shine.activity;

import android.os.Bundle;

import com.net.shine.R;
import com.net.shine.fragments.JobDetailParentFrg;

/**
 * Created by ujjawal-work on 17/08/16.
 */

public class JobDetailActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState==null){
            setBaseFrg(JobDetailParentFrg.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(getString(R.string.title_job_details));
    }
}
