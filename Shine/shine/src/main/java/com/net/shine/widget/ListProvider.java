package com.net.shine.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.net.shine.R;
import com.net.shine.models.SimpleSearchModel;

import java.util.ArrayList;

/**
 * Created by manishpoddar on 09/12/16.
 */

public class ListProvider implements RemoteViewsService.RemoteViewsFactory{
    private ArrayList<SimpleSearchModel.Result> mList = new ArrayList<>();
    private Context context = null;
    private int appWidgetId;

    public static final String MyApplyOnClick = "myApplyOnClickTag";

    private  Bundle b;

    public ListProvider(Context context, Intent intent) {
        this.context = context;

         appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
         Log.e("Widget",appWidgetId+"");

           b=intent.getExtras();

        Log.e("Widget","Bundle "+b);




        //this.mList= (ArrayList<SimpleSearchModel.Result>) b.getSerializable("list");


        // if(mList!=null)

        Log.d("Widget ","ListProvider List "+mList.size());
        //if(!(mList!=null&&mList.size()>0))
           populateListItem();
    }

    private void populateListItem() {
        if(RemoteFetchService.listItemList !=null ) {
            this.mList = (ArrayList<SimpleSearchModel.Result>) RemoteFetchService.listItemList.clone();
            Log.d("Widget","populatedItem Called");

            Log.d("Widget","populatedItem nexrt "+RemoteFetchService.listItemList.clone()+" "+RemoteFetchService.listItemList.size());


        }
//        else {
//            this.mList = new ArrayList<>();
//        }

        Log.d("Widget","populatedItem Called");

//        for (int i = 0; i < 10; i++) {
//            ListItem listItem = new ListItem();
//            listItem.heading = "Heading" + i;
//            listItem.content = i
//                    + " This is the content of the app widget listview.Nice content though";
//            listItemList.add(listItem);
//        }
        Log.e("Widget",mList.size()+"");

    }

    @Override
    public int getCount() {

        if(mList.size()>0)
            return mList.size();
        else
        return RemoteFetchService.listItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews v = new RemoteViews(
                context.getPackageName(), R.layout.widget_list_row);


        if(mList==null||mList.size()==0) {
            populateListItem();
        }
        v.setTextViewText(R.id.title,mList.get(position).jobTitle);
        v.setTextViewText(R.id.company,mList.get(position).comp_name);
        v.setTextViewText(R.id.location,mList.get(position).job_loc_str);

        v.setTextViewText(R.id.experience,mList.get(position).jobExperience);


        Bundle extras = new Bundle();
        extras.putInt(WidgetProvider.EXTRA_ITEM, position);
       /// extras.putSerializable(WidgetProvider.LIST_ITEM,listItem);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        // Make it possible to distinguish the individual on-click
        // action of a given item
        v.setOnClickFillInIntent(R.id.apply, fillInIntent);

        v.setOnClickFillInIntent(R.id.card_view, fillInIntent);



        return v;
    }


    protected PendingIntent getPendingSelfIntent(Context context, String action)
    {
        // if we brodcast than definetly we have to define ONRECEIVE
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }









    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {
    }

    @Override
    public void onDestroy() {
    }

}



