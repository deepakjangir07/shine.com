package com.net.shine.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.gson.Gson;
import com.net.shine.R;
import com.net.shine.activity.AuthActivity;
import com.net.shine.activity.HomeActivity;
import com.net.shine.activity.JobDetailActivity;
import com.net.shine.activity.MyProfileActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.ArrayList;

import static com.net.shine.config.URLConfig.APPLY_FROM_WIDGET;
import static com.net.shine.widget.RemoteFetchService.listItemList;

/**
 * Created by manishpoddar on 09/12/16.
 */

public class WidgetProvider  extends AppWidgetProvider {

    public static final String SEARCH="SEARCH_BUTTON";
    public static final String LOGIN="com.net.shine.widget.LOGIN_BUTTON";
    public static final String UPDATE="com.net.shine.widget.UPDATE_BUTTON";
    public static final String APPLY="APPLY_BUTTON";
    public static final String APPLY_ACTION = "com.net.shine.widget.APPLY_ACTION";
    public static final String JD_VIEW_ACTION = "com.net.shine.widget.JD_VIEW_ACTION";
    public static final String EXTRA_ITEM = "com.net.shine.widget.EXTRA_ITEM";
    public static final String LIST_ITEM = "com.net.shine.widget.LIST_ITEM";

    Intent toastIntent;
    Intent svcIntent;
    UserStatusModel userStatusModel;
    // String to be sent on Broadcast as soon as Data is Fetched
    // should be included on WidgetProvider manifest intent action
    // to be recognized by this WidgetProvider to receive broadcast
    public static final String DATA_FETCHED = "com.net.shine.DATA_FETCHED";


/*
     * this method is called every 30 mins as specified on widgetinfo.xml this
     * method is also called on every phone reboot from this method nothing is
     * updated right now but instead RetmoteFetchService class is called this
     * service will fetch data,and send broadcast to WidgetProvider this
     * broadcast will be received by WidgetProvider onReceive which in turn
     * updates the widget
     */

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
         userStatusModel= ShineSharedPreferences.getUserInfo(context);


        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {

            Log.d("WIDGET","OnUpdate Called");
            Intent serviceIntent = new Intent(context, RemoteFetchService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    appWidgetIds[i]);
            context.startService(serviceIntent);

            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);



             toastIntent = new Intent(context, WidgetProvider.class);
            // Set the action for the intent.
            // When the user touches a particular view, it will have the effect of
            // broadcasting TOAST_ACTION.
            toastIntent.setAction(WidgetProvider.APPLY_ACTION);
            toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            if(svcIntent!=null)
            svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

            PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(R.id.listViewWidget, toastPendingIntent);

            appWidgetManager.updateAppWidget(appWidgetIds[i], rv);

           // appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i],R.id.listViewWidget);

        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    private RemoteViews updateWidgetListView(Context context, int appWidgetId) {

        // which layout to show on widget

        userStatusModel= ShineSharedPreferences.getUserInfo(context);


        //Bundle bundle=new Bundle();

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);

        Log.e("app wid id",appWidgetId+"");

        // RemoteViews Service needed to provide adapter for ListView
         svcIntent = new Intent(context, WidgetService.class);
        // passing app widget id to that RemoteViews Service
        svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);


        //bundle.putSerializable("list",mList);
        // setting a unique Uri to the intent
        // don't know its purpose to me right now

        //svcIntent.putExtras(bundle);

        svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));
        // setting adapter to listview of the widget
        remoteViews.setRemoteAdapter(R.id.listViewWidget,
                svcIntent);

        // setting an empty view in case of no data

        remoteViews.setEmptyView(R.id.listViewWidget, R.id.empty_view);

//        if(userStatusModel!=null&& !TextUtils.isEmpty(userStatusModel.candidate_id)){
//
//            Log.d("WIDGET","Update profile");
//            remoteViews.setViewVisibility(R.id.tvnojobs, View.VISIBLE);
//            remoteViews.setViewVisibility(R.id.login_btn,View.GONE);
//            remoteViews.setViewVisibility(R.id.update_btn,View.VISIBLE);
//
//
//        }
//        else
//        {
//
//            Log.d("WIDGET","Login profile");
//            remoteViews.setViewVisibility(R.id.tvnojobs, View.GONE);
//            remoteViews.setViewVisibility(R.id.login_btn,View.VISIBLE);
//            remoteViews.setViewVisibility(R.id.update_btn,View.GONE);
//
//        }

        if(userStatusModel==null||TextUtils.isEmpty(userStatusModel.candidate_id))
        {

            Log.d("WIDGET","Login profile");
            remoteViews.setViewVisibility(R.id.tvnojobs, View.GONE);
            remoteViews.setViewVisibility(R.id.login_btn,View.VISIBLE);
            remoteViews.setViewVisibility(R.id.update_btn,View.GONE);
            remoteViews.setViewVisibility(R.id.listViewWidget,View.GONE);
            //listItemList=new ArrayList<>();

        }
        else
        {

            remoteViews.setViewVisibility(R.id.tvnojobs, View.GONE);
            remoteViews.setViewVisibility(R.id.login_btn,View.GONE);
            remoteViews.setViewVisibility(R.id.update_btn,View.GONE);
            remoteViews.setViewVisibility(R.id.listViewWidget,View.VISIBLE);


        }

        remoteViews.setOnClickPendingIntent(R.id.login_btn,getPendingSelfIntent(context,LOGIN));
        remoteViews.setOnClickPendingIntent(R.id.update_btn,getPendingSelfIntent(context,UPDATE));
        remoteViews.setOnClickPendingIntent(R.id.search, getPendingSelfIntent(context, SEARCH));




       // remoteViews.setOnClickPendingIntent(R.id.apply, getPendingSelfIntent(context, APPLY));

        return remoteViews;
    }


    protected PendingIntent getPendingSelfIntent(Context context, String action)
    {
        // if we brodcast than definetly we have to define ONRECEIVE
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

/*
     * It receives the broadcast as per the action set on intent filters on
     * Manifest.xml once data is fetched from RemotePostService,it sends
     * broadcast and WidgetProvider notifies to change the data the data change
     * right now happens on ListProvider as it takes RemoteFetchService
     * listItemList as data
     */

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);


        try {
            AppWidgetManager mgr = AppWidgetManager.getInstance(context);


            Log.d("OnReceive ","app widget "+ intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID));

            Log.d("OnReceive ","app widget "+ intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID)+" action "+intent.getAction());

            if(intent!=null&&intent.getAction()!=null) {

                if (listItemList == null || listItemList.size() == 0) {
                    String listItems = ShineSharedPreferences.getWidgetList(context);

                    Log.d("WIDGET ", "List items" +listItems);

                    try {

                        SimpleSearchModel result = new Gson().fromJson(listItems, SimpleSearchModel.class);

                        listItemList = new ArrayList<>();

                        listItemList.clear();


                        Log.d("WIDGET_LIST","list "+result);

                        if(result!=null&&result.results.size()>0) {
                            for (SimpleSearchModel.Result res : result.results) {
                                int len = res.job_loc_list.size();
                                String loc = "";
                                for (int j = 0; j < len; j++) {
                                    if (j >= len - 1) {
                                        loc += res.job_loc_list.get(j);
                                    } else {
                                        loc += res.job_loc_list.get(j) + " / ";
                                    }
                                }
                                res.job_loc_str = loc;
                            }


                            listItemList.addAll(result.results);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        return;
                    }
                }



                if (intent.getAction().equals(APPLY_ACTION)) {
                    int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                            AppWidgetManager.INVALID_APPWIDGET_ID);
                    int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);

                    Log.d("WIDGET ", "List " + intent.getExtras().toString() + "listItemList " + listItemList.size());



                    if (listItemList != null && listItemList.size() > 0) {

                       try {
                           ArrayList<SimpleSearchModel.Result> list = new ArrayList<>();
                           list.add(listItemList.get(viewIndex));
                           Bundle bundle = new Bundle();
                           bundle.putInt(URLConfig.INDEX, viewIndex);
                           bundle.putSerializable(JobDetailParentFrg.JOB_LIST, list);
                           bundle.putBoolean(APPLY_FROM_WIDGET, false);
                           Intent intent1 = new Intent(context, JobDetailActivity.class);
                           intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                           intent1.putExtras(bundle);
                           context.startActivity(intent1);

                           ShineCommons.trackShineEvents("Widget","JobListClick",context);

                       }
                       catch (Exception e)
                       {
                           e.printStackTrace();
                       }

                    } else {
                        Toast.makeText(context, URLConfig.PARSING_ERROR_MSG, Toast.LENGTH_SHORT).show();
                    }
                }


                if (intent.getAction().equals(JD_VIEW_ACTION)) {
                    int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                            AppWidgetManager.INVALID_APPWIDGET_ID);
                    int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);

                    Log.d("WIDGET ", "List " + intent.getExtras().toString());
                    if (listItemList != null && listItemList.size() > 0) {
                        ArrayList<SimpleSearchModel.Result> list = new ArrayList<>();
                        list.add(listItemList.get(viewIndex));
                        Bundle bundle = new Bundle();
                        bundle.putInt(URLConfig.INDEX, viewIndex);
                        bundle.putSerializable(JobDetailParentFrg.JOB_LIST, list);
                        bundle.putBoolean(APPLY_FROM_WIDGET, false);
                        Intent intent1 = new Intent(context, JobDetailActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent1.putExtras(bundle);
                        context.startActivity(intent1);

                    } else {
                        Toast.makeText(context, URLConfig.PARSING_ERROR_MSG, Toast.LENGTH_SHORT).show();
                    }
                }

                if (intent.getAction().equals(DATA_FETCHED)) {
                    int appWidgetId = intent.getIntExtra(
                            AppWidgetManager.EXTRA_APPWIDGET_ID,
                            AppWidgetManager.INVALID_APPWIDGET_ID);

                   // ArrayList<SimpleSearchModel.Result> mList= (ArrayList<SimpleSearchModel.Result>) intent.getSerializableExtra("list");
//
//
//                    Log.d("Widget","mList "+mList.size());
//                     RemoteFetchService.listItemList=mList;
//
//                    Log.d("Widget","mList1 "+RemoteFetchService.listItemList.size());

                    AppWidgetManager appWidgetManager = AppWidgetManager
                            .getInstance(context);
                    RemoteViews remoteViews = updateWidgetListView(context, appWidgetId);

                    appWidgetManager.updateAppWidget(appWidgetId, remoteViews);

                    remoteViews.setViewVisibility(R.id.listViewWidget,View.VISIBLE);


//                    appWidgetManager.notify();
                    appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.listViewWidget);
                }

                if (intent.getAction().equals(SEARCH)) {
                    context.startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }

                if (intent.getAction().equals(LOGIN)) {

                    userStatusModel=ShineSharedPreferences.getUserInfo(context);

                    if(userStatusModel==null) {

                        Bundle bundle = new Bundle();
                        intent = new Intent(context, AuthActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);
                        bundle.putInt(AuthActivity.SELECTED_FRAG, AuthActivity.LOGIN_FRAG);
                        intent.putExtras(bundle);
                        context.startActivity(intent);

                        ShineCommons.trackShineEvents("Widget","Login",context);


                    }
                    return;
                }

                if (intent.getAction().equals(UPDATE)) {


                    Bundle bundle = new Bundle();
                    intent = new Intent(context, MyProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                    ShineCommons.trackShineEvents("Widget","Update Profile",context);

                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);

        Log.d("WIDGET","DELETE WIDGET");


        ShineSharedPreferences.saveWidgetList(context,new SimpleSearchModel());
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        Log.d("WIDGET","ENABLED WIDGET");
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d("WIDGET","DISABLED WIDGET");

    }
}