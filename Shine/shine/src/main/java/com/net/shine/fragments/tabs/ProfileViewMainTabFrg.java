package com.net.shine.fragments.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;

import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.AutomatchJobFragment;
import com.net.shine.fragments.ProfileViewEmailFragment;
import com.net.shine.fragments.ProfileViewedSimilarJobs;
import com.net.shine.fragments.SearchRankFragment;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deepak on 21/12/16.
 */

public class ProfileViewMainTabFrg extends ProfileViewsCustomTabFrg {

    public static final int APPLICATION_TAB = 0;

    Bundle b;

    WhoViewMyProfileModel.Result model;


    public static ProfileViewMainTabFrg newInstance(Bundle args) {
        ProfileViewMainTabFrg fragment = new ProfileViewMainTabFrg();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(model.companyName);
        mActivity.showSelectedNavButton(BaseActivity.MORE_OPTION);
        mActivity.showBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            b=getArguments();
            model = (WhoViewMyProfileModel.Result) b.getSerializable("list_item");
        }

            currentSelected=APPLICATION_TAB;


    }


    @Override
    protected void PageSelected(int position) {
        super.PageSelected(position);

        if(position==0) {

            ManualScreenTracker.manualTracker("Applications-ProfileView");}
        else if(model.automatchJobsCount>0){
            if (model.mailCount>0){
                if(position==1){
                    ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                }
                else if(position==2){
                    ManualScreenTracker.manualTracker("Emails-ProfileView");
                }
                else if(position==3){
                    ManualScreenTracker.manualTracker("Search Rank-ProfileView");
                }
            }
            else if(model.avg_rank>0){

                if(position==1){
                    ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                }
                else if(position==2){

                    ManualScreenTracker.manualTracker("Search Rank-ProfileView");

                }
                else if(position==3){
                    ManualScreenTracker.manualTracker("Emails-ProfileView");
                }

            }
            else {

                if(position==1){
                    ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                }
                else if(position==2){
                    ManualScreenTracker.manualTracker("Emails-ProfileView");
                }
                else if(position==3){
                    ManualScreenTracker.manualTracker("Search Rank-ProfileView");
                }
            }
        }
        else {
            if (model.mailCount > 0){
                if(model.avg_rank > 0) {

                    if(position==1){

                        ManualScreenTracker.manualTracker("Emails-ProfileView");
                    }
                    else if(position==2){
                        ManualScreenTracker.manualTracker("Search Rank-ProfileView");
                    }
                    else if(position==3){

                        ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                    }

                }
                else {

                    if(position==1){

                        ManualScreenTracker.manualTracker("Emails-ProfileView");
                    }
                    else if(position==2){
                        ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                    }
                    else if(position==3){
                        ManualScreenTracker.manualTracker("Search Rank-ProfileView");
                    }
                }
            }
            else {
                if(model.avg_rank>0){

                    if(position==1){
                        ManualScreenTracker.manualTracker("Search Rank-ProfileView");
                    }
                    else if(position==2){
                        ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                    }
                    else if(position==3){
                        ManualScreenTracker.manualTracker("Emails-ProfileView");
                    }

                }
                else {

                    if(position==1){
                        ManualScreenTracker.manualTracker("AutoMatch Jobs-ProfileView");
                    }
                    else if(position==2){
                        ManualScreenTracker.manualTracker("Emails-ProfileView");
                    }
                    else if(position==3){
                        ManualScreenTracker.manualTracker("Search Rank-ProfileView");

                    }


                }
            }

        }

    }




    @Override
    public List<Fragment> getFragments() {

        SimpleSearchVO svo = new SimpleSearchVO();
        svo.setFromJD(true);
        svo.setRect_id(model.rectId+"");
        svo.setKeyword(model.companyName);
        b.putSerializable(URLConfig.SEARCH_VO_KEY, svo);
        b.putSerializable("companyModel", null);
        b.putBoolean("fromWhoViewed",true);


        List<Fragment> fList = new ArrayList<>();
        AutomatchJobFragment f1=   AutomatchJobFragment.newInstance(b);
        ProfileViewedSimilarJobs f2= ProfileViewedSimilarJobs.newInstance(b);
        f2.setTargetFragment(this, 19);
        ProfileViewEmailFragment f3 = ProfileViewEmailFragment.newInstance(b);
        SearchRankFragment f4 = SearchRankFragment.newInstance(b);

        if(model.automatchJobsCount>0){
            if (model.mailCount>0){
                fList.add(f2);
                fList.add(f1);
                fList.add(f3);
                fList.add(f4);
            }
            else if(model.avg_rank>0){

                fList.add(f2);
                fList.add(f1);
                fList.add(f4);
                fList.add(f3);
            }
            else {
                fList.add(f2);
                fList.add(f1);
                fList.add(f3);
                fList.add(f4);
            }
        }
        else {
            if (model.mailCount > 0){
                if(model.avg_rank > 0) {
                fList.add(f2);
                fList.add(f3);
                fList.add(f4);
                fList.add(f1);
            }
            else {
                    fList.add(f2);
                    fList.add(f3);
                    fList.add(f1);
                    fList.add(f4);
            }
            }
            else {
                if(model.avg_rank>0){
                    fList.add(f2);
                    fList.add(f4);
                    fList.add(f1);
                    fList.add(f3);

                }
                else {
                    fList.add(f2);
                    fList.add(f1);
                    fList.add(f3);
                    fList.add(f4);

                }
            }

        }


        return fList;
    }

    @Override
    public ArrayList<String> getNames() {
        ArrayList<String> tabName = new ArrayList<>();
        tabName.add(Html.fromHtml("<font color = '#ffaa00'>"+model.applicationActivities+"</font>") + "\n Applications");
        String rank = "";
        if(model.avg_rank>100){
            rank = "100+";
        }
        else {
            rank =model.avg_rank.toString();
        }


        if(model.automatchJobsCount>0){
            if (model.mailCount>0){

                tabName.add(model.automatchJobsCount+"\n Automatches");
                tabName.add(model.mailCount + "\n Emails");
                tabName.add(rank + "\nSearch Rank");
            }
            else if(model.avg_rank>0){


                tabName.add(model.automatchJobsCount+"\n Automatches");
                tabName.add(rank + "\nSearch Rank");
                tabName.add(model.mailCount + "\n Emails");
            }
            else {

                tabName.add(model.automatchJobsCount+"\n Automatches");
                tabName.add(model.mailCount + "\n Emails");
                tabName.add(rank + "\nSearch Rank");
            }
        }
        else {
            if (model.mailCount > 0){
                if(model.avg_rank > 0) {

                    tabName.add(model.mailCount + "\n Emails");
                    tabName.add(rank + "\nSearch Rank");
                    tabName.add(model.automatchJobsCount+"\n Automatches");
                }
                else {

                    tabName.add(model.mailCount + "\n Emails");
                    tabName.add(model.automatchJobsCount+"\n Automatches");
                    tabName.add(rank + "\nSearch Rank");
                }
            }
            else {
                if(model.avg_rank>0){

                    tabName.add(rank + "\nSearch Rank");
                    tabName.add(model.automatchJobsCount+"\n Automatches");
                    tabName.add(model.mailCount + "\n Emails");

                }
                else {

                    tabName.add(model.automatchJobsCount+"\n Automatches");
                    tabName.add(model.mailCount + "\n Emails");
                    tabName.add(rank + "\nSearch Rank");
                }
            }

        }


        return tabName;
    }
}
