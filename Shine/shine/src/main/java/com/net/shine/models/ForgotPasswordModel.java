package com.net.shine.models;

import java.io.Serializable;

/**
 * Created by dhruv on 30/7/15.
 */
public class ForgotPasswordModel implements Serializable {

    String email = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
