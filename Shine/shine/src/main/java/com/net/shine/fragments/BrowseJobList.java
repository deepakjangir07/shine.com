package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.BrowseJobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.models.SimpleSearchVO;

/**
 * Created by manishpoddar on 28/08/17.
 */

public class BrowseJobList extends BaseFragment implements View.OnClickListener {

    private TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8;


    public static BrowseJobList newInstance(){

        Bundle bundle = new Bundle();
        BrowseJobList fragment = new BrowseJobList();
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.browse_job_list,container,false);
        textView1 = (TextView) view.findViewById(R.id.textview1);
        textView2 = (TextView) view.findViewById(R.id.textview2);
        textView3 = (TextView) view.findViewById(R.id.textview3);
        textView4 = (TextView) view.findViewById(R.id.textview4);
        textView5 = (TextView) view.findViewById(R.id.textview5);
        textView6 = (TextView) view.findViewById(R.id.textview6);
        textView7 = (TextView) view.findViewById(R.id.textview7);
        textView8 = (TextView) view.findViewById(R.id.textview8);

        textView1.setOnClickListener(this);
        textView2.setOnClickListener(this);
        textView3.setOnClickListener(this);
        textView4.setOnClickListener(this);
        textView5.setOnClickListener(this);
        textView6.setOnClickListener(this);
        textView7.setOnClickListener(this);
        textView8.setOnClickListener(this);

        if(getArguments()!=null){
           int selectedTab = getArguments().getInt(MainTabFrg.SELECTED_TAB);

            if(selectedTab== BrowseJobsTabFrg.INDUSTRY){
                textView1.setText("IT - Software Jobs");
                textView2.setText("Recruitment Services Jobs");
                textView3.setText("BPO / Call Center Jobs");
                textView4.setText("Banking / Financial Services Jobs");
                textView5.setText("Medical / Healthcare Jobs");
                textView6.setText("Manufacturing Jobs");
                textView7.setText("Automobile / Auto Ancillaries Jobs");
                textView8.setText("Management Consulting Strategy Jobs");
            }

            else if(selectedTab==BrowseJobsTabFrg.FUNCTIONAL_AREA){
                textView1.setText("IT- Software Jobs");
                textView2.setText("Sales / BD Jobs");
                textView3.setText("Customer Services / Back Office Jobs");
                textView4.setText("Production / Maintenance / Service Jobs");
                textView5.setText("Finance / Accounts / Investment Banking Jobs");
                textView6.setText("Marketing / Advertising / Mr / Pr / Events Jobs");
                textView7.setText("Quality / Testing (QA-QC) Jobs ");
                textView8.setText("HR / Recruitment Jobs");
            }
            else if (selectedTab==BrowseJobsTabFrg.CITY){
                textView1.setText("Mumbai Jobs");
                textView2.setText("Bangalore Jobs");
                textView3.setText("Pune Jobs");
                textView4.setText("Hyderabad Jobs");
                textView5.setText("Delhi Jobs");
                textView6.setText("Gurgaon Jobs");
                textView7.setText("Chennai Jobs");
                textView8.setText("Chandigarh Jobs");
            }

            else if(selectedTab==BrowseJobsTabFrg.SKILLS){
                textView1.setText("Java Jobs");
                textView2.setText("PHP Jobs");
                textView3.setText("Marketing Jobs");
                textView4.setText("Accounting Jobs");
                textView5.setText("Data Entry Jobs");
                textView6.setText("Construction Jobs");
                textView7.setText("Design Jobs");
                textView8.setText("HR Jobs");

            }

        }


        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.textview1:
                submitSearch(textView1.getText().toString());
                break;

            case R.id.textview2:
                submitSearch(textView2.getText().toString());
                break;

            case R.id.textview3:
                submitSearch(textView3.getText().toString());
                break;

            case R.id.textview4:
                submitSearch(textView4.getText().toString());
                break;

            case R.id.textview5:
                submitSearch(textView5.getText().toString());
                break;

            case R.id.textview6:
                submitSearch(textView6.getText().toString());
                break;

            case R.id.textview7:
                submitSearch(textView7.getText().toString());
                break;

            case R.id.textview8:
                submitSearch(textView8.getText().toString());
                break;

        }

    }

    private void submitSearch(String keyword){

        SimpleSearchVO sVO = new SimpleSearchVO();
        sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);
        sVO.setKeyword(keyword);

        BaseFragment fragment = new SearchResultFrg();
        Bundle bundle = new Bundle();
        bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
        fragment.setArguments(bundle);
        mActivity.replaceFragmentWithBackStack(fragment);

    }
}
