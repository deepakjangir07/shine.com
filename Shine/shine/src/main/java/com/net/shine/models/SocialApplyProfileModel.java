package com.net.shine.models;

import android.text.TextUtils;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ujjawal-work on 07/06/16.
 */

public class SocialApplyProfileModel implements Serializable{


    public String id;
    public int candidate_location;
    public String cell_phone;
    public String country_code;
    public String current_company_name;
    public String current_job_title;
    public String email;
//    public String experience_in_months;
    public String experience_in_years;
    public int functional_area;
    public int industry;
    public String modified;
    public String profile_picture_url;
    public String name;
    public int profile_source;
    public int salary_in_lakh;
//    public int salary_in_thousand;
    public String source_id;
    public LinkedInAuthModel.Resume resume;

    public LinkedInAuthModel.UserDetail userDetail;

    public boolean isDataStale(){

        if(!TextUtils.isEmpty(modified) && modified.contains("T")){
            try {
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS", Locale.getDefault());
                Date dateObj = dt.parse(modified);
                Log.d("DEBUG::TIME_DIFF", (System.currentTimeMillis() - dateObj.getTime())+"");
                return System.currentTimeMillis() - dateObj.getTime() > 24 * 60 * 60 * 1000;
            }catch (Exception e){
                e.printStackTrace();
            }


            try {
                String date = modified.substring(0,10);
                String time = modified.substring(11,19);
                String formatted = date + " " + time;
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date dateObj = dt.parse(formatted);
                Log.d("DEBUG::TIME_DIFF", formatted);
                return System.currentTimeMillis() - dateObj.getTime() > 24 * 60 * 60 * 1000;
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        return true;

    }




}
