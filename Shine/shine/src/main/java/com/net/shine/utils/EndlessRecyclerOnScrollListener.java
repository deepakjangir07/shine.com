package com.net.shine.utils;

/**
 * Created by deepak on 28/7/15.
 */

import android.support.v7.widget.RecyclerView;

/**
 * Custom Scroll listener for RecyclerView.
 * Based on implementation https://gist.github.com/ssinss/e06f12ef66c51252563e
 */
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    public boolean loading = false; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 0; // The minimum amount of items to have below your current scroll position before loading more.
   private int firstVisibleItem, visibleItemCount, totalItemCount;

    private int currentPage = 1;

   private RecyclerViewPositionHelper mRecyclerViewHelper;

    @Override
    public synchronized void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(recyclerView);
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mRecyclerViewHelper.getItemCount();
        firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();

        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached
            // Do something
            currentPage++;
            onLoadMore(currentPage);
        }



    }

    public int getFirstVisibleItem(){
        if(mRecyclerViewHelper!=null)
            return mRecyclerViewHelper.findFirstVisibleItemPosition();
        else
            return firstVisibleItem;
    }

    //Start loading
    public abstract void onLoadMore(int currentPage);
}