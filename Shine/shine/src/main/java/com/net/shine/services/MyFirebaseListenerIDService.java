package com.net.shine.services;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by manishpoddar on 12/10/16.
 */

public class MyFirebaseListenerIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        Intent intent = new Intent(this, NewSendFCMIdService.class);
        startService(intent);
    }
}
