package com.net.shine.models;

import java.io.Serializable;


public class ReferralRequestModel implements Serializable{


    String Url = "";
    String job_id = "";
    String referrer_sid = "";
    String referrer_uid = "";
    int mode;
    String organization = "";
    String referrer_name = "";
    String referrer_image_url = "";
    String candidate_name = "";
    String referral_id = "";


    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getReferral_id() {
        return referral_id;
    }

    public void setReferral_id(String referral_id) {
        this.referral_id = referral_id;
    }

    public String getCandidate_name() {
        return candidate_name;
    }

    public void setCandidate_name(String candidate_name) {
        this.candidate_name = candidate_name;
    }

    public String getReferrer_image_url() {
        return referrer_image_url;
    }

    public void setReferrer_image_url(String referrer_image_url) {
        this.referrer_image_url = referrer_image_url;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getReferrer_name() {
        return referrer_name;
    }

    public void setReferrer_name(String referrer_name) {
        this.referrer_name = referrer_name;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getReferrer_uid() {
        return referrer_uid;
    }

    public void setReferrer_uid(String referrer_uid) {
        this.referrer_uid = referrer_uid;
    }

    public String getReferrer_sid() {
        return referrer_sid;
    }

    public void setReferrer_sid(String referrer_sid) {
        this.referrer_sid = referrer_sid;
    }

}
