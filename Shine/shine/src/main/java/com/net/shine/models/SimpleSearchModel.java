package com.net.shine.models;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.net.shine.config.URLConfig;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by jaspreet on 31/7/15.
 */
public  class SimpleSearchModel implements Serializable{




    public int count;
    public String next = "";
    public String previous = "";
    public ArrayList<Result> results = new ArrayList<>();


    public JsonElement facets;

    @Override
    public String toString() {
        return "SimpleSearchModel{" +
                "count=" + count +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", results=" + results +
                ", facets=" + facets +
                '}';
    }

    //Result class needs to be static because in case of JD from Email, we need to create a
    //new instance of Empty Result without creating the whole SimpleSearchModel
    public static class Result implements Serializable {


        public boolean isInsidePostApply = false;

        /**Job Listing Fields**/

        @SerializedName("jRR")
        public int resume_req = 1;

        public String jRUrl = "";

        @SerializedName("id")
        public String jobId = "";

        @SerializedName("jJobType")
        public int jJobType = 0;


        @SerializedName("jWLC")
        public String jWLC = "";

        @SerializedName("jWLocID")
        public int jWLocID;

        @SerializedName("jWSD")
        public String jWSD;

        @SerializedName("jExpDate")
        public String jExpDate;

        String walkInDate = null;
        String walkInVenue = null;

        public String getVenue(){

            if(!TextUtils.isEmpty(walkInVenue))
                return walkInVenue;

            try {
                if(!TextUtils.isEmpty(URLConfig.CITY_REVERSE_MAP.get(jWLocID+""))){
                    walkInVenue = jWLC + ", "  + URLConfig.CITY_REVERSE_MAP.get(jWLocID+"");
                } else {
                    walkInVenue = jWLC;
                }
                return walkInVenue;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            walkInVenue = jWLC;
            return walkInVenue;
        }

        public String getDate() {

            if(!TextUtils.isEmpty(walkInDate))
                return walkInDate;

            try{

            Date startDate = null;
            SimpleDateFormat serverDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat clientDateFormatter = new SimpleDateFormat("dd MMM", Locale.getDefault());

                if(jWSD!=null&&jWSD.length()>=10){
                String StrstartDate = jWSD.substring(0,10);
                startDate = serverDateFormatter.parse(StrstartDate);
            }
            Date expDate = null;
//            Date expTime = null;
            if(jExpDate.length()>=10){
                String StrexpDate = jExpDate.substring(0,10);
                expDate = serverDateFormatter.parse(StrexpDate);
//                String StrexpTime = jExpDate.substring(11,19);
//                expTime = serverTimeFormatter.parse(StrexpTime);
            }

            walkInDate =  clientDateFormatter.format(startDate) + " - " + clientDateFormatter.format(expDate);
//                +
//                    " | " + clientTimeFormatter.format(startTime) + " - " + clientTimeFormatter.format(expTime);

            }catch (Exception e){
                e.printStackTrace();
            }

            return walkInDate;

        }



        @SerializedName("jJT")
        public String jobTitle = "";

        public boolean is_applied=false;

        @SerializedName("jCUID")
        public ArrayList<String> comp_uid_list = new ArrayList<>() ;

        @SerializedName("jCName")
        public String comp_name = "";

        @Override
        public String toString() {
            return "Result{" +
                    "resume_req=" + resume_req +
                    ", jRUrl='" + jRUrl + '\'' +
                    ", jobId='" + jobId + '\'' +
                    ", jobTitle='" + jobTitle + '\'' +
                    ", is_applied=" + is_applied +
                    ", comp_uid_list=" + comp_uid_list +
                    ", comp_name='" + comp_name + '\'' +
                    ", rect_id='" + rect_id + '\'' +
                    ", is_company=" + is_company +
                    ", job_loc_list=" + job_loc_list +
                    ", job_loc_str='" + job_loc_str + '\'' +
                    ", company_slug='" + company_slug + '\'' +
                    ", title_slug='" + title_slug + '\'' +
                    ", frnd_model=" + frnd_model +
                    ", jobExperience='" + jobExperience + '\'' +
                    ", job_skills='" + job_skills + '\'' +
                    ", total_skills='" + total_skills + '\'' +
                    ", other_skills='" + other_skills + '\'' +
                    ", job_area_list=" + job_area_list +
                    ", job_func_area_str='" + job_func_area_str + '\'' +
                    ", jobIndustries='" + jobIndustries + '\'' +
                    ", jobSalary='" + jobSalary + '\'' +
                    ", job_desc='" + job_desc + '\'' +
                    ", posted_date='" + posted_date + '\'' +
                    ", comp_desc='" + comp_desc + '\'' +
                    '}';
        }

        @SerializedName("jCID")
        public String rect_id = "";

        @SerializedName("jCType")
        boolean is_company;

        @SerializedName("jLoc")
        public List<String> job_loc_list = new ArrayList<>();
        public String job_loc_str = "";

        @SerializedName("jCName_slug")
        public String company_slug = "";

        @SerializedName("jJT_slug")
        public String title_slug = "";

        //Gson will NOT fill this var, we fill it on getting the connections
        //Do not add a default value for this var, as
        //  - null means we need to make a conn hit
        //  - empty model means 0 connections in comp
        public DiscoverModel.Company frnd_model;

        @SerializedName("jExp")
        public String jobExperience = "";




        /**Job Detail Extra Fields**/

        @SerializedName("jKwd")
        public String total_skills = "";

        @SerializedName("jKwds")
        public String job_skills = "";

        @SerializedName("jKwdns")
        public String other_skills = "";

        //TODO Ujjawal: We can try using JsonElement instead of using two variables,
        //              its getAsString() method works good but needs testing.
        @SerializedName("jArea")
        public  ArrayList<String> job_area_list = new ArrayList<>();
        public  String job_func_area_str = "";

        @SerializedName("jInd")
        public String jobIndustries = "";

        @SerializedName("jSal")
        public String jobSalary = "";

        @SerializedName("jJD")
        public  String job_desc = "";

        @SerializedName("jPDate")
        public String posted_date = "";

        @SerializedName("jCD")
        public String comp_desc = "";

        @SerializedName("is_shortlisted")
        public boolean is_shortlisted = false;

        String postedDate = null;


        public String getPostedDate() {

            if(!TextUtils.isEmpty(postedDate))
                return postedDate;

            try{

                Date currentDate = Calendar.getInstance().getTime();
                Date posted = null;
                SimpleDateFormat serverDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String StrpostedDate="";
                String outputDateStr="";


                if(posted_date.length()>=10){
                    StrpostedDate = posted_date.substring(0,10);


                    DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
                    posted = inputFormat.parse(StrpostedDate);
                    outputDateStr = outputFormat.format(posted);
                    Log.d("dates",StrpostedDate+" "+ outputDateStr);
                }

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;
                long diff = 0;

                if(posted!=null){
                    diff = currentDate.getTime()-posted.getTime();
                    long elapsedDays = diff / daysInMilli;
                    System.out.println(elapsedDays + "posted_date_diff_day" );

                    if(elapsedDays==0)
                        postedDate = "Today";
                    else if(elapsedDays==1)
                        postedDate = "Yesterday";
                    else if(elapsedDays>1 && elapsedDays<=30)
                        postedDate = elapsedDays+" days ago";
                    else
                        postedDate = outputDateStr;
                }
                else
                    postedDate = "";







            }catch (Exception e){
                e.printStackTrace();
            }

            return postedDate;

        }


    }


}
