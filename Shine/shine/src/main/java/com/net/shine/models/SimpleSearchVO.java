package com.net.shine.models;

import java.io.Serializable;

public class SimpleSearchVO implements Serializable, Cloneable {


	@Override
	public SimpleSearchVO clone() throws CloneNotSupportedException {
		return (SimpleSearchVO) super.clone();
	}

	private String keyword = "";
	private String location = "";
	private String customJobAlertLocTag = "";
	private String minExp = "";
	private int minExpTag = 0;
    private String customJobAlertLocText="";

	private String minSal = "";
	private int minSalTag = 0;

	private String funcArea = "";
	private int funcAreaTag = 0;
	private String customJobAlerMinfuncAreaTag = "";
	private String indusType = "";
	private int indusTypeTag = 0;
	private String customJobAlerIndusTypeTag = "";
	private boolean homeFrg;
	private String customJobAlertName = "";

	private String url = "";
	private String rect_id = "";
	private boolean fromJD;
	private String jobId = "";
	private boolean fromSimilarJobs;
	private boolean fromSliderSearch;
	private boolean isAdvanceSearch;
	private long customJobLastUpdateTime = 0;
	private long locationTag = 0;

	private String companyId="";


	public String getCustomJobAlertLocText() {
        return customJobAlertLocText;
    }

    public void setCustomJobAlertLocText(String customJobAlertLocText) {
        this.customJobAlertLocText = customJobAlertLocText;
    }


	public String getCustomJobAlertName() {
		return customJobAlertName;
	}

	public void setCustomJobAlertName(String customJobAlertName) {
		this.customJobAlertName = customJobAlertName;
	}

	public long getCustomJobLastUpdateTime() {
		return customJobLastUpdateTime;
	}

	public void setCustomJobLastUpdateTime(long customJobLastUpdateTime) {
		this.customJobLastUpdateTime = customJobLastUpdateTime;
	}

	public boolean isHomeFrg() {
		return homeFrg;
	}

	public void setHomeFrg(boolean homeFrg) {
		this.homeFrg = homeFrg;
	}

	public int getMinExpTag() {
		return minExpTag;
	}

	public void setMinExpTag(int minExpTag) {
		this.minExpTag = minExpTag;
	}

	public int getMinSalTag() {
		return minSalTag;
	}

	public void setMinSalTag(int minSalTag) {
		this.minSalTag = minSalTag;
	}

	public int getFuncAreaTag() {
		return funcAreaTag;
	}

	public void setFuncAreaTag(int funcAreaTag) {
		this.funcAreaTag = funcAreaTag;
	}

	public int getIndusTypeTag() {
		return indusTypeTag;
	}

	public void setIndusTypeTag(int indusTypeTag) {
		this.indusTypeTag = indusTypeTag;
	}


	public boolean isFromSimilarJobs() {
		return fromSimilarJobs;
	}

	public void setFromSimilarJobs(boolean fromSimilarJobs) {
		this.fromSimilarJobs = fromSimilarJobs;
	}
	public boolean isFromJD(){
		return fromJD;
	}
	public void setFromJD(boolean fromJD) {
		this.fromJD = fromJD;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getRect_id(){return rect_id;}
	public void setRect_id(String rect_id){
		this.rect_id=rect_id;
	}

	private boolean hideModifySearchButton;

	public boolean isHideModifySearchButton() {
		return hideModifySearchButton;
	}

	public void setHideModifySearchButton(boolean hideModifySearchButton) {
		this.hideModifySearchButton = hideModifySearchButton;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMinExp() {
		return minExp;
	}

	public void setMinExp(String minExp) {
		this.minExp = minExp;
	}

	public String getMinSal() {
		return minSal;
	}

	public void setMinSal(String minSal) {
		this.minSal = minSal;
	}

	public String getFuncArea() {
		return funcArea;
	}

	public void setFuncArea(String funcArea) {
		this.funcArea = funcArea;
	}

	public String getIndusType() {
		return indusType;
	}

	public void setIndusType(String indusType) {
		this.indusType = indusType;
	}

	@Override
	public String toString() {
		// for debugging purpose
		return "Keyword= " + keyword + " Location= " + location + " MinExp= "
				+ minExp + " MinSal= " + minExp + " FunctionalArea " + funcArea;
	}

	public long getLocationTag() {
		return locationTag;
	}

	public void setLocationTag(long locationTag) {
		this.locationTag = locationTag;
	}

	public String getCustomJobAlerMinfuncAreaTag() {
		return customJobAlerMinfuncAreaTag;
	}

	public void setCustomJobAlerMinfuncAreaTag(
			String customJobAlerMinfuncAreaTag) {
		//Log.e("Functional_Area_Set",customJobAlerMinfuncAreaTag);
		this.customJobAlerMinfuncAreaTag = customJobAlerMinfuncAreaTag;
	}

	public String getCustomJobAlerIndusTypeTag() {
		return customJobAlerIndusTypeTag;
	}

	public void setCustomJobAlerIndusTypeTag(String customJobAlerIndusTypeTag) {
		this.customJobAlerIndusTypeTag = customJobAlerIndusTypeTag;
	}

	public String getCustomJobAlertLocTag() {
		return customJobAlertLocTag;
	}

	public void setCustomJobAlertLocTag(String customJobAlertLocTag) {
		this.customJobAlertLocTag = customJobAlertLocTag;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}



}
