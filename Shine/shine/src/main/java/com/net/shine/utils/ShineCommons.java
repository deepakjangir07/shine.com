package com.net.shine.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.reflect.TypeToken;
import com.moe.pushlibrary.PayloadBuilder;
import com.net.shine.MyApplication;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.ContactRetrieveService;
import com.net.shine.utils.tracker.EasyTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

public class ShineCommons {

	public static final String REGEX = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
	public final static String NAME_REGEX ="^[\\p{L} .'-]+$";

	public static long convertDatetoMilis(String date) throws java.text.ParseException {
		long timeInMilliseconds = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy", Locale.getDefault());
		try {
			if (date != null && !date.equals("")) {
				Date mDate = sdf.parse(date);

				timeInMilliseconds = mDate.getTime();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return timeInMilliseconds;
	}

    public static SimpleSearchModel.Result fromInbox(InboxAlertMailDetailModel.InboxJobDetail results){
        SimpleSearchModel.Result result = new SimpleSearchModel.Result();
        result.jobTitle = results.getTitle();
        result.jRUrl = results.jRUrl;
        result.is_applied = results.is_applied();
        result.comp_name = results.getCompany();
        return result;
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                return;
            }
            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight
                    + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFormattedDate(String date) {
        try {
            if(date==null){
                return "";
            }
            else {
                String[] splited = date.split("-");
                String month = splited[1];
                System.out.println("AB" + month);
                long monthOfYear = 0;
                long dayOfMonth = 0;
                long year = 0;
                if (!month.equals("")) {
                    monthOfYear = Integer.parseInt(month);
                }
                if (!splited[2].equals("")) {
                    dayOfMonth = Integer.parseInt(splited[2]);
                }
                if (!splited[0].equals("")) {
                    year = Integer.parseInt(splited[0]);
                }
                return (dayOfMonth
                        + " "
                        + URLConfig.MONTH_NAME_REVERSE_MAP.get(""
                        + (monthOfYear )) + " " + year);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }


	public static void trackShineEvents(String uiAction,
								  String uiLabel, Context mActivity) {
		try {
			EasyTracker.getInstance().setContext(mActivity);
			if(mActivity instanceof Activity) {
				EasyTracker.getInstance().activityStart((Activity) mActivity);
			}
			EasyTracker.getTracker().sendEvent("ShineAll",uiAction, uiLabel, 0L);


		} catch (Exception e) {

			e.printStackTrace();
		}

		Log.d(">>>>>tracking",uiAction+" "+uiLabel);


        try{
            FirebaseAnalytics mFirebaseAnalytics;
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "ShineAll");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, uiLabel);
            mFirebaseAnalytics.logEvent(uiAction, bundle);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        try{

            MoengageTracking.moengageEventTrack("ShineAll",uiLabel,uiAction,mActivity,new PayloadBuilder());
        }
        catch (Exception e)
        {

        }

	}



    public static void trackShineMultipleAppliesEvents(String uiAction,
                                        String uiLabel,long l, Context mActivity) {
        try {
            EasyTracker.getInstance().setContext(mActivity);
            if(mActivity instanceof Activity) {
                EasyTracker.getInstance().activityStart((Activity) mActivity);
            }
            EasyTracker.getTracker().sendEvent("ShineAll",uiAction, uiLabel, l);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(">>>>>tracking",uiAction+" "+uiLabel+" "+l);


        try{
            FirebaseAnalytics mFirebaseAnalytics;
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "ShineAll");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, uiLabel);
            mFirebaseAnalytics.logEvent(uiAction, bundle);


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try{
            MoengageTracking.moengageEventTrack("ShineAll",uiLabel,uiAction,mActivity,new PayloadBuilder());
        }
        catch (Exception e)
        {

        }

    }


	public static void trackCustomEvents(String uiAction,
									   String uiLabel, Activity mActivity) {
		try {
			EasyTracker.getInstance().setContext(mActivity);
			EasyTracker.getInstance().activityStart(mActivity);
			EasyTracker.getTracker().sendEvent("CustomEvents",uiAction, uiLabel, 0L);

		} catch (Exception e) {
			e.printStackTrace();
		}

        Log.d(">>>>>tracking",uiAction+" "+uiLabel);


        try{
            FirebaseAnalytics mFirebaseAnalytics;
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "CustomEvents");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, uiLabel);
            mFirebaseAnalytics.logEvent(uiAction, bundle);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try{
            MoengageTracking.moengageEventTrack("CustomEvents",uiLabel,uiAction,mActivity,new PayloadBuilder());
        }
        catch (Exception e)
        {

        }
    }




    public static void sendCustomDimension(UserStatusModel model,String screen)
    {


        try {
            if(model!=null) {
                if (model.is_mid_out == 1) {
                    EasyTracker.getInstance().sendCustomDimensions(10, "WebMidout", screen);
                } else if (model.is_resume_mid_out && (!model.has_education || !model.has_skills || !model.has_education)) {
                    EasyTracker.getInstance().sendCustomDimensions(10, "ResumeMidout+ProfileMidout", screen);
                } else if (!model.is_resume_mid_out && (!model.has_education || !model.has_skills || !model.has_education)) {
                    EasyTracker.getInstance().sendCustomDimensions(10, "ProfileMidout", screen);

                } else if (model.is_resume_mid_out && model.has_education && model.has_skills && model.has_education) {
                    EasyTracker.getInstance().sendCustomDimensions(10, "ResumeMidout", screen);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

	public static int getAppVersionCode(Context mContext) {
		try {
			return mContext.getPackageManager().getPackageInfo(
					mContext.getPackageName(), 0).versionCode;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static String getAppVersionName(Context mContext) {
		try {
			return mContext.getPackageManager().getPackageInfo(
					mContext.getPackageName(), 0).versionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void hideKeyboard(Activity mActivity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

			if(mActivity.getCurrentFocus()!=null)
            inputManager.hideSoftInputFromWindow(mActivity.getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


  public static void setSwipeRefeshColor(SwipeRefreshLayout swipeContainer)
  {
      try {
          swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                  android.R.color.holo_green_light,
                  android.R.color.holo_orange_light,
                  android.R.color.holo_red_light);
      } catch (Exception e) {
          e.printStackTrace();
      }
  }





	public static String getCompanyNames(ArrayList<String> comp_list, Set<String> unique_comp)
	{
        String co_names_similar = "";
        if (comp_list != null) {
            for (int k = 0; k < comp_list.size(); k++) {
                String company = comp_list.get(k);
                if (company != null
                        && !company.trim()
                        .equals("")
                        && (!unique_comp
                        .contains(company))) {
                    unique_comp
                            .add(company);
                }
            }
        }
        for(String s:unique_comp)
        {
                co_names_similar = co_names_similar
                        + s + "#";
        }
        return  co_names_similar;
	}


    public static void sendContact() {
        Context context = MyApplication.getInstance().getApplicationContext();
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            Log.d("m_cooker", "No Permission to read Contacts!! :(");
            return;
        }
        Intent intent = new Intent(context, ContactRetrieveService.class);
        intent.putExtra("isUserAction", false);
        context.startService(intent);
    }

    public static boolean appInstalledOrNot(String uri) {
        boolean app_installed;
        PackageManager pm = MyApplication.getInstance().getApplicationContext().getPackageManager();

        try {

            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            ApplicationInfo ai =
                    MyApplication.getInstance().getApplicationContext().getPackageManager().getApplicationInfo(uri,0);
            boolean appEnabled = ai.enabled;


            if (appEnabled==true){
                app_installed = true;}

            else {
                app_installed= false;
            }
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static int setTagAndValue(final HashMap<String, String> map,
                               final String[] list, int index, TextView eTiew) {
        int dropDowbIndex = 0;
        try {
            int len = list.length;
            for (int i = 0; i < len; i++) {
                String name = map.get("" + index);
                if (name != null && name.equals(list[i])) {
                    dropDowbIndex = i;
                    break;
                }
            }
            String value = map.get("" + index);
            if (value == null) {
                value = "";
            }
            eTiew.setText(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dropDowbIndex;
    }

    public static Integer[] setTagAndValueQualification(final HashMap<Integer, String> parentMap,
                                                        final HashMap<Integer,String> childMap,
                                                        Integer childindex,Integer parentIndex ,TextView eTiew,String other_edu) {

        Integer[] dropDowbIndex = new Integer[2];


        Log.d("ParentMap ","Pmap "+parentMap.toString());

        Log.d("childMap ","Cmap "+childMap.toString());

        Log.d("pKey",parentIndex+"");
        Log.d("Ckey",childindex+"");
        try{

            if(parentIndex==0 && childindex==0){

                parentIndex=-1;
                childindex=-1;
                dropDowbIndex[0]=parentIndex;
                dropDowbIndex[1]=childindex;
                eTiew.setText("");

            }
            else {
                dropDowbIndex[0] = childindex;
                dropDowbIndex[1] = parentIndex;

                String childString = "";
                String parentString = "";

                Log.d("pKey", parentIndex + "");
                Log.d("Ckey", childindex + "");

                childString = childMap.get(parentIndex);
                parentString = parentMap.get(childindex);

                if (!TextUtils.isEmpty(parentString) && !TextUtils.isEmpty(childString)) {

                    if(TextUtils.isEmpty(other_edu))
                    eTiew.setText(parentString + " (" + childString + ")");
                    else
                        eTiew.setText(parentString +  " (" + other_edu + ")");


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dropDowbIndex;
    }



    public static boolean isAndroidM()
    {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
    }


    public static void hitImpressionApi(Context mActivity,String url)
    {
        try {

            Log.d("IMPRESSION_API","impression api hit >>>>> ");

            Type type = new TypeToken<EmptyModel>(){}.getType();


            VolleyNetworkRequest networkRequest=new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    Log.d("IMPRESSION_API","impression api success ");
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("IMPRESSION_API","impression api error "+error);
                }
            },url,type);

            networkRequest.execute("IMPRESSION_API");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    public static void hitJDpixelApi(Context mActivity,String url)
    {
        try {

            Log.d("PIXEL_API","PIXEL api hit >>>>> ");

            Type type = new TypeToken<EmptyModel>(){}.getType();


            VolleyNetworkRequest networkRequest=new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    Log.d("PIXEL_API","PIXEL api success ");
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("PIXEL_API","PIXEL api error "+error);
                }
            },url,type);

            networkRequest.execute("PIXEL_API");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

