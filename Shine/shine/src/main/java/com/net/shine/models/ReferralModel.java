package com.net.shine.models;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by deepak on 3/8/15.
 */
public class ReferralModel implements Serializable {

    public String count="",next="",previous="";

    public ArrayList<RefrelResults> results = new ArrayList<>();

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public ArrayList<RefrelResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<RefrelResults> results) {
        this.results = results;
    }

    public static class RefrelResults implements Serializable
    {
        public  String job_id="",candidate_id="",candidate_name="",first_accepted_referral="",accepted_referrals_count="",job_title="",company_name="";


        public boolean job_expired;

        public ArrayList<Refrel> referrals = new ArrayList<>();

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        public String getCandidate_id() {
            return candidate_id;
        }

        public void setCandidate_id(String candidate_id) {
            this.candidate_id = candidate_id;
        }

        public String getCandidate_name() {
            return candidate_name;
        }

        public void setCandidate_name(String candidate_name) {
            this.candidate_name = candidate_name;
        }

        public String getFirst_accepted_referral() {
            return first_accepted_referral;
        }

        public void setFirst_accepted_referral(String first_accepted_referral) {
            this.first_accepted_referral = first_accepted_referral;
        }

        public String getAccepted_referrals_count() {
            return accepted_referrals_count;
        }

        public void setAccepted_referrals_count(String accepted_referrals_count) {
            this.accepted_referrals_count = accepted_referrals_count;
        }

        public String getJob_title() {
            return job_title;
        }

        public void setJob_title(String job_title) {
            this.job_title = job_title;
        }

        public String getCompany_name() {
            return company_name;
        }

        public void setCompany_name(String company_name) {
            this.company_name = company_name;
        }

        public ArrayList<Refrel> getReferrals() {
            return referrals;
        }

        public void setReferrals(ArrayList<Refrel> referrals) {
            this.referrals = referrals;
        }

    }
    public static class Refrel implements Serializable
    {
        public String referral_id="",referrer_sid="",referrer_uid="",referrer_name="",response_date="",request_date="",referrer_image_url="",organization="",referrer_corp_email="",expired="";
        public int mode;
        public int status;

        public String getReferral_id() {
            return referral_id;
        }

        public void setReferral_id(String referral_id) {
            this.referral_id = referral_id;
        }

        public String getReferrer_sid() {
            return referrer_sid;
        }

        public void setReferrer_sid(String referrer_sid) {
            this.referrer_sid = referrer_sid;
        }

        public String getReferrer_uid() {
            return referrer_uid;
        }

        public void setReferrer_uid(String referrer_uid) {
            this.referrer_uid = referrer_uid;
        }

        public String getReferrer_name() {
            return referrer_name;
        }

        public void setReferrer_name(String referrer_name) {
            this.referrer_name = referrer_name;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getResponse_date() {
            return response_date;
        }

        public void setResponse_date(String response_date) {
            this.response_date = response_date;
        }

        public String getRequest_date() {
            return request_date;
        }

        public void setRequest_date(String request_date) {
            this.request_date = request_date;
        }

        public String getReferrer_image_url() {
            return referrer_image_url;
        }

        public void setReferrer_image_url(String referrer_image_url) {
            this.referrer_image_url = referrer_image_url;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getReferrer_corp_email() {
            return referrer_corp_email;
        }

        public void setReferrer_corp_email(String referrer_corp_email) {
            this.referrer_corp_email = referrer_corp_email;
        }

        public String getExpired() {
            return expired;
        }

        public void setExpired(String expired) {
            this.expired = expired;
        }

        public int getMode() {
            return mode;
        }

        public void setMode(int mode) {
            this.mode = mode;
        }
    }


}

