package com.net.shine.fragments.components;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.EmploymentDetailModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by gobletsky on 8/7/15.
 */
public class EmploymentDetailComp implements View.OnClickListener, VolleyRequestCompleteListener {


    private Activity mActivity;
    private View view;
    private LinearLayout jobDetailView;
    private List<EmploymentDetailModel> jobList;
    private Dialog alertDialog;
    private String deleteJobId = null;
    private boolean hasCurrentJob = false;
    private View personal;



    public EmploymentDetailComp(Activity mActivity, View view, View personalView) {
        this.mActivity = mActivity;
        this.view = view;
        personal = personalView;
        jobDetailView = (LinearLayout) view.findViewById(R.id.job_detail_view);
    }



    public void showJobsDetails(List<EmploymentDetailModel> jobList) {
        try {
            this.jobList = jobList;

            TextView add_btn = (TextView) view
                    .findViewById(R.id.emp_add_btn);
            add_btn.setOnClickListener(this);

            jobDetailView.removeAllViews();
            LayoutInflater inflater = mActivity.getLayoutInflater();

            hasCurrentJob = false;


            int len = jobList.size();
            for (int i = 0; i < len; i++) {
                View job_sub_comp_view = inflater.inflate(R.layout.job_profile_detail_sub_comp, null);
                EmploymentDetailModel pModel = jobList.get(i);
                String jobTitle = pModel.getJobTitle();
                TextView jobTitle_tv = (TextView) job_sub_comp_view.findViewById(R.id.jobTitle);
                jobTitle_tv.setText(jobTitle);
                ImageView img_edit = (ImageView) job_sub_comp_view.findViewById(R.id.edit_job);
                img_edit.setVisibility(View.VISIBLE);
//                img_edit.setId(id);
                img_edit.setTag(i);
                img_edit.setOnClickListener(this);
                if(jobList.size()>1)
                {
                    ImageView img_delete = (ImageView) job_sub_comp_view
                            .findViewById(R.id.delete_job);
                    img_delete.setVisibility(View.VISIBLE);
//                    img_delete.setId(id2);
                    img_delete.setTag(i);
                    img_delete.setOnClickListener(this);
                }


                if (pModel.isCurrent()) {
                    hasCurrentJob = true;
//                    ShineSharedPreferences.updateJobTitle(mActivity,jobTitle);
                }

                TextView comp_name_tv = (TextView) job_sub_comp_view.findViewById(R.id.company_name_tv);
                comp_name_tv.setText(pModel.getComName());


                TextView duration_tv = (TextView) job_sub_comp_view.findViewById(R.id.duration_tv);
                String till;
                if (pModel.isCurrent()) {
                        till = "Present";
                } else {
                        till = URLConfig.MONTH_NAME_REVERSE_MAP
                                .get(pModel.getEndMonth()) + " "
                                + pModel.getEndYear();
                }

                String duration = "From "
                        + URLConfig.MONTH_NAME_REVERSE_MAP
                        .get(pModel.getStartMonth()) + " "

                        + pModel.getStartYear() + " to " + till;

                if(duration.contains("null"))
                {
                    duration_tv.setVisibility(View.GONE);
                }
                else
                {
                    duration_tv.setText(duration);
                }




                TextView industry_tv = (TextView) job_sub_comp_view.findViewById(R.id.ind_tv);
//                if(URLConfig.INDUSTRY_REVERSE_MAP.get(pModel.getIndustryIndex())!=null)
                     industry_tv.setText(URLConfig.INDUSTRY_REVERSE_MAP.get(pModel.getIndustryIndex() + ""));
//                else
//                    industry_tv.setText("Not Mentioned");


                TextView func_area = (TextView) job_sub_comp_view.findViewById(R.id.func_area_tv);
                        func_area.setText(URLConfig.FUNCTIONA_AREA_REVERSE_MAP.get(pModel.getFunctionalIndex() + ""));

                jobDetailView.addView(job_sub_comp_view);

                if(i!=(len-1)) {

                    View oneLineView = inflater.inflate(
                            R.layout.one_line_view, null);
                    jobDetailView.addView(oneLineView);
                }

            }

           if(jobList.size() >0)
            {
                    ShineSharedPreferences.updateJobTitle(mActivity,  jobList.get(0).getJobTitle());
            }
            UserStatusModel userProfileVO =ShineSharedPreferences.getUserInfo(mActivity);
            TextView tv_personal_job_title = (TextView) personal
                    .findViewById(R.id.job_title);
            tv_personal_job_title.setText(userProfileVO.job_title);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.delete_job:
                final EmploymentDetailModel model = jobList.get((Integer) view.getTag());
				final Dialog deleteDialog = DialogUtils.showAlertWithTwoButtons(mActivity);
				TextView t = (TextView) deleteDialog
						.findViewById(R.id.dialog_msg);
				t.setText("Are you sure you want to delete this job ?");
				deleteDialog.findViewById(R.id.yes_btn).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogUtils.dialogDismiss(deleteDialog);
                                deleteJob(model.getJobId());
                            }
                        });
                deleteDialog.findViewById(R.id.cancel_exit_btn)
						.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogUtils.dialogDismiss(deleteDialog);
                            }
                        });


				break;


                case R.id.edit_job:

				EmploymentDetailModel model2 = jobList.get((Integer) view.getTag());
				Bundle bundle = new Bundle();
                Gson gson = new Gson();
                String json = gson.toJson(model2);
				bundle.putString(URLConfig.KEY_MODEL_NAME, json);
//                bundle.putString(URLConfig.KEY_ACTION, URLConfig.UPDATE_OPERATION);
				BaseFragment frg = new EmploymentDetailsEditFragment(hasCurrentJob);
				frg.setArguments(bundle);
				((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);




				break;

            case R.id.emp_add_btn:
				BaseFragment frg2 = new EmploymentDetailsEditFragment(hasCurrentJob);
				((BaseActivity) mActivity).replaceFragmentWithBackStack(frg2);

				break;

            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;

        }

    }



    private void deleteJob(String jobID)
    {
        deleteJobId = jobID;

        Type listType = new TypeToken<EmploymentDetailModel>() {}.getType();

        VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                URLConfig.MY_TOTAL_JOBS_URL.replace(URLConfig.CANDIDATE_ID,ShineSharedPreferences.getCandidateId(mActivity))+jobID+"/", listType);
        downloader.setUseDeleteMethod();
        downloader.execute("deleteJob");
        alertDialog = DialogUtils.showCustomProgressDialog(mActivity, mActivity.getString(R.string.plz_wait));

    }



    @Override
    public void OnDataResponse(Object object, String tag) {

        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);

                    if(deleteJobId!=null)
                    {
                        ShineCommons.trackShineEvents("Delete", "Employment", mActivity);

                    for(int i=0;i<jobList.size();i++){

                            if(jobList.get(i).getJobId().equals(deleteJobId))
                            {
                                jobList.remove(i);
                                deleteJobId = null;
                                break;
                            }
                        }
                        DataCaching.getInstance().cacheData(DataCaching.JOB_LIST_SECTION,
                                "" + DataCaching.CACHE_TIME_LIMIT, jobList, mActivity);
                        showJobsDetails(jobList);
                    }



                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(final String error, String tag) {

        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            EmploymentDetailComp.this );
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
