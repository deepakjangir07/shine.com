package com.net.shine.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.RelevantSuggestionsFilterableArrayAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.views.LocationAutoComplete;
import com.net.shine.views.RelevantAutoComplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


public class SearchFormFrg extends BaseFragment implements
        OnClickListener, OnFocusChangeListener {
    RelevantAutoComplete keywordText;
    LocationAutoComplete locationText;
    private TextView minExpField, minSalaryField, funcAreaField, industryField, clearAll;
    private LinearLayout top_container;
    private SimpleSearchVO svo;
    private String location = "";
    private String keyword = "";
    private String experience = "";
    private JSONObject newJson;
    private UserStatusModel userProfileVO;
    private boolean modify = false;
    private boolean fromMore = false;
    private ImageView back_button;


    public static SearchFormFrg newInstance(Bundle args) {
        SearchFormFrg fragment = new SearchFormFrg();
        fragment.setArguments(args);
        return fragment;
    }


    public SearchFormFrg() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    TextInputLayout inputLayoutKeyword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.simple_search_view, container, false);
        mActivity.setActionBarVisible(false);

        try {

            Bundle bundle = getArguments();
            if (bundle != null) {
                fromMore = bundle.getBoolean("fromMore");
                location = bundle.getString("LOCATION");
                keyword = bundle.getString("KEYWORD");
                experience = bundle.getString("EXPERIENCE");

            }


            if (bundle != null) {
                svo = (SimpleSearchVO) bundle
                        .getSerializable(URLConfig.SEARCH_VO_MODIFY);
                modify = bundle.getBoolean("Modify Search");
            }

            //RESTORE TAGS
            int minExpTag = svo.getMinExpTag();
            int minSalaryTag = svo.getMinSalTag();
            int funcAreaTag = svo.getFuncAreaTag();
            int industryTag = svo.getIndusTypeTag();


            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

            top_container = (LinearLayout) view.findViewById(R.id.container);
            top_container.requestFocus();
            clearAll = (TextView) view.findViewById(R.id.clearall);
            clearAll.setOnClickListener(this);

            keywordText = (RelevantAutoComplete) view.findViewById(R.id.keyword);
            back_button = (ImageView) view.findViewById(R.id.back_button);
            back_button.setVisibility(View.VISIBLE);
            back_button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.popone();

                }
            });

            keywordText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.KEYWORD_SUGGESTION));
            keywordText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("item_clicked", keywordText.getText() + "");
                    keywordText.setSelection(keywordText.getText().length());
                }
            });
            keywordText.setText(keyword);
            inputLayoutKeyword = (TextInputLayout) view.findViewById(R.id.input_layout_keyword);
            TextChangeListener.addListener(keywordText, inputLayoutKeyword, mActivity);

            locationText = (LocationAutoComplete) view.findViewById(R.id.location);
            locationText.setAdapter(new RelevantSuggestionsFilterableArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    URLConfig.CITY_LIST));
            locationText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("item_clicked", locationText.getText() + "");
                    locationText.setSelection(locationText.getText().length());
                }
            });
            locationText.setText(location);

            minExpField = (TextView) view.findViewById(R.id.min_exp_field);
            minExpField.setTag(minExpTag);
            minExpField.setText(experience);
            minExpField.setOnClickListener(this);
            minExpField.setOnFocusChangeListener(this);


            minSalaryField = (TextView) view.findViewById(R.id.min_sal_field);
            minSalaryField.setTag(minSalaryTag);
            minSalaryField.setOnClickListener(this);
            minSalaryField.setOnFocusChangeListener(this);


            funcAreaField = (TextView) view.findViewById(R.id.func_area_field);
            funcAreaField.setTag(funcAreaTag);
            funcAreaField.setOnClickListener(this);
            funcAreaField.setOnFocusChangeListener(this);


            industryField = (TextView) view
                    .findViewById(R.id.industry_type_field);
            industryField.setTag(industryTag);
            industryField.setOnClickListener(this);
            industryField.setOnFocusChangeListener(this);

            View searchBtn = view.findViewById(R.id.search_btn);
            searchBtn.setOnClickListener(this);

            if (modify) {
                keywordText.setText(svo.getKeyword());
                locationText.setText(svo.getLocation());

                minExpField.setText(URLConfig.EXPERIENCE_REVERSE_MAP
                        .get(String.valueOf(svo.getMinExpTag())));

                if (svo.getMinSalTag() == -1)
                    minSalaryField.setText("");
                else {
                    minSalaryField
                            .setText(URLConfig.ANNUAL_SALARY_REVRSE_MAP.get(String.valueOf(svo.getMinSalTag())));
                }

                funcAreaField.setText(svo.getFuncArea());
                industryField.setText(svo.getIndusType());
            }

            locationText
                    .setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId,
                                                      KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                submitSearch();
                                return true;
                            }
                            return false;
                        }
                    });

            final LinearLayout expandSearch = (LinearLayout) view.findViewById(R.id.adv_search_fields);


            final TextView moreBtn = (TextView) view.findViewById(R.id.more);
            if (fromMore) {
                expandSearch.setVisibility(View.VISIBLE);
                moreBtn.setVisibility(View.GONE);
            }


            moreBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    expandSearch.setVisibility(View.VISIBLE);
                    moreBtn.setVisibility(View.GONE);
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        try {

            switch (v.getId()) {

                case R.id.search_btn:
                    ShineCommons.hideKeyboard(mActivity);
                    if (userProfileVO == null) {

                        newJson = new JSONObject();
                        try {
                            newJson.put("Label", "Search");
                            newJson.put("Category", "Non Logged");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {

                        newJson = new JSONObject();
                        try {
                            newJson.put("Label", "Search");
                            newJson.put("Category", "Logged In");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    submitSearch();

                    break;

                case R.id.clearall:
                    keywordText.setText("");
                    locationText.setText("");
                    minExpField.setTag(0);
                    minExpField.setText("");
                    minSalaryField.setTag(0);
                    minSalaryField.setText("");
                    funcAreaField.setTag(0);
                    funcAreaField.setText("");
                    industryField.setTag(0);
                    industryField.setText("");
                    break;


                default:
                    showSpinner(v.getId());
                    ShineCommons.hideKeyboard(mActivity);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }
    }

    private void showSpinner(int id) {
        try {
            switch (id) {
                case R.id.min_exp_field:

                    String[] expList = new String[URLConfig.EXPERIENCE_LIST2.length + 1];
                    expList[0] = "None";
                    for (int i = 0; i < URLConfig.EXPERIENCE_LIST2.length; i++) {
                        expList[i + 1] = URLConfig.EXPERIENCE_LIST2[i];
                    }

                    List<String> mainExpList=Arrays.asList(expList);

                    if(!TextUtils.isEmpty(minExpField.getText()))
                        minExpField.setTag(mainExpList.indexOf(minExpField.getText().toString()));
                    else
                        minExpField.setTag(-1);

                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_exp), minExpField,
                            expList, mActivity);

                    break;
                case R.id.min_sal_field:

                    String[] salList = new String[URLConfig.ANNUAL_SALARY_LIST.length + 1];
                    salList[0] = "None";
                    for (int i = 0; i < URLConfig.ANNUAL_SALARY_LIST.length; i++) {
                        salList[i + 1] = URLConfig.ANNUAL_SALARY_LIST[i];
                    }


                    List<String> s = Arrays.asList(salList);

                    if(minSalaryField.getText()!=null)
                    minSalaryField.setTag(s.indexOf(minSalaryField.getText().toString()));
                    else
                        minSalaryField.setTag(0);


                    DialogUtils.showNewSingleSpinnerRadioDialog(getString(R.string.hint_salary),
                            minSalaryField, salList, mActivity);
                    break;
                case R.id.func_area_field:




                    List<String> fa=Arrays.asList(URLConfig.FUNCTIONAL_AREA_LIST3);

                    if(funcAreaField.getText()!=null)
                    {
                        funcAreaField.setTag(fa.indexOf(funcAreaField.getText().toString()));
                    }
                    else
                        funcAreaField.setTag(-1);

                    Log.d("FA","tag on click "+funcAreaField.getTag());


                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_func_area), funcAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST3, mActivity);

                    break;
                case R.id.industry_type_field:

                    String[] indList = new String[URLConfig.INDUSTRY_LIST3.length + 1];
                    indList[0] = "None";
                    for (int i = 0; i < URLConfig.INDUSTRY_LIST3.length; i++) {
                        indList[i + 1] = URLConfig.INDUSTRY_LIST3[i];
                    }
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_industry), industryField,
                            indList, mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void submitSearch() {
        try {
            String keyword = keywordText.getText().toString().trim();
            String location = locationText.getText().toString().trim();
            String minExp = minExpField.getText().toString().trim();
            String minSal = minSalaryField.getText().toString().trim();
            String funcArea = funcAreaField.getText().toString().trim();
            String indusType = industryField.getText().toString().trim();
            if (keyword.length() == 0) {
                inputLayoutKeyword.setError(getString(R.string.sim_search_keyword_empty_msg));
                return;
            }

            if (location.trim().length() == 0) {
                location = "";
            }

            String minExpTag = "";
            if (minExp.length() == 0) {
                minExpTag = "0";
            } else {
                minExpTag = URLConfig.EXPERIENCE_MAP.get(minExp);
            }

            String minSalTag = "";
            if (minSal.length() == 0) {
                minSal = "None";
                minSalTag = "-1";
            } else {
                minSalTag = URLConfig.ANNUAL_SALARY_MAP.get(minSal);
            }

            if (funcArea.length() == 0) {
                funcArea = "";
            }

            if (indusType.length() == 0) {
                indusType = "";
            }

            SimpleSearchVO sVO = new SimpleSearchVO();
            if (userProfileVO != null) {
                String URL = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);
                sVO.setUrl(URL);

            } else {
                sVO.setUrl(URLConfig.SEARCH_RESULT_URL_LOGOUT);

            }
            sVO.setKeyword(keyword);
            sVO.setLocation(location);
            sVO.setMinExp(minExp);
            sVO.setMinExpTag(Integer.parseInt(minExpTag));

            sVO.setFuncArea(funcArea);
            sVO.setFuncAreaTag((Integer) funcAreaField.getTag());

            sVO.setIndusType(indusType);
            sVO.setIndusTypeTag((Integer) industryField.getTag());

            sVO.setMinSal(minSal);
            if (minSal.equals(URLConfig.NO_SELECTION)) {
                sVO.setMinSalTag(-1);
            } else {
                sVO.setMinSalTag(Integer.parseInt(minSalTag));

            }
            mActivity.popone();

            BaseFragment fragment = new SearchResultFrg();
            Bundle bundle = new Bundle();
            bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
            fragment.setArguments(bundle);
            mActivity.singleInstanceReplaceFragment(fragment,true);
            ShineCommons.trackShineEvents("Search", "Modify", mActivity);

            recentSearch(sVO);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void recentSearch(final SimpleSearchVO sVO) {
        try {
            Runnable runnable = new Runnable() {

                @Override
                public void run() {

                    MyApplication.getDataBase().addRecentSearch(sVO);
                }
            };
            new Thread(runnable).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getString(R.string.search));
        top_container.requestFocus();
    }


}