package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.SaveJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.EndlessRecyclerOnScrollListener;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by manishpoddar on 30/06/17.
 */

public class SavedJobsFrag extends BaseFragment implements VolleyRequestCompleteListener, GetConnectionListener, SwipeRefreshLayout.OnRefreshListener,
        AdapterView.OnItemClickListener{

    private RecyclerView gridView;
    private SaveJobsAdapter adapter;
    private boolean loadMore = false;
    private View LoadingCmp;
    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
    private View view;
    private Set<String> unique_companies = new HashSet<>();
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private String co_names = "";
    private SwipeRefreshLayout swipeContainer;
    private View errorLayout;
    private String nextUrl="";

    private  boolean fromRefresh=false;

    public static SavedJobsFrag newInstance(Bundle args) {

        SavedJobsFrag fragment = new SavedJobsFrag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.matched_jobs_view, container, false);
        try {
            swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            swipeContainer.setOnRefreshListener(this);
            errorLayout = view.findViewById(R.id.error_layout);
            errorLayout.setVisibility(View.GONE);
            gridView = (RecyclerView) view.findViewById(R.id.grid_view);
            gridView.setLayoutManager(new LinearLayoutManager(mActivity));
            adapter = new SaveJobsAdapter(mActivity, jobList);
            gridView.setAdapter(adapter);

            EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener() {

                @Override
                public void onLoadMore(int currentPage) {

                    if(!TextUtils.isEmpty(nextUrl)&&!loadMore) {
                        loadMore=true;
                        downloadsearchresultJobs(nextUrl,view.findViewById(R.id.loading_grid),true,false);
                    }
                }
            };

            gridView.addOnScrollListener(mScrollListener);

            if (jobList.size() == 0) {
                String url = URLConfig.SAVED_JOBS_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                        +"?perpage=" + URLConfig.PER_PAGE +"&fl=id,jCID,jRUrl,is_applied,jCName,jCType,jKwds,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";

                downloadsearchresultJobs(url,view.findViewById(R.id.loading_cmp),false,false);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        try {

            inflater.inflate(R.menu.multi_apply_menu, menu);
            final MenuItem alertMenuItem = menu.findItem(R.id.notification);
            alertMenuItem.setActionView(R.layout.notification_badge_layout);
            RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
            final TextView notification_count = (TextView) rootView.findViewById(R.id.txtCount);
            if (!TextUtils.isEmpty(ShineSharedPreferences.getNotificationCount(mActivity,
                    ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"))) {
                notification_count.setVisibility(View.VISIBLE);
                notification_count.setText(ShineSharedPreferences.getNotificationCount(mActivity,
                        ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count"));
            } else {
                notification_count.setVisibility(View.GONE);
            }
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notification_count.setVisibility(View.GONE);
                    BaseFragment fragment = new NotificationsFrag();
                    mActivity.singleInstanceReplaceFragment(fragment);
                    ShineCommons.trackShineEvents("Notification","SavedJobs",mActivity);

                    ShineSharedPreferences.saveNotificationCount(mActivity, ShineSharedPreferences.getCandidateId(mActivity) + "_noti_count", "");

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){
            case R.id.action_multi_apply:
                adapter.startActionMode(view.findViewById(R.id.multi_apply_button));
                break;
        }
        return false;
    }
    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();

    }

    private void downloadsearchresultJobs(String url, View loadingView,
                                          boolean flag,boolean refresh) {

        try {
            LoadingCmp = loadingView;
            loadMore = flag;
            fromRefresh=refresh;

            if(!refresh) {
                DialogUtils.showLoading(mActivity, getString(R.string.loading), LoadingCmp);
            }

            if (loadingView.getId() == R.id.loading_cmp&&!fromRefresh)
                gridView.setVisibility(View.GONE);

            Type type = new TypeToken<SimpleSearchModel>() {
            }.getType();

            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    url, type);
            req.execute("savedJobsFrag");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showresult() {
        try {


            LoadingCmp.setVisibility(View.GONE);
            if (jobList.size() > 0) {
                view.findViewById(R.id.grid_view).setVisibility(View.VISIBLE);
                adapter.rebuildListObject(jobList);
                adapter.notifyDataSetChanged();
                invalidateWithFriendsData();
                MoengageTracking.setSaveJobExist(mActivity,true);
            }
            else
            {
                MoengageTracking.setSaveJobExist(mActivity,false);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callGetFriend() {
        GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
        friends.getFriendsInCompanyVolleyRequest(co_names);
    }

    private void invalidateWithFriendsData() {
        for (int m = 0; m < jobList.size(); m++) {
            SimpleSearchModel.Result job = jobList.get(m);
            if (job == null)
                continue;
            if (job.comp_uid_list != null) {
                for (int n = 0; n < job.comp_uid_list.size(); n++) {
                    if (friend_list.containsKey(job.comp_uid_list.get(n))) {
                        job.frnd_model = friend_list.get(job.comp_uid_list.get(n));
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
        gridView.invalidate();
    }

    @Override
    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
        if (!isAdded())
            return;
        friend_list = result;
        invalidateWithFriendsData();
    }



    @Override
    public void OnDataResponse(Object object, String tag) {


        try {
            errorLayout.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            if (jobList == null||fromRefresh)
                jobList = new ArrayList<>();
            swipeContainer.setRefreshing(false);

            SimpleSearchModel model = (SimpleSearchModel) object;

            nextUrl = model.next;

            for (SimpleSearchModel.Result res : model.results) {
                int len = res.job_loc_list.size();
                String loc = "";
                for (int j = 0; j < len; j++) {
                    if (j >= len - 1) {
                        loc += res.job_loc_list.get(j);
                    } else {
                        loc += res.job_loc_list.get(j) + " / ";
                    }
                }
                res.job_loc_str = loc;
            }


            co_names = "";

            int prevSize=jobList.size();

            jobList.addAll(model.results);

            for (int i = 0; i < model.results.size(); i++) {
                co_names = ShineCommons.getCompanyNames(model.results.get(i).comp_uid_list, unique_companies);
            }

            // only call if we have got new companies
            if (!co_names.equals(""))
                callGetFriend();


            loadMore = false;
            showresult();

            if (nextUrl == null) {

                if (jobList.size() ==0)
                {
                    gridView.setVisibility(View.GONE);
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,"You have not saved any job",
                            "Go to job detail to save job","",DialogUtils.ERR_NO_SAVED_JOBS,null);

                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            swipeContainer.setRefreshing(false);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LoadingCmp.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                    gridView.setVisibility(View.GONE);
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.technical_error),
                            mActivity.getString(R.string.technical_error2),
                            "",
                            DialogUtils.ERR_TECHNICAL,null);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onRefresh() {
        errorLayout.setVisibility(View.GONE);

        String url = URLConfig.SAVED_JOBS_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                +"?perpage=" + URLConfig.PER_PAGE +"&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
        MyApplication.getInstance().getRequestQueue().getCache().clear();
        downloadsearchresultJobs(url,view.findViewById(R.id.loading_cmp),false,true);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {

            //TODO: Don't create this list every time
            ArrayList<SimpleSearchModel.Result> jList = new ArrayList<>();
            Bundle bundle = new Bundle();
            bundle.putInt(URLConfig.INDEX, position);
            bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jList);
            BaseFragment fragment = new JobDetailParentFrg();
            fragment.setArguments(bundle);
            mActivity.singleInstanceReplaceFragment(fragment);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
