package com.net.shine.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.RequestReferralAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.models.ReferralModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class RequestReferralsFrg extends BaseFragment implements
        VolleyRequestCompleteListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {

    boolean loadMore = false;
    private View LoadingCmp, parentView;
    private ListView gridView;
    private int noofhits = 1, totalJobs;
    private RequestReferralAdapter adapter;
    private TextView count_tv;
    private View errorLayout;
    private ArrayList<ReferralModel.RefrelResults> jobList = new ArrayList<>();
    private UserStatusModel userProfileVO;
    private int index;
    private boolean isDownloading;
    private SwipeRefreshLayout swipeContainer;

    public static RequestReferralsFrg newInstance() {
        Bundle args = new Bundle();
        RequestReferralsFrg fragment = new RequestReferralsFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        Log.d("Debug ", "Request OnCreate");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.request_referral, container, false);
        swipeContainer = (SwipeRefreshLayout) parentView.findViewById(R.id.swipeContainer);
        ShineCommons.setSwipeRefeshColor(swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        gridView = (ListView) parentView.findViewById(R.id.list_view);
        count_tv = (TextView) parentView.findViewById(R.id.count_textview);
        gridView.setOnScrollListener(this);
        LoadingCmp = parentView.findViewById(R.id.loading_cmp);
        errorLayout = parentView.findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        jobList = new ArrayList<>();

        adapter = new RequestReferralAdapter(mActivity, jobList);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                if (jobList.get(position).getJob_id() != null && !jobList.get(position).getJob_id().equals("null")) {
                    goToJDPage(position);
                }


            }
        });

        downloadRequestRefarrals(noofhits, LoadingCmp, false, false);
        return parentView;
    }

    private void downloadRequestRefarrals(int pageNumber, View loadingView,
                                          boolean flag, boolean isRefresh) {
        try {
            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
            count_tv.setVisibility(View.GONE);
            errorLayout.setVisibility(View.GONE);
            LoadingCmp = loadingView;
            noofhits = pageNumber;
            loadMore = flag;
            isDownloading = true;
            if (!isRefresh)
                DialogUtils.showLoading(mActivity, getString(R.string.loading), LoadingCmp);

            int noOfRecords = URLConfig.PER_PAGE_JOB_ALERT_MAIL;
            int numberOfResult;
            numberOfResult = noOfRecords;

            Log.d("Debug", "In Download result " + noofhits);


            if (userProfileVO != null) {


                Type listType = new TypeToken<ReferralModel>() {
                }.getType();

                String URL = URLConfig.GET_REFERRAL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);


                String secondPartUrl = "referral_type=requested&page=" + noofhits + "&perpage=" + numberOfResult;
                Log.d("Debug ", "URL " + URL + secondPartUrl);


                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, RequestReferralsFrg.this,
                        URL + secondPartUrl, listType);


                downloader.execute("RequestReferrals");


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        try {
            if (totalItemCount != 0) {
                int lastScreen = firstVisibleItem + visibleItemCount;

                if ((lastScreen == totalItemCount && !(loadMore))) {
                    loadMoreData();
                } else if (LoadingCmp.getTag() != null) {
                    if (LoadingCmp.getTag().equals("error")) {
                        parentView.findViewById(R.id.loading_grid)
                                .setVisibility(View.VISIBLE);
                        loadMore = false;
                    }
                }
            } else {
                if (parentView.findViewById(R.id.loading_grid) != null) {
                    parentView.findViewById(R.id.loading_grid)
                            .setVisibility(View.GONE);
                }
            }

            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {
            ReferralModel map = (ReferralModel) object;

            if (jobList == null || !loadMore)
                jobList = new ArrayList<>();
            LoadingCmp.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);


            jobList.addAll(map.getResults());

            if (!map.getCount().equals(""))
                totalJobs = Integer.parseInt(map.getCount());
            else
                totalJobs = 0;


            if (totalJobs > 1) {
                count_tv.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs + "</font></b> referral requests have been sent by you"));

            } else {
                count_tv.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs + "</font></b> referral request has been sent by you"));

            }
            count_tv.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.VISIBLE);


            showresult();


            loadMore = false;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


    }

    private void showresult() {
        try {

            if (jobList.size() > 0) {
                gridView.setVisibility(View.VISIBLE);
                LoadingCmp.setVisibility(View.GONE);
                errorLayout.setVisibility(View.GONE);
                adapter.rebuildListObject(jobList);
                adapter.notifyDataSetChanged();

            } else {

                gridView.setVisibility(View.GONE);
                count_tv.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);

                String text_before_matched_jobs = "Apply to ";
                String text_matched_jobs = "jobs where your connections work";
                String text_after_matched_jobs = " & increase your chances of getting hired through referral.";
                String text_final = text_before_matched_jobs+text_matched_jobs+text_after_matched_jobs;

                gridView.setVisibility(View.GONE);
                BaseFragment fragment = new JobsTabFrg();
                Bundle bundle = new Bundle();
                bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.NETWORK_TAB);
                fragment.setArguments(bundle);
                DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                        mActivity.getString(R.string.no_request_referral),
                        text_final,mActivity.getString(R.string.jobs_with_connections),DialogUtils.ERR_NO_REFERRAL_REQUESTED,fragment);




            }
            isDownloading = false;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void OnDataResponseError(final String error, String tag) {

        try {
            swipeContainer.setRefreshing(false);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        count_tv.setVisibility(View.GONE);
                        gridView.setVisibility(View.GONE);
                        LoadingCmp.setVisibility(View.GONE);
                        errorLayout.setVisibility(View.VISIBLE);

                        DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                                mActivity.getString(R.string.technical_error),
                                mActivity.getString(R.string.technical_error2),
                                "",
                                DialogUtils.ERR_TECHNICAL,null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            if (!isDownloading) {
                if (adapter.getCount() == 0) {
                    gridView.setVisibility(View.GONE);
                    index = gridView.getFirstVisiblePosition();
                    gridView.post(new Runnable() {
                        @Override
                        public void run() {
                            gridView.setSelection(index);

                        }
                    });
                    jobList.clear();
                    adapter.notifyDataSetChanged();
                    downloadRequestRefarrals(1,
                            parentView.findViewById(R.id.loading_cmp), false, false);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMoreData() {
        try {

            int noofhits = (jobList.size() / URLConfig.PER_PAGE_JOB_ALERT_MAIL) + 1;
            int totalPage = totalJobs / URLConfig.PER_PAGE_JOB_ALERT_MAIL;

            int modtotalnoPages = totalPage % URLConfig.PER_PAGE_JOB_ALERT_MAIL;

            if (modtotalnoPages != 0) {
                totalPage = totalPage + 1;
            }

            if (noofhits <= totalPage && totalJobs > jobList.size()) {
                View view = parentView.findViewById(R.id.loading_grid);

                downloadRequestRefarrals(noofhits, view, true, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToJDPage(int position) {


            if(jobList.get(position).job_expired)
            {
                Toast.makeText(mActivity,"This job has been expired",Toast.LENGTH_SHORT).show();
            }
            else {

                ArrayList<SimpleSearchModel.Result> jobLists = new ArrayList<>();
                SimpleSearchModel.Result searchResultVO = new SimpleSearchModel.Result();
                searchResultVO.jobId = (jobList.get(position).getJob_id());
                jobLists.add(searchResultVO);
                Bundle bundle = new Bundle();
                bundle.putInt(URLConfig.INDEX, position);
                bundle.putString(URLConfig.JOB_TYPE, "search_job");
                bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jobLists);
                BaseFragment fragment = new JobDetailParentFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);
            }
    }

    @Override
    public void onRefresh() {

        errorLayout.setVisibility(View.GONE);

        downloadRequestRefarrals(1, LoadingCmp, false, true);
    }
}
