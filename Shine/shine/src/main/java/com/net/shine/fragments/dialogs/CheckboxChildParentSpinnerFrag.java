package com.net.shine.fragments.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.CheckboxExpandableListAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class CheckboxChildParentSpinnerFrag extends DialogFragment implements View.OnClickListener, ExpandableListView.OnChildClickListener {


    CheckboxExpandableListAdapter listAdapter;
    ExpandableListView lv;
    String title;
    TextView tView;
    View view;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, Integer> indexMap;
    List<String> completeList;
    HashSet<Integer> expandedGroupPos = new HashSet<>();
    HashSet<String> selectedChildSet = new HashSet<>();

    public CheckboxChildParentSpinnerFrag(){

    }

    public CheckboxChildParentSpinnerFrag(String title, final TextView tView,
                                          final String[] list, boolean limit) {
        this.title = title;
        this.tView = tView;
        shoudlLimit = limit;
        completeList = new ArrayList<>();
        listDataChild = new HashMap<>();
        listDataHeader = new ArrayList<>();
        indexMap = new HashMap<>();
        String currParent = "";
        Integer i =0;
        for (String str : list) {
            completeList.add(str);
            indexMap.put(str, i);
            i++;
            if (str.startsWith("#")) {
                currParent = str.substring(1);
                listDataHeader.add(currParent);
                listDataChild.put(currParent, new ArrayList<String>());
            } else {
                listDataChild.get(currParent).add(str);
            }
        }
    }


    BaseActivity mActivity;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivity = ((BaseActivity) activity);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ShineBaseThemeNoOverlay);
    }

    // @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.child_parent_spinner_frag, container, false);

        view.findViewById(R.id.update_btn)
                .setOnClickListener(this);
        view.findViewById(R.id.cancel_btn).setOnClickListener(this);



        lv = (ExpandableListView) view.findViewById(R.id.cus_lv_spinner);

        listAdapter = new CheckboxExpandableListAdapter(mActivity, listDataHeader, listDataChild);

        lv.setAdapter(listAdapter);

        int[] curr = (int[]) tView.getTag();
        int groupToScroll = -1;
        for(int i=0;i<curr.length;i++)
        {
            String selectedChildText = completeList.get(curr[i]);
            selectedChildSet.add(selectedChildText);
            int info[] = listAdapter.getChildInfoByName(selectedChildText);
            if(groupToScroll==-1 || groupToScroll > info[0])
                groupToScroll = info[0];
            expandedGroupPos.add(info[0]);
            listAdapter.setSelectedChild(selectedChildText);
        }
        TextView tv_title = (TextView) view.findViewById(R.id.header_title);
        tv_title.setText(title);

        final EditText et = (EditText) view.findViewById(R.id.et_spinner_search);


        if(completeList!=null&&completeList.size()<13)
        {
            et.setVisibility(View.GONE);
        }
        else {
            et.setVisibility(View.VISIBLE);
        }

        lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (lv.isGroupExpanded(groupPosition)) {
                    lv.collapseGroup(groupPosition);
                    if (et.getText().toString().isEmpty())
                        expandedGroupPos.remove(groupPosition);
                } else {
                    if (et.getText().toString().isEmpty())
                        expandedGroupPos.add(groupPosition);
                    lv.expandGroup(groupPosition);
                    return true;
                }
                return false;
            }

        });


        lv.setOnChildClickListener(this);

        for(Integer pos: expandedGroupPos)
            lv.expandGroup(pos);
        if(groupToScroll>0)
        {
            lv.setSelectedGroup(groupToScroll);
        }


        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    return true;
                }
                return false;
            }
        });


        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String prefix = s.toString().toLowerCase();
                HashMap<String, List<String>> newListDataChild = new HashMap<>();
                List<String> newListDataHeader = new ArrayList<>();

                for(String parent: listDataHeader)
                {
                    ArrayList<String> newChildList = new ArrayList<>();
                    List<String> childList = listDataChild.get(parent);
                    for(String child: childList)
                    {
                        if(child.toLowerCase().contains(prefix)) {
                            newChildList.add(child);
                        }
                    }
                    if(newChildList.size()>0)
                    {
                        newListDataHeader.add(parent);
                        newListDataChild.put(parent, newChildList);
                    }
                }


                listAdapter.refreshLists(newListDataHeader, newListDataChild);

                if(prefix.isEmpty())
                {
                    //restore users expand selection
                    for(int i=0;i<newListDataChild.size();i++)
                    {
                        if(expandedGroupPos.contains(i))
                            lv.expandGroup(i);
                        else
                            lv.collapseGroup(i);
                    }
                }
                else
                {
                    //expand all
                    for(int i=0;i<newListDataChild.size();i++)
                    {
                        lv.expandGroup(i);
                    }
                }



            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    @Override
    public void onClick(View v) {
//        super.onClick(v);

        switch (v.getId()) {
            case R.id.update_btn:
                try {
                    mActivity.getSupportFragmentManager().popBackStackImmediate();

                    int[] selected_pos = new int[selectedChildSet.size()];
                    int i=0;
                    for(String s:selectedChildSet) {
                        selected_pos[i] = indexMap.get(s);
                        i++;
                    }
                    String text;
                    if (selected_pos.length==1 && URLConfig.NO_SELECTION
                            .equals(completeList.get(selected_pos[0]))) {
                        text = "";
                    } else {
                        text = selectedChildSet.toString();
                    }
                    text = text.replace("]", "").replace("[", "");
                    text = text.replace(",", ", ");
                    ShineCommons.hideKeyboard(mActivity);
//                    TextView newTv = (TextView) mActivity.findViewById(tView.getId());
                    tView.setText(text);
                    tView.setTag(selected_pos);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                break;
            case R.id.cancel_btn:
                mActivity.popone();
                break;
        }


    }


    private boolean shoudlLimit = false;
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        String selected_child = ((TextView)v).getText().toString();

        if(selectedChildSet.contains(selected_child))
        {
            listAdapter.setSelectedChild(selected_child);
            selectedChildSet.remove(selected_child);
        }
        else
        {

            if(selectedChildSet.size()>=10 && shoudlLimit)
            {
                Toast.makeText(mActivity, "Number of items should be less than 10.", Toast.LENGTH_SHORT).show();
            }
            else
            {
                listAdapter.setSelectedChild(selected_child);
                selectedChildSet.add(selected_child);
            }

        }
        return false;
    }
}
