package com.net.shine;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.moe.pushlibrary.MoEHelper;
import com.net.shine.config.URLConfig;
import com.net.shine.utils.TypefaceUtil;
import com.net.shine.utils.db.LookupInit;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.db.ShineSqliteDataBase;
import com.net.shine.utils.tracker.EasyTracker;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends MultiDexApplication {

	private static ShineSqliteDataBase shineDataBase;
	private static MyApplication mInstance;
	private RequestQueue mRequestQueue;
	private static final String TAG = MyApplication.class.getSimpleName();

	public static boolean isPhonebookSyncing = false;
	public static boolean isLinkedInSyncing = false;
	public static boolean isGmailSyncing = false;

	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());

		TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Roboto-Light.ttf");


		shineDataBase = new ShineSqliteDataBase(this);
		MoEHelper.getInstance(this).autoIntegrate(this);
//		MoEHelper.getInstance(getApplicationContext()).setLogLevel(Logger.VERBOSE);


		mInstance = this;

		VolleyLog.DEBUG = true;

		try {
			if (ShineSharedPreferences.getServerLookupVersion(this)== LookupInit.CURRENT_FILE_LOOKUP_VERSION) {
				Log.d("DEBUG::LOOKUP", "MyApplication calling LookupInitFromFile");
				LookupInit.LookupInitFromFile(this);
			} else {
				try {
					if(shineDataBase.isDBdataValid(this)) {
						Log.d("DEBUG::LOOKUP", "MyApplication calling LookupInitFromDB");
						LookupInit.LookupInitFromDB(this);
					}
					else {
						LookupInit.LookupInitFromFile(this);
					}
				}
				catch (Exception e) {
					Log.d("DEBUG::LOOKUP", "MyApplication Exception occured, calling LookupInitFromFile");
					e.printStackTrace();
					LookupInit.LookupInitFromFile(this);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		try {
			URLConfig.initializeSuggestion(getApplicationContext());
			URLConfig.initializeInstituteSuggestion(getApplicationContext());
			URLConfig.initializeSkillSuggestion(getApplicationContext());
		}catch (Exception e){
			e.printStackTrace();
		}

        try {
        }
        catch (Exception e) {
            e.printStackTrace();
        }

		try {
			EasyTracker.getInstance().setContext(this);
		}catch (Exception e){
			e.printStackTrace();
		}

		//TODO: For check user is a new user or an existing user to MoEngage

		try {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(this);
			if (!prefs.getBoolean("MoEngageFirstTime", false)) {
				MoEHelper.getInstance(this).setExistingUser(false);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean("MoEngageFirstTime", true);
				editor.apply();

			}
			else {
				MoEHelper.getInstance(this).setExistingUser(true);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	private ImageLoader mImageLoader;

	public ImageLoader getImageLoader() {
		if(mImageLoader==null) {
            ImageLoader.ImageCache mCache =  new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap>
                        cache = new LruCache<>(20);
                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            };
            mImageLoader = new ImageLoader(getRequestQueue(),mCache);
        }
		return mImageLoader;
	}

	public static ShineSqliteDataBase getDataBase() {
		return shineDataBase;
	}

	public static synchronized MyApplication getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext(),
					new HurlStack());
		}
		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

		getRequestQueue().add(req);
	}


    public HashMap<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json; charset=utf-8");

        String client =  ShineSharedPreferences.getClientAccessToken(getApplicationContext());
        if(client==null || client.equals("")) {
            client = "shine";
        }

        headers.put(URLConfig.CLIENT_ACCESS_TOKEN, client);
        headers.put(URLConfig.USER_ACCESS_TOKEN, ShineSharedPreferences.getUserAccessToken(getApplicationContext()));
        headers.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; google_sdk Build/MR1) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

        Log.d("DEBUG::HEADERS", headers.toString());
        return headers;

    }

    public Map<String, String> getMultiHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");
//        headers.put("Content-Type", "application/json; charset=utf-8");

        String client =  ShineSharedPreferences.getClientAccessToken(getApplicationContext());
        if(client==null || client.equals("")) {
            client = "shine";
        }

        headers.put(URLConfig.CLIENT_ACCESS_TOKEN, client);
        headers.put(URLConfig.USER_ACCESS_TOKEN, ShineSharedPreferences.getUserAccessToken(getApplicationContext()));
        headers.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; google_sdk Build/MR1) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

        Log.d("DEBUG::HEADERS", headers.toString());
        return headers;

    }

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public static String getResumeDir(){
		return getInstance().getApplicationContext().getFilesDir()+ File.separator+"resumes"+File.separator;
	}

	private boolean isUpdateFlowShown = false;
	public boolean getIsUpdateFlowShown(){
		return isUpdateFlowShown || !ShineSharedPreferences.isUpdateFlowEnabled(getApplicationContext());
	}
	public void setIsUpdateFlowShown(boolean flag){
		isUpdateFlowShown = flag;
	}





}
