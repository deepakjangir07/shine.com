package com.net.shine.fragments;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.adapters.DiscoverFriendRecyclerViewAdapter;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchVO;

public class ShowDialogDiscoverFriendsFragment extends BaseFragment {


    DiscoverModel.Company companyModel;
    View inflatedView;
    RecyclerView list;


    public static ShowDialogDiscoverFriendsFragment newInstance(DiscoverModel.Company companyModel) {

        Bundle args = new Bundle();
        args.putSerializable("companyModel", companyModel);
        ShowDialogDiscoverFriendsFragment fragment = new ShowDialogDiscoverFriendsFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.companyModel = (DiscoverModel.Company) getArguments().getSerializable("companyModel");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.frnd_list_popup, container, false);

        showDialogDiscover(companyModel);


        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setActionBarVisible(false);
    }

    public void showDialogDiscover(final DiscoverModel.Company companyModel) {


        list = (RecyclerView) inflatedView.findViewById(R.id.frnd_list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        TextView title = (TextView) inflatedView
                .findViewById(R.id.title_connect);
        TextView job_count = (TextView) inflatedView
                .findViewById(R.id.job_count);

        title.setText(Html.fromHtml("<b>" + companyModel.data.friends.size()
                + "</b> connections work  in <b>" + companyModel.company
                + "</b>"));
        if (companyModel.data.job_count > 0) {
            if (companyModel.data.job_count == 1) {

                job_count.setText(Html
                        .fromHtml("<b>"
                                + companyModel.data.job_count + "</b>" + " Job"));

            } else {
                job_count.setText(Html
                        .fromHtml("<b>"
                                + companyModel.data.job_count + "</b>" + " Jobs"));
            }
        } else {
            job_count.setVisibility(View.GONE);
        }
        Display display = (mActivity).getWindowManager()
                .getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        job_count.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
               // showCompanyJobs(companyModel);


            }
        });


        inflatedView.findViewById(R.id.title_back_caret).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            mActivity.popone();
                            mActivity.getSupportActionBar().show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        inflatedView.setBackgroundDrawable((mActivity).getResources()
                .getDrawable(R.drawable.popup_like_facebook_connection));
        DiscoverFriendRecyclerViewAdapter adapter = new DiscoverFriendRecyclerViewAdapter(
                mActivity, companyModel, 2);
        adapter.setConnected_through(DiscoverFriendRecyclerViewAdapter.DISCOVER_CONNECTION);

        list.setAdapter(adapter);

    }

    public void showCompanyJobs(DiscoverModel.Company row) {

        Log.d("DEEPAK ", "search Frg called ");
        SearchResultFrg fragment = new SearchResultFrg();

        Bundle bundle = new Bundle();

        SimpleSearchVO svo = new SimpleSearchVO();

        bundle.putSerializable("companyModel", row);

        bundle.putSerializable("companyIdObject", svo);

        bundle.putBoolean("fromDiscoverConnect", true);

        fragment.setArguments(bundle);

        mActivity.replaceFragmentWithBackStack(fragment);

    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            mActivity.getSupportActionBar().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
