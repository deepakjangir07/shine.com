package com.net.shine.adapters;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.fragments.ShowDialogDiscoverFriendsFragment;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;


public class DiscoverCompanyListAdapter extends BaseAdapter {


    private BaseActivity mActivity;
    private ArrayList<DiscoverModel.Company> mList;

    public DiscoverCompanyListAdapter(BaseActivity activity, ArrayList<DiscoverModel.Company> list) {
        this.mActivity = activity;
        this.mList = list;

    }


    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int postition) {
        return mList.get(postition);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        View view = convertView;
        final CompanyViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = li.inflate(R.layout.discover_listview_row, viewGroup, false);
            viewHolder = new CompanyViewHolder();

            viewHolder.company_name = (TextView) view
                    .findViewById(R.id.company_name);
            viewHolder.number_of_connections = (TextView) view
                    .findViewById(R.id.no_of_connections);
            viewHolder.connections = (TextView) view
                    .findViewById(R.id.name_of_connections);
            viewHolder.noOfJobs = (TextView) view
                    .findViewById(R.id.no_of_jobs);
            viewHolder.companyImage = (ImageView) view
                    .findViewById(R.id.imageView1);
            view.setTag(viewHolder);
        } else {
            viewHolder = (CompanyViewHolder) view.getTag();
        }

        final DiscoverModel.Company row = mList.get(position);


        if(URLUtil.isValidUrl(row.data.company_image_url)){
            Glide.with(mActivity).load(row.data.company_image_url).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                    ColorGenerator generator = ColorGenerator.MATERIAL;
//                    int color = generator.getRandomColor();
//                    TextDrawable drawable = TextDrawable.builder()
//                            .buildRect(row.company.substring(0, 1).toUpperCase(), color);
//                    viewHolder.companyImage.setImageDrawable(drawable);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    return false;
                }
            }).into(viewHolder.companyImage);
        }
        else {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(row.company.substring(0, 1).toUpperCase(), color);
            viewHolder.companyImage.setImageDrawable(drawable);
        }

        viewHolder.company_name.setText(row.company);
        viewHolder.connections.setVisibility(View.GONE);
        int friend_count = row.data.friends.size();
        if (friend_count == 2) {
            viewHolder.number_of_connections
                    .setText(Html.fromHtml("<font color='#333333'>"
                            + row.data.friends.get(0).name
                            + "</font>" + "<b> + " + (friend_count - 1)
                            + "</b>" + " connection "
                            + "<font color='#333333'>work here</font>"));
        } else if (friend_count > 2) {
            viewHolder.number_of_connections
                    .setText(Html.fromHtml("<font color='#333333'>"
                            + row.data.friends.get(0).name
                            + "</font>" + "<b> + " + (friend_count - 1)
                            + "</b>" + " connections "
                            + "<font color='#333333'>work here</font>"));
        } else {

            viewHolder.number_of_connections
                    .setText(Html
                            .fromHtml("<font color='#2c84cc'>"
                                    + row.data.friends.get(0).name
                                    + "</font>"
                                    + "<font color='#333333'> works here</font>"));
        }

        if (row.data.job_count == 0) {

            viewHolder.noOfJobs.setVisibility(View.GONE);


        } else if (row.data.job_count == 1) {

            viewHolder.noOfJobs.setVisibility(View.VISIBLE);
            viewHolder.noOfJobs.setText(Html.fromHtml("<b>" + row.data.job_count
                    + "</b>" + " Job"));

        } else {
            viewHolder.noOfJobs.setVisibility(View.VISIBLE);
            viewHolder.noOfJobs.setText(Html.fromHtml("<b>" + row.data.job_count
                    + "</b>" + " Jobs"));

        }

        view.findViewById(R.id.no_of_jobs).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showCompanyJobs(row);


                    }
                });

        view.findViewById(R.id.connectionsContainer)
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        // showDialogDiscover(row);
                        ShowDialogDiscoverFriendsFragment showDialogDiscoverFriendsFragment = ShowDialogDiscoverFriendsFragment.newInstance(row);
                        mActivity.setFragment(showDialogDiscoverFriendsFragment);
                        ShineCommons.trackShineEvents("KonnectFriendList", "Discover", mActivity);


                    }
                });


        return view;
    }


    public void rebuildListObject(ArrayList itemList) {

        this.mList = itemList;
    }


    public void showCompanyJobs(DiscoverModel.Company row) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("companyModel", row);
        bundle.putSerializable("companyIdObject", new SimpleSearchVO());
        bundle.putBoolean("fromDiscoverConnect", true);
        BaseFragment fragment = new SearchResultFrg();
        fragment.setArguments(bundle);
        mActivity.singleInstanceReplaceFragment(fragment);
    }

    class CompanyViewHolder {

        TextView company_name;
        TextView number_of_connections;
        TextView connections;
        TextView noOfJobs;
        ImageView companyImage;
    }

}



