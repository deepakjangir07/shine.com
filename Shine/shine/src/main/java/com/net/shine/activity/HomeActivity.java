package com.net.shine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.AppInviteFriendsListFrg;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.CustomJobAlertEditFragment;
import com.net.shine.fragments.DiscoverFragment;
import com.net.shine.fragments.JobAlertDetailParentFragment;
import com.net.shine.fragments.JobDetailFrg;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.fragments.KonnectFrag;
import com.net.shine.fragments.LoginHomeFrg;
import com.net.shine.fragments.LogoutHomeFrg;
import com.net.shine.fragments.MyCustomJobAlertFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.fragments.MyProfileViewFrg;
import com.net.shine.fragments.RateTheApp;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.fragments.components.EducationDetailsEditFragment;
import com.net.shine.fragments.components.EmploymentDetailsEditFragment;
import com.net.shine.fragments.tabs.InboxTabFrg;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.fragments.tabs.ReferralTabFrg;
import com.net.shine.fragments.tabs.TwoTabFrg;
import com.net.shine.models.EducationDetailModel;
import com.net.shine.models.EmploymentDetailModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;

/**
 * Created by ujjawal-work on 11/08/16.
 */

public class HomeActivity extends BaseActivity {

    private final int LOGIN_HANDLER_REQUEST_CODE = 100;
    BaseFragment fragment;
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState==null){
            if(ShineSharedPreferences.getUserInfo(getApplicationContext())==null){
                setBaseFrg(LogoutHomeFrg.newInstance());
            } else{
                setBaseFrg(LoginHomeFrg.newInstance());
            }
                Bundle args = getIntent().getExtras();
                Log.d("create_alert","inside3"+getIntent().getExtras());

                if(args!=null) {
                    int login_request_type = args.getInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);


                    Log.d("NON_SKIP_TEST ","login_request_type "+login_request_type);
                    Intent intent;
                    switch (login_request_type) {
                        case ShineAuthUtils.REQUESTED_TO_KONNECT:
                            if(ShineSharedPreferences.getLinkedinImported(this)
                                    &&ShineSharedPreferences.getGoogleImported(this)){
                                fragment = new DiscoverFragment();
                            }
                            else {
                                fragment = new KonnectFrag();
                            }
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_PROFILE:
                            fragment = new MyProfileFrg();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_REFERRAL_RECEIVED:
                            fragment = new ReferralTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(TwoTabFrg.SELECTED_TAB,ReferralTabFrg.RECEIVED_REF_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_MESSAGES:
                            fragment = new InboxTabFrg();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_FEEDBACK:
                            fragment = new RateTheApp();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_RESUME:
                            intent = new Intent(getApplicationContext(), ResumeUploadActivity.class);
                            startActivity(intent);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_MATCHED_JOB:
                            fragment = new JobsTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.MATCHED_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_RECOMMEDED_JOBS:
                            fragment = new JobsTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.RECOMMENDED_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_SAVED_JOBS:
                            fragment = new JobsTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.SAVED_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_RECRUITER_MAIL:
                            fragment = new JobAlertDetailParentFragment();
                            bundle = new Bundle();
                            bundle = getIntent().getExtras();
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_INBOX_TAB:
                            fragment = new InboxTabFrg();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_ALERT_MAIL:
                            fragment = new JobAlertDetailParentFragment();
                            bundle = new Bundle();
                            bundle = getIntent().getExtras();
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_JOB_ALERT:
                            Log.d("create-alert","inside3");
                            if(ShineSharedPreferences.getUserInfo(this)!=null){
                                fragment = new MyCustomJobAlertFragment();
                                singleInstanceReplaceFragment(fragment,true);
                            }
                            else {
                                fragment = new CustomJobAlertEditFragment();
                                singleInstanceReplaceFragment(fragment,true);
                            }

                            break;

                        case ShineAuthUtils.REQUESTED_FOR_WHO_VIEWED:
                            fragment = new MyProfileViewFrg();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTES_FOR_JOB_DETAIL:
                            fragment = new JobDetailParentFrg();
                            fragment.setArguments(getIntent().getExtras());
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_APP_INVITE:
                            fragment = new AppInviteFriendsListFrg();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_SEARCH:
                            fragment = new SearchResultFrg();
                            Log.d("job_search_link","inside3");

                            bundle = new Bundle();
                            bundle = getIntent().getExtras();
                            fragment.setArguments(bundle);
                            replaceFragmentWithBackStack(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_REFERRAL_REQUESTED:
                            fragment = new ReferralTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(TwoTabFrg.SELECTED_TAB,ReferralTabFrg.REQUESTED_REF_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_CONNECTION_JOBS:
                            fragment = new JobsTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.NETWORK_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_APPLIED_JOBS:
                            fragment = new JobsTabFrg();
                            bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.APPLIED_TAB);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_EDUCATION_ADD:

                            EducationDetailModel model = new EducationDetailModel();
                            Bundle bundle = new Bundle();
                            Gson gson = new Gson();
                            String json = gson.toJson(model);
                            bundle.putString(URLConfig.KEY_MODEL_NAME, json);
                            fragment = new EducationDetailsEditFragment();
                            singleInstanceReplaceFragment(fragment,true);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_EMPLOYMENT_ADD:
                            EmploymentDetailModel model2 = new EmploymentDetailModel();
                            Bundle bundle2 = new Bundle();
                            Gson gson2 = new Gson();
                            String json2 = gson2.toJson(model2);
                            bundle2.putString(URLConfig.KEY_MODEL_NAME, json2);
                            fragment = new EmploymentDetailsEditFragment();
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_EXPERIENCE:
                            fragment = new MyProfileFrg();
                            bundle = new Bundle();
                            bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_EXPERIENCE);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_CERTIFICATION:
                            fragment = new MyProfileFrg();
                            bundle = new Bundle();
                            bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_CERTIFICATION);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_DESIRED_JOB_DETAIL:
                            fragment = new MyProfileFrg();
                            bundle = new Bundle();
                            bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_DESIRED);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_SKILL:
                            fragment = new MyProfileFrg();
                            bundle = new Bundle();
                            bundle.putInt(MyProfileFrg.SCROLL_TO,MyProfileFrg.SCROLL_TO_SKILL);
                            fragment.setArguments(bundle);
                            singleInstanceReplaceFragment(fragment);
                            break;

                        case ShineAuthUtils.REQUESTED_FOR_NON_SKIP_REGISTERATION:

                            finish();
                            finish();

                            break;

                        default:
                            if(LoginEventHandler.isHandlingNeeded()){


                                Log.d("NON_SKIP_TEST ","login_request_type default");
                                intent = new Intent(getApplicationContext(), LoginEventHandler.class);
                                intent.putExtras(getIntent().getExtras());
                                startActivityForResult(intent, LOGIN_HANDLER_REQUEST_CODE);
                            }
                            else {
                                Log.d("NON_SKIP_TEST ","login_request_type default with JOB APPLY");

                                handleLoginRequestType();
                            }
                            break;

                    }

                }
            }
        }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(gDriveCompat!=null){
            gDriveCompat.onActivityResult(requestCode, resultCode, data);
        }

        if(callerCompat!=null){
            callerCompat.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode){
            case LOGIN_HANDLER_REQUEST_CODE:
                handleLoginRequestType();
                break;
        }
    }


    private void handleLoginRequestType(){

        Log.d(">>>>>","HandleLoginRequestType");
        Bundle args = getIntent().getExtras();



        if(args!=null) {


            Log.d(">>>>>","HandleLoginRequestType inSide args "+args);
            int login_request_type = args.getInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL);


            Log.d(">>>>>","HandleLoginRequestType inSide args "+login_request_type);

            switch (login_request_type) {
                case ShineAuthUtils.REQUESTED_TO_APPLY_JOBS:

                    BaseFragment fragment = new JobDetailParentFrg();
                    Bundle bundle = new Bundle();
                    bundle.putBundle(ApplyJobsNew.APPLY_BUNDLE, args.getBundle(ApplyJobsNew.APPLY_BUNDLE));
                    bundle.putInt(JobDetailFrg.FROM_SCREEN, JobDetailFrg.FROM_LOGIN_TO_APPLY);

                    fragment.setArguments(bundle);


                    singleInstanceReplaceFragment(fragment);

                    break;

                case ShineAuthUtils.REQUESTED_NORMAL:

                    fragment = new JobsTabFrg();
                    bundle = new Bundle();
                    bundle.putInt(MainTabFrg.SELECTED_TAB,JobsTabFrg.MATCHED_TAB);
                    fragment.setArguments(bundle);
                    singleInstanceReplaceFragment(fragment);

                    break;



            }
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
    }


    public HomeActivity.GoogleResolutionOnResult gDriveCompat;


    public interface GoogleResolutionOnResult {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }



    public HomeActivity.TrueCallerCompat callerCompat;


    public interface TrueCallerCompat {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

}
