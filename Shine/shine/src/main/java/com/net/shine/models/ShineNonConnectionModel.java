package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ggnf2853 on 16/03/16.
 */
public class ShineNonConnectionModel implements Serializable {

   public int total_count=0,page_no=0;
   public String next="",previous="";

    public ArrayList<DiscoverModel.Company.CompanyData.Friends> results = new ArrayList<>();


    @Override
    public String toString() {
        return "ShineNonConnectionModel{" +
                "total_count=" + total_count +
                ", page_no=" + page_no +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", results=" + results +
                '}';
    }
}
