package com.net.shine.models;

import com.google.gson.annotations.SerializedName;
import com.net.shine.config.URLConfig;

import java.io.Serializable;

public class EducationDetailModel implements Serializable, Comparable<EducationDetailModel> {
	/**
	 * 
	 */
	@SerializedName("education_level")
	public int educationNameIndex  = - 1;

	@SerializedName("education_specialization")
	public int educationStreamIndex = -1;

	@SerializedName("institute_name")
	public String institueName = "";

	@SerializedName("course_type")
	public int courseTypeIndex = - 1;

	@SerializedName("year_of_passout")
	public int passoutYearIndex  = -1;

	@SerializedName("id")
	public String educationId = "";

	@SerializedName("education_specialization_custom")

	public String other_specialization="";



	@Override
	public int compareTo(EducationDetailModel educationDetailModel) {

		Integer e1Index = 0;
		for (int i = 0; i < URLConfig.EDUCATIONAL_QUALIFICATION_LIST2.length; i++) {
			String name = URLConfig.EDUCATIONAL_QUALIFICATION_REVERSE_MAP.get("" + this.educationNameIndex);
			if (name != null && name.equals(URLConfig.EDUCATIONAL_QUALIFICATION_LIST2[i])) {
				e1Index = i;
				break;
			}
		}


		Integer e2Index = 0;
		for (int i = 0; i < URLConfig.EDUCATIONAL_QUALIFICATION_LIST2.length; i++) {
			String name = URLConfig.EDUCATIONAL_QUALIFICATION_MAP.get("" + educationDetailModel.educationNameIndex);
			if (name != null && name.equals(URLConfig.EDUCATIONAL_QUALIFICATION_LIST2[i])) {
				e2Index = i;
				break;
			}
		}
		return e2Index.compareTo(e1Index);
	}

	@Override
	public String toString() {
		return "EducationDetailModel{" +
				"educationNameIndex=" + educationNameIndex +
				", educationStreamIndex=" + educationStreamIndex +
				", intstitueName='" + institueName + '\'' +
				", courseTypeIndex=" + courseTypeIndex +
				", passoutYearIndex=" + passoutYearIndex +
				", educationId='" + educationId + '\'' +
				'}';
	}
}
