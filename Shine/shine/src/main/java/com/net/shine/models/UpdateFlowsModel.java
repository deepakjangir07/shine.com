package com.net.shine.models;

import com.google.gson.JsonElement;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gaurav on 18/9/15.
 */
public class UpdateFlowsModel implements Serializable {

//    {"next_widget":1,"prefill_details":{"cell_phone":"9988998899","country_code":"91"}}

//    {"next_widget":3,"prefill_details":{"skill_suggestions":[
// {"value":"Test Automation","years_of_experience":null,"name":"Test Automation","experience":null,"id":10814},
// {"value":"VBScript","years_of_experience":null,"name":"VBScript","experience":null,"id":10816},
// {"value":"QTP (Training Knowledge)","years_of_experience":null,"name":"QTP (Training Knowledge)","experience":null,"id":10817},
// {"value":"Autoit","years_of_experience":null,"name":"Autoit","experience":null,"id":10818},
// {"value":"Audit Command Language","years_of_experience":null,"name":"Audit Command Language","experience":null,"id":10819}],
// "experience_required":true}}

//    {"next_widget":4,"prefill_details":{"experience_in_years":9,"experience_in_months":1}}

//    {"next_widget":5,"prefill_details":{"salary_in_lakh":12,"salary_in_thousand":0}}

    public int next_widget;
    public JsonElement prefill_details;


    public static class MobileVerificationWidget implements Serializable{
        public String cell_phone;
        public String country_code;
    }

    public static class LinkedinSyncWidget implements Serializable{

    }

    public static class SkillsWidget implements Serializable{


        public ArrayList<Skill_suggestions> skill_suggestions = new ArrayList<>();
        public boolean experience_required;

        public static class Skill_suggestions implements Serializable{
            public String value;
            public int years_of_experience;
        }



    }

    public static class TotalExperienceWidget implements Serializable{
        public int experience_in_years;
        public int experience_in_months;
    }

    public static class CurrentSalaryWidget implements Serializable{
            public int salary_in_lakh;
            public int salary_in_thousand;
    }





}
