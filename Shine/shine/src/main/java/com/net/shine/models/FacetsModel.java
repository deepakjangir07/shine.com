package com.net.shine.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class FacetsModel implements Serializable, Cloneable {

	@Override
	public FacetsModel clone() throws CloneNotSupportedException {
		return (FacetsModel) super.clone();
	}

	private List<FacetsSubModel> jIndustry = new ArrayList<>();
	private List<FacetsSubModel> jLocation = new ArrayList<>();
	private List<FacetsSubModel> jFunctionalArea = new ArrayList<>();
	private List<FacetsSubModel> jSalary = new ArrayList<>();
	private List<FacetsSubModel> jExperience = new ArrayList<>();

	public List<FacetsSubModel> getjJobType() {
		return jJobType;
	}

	public void setjJobType(List<FacetsSubModel> jJobType) {
		this.jJobType = jJobType;
	}

	public List<FacetsSubModel> getjTopCompanies() {
		return jTopCompanies;
	}

	public void setjTopCompanies(List<FacetsSubModel> jTopCompanies) {
		this.jTopCompanies = jTopCompanies;
	}

	private List<FacetsSubModel> jJobType = new ArrayList<>();
	private List<FacetsSubModel> jTopCompanies= new ArrayList<>();


	public List<FacetsSubModel> getjIndustry() {
		return jIndustry;
	}
	public void setjIndustry(List<FacetsSubModel> jIndustry) {
		this.jIndustry = jIndustry;
	}
	public List<FacetsSubModel> getjLocation() {
		return jLocation;
	}
	public void setjLocation(List<FacetsSubModel> jLocation) {
		this.jLocation = jLocation;
	}
	public List<FacetsSubModel> getjFunctionalArea() {
		return jFunctionalArea;
	}
	public void setjFunctionalArea(List<FacetsSubModel> jFunctionalArea) {
		this.jFunctionalArea = jFunctionalArea;
	}
	public List<FacetsSubModel> getjSalary() {
		return jSalary;
	}
	public void setjSalary(List<FacetsSubModel> jSalary) {
		this.jSalary = jSalary;
	}
	public List<FacetsSubModel> getjExperience() {
		return jExperience;
	}
	public void setjExperience(List<FacetsSubModel> jExperience) {
		this.jExperience = jExperience;
	}
	
}
