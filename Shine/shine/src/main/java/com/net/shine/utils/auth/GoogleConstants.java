package com.net.shine.utils.auth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.net.shine.activity.BaseActivity;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.utils.DialogUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ujjawal-work on 25/05/16.
 */

public class GoogleConstants {


    public static String client_id="284765467291-lo164ec1u4pcnhtrbicird7tsdt553sm.apps.googleusercontent.com";

    private static String client_secret="pluMwKGSOCVxn5E-RjK7PTKG";


    public static ActivityResultHandler GoogleConnect(final BaseActivity mActivity, final BaseFragment fragment, final GoogleCallBack mCallback){


//        Dialog alertDialog = ErrorsScreen.showCustomProgressDialog(mActivity, "Please Wait...");
//        ShineCommons.hideDialog(alertDialog);

        final int GOOGLE_REQUEST = new Random().nextInt(1000);

        if(mActivity.mGoogleApiClient==null) {

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestScopes(Drive.SCOPE_FILE)
                    .requestScopes(Drive.SCOPE_APPFOLDER)
                    .requestScopes(new Scope("https://www.google.com/m8/feeds/"))
                    .requestServerAuthCode(client_id)
                    .build();


            mActivity.mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .enableAutoManage(mActivity /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            Log.d("DEBUG::GAPI", connectionResult + " ");
                        }
                    })
                    .setAccountName(null)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        if(mActivity.mGoogleApiClient.isConnected()) {
            mActivity.mGoogleApiClient.clearDefaultAccountAndReconnect().setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mActivity.mGoogleApiClient);
                    fragment.startActivityForResult(signInIntent, GOOGLE_REQUEST);
                }
            });
        }
        else {
            mActivity.mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(@Nullable Bundle bundle) {
                    mActivity.mGoogleApiClient.unregisterConnectionCallbacks(this);
                    mActivity.mGoogleApiClient.clearDefaultAccountAndReconnect().setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mActivity.mGoogleApiClient);
                            fragment.startActivityForResult(signInIntent, GOOGLE_REQUEST);
                        }
                    });
                }

                @Override
                public void onConnectionSuspended(int i) {

                }
            });
           mActivity.mGoogleApiClient.connect();
        }


        return new ActivityResultHandler() {
            @Override
            public void handleOnActivityResult(final BaseActivity mActivity, final int requestCode, int resultCode, Intent data) {

                Log.d("DEBUG::UJJ", "Inside Handler " + " " + requestCode + " " + GOOGLE_REQUEST);

                if (requestCode == GOOGLE_REQUEST) {

                    if (resultCode == Activity.RESULT_OK) {
                        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                        if (result.isSuccess()) {
                            GoogleSignInAccount acct = result.getSignInAccount();
                            if (acct == null) {
                                Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                                Log.d("DEUBG::LOGINERROR", "Unable to Login: " + result.getStatus());
                                return;
                            }

                            final String serverAuthCode = acct.getServerAuthCode();

                            new AsyncTask<Void, String, String[]>() {

                                Dialog plzWaitDialog;

                                @Override
                                protected void onPreExecute() {
                                    plzWaitDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                                    super.onPreExecute();
                                }

                                @Override
                                protected String[] doInBackground(Void... param) {
                                    String url = "https://www.googleapis.com/oauth2/v4/token";
                                    try {
                                        StringBuilder content = new StringBuilder();
                                        URL mUrl = new URL(url);
                                        HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                                        connection.setRequestMethod("POST");
                                        List<NameValuePair> params = new ArrayList<>();
                                        params.add(new BasicNameValuePair("grant_type", "authorization_code"));
                                        params.add(new BasicNameValuePair("client_id", client_id));
                                        params.add(new BasicNameValuePair("client_secret", client_secret));
                                        params.add(new BasicNameValuePair("redirect_uri", ""));
                                        params.add(new BasicNameValuePair("code", serverAuthCode));

                                        OutputStream os = connection.getOutputStream();
                                        BufferedWriter writer = new BufferedWriter(
                                                new OutputStreamWriter(os, "UTF-8"));
                                        StringBuilder output = new StringBuilder();
                                        boolean first = true;
                                        for (NameValuePair pair : params) {
                                            if (first)
                                                first = false;
                                            else
                                                output.append("&");
                                            output.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                                            output.append("=");
                                            output.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
                                        }
                                        writer.write(output.toString());
                                        writer.flush();
                                        writer.close();
                                        os.close();


                                        if (connection.getResponseCode() == 200) {
                                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                            String line;
                                            while ((line = bufferedReader.readLine()) != null)
                                                content.append(line).append("\n");
                                            bufferedReader.close();
                                            JSONObject result = new JSONObject(content.toString());
                                            String accessToken = result.getString("access_token");
                                            String expiry = result.getString("expires_in");
                                            String[] arr = new String[2];
                                            arr[0]=  accessToken;
                                            arr[1] = expiry;
                                            Log.d("DEBUG::GOOGLE_LOGIN", content.toString());
                                            return arr;
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(String[] s) {
                                    if(s!=null && s.length==2)
                                        mCallback.onGoogleConnected(s[0], s[1], plzWaitDialog);
                                    else
                                    {
                                        DialogUtils.dialogDismiss(plzWaitDialog);
                                        Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }.execute();

                        } else {
                            Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                            Log.d("DEUBG::LOGINERROR", "Unable to Login: " + result.getStatus());                        }
                    } else {
                        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

                        if (result!=null) {
                            if(result.isSuccess()) {
                                GoogleSignInAccount acct = result.getSignInAccount();
                                if (acct == null) {
                                    Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                                    Log.d("DEUBG::LOGINERROR", "Unable to Login: " + result.getStatus());                                }
                            }
                            else{
                                Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                                Log.d("DEUBG::LOGINERROR", "Unable to Login: " + result.getStatus());                            }
                        }
                        else{
                            Toast.makeText(mActivity, "Unable to Login", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }
        };

    }



    public interface GoogleCallBack {

        void onGoogleConnected(String accessToken, String expiry, Dialog customDialog);

    }

}
