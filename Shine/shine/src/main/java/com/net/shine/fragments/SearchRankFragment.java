package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.models.WhoViewMyProfileModel;
import com.net.shine.utils.ShineCommons;

/**
 * Created by manishpoddar on 03/04/17.
 */

public class SearchRankFragment extends BaseFragment {


    private View parentView;
    private View errorLayout, rankContainer;
    private TextView rank_text;
    private WhoViewMyProfileModel.Result result;
    private TextView errorLine1,errorLine2;
    private Button action;
    private Bundle bundle;


    public static SearchRankFragment newInstance(Bundle args) {
        SearchRankFragment fragment = new SearchRankFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.search_rank_frag, container, false);
        bundle= getArguments();

        errorLayout = parentView.findViewById(R.id.error_layout);
        errorLine1 = (TextView) parentView.findViewById(R.id.error_line1);
        errorLine2 = (TextView) parentView.findViewById(R.id.error_line2);
        action = (Button) parentView.findViewById(R.id.action_button);
        action.setText(mActivity.getString(R.string.update_profile));
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseFragment fragment = new MyProfileFrg();
                mActivity.singleInstanceReplaceFragment(fragment);
                ShineCommons.trackShineEvents("ProfileViews","Update Profile-Search Rank",mActivity);

            }
        });
        rank_text = (TextView) parentView.findViewById(R.id.rank);
        rankContainer = parentView.findViewById(R.id.rank_container);
        result= (WhoViewMyProfileModel.Result) bundle.getSerializable("list_item");



        if(result.avg_rank==0){
            rankContainer.setVisibility(View.VISIBLE);
            rank_text.setText(result.avg_rank.toString());
            errorLine1.setText(mActivity.getString(R.string.no_rank));
            errorLine2.setText(mActivity.getString(R.string.update_rank));

        }
        else if(result.avg_rank>5){
            rankContainer.setVisibility(View.VISIBLE);
            if(result.avg_rank>100){
                rank_text.setText("100+");
            }
            else {
            rank_text.setText(result.avg_rank.toString());
            }

            errorLine1.setText(mActivity.getString(R.string.for_rank_beyond_five_msg1));
            errorLine2.setText(mActivity.getString(R.string.for_rank_beyond_five_msg2));

        }
        else {
            rankContainer.setVisibility(View.VISIBLE);
            rank_text.setText(result.avg_rank.toString());
            errorLine1.setText(mActivity.getString(R.string.for_rank_below_five_msg1));
            errorLine2.setText(mActivity.getString(R.string.for_rank_below_five_msg2));

        }





        return parentView;
    }
}