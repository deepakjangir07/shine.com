package com.net.shine.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.util.Log;

import com.net.shine.R;

import java.util.List;

/**
 * Created by manishpoddar on 06/10/16.
 */

public class ChromeCustomTabs {


    // Package name for the Chrome channel the client wants to connect to. This
// depends on the channel name.
// Stable = com.android.chrome
// Beta = com.chrome.beta
// Dev = com.chrome.dev

    public static final String CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome";
    private static final String SERVICE_ACTION = "android.support.customtabs.action.CustomTabsService";




    private static CustomTabsClient mCustomTabsClient;
    private static CustomTabsSession mCustomTabsSession;
    private static CustomTabsServiceConnection mCustomTabsServiceConnection;
    private static CustomTabsIntent mCustomTabsIntent;
    static CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
    static String Url;
    static Activity act;





    private static boolean isChromeCustomTabsSupported(@NonNull final Context context) {
        Intent serviceIntent = new Intent(SERVICE_ACTION);
        serviceIntent.setPackage(CUSTOM_TAB_PACKAGE_NAME);
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentServices(serviceIntent, 0);
        return !(resolveInfos == null || resolveInfos.isEmpty());
    }

    public static void openCustomTab(Activity context, String shineurl) {

        Url = shineurl;
        act = context;



        Log.d("in here", "custom tabs");

        builder.setShowTitle(true);

//        String shareLabel = act.getResources().getString(R.string.share_this_job_title);
//        Bitmap icon = BitmapFactory.decodeResource(act.getResources(), (R.drawable.ic_share));
//
//        //Create a PendingIntent to your BroadCastReceiver implementation
//        Intent actionIntent = new Intent(
//                act.getApplicationContext(), ShareBroadcastReceiver.class);
//        actionIntent.setData(Uri.parse(Url));
//        PendingIntent pendingIntent =
//                PendingIntent.getBroadcast(act, 0, actionIntent, 0);
//
//        //Set the pendingIntent as the action to be performed when the button is clicked.
//        builder.setActionButton(icon, shareLabel, pendingIntent);


        builder.setToolbarColor(context.getResources().getColor(R.color.colorPrimary));
        builder.setStartAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out);
        builder.setExitAnimations(context, android.R.anim.fade_out, android.R.anim.fade_in);
        mCustomTabsIntent = builder.build();

        mCustomTabsIntent.launchUrl(context, Uri.parse(shineurl));

        try {


            mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
                @Override
                public void onCustomTabsServiceConnected(ComponentName name, CustomTabsClient client) {
                    mCustomTabsClient = client;
                    mCustomTabsClient.warmup(0L);
                    mCustomTabsSession = mCustomTabsClient.newSession(null);
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {

                    mCustomTabsClient = null;


                }
            };

            if (isChromeCustomTabsSupported(act)) {
                try {
                    CustomTabsClient.bindCustomTabsService(context, CUSTOM_TAB_PACKAGE_NAME, mCustomTabsServiceConnection);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//    public static class ShareBroadcastReceiver extends BroadcastReceiver{
//
//
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            String url = intent.getDataString();
//
//            if (url != null) {
//                Intent shareIntent = new Intent(Intent.ACTION_SEND);
//                shareIntent.setType("text/plain");
//                shareIntent.putExtra(Intent.EXTRA_TEXT, url);
//
//                Intent chooserIntent = Intent.createChooser(shareIntent, "Share url");
//                chooserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                context.startActivity(chooserIntent);
//            }
//
//        }
//    }




}


