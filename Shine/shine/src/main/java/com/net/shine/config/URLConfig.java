package com.net.shine.config;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.utils.db.ShineSqliteDataBase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class URLConfig {

	public static int AD=0;
	public static int BELOW_AD=0;
	public static String key="el!bomen!h$";
	public static String DEVICE_ANDROID="1";
	public static String VERSION_CODE = "version_code";
	public static String LINKEDIN_HEADER_KEY = "X-Konnect";
	public static String LINKEDIN_HEADER_VALUE = "WqqEWELsTGp/96KEGOunIV/B90lAAmvUaM82/BkXQVjCq85S+IYs/xxxT4e6c0yCbU/J3vEi1lhtkCnMw70vcg+1u5Bsvh6sPAcTAc2RB9gK/DrWEaKtIMyYjjdQiUacJmrkkpojTudR02jJhURVPapoEngyGOAG2qhk5K0hnJo=";
	public static String FCM_REG_ID = "reg_id";
    public static String CLIENT_ACCESS_TOKEN="Client-Access-Token";
    public static String USER_ACCESS_TOKEN="User-Access-Token";
    public static String JOB_ALERT_ID="jobalertid";
    public static String REFERRAL_ID="<referral_id>";
	public static String MOBILE_NO = "<mob>";
	public static final int RATE_COUNT = 10;
	public static final String SORT_DATE = "sort_date";
	public static String NO_SELECTION = "None";
	public static final String SEARCH_VO_KEY = "searchvo";
	public static final String SEARCH_VO_WITH_FACETS = "search_facets_with_vo";
	public static final String SEARCH_FACETS_VO_KEY = "searchfacetsvo";
	public static final String APPLY_FROM_WIDGET="apply_from_widget";
	public static final String CUSTOM_JOB_MODIFY = "custom-job_vomodify";
	public static final String CUSTOM_JOB_NEW_ADD = "custom-job_new_add";
	public static final String SEARCH_VO_MODIFY = "searchvomodify";
	public static final String DEVICE_NOTIFICATION_STATUS = "device_notification_status";
	public static final int NO_OF_DATE = 90;
	public static int PER_PAGE = 12;
	public static int PER_PAGE_JOB_ALERT_MAIL = 10;


	public static final String APP_GOOGLE_PLAY_LINK = "https://play.google.com/store/apps/details?id=com.net.shine";
	public static final String CARRER_PLUS_LINK = "http://careerplus.shine.com/?utm_source="
			+ "shineapp&utm_medium=app&utm_campaign=mobile";
	public static final String CARRER_INFO_LINK = "http://info.shine.com/?utm_source="
			+ "shineapp&utm_medium=app&utm_campaign=mobile";

	public static final String PHONEBOOK_SYNC_TIME = "phonebook_sync_time";
	public static final String PHONEBOOK_SYNC_STATUS = "phonebook_sync";
    public static final String JOB_ID = "<JOB_ID>";
	public static final String JOB_ID_GCM = "job_id";
    public static final String CANDIDATE_ID = "CANDIDATE_ID";
	public static final String EMAIL_ID = "email_id";
	public static final String ALERT_ID = "alert_id";
    public static final String ID2 = "<ID2>";

    public static final String RESUME_ID = "<RESUME_ID>";
    public static boolean LOOKUP_INSERTION_FLAG = false;

	// New apis

    public static final String CLIENT_AUTH_API= ServerConfig.SERVER_IP +"/api/v2/client/access/";
    public static final String USER_AUTH_API= ServerConfig.SERVER_IP +"/api/v2/user/access/";
    public static final String FCM_POST_API = ServerConfig.SERVER_IP +"/api/v2/post-gcm/";


	public static final String REG_DUMP_API = ServerConfig.SERVER_IP +"/api/v3/registration-dump/"+ CANDIDATE_ID +"/";
	public static final String LINKEDIN_LOGIN_API = ServerConfig.SERVER_IP +"/api/v2/linkedin/login/";
	public static final String LINKEDIN_REGISTER_API = ServerConfig.SERVER_IP +"/api/v2/linkedin/registration/";
	public static final String LINKEDIN_PROFILE_SYNC_API = ServerConfig.SERVER_IP +"/api/v2/linkedin/candidates/"+CANDIDATE_ID+"/profile-sync/";


	public static final String RECENT_SEARCHES_API = ServerConfig.SERVER_IP +"/api/v2/search/recent-searches/"+CANDIDATE_ID+"/";


	public static final String GOOGLE_LOGIN_API = ServerConfig.SERVER_IP +"/api/v2/google-plus/login/";
	public static final String GOOGLE_REGISTER_API = ServerConfig.SERVER_IP +"/api/v2/google-plus/registration/";
	public static final String GOOGLE_PROFILE_SYNC_API = ServerConfig.SERVER_IP +"/api/v2/google-plus/candidates/"+CANDIDATE_ID+"/profile-sync/";
	public static final String GOOGLE_APPLY_API = ServerConfig.SERVER_IP +"/api/v2/google-plus/apply/";
	public static final String LINKEDIN_APPLY_API = ServerConfig.SERVER_IP +"/api/v2/linkedin/apply/";

	public static final String BULK_JOBS_APPLY_API = ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/job-apply/multiple/";


	public static final String FACEBOOK_LOGIN_API = ServerConfig.SERVER_IP +"/api/v2/facebook/login/";
	public static final String FACEBOOK_REGISTER_API = ServerConfig.SERVER_IP +"/api/v2/facebook/registration/";
	public static final String FACEBOOK_PROFILE_SYNC_API = ServerConfig.SERVER_IP +"/api/v2/facebook/candidates/"+CANDIDATE_ID+"/profile-sync/";
	public static final String FACEBOOK_APPLY_API = ServerConfig.SERVER_IP +"/api/v2/facebook/apply/";


	public static final String SOCIAL_APPLY_API = ServerConfig.SERVER_IP + "/api/v2/social-application/";
	public static final String SOCIAL_APPLY_AND_REGISTER_API = ServerConfig.SERVER_IP + "/api/v2/direct-applications/";



	 public static final String JD_PIXEL_URL="https://www3.shine.com/mylogger.gif?";

	public static String AD_URL_LOGOUT=ServerConfig.SERVER_IP+"/api/v2/shine-ad-plus/0/";

	public static String AD_URL_LOGGEDIN=ServerConfig.SERVER_IP+"/api/v2/shine-ad-plus/0/"+CANDIDATE_ID+"/";


	public static String BELOW_AD_URL_LOGOUT=ServerConfig.SERVER_IP+"/api/v2/shine-ad-plus/1/";

	public static String BELOW_AD_URL_LOGGEDIN=ServerConfig.SERVER_IP+"/api/v2/shine-ad-plus/1/"+CANDIDATE_ID+"/";


	public static  String SHINE_NON_CONNECTIONS=
			ServerConfig.SERVER_IP+"/api/v2/candidate/"+CANDIDATE_ID+"/non-shine-connections/";

    public static final String FORGOT_PASSWORD = ServerConfig.SERVER_IP
            + "/api/v2/login/forgot-password/";
    public static final String REGISTRATION_URL = ServerConfig.SERVER_IP
            + "/api/v3/mobile/candidate-profiles/";
    public static final String EMAIL_ID_CHECK_URL = ServerConfig.SERVER_IP

            + "/api/v2/email-exists/?email=" + EMAIL_ID;

	public static final String CHANGE_PASSWORD_URL = ServerConfig.SERVER_IP
			+"/api/v2/candidate/"+CANDIDATE_ID+"/change-password/";

	public static final String SUDO_APPLY_URL = ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+
			"/pseudo-job-apply/"+JOB_ID+"/";
    public static final String REGISTRATION_COMPLETE_URL = ServerConfig.SERVER_IP
            + "/api/v3/mobile/candidate-profiles/"+ CANDIDATE_ID +"/";
    public static final String KONNECT_PHONEBOOK_URL = ServerConfig.SERVER_IP
            +"/api/v2/candidate/contacts/phonebook/";

	public static final String SAVE_JOB_URL = ServerConfig.SERVER_IP
			+"/api/v2/candidate/"+CANDIDATE_ID+"/shortlist-jobs/";
	public static final String UNSAVE_JOB_URL = ServerConfig.SERVER_IP
			+"/api/v2/candidate/"+CANDIDATE_ID+"/shortlist-jobs/"+JOB_ID+"/";

	public static final String SAVED_JOBS_URL = ServerConfig.SERVER_IP
			+"/api/v2/search/candidate/"+CANDIDATE_ID+"/shortlisted/";
    public static final String KONNECT_GMAIL_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/contacts/gmail/";
    public static final String KONNECT_LINKEDIN_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/contacts/linkedin/";
    public static final String KONNECT_CONNECTIONS_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/connections/";
	public static final String RELEVENT_SUGGESTION_URL = ServerConfig.SERVER_IP
			+ "/api/v2/search/keyword-suggestion/";

    public static final String USER_SERVER_STATUS_URL= ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/status/";

    public static final String PROFILE_DETAIL_URL = ServerConfig.SERVER_IP
            + "/api/v3/candidate-profiles/"+ CANDIDATE_ID +"/";
	public static final String UPDATE_FLOW_URL = ServerConfig.SERVER_IP
			+ "/api/v3/candidate/"+CANDIDATE_ID+"/profile-update-flows/next/";

	public static final String LINKEDIN_UPDATE_FLOW_DELETE_URL = ServerConfig.SERVER_IP
			+ "/api/v3/candidate/"+CANDIDATE_ID+"/profile-update-flows/2/";

	public static final String WHO_VIEW_PROFILE_URL = ServerConfig.SERVER_IP
            + "/api/v3/candidate/activities/"+ CANDIDATE_ID +"/";



    public static final String APPLY_JOBS_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/job-apply/";
	public static final String SOCIAL_APPLY_RESUME_URL = ServerConfig.SERVER_IP
			+ "/api/v2/social-profiles/"+CANDIDATE_ID+"/resume-file/";	//Note that here candidate is the temp profile id
	public static final String APPLY_WITHOUT_RESUME_URL = ServerConfig.SERVER_IP
			+ "/api/v2/without-resume-application/";
    public static final String INBOX_LIST_RECRUITER_MAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/inbox/";
    public static final String INBOX_LIST_JOB_ALERT_MAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/job-alert-mails/";
    public static final String RECRUITER_MAIL_DETAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/inbox/"+ID2+"/";
//	public static final String KEYWORD_SUGGESTION_URL = ServerConfig.SERVER_IP_SHINE
//			+"/search/api/v1/keyword/?format=json";
//	public static final String INSTITUTE_SUGGESTION_URL = ServerConfig.SERVER_IP_SHINE
//			+"/lookup/educationinstitute/";
//	public static final String SKILL_SUGGESTION_URL=ServerConfig.SERVER_IP_SHINE+"/lookup/skill/";

    public static final String JOB_ALERT_MAIL_DETAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/job-alert-mails/"+ID2+"/";
    public static final String VERIFY_EMAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/email-verify/";
    public static final String CUSTOM_JOB_ALERT_CREATE= ServerConfig.SERVER_IP +"/api/v2/job-alerts/";
    public static final String CUSTOM_JOB_ALERT_CREATE_USER= ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/job-alerts/";
    public static final String CUSTOM_ALERT_URL= ServerConfig.SERVER_IP +"/api/v2/job-alerts/";

    public static final String UPDATE_JOB_ALERT= ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/job-alerts/jobalertid/";

    public static final String DELETE_JOB_ALERT= ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/job-alerts/"+ ALERT_ID +"/";

	public static final String NOTIFICATION_API = ServerConfig.SERVER_IP+"/api/v2/candidate-notification-data/"+CANDIDATE_ID+"/";
	public static final String NOTIFICATION_TIMESTAMP_UPDATE_API = ServerConfig.SERVER_IP+"/api/v2/candidate-meta-data/"
			+CANDIDATE_ID+"/?field_name=";

    public static final String INBOX_APPLY_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/inbox-job-apply/";

    public static final String UPDATE_EDU_DETAILS= ServerConfig.SERVER_IP +"/api/v3/candidate/"+ CANDIDATE_ID +"/educations/";
	public static final String GCM_UPDATE_EMPLOYMENT_DETAIL= ServerConfig.SERVER_IP
			+ "/api/v2/candidate/"+ CANDIDATE_ID +"/jobs/"+JOB_ID_GCM+"/";
    public static final String ACCEPT_REJECT_URL= ServerConfig.SERVER_IP +"/api/v2/candidate/referral/<referral_id>/";

    public static final String VIEW_MESSAGE_URL= ServerConfig.SERVER_IP_MOBILE_SHINE +"/refer-your-friend/";

    public static final String VIEW_MESSAGE_URL_ENCRYPTED= ServerConfig.SERVER_IP_SHINE +"/refer-your-friend/";

	public static final String UPDATE_TOTAL_EXP_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/" + CANDIDATE_ID + "/total-experience/";
	public static final String UPDATE_TOTAL_SAL_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/" + CANDIDATE_ID + "/salary/";


	public static final String BULK_ADD_SKILL_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/" + CANDIDATE_ID + "/bulk-skills-create/";
    public static final String BULK_UPDATE_SKILL_URL= ServerConfig.SERVER_IP +"/api/v2/candidate/"+ CANDIDATE_ID +"/bulk-skills/";


	public static final String BULK_UPDATE_CERTIFICATION=ServerConfig.SERVER_IP+"/api/v2/candidate/"+ CANDIDATE_ID +"/bulk-certifications/";

    public static final String GET_REFERRAL_URL= ServerConfig.SERVER_IP +"/api/v2/candidate/"+CANDIDATE_ID+"/referrals/?";



    public static final String SEARCH_RESULT_URL_LOGOUT= ServerConfig.SERVER_IP +"/api/v2/search/simple/";

     public static final String SEARCH_RESULT_URL_LOGIN = ServerConfig.SERVER_IP +"/api/v2/search/candidate/"+CANDIDATE_ID+"/simple/";

	public static final String JOBS_IN_KONNECT_URL_LoggedIn = ServerConfig.SERVER_IP +"/api/v2/search/candidate/"+ CANDIDATE_ID +"/matched-best/";

    public static final String JOB_DETAIL_URL_LOGGED_IN= ServerConfig.SERVER_IP +"/api/v2/search/candidate/"+CANDIDATE_ID+"/job-description/";
    public static final String JOB_DETAIL_URL_LOGGED_OUT= ServerConfig.SERVER_IP +"/api/v2/search/job-description/";

    public static final String SIMILAR_JOBS_URL_LOGOUT= ServerConfig.SERVER_IP +"/api/v2/search/similar/?";
    public static final String SIMILAR_JOBS_URL_LOGIN= ServerConfig.SERVER_IP +"/api/v2/search/candidate/"+CANDIDATE_ID+"/similar/?";


	public static final String LOOKUP_SERVER = ServerConfig.SERVER_IP;

	public static final String LOOKUP_FUNCTIONALAREA = LOOKUP_SERVER
			+ "/api/v2/lookup/functionalarea/";
	public static final String LOOKUP_SKILL = LOOKUP_SERVER + "/api/v2/lookup/skill/";
	public static final String LOOKUP_INDUSTRY = LOOKUP_SERVER
			+ "/api/v2/lookup/industry/";
	public static final String LOOKUP_TEAMSIZEMANAGED = LOOKUP_SERVER
			+ "/api/v2/lookup/teamsizemanaged/";
	public static final String LOOKUP_ANNUALSALARY = LOOKUP_SERVER
			+ "/api/v2/lookup/annualsalary/";
	public static final String LOOKUP_SHIFTTYPE = LOOKUP_SERVER
			+ "/lookup/shifttype/";
//	public static final String LOOKUP_EDUCATIONQUALIFICATIONLEVEL = LOOKUP_SERVER
//			+ "/api/v2/lookup/educationqualificationlevel/";
	public static final String LOOKUP_EDUCATIONSTREAM = LOOKUP_SERVER
			+ "/api/v2/lookup/educationstream/";
	public static final String LOOKUP_EDUCATIONINSTITUTE = LOOKUP_SERVER
			+ "/api/v2/lookup/educationinstitute/";
	public static final String LOOKUP_CITY = LOOKUP_SERVER + "/api/v2/lookup/city/";
	public static final String LOOKUP_EXPERIENCE = LOOKUP_SERVER
			+ "/api/v2/lookup/experience/";
	public static final String LOOKUP_COURSETYPE = LOOKUP_SERVER
			+ "/api/v2/lookup/coursetype/";

	public static final String CONFIG_STATUS = LOOKUP_SERVER
			+ "/api/v2/config/?platform=android";

	public static final String SUSPICIOUS_PHONE_NUMBER_URL = ServerConfig.SERVER_IP
			+ "/api/v1/suspicious-phone-numbers/"+MOBILE_NO+"/";
    public static final String VERIFY_MOBILE_OTP_URL = ServerConfig.SERVER_IP
            +"/api/v1/otp-served-phone/"+MOBILE_NO+"/";
    public static final String CELLPHONE_VERIFIED = ServerConfig.SERVER_IP
            +"/api/v2/verified-phone-number/"+CANDIDATE_ID+"/";






	public static final String CELLPHONE_VERIFIED_TRUECALLER = ServerConfig.SERVER_IP
			+"/api/v2/candidate/"+CANDIDATE_ID+"/mobile-verification/true-caller/";

	public static final int COMPANIES_PER_PAGE = 15;
	public static final String TERMS_AND_CONDITION = "https://www.shine.com/mobileapp/termsandconditions/";
	public static final String CLEAR_GCM_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/clear-cache/";

	public static final String MATCH_JOB_URL = ServerConfig.SERVER_IP + "/api/v2/search/candidate/"+ CANDIDATE_ID +"/matched/?konnect=1";
    public static final String MATCH_JOB_OTHER_URL = ServerConfig.SERVER_IP
            + "/api/v2/search/candidate/"+ CANDIDATE_ID +"/matched-other/?konnect=1";

    public static final String MATCH_JOB_CONNECT_URL = ServerConfig.SERVER_IP + "/api/v2/search/candidate/"+ CANDIDATE_ID +"/matched-connection/";

	public static final String FEEDBACK_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/feedback/";
	public static final String CANDIDATE_CONNECT_MAIL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/connect/";

    public static final String MY_DESIRED_JOB_DETAIL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/preferences/bulk/";
    public static final String MY_TOTAL_PROFESSIONAL_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate-work-experience/"+ CANDIDATE_ID +"/";
    public static final String MY_TOTAL_JOBS_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/jobs/";


	public static final String JOBS_APPLIED_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/"+ CANDIDATE_ID +"/applied-jobs/";


	public static final String RESUME_UPLOAD_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/"+ CANDIDATE_ID +"/resumefiles/";
    public static final String RESUME_UPLOAD_PARSER_URL = ServerConfig.SERVER_IP
            + "/api/v2/candidate/"+ CANDIDATE_ID +"/parser-resume-files/";

	public static final String RESUME_EDIT_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/" + CANDIDATE_ID +"/resumes/" + RESUME_ID+"/";
	public static final String RESUME_DOWNLOAD_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/"+ CANDIDATE_ID +"/resumefiles/" + RESUME_ID+"/";

	public static final String MY_PROFILE_URL = ServerConfig.SERVER_IP
			+ "/api/v2/candidate-personal-details/"+ CANDIDATE_ID +"/";

	public static final String MY_EDUCATIONS_URL = ServerConfig.SERVER_IP
			+ "/api/v3/candidate/"+ CANDIDATE_ID +"/educations/";

	public static final String SEND_REFERRAL_URL = ServerConfig.SERVER_IP + "/api/v2/candidate/"+CANDIDATE_ID+"/referral/";

	public static final String MOBILE_VERIFY = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/mobile-verify/";
	public static final String VERIFY_OTP_WITHOUT_RESUME = ServerConfig.SERVER_IP
			+ "/api/v2/without-resume-application/otp/"+MOBILE_NO+"/"+JOB_ID+"/";


	public static final String DISCOVER_FRIENDS = ServerConfig.SERVER_IP
			+ "/api/v2/candidate/discover/";

	public static final String DYNAMIC_POSTBACK_URL = ServerConfig.SERVER_IP
			+ "/api/v2/admin/post-back-url/?";

	public static final String NON_SHINE_CONNECTION_LINK=ServerConfig.SERVER_IP+"/app-referral/?id=com.net.shine&hl=en&mt=8&referrer=utm_source%3DAppInvite";
//&referrer=utm_source%3DHTMobile
	public static final String INDEX = "index";
	public static final String INDEX_MAIL = "index_mail";

	public static final String KEY_SEARCH_FACETS = "search_facets";



	public static final String KEY_ACTION = "action";
	public static final String FROM_INSIDE_ADD_RESUME = "add_additional_resume";

	public static final String KEY_MY_PROFILE_EDIT = "edit_my_prf";
	public static final String KEY_PROFILE_TYPE = "p_type";
	public static final String ADD_OPERATION = "add";
	public static final String UPDATE_ALL_OPERATION = "updateall";

	public static final String RESUMES_PROFILE = "resume_info";

	public static final String KEY_MODEL_NAME = "profile_model";

	public static final String FROM_NOTIFICATION = "from_notification";
	public static final String NOTIFICATION_TYPE = "noti_type";

	public static final String MATCHED_JOBS_NOTIFI = "1";
    public static final String REFERRAL_REQUEST_NOTIFI = "14";
    public static final String REFERRAL_RECEIVED_NOTIFI = "13";
	public static final String APPLIED_JOBS_NOTIFI = "23";
	public static final	String APP_UPDATE = "30";

	public static final String APP_INVITE="31";
	public static final String MY_CONNECTION_JOB="34";
	public static final String MY_PROFILE_NOTIFI = "2";
	public static final String FEEDBACK_NOTIFI = "10";

	public static final String MY_PROFILE_VIEW_NOTIFI = "3";
	public static final String JOB_DETAILS_NOTIFI = "4";
	public static final String RESUME_NOTIFY = "resume_notify";

	public static final String EDUCATION_ADD_NOTIFI = "35";
	public static final String EMPLOYMENT_ADD_NOTIFI = "36";
	public static final String EXPERINCE_NOTIFI = "37";
	public static final String CERTIFICATION_NOTIFI = "38";
	public static final String DESIRED_JOB_DETAILS_NOTIFI="39";
	public static final String SKILL_NOTIFI = "40";
	public static final String SAVED_JOBS_NOTIFI = "41";
    public static final String LOGIN_NOTIFI = "42";
    public static final String REGISTER_NOTIFI = "43";


	public static final String MAIL_NOTIFY = "5";

	public static  final String INBOX_JOB_ALERT_NOTIFY="22";
	public static final String MAIL_NOTIFY_MAIL_ID = "mail_id";

	public static final String INBOX_JOB_ALERT_ID="inbox_alert_id";

    public static final String MAIL_NOTIFY_USER_ID = "user_id";
	public static final String MAIL_NOTIFY_OBJ_ID = "obj_id";

	public static final String REGISTER_COMPLETE_NOTIFY = "6";
	public static final String DISCOVER_NOTIFY = "7";
	public static final String JOB_ALERT_NOTIFI = "8";

	public static final String SEARCH_SIMPLE_NOTIFY = "9";
	public static final String JOB_ALERT_NOTIFI_FLAG = "job_alert_notify_flag";

	//FIXME: 10/27/2015 This param is passed unnecessarily when calling JobDetailParentFrg
	public static final String JOB_TYPE = "job_type";

	public static final String MAIL_TYPE = "mail_type";

	public static HashMap<String, String> SALARY_MAP;
	public static HashMap<String, String> YEAR_MAP;

	public static final int FROM_EMAIL_LINK = 2;
	public static final String JOB_ID_FROM_EMAIL_LINK = "job_id";

    public static final String JOB_SEARCH_KEYWORD="search_keyword";

	public static final String USERNAME = "username";
	public static final String USERID = "userid";
	public static final String JOBTITLE = "JobTitle";
	public static final String NEXTSTATUS = "getnextstatus";
	public static final String USEREMAIL = "useremail";
	public static final String USEREMOB = "usermob";
    public static final String IS_MID_OUT="is_mid_out";


	public static String[] NON_SKIP_VENDOR_IDS ={"71","72","73","74","75","76","77","78","79","80"};

	public static String[] YEARS_NAME_LIST = { "2016", "2015", "2014", "2013", "2012",
			"2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004",
			"2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996",
			"1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988",
			"1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980",
			"1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972",
			"1971", "1970" };

	public static final String[] SALARY_LIST = { NO_SELECTION, "Rs 0 / Yr",
			"< Rs 50,000 / Yr", "Rs 50,000 - 1 Lakh / Yr",
			"Rs 1.0 - 1.5 Lakh / Yr", "Rs 1.5 - 2.0 Lakh / Yr",
			"Rs 2.0 - 2.5 Lakh / Yr", "Rs 2.5 - 3.0 Lakh / Yr",
			"Rs 3.0 - 3.5 Lakh / Yr", "Rs 3.5 - 4.0 Lakh / Yr",
			"Rs 4.0 - 4.5 Lakh / Yr", "Rs 4.5 - 5.0 Lakh / Yr",
			"Rs 5 - 6 Lakh / Yr", "Rs 6 - 7 Lakh / Yr", "Rs 7 - 8 Lakh / Yr",
			"Rs 8 - 9 Lakh / Yr", "Rs 9 - 10 Lakh / Yr",
			"Rs 10 - 12 Lakh / Yr", "Rs 12 - 14 Lakh / Yr",
			"Rs 14 - 16 Lakh / Yr", "Rs 16 - 18 Lakh / Yr",
			"Rs 18 - 20 Lakh / Yr", "Rs 20 - 22 Lakh / Yr",
			"Rs 22 - 24 Lakh / Yr", "Rs 24 - 26 Lakh / Yr",
			"Rs 26 - 28 Lakh / Yr", "Rs 28 - 30 Lakh / Yr",
			"Rs 30 - 32 Lakh / Yr", "Rs 32 - 34 Lakh / Yr",
			"Rs 34 - 36 Lakh / Yr", "Rs 36 - 38 Lakh / Yr",
			"Rs 38 - 40 Lakh / Yr", "Rs 40 - 42 Lakh / Yr",
			"Rs 42 - 44 Lakh / Yr", "Rs 44 - 46 Lakh / Yr",
			"Rs 46 - 48 Lakh / Yr", "Rs 48 - 50 Lakh / Yr",
			"Rs 50 - 75 Lakh / Yr", "Rs 75 Lakh - 1 Crore / Yr",
			"> Rs 1 Crore / Yr" };

	public static String[] COUNTRY_LIST;

	public static HashMap<String, String> COUNTRY_MAP= new HashMap<>();
	public static HashMap<String, String> COUNTRY_REVERSE_MAP= new HashMap<>();

	public static String[] CITY_LIST;
	public static String[] CITY_LIST_EDIT;
	public static HashMap<String, String> CITY_MAP = new HashMap<>();
	public static HashMap<String, String> CITY_REVERSE_MAP = new HashMap<>();

	public static String[] GENDER_LIST = { "Male", "Female" };
	public static String[] JOB_TYPE_LIST = { "Any", "Full Time", "Part Time",
			"Internship" };
	public static String[] jOB_TYPE_LIST_VAL = { "0", "2", "3", "5" };

	public static String[] SHIFT_TYPE_LIST = { "Rotating", "Morning", "Noon",
			"Night", "Evening", "Split" };
	public static HashMap<String, String> GENDER_MAP;
	public static HashMap<String, String> GENDER_REVERSE_MAP;
	public static HashMap<String, String> JOB_MAP;
	public static HashMap<String, String> JOB_REVERSE_MAP;
    public static HashMap<String, String> SHIFT_TYPE_MAP,SHIFT_TYPE_REVERSE_MAP;



    static {


		SALARY_MAP = new HashMap<>();
		int len = SALARY_LIST.length;
		for (int i = 0; i < len; i++) {
			if (i != 0) {
				SALARY_MAP.put(SALARY_LIST[i], "" + (i + 2)); // According to
				// shine.com,
				// not from
				// sumosc.shine.com
			} else {
				SALARY_MAP.put(SALARY_LIST[i], "");
			}

		}

		YEAR_MAP = new HashMap<>();


		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		if(year>1970){
			int count = year-1969;
			YEARS_NAME_LIST = new String[count];
			for(int i=year,j=0;j<count;i--,j++){
				YEARS_NAME_LIST[j] = String.valueOf(i);
			}
		}
		else{
			YEARS_NAME_LIST = new String[1];
			YEARS_NAME_LIST[0] = "1970";
		}


		len = YEARS_NAME_LIST.length;
		for (int i = 0; i < len; i++) {

			YEAR_MAP.put(YEARS_NAME_LIST[i], YEARS_NAME_LIST[i]);

		}
        SHIFT_TYPE_MAP = new HashMap<>();
        SHIFT_TYPE_REVERSE_MAP = new HashMap<>();


        len = SHIFT_TYPE_LIST.length;
        for (int i = 0; i < len; i++) {

            SHIFT_TYPE_MAP.put(i+"", SHIFT_TYPE_LIST[i]);
            SHIFT_TYPE_REVERSE_MAP.put( SHIFT_TYPE_LIST[i],""+i);

        }

		GENDER_MAP = new HashMap<>();
		GENDER_REVERSE_MAP = new HashMap<>();
		len = GENDER_LIST.length;
		for (int i = 0; i < len; i++) {
			GENDER_MAP.put(GENDER_LIST[i], "" + (i + 1));
			GENDER_REVERSE_MAP.put("" + (i + 1), GENDER_LIST[i]);

		}
		JOB_MAP = new HashMap<>();
		JOB_REVERSE_MAP = new HashMap<>();
		len = JOB_TYPE_LIST.length;
		for (int i = 0; i < len; i++) {
			JOB_MAP.put(JOB_TYPE_LIST[i], "" + (i + 1));
			JOB_REVERSE_MAP.put("" + (i + 1), JOB_TYPE_LIST[i]);

		}
	}




	public static Set<String> jobAppliedSet = new HashSet<>();

	public static final int UPDATE_RESUMES = 14;

	public static boolean isRecentSync = false;

	public static final String PARSING_ERROR_MSG = "An unexpected error has occured. Please try again after some time.";

	//Init City From DB
	public static void initializeCityList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getCityList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pid = crs.getString(crs.getColumnIndex("pid"));
					String cid = crs.getString(crs.getColumnIndex("cid"));
					if (!list2.contains(pid))
						list2.add(pid.trim());
					list2.add(cid);
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					String cdesc = crs.getString(crs.getColumnIndex("cdesc"));
					if (!list.contains("#" + pdesc.trim()))
						list.add("#" + pdesc.trim());
					list.add(cdesc);

				}
				crs.close();
			}

			if (CITY_MAP.size()==0 && CITY_REVERSE_MAP.size() ==0) {
				
				int size = list.size();
				CITY_LIST = new String[list.size()];
				for (int i = 0; i < size; i++) {
					CITY_REVERSE_MAP.put(list2.get(i), list.get(i));
					CITY_MAP.put(list.get(i), list2.get(i));
					CITY_LIST[i] = list.get(i);
				}
				if (CITY_LIST != null && CITY_LIST.length > 5) {
					CITY_LIST_EDIT = new String[URLConfig.CITY_LIST.length - 4];
					for (int i = 0; i < URLConfig.CITY_LIST.length - 4; i++) {
						CITY_LIST_EDIT[i] = URLConfig.CITY_LIST[i + 4];

					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//Init Country from File
	public static void initializeCountyList(Context context, int txt) {
		try {
			String countryList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = countryList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}

					index++;
				}
			}

			COUNTRY_MAP = new HashMap<>();
			COUNTRY_REVERSE_MAP = new HashMap<>();

			int size = list.size();
			COUNTRY_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				COUNTRY_REVERSE_MAP.put(list2.get(i), list.get(i));
				COUNTRY_MAP.put(list.get(i), list2.get(i));
				COUNTRY_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] FUNCTIONAL_AREA_LIST2;
	public static String[] FUNCTIONAL_AREA_LIST3;
	public static ArrayList<String> FUNCATIONAL_PARENT_LIST = new ArrayList<>();
	public static HashMap<String, String> FUNCTIONA_AREA_MAP = new HashMap<>();
	public static HashMap<String, String> FUNCTIONA_AREA_REVERSE_MAP= new HashMap<>();
	public static HashMap<String, ArrayList<String>> FUNCTIONAL_AREA_CHILD= new HashMap<>();




	//Init from DB
	public static void initializeFunctionalAreaList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getFunctionalAreaList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pid = crs.getString(crs.getColumnIndex("pid"));
					String cid = crs.getString(crs.getColumnIndex("cid"));
					if (!list2.contains(pid))
						list2.add(pid.trim());
					list2.add(cid);
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					String cdesc = crs.getString(crs.getColumnIndex("cdesc"));
					if (!list.contains("#" + pdesc.trim()))
						list.add("#" + pdesc.trim());
					list.add(cdesc);

				}
				crs.close();
			}
			if (FUNCTIONAL_AREA_CHILD.size()==0 && FUNCTIONA_AREA_MAP.size()==0 
					&& FUNCTIONA_AREA_REVERSE_MAP.size()==0 
					&& FUNCATIONAL_PARENT_LIST.size()==0 ) {

				

				int size = list.size();
				String last = "Start";
				ArrayList<String> l = null;
				FUNCTIONAL_AREA_LIST2 = new String[list.size()];
				FUNCTIONAL_AREA_LIST3 = new String[list.size()];

				for (int i = 0; i < size; i++) {
					String temp = list.get(i);
					if (temp.startsWith("#")) {
						// //Log.e("Parent ==> New String",temp);
						if (i == 0)
							FUNCATIONAL_PARENT_LIST.add("None");
						FUNCATIONAL_PARENT_LIST.add(temp.replace("#", "")
								.trim());

						if (l != null)
							FUNCTIONAL_AREA_CHILD.put(last, l);

						l = new ArrayList<>();
						last = temp;
					} else {
						// //Log.e("Child ",temp+" Parent = "+last);
						if(l!=null)
							l.add(temp);
					}
					FUNCTIONA_AREA_REVERSE_MAP.put(list2.get(i), list.get(i));
					FUNCTIONA_AREA_MAP.put(list.get(i), list2.get(i));
					FUNCTIONAL_AREA_LIST2[i] = list.get(i);
					FUNCTIONAL_AREA_LIST3 = FUNCATIONAL_PARENT_LIST
							.toArray(new String[FUNCATIONAL_PARENT_LIST.size()]);

				}

				if (FUNCTIONAL_AREA_CHILD == null) {
					for (String key : FUNCTIONAL_AREA_CHILD.keySet()) {
						String parent = key;
						// //Log.e("Final MAP child","Parent "+parent);
						ArrayList<String> children = FUNCTIONAL_AREA_CHILD
								.get(parent);

						if (children != null) {
							ArrayList<String> child_id = new ArrayList<String>();
							for (int c = 0; c < children.size(); c++) {
								String id = FUNCTIONA_AREA_MAP.get(children
										.get(c));
								child_id.add(id);
							}
							FUNCTIONAL_AREA_CHILD.put(parent, child_id);
						}
					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}


	public static String[] EDUCATIONAL_QUALIFICATION_LIST2;
	public static String[] EDUCATIONAL_QUALIFICATION_LIST3;
	public static ArrayList<Integer> EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST = new ArrayList<>();

	public static ArrayList<Integer> EDUCATIONAL_QUALIFICATION_CHILD_CID_LIST = new ArrayList<>();


    public static HashMap<Integer,ArrayList<Integer>> CHILD_CID_MAP=new HashMap<>();


	public static HashMap<Integer,ArrayList<Integer>> CHILD_SEARCH_CID_MAP=new HashMap<>();


	public static ArrayList<String> EDUCATIONAL_QUALIFICATION_PARENT_LIST = new ArrayList<>();

	public static HashMap<String, String> EDUCATIONAL_QUALIFICATION_MAP = new HashMap<>();
	public static HashMap<String, String> EDUCATIONAL_QUALIFICATION_REVERSE_MAP= new HashMap<>();
	public static HashMap<String, ArrayList<String>> EDUCATIONAL_QUALIFICATION_CHILD= new HashMap<>();
	public static HashMap<Integer, String> EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP= new HashMap<>();
	public static HashMap<Integer, String> EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP= new HashMap<>();


	public static HashMap<String, Integer> EDUCATIONAL_QUALIFICATION_PARENT_MAP= new HashMap<>();





	//Init from DB
	public static void initializeEducationalQualificationList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();

			ArrayList<String> list3 = new ArrayList<>();

			Cursor crs = ShineSqliteDataBase.getQualificationLevelList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pid = crs.getString(crs.getColumnIndex("pid"));
					String cid = crs.getString(crs.getColumnIndex("cid"));
					if (!list2.contains(pid))
						list2.add(pid.trim());
					list2.add(cid);

					// This list for create childMap for qualification
					if (!list3.contains("#" + pid.trim()))
						list3.add("#" + pid.trim());
					list3.add(cid);



					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					String cdesc = crs.getString(crs.getColumnIndex("cdesc"));
					if (!list.contains("#" + pdesc.trim()))
						list.add("#" + pdesc.trim());
					list.add(cdesc);


					EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP.put(Integer.parseInt(pid), pdesc);
					EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP.put(Integer.parseInt(cid), cdesc);
					EDUCATIONAL_QUALIFICATION_PARENT_MAP.put(pdesc, Integer.parseInt(pid));

					if (!EDUCATIONAL_QUALIFICATION_CHILD_CID_LIST.contains(Integer.parseInt(cid)))
						EDUCATIONAL_QUALIFICATION_CHILD_CID_LIST.add(Integer.parseInt(cid));
					if (!EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.contains(Integer.parseInt(pid)))
						EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.add(Integer.parseInt(pid));


				}
				crs.close();
			}
			if (EDUCATIONAL_QUALIFICATION_CHILD.size() == 0 && EDUCATIONAL_QUALIFICATION_MAP.size() == 0
					&& EDUCATIONAL_QUALIFICATION_REVERSE_MAP.size() == 0
					&& EDUCATIONAL_QUALIFICATION_PARENT_LIST.size() == 0) {


				int size = list.size();
				String last = "Start";
				ArrayList<String> l = null;
				EDUCATIONAL_QUALIFICATION_LIST2 = new String[list.size()];
				EDUCATIONAL_QUALIFICATION_LIST3 = new String[list.size()];

				for (int i = 0; i < size; i++) {
					String temp = list.get(i);
					if (temp.startsWith("#")) {
						// //Log.e("Parent ==> New String",temp);
						if (i == 0)
							EDUCATIONAL_QUALIFICATION_PARENT_LIST.add("None");
						EDUCATIONAL_QUALIFICATION_PARENT_LIST.add(temp.replace("#", "")
								.trim());

						if (l != null)
							EDUCATIONAL_QUALIFICATION_CHILD.put(last, l);

						l = new ArrayList<>();
						last = temp;
					} else {
						// //Log.e("Child ",temp+" Parent = "+last);
						if (l != null)
							l.add(temp);
					}
					EDUCATIONAL_QUALIFICATION_REVERSE_MAP.put(list2.get(i), list.get(i));
					EDUCATIONAL_QUALIFICATION_MAP.put(list.get(i), list2.get(i));
					EDUCATIONAL_QUALIFICATION_LIST2[i] = list.get(i);
					EDUCATIONAL_QUALIFICATION_LIST3 = EDUCATIONAL_QUALIFICATION_PARENT_LIST
							.toArray(new String[EDUCATIONAL_QUALIFICATION_PARENT_LIST.size()]);

				}

				if (EDUCATIONAL_QUALIFICATION_CHILD == null) {
					for (String key : EDUCATIONAL_QUALIFICATION_CHILD.keySet()) {
						String parent = key;
						// //Log.e("Final MAP child","Parent "+parent);
						ArrayList<String> children = EDUCATIONAL_QUALIFICATION_CHILD
								.get(parent);

						if (children != null) {
							ArrayList<String> child_id = new ArrayList<String>();
							for (int c = 0; c < children.size(); c++) {
								String id = EDUCATIONAL_QUALIFICATION_MAP.get(children
										.get(c));
								child_id.add(id);
							}
							EDUCATIONAL_QUALIFICATION_CHILD.put(parent, child_id);
						}
					}
				}
			}

            //For creating child_map(int,Arraylist<int>) for nested education
			try {
				ArrayList<Integer> child_array = null;
				for(String s:list3)
                {
                    String parent="";
                    if(s.contains("#"))
                    {
                       parent=s.replace("#","");

                        child_array=new ArrayList<>();

                    }
                    else {
                        if(child_array!=null)
                        child_array.add(Integer.parseInt(s));
                    }
                    if(!TextUtils.isEmpty(parent)&&child_array!=null)
                    CHILD_CID_MAP.put(Integer.parseInt(parent),child_array);
                }
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public static ArrayList<String> KEYWORD_SUGGESTION = new ArrayList<>();
	public static final String KEYWORD_HIT_TAG = "auto-suggestion";
	public static void initializeSuggestion(Context context){

		Type type = new TypeToken<String[]>() {
		}.getType();
//		if(!MyApplication.getInstance().isCached(KEYWORD_SUGGESTION_URL)){
			try {
				String suggestions = getFile(context, R.raw.keywords);
				Gson gson = new Gson();
				String s[] = gson.fromJson(suggestions, type);
				KEYWORD_SUGGESTION = new ArrayList<>();
				Collections.addAll(KEYWORD_SUGGESTION, s);
			}catch (Exception e){
				e.printStackTrace();
			}
//		}
//
//		if(MyApplication.getInstance().isRefreshNeeded(KEYWORD_SUGGESTION_URL)){
//			VolleyNetworkRequest req = new VolleyNetworkRequest(context, new VolleyRequestCompleteListener() {
//				@Override
//				public void OnDataResponse(Object object, String tag) {
//
//					try {
//						String a[] = (String[]) object;
//						KEYWORD_SUGGESTION = new ArrayList<>();
//						Collections.addAll(KEYWORD_SUGGESTION, a);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//
//				@Override
//				public void OnDataResponseError(String error, String tag) {
//
//				}
//			}, KEYWORD_SUGGESTION_URL, type);
//			req.execute(KEYWORD_HIT_TAG);
//		}

	}

	public static ArrayList<String> INSTITUTE_SUGGESTION = new ArrayList<>();
	public static final String INSTITUTE_HIT_TAG = "auto-suggestion-institute";
	public static void initializeInstituteSuggestion(Context context){

		Type type = new TypeToken<JsonElement[]>() {
		}.getType();
//		if(!MyApplication.getInstance().isCached(INSTITUTE_SUGGESTION_URL)){
			try {
				String suggestions = getFile(context, R.raw.institute_file);
				Gson gson = new Gson();
				JsonElement[] s = gson.fromJson(suggestions, type);
				Log.d("debug", Arrays.toString(s));
				INSTITUTE_SUGGESTION = new ArrayList<>();
				for (JsonElement value : s)
					if (value.getAsJsonObject().has("pdesc"))
						INSTITUTE_SUGGESTION.add(value.getAsJsonObject().get("pdesc").getAsString());
			}catch (Exception e){
				e.printStackTrace();
			}
//		}
//
//		if(MyApplication.getInstance().isRefreshNeeded(INSTITUTE_SUGGESTION_URL)){
//			VolleyNetworkRequest req = new VolleyNetworkRequest(context, new VolleyRequestCompleteListener() {
//				@Override
//				public void OnDataResponse(Object object, String tag) {
//
//					try {
//						JsonElement[] a = (JsonElement[]) object;
//						INSTITUTE_SUGGESTION = new ArrayList<>();
//						for (JsonElement value : a)
//							if (value.getAsJsonObject().has("pdesc"))
//								INSTITUTE_SUGGESTION.add(value.getAsJsonObject().get("pdesc").getAsString());
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//
//				@Override
//				public void OnDataResponseError(String error, String tag) {
//
//				}
//			}, INSTITUTE_SUGGESTION_URL, type);
//			req.execute(INSTITUTE_HIT_TAG);
//		}


	}

	public static ArrayList<String> SKILL_SUGGESTION = new ArrayList<>();
	public static final String SKILL_HIT_TAG = "auto-suggestion-skill";
	public static void initializeSkillSuggestion(Context context){

		Type type = new TypeToken<JsonElement[]>() {
		}.getType();
//		if(!MyApplication.getInstance().isCached(SKILL_SUGGESTION_URL)){
			try {
				String suggestions = getFile(context, R.raw.skill_file);
				Gson gson = new Gson();
				JsonElement[] s = gson.fromJson(suggestions, type);
				Log.d("debug", Arrays.toString(s));
				SKILL_SUGGESTION = new ArrayList<>();
				for (JsonElement value : s)
					if (value.getAsJsonObject().has("pdesc"))
						SKILL_SUGGESTION.add(value.getAsJsonObject().get("pdesc").getAsString());
			}catch (Exception e){
				e.printStackTrace();
			}
//		}
//
//		if(MyApplication.getInstance().isRefreshNeeded(SKILL_SUGGESTION_URL)){
//			VolleyNetworkRequest req = new VolleyNetworkRequest(context, new VolleyRequestCompleteListener() {
//				@Override
//				public void OnDataResponse(Object object, String tag) {
//
//					try {
//						JsonElement[] a = (JsonElement[]) object;
//						SKILL_SUGGESTION = new ArrayList<>();
//						for (JsonElement value : a)
//							if (value.getAsJsonObject().has("pdesc"))
//								SKILL_SUGGESTION.add(value.getAsJsonObject().get("pdesc").getAsString());
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//
//				@Override
//				public void OnDataResponseError(String error, String tag) {
//
//				}
//			}, SKILL_SUGGESTION_URL, type);
//			req.execute(SKILL_HIT_TAG);
//		}

	}


	public static String[] MONTH_NAME_LIST;

	public static HashMap<String, String> MONTH_NAME_MAP;
	public static HashMap<String, String> MONTH_NAME_REVERSE_MAP;

	//Init From File
	public static void initializeMonthNameList(Context context, int txt) {
		try {
			String funtionalList = getFile(context, txt);
			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = funtionalList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}

					index++;
				}
			}
			MONTH_NAME_MAP = new HashMap<>();
			MONTH_NAME_REVERSE_MAP = new HashMap<>();

			int size = list.size();
			MONTH_NAME_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				MONTH_NAME_REVERSE_MAP.put(list2.get(i), list.get(i));
				MONTH_NAME_MAP.put(list.get(i), list2.get(i));
				MONTH_NAME_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] INDUSTRY_LIST2;
	public static String[] INDUSTRY_LIST3;

	public static HashMap<String, String> INDUSTRY_MAP= new HashMap<>();
	public static HashMap<String, String> INDUSTRY_REVERSE_MAP = new HashMap<>();

	//Init from DB
	public static void initializeIndustryList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getIndustryList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);

				}
				crs.close();
			}

			if (INDUSTRY_MAP.size()==0 && INDUSTRY_REVERSE_MAP.size()==0) {


				int size = list.size();
				INDUSTRY_LIST2 = new String[list.size()];
				for (int i = 0; i < size; i++) {
					INDUSTRY_MAP.put(list.get(i), list2.get(i));
					INDUSTRY_REVERSE_MAP.put(list2.get(i), list.get(i));
					INDUSTRY_LIST2[i] = list.get(i);
				}

				if (INDUSTRY_LIST2.length > 2)
					INDUSTRY_LIST3 = new String[URLConfig.INDUSTRY_LIST2.length - 1];
				for (int i = 0; i < URLConfig.INDUSTRY_LIST2.length - 1; i++) {
					INDUSTRY_LIST3[i] = URLConfig.INDUSTRY_LIST2[i + 1];

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] EXPERIENCE_LIST2;
	public static String[] WORK_EXPERIENCE_LIST2;



	public static HashMap<String, String> EXPERIENCE_MAP = new HashMap<>();
	public static HashMap<String, String> EXPERIENCE_REVERSE_MAP = new HashMap<>();

	//Init from DB
	public static void initializeExperienceLevelList(Context context) {
		// new Lookups changes

		try {
			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = MyApplication.getDataBase().getExpLevelList();

			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);
				}
				crs.close();
			}
			if (EXPERIENCE_MAP.size()==0 && EXPERIENCE_REVERSE_MAP.size()==0
					) {

				int size = list.size();

				if (size > 0) {
					EXPERIENCE_LIST2 = new String[list.size()];


					WORK_EXPERIENCE_LIST2 = new String[list.size() - 1];
					int j = 0;
					for (int i = 0; i < size; i++) {

						EXPERIENCE_REVERSE_MAP.put(list2.get(i), list.get(i));
						EXPERIENCE_MAP.put(list.get(i), list2.get(i));
						EXPERIENCE_LIST2[i] = list.get(i);


                        if (i != 1) {
							WORK_EXPERIENCE_LIST2[j] = list.get(i);
							j++;
						}

					}


				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] EXPERIENCE_MONTHS_LIST;

	public static HashMap<String, String> EXPERIENCE_MONTHS_MAP;
	public static HashMap<String, String> EXPERIENCE_MONTHS_REVERSE_MAP;

	//Init from File
	public static void initializeExperienceMonthsLevelList(Context context,
			int txt) {
		try {

			String expMonthsList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = expMonthsList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}

					index++;
				}
			}

			EXPERIENCE_MONTHS_MAP = new HashMap<>();
			EXPERIENCE_MONTHS_REVERSE_MAP = new HashMap<>();

			int size = list.size();
			EXPERIENCE_MONTHS_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				EXPERIENCE_MONTHS_REVERSE_MAP.put(list2.get(i), list.get(i));
				EXPERIENCE_MONTHS_MAP.put(list.get(i), list2.get(i));
				EXPERIENCE_MONTHS_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] ANNUAL_SALARY_LIST;

	public static HashMap<String, String> ANNUAL_SALARY_MAP= new HashMap<>();
	public static HashMap<String, String> ANNUAL_SALARY_REVRSE_MAP= new HashMap<>();

	//Init from DB
	public static void initializeSalaryList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getAnnualSalaryList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);
				}
				crs.close();
			}

			if (ANNUAL_SALARY_MAP.size()==0 && ANNUAL_SALARY_REVRSE_MAP.size()==0) {

				int size = list.size();
				ANNUAL_SALARY_LIST = new String[list.size()];
				for (int i = 0; i < size; i++) {
					ANNUAL_SALARY_REVRSE_MAP.put(list2.get(i), list.get(i));

					ANNUAL_SALARY_MAP.put(list.get(i), list2.get(i));
					ANNUAL_SALARY_LIST[i] = list.get(i);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] ANNUAL_SALARYL_LIST;

	public static HashMap<String, String> ANNUAL_SALARYL_MAP;
	public static HashMap<String, String> ANNUAL_SALARYL_REVRSE_MAP;

	//Init from File
	public static void initializeSalaryLList(Context context, int txt) {
		try {
			String salaryList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = salaryList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}
					index++;
				}
			}

			ANNUAL_SALARYL_MAP = new HashMap<>();
			ANNUAL_SALARYL_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			ANNUAL_SALARYL_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				ANNUAL_SALARYL_REVRSE_MAP.put(list2.get(i), list.get(i));

				ANNUAL_SALARYL_MAP.put(list.get(i), list2.get(i));
				ANNUAL_SALARYL_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] ANNUAL_SALARYT_LIST;

	public static HashMap<String, String> ANNUAL_SALARYT_MAP;
	public static HashMap<String, String> ANNUAL_SALARYT_REVRSE_MAP;

	//Init from File
	public static void initializeSalaryTList(Context context, int txt) {
		try {
			String salaryList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = salaryList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}
					index++;
				}
			}

			ANNUAL_SALARYT_MAP = new HashMap<>();
			ANNUAL_SALARYT_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			ANNUAL_SALARYT_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				ANNUAL_SALARYT_REVRSE_MAP.put(list2.get(i), list.get(i));

				ANNUAL_SALARYT_MAP.put(list.get(i), list2.get(i));
				ANNUAL_SALARYT_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] TEAM_SIZE_LIST;

	public static HashMap<String, String> TEAM_SIZE_MAP = new HashMap<>();
	public static HashMap<String, String> TEAM_SIZE_REVRSE_MAP = new HashMap<>();

	//Init from DB
	public static void initializeTeamList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getTeamSizeManagedList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);
				}
				crs.close();
			}

			if (TEAM_SIZE_MAP.size()==0 && TEAM_SIZE_REVRSE_MAP.size()==0) {
				
				int size = list.size();
				TEAM_SIZE_LIST = new String[list.size()];
				for (int i = 0; i < size; i++) {
					TEAM_SIZE_REVRSE_MAP.put(list2.get(i), list.get(i));
					TEAM_SIZE_MAP.put(list.get(i), list2.get(i));
					TEAM_SIZE_LIST[i] = list.get(i);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] NOTICE_PERIOD_LIST;

	public static HashMap<String, String> NOTICE_PERIOD_MAP;
	public static HashMap<String, String> NOTICE_PERIOD_REVRSE_MAP;

	//Init from File
	public static void initializeNoticePeriodList(Context context, int txt) {
		try {
			String teamList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = teamList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}

					index++;
				}
			}

			NOTICE_PERIOD_MAP = new HashMap<>();
			NOTICE_PERIOD_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			NOTICE_PERIOD_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				NOTICE_PERIOD_REVRSE_MAP.put(list2.get(i), list.get(i));
				NOTICE_PERIOD_MAP.put(list.get(i), list2.get(i));
				NOTICE_PERIOD_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
//
//	public static String[] QUALIFICATION_LIST;
//
//	public static HashMap<String, String> QUALIFICATIONE_MAP = new HashMap<>();
//	public static HashMap<String, String> QUALIFICATION_REVRSE_MAP = new HashMap<>();
//
//	//Init from DB
//	public static void initializeQualificationList(Context context) {
//		try {
//
//			ArrayList<String> list = new ArrayList<>();
//			ArrayList<String> list2 = new ArrayList<>();
//			Cursor crs = ShineSqliteDataBase
//					.getQualificationLevelList();
//			if (crs.getCount() > 0) {
//				while (crs.moveToNext()) {
//					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
//					list.add(pdesc.trim());
//					String pid = crs.getString(crs.getColumnIndex("pid"));
//					list2.add(pid);
//				}
//				crs.close();
//			}
//
//
//			if (QUALIFICATIONE_MAP.size()==0 && QUALIFICATION_REVRSE_MAP.size()==0) {
//
//				int size = list.size();
//				QUALIFICATION_LIST = new String[list.size()];
//				for (int i = 0; i < size; i++) {
//
//					QUALIFICATION_REVRSE_MAP.put(list2.get(i), list.get(i));
//					QUALIFICATIONE_MAP.put(list.get(i), list2.get(i));
//					QUALIFICATION_LIST[i] = list.get(i);
//				}
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

//	public static String[] QUALI_STREAM_LIST;

//	public static HashMap<String, String> QUALI_STREAM_MAP;
//	public static HashMap<String, String> QUALI_STREAM_REVRSE_MAP;

	//Init from File
//	public static void initializeQualiStreamList(Context context, int txt) {
//		try {
//			String SList = getFile(context, txt);
//			ArrayList<String> list = new ArrayList<>();
//			ArrayList<String> list2 = new ArrayList<>();
//			String[] str = SList.split(",");
//			for (String s : str) {
//				String[] str2 = s.split(":");
//				int index = 0;
//				for (String s1 : str2) {
//					if (index == 1) {
//						list.add(s1.trim());
//					} else {
//						list2.add(s1.trim());
//					}
//
//					index++;
//				}
//			}
//
//			QUALI_STREAM_MAP = new HashMap<>();
//			QUALI_STREAM_REVRSE_MAP = new HashMap<>();
//
//			int size = list.size();
//			QUALI_STREAM_LIST = new String[list.size()];
//			for (int i = 0; i < size; i++) {
//				QUALI_STREAM_REVRSE_MAP.put(list2.get(i), list.get(i));
//				QUALI_STREAM_MAP.put(list.get(i), list2.get(i));
//				QUALI_STREAM_LIST[i] = list.get(i);
//			}
//		} catch (Exception e) { // TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	public static String[] PASSOUT_YEAR_LIST;

	public static HashMap<String, String> PASSOUT_YEAR_MAP;
	public static HashMap<String, String> PASSOUT_YEAR_REVRSE_MAP;

	//Init by self
	public static void initializePassoutYearList() {
		try {
			PASSOUT_YEAR_MAP = new HashMap<>();
			PASSOUT_YEAR_REVRSE_MAP = new HashMap<>();
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR)+5;
			if(year>1970) {
				int count = year - 1969;
				PASSOUT_YEAR_LIST = new String[count];
				for (int i = 0, j= year; i < count; i++, j--) {
					String yr = j+"";
					PASSOUT_YEAR_REVRSE_MAP.put(yr, yr);
					PASSOUT_YEAR_MAP.put(yr, yr);
					PASSOUT_YEAR_LIST[i] = yr;
				}
			}else{
				PASSOUT_YEAR_LIST = new String[1];
				PASSOUT_YEAR_LIST[0] = "1970";
				PASSOUT_YEAR_REVRSE_MAP.put("1970", "1970");
				PASSOUT_YEAR_MAP.put("1970", "1970");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] INSTITUTE_LIST;

	public static HashMap<String, String> INSTITUTE_MAP= new HashMap<>();
	public static HashMap<String, String> INSTITUTE_REVRSE_MAP= new HashMap<>();

	//Init from DB
	public static void initializeInstituteList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase
					.getEducationInstituteList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);
				}
				crs.close();
			}

			if (INSTITUTE_MAP.size()==0 && INSTITUTE_REVRSE_MAP.size()==0) {

				int size = list.size();
				INSTITUTE_LIST = new String[list.size()];
				for (int i = 0; i < size; i++) {
					if (i == 0) {
						INSTITUTE_MAP.put(list.get(i), "");

					} else {
						INSTITUTE_REVRSE_MAP.put(list2.get(i), list.get(i));
						INSTITUTE_MAP.put(list.get(i), list2.get(i));
					}
					INSTITUTE_LIST[i] = list.get(i);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] COURSE_TYPE_LIST;

	public static HashMap<String, String> COURSE_TYPE_MAP= new HashMap<>();
	public static HashMap<String, String> COURSE_TYPE_REVRSE_MAP = new HashMap<>();

	//Init from DB
	public static void initializCourseTypeList(Context context) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			Cursor crs = ShineSqliteDataBase.getCourseTypeList();
			if (crs.getCount() > 0) {
				while (crs.moveToNext()) {
					String pdesc = crs.getString(crs.getColumnIndex("pdesc"));
					list.add(pdesc.trim());
					String pid = crs.getString(crs.getColumnIndex("pid"));
					list2.add(pid);
				}
				crs.close();
			}

			if (COURSE_TYPE_MAP.size()==0 & COUNTRY_REVERSE_MAP.size()==0
					&& COURSE_TYPE_REVRSE_MAP.size()==0) {


				int size = list.size();
				COURSE_TYPE_LIST = new String[list.size()];
				for (int i = 0; i < size; i++) {

					COURSE_TYPE_REVRSE_MAP.put(list2.get(i), list.get(i));
					COURSE_TYPE_MAP.put(list.get(i), list2.get(i));
					COURSE_TYPE_LIST[i] = list.get(i);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String[] SKILLS_LIST;

    public static String[] newSkillList()
	{
		String[] new_list = URLConfig.SKILLS_LIST;
		final List<String> list =  new ArrayList<String>();
		Collections.addAll(list, new_list);
		list.remove(0);
		new_list= list.toArray(new String[list.size()]);
		return  new_list;
	}

	public static HashMap<String, String> SKILLS_MAP =new HashMap<>();
	public static HashMap<String, String> SKILLS_REVRSE_MAP= new HashMap<>();

	//Init from File
	public static void initializSkillsList(Context context, int txt) {
		try {


			String SList = getFile(context, txt);

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			String[] str = SList.split(",");
			for (String s : str) {
				String[] str2 = s.split(":");
				int index = 0;
				for (String s1 : str2) {
					if (index == 1) {
						list.add(s1.trim());
					} else {
						list2.add(s1.trim());
					}

					index++;
				}
			}

			SKILLS_MAP = new HashMap<>();
			SKILLS_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			SKILLS_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {

				SKILLS_REVRSE_MAP.put(list2.get(i), list.get(i));
				SKILLS_MAP.put(list.get(i), list2.get(i));
				SKILLS_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getFile(Context context, int id) {
		String text = "";
		ByteArrayOutputStream byteArrayOutputStream=null;
		try {
			Resources res = context.getResources();
			InputStream is = res.openRawResource(id);
			 byteArrayOutputStream = new ByteArrayOutputStream();

			//int size = is.available();
			//byte[] buffer = new byte[size];
			int i=is.read();
			while(i!=-1)
			{
				byteArrayOutputStream.write(i);
				i = is.read();
			}
			is.close();
			//text = new String(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return byteArrayOutputStream.toString();
	}

	//Init from DB
	public static void initializeCityList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();

			for (HashMap<String, String> map : arrayList) {
				String pid = map.get("pid");
				String cid = map.get("cid");
				if (!list2.contains(pid))
					list2.add(pid.trim());
				list2.add(cid);
				String pdesc = map.get("pdesc");
				String cdesc = map.get("cdesc");
				if (!list.contains("#" + pdesc.trim()))
					list.add("#" + pdesc.trim());
				list.add(cdesc);
			}

			CITY_MAP = new HashMap<>();
			CITY_REVERSE_MAP = new HashMap<>();

			int size = list.size();
			CITY_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				CITY_REVERSE_MAP.put(list2.get(i), list.get(i));
				CITY_MAP.put(list.get(i), list2.get(i));
				CITY_LIST[i] = list.get(i);

				// //system.out.println("Key::::" + list2.get(i) + " Value:::" +
				// list.get(i));
			}
			if (CITY_LIST != null && CITY_LIST.length > 5) {
				CITY_LIST_EDIT = new String[URLConfig.CITY_LIST.length - 4];
				for (int i = 0; i < URLConfig.CITY_LIST.length - 4; i++) {
					CITY_LIST_EDIT[i] = URLConfig.CITY_LIST[i + 4];

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//Init from DB
	public static void initializeFunctionalAreaList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pid = map.get("pid");
				String cid = map.get("cid");
				if (!list2.contains(pid))
					list2.add(pid.trim());
				list2.add(cid);
				String pdesc = map.get("pdesc");
				String cdesc = map.get("cdesc");
				if (!list.contains("#" + pdesc.trim()))
					list.add("#" + pdesc.trim());
				list.add(cdesc);
			}
			FUNCTIONAL_AREA_CHILD = new HashMap<>();
			FUNCTIONA_AREA_MAP = new HashMap<>();
			FUNCTIONA_AREA_REVERSE_MAP = new HashMap<>();
			FUNCATIONAL_PARENT_LIST = new ArrayList<>();

			int size = list.size();
			String last = "Start";
			ArrayList<String> l = null;
			FUNCTIONAL_AREA_LIST2 = new String[list.size()];
			FUNCTIONAL_AREA_LIST3 = new String[list.size()];

			for (int i = 0; i < size; i++) {
				String temp = list.get(i);
				if (temp.startsWith("#")) {
					// //Log.e("Parent ==> New String",temp);
					if (i == 0)
						FUNCATIONAL_PARENT_LIST.add("None");
					FUNCATIONAL_PARENT_LIST.add(temp.replace("#", "").trim());

					if (l != null)
						FUNCTIONAL_AREA_CHILD.put(last, l);

					l = new ArrayList<String>();
					last = temp;
				} else {
					// //Log.e("Child ",temp+" Parent = "+last);
					if(l!=null)
						l.add(temp);
				}
				FUNCTIONA_AREA_REVERSE_MAP.put(list2.get(i), list.get(i));
				FUNCTIONA_AREA_MAP.put(list.get(i), list2.get(i));
				FUNCTIONAL_AREA_LIST2[i] = list.get(i);
				FUNCTIONAL_AREA_LIST3 = FUNCATIONAL_PARENT_LIST
						.toArray(new String[FUNCATIONAL_PARENT_LIST.size()]);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String key : FUNCTIONAL_AREA_CHILD.keySet()) {
			String parent = key;
			// //Log.e("Final MAP child","Parent "+parent);
			ArrayList<String> children = FUNCTIONAL_AREA_CHILD.get(parent);

			if (children != null) {
				ArrayList<String> child_id = new ArrayList<String>();
				for (int c = 0; c < children.size(); c++) {
					String id = FUNCTIONA_AREA_MAP.get(children.get(c));
					child_id.add(id);
				}
				FUNCTIONAL_AREA_CHILD.put(parent, child_id);
			}
		}

	}

	public static void initializeIndustryList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();

			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}

			INDUSTRY_MAP = new HashMap<>();
			INDUSTRY_REVERSE_MAP = new HashMap<>();

			int size = list.size();
			INDUSTRY_LIST2 = new String[list.size()];
			for (int i = 0; i < size; i++) {
				INDUSTRY_MAP.put(list.get(i), list2.get(i));
				INDUSTRY_REVERSE_MAP.put(list2.get(i), list.get(i));
				INDUSTRY_LIST2[i] = list.get(i);
			}

			if (INDUSTRY_LIST2.length > 2)
				INDUSTRY_LIST3 = new String[URLConfig.INDUSTRY_LIST2.length - 1];
			for (int i = 0; i < URLConfig.INDUSTRY_LIST2.length - 1; i++) {
				INDUSTRY_LIST3[i] = URLConfig.INDUSTRY_LIST2[i + 1];

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initializeExperienceLevelList(
			ArrayList<HashMap<String, String>> arrayList) {
		// new Lookups changes

		try {
			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}
			EXPERIENCE_MAP = new HashMap<>();
			EXPERIENCE_REVERSE_MAP = new HashMap<>();
			int size = list.size();
			if (size > 0) {
				EXPERIENCE_LIST2 = new String[list.size()];

				WORK_EXPERIENCE_LIST2 = new String[list.size() - 1];
				int j = 0;
				for (int i = 0; i < size; i++) {
					EXPERIENCE_REVERSE_MAP.put(list2.get(i), list.get(i));
					EXPERIENCE_MAP.put(list.get(i), list2.get(i));
					EXPERIENCE_LIST2[i] = list.get(i);
					if (i != 1) {
						WORK_EXPERIENCE_LIST2[j] = list.get(i);
						j++;
					}

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initializeSalaryList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}
			ANNUAL_SALARY_MAP = new HashMap<>();
			ANNUAL_SALARY_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			ANNUAL_SALARY_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				ANNUAL_SALARY_REVRSE_MAP.put(list2.get(i), list.get(i));

				ANNUAL_SALARY_MAP.put(list.get(i), list2.get(i));
				ANNUAL_SALARY_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initializeTeamList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}
			TEAM_SIZE_MAP = new HashMap<>();
			TEAM_SIZE_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			TEAM_SIZE_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				TEAM_SIZE_REVRSE_MAP.put(list2.get(i), list.get(i));
				TEAM_SIZE_MAP.put(list.get(i), list2.get(i));
				TEAM_SIZE_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public static void initializeQualificationList(
			ArrayList<HashMap<String, String>> arrayList){
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();

			ArrayList<Integer> pid_list=new ArrayList<>();
			ArrayList<Integer> cid_list=new ArrayList<>();

			for (HashMap<String, String> map : arrayList) {
				String pid = map.get("pid");
				String cid = map.get("cid");
				if (!list2.contains(pid))
					list2.add(pid.trim());
				list2.add(cid);
				String pdesc = map.get("pdesc");
				String cdesc = map.get("cdesc");
				if (!list.contains("#" + pdesc.trim()))
					list.add("#" + pdesc.trim());
				list.add(cdesc);

				if(!EDUCATIONAL_QUALIFICATION_CHILD_CID_LIST.contains(Integer.parseInt(cid)))
				EDUCATIONAL_QUALIFICATION_CHILD_CID_LIST.add(Integer.parseInt(cid));
				if(!EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.contains(Integer.parseInt(pid)))
				EDUCATIONAL_QUALIFICATION_PARENT_PID_LIST.add(Integer.parseInt(pid));

				EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP.put(Integer.parseInt(pid),pdesc);
				EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP.put(Integer.parseInt(cid),cdesc);

			}



            EDUCATIONAL_QUALIFICATION_CHILD = new HashMap<>();
            EDUCATIONAL_QUALIFICATION_MAP = new HashMap<>();
            EDUCATIONAL_QUALIFICATION_REVERSE_MAP = new HashMap<>();
            EDUCATIONAL_QUALIFICATION_PARENT_LIST = new ArrayList<>();

			int size = list.size();
			String last = "Start";
			ArrayList<String> l = null;
            EDUCATIONAL_QUALIFICATION_LIST2 = new String[list.size()];
            EDUCATIONAL_QUALIFICATION_LIST3 = new String[list.size()];

			for (int i = 0; i < size; i++) {
				String temp = list.get(i);
				if (temp.startsWith("#")) {
					// //Log.e("Parent ==> New String",temp);
					if (i == 0)
                        EDUCATIONAL_QUALIFICATION_PARENT_LIST.add("None");
                    EDUCATIONAL_QUALIFICATION_PARENT_LIST.add(temp.replace("#", "").trim());

					if (l != null)
                        EDUCATIONAL_QUALIFICATION_CHILD.put(last, l);

					l = new ArrayList<String>();
					last = temp;
				} else {
					// //Log.e("Child ",temp+" Parent = "+last);
					if(l!=null)
						l.add(temp);
				}
                EDUCATIONAL_QUALIFICATION_REVERSE_MAP.put(list2.get(i), list.get(i));
                EDUCATIONAL_QUALIFICATION_MAP.put(list.get(i), list2.get(i));
                EDUCATIONAL_QUALIFICATION_LIST2[i] = list.get(i);
                EDUCATIONAL_QUALIFICATION_LIST3 = EDUCATIONAL_QUALIFICATION_PARENT_LIST
						.toArray(new String[EDUCATIONAL_QUALIFICATION_PARENT_LIST.size()]);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (String key : EDUCATIONAL_QUALIFICATION_CHILD.keySet()) {
			String parent = key;
			// //Log.e("Final MAP child","Parent "+parent);
			ArrayList<String> children = EDUCATIONAL_QUALIFICATION_CHILD.get(parent);

			if (children != null) {
				ArrayList<String> child_id = new ArrayList<String>();
				for (int c = 0; c < children.size(); c++) {
					String id = EDUCATIONAL_QUALIFICATION_MAP.get(children.get(c));
					child_id.add(id);
				}
                EDUCATIONAL_QUALIFICATION_CHILD.put(parent, child_id);
			}
		}

	}

//	public static void initializeQualificationList(
//			ArrayList<HashMap<String, String>> arrayList) {
//		try {
//
//			ArrayList<String> list = new ArrayList<>();
//			ArrayList<String> list2 = new ArrayList<>();
//			for (HashMap<String, String> map : arrayList) {
//				String pdesc = map.get("pdesc");
//				list.add(pdesc.trim());
//				String pid = map.get("pid");
//				list2.add(pid);
//			}
//			QUALIFICATIONE_MAP = new HashMap<>();
//			QUALIFICATION_REVRSE_MAP = new HashMap<>();
//
//			int size = list.size();
//			QUALIFICATION_LIST = new String[list.size()];
//			for (int i = 0; i < size; i++) {
//				QUALIFICATION_REVRSE_MAP.put(list2.get(i), list.get(i));
//				QUALIFICATIONE_MAP.put(list.get(i), list2.get(i));
//				QUALIFICATION_LIST[i] = list.get(i);
//			}
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	public static void initializeInstituteList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}
			INSTITUTE_MAP = new HashMap<>();
			INSTITUTE_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			INSTITUTE_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {
				if (i == 0) {
					INSTITUTE_MAP.put(list.get(i), "");

				} else {
					INSTITUTE_REVRSE_MAP.put(list2.get(i), list.get(i));
					INSTITUTE_MAP.put(list.get(i), list2.get(i));
				}
				INSTITUTE_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initializCourseTypeList(
			ArrayList<HashMap<String, String>> arrayList) {
		try {

			ArrayList<String> list = new ArrayList<>();
			ArrayList<String> list2 = new ArrayList<>();
			for (HashMap<String, String> map : arrayList) {
				String pdesc = map.get("pdesc");
				list.add(pdesc.trim());
				String pid = map.get("pid");
				list2.add(pid);
			}

			COURSE_TYPE_MAP = new HashMap<>();
			COURSE_TYPE_REVRSE_MAP = new HashMap<>();

			int size = list.size();
			COURSE_TYPE_LIST = new String[list.size()];
			for (int i = 0; i < size; i++) {

				COURSE_TYPE_REVRSE_MAP.put(list2.get(i), list.get(i));
				COURSE_TYPE_MAP.put(list.get(i), list2.get(i));
				COURSE_TYPE_LIST[i] = list.get(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
