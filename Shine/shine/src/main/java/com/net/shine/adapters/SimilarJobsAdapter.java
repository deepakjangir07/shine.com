package com.net.shine.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.models.SimpleSearchModel;

import java.util.ArrayList;

public class SimilarJobsAdapter extends JobsListRecyclerAdapter {


    public SimilarJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list,"SimilarJobs");
    }

    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);




        if(getItemViewType(position)==JOB_ITEM)
        {
            viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(URLConfig.INDEX, viewHolder.getAdapterPosition());
                    bundle.putString(URLConfig.JOB_TYPE, "search_job");
                    bundle.putSerializable(JobDetailParentFrg.JOB_LIST, mList);
                    BaseFragment fragment = new JobDetailParentFrg();
                    fragment.setArguments(bundle);
                    mActivity.setFragment(fragment);


                }
            });

        }







    }



}
