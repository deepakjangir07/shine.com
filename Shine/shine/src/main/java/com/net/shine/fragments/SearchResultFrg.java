package com.net.shine.fragments;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.DiscoverResultsJobsAdapter;
import com.net.shine.adapters.JobListRecyclerAdapterForAds;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.adapters.SearchResultJobsAdapter;
import com.net.shine.adapters.SimilarJobsAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.fragments.tabs.MainTabFrg;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.FacetsModel;
import com.net.shine.models.SetFacetsModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.CustomAlert;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.EndlessRecyclerOnScrollListener;
import com.net.shine.utils.FacetsUtils;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.EasyTracker;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class SearchResultFrg extends BaseFragment implements
        VolleyRequestCompleteListener, GetConnectionListener, SwipeRefreshLayout.OnRefreshListener {


    private SwipeRefreshLayout swipeContainer;
    public SimpleSearchVO svo;
    private RecyclerView gridView;
    private JobsListRecyclerAdapter adapter;
    private JobListRecyclerAdapterForAds adAdapter;
    private View errorLayout, LoadingCmp, parentView;
    boolean loadMore = false, fromFacets = false, expired_job = false, FROM_NEW_ADD_CJA = false, fromRefresh = false;
    private int totalJobs;
    private FacetsModel facetModel;
    private ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
    private String jobType = "search_job", co_names = "", nextUrl = "";
    private UserStatusModel userProfileVO;
    public boolean fromDiscoverConnect = false;
    private DiscoverModel.Company dfm;
    private SetFacetsModel setFacetsModel;
    private HashMap<String, DiscoverModel.Company> friend_list = new HashMap<>();
    private Set<String> unique_companies = new HashSet<>();
    private GoogleApiClient client;
    private LinearLayout job_count_layout;
    private TextView jobs_count;
    private float dpHeight, dpWidth;
    private TextView save_job_alert, saved_job_alert;
    private Dialog alertDialog;


    public static SearchResultFrg newInstance(Bundle args) {
        SearchResultFrg fragment = new SearchResultFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Log.d("SearchResultFrg ", "onCreateView  Called ");

        mActivity.hideBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);

        DisplayMetrics displayMetrics = mActivity.getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels;
        dpWidth = displayMetrics.widthPixels;

        Log.d("screen_size", dpWidth + "and" + dpHeight);

        parentView = inflater.inflate(R.layout.search_result_gridview, container, false);
        try {

            swipeContainer = (SwipeRefreshLayout) parentView.findViewById(R.id.swipeContainer);
            ShineCommons.setSwipeRefeshColor(swipeContainer);
            errorLayout = parentView.findViewById(R.id.error_layout);
            errorLayout.setVisibility(View.GONE);
            swipeContainer.setOnRefreshListener(this);
            job_count_layout = (LinearLayout) parentView.findViewById(R.id.jobs_count_layout);
            jobs_count = (TextView) parentView.findViewById(R.id.jobs_count_textview);
            userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

            svo = new SimpleSearchVO();
            Bundle bundle = getArguments();
            fromFacets = bundle.getBoolean("FROM_FACETS");

            try {

                if (bundle.containsKey("fromExpiredJob")) {
                    expired_job = bundle.getBoolean("fromExpiredJob");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            fromDiscoverConnect = bundle.getBoolean("fromDiscoverConnect");
            dfm = (DiscoverModel.Company) bundle.getSerializable("companyModel");

            if (fromFacets) {
                try {
                    setFacetsModel = (SetFacetsModel) bundle
                            .getSerializable(URLConfig.SEARCH_VO_WITH_FACETS);
                    facetModel = (FacetsModel) bundle.getSerializable("LASTFACET");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (fromDiscoverConnect) {
                svo = (SimpleSearchVO) bundle
                        .getSerializable("companyIdObject");

            } else {
                svo = (SimpleSearchVO) bundle
                        .getSerializable(URLConfig.SEARCH_VO_KEY);

            }

            Log.d("CJA","result "+svo);


            if (fromDiscoverConnect || (svo != null && svo.isFromJD() && dfm != null)) {
                parentView.findViewById(R.id.save_job_alert).setVisibility(
                        View.GONE);


                TextView number_of_connections = (TextView) parentView
                        .findViewById(R.id.frndsCount);
                if (dfm != null) {
                    parentView.findViewById(R.id.connectionsContainer_discover)
                            .setVisibility(View.VISIBLE);
                    if (dfm.data.friends.size() > 1) {

                        number_of_connections.setText(Html
                                .fromHtml("<font color='#333333'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connections "
                                        + "<font color='#333333'>work here</font>"));

                    } else {
                        number_of_connections.setText(Html
                                .fromHtml("<font color='#2c84cc'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> "
                                        + "<font color='#333333'> works here</font>"));

                    }
                }

            }


            gridView = (RecyclerView) parentView.findViewById(R.id.grid_view);
            gridView.setLayoutManager(new LinearLayoutManager(mActivity));
            EndlessRecyclerOnScrollListener mScrollListener = new EndlessRecyclerOnScrollListener() {

                @Override
                public void onLoadMore(int currentPage) {

                    if (!TextUtils.isEmpty(nextUrl) && !loadMore) {
                        loadMore = true;
                        downloadsearchresultJobs(nextUrl, parentView.findViewById(R.id.loading_grid), true, false);
                    }
                }
            };

            gridView.addOnScrollListener(mScrollListener);

            if (svo != null && svo.isFromSimilarJobs()) {
                adapter = new SimilarJobsAdapter(mActivity, jobList);
                gridView.setAdapter(adapter);

            } else if (fromDiscoverConnect) {

                adapter = new DiscoverResultsJobsAdapter(mActivity, jobList);
                gridView.setAdapter(adapter);

            } else if (svo.isFromJD()) {
                if (dfm == null) {
                    adAdapter = new SearchResultJobsAdapter(mActivity, jobList);
                    gridView.setAdapter(adAdapter);

                } else {
                    adapter = new DiscoverResultsJobsAdapter(mActivity, jobList);
                    gridView.setAdapter(adapter);

                }
            } else {


                adAdapter = new SearchResultJobsAdapter(mActivity, jobList);
                gridView.setAdapter(adAdapter);

            }


            parentView.findViewById(R.id.connectionsContainer_discover)
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            ShowDialogDiscoverFriendsFragment showDialogDiscoverFriendsFragment = ShowDialogDiscoverFriendsFragment.newInstance(dfm);
                            mActivity.setFragment(showDialogDiscoverFriendsFragment);


                        }
                    });


            downloadsearchresultJobs(getFirstUrl(),
                    parentView.findViewById(R.id.loading_cmp), false, false);


            save_job_alert = (TextView) parentView.findViewById(R.id.save_job_alert);
            saved_job_alert = (TextView) parentView.findViewById(R.id.saved_job_alert);
            save_job_alert.setVisibility(View.VISIBLE);
            saved_job_alert.setVisibility(View.GONE);
            save_job_alert.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(svo.getKeyword())) {
                        alertDialog = DialogUtils.AlertSaveDialog(mActivity);

                        Log.d(">>>>>MAnish","inside_click");


                        if (ShineSharedPreferences.getUserInfo(mActivity) != null)
                            alertDialog.findViewById(R.id.email).setVisibility(View.GONE);

                        alertDialog.findViewById(R.id.close)
                                .setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            DialogUtils.dialogDismiss(alertDialog);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                        final EditText searchKey = (EditText) alertDialog.findViewById(R.id.name);
                        final EditText emailText = (EditText) alertDialog.findViewById(R.id.email);
                        String keyword = svo.getKeyword();
                        if (svo.getKeyword().endsWith(",")) {

                            keyword = svo.getKeyword().substring(0, svo.getKeyword().length() - 1);
                        }
                        searchKey.setText(keyword);
                        searchKey.setSelection(keyword.length());

                        alertDialog.findViewById(R.id.save).setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (ShineSharedPreferences.getUserInfo(mActivity) == null && (emailText.getText() == null || TextUtils.isEmpty(emailText.getText().toString().trim()))) {
                                    Toast.makeText(mActivity, "Please enter address",
                                            Toast.LENGTH_SHORT).show();
                                    emailText.setHintTextColor(getResources().getColor(R.color.red));
                                    return;
                                }

                                if (ShineSharedPreferences.getUserInfo(mActivity) == null && !Pattern.matches(ShineCommons.REGEX, emailText.getText().toString().trim())) {
                                    Toast.makeText(mActivity, "Please enter a valid email address",
                                            Toast.LENGTH_SHORT).show();
                                    emailText.setTextColor(getResources().getColor(R.color.red));
                                    return;
                                }

                                if (searchKey.getText() != null && !TextUtils.isEmpty(searchKey.getText().toString().trim())) {
                                    String user_alert_name = searchKey.getText().toString();
                                    svo.setCustomJobAlertName(user_alert_name);
                                } else {
                                    Toast.makeText(mActivity, "Please enter the alert name",
                                            Toast.LENGTH_SHORT).show();
                                    searchKey.setHintTextColor(getResources().getColor(R.color.red));
                                    return;
                                }

                                Log.d("search_alert_key", searchKey.getText().toString());

                                if (!svo.getIndusType().equals("") && svo.getIndusType() != null) {
                                    svo.setIndusTypeTag(Integer.parseInt(URLConfig.INDUSTRY_MAP
                                            .get(svo.getIndusType())));
                                }
                                if (!svo.getFuncArea().equals("")) {
                                    if (URLConfig.FUNCTIONA_AREA_MAP.get(svo.getFuncArea()) != null) {
                                        svo.setFuncAreaTag(Integer.parseInt(URLConfig.FUNCTIONA_AREA_MAP.get(svo
                                                .getFuncArea())));
                                    } else {
                                        svo.setFuncAreaTag(0);
                                    }

                                    Log.d("FA ","TAG "+svo.getFuncAreaTag());
                                }
                                if (!svo.getMinExp().equals("") && svo.getMinExp() != null
                                        && svo.getMinExpTag() != 0) {
                                } else {
                                    svo.setMinExpTag(0);
                                }



                                if (!svo.getMinSal().equals("") && svo.getMinSal() != null) {

                                } else {
                                    svo.setMinSalTag(0);
                                }
                                if (fromFacets) {


                                    if (userProfileVO != null) {
                                        CustomAlert ca = new CustomAlert(mActivity);
                                        URLConfig.FUNCTIONAL_AREA_CHILD.get(svo.getFuncArea());
                                        ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_SEARCH);
                                        ca.saveCustomAlertFromFacets(
                                                userProfileVO,
                                                svo,
                                                null,
                                                URLConfig.FUNCTIONAL_AREA_CHILD.get("#"
                                                        + svo.getFuncArea()), setFacetsModel, parentView);


                                    } else {

                                        CustomAlert ca = new CustomAlert(mActivity);
                                        ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_SEARCH);
                                        ca.saveCustomAlertFromFacets(null, svo, emailText
                                                        .getText().toString(),
                                                URLConfig.FUNCTIONAL_AREA_CHILD.get("#"
                                                        + svo.getFuncArea()), setFacetsModel, parentView);


                                    }


                                } else {


                                    if (userProfileVO != null) {
                                        CustomAlert ca = new CustomAlert(mActivity);
                                        URLConfig.FUNCTIONAL_AREA_CHILD.get(svo.getFuncArea());
                                        ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_SEARCH);
                                        ca.saveCustomAlert(
                                                userProfileVO,
                                                svo,
                                                null,
                                                URLConfig.FUNCTIONAL_AREA_CHILD.get("#"
                                                        + svo.getFuncArea()), parentView);


                                    } else {

                                        CustomAlert ca = new CustomAlert(mActivity);
                                        ca.setAdd_alert_option(CustomAlert.ADD_ALERT_THROUGH_SEARCH);
                                        ca.saveCustomAlert(null, svo, emailText
                                                        .getText().toString(),
                                                URLConfig.FUNCTIONAL_AREA_CHILD.get("#"
                                                        + svo.getFuncArea()), parentView);


                                    }
                                }
                                DialogUtils.dialogDismiss(alertDialog);


                            }
                        });


                    }


                }
            });


            if (bundle.containsKey(URLConfig.CUSTOM_JOB_NEW_ADD)) {
                FROM_NEW_ADD_CJA = bundle.getBoolean(URLConfig.CUSTOM_JOB_NEW_ADD);

                if (FROM_NEW_ADD_CJA) {

                    save_job_alert.setVisibility(View.GONE);
                    saved_job_alert.setVisibility(View.VISIBLE);
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return parentView;
    }


    @Override
    public void onPause() {
        super.onPause();
        URLConfig.AD++;


    }

    private String getFirstUrl() {

        try {


            if (fromDiscoverConnect && userProfileVO != null) {

                if (dfm != null && dfm.data.company_uid != null) {
                    String seconPartUrl = "?rect_uid=" + dfm.data.company_uid +
                            "&page=" + 1 +
                            "&perpage=" + URLConfig.PER_PAGE;
                    return URLConfig.JOBS_IN_KONNECT_URL_LoggedIn.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                            + seconPartUrl + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug," +
                            "jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
                } else {
                    String seconPartUrl = "?rect=" + svo.getRect_id() +
                            "&page=" + 1 +
                            "&perpage=" + URLConfig.PER_PAGE;
                    return URLConfig.JOBS_IN_KONNECT_URL_LoggedIn.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                            + seconPartUrl + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug," +
                            "jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
                }
            } else if (fromFacets) {


                ArrayList<String> locTagArray = setFacetsModel.getLocation();
                ArrayList<String> salTagArray = setFacetsModel.getSalary();
                ArrayList<String> exTagArray = setFacetsModel.getExperience();
                ArrayList<String> indTagArray = setFacetsModel.getIndustry();
                ArrayList<String> funTagArray = setFacetsModel.getFunctionalArea();

                ArrayList<String> jobTypeTagArray = setFacetsModel.getJobType();
                ArrayList<String> topComapniesTagArray = setFacetsModel.getTopCompanies();
                String location = "";
                String exp = "";
                String salary = "";
                String industry = "";
                String funcArea = "";

                String jobType = "";
                String topCompanies = "";
                String sort = "&sort=" + setFacetsModel.getSort();

                String minsal = "";
                String minexp = "";
                if (svo.getMinExpTag() != 0) {
                    minexp = svo.getMinExpTag() + "";
                }
                if (svo.getMinSalTag() != 0) {
                    minsal = svo.getMinSalTag() + "";
                }

                if (locTagArray != null && locTagArray.size() > 0) {
                    location = createFacetsParams("&location=", locTagArray);
                }
                if (exTagArray != null && exTagArray.size() > 0) {
                    exp = createFacetsParams("&fexp=", exTagArray);
                }
                if (salTagArray != null && salTagArray.size() > 0) {
                    salary = createFacetsParams("&fsalary=", salTagArray);
                }
                if (indTagArray != null && indTagArray.size() > 0) {
                    industry = createFacetsParams("&findustry=", indTagArray);
                }
                if (funTagArray != null && funTagArray.size() > 0) {
                    funcArea = createFacetsParams("&farea=", funTagArray);
                }


                if (jobTypeTagArray != null && jobTypeTagArray.size() > 0) {
                    jobType = createFacetsParams("&job_type=", jobTypeTagArray);
                }
                if (topComapniesTagArray != null && topComapniesTagArray.size() > 0) {
                    topCompanies = createFacetsParams("&fcid=", topComapniesTagArray);
                }
                String finalUrl = URLConfig.SEARCH_RESULT_URL_LOGOUT;
                if (userProfileVO != null) {
                    finalUrl = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                }
                return finalUrl + "?q=" + URLEncoder.encode(svo.getKeyword(), "UTF-8")
                        + "&page=" + "" + 1 + "&perpage=" + "" + URLConfig.PER_PAGE + "&loc=" + URLEncoder.encode(svo.getLocation(), "UTF-8")
                        + "&minexp=" + minexp + "&minsal=" + minsal + "&ind=" + URLConfig.INDUSTRY_MAP.get(svo.getIndusType())
                        + "&area=" + URLConfig.FUNCTIONA_AREA_MAP.get("#" + svo.getFuncArea()) + location + exp + salary
                        + industry + funcArea + jobType + topCompanies + sort + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
            } else if (svo.isFromSimilarJobs()) {
                return svo.getUrl() + "&jobid=" + svo.getJobId() + "&page="
                        + 1 + "&perpage=" + URLConfig.PER_PAGE
                        + "&sort=" + "&date_sort="
                        + URLConfig.NO_OF_DATE
                        + "&fl=id,jCID,jRUrl,is_applied,jCName,jCType,jKwds,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";
            } else if (svo.isFromJD()) {

                String finalUrl = URLConfig.SEARCH_RESULT_URL_LOGOUT;
                if (userProfileVO != null) {
                    finalUrl = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                }

                return finalUrl +
                        "?page=" + "" + 1 +
                        "&perpage=" + "" + URLConfig.PER_PAGE
                        + "&fl=id,jCID,jRUrl,is_applied,jCName,jKwds,jCType,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate"
                        + "&rect=" + svo.getRect_id();
            } else {

                String minsal = "";
                String minexp = "";
                if (svo.getMinExpTag() != 0) {
                    minexp = svo.getMinExpTag() + "";
                }
                if (svo.getMinSalTag() != 0) {
                    minsal = svo.getMinSalTag() + "";
                }

                String finalUrl = URLConfig.SEARCH_RESULT_URL_LOGOUT;
                if (userProfileVO != null) {
                    finalUrl = URLConfig.SEARCH_RESULT_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                }
                return finalUrl + "?q=" + URLEncoder.encode(svo.getKeyword(), "UTF-8") +
                        "&page=" + "" + 1 +
                        "&loc=" + URLEncoder.encode(svo.getLocation(), "UTF-8") +
                        "&area=" + URLConfig.FUNCTIONA_AREA_MAP.get("#" + svo.getFuncArea()) +
                        "&ind=" + URLConfig.INDUSTRY_MAP.get(svo.getIndusType()) +
                        "&minsal=" + minsal +
                        "&minexp=" + minexp +
                        "&perpage=" + "" + URLConfig.PER_PAGE
                        + "&fl=id,jCID,jRUrl,is_applied,jCName,jCType,jKwds,jJT,jExp,jCUID,jLoc,jJT_slug,jCName_slug,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID,jPDate";

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EasyTracker.getInstance().setContext(mActivity);
        client = new GoogleApiClient.Builder(getContext()).addApi(AppIndex.API).build();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        try {
            if (getArguments() != null && getArguments().containsKey(URLConfig.SEARCH_VO_KEY)) {
                final SimpleSearchVO svo = (SimpleSearchVO) getArguments()
                        .getSerializable(URLConfig.SEARCH_VO_KEY);
                if (!fromDiscoverConnect && svo != null && !svo.isFromSimilarJobs()) {
                    inflater.inflate(R.menu.filter_menu, menu);

                    final MenuItem alertMenuItem = menu.findItem(R.id.action_search);
                    alertMenuItem.setActionView(R.layout.search_bar_layout);
                    RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
                    TextView textView = (TextView) rootView.findViewById(R.id.custom_search_bar);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textView.getLayoutParams();
                    if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                        Double d = (dpWidth / 1.6);
                        params.width = d.intValue();
                    } else {
                        Double d = (dpWidth / 1.3);
                        params.width = d.intValue();
                    }


                    textView.setLayoutParams(params);
                    textView.setText(svo.getKeyword());
                    rootView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BaseFragment fragment = new SearchFormFrg();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(URLConfig.SEARCH_VO_MODIFY, svo);
                            bundle.putBoolean("Modify Search", true);
                            fragment.setArguments(bundle);
                            mActivity.singleInstanceReplaceFragment(fragment,true);

                        }
                    });

                } else if (svo != null && svo.isFromSimilarJobs())
                    inflater.inflate(R.menu.similar_search_menu, menu);


                if (ShineSharedPreferences.getUserInfo(mActivity) == null || (svo != null && svo.isFromSimilarJobs())) {
                    menu.findItem(R.id.action_multi_apply).setVisible(false);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            Log.d("Debug::Test", "OnStart SearchResultFrg");
            client.connect();
            AppIndex.AppIndexApi.start(client, getAction());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Action getAction() {

        if (svo != null)
            return Action.newAction(
                    Action.TYPE_VIEW,
                    "Shine Job Search",
                    Uri.parse("https://www.shine.com/job-search/" + svo.getKeyword()),
                    Uri.parse("android-app://com.net.shine/https/www.shine.com/job-search/" + svo.getKeyword())
            );
        return null;
    }

    @Override
    public void onStop() {
        super.onStop();

        try {
            Log.d("Debug::Test", "OnStop SearchResultFrg");
            AppIndex.AppIndexApi.end(client, getAction());
            client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_filter:
                if (facetModel!=null)
                    initilizeFacetFragment();
                else
                    Toast.makeText(mActivity, "No filters available", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_multi_apply:

                if (adapter != null && adapter instanceof JobsListRecyclerAdapter) {

                    adapter.startActionMode(parentView.findViewById(R.id.multi_apply_button));

                } else if (adAdapter != null && adAdapter instanceof JobListRecyclerAdapterForAds) {
                    adAdapter.startActionMode(parentView.findViewById(R.id.multi_apply_button));

                }

                break;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();


        if(svo!=null){
            if (fromDiscoverConnect) {
                mActivity.setTitle("Jobs"); // change title of screen
                ManualScreenTracker.manualTracker("DiscoverJobs");
                mActivity.showBackButton();
            }

            else if(svo.isFromSimilarJobs()){
                mActivity.setTitle(getString(R.string.similiar_jobs));
                ManualScreenTracker.manualTracker("SimilarJobs");
                mActivity.showBackButton();
            }

            else {
                mActivity.setTitle("");
                ManualScreenTracker.manualTracker("JSRP");
                mActivity.hideBackButton();
            }

        }


        mActivity.showNavBar();
        mActivity.showSelectedNavButton(mActivity.SEARCH_OPTION);
        mActivity.setActionBarVisible(true);


    }


    private void downloadsearchresultJobs(String url, View loadingView,
                                          boolean flag, boolean isRefresh) {

        try {

            LoadingCmp = loadingView;
            loadMore = flag;
            fromRefresh = isRefresh;

            if (!isRefresh) {
                DialogUtils.showLoading(mActivity, getString(R.string.loading),
                        LoadingCmp);
            }
            if (loadingView.getId() == R.id.loading_cmp && !fromRefresh)
                gridView.setVisibility(View.GONE);

            Type type = new TypeToken<SimpleSearchModel>() {
            }.getType();

            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this, url, type);

            downloader.execute("jobList");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initilizeFacetFragment() {
        try {
            BaseFragment frg = new SearchFacetsFragment();
            if (facetModel != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_FACETS_VO_KEY,
                        facetModel.clone());
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, svo.clone());
                if (setFacetsModel != null) {
                    System.out.println(setFacetsModel.getSalary());
                    bundle.putSerializable(URLConfig.SEARCH_VO_WITH_FACETS, setFacetsModel.copy());
                }
                frg.setArguments(bundle);
            }
            mActivity.setFragment(frg);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showresult() {
        try {

            errorLayout.setVisibility(View.GONE);

            if (totalJobs > 0) {
                MoengageTracking.setSearchAttributes(mActivity,svo);
                LoadingCmp.setVisibility(View.GONE);
                if (LoadingCmp.getId() == R.id.loading_cmp)
                    gridView.setVisibility(View.VISIBLE);
                parentView.findViewById(R.id.top_fragment).setVisibility(
                        View.VISIBLE);

                if (!svo.isFromSimilarJobs() && !fromDiscoverConnect) {


                    if (expired_job) {
                        jobs_count.setText(Html.fromHtml("<b>The job you are looking for has expired. Here are some similar jobs </b>" + "<br/>" + "<b>" + totalJobs + "</b>"
                                + " jobs found"));
                        save_job_alert.setVisibility(View.GONE);
                        saved_job_alert.setVisibility(View.GONE);
                    } else {
                        jobs_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs + "</font> </b>"
                                + "<font color = '#000000'>  Jobs found </font>"));


                        save_job_alert.setVisibility(View.VISIBLE);
                        saved_job_alert.setVisibility(View.GONE);
                    }
                    job_count_layout.setVisibility(View.VISIBLE);


                } else if (dfm != null && fromDiscoverConnect) {
                    job_count_layout.setVisibility(View.VISIBLE);
                    save_job_alert.setVisibility(View.GONE);

                } else
                    job_count_layout.setVisibility(View.GONE);

                if (adapter != null && adapter instanceof JobsListRecyclerAdapter) {
                    adapter.rebuildListObject(jobList);
                    adapter.notifyDataSetChanged();
                } else if (adAdapter != null && adAdapter instanceof JobListRecyclerAdapterForAds) {
                    adAdapter.rebuildListObject(jobList);
                    adAdapter.notifyDataSetChanged();

                }


                invalidateWithFriendsData();

            } else {
                if (fromDiscoverConnect) {

                    parentView.findViewById(R.id.err_screen).setVisibility(View.VISIBLE);
                    parentView.findViewById(R.id.connectionsContainer_discover).setVisibility(View.GONE);
                    parentView.findViewById(R.id.apply_job).setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            BaseFragment fragment = new JobsTabFrg();
                            Bundle bundle = new Bundle();
                            bundle.putInt(MainTabFrg.SELECTED_TAB, JobsTabFrg.APPLIED_TAB);
                            fragment.setArguments(bundle);
                            mActivity.replaceFragmentWithBackStack(fragment);

                        }
                    });

                    gridView.setVisibility(View.GONE);
                    LoadingCmp.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.GONE);
                    job_count_layout.setVisibility(View.GONE);


                } else {

                    errorLayout.setVisibility(View.VISIBLE);
                    job_count_layout.setVisibility(View.GONE);

                    BaseFragment fragment;
                    if (userProfileVO == null) {
                        fragment = new LogoutHomeFrg();
                    } else {
                        fragment = new LoginHomeFrg();
                    }
                    DialogUtils.showErrorActionableMessage(mActivity, errorLayout,
                            mActivity.getString(R.string.no_result_found_msg),
                            mActivity.getString(R.string.no_result_found_msg2),
                            mActivity.getString(R.string.search_job),
                            DialogUtils.ERR_NO_SEARCH_RESULT, fragment);

                    gridView.setVisibility(View.GONE);
                    LoadingCmp.setVisibility(View.GONE);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callGetFriend() {
        GetFriendsInCompany friends = new GetFriendsInCompany(mActivity, this);
        friends.getFriendsInCompanyVolleyRequest(co_names);
    }

    private void invalidateWithFriendsData() {
        for (int m = 0; m < jobList.size(); m++) {
            SimpleSearchModel.Result job = jobList.get(m);
            if (job.comp_uid_list != null) {
                for (int n = 0; n < job.comp_uid_list.size(); n++) {
                    if (friend_list.containsKey(job.comp_uid_list.get(n))) {
                        job.frnd_model = friend_list.get(job.comp_uid_list.get(n));
                    }
                }
            }
        }


        if (adapter != null && adapter instanceof JobsListRecyclerAdapter) {
            adapter.rebuildListObject(jobList);
            adapter.notifyDataSetChanged();
        } else if (adAdapter != null && adAdapter instanceof JobListRecyclerAdapterForAds) {
            adAdapter.rebuildListObject(jobList);
            adAdapter.notifyDataSetChanged();

        }

        gridView.invalidate();
    }

    @Override
    public void getFriends(HashMap<String, DiscoverModel.Company> result) {
        if (!isAdded())
            return;
        friend_list = result;

        if (result.size() > 0) {
            invalidateWithFriendsData();
        }
    }


    @Override
    public void OnDataResponse(final Object object, String tag) {
        try {

            swipeContainer.setRefreshing(false);
            errorLayout.setVisibility(View.GONE);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if (object != null) {

                        if (!loadMore)
                            jobList = new ArrayList<>();
                        SimpleSearchModel model = (SimpleSearchModel) object;

                        nextUrl = model.next;


                        JsonElement jsonElement = model.facets;
                        if (jsonElement != null) {


                            String s = model.facets.toString();
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(s);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            HashMap<String, Object> facetMap = FacetsUtils.getFacetsModels(jsonObject);
                            FacetsModel newfacetModel = (FacetsModel) facetMap.get(URLConfig.KEY_SEARCH_FACETS);
                            if (newfacetModel != null) {
                                facetModel = newfacetModel;
                            }

                        }

                        for (SimpleSearchModel.Result res : model.results) {
                            int len = res.job_loc_list.size();
                            String loc = "";
                            for (int j = 0; j < len; j++) {
                                if (j >= len - 1) {
                                    loc += res.job_loc_list.get(j);
                                } else {
                                    loc += res.job_loc_list.get(j) + " / ";
                                }
                            }
                            res.job_loc_str = loc;
                        }
                        co_names = "";
                        totalJobs = model.count;
                        int prevSize = jobList.size();


                        /**
                         *
                         * This is for add an AD element in listview at position 3
                         */
                        if (prevSize == 0 && ShineSharedPreferences.isCareerPlusAds(mActivity) && !fromDiscoverConnect && model.results.size() > 3 && (svo != null && !svo.isFromSimilarJobs())) {

                            SimpleSearchModel.Result model1 = new SimpleSearchModel.Result();
                            model.results.add(3, model1);
                        }


                        jobList.addAll(model.results);


                        if (jobList.size() > 0 && !fromDiscoverConnect) {
                            for (int i = prevSize; i < jobList.size(); i++) {
                                co_names = ShineCommons.getCompanyNames(jobList.get(i).comp_uid_list, unique_companies);
                            }
                        }
                        if (fromDiscoverConnect) {
                            for (int i = prevSize; i < jobList.size(); i++) {
                                jobList.get(i).frnd_model = dfm;
                            }

                            String oneJob = "Jobs";

                            if (totalJobs == 1)
                                oneJob = "Job";

                            if (dfm != null) {
                                jobs_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs
                                        + "</font> </b>" + " " + oneJob + " in " + "<b>"
                                        + dfm.company + "</b>"));
                            } else {
                                if (jobList.size() > 0) {
                                    jobs_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs
                                            + "</font> </b>" + " " + oneJob + " in " + "<b>"
                                            + jobList.get(0).comp_name + "</b>"));
                                }

                            }
                            job_count_layout.setVisibility(View.VISIBLE);

                        }

                        Log.d("DEEPAK ", "co " + co_names.toString());
                        if (userProfileVO != null && !co_names.equals(""))
                            callGetFriend();
                        loadMore = false;

                        showresult();
                    }
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {

            swipeContainer.setRefreshing(false);
            gridView.setVisibility(View.GONE);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    LoadingCmp.setVisibility(View.GONE);

                    if (loadMore) {
                        Toast.makeText(mActivity, error, Toast.LENGTH_SHORT).show();
                    } else {
                        errorLayout.setVisibility(View.VISIBLE);
                        DialogUtils.showErrorActionableMessage(mActivity, errorLayout,
                                mActivity.getString(R.string.technical_error),
                                mActivity.getString(R.string.technical_error2),
                                "",
                                DialogUtils.ERR_TECHNICAL, null);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Create facet param for making url
    public String createFacetsParams(String key, ArrayList<String> array) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String values : array) {
            stringBuilder.append(key).append(values);
        }
        return stringBuilder.toString();
    }


    @Override
    public void onRefresh() {

        errorLayout.setVisibility(View.GONE);
        downloadsearchresultJobs(getFirstUrl(),
                parentView.findViewById(R.id.loading_cmp), false, true);


    }
}
