package com.net.shine.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;

public class JobDetailParentFrg extends BaseFragment {

	public static String JOB_LIST = "job_list";
    ViewPager viewPager;
    int savePosition;

    public static JobDetailParentFrg newInstance(Bundle args) {
        JobDetailParentFrg frg = new JobDetailParentFrg();
        frg.setArguments(args);
        return frg;
    }

    public JobDetailParentFrg(){

    }

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.job_detail_view, container, false);

		try {

			Bundle bundle = getArguments();
            Log.d("bundle_jobs",bundle+"");
			int currentIndex = 0;
			ArrayList<SimpleSearchModel.Result> jobList = new ArrayList<>();
            int from_screen = bundle.getInt(JobDetailFrg.FROM_SCREEN, JobDetailFrg.FROM_LISTING);
            SimpleSearchModel.Result srVo;

            switch (from_screen){
                case JobDetailFrg.FROM_WIDGET_JOBS_SCREEN:
                case JobDetailFrg.FROM_APPLIED_JOBS_SCREEN:
                case JobDetailFrg.FROM_LISTING:
                    currentIndex = bundle.getInt(URLConfig.INDEX, 0);
                    //noinspection unchecked
                    jobList = (ArrayList<SimpleSearchModel.Result>) bundle.getSerializable(JOB_LIST);
                    break;
                case JobDetailFrg.FROM_EMAIL_LINK:
                    srVo = new SimpleSearchModel.Result();
                    srVo.jobId = bundle.getString(URLConfig.JOB_ID_FROM_EMAIL_LINK);
                    jobList = new ArrayList<>();
                    jobList.add(srVo);
                    break;
                case JobDetailFrg.FROM_REFERRAL_NOTI:
                    srVo = new SimpleSearchModel.Result();
                    srVo.jobId = bundle.getString(JobDetailFrg.BUNDLE_JOB_ID_KEY);
                    srVo.is_applied = true;
                    jobList = new ArrayList<>();
                    jobList.add(srVo);
                    break;
                case JobDetailFrg.FROM_LOGIN_TO_APPLY:
                    Bundle job_bundle = bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE);
                    jobList = new ArrayList<>();
                    if (job_bundle != null) {
                        jobList.add((SimpleSearchModel.Result) job_bundle.getSerializable(JobDetailFrg.BUNDLE_JOB_DATA_KEY));
                    }
                    break;
            }

             viewPager = (ViewPager) view.findViewById(R.id.view_pager);

            final MyAdapter mAdapter = new MyAdapter(getChildFragmentManager(), jobList, bundle);
			viewPager.setAdapter(mAdapter);

            mAdapter.notifyDataSetChanged();
			viewPager.setCurrentItem(currentIndex);




            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                }

                @Override
                public void onPageSelected(int position) {

                    Log.d("OnPageSelected ","position "+position);
                    ShineCommons.trackCustomEvents("ViewPagerScroll","JD",mActivity);


                    if (ShineSharedPreferences.getUserInfo(getContext()) != null) {
                        ManualScreenTracker.manualTracker("JobDetails-LoggedIn");
                    } else {
                        ManualScreenTracker.manualTracker("JobDetails-NonLoggedIn");
                    }

                    URLConfig.BELOW_AD++;

                }

                @Override
                public void onPageScrollStateChanged(int state) {


                }
            });




		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}




    public class MyAdapter extends FragmentPagerAdapter {
        private ArrayList<SimpleSearchModel.Result> jobList;
        private int count = 0;
        private Bundle bundle = new Bundle();

        private  int mPosition;

        public MyAdapter(FragmentManager fm, ArrayList<SimpleSearchModel.Result> jobLst, Bundle bundle) {
            super(fm);
            this.jobList = jobLst;
            if(bundle!=null)
                this.bundle = bundle;
        }



        @Override
        public int getCount() {
            if (count != jobList.size()) {
                count = jobList.size();
                notifyDataSetChanged();
            }
            return jobList.size();
        }


        @Override
        public Fragment getItem(int position) {
            mPosition=position;

            return JobDetailFrg.newInstance(jobList.get(position), bundle);
        }
    }



}
