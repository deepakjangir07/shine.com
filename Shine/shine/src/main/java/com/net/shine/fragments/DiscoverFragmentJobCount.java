package com.net.shine.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.adapters.DiscoverCompanyListAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class DiscoverFragmentJobCount extends BaseFragment implements
		VolleyRequestCompleteListener, OnScrollListener,SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

	private ListView gridView;
	private DiscoverCompanyListAdapter adapter;
	private ArrayList<DiscoverModel.Company> jobList = new ArrayList<>();
	private View LoadingCmp;
	private int total_companies;
	private View errorLayout;
	private View view;
	private boolean loadMore = false;
	private View sync_now;
	private RelativeLayout searchView;
	private TextView searchBtn;
	private EditText searchText;
    public static boolean isSearch = false;
	private RelativeLayout topLayout;
	private TextView comp_count;

    private SwipeRefreshLayout swipeContainer;

	public static DiscoverFragmentJobCount newInstance(Bundle args) {
		DiscoverFragmentJobCount fragment = new DiscoverFragmentJobCount();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.sort_sync_menu, menu);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.discover_fragment_layout, container, false);
        swipeContainer=(SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
        ShineCommons.setSwipeRefeshColor(swipeContainer);
        swipeContainer.setOnRefreshListener(this);

        searchView = (RelativeLayout) view.findViewById(R.id.search_discover);
		errorLayout = view.findViewById(R.id.error_layout);
		errorLayout.setVisibility(View.GONE);

		searchBtn = (TextView) view.findViewById(R.id.search_company_btn);
		topLayout = (RelativeLayout) view.findViewById(R.id.top_bar);
		searchBtn.setOnClickListener(this);
		searchText = (EditText) view.findViewById(R.id.search_company_text);
		ImageView close_search_btn = (ImageView) view.findViewById(R.id.close_search);
		close_search_btn.setOnClickListener(this);
		gridView = (ListView) view.findViewById(R.id.friends_list);
		jobList.clear();
		adapter = new DiscoverCompanyListAdapter(mActivity, jobList);
		gridView.setAdapter(adapter);
		gridView.setOnScrollListener(this);
		sync_now = view.findViewById(R.id.sync_now_button);
		comp_count = (TextView) view.findViewById(R.id.comp_cout);
		TextView search_list = (TextView) view.findViewById(R.id.search_list);
		search_list.setOnClickListener(this);
		downloadsearchresultJobs(1, view.findViewById(R.id.loading_cmp), false,false);

		return view;

	}

	private void downloadsearchresultJobs(int pageNumber, View loadingView,
			boolean flag,boolean isRefresh) {
		try {
			LoadingCmp = loadingView;
			loadMore = flag;
            if(!isRefresh)
			DialogUtils.showLoading(mActivity, getString(R.string.loading),
                    LoadingCmp);


                String urlSecondPart = "?shine_id="+ ShineSharedPreferences.getCandidateId(mActivity)+"&page="+pageNumber+"&sort_by="+"job_count";


                Type type = new TypeToken<DiscoverModel>(){}.getType();

                VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                        URLConfig.DISCOVER_FRIENDS+urlSecondPart, type);
                req.execute("DiscoverFragJobCount");


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void searchresultCompany(View loadingView, String companyName) {

		try {
			LoadingCmp = loadingView;
			jobList.clear();
			DialogUtils.showLoading(mActivity, getString(R.string.loading),
					LoadingCmp);
			searchBtn.setBackgroundColor(Color.parseColor("#d3d3d3"));
			searchBtn.setClickable(false);

            String urlSecondPart = "?shine_id="+ShineSharedPreferences.getCandidateId(mActivity)+"&co_name="+companyName+"&sort_by="+"job_count";

			isSearch = true;


            Type type = new TypeToken<DiscoverModel>(){}.getType();

            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.DISCOVER_FRIENDS+urlSecondPart, type);
            req.execute("DiscoverFragCompanySearch");


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.search_company_btn:
			String co_name = searchText.getText().toString();
			ShineCommons.hideKeyboard(mActivity);

			gridView.setVisibility(View.GONE);
			searchresultCompany(view.findViewById(R.id.loading_cmp), co_name);
			ShineCommons.hideKeyboard(mActivity);


			ShineCommons.trackShineEvents("Search","CompanySearch",mActivity);




			break;

		case R.id.search_list:
			if (searchView.getVisibility() == View.VISIBLE) {
				searchView.setVisibility(View.GONE);
			} else {
				searchView.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.close_search:
			if (searchView.getVisibility() == View.VISIBLE) {
				searchView.setVisibility(View.GONE);
			} else {
				searchView.setVisibility(View.VISIBLE);
			}
			ShineCommons.hideKeyboard(mActivity);

			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			mActivity.setTitle(getString(R.string.title_konnect));
			mActivity.showSelectedNavButton(BaseActivity.MORE_OPTION);
			mActivity.showBackButton();
			mActivity.showNavBar();
			mActivity.setActionBarVisible(true);
			ManualScreenTracker.manualTracker("Discover");


		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private void showresult() {
		try {
			searchBtn.setBackgroundResource(R.drawable.button_yellow);
			searchBtn.setClickable(true);
			if (total_companies > 0) {
				LoadingCmp.setVisibility(View.GONE);
				topLayout.setVisibility(View.VISIBLE);
				view.findViewById(R.id.friends_list)
						.setVisibility(View.VISIBLE);

				comp_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + total_companies
						+ "</font> </b>" + " companies found"));
				adapter.rebuildListObject(jobList);
				adapter.notifyDataSetChanged();

				if (!isSearch) {
					sync_now.setVisibility(View.GONE);
				}
			} else {

				if (mActivity != null) {

					if (isSearch) {

						comp_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + " 0 "
								+ "</font> </b>" + " companies found"));
						topLayout.setVisibility(View.GONE);
						DialogUtils.showErrorMsg(LoadingCmp,
								getString(R.string.no_discover_friends_search),
								DialogUtils.ERR_NO_KONNECT_COMPANIES);
					}
					else
					{
						comp_count.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + " 0 " + "</font> </b>"
								+ " companies found"));
						topLayout.setVisibility(View.GONE);
						DialogUtils.showErrorMsg(LoadingCmp,
								getString(R.string.no_discover_friends_jobs),
								DialogUtils.ERR_NO_KONNECT_COMPANIES);
					}

					if (!isSearch) {
						sync_now.setVisibility(View.VISIBLE);
					}

					sync_now.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							mActivity.singleInstanceReplaceFragment(new KonnectFrag());
						}
					});


				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadMoreData() {

		try {
			int noofhits = (jobList.size() / URLConfig.COMPANIES_PER_PAGE) + 1;
			int totalPage = total_companies / URLConfig.COMPANIES_PER_PAGE;
			int modtotalnoPages = totalPage % URLConfig.COMPANIES_PER_PAGE;

			if (modtotalnoPages != 0) {
				totalPage = totalPage + 1;
			}

			if (noofhits <= totalPage && total_companies > jobList.size()) {
				downloadsearchresultJobs(noofhits,
						view.findViewById(R.id.loading_grid), true,false);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		try {

			if (totalItemCount != 0) {
				int lastScreen = firstVisibleItem + visibleItemCount;
				if ((lastScreen == totalItemCount) && !(loadMore)) {
					loadMoreData();
				} else if (LoadingCmp.getTag() != null) {
					if (LoadingCmp.getTag().equals("error")) {
						view.findViewById(R.id.loading_grid).setVisibility(
								View.VISIBLE);
						loadMore = false;
					}
				}
			} else {

				if (view.findViewById(R.id.loading_grid) != null) {
					view.findViewById(R.id.loading_grid).setVisibility(
							View.GONE);
				}

			}

            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void OnDataResponse(Object object, String tag) {

		try{


			DiscoverModel model = (DiscoverModel) object;


			total_companies = model.total_count;
            if(jobList==null || !loadMore)
                jobList = new ArrayList<>();
            jobList.addAll(model.results);

			swipeContainer.setRefreshing(false);
			view.findViewById(R.id.loading_cmp)
					.setVisibility(View.GONE);

					loadMore = false;
					showresult();
        } catch (Exception e) {
            e.printStackTrace();
        }

	}

	@Override
	public void OnDataResponseError(final String error, String tag) {
        swipeContainer.setRefreshing(false);
		errorLayout.setVisibility(View.VISIBLE);
		gridView.setVisibility(View.GONE);
		try {
			mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
					view.findViewById(R.id.loading_cmp)
							.setVisibility(View.GONE);
                    searchBtn.setBackgroundResource(R.drawable.button_yellow);
                    searchBtn.setClickable(true);
					DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
							mActivity.getString(R.string.technical_error),
							mActivity.getString(R.string.technical_error2),"",DialogUtils.ERR_TECHNICAL,null);
                    searchView.setVisibility(View.GONE);
                }
            });

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    @Override
    public void onRefresh() {
        try {

			MyApplication.getInstance().getRequestQueue().getCache().clear();
            downloadsearchresultJobs(1, view.findViewById(R.id.loading_cmp), false, true);
			errorLayout.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()){

			case R.id.action_sort:
				showDialogForSortingJobCount();
				break;

			case R.id.action_sync:
				BaseFragment fragment = new KonnectFrag();
				mActivity.singleInstanceReplaceFragment(fragment);



        }
        return false;
    }

    public void showDialogForSortingJobCount() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.sort_option_dialog_discover);

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        RadioButton btnSortJobCount = (RadioButton) dialog.findViewById(R.id.sort_jobcount);
        RadioButton btnSortConnections = (RadioButton) dialog
                .findViewById(R.id.sort_connections);

        btnSortJobCount.getBackground().setColorFilter(Color.parseColor("#009688"), PorterDuff.Mode.SRC_IN);
        btnSortConnections.getBackground().setColorFilter(Color.parseColor("#009688"), PorterDuff.Mode.SRC_IN);

        btnSortJobCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogUtils.dialogDismiss(dialog);

            }
        });
        btnSortConnections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				mActivity.onBackPressed();
				mActivity.singleInstanceReplaceFragment(new DiscoverFragment());
				DialogUtils.dialogDismiss(dialog);
            }
        });
			btnSortJobCount.setChecked(true);

        if (!mActivity.isFinishing())
            dialog.show();

    }

}