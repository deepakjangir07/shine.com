package com.net.shine.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.net.shine.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LookupIntentService extends IntentService {

//	String tableName;

	public LookupIntentService() {
		super("LookupIntentService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		String tableName = intent.getStringExtra("tableName");
		String result = intent.getStringExtra("result");
		int lookup_version = intent.getIntExtra("version", 0);
		parseJson(tableName, result, lookup_version);

	}

	private void parseJson(final String table, final String result1,
			final int lookup_version) {

		HashMap<String, String> map;
		final ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
		try {
			if(result1==null)
				return;

			JSONArray result = new JSONArray(result1);

			for (int i = 0; i < result.length(); i++) {
				map = new HashMap<>();
				JSONObject jobj = result.getJSONObject(i);
				String pid = jobj.getString("pid");
				String pdesc = jobj.getString("pdesc");
				JSONArray childarray = jobj.getJSONArray("child");
				if (childarray.length() > 0) {

					for (int j = 0; j < childarray.length(); j++) {
						HashMap<String, String> childmap = new HashMap<>();
						JSONObject childobj = childarray.getJSONObject(j);
						String cid = childobj.getString("cid");
						String cdesc = childobj.getString("cdesc");

						childmap.put("pid", pid);
						childmap.put("pdesc", pdesc);
						childmap.put("cid", cid);
						childmap.put("cdesc", cdesc.replace(",","/")); //Replace comma into

						arrayList.add(childmap);

					}
				} else {

					map.put("pid", pid);
					map.put("pdesc", pdesc);
					arrayList.add(map);

				}

			}

			try {
				Runnable runnable = new Runnable() {

					@Override
					public void run() {
                        //check insertion

						if(!MyApplication.getDataBase().isTableExists(table))
						{
							MyApplication.getDataBase().createTable(table);
						}


						Log.d("DEBUG::LOOKUP", "LookupIntentService Adding data table " + table);
                            MyApplication.getDataBase().deleteRecords(table);
                            MyApplication.getDataBase().insertLookupValues(table,
                                    arrayList);
                            MyApplication.getDataBase().updateLookupVersion(table,
                                    lookup_version);

                        }

				};
				new Thread(runnable).start();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
