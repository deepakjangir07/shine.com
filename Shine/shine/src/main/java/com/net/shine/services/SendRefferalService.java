package com.net.shine.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.ReferralRequestModel;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

public class SendRefferalService extends Service implements VolleyRequestCompleteListener {

    private ReferralRequestModel model;

    public SendRefferalService()
    {
        //default ConstructorS
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("REF", "Starting Service");
        if(intent!=null)
        {
            Bundle bundle = intent.getBundleExtra("bundle");
            if(bundle!=null && intent.hasExtra("bundle"))
                model = (ReferralRequestModel)(intent.getBundleExtra("bundle").getSerializable("model"));
            if(model==null)
                stopSelf();
            else
                sendRef();
        }
        else
            stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void sendRef()
    {
        Log.d("REF", "Sending Referral");

        try {
            HashMap<String, String> paramsMap = new HashMap<>();

            //mandatory params
            paramsMap.put("job_id",model.getJob_id());
            paramsMap.put("mode",model.getMode()+"");
            paramsMap.put("referrer_name",model.getReferrer_name());
            paramsMap.put("candidate_name",model.getCandidate_name());
            paramsMap.put("referral_id",model.getReferral_id());
        /*paramsMap.put("job_id",model.getJob_id());
        paramsMap.put("job_id",model.getJob_id());
        paramsMap.put("job_id",model.getJob_id());
        paramsMap.put("job_id",model.getJob_id());
*/

            //optional params
            if(model.getReferrer_sid()!=null&&!model.getReferrer_sid().equals(""))
            {
                paramsMap.put("referrer_sid",model.getReferrer_sid());
            }
            if(model.getReferrer_uid()!=null&&!model.getReferrer_uid().equals(""))
            {
                paramsMap.put("referrer_uid",model.getReferrer_uid());
            }
            if(model.getReferrer_image_url()!=null&&!model.getReferrer_image_url().equals(""))
            {
                paramsMap.put("referrer_image_url",model.getReferrer_image_url());
            }
            if(model.getOrganization()!=null&&!model.getOrganization().equals(""))
            {
                paramsMap.put("organization",model.getOrganization());
            }


            Log.d("REF ","url "+model.getUrl());
            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest downloader = new VolleyNetworkRequest(getApplicationContext(),this,
                    model.getUrl(),type);

            downloader.setUsePutMethod(paramsMap);
            downloader.execute("SendReferralService");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void OnDataResponse(Object object, String tag) {
        Log.d("REF", "Referral Sent Successfully");
            stopSelf();
       /* HashMap<String, Object> cachedData = new HashMap<>();
        DataCaching.saveObject(cachedData, getApplicationContext(), DataCaching.RECEIVED_REFERRAL_SECTION);
        DataCaching.saveObject(cachedData, getApplicationContext(), DataCaching.REQUEST_REFERRAL_SECTION);*/
    }

    @Override
    public void OnDataResponseError(String error, String tag) {
        Log.d("REF", "Scheduling Referral rehit");
   Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("REF", "Sending Referral again");
                sendRef();
            }
        }, 5*60*1000);


    }



}
