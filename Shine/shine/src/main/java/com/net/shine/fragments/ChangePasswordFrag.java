package com.net.shine.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetUserStatusListener;
import com.net.shine.models.EmptyModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.GetUserAccessToken;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * Created by manishpoddar on 30/06/17.
 */

public class ChangePasswordFrag extends BaseFragment implements VolleyRequestCompleteListener,GetUserStatusListener {

    private TextInputLayout inputLayoutOldPassword,inputLayoutNewPassword,inputLayoutConfirmPassword;
    private EditText oldPasswordField,newPasswordField,confirmPasswordField;
    private Button changeButton;
    private ProgressDialog plzwait;


    public static ChangePasswordFrag newInstance() {
        
        Bundle args = new Bundle();
        
        ChangePasswordFrag fragment = new ChangePasswordFrag();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password,container,false);

        inputLayoutOldPassword = (TextInputLayout) view.findViewById(R.id.input_layout_oldpassword);
        inputLayoutNewPassword = (TextInputLayout) view.findViewById(R.id.input_layout_newpassword);
        inputLayoutConfirmPassword = (TextInputLayout) view.findViewById(R.id.input_layout_confirmpassword);
        oldPasswordField = (EditText) view.findViewById(R.id.old_password);
        newPasswordField = (EditText) view.findViewById(R.id.new_password);
        confirmPasswordField = (EditText) view.findViewById(R.id.confirm_password);
        TextChangeListener.addListener(oldPasswordField, inputLayoutOldPassword, mActivity);
        TextChangeListener.addListener(newPasswordField, inputLayoutNewPassword, mActivity);
        TextChangeListener.addListener(confirmPasswordField, inputLayoutConfirmPassword, mActivity);
        changeButton = (Button) view.findViewById(R.id.changeBtn);


        oldPasswordField.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

                oldPasswordField.setTextColor(getResources().getColor(
                        R.color.black));

            }
        });
        newPasswordField.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

                newPasswordField.setTextColor(getResources().getColor(
                        R.color.black));

            }
        });
        confirmPasswordField.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

                confirmPasswordField.setTextColor(getResources().getColor(
                        R.color.black));

            }
        });

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPasswordField.length() == 0) {
                    inputLayoutOldPassword.setErrorEnabled(true);
                    inputLayoutOldPassword.setError(getResources().getString(
                            R.string.password_empty_msg));
                    return;

                }
                if (newPasswordField.length() == 0) {
                    inputLayoutNewPassword.setErrorEnabled(true);
                    inputLayoutNewPassword.setError(getResources().getString(
                            R.string.password_empty_msg));
                    return;
                }

                if (confirmPasswordField.length() == 0) {
                    inputLayoutConfirmPassword.setErrorEnabled(true);
                    inputLayoutConfirmPassword.setError(getResources().getString(
                            R.string.password_empty_msg));
                    return;
                }

                if (!newPasswordField.getText().toString().equals(confirmPasswordField.getText().toString())) {
                    Toast.makeText(getActivity(), "Passwords do not match ", Toast.LENGTH_LONG).show();
                    newPasswordField.setTextColor(getResources().getColor(R.color.red));
                    confirmPasswordField.setTextColor(getResources().getColor(R.color.red));
                    return;
                }

                else {

                    ShineCommons.hideKeyboard(mActivity);

                    Type listType = new TypeToken<EmptyModel>() {
                    }.getType();
                    VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity,ChangePasswordFrag.this
                            ,URLConfig.CHANGE_PASSWORD_URL.replace(URLConfig.CANDIDATE_ID,
                            ShineSharedPreferences.getCandidateId(mActivity)), listType);

                    HashMap<String, String> map = new HashMap<>();
                    map.put("old_password", oldPasswordField.getText().toString());
                    map.put("new_password", newPasswordField.getText().toString());
                    downloader.setUsePostMethod(map);
                    downloader.execute("change_password");

                    if (plzwait != null) {
                        plzwait.dismiss();
                    }
                    plzwait = new ProgressDialog(getActivity());
                    plzwait.setMessage("Please Wait...");
                    plzwait.setCancelable(false);
                    plzwait.show();


                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();
        mActivity.setTitle("Change password");
        ManualScreenTracker.manualTracker("Change Password");

    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        ShineSharedPreferences.saveUserPass(mActivity,newPasswordField.getText().toString());
        new GetUserAccessToken(mActivity, ShineSharedPreferences.getUserEmail(mActivity),
                newPasswordField.getText().toString(), this).getUserAccessToken();
    }

    @Override
    public void OnDataResponseError(String error, String tag) {

        if(plzwait !=null)
        {
            plzwait.dismiss();
        }

        DialogUtils.showCustomAlertsNoTitle(mActivity, error);


    }

    @Override
    public void hitUserStatusApi(String candidateId) {

        if(plzwait!=null){
            plzwait.dismiss();
        }
        mActivity.onBackPressed();
        Toast.makeText(mActivity,"Password changed successfully",Toast.LENGTH_SHORT).show();
        ShineCommons.trackShineEvents("Change Password","MenuDraw",mActivity);


    }

    @Override
    public void onUserAuthError(String error) {

        if(plzwait!=null){
            plzwait.dismiss();
        }
        DialogUtils.showCustomAlertsNoTitle(mActivity, error);

    }
}
