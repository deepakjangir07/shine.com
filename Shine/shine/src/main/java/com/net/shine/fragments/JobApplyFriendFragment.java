package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.activity.ThanksAppliedActivity;
import com.net.shine.adapters.DiscoverFriendRecyclerViewAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.interfaces.GetConnectionListener;
import com.net.shine.models.ApplyJobModel;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.GetFriendsInCompany;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;


public class JobApplyFriendFragment extends BaseFragment {



    private int fromScreen;
    private SimpleSearchModel.Result svo;
    private InboxAlertMailDetailModel.InboxJobDetail javo;
    private View inflatedView;
    private TextView rec_redirect_text;
    RecyclerView list;

    public static JobApplyFriendFragment newInstance(Bundle args) {


        JobApplyFriendFragment fragment = new JobApplyFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private boolean isFromGCM = false;
    private String jobID = "";
    private String compname = "";
    private String jobtitle = "";

    private static JobApplyFriendFragment newInstance(boolean fromGCM, String jobID, String compName, String jobTtile) {

        Bundle args = new Bundle();
        args.putBoolean("fromGCM", fromGCM);
        args.putString("jobID", jobID);
        args.putString("compName", compName);
        args.putString("jobTtile", jobTtile);
        JobApplyFriendFragment fragment = new JobApplyFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fromScreen = getArguments().getInt("fromScreen", 0);
        isFromGCM = getArguments().getBoolean("FromGCM", false);
        jobID = getArguments().getString("jobID", "");
        compname = getArguments().getString("compname", "");
        jobtitle = getArguments().getString("jobtitle", "");
        svo = (SimpleSearchModel.Result) getArguments().getSerializable("svo");
        javo = (InboxAlertMailDetailModel.InboxJobDetail) getArguments().getSerializable("java");
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.apply_frnd_list_popup, container, false);

        list = (RecyclerView) inflatedView.findViewById(R.id.frnd_list);

        list.setLayoutManager(new LinearLayoutManager(getContext()));



        if(isFromGCM)
        {

            TextView job_profile = (TextView) inflatedView
                    .findViewById(R.id.job_profile);
            TextView company_name = (TextView) inflatedView
                    .findViewById(R.id.company_name);
//            inflatedView.setVisibility(View.GONE);
            job_profile.setText(jobtitle+"");
            company_name.setText(compname+"");
            inflatedView.findViewById(R.id.apply_button_layout).setVisibility(View.GONE);
            inflatedView.findViewById(R.id.apply_thanks_layout).setVisibility(View.VISIBLE);
            inflatedView.findViewById(R.id.ref_txt).setVisibility(View.VISIBLE);
            final Dialog progressDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
            Type type = new TypeToken<SimpleSearchModel>(){}.getType();
            String URL = URLConfig.JOB_DETAIL_URL_LOGGED_IN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getUserInfo(mActivity).candidate_id);

            VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {
                    svo = ((SimpleSearchModel) object).results.get(0);

                    new GetFriendsInCompany(mActivity, new GetConnectionListener() {
                        @Override
                        public void getFriends(HashMap<String, DiscoverModel.Company> result) {
                            for(DiscoverModel.Company comp: result.values())
                                svo.frnd_model = comp;
                            progressDialog.dismiss();
                            showDialog(svo);
//                            inflatedView.setVisibility(View.VISIBLE);
                        }
                    }).getFriendsInCompanyVolleyRequest(ShineCommons.getCompanyNames(svo.comp_uid_list, new HashSet<String>()));
                }

                @Override
                public void OnDataResponseError(String error, String tag) {
                    progressDialog.dismiss();
                    mActivity.popone();

                }
            },
                    URL + jobID + "/" +
                            "?fl=id,jSal,jCID,jCName,jJT,jCUID,jLoc,jExp,jJT_slug,jCName_slug,is_applied,jRR,jJobType,jWLC,jWSD,jExpDate,jWLocID",
                    type);
            req.execute("jobdetail");
        }
        else
        {
            if(svo!=null)
                showDialog(svo);
            else if(javo!=null)
                showDialog(javo);
        }

        return inflatedView;
    }

    public void showDialog(final InboxAlertMailDetailModel.InboxJobDetail svo) {

        int fromWhich  = 3;

//        LayoutInflater layoutInflater = (LayoutInflater) (mActivity)
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View inflatedView = layoutInflater.inflate(R.layout.apply_frnd_list_popup, null,
//                false);
        final TextView job_profile = (TextView) inflatedView
                .findViewById(R.id.job_profile);
        TextView company_name = (TextView) inflatedView
                .findViewById(R.id.company_name);
        TextView title = (TextView) inflatedView.findViewById(R.id.headline);


        rec_redirect_text=(TextView)inflatedView.findViewById(R.id.apply_req_text);

        if(!TextUtils.isEmpty(svo.jRUrl))
        {
            rec_redirect_text.setText(svo.frnd_model.data.friends.size()+" of your connections work here.\nApply and connect with them to get the referral offline");
            title.setVisibility(View.GONE);

        }
        else{
            title.setVisibility(View.VISIBLE);

        }

        final Button apply = (Button) inflatedView.findViewById(R.id.apply);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(URLUtil.isValidUrl(svo.jRUrl))
                {
                    showCustomApplyRedirectAlerts(mActivity, svo.getJob_id(), ShineCommons.fromInbox(svo));
                    return;
                }

                ApplyJobsNew.dialogApplyFlow("JobConnectionPopup",svo.getJob_id(), mActivity, new ApplyJobsNew.onApplied() {
                    @Override
                    public void onApplied(String appliedResponse, Dialog plzWaitDialog) {

                        DialogUtils.dialogDismiss(plzWaitDialog);
                        DiscoverModel.Company dfm = svo.frnd_model;
                        try {
                            mActivity.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Bundle b=new Bundle();
                        b.putSerializable("dfm",dfm);
                        b.putSerializable("srvo",svo);
                        b.putBoolean("wasAlreadyApplied",false);

                        Intent intent=new Intent(mActivity.getBaseContext(), ThanksAppliedActivity.class);
                        intent.putExtras(b);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        mActivity.startActivity(intent);

                       // mActivity.setFragment(ThanksAppliedFrg.newInstance(dfm, svo, false));

                    }
                });

            }
        });


        if (svo.is_applied()  || URLConfig.jobAppliedSet.contains(svo.getJob_id())) {
            fromWhich = 4;
            inflatedView.findViewById(R.id.apply_button_layout).setVisibility(View.GONE);
            inflatedView.findViewById(R.id.apply_thanks_layout).setVisibility(View.VISIBLE);
            inflatedView.findViewById(R.id.ref_txt).setVisibility(View.VISIBLE);
        }

        job_profile.setText(svo.getTitle());
        company_name.setText(svo.getCompany());
        title.setText(svo.frnd_model.data.friends.size() + " connections can refer you in "
                + svo.getCompany());

//        Display display = (mActivity).getWindowManager()
//                .getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int dialog_size = (int) (size.y - (3.8 * size.y / 100));
//        popWindow = new PopupWindow(inflatedView, size.x, dialog_size, true);
        inflatedView.findViewById(R.id.back_cross).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            mActivity.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

//        popWindow.setFocusable(true);
        inflatedView.setBackgroundDrawable((mActivity).getResources()
                .getDrawable(R.drawable.popup_like_facebook_connection_not_rounded));

        DiscoverFriendRecyclerViewAdapter adapter = new DiscoverFriendRecyclerViewAdapter(
                mActivity, svo.frnd_model, fromWhich, svo);

        adapter.setConnected_through(DiscoverFriendRecyclerViewAdapter.MATCH_JOB);
//        popWindow.setAnimationStyle(R.style.popup_anim);
//
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                popWindow.showAtLocation(inflatedView, Gravity.LEFT, 0, 0);
//            }
//        }, 100);

        list.setAdapter(adapter);

    }



    public void showDialog(final SimpleSearchModel.Result svo) {

//        is_Reff_noti_popup_shown = true;

        this.svo=svo;

        int fromWhich = 3;

//        LayoutInflater layoutInflater = (LayoutInflater) (mActivity)
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View inflatedView = layoutInflater.inflate(R.layout.apply_frnd_list_popup, null,
//                false);
        TextView job_profile = (TextView) inflatedView
                .findViewById(R.id.job_profile);
        TextView company_name = (TextView) inflatedView
                .findViewById(R.id.company_name);

        final Button apply = (Button) inflatedView.findViewById(R.id.apply);

        rec_redirect_text=(TextView)inflatedView.findViewById(R.id.apply_req_text);
        TextView title = (TextView) inflatedView.findViewById(R.id.headline);


        if(!TextUtils.isEmpty(svo.jRUrl))
        {
            rec_redirect_text.setText(svo.frnd_model.data.friends.size()+" of your connections work here.\nApply and connect with them to get the referral offline");

            title.setVisibility(View.GONE);

        }
        else{
            title.setVisibility(View.VISIBLE);

        }

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (URLUtil.isValidUrl(svo.jRUrl)) {
                    showCustomApplyRedirectAlerts(mActivity, svo.jobId, svo);
                    return;
                }


                ApplyJobsNew.dialogApplyFlow("JobConnectionPopup",svo.jobId, mActivity, new ApplyJobsNew.onApplied() {
                    @Override
                    public void onApplied(String appliedResponse, Dialog plzWaitDialog) {

                        Log.d("DEBUG::DIALOGAPPLY", "ONAPPLIED called");
                        DialogUtils.dialogDismiss(plzWaitDialog);
                        DiscoverModel.Company dfm = svo.frnd_model;
                        try {
                            mActivity.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Bundle b=new Bundle();
                        b.putSerializable("dfm",dfm);
                        b.putSerializable("srvo",svo);
                        b.putBoolean("wasAlreadyApplied",false);

                        Intent intent=new Intent(mActivity.getBaseContext(), ThanksAppliedActivity.class);
                        intent.putExtras(b);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        mActivity.startActivity(intent);

                        //mActivity.setFragment(ThanksAppliedFrg.newInstance(dfm, svo, false));


                    }
                });

            }
        });

        if (svo.is_applied || fromScreen == JobDetailFrg.FROM_APPLIED_JOBS_SCREEN
                || URLConfig.jobAppliedSet.contains(svo.jobId)) {
            fromWhich = 4;
            inflatedView.findViewById(R.id.apply_button_layout).setVisibility(View.GONE);
            inflatedView.findViewById(R.id.apply_thanks_layout).setVisibility(View.VISIBLE);
            inflatedView.findViewById(R.id.ref_txt).setVisibility(View.VISIBLE);
//			mActivity.setFragment(new ThanksAppliedFrg(svo.getFrnds_model(), svo, true));
//			return;
        }

        job_profile.setText(svo.jobTitle);
        company_name.setText(svo.comp_name);

        title.setText(svo.frnd_model.data.friends.size() + " connections can refer you in "
                + svo.comp_name);

//        Display display = (mActivity).getWindowManager()
//                .getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int dialog_size = (int) (size.y - (3.8 * size.y / 100));

        inflatedView.findViewById(R.id.back_cross).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            mActivity.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        inflatedView.setBackgroundDrawable((mActivity).getResources()
                .getDrawable(R.drawable.popup_like_facebook_connection_not_rounded));

        DiscoverFriendRecyclerViewAdapter adapter = new DiscoverFriendRecyclerViewAdapter(
                mActivity, svo, fromWhich);
        adapter.setConnected_through(DiscoverFriendRecyclerViewAdapter.MATCH_JOB);

        list.setAdapter(adapter);

    }




    public  void showCustomApplyRedirectAlerts(final BaseActivity mActivity, final String jobId, final SimpleSearchModel.Result result) {

        try {
            final Dialog alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_apply_redirect_dialog);

            alertDialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.dialogDismiss(alertDialog);
                }
            });
            alertDialog.findViewById(R.id.go_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Deepak ","go btn click");
                    DialogUtils.dialogDismiss(alertDialog);
                    final String urlToHit = URLConfig.SUDO_APPLY_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity))
                            .replace(URLConfig.JOB_ID, jobId);
                    final Dialog dialog = DialogUtils.showCustomProgressDialog(mActivity, "Please Wait...");
                    Type type = new TypeToken<ApplyJobModel>() {
                    }.getType();
                    final VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                        @Override
                        public void OnDataResponse(Object object, String tag) {



                            Log.d("Deepak ","go btn click 1 "+result.isInsidePostApply);


//                                if (!MyApplication.getInstance().getIsUpdateFlowShown()) {
//                                    updateFlowFromRedirectJobs(mActivity, dialog, jobId, result);
//                                } else {
                                String text = "<font color = '#333333'> You are being redirected to </font> <font color = '#2c84cc'> company website </font> <font color = '#333333'> to complete the application for this job. </font>";

                                TextView extraText = (TextView) inflatedView.findViewById(R.id.extra_text);

                                String text1 = "<b>"+ svo.frnd_model.data.friends.size()+"</b> of your connection work in <b>"+svo.comp_name+"</b><br> Apply and connect with them to get a referral offline";
                                extraText.setVisibility(View.VISIBLE);
                                extraText.setText(Html.fromHtml(text1));
                                rec_redirect_text.setText(Html.fromHtml(text));
                                rec_redirect_text.setTextSize(TypedValue.COMPLEX_UNIT_PX,mActivity.getResources().getDimension(R.dimen.fourteen_sp));
                                rec_redirect_text.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                                        {
                                            System.out.println("-- package install");
                                            ChromeCustomTabs.openCustomTab(mActivity,result.jRUrl);
                                        }
                                        else {
                                            Bundle bundle = new Bundle();
                                            Intent intent = new Intent(mActivity,CustomWebView.class);
                                            bundle.putString("shineurl",result.jRUrl);
                                            intent.putExtras(bundle);
                                            mActivity.startActivity(intent);

                                        }
                                    }
                                });

                                DiscoverFriendRecyclerViewAdapter adapter = new DiscoverFriendRecyclerViewAdapter(
                                        mActivity, result, 8);
                                adapter.setConnected_through(DiscoverFriendRecyclerViewAdapter.MATCH_JOB);

                                list.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                Log.d("RECRUITER","redirect true");
                               // similarJobsFromRedirectJobs(mActivity, jobId, result, dialog);

                                DialogUtils.dialogDismiss(dialog);

//                                }

                            ShineCommons.trackShineEvents("Apply", "RecruiterRedirect", mActivity);

                            if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                            {
                                System.out.println("-- package install");
                                ChromeCustomTabs.openCustomTab(mActivity,result.jRUrl);
                            }
                            else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity,CustomWebView.class);
                                bundle.putString("shineurl",result.jRUrl);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);

                            }

                        }

                        @Override
                        public void OnDataResponseError(String error, String tag) {
                            DialogUtils.dialogDismiss(dialog);
//                            final Dialog errorDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
//                            errorDialog.findViewById(R.id.ok_btn).setOnClickListener(
//                                    new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View view) {
//                                            DialogUtils.dialogDismiss(errorDialog);
//                                        }
//                                    });


                            if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                            {
                                System.out.println("-- package install");
                                ChromeCustomTabs.openCustomTab(mActivity,result.jRUrl);
                            }
                            else {
                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(mActivity,CustomWebView.class);
                                bundle.putString("shineurl",result.jRUrl);
                                intent.putExtras(bundle);
                                mActivity.startActivity(intent);

                            }
                        }

                    },
                            urlToHit, type);
                    req.setUsePutMethod(new HashMap());
                    req.execute("applyjobs");
                }
            });

            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}
