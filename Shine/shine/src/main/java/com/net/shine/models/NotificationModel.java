package com.net.shine.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manishpoddar on 30/08/17.
 */

public class NotificationModel implements Serializable {


    @SerializedName("new_data")
    @Expose
    public NewData newData;

    @SerializedName("old_data")
    @Expose
    public OldData oldData ;

    public class NewData implements Serializable{

        @SerializedName("job_alert")
        @Expose
        public JobAlert jobAlert;

        @SerializedName("who_viewed")
        @Expose
        public WhoViewed whoViewed;

        @SerializedName("rec_mail")
        @Expose
        public RecruiterMail recruiterMail;

    }

    public class OldData implements Serializable{

        @SerializedName("job_alert")
        @Expose
        public JobAlert jobAlert;

        @SerializedName("who_viewed")
        @Expose
        public WhoViewed whoViewed;

        @SerializedName("rec_mail")
        @Expose
        public RecruiterMail recruiterMail;

    }

    public class JobAlert implements Serializable {

        @SerializedName("count")
        @Expose
        public Integer count;
        @SerializedName("timestamp")
        @Expose
        public String timestamp;

    }

    public class WhoViewed implements Serializable {

        @SerializedName("count")
        @Expose
        public Integer count;
        @SerializedName("timestamp")
        @Expose
        public String timestamp;

    }

    public class RecruiterMail implements Serializable {

        @SerializedName("count")
        @Expose
        public Integer count;
        @SerializedName("timestamp")
        @Expose
        public String timestamp;

    }
}
