package com.net.shine.adapters;

import android.os.Bundle;
import android.view.View;

import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.ArrayList;

/**
 * Created by manishpoddar on 18/07/17.
 */

public class SaveJobsAdapter extends JobsListRecyclerAdapter {

    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);

        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (mList.get(viewHolder.getAdapterPosition()) == null) //this means it is title's position
                    {
                        return;
                    }

                    if (inActionMode()) {
                        onItemClickInActionMode(viewHolder.getAdapterPosition());
                        return;
                    }


                    //TODO: Don't create this list every time
                    ArrayList<SimpleSearchModel.Result> jList = new ArrayList<>();
                    int nullpos = Integer.MAX_VALUE;
                    for (int i = 0; i < mList.size(); i++) {
                        if (mList.get(i) != null) {
                            jList.add(mList.get(i));
                        } else {
                            nullpos = i;
                        }
                    }


                    int pos = viewHolder.getAdapterPosition();
                    if (jList != null && jList.size() > 4 && ShineSharedPreferences.isCareerPlusAds(mActivity)) {
                        jList.remove(3);
                        if(pos>3)
                            pos = pos - 1;

                    }

                    Bundle bundle = new Bundle();
                    bundle.putInt(URLConfig.INDEX, pos);
                    bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jList);
                    BaseFragment fragment = new JobDetailParentFrg();
                    fragment.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public SaveJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list, "SavedJobs");

    }



}

