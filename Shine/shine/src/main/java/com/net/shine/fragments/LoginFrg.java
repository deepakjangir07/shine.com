package com.net.shine.fragments;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.dialogs.TermsAndConditionsDialog;
import com.net.shine.interfaces.GetUserStatusListener;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.models.AdModel;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.LinkedInAuthModel;
import com.net.shine.models.NotificationModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ApplyJobsNew;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.ActivityResultHandler;
import com.net.shine.utils.auth.FacebookConstants;
import com.net.shine.utils.auth.GetUserAccessToken;
import com.net.shine.utils.auth.GoogleConstants;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.auth.SocialAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;
import com.net.shine.widget.WidgetProvider;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static android.R.attr.type;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by ujjawal-work.
 */

public class LoginFrg extends BaseFragment implements GetUserStatusListener, SocialAuthUtils.AuthDialogCallback,
        View.OnClickListener {


    public static LoginFrg newInstance(Bundle args) {
        LoginFrg fragment = new LoginFrg();
        fragment.setArguments(args);
        return fragment;
    }

    public static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$",
            Pattern.CASE_INSENSITIVE);
    public static Set<String> emailSet = new HashSet<>();

    private AutoCompleteTextView emailField;
    private EditText passwordField;
    private TextView showField;
    private TextInputLayout inputLayoutEmail;
    private TextInputLayout inputLayoutPassword;
    private CheckBox termsBoxImg;
    private Dialog alertDialog;
    private boolean termsCheck = true;
    private View view;
    private LinkedInAuthModel model;
    private String comp_name = "";
    private String job_title ="";

    private boolean to_konnect = false;

    private LinearLayout register_btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());

        super.onCreateView(inflater, container, savedInstanceState);


        view = inflater.inflate(R.layout.login_view, container, false);
        view.findViewById(R.id.linkedin_btn).setOnClickListener(this);
        view.findViewById(R.id.google_btn).setOnClickListener(this);
        view.findViewById(R.id.facebook_btn).setOnClickListener(this);
        view.findViewById(R.id.linkedin_apply_btn).setOnClickListener(this);
        view.findViewById(R.id.google_apply_btn).setOnClickListener(this);
        view.findViewById(R.id.facebook_apply_btn).setOnClickListener(this);


        if(getArguments()!=null) {
            to_konnect = getArguments().getInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_NORMAL)==ShineAuthUtils.REQUESTED_TO_KONNECT;
            Bundle apply_bundle = getArguments().getBundle(ApplyJobsNew.APPLY_BUNDLE);

            if (apply_bundle != null) {
                if (apply_bundle.getBoolean(ApplyJobsNew.APPLY_WALKIN, false)
                        || apply_bundle.getBoolean(ApplyJobsNew.APPLY_REDIRECT, false)
                        || apply_bundle.getInt(ApplyJobsNew.APPLY_WITHOUT_RESUME) == 0) {
                    view.findViewById(R.id.social_login_btn).setVisibility(View.VISIBLE);
                    view.findViewById(R.id.social_apply_btn).setVisibility(View.GONE);
                } else {

                    if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity)))
                    {
                        view.findViewById(R.id.social_apply_btn).setVisibility(View.GONE);

                    }
                    else {

                        view.findViewById(R.id.social_login_btn).setVisibility(View.GONE);
                        view.findViewById(R.id.social_apply_btn).setVisibility(View.VISIBLE);
                    }
                }
            } else {
                view.findViewById(R.id.social_login_btn).setVisibility(View.VISIBLE);
                view.findViewById(R.id.social_apply_btn).setVisibility(View.GONE);
            }
        }
        else {
            view.findViewById(R.id.social_login_btn).setVisibility(View.VISIBLE);
            view.findViewById(R.id.social_apply_btn).setVisibility(View.GONE);

        }





        inputLayoutEmail = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        inputLayoutPassword = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        final LinearLayout mainContainer = (LinearLayout) view.findViewById(R.id.main_container);
        mainContainer.requestFocus();

        showField = (TextView) view.findViewById(R.id.show);
        showField.setOnClickListener(this);
        view.findViewById(R.id.login_btn).setOnClickListener(this);
        view.findViewById(R.id.forgot_pass_btn).setOnClickListener(this);
        emailField = (AutoCompleteTextView) view.findViewById(R.id.email);

        register_btn=(LinearLayout)view.findViewById(R.id.register_btn);
        register_btn.setOnClickListener(this);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
            Account[] accounts = AccountManager.get(getActivity())
                    .getAccounts();
            for (Account account : accounts) {
                if (EMAIL_PATTERN.matcher(account.name).matches()) {
                    emailSet.add(account.name);
                }
            }
        }

        emailField.setAdapter(new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                new ArrayList<>(emailSet)));
        passwordField = (EditText) view.findViewById(R.id.password);
        passwordField.setTransformationMethod(new PasswordTransformationMethod());
        passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                ShineCommons.hideKeyboard(mActivity);
                submit();
                return true;
            }
        });

        termsBoxImg = (CheckBox) view.findViewById(R.id.terms_check);
        termsBoxImg.setChecked(true);
        view.findViewById(R.id.terms_text).setOnClickListener(this);
        view.findViewById(R.id.terms_check_layout).setOnClickListener(this);
        TextChangeListener.addListener(emailField, inputLayoutEmail, mActivity);
        TextChangeListener.addListener(passwordField, inputLayoutPassword, mActivity);

        emailField.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String email = "" + emailField.getText();
                    email = email.trim();
                    if (!Pattern.matches(ShineCommons.REGEX, email)) {
                        emailField.setTextColor(ContextCompat
                                .getColor(mActivity, R.color.red));
                        inputLayoutEmail.setErrorEnabled(true);
                        inputLayoutEmail.setError(getResources().getString(
                                R.string.email_invalid_address_msg));
                    } else {
                        inputLayoutEmail.setErrorEnabled(false);
                    }
                }
            }
        });

        if(getArguments()!=null){
            if(getArguments().getSerializable("model")!=null){

                model = (LinkedInAuthModel) getArguments().getSerializable("model");
                if(getArguments().containsKey("link_account")){
                    model.source_type = type;
                    view.findViewById(R.id.social_login_btn).setVisibility(View.GONE);
                }
            }
        }


        return view;
    }

    private ActivityResultHandler resultHandler;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("DEBUG::UJJ", "LoginFrg onActResult");

        if (resultHandler != null) {
            Log.d("DEBUG::UJJ", "LoginFrg onActivityResult calling handler");
            resultHandler.handleOnActivityResult(mActivity, requestCode, resultCode, data);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.showBackButton();
        if(getArguments()!=null) {
            Bundle apply_bundle = getArguments().getBundle(ApplyJobsNew.APPLY_BUNDLE);

            if (apply_bundle != null) {
            mActivity.setTitle("Sign in to apply");}
            else {
                mActivity.setTitle(R.string.sign_in_btn);
            }

            if (to_konnect) {
                mActivity.setTitle(getString(R.string.title_sign_in_to_discover));
            }
            else {
                mActivity.setTitle("Sign in");
            }
            }
        else {
            mActivity.setTitle(R.string.sign_in_btn);
        }

        ManualScreenTracker.manualTracker("Login");

    }

    @Override
    public void onClick(View v) {
        try {
            Bundle apply_bundle = null;
            if(getArguments()!=null) {
                 apply_bundle = getArguments();


                    if(apply_bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE)!=null) {
                        apply_bundle.putInt(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_TO_APPLY_JOBS);

                        SimpleSearchModel.Result svo;
                        svo = (SimpleSearchModel.Result) apply_bundle.getBundle(ApplyJobsNew.APPLY_BUNDLE).getSerializable(JobDetailFrg.BUNDLE_JOB_DATA_KEY);

                        comp_name = svo != null ? svo.comp_name : "";
                        job_title = svo != null ? svo.jobTitle : "";
                    }



            }else {
                comp_name = "";
                job_title = "";
            }



            switch (v.getId()) {

                case R.id.linkedin_btn: //LinkedIn Login
                    ShineCommons.trackShineEvents("LoginAttempt", "LinkedIn", mActivity);

                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomProgressDialog(getActivity(), "Please Wait...");
                    DialogUtils.hideDialog(alertDialog);
                    LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {
                        @Override
                        public void onLinkedInConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            if (TextUtils.isEmpty(accessToken)) {
                                ShineCommons.trackShineEvents("LinkedinLoginFail", "UserLogin", mActivity);
                            }
                            SocialAuthUtils.loginUser(accessToken, expiry, SocialAuthUtils.LINKEDIN_AUTH, getArguments(), mActivity, customDialog, LoginFrg.this);
                        }

                        @Override
                        public void onLinkedInSuccess() {
                        }

                        @Override
                        public void onLinkedInFailed() {
                        }
                    }, true, alertDialog);
                    break;
                case R.id.google_btn:   //Google Login
                    ShineCommons.trackShineEvents("LoginAttempt", "Google", mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    resultHandler = GoogleConstants.GoogleConnect(mActivity, this, new GoogleConstants.GoogleCallBack() {
                        @Override
                        public void onGoogleConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.loginUser(accessToken, expiry, SocialAuthUtils.GOOGLE_AUTH, getArguments(), mActivity, customDialog, LoginFrg.this);
                        }
                    });
                    break;
                case R.id.facebook_btn: //Facebook Login
                    ShineCommons.trackShineEvents("LoginAttempt", "Facebook", mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    resultHandler = FacebookConstants.FacebookConnect(this, new FacebookConstants.FacebookCallBack() {
                        @Override
                        public void onFacebookConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.loginUser(accessToken, expiry, SocialAuthUtils.FACEBOOK_AUTH, getArguments(), mActivity, customDialog, LoginFrg.this);
                        }
                    });
                    break;

                case R.id.linkedin_apply_btn:   //LinkedIn Apply

                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomProgressDialog(getActivity(), "Please Wait...");
                    DialogUtils.hideDialog(alertDialog);
                    LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {
                        @Override
                        public void onLinkedInConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            if (TextUtils.isEmpty(accessToken)) {
                                return;
                            }
                            SocialAuthUtils.socialApplyUser(accessToken, expiry, SocialAuthUtils.LINKEDIN_AUTH, getArguments(), mActivity, customDialog, comp_name, job_title);
                        }
                    }, true, alertDialog);

                    break;

                case R.id.google_apply_btn:  //Google Apply
                    DialogUtils.dialogDismiss(alertDialog);
                    Log.d("DEBUG::UJJ", "before calling google Connect" + (resultHandler == null));
                    resultHandler = GoogleConstants.GoogleConnect(mActivity, this, new GoogleConstants.GoogleCallBack() {
                        @Override
                        public void onGoogleConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.socialApplyUser(accessToken, expiry, SocialAuthUtils.GOOGLE_AUTH, getArguments(), mActivity, customDialog, comp_name, job_title);
                        }
                    });
                    break;

                case R.id.facebook_apply_btn:    //Facebook Apply
                    DialogUtils.dialogDismiss(alertDialog);
                    resultHandler = FacebookConstants.FacebookConnect(this, new FacebookConstants.FacebookCallBack() {
                        @Override
                        public void onFacebookConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.socialApplyUser(accessToken, expiry, SocialAuthUtils.FACEBOOK_AUTH, getArguments(), mActivity, customDialog, comp_name, job_title);
                        }
                    });
                    break;
                case R.id.forgot_pass_btn:
                    mActivity.replaceFragmentWithBackStack(new ForgotPasswordFrg());
                    break;
                case R.id.login_btn:
                    submit();
                    ShineCommons.trackShineEvents("LoginAttempt", "Shine",
                            mActivity);
                    break;
                case R.id.show:
                    if (showField.getText().equals("Show")) {
                        showField.setText("Hide");
                        passwordField.setTransformationMethod(HideReturnsTransformationMethod
                                .getInstance());
                        passwordField.setSelection(passwordField.getText().length());

                    } else if (showField.getText().equals("Hide")) {
                        showField.setText("Show");
                        passwordField.setTransformationMethod(PasswordTransformationMethod
                                .getInstance());
                        passwordField.setSelection(passwordField.getText().length());
                    }
                    break;
                case R.id.ok_btn:
                    DialogUtils.dialogDismiss(alertDialog);
                    break;
                case R.id.terms_text:
                    TermsAndConditionsDialog termsDialog = new TermsAndConditionsDialog();
                    FragmentManager fm = getChildFragmentManager();
                    termsDialog.show(fm, "terms_check");
                    break;
                case R.id.terms_check_layout:
                    if (termsCheck) {
                        termsCheck = false;
                        termsBoxImg.setChecked(false);
                    } else {
                        termsCheck = true;
                        termsBoxImg.setChecked(true);
                    }
                    break;

                case R.id.register_btn:

                    BaseFragment fragment1 = new RegistrationFrg();
                    fragment1.setArguments(apply_bundle);
                    mActivity.singleInstanceReplaceFragment(fragment1);

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void submit() {
        try {
            boolean isEmpty = false;
            String email = "" + emailField.getText();
            email = email.trim();
            if (email.length() == 0) {
                inputLayoutEmail.setErrorEnabled(true);
                inputLayoutEmail.setError(getResources().getString(
                        R.string.email_invalid_address_msg));
                isEmpty = true;
            }
            String password = "" + passwordField.getText();
            password = password.trim();
            if (password.length() == 0) {
                inputLayoutPassword.setErrorEnabled(true);
                inputLayoutPassword.setError(getResources().getString(
                        R.string.password_empty_msg));
                isEmpty = true;
            }
            if (!Pattern.matches(ShineCommons.REGEX, email)) {
                DialogUtils.showErrorToast(getString(R.string.email_invalid_address_msg));
                emailField.setTextColor(ContextCompat.getColor(mActivity, R.color.red));
                inputLayoutEmail.setErrorEnabled(true);
                inputLayoutEmail.setError(getResources().getString(
                        R.string.email_invalid_address_msg));
                return;
            }
            if (isEmpty) {
                DialogUtils.showErrorToast(getResources().getString(R.string.required_fields));
                return;
            }
            if (!termsCheck) {
                DialogUtils.showErrorToast(getString(R.string.please_accept_tc_msg));
                return;
            }
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));
            emailSet.add(email);
            new GetUserAccessToken(mActivity, email, password, this).getUserAccessToken();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void hitUserStatusApi(String candidateId) {
        String URL = URLConfig.USER_SERVER_STATUS_URL.replace(URLConfig.CANDIDATE_ID, candidateId) + "?nid=" + ShineSharedPreferences.getFCMRegistrationId(mActivity) +
                "&vc=" + ShineCommons.getAppVersionCode(mActivity) + "&dt=" + URLConfig.DEVICE_ANDROID;
        Type type = new TypeToken<UserStatusModel>() {
        }.getType();
        VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {
                final UserStatusModel userMmodel = (UserStatusModel) object;


                if (model != null) {
                    String sync_url = "";
                    switch (model.source_type) {
                        case SocialAuthUtils.FACEBOOK_AUTH:
                            sync_url = URLConfig.FACEBOOK_PROFILE_SYNC_API;
                            break;
                        case SocialAuthUtils.LINKEDIN_AUTH:
                            sync_url = URLConfig.LINKEDIN_PROFILE_SYNC_API;
                            break;
                        case SocialAuthUtils.GOOGLE_AUTH:
                            sync_url = URLConfig.GOOGLE_PROFILE_SYNC_API;
                            break;
                    }
                    Type type = new TypeToken<EmptyModel>() {
                    }.getType();
                    VolleyNetworkRequest request = new VolleyNetworkRequest(getContext(), new VolleyRequestCompleteListener() {
                        @Override
                        public void OnDataResponse(Object empty, String tag) {
                            DialogUtils.dialogDismiss(alertDialog);
                            switch (model.source_type) {
                                case SocialAuthUtils.FACEBOOK_AUTH:
                                    if (!TextUtils.isEmpty(model.accessToken))
                                        ShineSharedPreferences.setFacebookAccessToken(model.accessToken, mActivity);
                                    if (!TextUtils.isEmpty(model.expiry))
                                        ShineSharedPreferences.setFacebookAccessExpiry(model.expiry, mActivity);
                                    ShineCommons.trackShineEvents("LoginSuccess", "Facebook-Synced", mActivity);
                                    break;
                                case SocialAuthUtils.LINKEDIN_AUTH:
                                    if (!TextUtils.isEmpty(model.accessToken))
                                        ShineSharedPreferences.setLinkedinAccessToken(model.accessToken, mActivity);
                                    if (!TextUtils.isEmpty(model.expiry))
                                        ShineSharedPreferences.setLinkedinAccessExpiry(model.expiry, mActivity);
                                    ShineCommons.trackShineEvents("LoginSuccess", "LinkedIn-Synced", mActivity);
                                    break;
                                case SocialAuthUtils.GOOGLE_AUTH:
                                    if (!TextUtils.isEmpty(model.accessToken))
                                        ShineSharedPreferences.setGoogleAccessToken(model.accessToken, mActivity);
                                    if (!TextUtils.isEmpty(model.expiry))
                                        ShineSharedPreferences.setGooleAccessExpiry(model.expiry, mActivity);
                                    ShineCommons.trackShineEvents("LoginSuccess", "Google-Synced", mActivity);
                                    break;
                            }
                            ShineAuthUtils.LoginUser(userMmodel, mActivity, getArguments());
                        }

                        @Override
                        public void OnDataResponseError(String error, String tag) {
                            Log.d("Debug::sync", "Profile Sync failed");
                            DialogUtils.dialogDismiss(alertDialog);
                            Toast.makeText(getContext(), error + "", Toast.LENGTH_SHORT).show();
                            ShineAuthUtils.LoginUser(userMmodel, mActivity, getArguments());
                        }
                    }, sync_url.replace(URLConfig.CANDIDATE_ID, userMmodel.candidate_id), type);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("candidate_id", userMmodel.candidate_id);
                    switch (model.source_type) {
                        case SocialAuthUtils.FACEBOOK_AUTH:
                            map.put("access_token", model.accessToken);
                            break;
                        case SocialAuthUtils.LINKEDIN_AUTH:
                            map.put("token", model.accessToken);
                            break;
                        case SocialAuthUtils.GOOGLE_AUTH:
                            map.put("access_token", model.accessToken);
                            break;
                    }
                    map.put("expires_in", model.expiry);
                    request.setUsePostMethod(map);
                    request.execute("Profile-Sync");


                } else {
                    DialogUtils.dialogDismiss(alertDialog);
                    ShineAuthUtils.LoginUser(userMmodel, mActivity, getArguments());
                    ShineCommons.trackShineEvents("LoginSuccess", "Shine", mActivity);
                }



                int[] ids = AppWidgetManager.getInstance(getApplicationContext()).getAppWidgetIds(new ComponentName(getApplicationContext(), WidgetProvider.class));
                WidgetProvider myWidget = new WidgetProvider();

               Log.d("WIDGET","id  "+ids);


                myWidget.onUpdate(getApplicationContext(), AppWidgetManager.getInstance(getApplicationContext()),ids);
            }

            @Override
            public void OnDataResponseError(final String error, String tag) {
                try {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isAdded()) {
                                DialogUtils.dialogDismiss(alertDialog);
                                alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                                alertDialog.findViewById(R.id.ok_btn)
                                        .setOnClickListener(LoginFrg.this);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, URL, type);
        req.execute("UserStatus");



     /*
     *  Hit for check CareerPlus ads
     */
        try {
            if (!TextUtils.isEmpty(candidateId)) {
                GetCareerPlusAds(candidateId);
                NotificationApiHit(candidateId);
                MoengageTracking.setProfileAttributes(mActivity);
                MoengageTracking.setLoginAttribute(mActivity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onUserAuthError(String error) {
        DialogUtils.dialogDismiss(alertDialog);
        DialogUtils.showCustomAlertsNoTitle(mActivity, error);
    }


    @Override
    public void showAuthDialog(final LinkedInAuthModel model, final int type) {
        final Dialog dialog = DialogUtils.getLinkedinRedirectDialog(getActivity(), model.prefill_details.email);

        dialog.findViewById(R.id.register_linkedin_flow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Bundle bun = new Bundle();
                    bun.putSerializable("model", model);
                    switch (type) {
                        case SocialAuthUtils.LINKEDIN_AUTH:

                            bun.putString("new_title", "Register with LinkedIn");
                            break;
                        case SocialAuthUtils.GOOGLE_AUTH:
                            bun.putString("new_title", "Register with Google");
                            break;
                        case SocialAuthUtils.FACEBOOK_AUTH:
                            bun.putString("new_title", "Register with Facebook");
                            break;
                    }
                    BaseFragment fragment = new RegistrationFrg();
                    fragment.setArguments(bun);
                    mActivity.singleInstanceReplaceFragment(fragment);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.login_linkedin_flow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Bundle bun = new Bundle();
                    bun.putSerializable("model", model);
                    switch (type) {
                        case SocialAuthUtils.LINKEDIN_AUTH:

                            bun.putString("new_title", "Login with LinkedIn");
                            break;
                        case SocialAuthUtils.GOOGLE_AUTH:
                            bun.putString("new_title", "Login with Google");
                            break;
                        case SocialAuthUtils.FACEBOOK_AUTH:
                            bun.putString("new_title", "Login with Facebook");
                            break;
                    }
                BaseFragment fragment = new LoginFrg();
                bun.putInt("link_account",100);
                fragment.setArguments(bun);
                mActivity.singleInstanceReplaceFragment(fragment);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.cross).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.dialogDismiss(dialog);
            }
        });
        dialog.show();
    }



    public void NotificationApiHit(final String candidate_id){
        String url ="";
        if(!TextUtils.isEmpty(candidate_id)){

            Log.d("notification_response","inside hit");

            url = URLConfig.NOTIFICATION_API.replace(URLConfig.CANDIDATE_ID,candidate_id);

            Type type = new TypeToken<NotificationModel>(){

            }.getType();

            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                    Log.d("notification_response",object.toString());
                    Gson gson = new Gson();
                    String notificationData = gson.toJson(object);
                    ShineSharedPreferences.saveNotificationData(mActivity,
                            candidate_id+"_noti",notificationData);
                    NotificationCount(candidate_id);
                    Log.d("notification_response1",notificationData);
                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                    Log.d("Notification","error "+error);

                }
            },url,type);

            request.execute("notification");
        }
    }

    public void NotificationCount(String candidate_id) {
        if (!TextUtils.isEmpty(candidate_id)) {
            final String notificationData = ShineSharedPreferences.getNotificationData(mActivity,
                    candidate_id + "_noti");
            if (!TextUtils.isEmpty(notificationData)) {
                Gson gson = new Gson();

                NotificationModel model = gson.fromJson(notificationData, NotificationModel.class);
                if (model != null) {
                    if (model.newData != null) {
                        if (model.newData.jobAlert != null && model.newData.recruiterMail != null
                                && model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                    candidate_id+"_noti_count","3");
                        } else if ((model.newData.jobAlert != null && model.newData.recruiterMail != null) ||
                                (model.newData.recruiterMail != null && model.newData.whoViewed != null) ||
                                (model.newData.jobAlert != null && model.newData.whoViewed != null)) {

                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                    candidate_id+"_noti_count","2");
                        } else if (model.newData.jobAlert != null || model.newData.recruiterMail != null
                                || model.newData.whoViewed != null) {
                            ShineSharedPreferences.saveNotificationCount(mActivity,
                                   candidate_id+"_noti_count","1");
                        }
                    }
                }
            }
        }
    }


    public void GetCareerPlusAds(final String candidate_id) {

        String url = "";
        if (candidate_id != null) {
            url = URLConfig.AD_URL_LOGGEDIN.replace(URLConfig.CANDIDATE_ID, candidate_id);
        } else {
            url = URLConfig.AD_URL_LOGOUT;
        }

        Type type = new TypeToken<AdModel>() {
        }.getType();

        VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
            @Override
            public void OnDataResponse(Object object, String tag) {

                Gson gson = new Gson();
                String adJson = gson.toJson(object);
                if (!TextUtils.isEmpty(candidate_id))
                    ShineSharedPreferences.saveCareerPlusAds(mActivity, candidate_id, adJson);
                else
                    ShineSharedPreferences.saveCareerPlusAds(mActivity, "", adJson);
            }

            @Override
            public void OnDataResponseError(String error, String tag) {

                Log.d("ADS", "error " + error);

            }
        }, url, type);

        request.execute("AD");


    }
}
