package com.net.shine.fragments.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.net.shine.fragments.BrowseJobList;
import com.net.shine.fragments.TopCompaniesFrg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manishpoddar on 14/07/17.
 */

public class BrowseJobsTabFrg extends MainTabFrg {

    public static final int TOP_COMPANIES = 0;
    public static final int INDUSTRY = 1;
    public static final int FUNCTIONAL_AREA = 2;
    public static final int CITY=3;
    public static final int SKILLS = 4;

    public static BrowseJobsTabFrg newInstance() {
        Bundle bundle = new Bundle();
        BrowseJobsTabFrg fragment = new BrowseJobsTabFrg();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity.setTitle("Jobs for you");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.showSelectedNavButton(mActivity.JOBS_OPTION);
        if(getArguments()!=null)
        {
            currentSelected = getArguments().getInt(SELECTED_TAB, TOP_COMPANIES);
        }
        else {
            currentSelected=TOP_COMPANIES;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Jobs for you");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.setActionBarVisible(true);
        mActivity.showSelectedNavButton(mActivity.JOBS_OPTION);

    }

    @Override
    public List<Fragment> getFragments() {
        List<Fragment> frgList = new ArrayList<>();
        TopCompaniesFrg f1 = TopCompaniesFrg.newInstance();

        BrowseJobList f2 = new BrowseJobList();
        Bundle bundle = new Bundle();
        bundle.putInt(MainTabFrg.SELECTED_TAB,INDUSTRY);
        f2.setArguments(bundle);

        BrowseJobList f3 = new BrowseJobList();
        Bundle bundle1 = new Bundle();
        bundle1.putInt(MainTabFrg.SELECTED_TAB,FUNCTIONAL_AREA);
        f3.setArguments(bundle1);

        BrowseJobList f4 = new BrowseJobList();
        Bundle bundle2 = new Bundle();
        bundle2.putInt(MainTabFrg.SELECTED_TAB,CITY);
        f4.setArguments(bundle2);

        BrowseJobList f5 = new BrowseJobList();
        Bundle bundle3 = new Bundle();
        bundle3.putInt(MainTabFrg.SELECTED_TAB,SKILLS);
        f5.setArguments(bundle3);

        frgList.add(f1);
        frgList.add(f2);
        frgList.add(f3);
        frgList.add(f4);
        frgList.add(f5);

        return frgList;
    }

    @Override
    public ArrayList<String> getNames() {
        ArrayList<String> tabName = new ArrayList<>();
        tabName.add("Top companies");
        tabName.add("Industry");
        tabName.add("Functional area");
        tabName.add("City");
        tabName.add("Skill");
        return tabName;
    }
}
