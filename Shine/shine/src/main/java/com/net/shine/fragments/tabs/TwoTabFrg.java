package com.net.shine.fragments.tabs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.net.shine.R;
import com.net.shine.adapters.MainPagerAdapter;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.interfaces.TabChangeListener;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;
import java.util.List;

public abstract class TwoTabFrg extends BaseFragment {

    public static final String SELECTED_TAB = "selected_tab";
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    protected int currentSelected;

    protected String title1 = "";
    protected String title2 = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ShineCommons.hideKeyboard(mActivity);
        mActivity.hideBackButton();
        mActivity.showNavBar();
        System.out.println("on create-tab");
    }


    public abstract List<Fragment> getFragments();

    private List<TabChangeListener> listeners;

    public abstract ArrayList<String> getNames();

    ArrayList<String> tabName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.two_tab_frg, container, false);
        try {
            List<Fragment> fragments = getFragments();

            listeners = new ArrayList<>();

            for(Fragment frg: fragments)
                if(frg instanceof TabChangeListener)
                    listeners.add((TabChangeListener) frg);

            tabName = getNames();

            mViewPager = (ViewPager) view.findViewById(R.id.pager);

            tabLayout = (TabLayout) view.findViewById(R.id.tabs);

            tabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.link), Color.BLACK);

            tabLayout.addTab(tabLayout.newTab().setText(tabName.get(0)));
            tabLayout.addTab(tabLayout.newTab().setText(tabName.get(1)));

            MainPagerAdapter mSearchPagerAdapter = new MainPagerAdapter(mActivity, getChildFragmentManager(), fragments, tabName);

            mViewPager.setAdapter(mSearchPagerAdapter);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    currentSelected = position;
                    PageSelected(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

            try {
                tabLayout.setupWithViewPager(mViewPager);
                if (tabLayout.getSelectedTabPosition() == 1) {
                    currentSelected = 1;
                }
                mViewPager.setCurrentItem(currentSelected);
                Log.d("CURRENT TAB ", "selected tab " + currentSelected + " page " + mViewPager.getCurrentItem());
            } catch (Exception e) {
                e.printStackTrace();
            }
            PageSelected(currentSelected);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    protected void PageSelected(int position) {


    }

    public void setTabTitle(int position, String Title)
    {
        try {
            tabLayout.getTabAt(position).setText(Title);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.hideBackButton();
        mActivity.showNavBar();
    }

//    public void goToTabAt(int pos, Bundle bun, int auth_type){
//        try {
//
//            Log.d("DEBUG::UJJ", "new Title:" + bun.getString("new_title", ""));
//
//            if(!TextUtils.isEmpty(bun.getString("new_title", ""))) {
//                if(pos==0 ) {
//                    title1 = bun.getString("new_title", "Login");
//                    if(mActivity!=null)
//                        mActivity.setTitle(title1);
//                }
//                else if(pos==1) {
//                    title2 = bun.getString("new_title", "Register");
//                    if(mActivity!=null)
//                        mActivity.setTitle(title2);
//                }
//            }
//
//            if(mViewPager!=null){
//                mViewPager.setCurrentItem(pos);
//                if(listeners!=null) {
//
//                    Log.d("MAINTABFRG","listers "+listeners.size());
//
//                    Log.d("MAINTABFRG","listers "+listeners.toString());
//                    listeners.get(pos).onTabChanged(bun, auth_type);
//
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//    }



}



