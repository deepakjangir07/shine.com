package com.net.shine.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.net.shine.R;
import com.net.shine.activity.SendMailActivity;
import com.net.shine.config.ServerConfig;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.InboxAlertMailDetailModel;
import com.net.shine.models.ReferralRequestModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.services.SendRefferalService;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.ReferralCipher;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.views.KonnectPopupWindow;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

public class DiscoverFriendRecyclerViewAdapter extends RecyclerView.Adapter<DiscoverFriendRecyclerViewAdapter.CustomViewHolder> {


    public Activity activity;
    private UserStatusModel userProfileVO;
    public ArrayList<DiscoverModel.Company.CompanyData.Friends> friendLists;
    private int fromWhichPage;

    //2, we are from Konnect, no jobID
    //3, we are in middle Apply Popup
    //4, we have applied for this job
    //1 is obsolete now

    private int positionForLinkedIn;

    private String jobTitle, organizationName, connected_via_study = "",
            connected_via_work = "", jobUrl, jobID, jobLocation, jobExperience;
    private String mailCopy = "";

    public static final int MATCH_JOB = 1;
    public static final int DISCOVER_CONNECTION = 2;

    private int connected_through = 0;

    public void setConnected_through(int connected_through) {
        this.connected_through = connected_through;
    }

    public DiscoverFriendRecyclerViewAdapter(Activity a, DiscoverModel.Company svo, int fromWhichPage, SimpleSearchModel.Result svo1) {
        activity = a;
        this.fromWhichPage = fromWhichPage;
        this.friendLists =  svo.data.friends;
        userProfileVO = ShineSharedPreferences.getUserInfo(activity);
        jobTitle = svo1.jobTitle;
        jobID = svo1.jobId;
        organizationName = svo1.comp_name;
        jobUrl = ServerConfig.SERVER_IP_SHINE + "/jobs/" + svo1.title_slug + "/"
                + svo1.company_slug + "/" + svo1.jobId + "/";
        jobExperience = svo1.jobExperience;
        jobLocation = svo1.job_loc_str;
        ManualScreenTracker.manualTracker("Connections-Discover");
    }

    public DiscoverFriendRecyclerViewAdapter(Activity a, DiscoverModel.Company svo,
                                             int fromWhichPage) {
        activity = a;
        this.fromWhichPage = fromWhichPage;
        this.friendLists = svo.data.friends;
        userProfileVO = ShineSharedPreferences.getUserInfo(activity);
        ManualScreenTracker.manualTracker("Connections-Discover");
    }

    public DiscoverFriendRecyclerViewAdapter(Activity a, SimpleSearchModel.Result svo1,
                                             int fromWhichPage) {
        activity = a;
        this.fromWhichPage = fromWhichPage;
        this.friendLists = svo1.frnd_model.data.friends;
        userProfileVO = ShineSharedPreferences.getUserInfo(activity);
        jobTitle = svo1.jobTitle;
        jobID = svo1.jobId;
        organizationName = svo1.comp_name;
        jobUrl = ServerConfig.SERVER_IP_SHINE + "/jobs/" + svo1.title_slug + "/"
                + svo1.company_slug + "/" + svo1.jobId + "/";
        jobExperience = svo1.jobExperience;
        jobLocation = svo1.job_loc_str;

        ManualScreenTracker.manualTracker("Connections-Jobs");

    }

    public DiscoverFriendRecyclerViewAdapter(Activity a, DiscoverModel.Company svo,
                                             int fromWhichPage,
                                             InboxAlertMailDetailModel.InboxJobDetail svo1) {
        activity = a;
        this.fromWhichPage = fromWhichPage;
        this.friendLists = svo.data.friends;
        userProfileVO = ShineSharedPreferences.getUserInfo(activity);
        jobTitle = svo1.getTitle();
        jobID = svo1.getJob_id();
        organizationName = svo1.getCompany();

        jobUrl = ServerConfig.SERVER_IP_SHINE + "/jobs/" + svo1.getTitleSlug() + "/"
                + svo1.getCompanySlug() + "/" + svo1.getJob_id() + "/";
        jobExperience = svo1.getExperience();
        String loc_arr[] = svo1.getLocation();
        if(loc_arr!=null && loc_arr.length>0)
        {
            jobLocation = loc_arr[0];
            for(int i=1;i<loc_arr.length;i++)
            {
                jobLocation = jobLocation + ", "  + loc_arr[i];
            }
        }


        ManualScreenTracker.manualTracker("Connections-Discover");

    }

    @Override
    public int getItemCount() {
        if(friendLists==null)
            return 0;
        return this.friendLists.size();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(activity).inflate(R.layout.phbook_row, parent, false);

        try {
            RecyclerView recyclerView = (RecyclerView) parent;
            return new CustomViewHolder(convertView, recyclerView);
        }catch (Exception e){
            e.printStackTrace();
            return new CustomViewHolder(convertView);
        }

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        holder.worked_with.setVisibility(View.GONE);
        holder.study_with.setVisibility(View.GONE);
        connected_via_study = connected_via_work = "";
        mailCopy = "";
        final DiscoverModel.Company.CompanyData.Friends friends = friendLists.get(position);

        int source = check_connection_source(friends.linkedin_id,
                    friends.facebook_url, friends.emails, friends.phones,
                    friends.shine_id);

        if (source == 1) {
            holder.worked_with.setText("Connected via LinkedIn");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 2) {
            holder.worked_with.setText("Connected via Facebook");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 3) {
            holder.worked_with.setText("Connected via Email");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 4) {
            holder.worked_with.setText("Connected via Phonebook");
            holder.worked_with.setVisibility(View.VISIBLE);
        } else if (source == 5
                && (!friends.direct_connection)) {
            connected_via_work = friends.worked_together;
            connected_via_study = friends.studied_together;

            if (connected_via_work != null
                    && (!connected_via_work.trim().equals(""))) {
                holder.worked_with.setText("worked together at " + connected_via_work);
                holder.worked_with.setVisibility(View.VISIBLE);
            } else {
                holder.worked_with.setVisibility(View.GONE);
                connected_via_work = "";
            }

            if (connected_via_study != null
                    && (!connected_via_study.trim().equals(""))) {
                holder.study_with
                        .setText("studied together at " + connected_via_study);
                holder.study_with.setVisibility(View.VISIBLE);
                connected_via_study = "studied together at "
                        + connected_via_study;
            } else {
                holder.study_with.setVisibility(View.GONE);
                connected_via_study = "";
            }
            String lastLine = "<br/>If you recall, we ";
            if (!holder.study_with.getText().toString().equals("")) {
                lastLine = lastLine
                        + holder.study_with.getText().toString().substring(0, 1)
                        .toLowerCase()
                        + holder.study_with.getText().toString().substring(1);
            }
            if (!holder.worked_with.getText().toString().equals("")) {
                if (holder.study_with.getText().toString().equals("")) {
                    lastLine = lastLine
                            + " and "
                            + holder.worked_with.getText().toString().substring(0, 1)
                            .toLowerCase()
                            + holder.worked_with.getText().toString().substring(1);
                } else {
                    lastLine = lastLine
                            + holder.worked_with.getText().toString().substring(0, 1)
                            .toLowerCase()
                            + holder.worked_with.getText().toString().substring(1);
                }
            }

            if (lastLine.equals("<br/>If you recall, we "))
            {
                mailCopy = "";
            }

        }


        if (!TextUtils.isEmpty(friends.linkedin_image_url.trim())) {
            try {

                Glide.with(activity).load(friends.linkedin_image_url).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                        Log.d("glide","exception"+e);
                        ColorGenerator generator = ColorGenerator.MATERIAL;
                        int color = generator.getRandomColor();
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRect(friends.name.substring(0,1).toUpperCase(), color);
                        holder.friendImage.setImageDrawable(drawable);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        Log.d("glide","resource"+resource+"");

                        return false;
                    }
                }).into(holder.friendImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(friends.name.substring(0,1).toUpperCase(), color);
            holder.friendImage.setImageDrawable(drawable);
        }


        holder.btnEmail.setFocusable(false);
        holder.btnEmail.setFocusableInTouchMode(false);
        holder.btnEmail.setDuplicateParentStateEnabled(false);
        String strName = friends.name;
        holder.contactName.setText(strName);

        if (fromWhichPage == 3)
        {
            if(!URLConfig.jobAppliedSet.contains(jobID)) //ideal case
            {
                holder.konnect.setVisibility(View.GONE);

            }
            else //exceptional case when resume midout applied
            {
                holder.konnect_text.setText("Get Referral");
                holder.btnPhone.setVisibility(View.GONE);
            }

        } else if (fromWhichPage == 4) {
            holder.konnect_text.setText("Get Referral");
            holder.btnPhone.setVisibility(View.GONE);
        }

        else if(fromWhichPage==5) {
            holder.konnect_text.setText("Invite");
            holder.btnPhone.setVisibility(View.GONE);
        }
        else {
            holder.konnect_text.setText("Connect");
        }

        holder.konnect_text.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                connected_via_work = friends.worked_together;
                connected_via_study = friends.studied_together;

                if (connected_via_work != null
                        && (!connected_via_work.trim().equals(""))) {
                    connected_via_work = "worked together at "
                            + connected_via_work;
                }
                if (connected_via_study != null
                        && (!connected_via_study.trim().equals(""))) {

                    connected_via_study = "studied together at "
                            + connected_via_study;
                }

                v.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View v) {

                    }

                    @Override
                    public void onViewDetachedFromWindow(View v) {
                        v.removeOnAttachStateChangeListener(this);
                        KonnectPopupWindow.dismissInstance();
                    }
                });

                if(holder.recyclerView!=null)
                    holder.recyclerView.smoothScrollToPosition(position);
                try {

                    if(position != friendLists.size()-1)
                    {
                        KonnectPopupWindow.getInstance(activity).show(v, holder.rlmore, false);
                    }
                    else
                    {
                        KonnectPopupWindow.getInstance(activity).show(v, holder.rlmore, true);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }

        });


        if (TextUtils.isEmpty(friends.emails)) {
            holder.rlmore.findViewById(R.id.btnEmail_layout).setVisibility(
                    View.GONE);
        }  else  {
            holder.rlmore.findViewById(R.id.btnEmail_layout).setVisibility(
                    View.VISIBLE);
        }


        if (TextUtils.isEmpty(friends.phones)) {
            holder.rlmore.findViewById(R.id.btnPhone_layout).setVisibility(
                    View.GONE);
            holder.rlmore.findViewById(R.id.btnWhatsapp_layout).setVisibility(
                    View.GONE);
        } else {
            holder.rlmore.findViewById(R.id.btnPhone_layout).setVisibility(
                    View.VISIBLE);
            holder.rlmore.findViewById(R.id.btnWhatsapp_layout).setVisibility(
                    View.VISIBLE);
        }

        if (TextUtils.isEmpty(friends.facebook_url)) {
            holder.rlmore.findViewById(R.id.btnFacebook_layout).setVisibility(
                    View.GONE);
        } else  {
            holder.rlmore.findViewById(R.id.btnFacebook_layout).setVisibility(
                    View.VISIBLE);
        }


        if (TextUtils.isEmpty(friends.linkedin_id)) {
            holder.rlmore.findViewById(R.id.btnLinkdin_layout).setVisibility(
                    View.GONE);
        } else {


            holder.rlmore.findViewById(R.id.btnLinkdin_layout).setVisibility(
                    View.VISIBLE);
        }

        if (TextUtils.isEmpty(friends.shine_id)
                || friends.direct_connection) {
            holder.rlmore.findViewById(R.id.btnShine_layout).setVisibility(
                    View.GONE);
        } else {
            holder.rlmore.findViewById(R.id.btnShine_layout).setVisibility(
                    View.VISIBLE);
        }

        holder.btnEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String emailStr = friends.emails;
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                String nameUser = friends.name;

                String ref_URL = SendReferral(position);

                if (fromWhichPage == 1 || fromWhichPage == 4) {
                    try {
                        String subject = "Hi " + nameUser
                                + ", " + userProfileVO.full_name + " wants you to Refer them\n";

                        Spanned mailText = Html
                                .fromHtml("<html><head><title></title></head><body><p>Hi&nbsp;"
                                        + nameUser
                                        + ",</p><p></p><p>I came across the following job in your company and am interested in it.</p><p>"
                                        + jobTitle
                                        + " at "
                                        + organizationName
                                        + "</p><a href=\""
                                        + jobUrl
                                        + "\" >"
                                        + jobUrl
                                        + "</a><p>I would like to request you to refer my candidature to the hiring manager. " +
                                        "You can do so by clicking on the link below. </p> "
                                        + ref_URL
                                        + "<p>Thanks for your help.</p>"
                                        + "<p>Warm regards</p><p>"
                                        + userProfileVO.full_name
                                        + "</p><p>"
                                        + userProfileVO.mobile_no
                                        + "</p><p></p>"
                                        + "</body></html>");
                        String textToSend = "mailto:" + emailStr + "?subject="
                                + subject + "&body=" + mailText;
                        intent.setType("text/html");
                        intent.setData(Uri.parse(textToSend));
                        KonnectPopupWindow.dismissInstance();
                        activity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        String subject = "Hi " + nameUser
                                + ", I want to connect with you";
                        Spanned mailText = Html
                                .fromHtml("<html><head><title></title></head><body><p>Hi&nbsp;"
                                        + nameUser
                                        + ",</p><p></p><p>I would like to connect with you for possible openings in your company.</p><p>It would be great if you could reply to this email or send me your contact details to discuss this further.</p><p>Looking forward to hear from you.</p><p>Thanks and regards</p><p>"
                                        + userProfileVO.full_name
                                        + "</p><p>"
                                        + userProfileVO.mobile_no
                                        + "</p><p>"
                                        + "</p></body></html>");
                        String textToSend = "mailto:" + emailStr + "?subject="
                                + subject + "&body=" + mailText;
                        intent.setData(Uri.parse(textToSend));
                        KonnectPopupWindow.dismissInstance();
                        activity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (activity != null) {

                    if (connected_through == DISCOVER_CONNECTION) {
                        ShineCommons.trackShineEvents("CompanyConnect","Email", activity);

                    } else if (connected_through == MATCH_JOB) {
                        ShineCommons.trackShineEvents("JobReferralConnect","Email", activity);

                    }

                }
            }
        });

        holder.btnwhatsapp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String Ref_URL = SendReferral(position);

                Log.d("REF", "from: " + fromWhichPage + " url: " + Ref_URL);

                Uri uri = Uri.parse("smsto:" + friends.phones);
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.setPackage("com.whatsapp");
                ClipboardManager clipboard;
                ClipData clip;
                if (fromWhichPage == 1 || fromWhichPage == 4) {

                    clip = ClipData.newPlainText(
                            "msg",
                            Html.fromHtml("<html><p>Hi, </p><p>I came across the following job opportunity in your company on Shine.com <br />"
                                    + jobUrl
                                    + "<br /> It would be great if you can refer me for the same here: <br /> " + Ref_URL
                                    + "</p> <p> Regards <br/>"
                                    + userProfileVO.full_name + "</p></html>"));

                } else {

                    clip = ClipData.newPlainText(
                            "msg",
                            Html.fromHtml("<html><p>Hi, </p><p>I would like to connect with you regarding a job opportunity in your company.</p> <p> Regards <br/>"
                                    + userProfileVO.full_name + "</p></html>"));
                }

                clipboard = (ClipboardManager) activity
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(clip);

                try {
                    KonnectPopupWindow.dismissInstance();
                    activity.startActivity(i);
                    Toast.makeText(activity,
                            "Your message is copied to clipboard",
                            Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(activity, "Sorry whatsapp not installed",
                            Toast.LENGTH_LONG).show();
                    final String appPackageName = "com.whatsapp";

                    try {


                        System.out.println("-- package not install "+friends.phones);
                        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:" +friends.phones));
                        Spanned smsText;

                        if(fromWhichPage==1 || fromWhichPage ==4){
                            smsText = Html.fromHtml("<html><p>Hi, </p><p>I came across the following job opportunity in your company on Shine.com <br />"
                                    + jobUrl
                                    + "<br /> It would be great if you can refer me for the same here: <br /> " + Ref_URL
                                    + "</p> <p> Regards <br/>"
                                    + userProfileVO.full_name + "</p></html>");
                        }
                        else {
                            smsText = Html.fromHtml("<html><p>Hi, </p><p>I would like to connect with you regarding a job opportunity in your company.</p> <p> Regards <br/>"
                                    + userProfileVO.full_name + "</p></html>");
                        }

                        Log.d("message body",smsText+"");
                        intent.putExtra("sms_body", smsText.toString());
                        activity.startActivity(intent);
                    } catch (Exception er) {
                        er.printStackTrace();
                    }
                }
                if (activity != null) {

                    if (connected_through == DISCOVER_CONNECTION) {
                        ShineCommons.trackShineEvents("CompanyConnect","Message", activity);


                    } else if (connected_through == MATCH_JOB) {
                        ShineCommons.trackShineEvents("JobReferralConnect","Email", activity);


                    }

                }

            }
        });


        holder.btnLinkdin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(friends.linkedin_url)) {
                    final Dialog alertDialog = new Dialog(activity);
                    alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alertDialog.setContentView(R.layout.cus_linkedin_limit_dialog);
                    alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    TextView linkedin_error = (TextView) alertDialog.findViewById(R.id.linkedin_err_txt);
                    linkedin_error.setText("We're currently unable to connect this person via our app.\n" +
                            "You can directly get in touch with person " + friends.name + " via linkedin");

                    TextView cancel_button = (TextView) alertDialog.findViewById(R.id.linkedin_limit_send_btn);
                    cancel_button.setText("Cancel");
                    alertDialog.findViewById(R.id.no_btn).setVisibility(View.GONE);
                    alertDialog.findViewById(R.id.linkedin_limit_send_btn).setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }

                else {

                if (fromWhichPage != 2) {
                    String accessToken = ShineSharedPreferences.getLinkedinAccessToken(activity);
                    long expiry = ShineSharedPreferences.getLinkedinAccessExpiry(activity);
                    if (accessToken.equals("") || expiry < System.currentTimeMillis()) {
                        LinkedinConnect();
                        positionForLinkedIn = position;
                    } else {
                        KonnectPopupWindow.dismissInstance();
                        Intent intent = new Intent(activity, SendMailActivity.class);
                        Bundle args = new Bundle();
                        args.putInt(SendMailActivity.TO_SCREEN, SendMailActivity.LINKEDIN_MAIL_SCREEN);
                        args.putInt("fromWhich", 1);
                        args.putString("recipientProfileID", friends.linkedin_id);
                        args.putString("recipientName", friends.name);
                        args.putString("jobUrl", jobUrl);
                        args.putSerializable("model", generateReferral(position));
                        args.putString("recipientProfileUrl", friends.linkedin_url);
                        intent.putExtras(args);
                        activity.startActivity(intent);
//                        ((BaseActivity) activity)
//                                .setFragment(LinkedInMailFragment.newInstance(1, friends.linkedin_id, friends.name//, jobTitle, organizationName
//                                        , jobUrl, generateReferral(position), friends.linkedin_url));
                    }
                } else {
                    KonnectPopupWindow.dismissInstance();
                    ShineCommons.trackCustomEvents("AppExit", "LinkedIn", activity);
                    try {

                        activity.getPackageManager().getPackageInfo(
                                "com.linkedin.android", 0);
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                .parse(friends.linkedin_url)));

                    } catch (Exception e) {
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                .parse(friends.linkedin_url)));

                        e.printStackTrace();
                    }

                }

                if (activity != null) {

                    if (connected_through == DISCOVER_CONNECTION) {


                    } else if (connected_through == MATCH_JOB) {


                    }
                }

            }
            }
        });

        holder.btnPhone.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                if (friends.phones != null) {
                    Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                            + friends.phones));
                    KonnectPopupWindow.dismissInstance();
                    activity.startActivity(i);
                    if (activity != null) {

                        if (connected_through == DISCOVER_CONNECTION) {
                            ShineCommons.trackShineEvents("CompanyConnect", "Call", activity);

                        } else if (connected_through == MATCH_JOB) {
                            ShineCommons.trackShineEvents("JobReferralConnect", "Call", activity);


                        }
                    }
                }
            }
        });

        holder.btnShine.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                connected_via_work = friends.worked_together;
                connected_via_study = friends.studied_together;

                if (connected_via_work != null
                        && (!connected_via_work.trim().equals(""))) {
                    connected_via_work = "worked together at "
                            + connected_via_work;
                }
                if (connected_via_study != null
                        && (!connected_via_study.trim().equals(""))) {

                    connected_via_study = "studied together at "
                            + connected_via_study;
                }

                String shineid = friends.shine_id;
                String nameUser = friends.name;

                String user_id = userProfileVO.candidate_id;
                if (fromWhichPage == 1 || fromWhichPage == 4) {
                    KonnectPopupWindow.dismissInstance();
                    Bundle b = new Bundle();
                    b.putInt(SendMailActivity.TO_SCREEN, SendMailActivity.SHINE_MAIL_SCREEN);
                    b.putString("user_id", user_id);
                    b.putString("friend_id", shineid);
                    b.putString("recipient", nameUser);
                    b.putString("job_title", jobTitle);
                    b.putString("organization", organizationName);
                    b.putInt("fromWhich", 1);
                    b.putString("connectedViaStudy", connected_via_study);
                    b.putSerializable("model", generateReferral(position));
                    b.putString("location", jobLocation==null?"":jobLocation);
                    b.putString("experience", jobExperience==null?"":jobExperience);
                    b.putString("connectedViaWork", connected_via_work);
                    b.putString("mailCopy", mailCopy);
                    b.putString("JobID", jobID);
                    Intent intent = new Intent(activity, SendMailActivity.class);
                    intent.putExtras(b);
                    activity.startActivity(intent);
//                    ((BaseActivity) activity)
//                            .setFragment(ShineMailFragment.newInstance(1, user_id,
//                                    shineid, nameUser, jobTitle,
//                                    organizationName, connected_via_study,
//                                    connected_via_work, mailCopy, jobID, generateReferral(position), jobExperience, jobLocation));
                } else {
                    KonnectPopupWindow.dismissInstance();
                    Bundle b = new Bundle();
                    b.putInt(SendMailActivity.TO_SCREEN, SendMailActivity.SHINE_MAIL_SCREEN);
                    b.putString("user_id", user_id);
                    b.putString("friend_id", shineid);
                    b.putString("recipient", nameUser);
                    b.putString("job_title", null);
                    b.putString("organization", null);
                    b.putInt("fromWhich", 2);
                    b.putString("connectedViaStudy", connected_via_study);
                    b.putSerializable("model", null);
                    b.putString("location", jobLocation==null?"":jobLocation);
                    b.putString("experience", jobExperience==null?"":jobExperience);
                    b.putString("connectedViaWork", connected_via_work);
                    b.putString("mailCopy", mailCopy);
                    b.putString("JobID", jobID);
                    Intent intent = new Intent(activity, SendMailActivity.class);
                    intent.putExtras(b);
                    activity.startActivity(intent);
//                    ((BaseActivity) activity)
//                            .setFragment(ShineMailFragment.newInstance(2, user_id,
//                                    shineid, nameUser, null, null,
//                                    connected_via_study, connected_via_work,
//                                    mailCopy, jobID, null, jobExperience, jobLocation));
                }
            }
        });

    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{

        public LinearLayout ll;
        public ImageView friendImage;
        public TextView worked_with, study_with;
        public TextView contactName;
        public View rlmore;
        public View btnPhone;
        public View btnwhatsapp;
        public ImageButton btnLinkdin;
        public ImageButton btnShine;
        public View btnEmail;
        public View konnect;
        public TextView konnect_text;
        public RecyclerView recyclerView;

        public CustomViewHolder(View convertView, RecyclerView recyclerView){

            this(convertView);
            this.recyclerView = recyclerView;

        }



        public CustomViewHolder(View convertView) {
            super(convertView);
            ll = (LinearLayout) convertView
                    .findViewById(R.id.contact_info);
            friendImage = (ImageView) convertView
                    .findViewById(R.id.image_thumbnail);
            worked_with = (TextView) ll.findViewById(R.id.work_connection);
            study_with = (TextView) ll.findViewById(R.id.study_connection);
            contactName = (TextView) ll.findViewById(R.id.ContactName);
            rlmore = LayoutInflater.from(activity).inflate(R.layout.konnect_more_layout, new LinearLayout(activity), false);
            btnPhone = rlmore.findViewById(R.id.btnPhone);
            btnwhatsapp = rlmore.findViewById(R.id.btnWhatsapp);
            btnLinkdin = (ImageButton) rlmore
                    .findViewById(R.id.btnLinkdin);
            btnShine = (ImageButton) rlmore
                    .findViewById(R.id.btnShine);
            btnEmail = rlmore.findViewById(R.id.btnEmail);
            konnect = convertView.findViewById(R.id.konnect);
            konnect_text = (TextView) convertView.findViewById(R.id.konnect_text);
        }
    }


    private int check_connection_source(String linkedin, String facebook,
                                        String email, String phone_number, String shine_id) {
        if (linkedin != null && (!linkedin.trim().equals("")))
            return 1;
        else if (facebook != null && (!facebook.trim().equals("")))
            return 2;
        else if (email != null && (!email.trim().equals("")))
            return 3;
        else if (phone_number != null && (!phone_number.trim().equals("")))
            return 4;
        else if (shine_id != null && (!shine_id.trim().equals("")))
            return 5;
        else
            return 6;
    }


    private ReferralRequestModel generateReferral(int position) {

        ReferralRequestModel model = new ReferralRequestModel();
        model.setJob_id(jobID);

        String url = URLConfig.SEND_REFERRAL_URL;

        url = url.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

        model.setUrl(url);

        DiscoverModel.Company.CompanyData.Friends friend = friendLists.get(position);


        if (friend.shine_id != null && !friend.shine_id.equals(""))
            model.setReferrer_sid(friend.shine_id);
        else
            model.setReferrer_uid(friend.uid);

        model.setCandidate_name(userProfileVO.full_name);
        model.setReferrer_name(friend.name);
        model.setReferrer_image_url(friend.linkedin_image_url);


        int source = check_connection_source(friend.linkedin_id,
                friend.facebook_url, friend.emails, friend.phones,
                friend.shine_id);

        String connected_via_work;// = worked_together.get(position);
        String connected_via_study;// = study_together.get(position);
        model.setOrganization("");
        int mode = 0;
        if (source == 1) {
            mode = 3;
        } else if (source == 3) {
            mode = 2;
        } else if (source == 4) {
            mode = 1;
        } else if (source == 5
                && (!friend.direct_connection)) {
            connected_via_work = friend.worked_together;
            connected_via_study = friend.studied_together;

            if (connected_via_work != null
                    && (!connected_via_work.trim().equals(""))) {
                mode = 4;
                model.setOrganization(connected_via_work);
            }

            if (connected_via_study != null
                    && (!connected_via_study.trim().equals(""))) {
                mode = 5;
                model.setOrganization(connected_via_study);
            }
        }

        model.setMode(mode);

        String referral_ID = UUID.randomUUID().toString();

        model.setReferral_id(referral_ID);


        Log.d("REF", "jobid: " + model.getJob_id() +
                "\n refuid: " + model.getReferrer_uid() +
                "\n refsid: " + model.getReferrer_sid() +
                "\n refname: " + model.getReferrer_name() +
                "\n candname: " + model.getCandidate_name() +
                "\n refimgurl " + model.getReferrer_image_url() +
                "\n org: " + model.getOrganization() +
                "\n refrlId: " + model.getReferral_id() +
                "\n url: " + model.getUrl() +
                "\n mode: " + model.getMode());

        return model;
    }


    public String SendReferral(int position) {
        if (fromWhichPage == 2)//we are from KonnectFrag, no info that a job is present or not.
            return "";
        ReferralRequestModel model = new ReferralRequestModel();
        model.setJob_id(jobID);

        String url = URLConfig.SEND_REFERRAL_URL;

            url = url.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);

        model.setUrl(url);

        DiscoverModel.Company.CompanyData.Friends friend = friendLists.get(position);


        if (friend.shine_id != null && !friend.shine_id.equals(""))
            model.setReferrer_sid(friend.shine_id);
        else
            model.setReferrer_uid(friend.uid);

        model.setCandidate_name(userProfileVO.full_name);
        model.setReferrer_name(friend.name);
        model.setReferrer_image_url(friend.linkedin_image_url);


        int source = check_connection_source(friend.linkedin_id,
                friend.facebook_url, friend.emails, friend.phones,
                friend.shine_id);

        String connected_via_work;// = worked_together.get(position);
        String connected_via_study;// = study_together.get(position);
        model.setOrganization("");
        int mode = 0;
        if (source == 1) {
            mode = 3;
        } else if (source == 3) {
            mode = 2;
        } else if (source == 4) {
            mode = 1;
        } else if (source == 5
                && (!friend.direct_connection)) {
            connected_via_work = friend.worked_together;
            connected_via_study = friend.studied_together;

            if (connected_via_work != null
                    && (!connected_via_work.trim().equals(""))) {
                mode = 4;
                model.setOrganization(connected_via_work);
            }

            if (connected_via_study != null
                    && (!connected_via_study.trim().equals(""))) {
                mode = 5;
                model.setOrganization(connected_via_study);
            }
        }

        model.setMode(mode);

        String referral_ID = UUID.randomUUID().toString();

        model.setReferral_id(referral_ID);


        Log.d("REF", "jobid: " + model.getJob_id() +
                "\n refuid: " + model.getReferrer_uid() +
                "\n refsid: " + model.getReferrer_sid() +
                "\n refname: " + model.getReferrer_name() +
                "\n candname: " + model.getCandidate_name() +
                "\n refimgurl " + model.getReferrer_image_url() +
                "\n org: " + model.getOrganization() +
                "\n refrlId: " + model.getReferral_id() +
                "\n url: " + model.getUrl() +
                "\n mode: " + model.getMode());


        Intent i = new Intent(activity, SendRefferalService.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("model", model);
        i.putExtra("bundle", bundle);
        activity.startService(i);




        return URLConfig.VIEW_MESSAGE_URL_ENCRYPTED +
                ReferralCipher.cipher(model.getJob_id() + "/" +  userProfileVO.candidate_id
                        + "/" + model.getReferral_id(), ReferralCipher.ENC_MODE) + "/";


    }






    Dialog auth_dialog;
    WebView web;

    private void LinkedinConnect() {


        auth_dialog = new Dialog(activity,
                android.R.style.Theme_Translucent_NoTitleBar);
        auth_dialog.setContentView(R.layout.auth_dialog);
        web = (WebView) auth_dialog.findViewById(R.id.webv);
        auth_dialog.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

        String authUrl = LinkedinConstants.getAuthorizationUrl();
        web.loadUrl(authUrl);
        web.getSettings().setJavaScriptEnabled(true);

        web.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                auth_dialog.findViewById(R.id.progress_bar).setVisibility(
                        View.GONE);
                web.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String authorizationUrl) {

                if (authorizationUrl.startsWith(LinkedinConstants.REDIRECT_URI)) {
                    Uri uri = Uri.parse(authorizationUrl);
                    Log.d("LINKED IN AUTH RESPONSE", authorizationUrl);

                    String error = uri.getQueryParameter("error");
                    if (error != null) {
                        Toast.makeText(
                                activity,
                                "Error occured : " + error,
                                Toast.LENGTH_SHORT).show();
                        auth_dialog.dismiss();
                        return true;
                    }

                    String stateToken = uri
                            .getQueryParameter(LinkedinConstants.STATE_PARAM);
                    if (stateToken == null
                            || !stateToken.equals(LinkedinConstants.STATE)) {
                        Toast.makeText(activity,
                                "Sorry..state token doesn't match",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    String authorizationToken = uri
                            .getQueryParameter(LinkedinConstants.RESPONSE_TYPE_VALUE);
                    if (authorizationToken == null) {
                        Toast.makeText(activity,
                                "Sorry..you did not allow authorization",
                                Toast.LENGTH_SHORT).show();
                        return true;
                    }

                    String accessTokenUrl = LinkedinConstants
                            .getAccessTokenUrl(authorizationToken);

                    new GetAccessTokenAsyncTask().execute(accessTokenUrl);

                } else {
                    web.loadUrl(authorizationUrl);
                }
                return true;
            }

        });
        auth_dialog.setTitle("LinkedIn Authentication");
        if (!activity.isFinishing())
            auth_dialog.show();
    }


    private class GetAccessTokenAsyncTask extends AsyncTask<String, Void, Boolean> {


        ProgressDialog customDialog;

        @Override
        protected void onPreExecute() {

            customDialog = new ProgressDialog(activity, ProgressDialog.STYLE_SPINNER);
            customDialog.setMessage("Authorizing...");
            customDialog.setCancelable(false);
            customDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... urls) {
            if (urls.length > 0) {
                String url = urls[0];
                try {
                    StringBuilder content = new StringBuilder();
                    URL mUrl = new URL(url);
                    HttpURLConnection connection = (HttpURLConnection) mUrl.openConnection();
                    if (connection.getResponseCode() == 200) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null)
                            content.append(line).append("\n");
                        bufferedReader.close();
                        JSONObject resultJson = new JSONObject(content.toString());
                        int expiresIn = resultJson.has("expires_in") ? resultJson
                                .getInt("expires_in") : 0;
                        String accessToken = resultJson.has("access_token") ? resultJson
                                .getString("access_token") : null;
                        if (expiresIn > 0 && accessToken != null) {
                            Log.i("DEBUG", "This is the access Token: "
                                    + accessToken + ". It will expires in "
                                    + expiresIn + " secs");
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.SECOND, expiresIn);
                            long expireDate = calendar.getTimeInMillis();

                            ShineSharedPreferences.setLinkedinAccessToken(accessToken, activity);
                            ShineSharedPreferences.setLinkedinAccessExpiry(expireDate+"", activity);

                            return true;
                        }
                        else {
                            return false;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean status) {
            customDialog.dismiss();
            if (status) {
                auth_dialog.hide();
                KonnectPopupWindow.dismissInstance();
                DiscoverModel.Company.CompanyData.Friends friend = friendLists.get(positionForLinkedIn);
                if (fromWhichPage != 2) {
                    Intent intent = new Intent(activity, SendMailActivity.class);
                    Bundle args = new Bundle();
                    args.putInt(SendMailActivity.TO_SCREEN, SendMailActivity.LINKEDIN_MAIL_SCREEN);
                    args.putInt("fromWhich", 1);
                    args.putString("recipientProfileID", friend.linkedin_id);
                    args.putString("recipientName", friend.name);
                    args.putString("jobUrl", jobUrl);
                    args.putSerializable("model", generateReferral(positionForLinkedIn));
                    args.putString("recipientProfileUrl", friend.linkedin_url);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(activity, SendMailActivity.class);
                    Bundle args = new Bundle();
                    args.putInt(SendMailActivity.TO_SCREEN, SendMailActivity.LINKEDIN_MAIL_SCREEN);
                    args.putInt("fromWhich", 2);
                    args.putString("recipientProfileID", friend.linkedin_id);
                    args.putString("recipientName", friend.name);
                    args.putString("jobUrl", jobUrl);
                    args.putSerializable("model", null);
                    args.putString("recipientProfileUrl", friend.linkedin_url);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }

            } else {
                Toast.makeText(activity,
                        "Unable to get authorization.",
                        Toast.LENGTH_SHORT).show();
                ShineCommons.trackShineEvents("LinkedinLoginFail","JobReferralConnect",activity);
            }
        }

    }

}