package com.net.shine.views;


import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.net.shine.R;


public class KonnectPopupWindow extends PopupWindow {


    private static KonnectPopupWindow mInstance;

    public static KonnectPopupWindow getInstance(Context context) {


        if (mInstance == null) {
            mInstance = new KonnectPopupWindow(context);
            mInstance.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        dismissInstance();
                    }
                    return false;
                }
            });
            mInstance.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            mInstance.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            mInstance.setTouchable(true);
            mInstance.setOutsideTouchable(true);
            mInstance.setFocusable(false);
            mInstance.setBackgroundDrawable((new ColorDrawable(android.graphics.Color.TRANSPARENT)));

        }
        return mInstance;
    }

    KonnectPopupWindow(Context context) {
        super(context);
    }


    public void show(View anchor, View cotentView, boolean showAbove) {
        dismiss();
        setContentView(cotentView);

        if (showAbove) {
            int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            View content = getContentView();
            content.setBackgroundResource(R.drawable.connect_hover_mod);
            content.measure(widthMeasureSpec, heightMeasureSpec);

            int anchorHeight = anchor.getMeasuredHeight();
            int popupHeight = content.getMeasuredHeight();
            if (anchorHeight <= 0 || anchorHeight >= popupHeight) {
                anchorHeight = 0;//assume a default value
            }
            showAsDropDown(anchor, 0, -(anchorHeight + popupHeight));
        } else {
            showAsDropDown(anchor, 0, 0);
        }

        isShowing = true;
    }

    private static boolean isShowing = false;

    public static boolean dismissInstance() {
        try {
            if (mInstance != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mInstance.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
                if (isShowing) {
                    isShowing = false;
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    public static boolean isInstanceShowing() {
        return mInstance != null && isShowing;
    }


}