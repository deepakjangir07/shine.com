package com.net.shine.models;


import java.io.Serializable;

public class GmailProfileModel implements Serializable {
	
	  private String n = "";
	  private String e = "";
	  private String p = "";

	  public GmailProfileModel() {}

	  public GmailProfileModel(GmailProfileModel model) {
	    this.n = model.n;
	    this.e = model.e;
	    this.p = model.p;
	  }

	  public GmailProfileModel(String username, String email, String phone) {
	    this.n = username;
	    this.e = email;
	    this.p = phone;

	  }

	  public void setUsername(String username) {
	    this.n = username;
	  }
	  public String getUsername() {
	    return this.n;
	  }
	  public void setEmail(String email) {
	    this.e = email;
	  }
	  public String getEmail() {
	    return this.e;
	  }

	public String getPhone() {
		return p;
	}

	public void setPhone(String phone) {
		this.p = phone;
	}
	
	public String toString()
	{
		return "n : "+n+" e : "+e+" p :"+p;
	}
	
	}