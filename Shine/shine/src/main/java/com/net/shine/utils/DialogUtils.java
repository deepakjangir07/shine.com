package com.net.shine.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.HomeActivity;
import com.net.shine.activity.SplashScreen;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.ChildParentSpinnerWithEditTextFrag;
import com.net.shine.fragments.dialogs.CheckBoxSpinnerFragment;
import com.net.shine.fragments.dialogs.CheckboxChildParentSpinnerFrag;
import com.net.shine.fragments.dialogs.ChildParentSpinnerFrag;
import com.net.shine.fragments.dialogs.SpinnerFragment;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.auth.ShineAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.net.shine.activity.BaseActivity.TYPE_FORCED;

/**
 * Created by ujjawal-work on 11/08/16.
 */

public class DialogUtils {
    private static Dialog alertDialog = null;


    public static final int ERR_NO_IMG = 0;
    public static final int ERR_NO_EMAIL = R.drawable.error_no_email;
    public static final int ERR_NO_SEARCH_RESULT = R.drawable.error_no_search_result;
    public static final int ERR_NO_VIEWS = R.drawable.error_no_profile_view_main;
    public static final int ERR_TECHNICAL = R.drawable.error_technical;
    public static final int ERR_GENERIC = R.drawable.error_no_result;
    public static final int ERR_NO_JOBS_WITH_CONNECTION = R.drawable.error_no_jobs_connection;
    public static final int ERR_NO_REFERRAL_RECEIVED = R.drawable.error_no_referral_received;
    public static final int ERR_NO_REFERRAL_REQUESTED = R.drawable.error_no_referral_requested;
    public static final int ERR_NO_SAVED_JOBS = R.drawable.error_jobshortlisted;
    public static final int ERR_NO_CUSTOM_ALERT = R.drawable.error_no_custom_alert;
    public static final int ERR_NO_KONNECT_COMPANIES = R.drawable.error_konnect_companies;

    public static final int ERR_NO_NOTIFICATION = R.drawable.error_notification;



    public static void showErrorActionableMessage(final BaseActivity mActivity, View errorView, String errorLine1,
                                                  String errorLine2, String buttonText, int type, final BaseFragment fragment){
        if(errorView.findViewById(R.id.error_image)!=null){
            ImageView errorImg = (ImageView) errorView.findViewById(R.id.error_image);
            errorImg.setImageResource(type);
            errorImg.setVisibility(View.VISIBLE);
        }
        errorView.setVisibility(View.VISIBLE);
        TextView errorMsg1= (TextView) errorView.findViewById(R.id.error_line1);
        TextView errorMsg2= (TextView) errorView.findViewById(R.id.error_line2);
        Button actionButton= (Button) errorView.findViewById(R.id.action_button);

        if (!TextUtils.isEmpty(errorLine1))
            errorMsg1.setText(errorLine1);
        else
            errorMsg1.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(errorLine2))
            errorMsg2.setText(errorLine2);
        else
            errorMsg2.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(buttonText))
            actionButton.setText(buttonText);
        else
            actionButton.setVisibility(View.GONE);
        if(fragment!=null && !TextUtils.isEmpty(buttonText)){
                    actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.replaceFragmentWithBackStack(fragment);
                }
            });
        }
    }

    public static void showErrorActionableMessage(final Activity mActivity, View errorView, String errorLine1,
                                                  Spannable errorLine2, String buttonText, int type, final Intent intent){
        if(errorView.findViewById(R.id.error_image)!=null){
            ImageView errorImg = (ImageView) errorView.findViewById(R.id.error_image);
            errorImg.setImageResource(type);
            errorImg.setVisibility(View.VISIBLE);
        }
        errorView.setVisibility(View.VISIBLE);
        TextView errorMsg1= (TextView) errorView.findViewById(R.id.error_line1);
        TextView errorMsg2= (TextView) errorView.findViewById(R.id.error_line2);
        Button actionButton= (Button) errorView.findViewById(R.id.action_button);

        if (!TextUtils.isEmpty(errorLine1))
            errorMsg1.setText(errorLine1);
        else
            errorMsg1.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(errorLine2)){
            errorMsg2.setText(errorLine2);
            errorMsg2.setMovementMethod(LinkMovementMethod.getInstance());
            errorMsg2.setHighlightColor(Color.TRANSPARENT);
        }
        else
            errorMsg2.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(buttonText))
            actionButton.setText(buttonText);
        else
            actionButton.setVisibility(View.GONE);
        if(intent!=null && !TextUtils.isEmpty(buttonText)){
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mActivity.startActivity(intent);
                }
            });
        }
    }


    public static void showErrorMsg(View loadingView, String errorMsg, int type) {


        if (type != ERR_NO_IMG && loadingView.findViewById(R.id.error_img) != null) {
            ImageView errImg = (ImageView) loadingView.findViewById(R.id.error_img);
            errImg.setImageResource(type);
            errImg.setVisibility(View.VISIBLE);
        } else if (loadingView.findViewById(R.id.error_img) != null) {
            loadingView.findViewById(R.id.error_img).setVisibility(View.GONE);
        }

        loadingView.setVisibility(View.VISIBLE);
        if(loadingView.findViewById(R.id.progress_bar)!=null)
            loadingView.findViewById(R.id.progress_bar).setVisibility(View.GONE);
        if(loadingView.findViewById(R.id.loader)!=null)
            loadingView.findViewById(R.id.loader).setVisibility(View.GONE);
        ((TextView) loadingView.findViewById(R.id.progress_text))
                .setText(errorMsg);

    }

    public static void showErrorToast(String errorMsg) {


        Toast.makeText(MyApplication.getInstance().getApplicationContext(), errorMsg+"", Toast.LENGTH_SHORT).show();

    }


    public static void showErrorToastLong(String errorMsg) {


        Toast.makeText(MyApplication.getInstance().getApplicationContext(), errorMsg+"", Toast.LENGTH_LONG).show();

    }






    public static void showNewSingleSpinnerDialog(String title, final TextView tView,
                                                  final String[] list, Context context, boolean checkLimit) {

        try {
            DialogFragment fragment = new CheckBoxSpinnerFragment(title, tView, list, checkLimit);
            BaseActivity act = (BaseActivity) context;
            android.support.v4.app.FragmentTransaction ft = act.getSupportFragmentManager().beginTransaction();
            Fragment prev = act.getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            fragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showNewMultiSpinnerDialog(String title, final TextView tView,
                                                 final String[] list, Context context, boolean limit) {

        try {
            DialogFragment fragment = new CheckboxChildParentSpinnerFrag(title, tView, list, limit);
            BaseActivity act = (BaseActivity) context;
            android.support.v4.app.FragmentTransaction ft = act.getSupportFragmentManager().beginTransaction();
            Fragment prev = act.getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            fragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showNewSingleSpinnerRadioDialog(String title, final TextView tView,
                                                       final String[] list, Context context) {


        try {
            DialogFragment fragment = new SpinnerFragment(title, tView, list);
            BaseActivity act = (BaseActivity) context;
            android.support.v4.app.FragmentTransaction ft = act.getSupportFragmentManager().beginTransaction();
            Fragment prev = act.getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            fragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public static void showNewMultiSpinnerDialogRadio(String title, final TextView tView,
                                                      final String[] list, Context context) {

        try {
            DialogFragment fragment = new ChildParentSpinnerFrag(title, tView, list);
            BaseActivity act = (BaseActivity) context;
            android.support.v4.app.FragmentTransaction ft = act.getSupportFragmentManager().beginTransaction();
            Fragment prev = act.getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            fragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showNewMultiSpinnerwithEdittextDialogRadio(String title, final TextView tView,
                                                                  final String[] list,String other_edu, Context context) {

        try {
            DialogFragment fragment = new ChildParentSpinnerWithEditTextFrag(title, tView, list,other_edu);


            BaseActivity act = (BaseActivity) context;
            android.support.v4.app.FragmentTransaction ft = act.getSupportFragmentManager().beginTransaction();
            Fragment prev = act.getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            fragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showNotification(Context mContext, String msg) {


        try {

            if (mContext == null)
                return;

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                    .setSmallIcon(R.drawable.ic_noti)
                    .setContentTitle(mContext.getString(R.string.app_name))
                    .setContentText(msg)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .setBigContentTitle(mContext.getString(R.string.app_name))
                            .bigText(msg))
                    .setAutoCancel(true);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
            UserStatusModel userProfileVO = ShineSharedPreferences.getUserInfo(mContext);

            Intent notificationIntent;
            notificationIntent=new Intent(mContext, HomeActivity.class);
            notificationIntent.putExtra(ShineAuthUtils.REQUESTED_TYPE_KEY, ShineAuthUtils.REQUESTED_TO_KONNECT);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            stackBuilder.addNextIntent(notificationIntent);
            PendingIntent resultPendingIntent = stackBuilder
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(8, mBuilder.build());


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static Dialog showCustomAlertsNoTitle(final Activity mActivity,
                                                 String msg) {
        final Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_alert_dialog_no_title);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            TextView ratingMsg = (TextView) alertDialog
                    .findViewById(R.id.dialog_msg);
            if (msg.contains("088667")) {
                int l = msg.length();
                SpannableStringBuilder sb_mob = new SpannableStringBuilder(msg);
                sb_mob.setSpan(new UnderlineSpan(), l - 14, l - 1,
                        Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                sb_mob.setSpan(
                        new ForegroundColorSpan(Color.parseColor("#3E7BBE")),
                        l - 14, l - 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                ratingMsg.setText(sb_mob);
                ratingMsg.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri
                                .parse("tel:" + "08866788667"));
                        mActivity.startActivity(intent);
                    }
                });
            } else {
                ratingMsg.setText(msg);
            }
            alertDialog.findViewById(R.id.ok_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            if (!mActivity.isFinishing())
                alertDialog.show();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static void dialogDismiss(Dialog dialog)
    {
        try{
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideDialog(Dialog dialog){
        try{
            if (dialog != null && dialog.isShowing()) {
                dialog.hide();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showDialog(Dialog dialog){
        try{
            if (dialog != null) {
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void showLoading(Activity mActivity, String msg,
                                   View loadingView) {
        try {
            Log.d("DEBUG ", "SHOW LOADING CALLED " + loadingView);


            if (loadingView.findViewById(R.id.loader) != null) {
                ImageView loader = (ImageView) loadingView.findViewById(R.id.loader);
                loader.setVisibility(View.VISIBLE);
                AnimationDrawable drawable = (AnimationDrawable) loader.getDrawable();
                drawable.setCallback(loader);
                drawable.setVisible(true, true);
                drawable.start();
            }

            if (loadingView.findViewById(R.id.progress_bar) != null) {
                loadingView.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
            }

            if (loadingView.findViewById(R.id.msg_tv_spannable) != null)
                loadingView.findViewById(R.id.msg_tv_spannable).setVisibility(
                        View.GONE);

            ((TextView) loadingView.findViewById(R.id.progress_text))
                    .setText(mActivity.getString(R.string.loading));
            loadingView.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static Dialog showCustomProgressDialog(final Activity mActivity, String msg) {
         Dialog alertDialog = null;
        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cust_prg_dialog);
            alertDialog.setCancelable(false);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            alertDialog.findViewById(R.id.loading_cmp).setVisibility(
                    View.VISIBLE);
            if (alertDialog.findViewById(R.id.loader) != null) {
                ImageView loader = (ImageView) alertDialog.findViewById(R.id.loader);
                loader.setVisibility(View.VISIBLE);
                AnimationDrawable drawable = (AnimationDrawable) loader.getDrawable();
                drawable.setCallback(loader);
                drawable.setVisible(true, true);
                drawable.start();
            }
            TextView progressText = (TextView) alertDialog
                    .findViewById(R.id.progress_text);
            progressText.setText(msg);

           if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static Dialog getLinkedinRedirectDialog(final Activity mActivity, String email){
        Dialog  alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_linkedin_flow);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        TextView progressText = (TextView) alertDialog
                .findViewById(R.id.text_linkedin_sync);
        if(!TextUtils.isEmpty(email)) {
            progressText.setText(Html.fromHtml("Sorry, there's no Shine account associated with <b>" + email +"</b>"));
        }
        return alertDialog;
    }

    public static Dialog showAlertWithTwoButtons(Activity mActivity) {
        Dialog  alertDialog = new Dialog(mActivity);
        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_two_button_dialog);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (!mActivity.isFinishing())
                alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static Dialog showEmailDialog(final Activity mActivity) {
        Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.custom_email_dialog);

            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            alertDialog.setCancelable(true);


            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static Dialog AlertSaveDialog(final Activity mActivity) {
        Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.alert_save_dialog);

            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            alertDialog.setCancelable(true);



            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }
    public static Dialog showOTPDialog(final Activity mActivity, String mob) {
        Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_otp_dialog);
            alertDialog.setCancelable(false);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if(TextUtils.isEmpty(mob))
                mob = "";
            TextView progressText = (TextView) alertDialog
                    .findViewById(R.id.detail_text);

            final TextView timerTV  = (TextView) alertDialog.findViewById(R.id.timer);
            final View resendLayoutView = alertDialog.findViewById(R.id.resend_layout);

            final CountDownTimer countDownTimer = new CountDownTimer(1000*60, 1000) {
                public void onTick(long millisUntilFinished) {
                    try {
                        timerTV.setText("Resend in " + (millisUntilFinished/1000) + " sec");
                    }catch (Exception e){
                        cancel();
                        e.printStackTrace();
                    }
                }
                public void onFinish() {
                    try {
                        timerTV.setVisibility(View.GONE);
                        resendLayoutView.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        cancel();
                        e.printStackTrace();
                    }
                }
            };
            countDownTimer.start();
            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    try {
                        countDownTimer.cancel();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });


            progressText.setText("We have sent an One Time Password (OTP) request to your mobile number " + mob);

            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static Dialog showCustomResumeAlerts(final Activity mActivity) {
        Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_resume_midout_dialog);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }





    public static Dialog showSyncDialog(Activity mActivity, String name) {
        Dialog alertDialog = new Dialog(mActivity);
        try {
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_import_contact_dialog);
            alertDialog.setCancelable(false);

            ImageView imageView = (ImageView) alertDialog
                    .findViewById(R.id.sync_icon);
            ProgressBar bar = (ProgressBar) alertDialog
                    .findViewById(R.id.syncBar);
            bar.setIndeterminate(true);

            if (name.equals("linkedin"))
                imageView.setBackgroundResource(R.drawable.ic_linkedin);
            else
                imageView.setBackgroundResource(R.drawable.ic_gmail);

            alertDialog.setTitle("");
            if (!mActivity.isFinishing())
                alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static Dialog showCustomLoginAlerts(final Activity mActivity, String msg) {
        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_login_dialog);

            if (msg != null) {
                TextView tView = (TextView) alertDialog
                        .findViewById(R.id.dialog_msg);
                tView.setText(msg);
            }
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!mActivity.isFinishing())
                alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }




    public static Dialog rateAppAlert(Activity mActivity) {

        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.cus_rate_dialog);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!mActivity.isFinishing())
                alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }


    public static boolean showRateIfNeeded(final BaseActivity mActivity)
    {
            boolean isShown = false;
            SharedPreferences searchPref = mActivity
                    .getSharedPreferences(
                            ShineSharedPreferences.RATE_APP_PREF, 0);
            int ratecount = searchPref.getInt(
                    ShineSharedPreferences.RATE_APP_COUNT, 0);
            boolean dontflag = searchPref.getBoolean(
                    ShineSharedPreferences.DONT_ASK_AGAIN, false);
            Calendar cal = Calendar.getInstance();
            String current = cal.getTime().toString();


            String getDate = searchPref.getString(
                    ShineSharedPreferences.RATE_APP_DATE, current);
            SimpleDateFormat sdf = new SimpleDateFormat(
                    "EEE MMM dd hh:mm:ss zzz yyyy");

            try {
                Date cur = sdf.parse(current);
                Date saved = sdf.parse(getDate);
                int comparison = cur.compareTo(saved);

                if (comparison == -1) {
                    //reset rate count
                    ShineSharedPreferences.updateRateAppCount(mActivity,
                            false, false, false, true);
                }
                if (ratecount >= URLConfig.RATE_COUNT && !dontflag
                        && (comparison != -1))
                {
                    final Dialog rateDialog = rateAppAlert(mActivity);
                    isShown = true;
                    //reset rate count
                    ShineSharedPreferences.updateRateAppCount(mActivity, false, false,
                            false, true);
                    rateDialog.findViewById(R.id.not_container).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            LinearLayout feedbackContainer =(LinearLayout) rateDialog.findViewById(R.id.feeback_container);

                            final Animation slideUp = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.slide_up);
                            final Animation slideDown = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.slide_down);


                            if(feedbackContainer.getVisibility()==View.VISIBLE){

                                feedbackContainer.startAnimation(slideDown);
                                feedbackContainer.setVisibility(View.GONE);
                            }
                            else
                            {
                                feedbackContainer.startAnimation(slideUp);
                                feedbackContainer.setVisibility(View.VISIBLE);
                            }




                        }
                    });
                    rateDialog.findViewById(R.id.yes_container).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //set date
                            ShineSharedPreferences.updateRateAppCount(mActivity, false,
                                    false, true, false);
                            Intent rate_app = new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(URLConfig.APP_GOOGLE_PLAY_LINK));
                            mActivity.startActivity(rate_app);
                            dialogDismiss(rateDialog);


                            ShineCommons.trackShineEvents("RateApp", "RateAppDialog", mActivity);

                        }
                    });
                    rateDialog.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            UserStatusModel userProfileVO;
                            EditText getFeedBack = (EditText) rateDialog.findViewById(R.id.feedback);
                            String getText=getFeedBack.getText().toString();
                            TextInputLayout txtfeebacklayout=(TextInputLayout) rateDialog.findViewById(R.id.feeback_inputlayout);

                            TextChangeListener.addListener(getFeedBack,txtfeebacklayout,mActivity);

                            if(getText.length()>0) {

                                userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
                                String mydate = java.text.DateFormat.getDateTimeInstance().format(
                                        Calendar.getInstance().getTime());
                                String myDeviceModel = android.os.Build.MODEL;
                                String myVersion = android.os.Build.VERSION.RELEASE;
                                PackageInfo pInfo = null;
                                try {
                                    pInfo = mActivity.getPackageManager().getPackageInfo(
                                            mActivity.getPackageName(), 0);
                                } catch (PackageManager.NameNotFoundException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                String subject = "Feedback-" + userProfileVO.full_name + "-" + mydate;



                                String verNum="";
                                if(pInfo!=null){
                                    verNum= pInfo.versionName;
                                }


                                Type type = new TypeToken<EmptyModel>(){}.getType();
                                VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                                    @Override
                                    public void OnDataResponse(Object object, String tag) {
                                        Log.d("--resp",object.toString());
                                        //set date
                                        ShineSharedPreferences.updateRateAppCount(mActivity, false,
                                                false, true, false);
                                        ShineCommons.trackShineEvents("FeedbackSend", "RateAppDialog", mActivity);

                                    }

                                    @Override
                                    public void OnDataResponseError(String error, String tag) {
                                        Log.d("--err",error);
                                    }
                                },
                                        URLConfig.FEEDBACK_URL, type);
                                HashMap<String, String> postParams = new HashMap<>();
                                postParams.put("candidate_id",  userProfileVO.candidate_id);
                                postParams.put("subject",  subject);
                                postParams.put("time",  mydate);
                                postParams.put("device",  myDeviceModel);
                                postParams.put("os_version",  myVersion);
                                postParams.put("app_version",  verNum);
                                postParams.put("feedback", getText);
                                request.setUsePostMethod(postParams);

                                request.execute("feedbackFrag");

                               dialogDismiss(rateDialog);



                            }

                            else
                            {
                                txtfeebacklayout.setError("Write your feedback before sending.");
                            }
                        }
                    });



                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        return isShown;
        }

    public static Dialog updateDialog(Activity mActivity) {

        try {
            alertDialog = new Dialog(mActivity);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(R.layout.update_dialog);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return alertDialog;
    }

    public static void showUpdateDialog(final Activity activity, String type) {
        if (type.equals(TYPE_FORCED)) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.update);
            Button dialogButton = (Button) dialog.findViewById(R.id.update_now);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("inside click","force update"+activity);

                    try {
                        Intent update_app = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=com.net.shine"));
                        if(activity instanceof SplashScreen){
                            ((SplashScreen)activity).startActivityFromSuper(update_app);
                        } else {
                            activity.startActivity(update_app);
                        }
                    } catch (Exception e) {
                        Intent update_app = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(URLConfig.APP_GOOGLE_PLAY_LINK));
                        if(activity instanceof SplashScreen){
                            ((SplashScreen)activity).startActivityFromSuper(update_app);
                        } else {
                            activity.startActivity(update_app);
                        }
                        e.printStackTrace();
                    }

                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    System.exit(0);
                    //Exit whole app here...not just activity
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        } else {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.update_dialog);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            TextView updateText = (TextView) dialog
                    .findViewById(R.id.rate_button_text);
            TextView dialogMsg = (TextView) dialog
                    .findViewById(R.id.dialog_msg);
            updateText.setText(activity.getResources().getString(R.string.update_now_old));
            dialogMsg.setText(activity.getResources().getString(R.string.update_app_detail_old));
            dialog.findViewById(R.id.not_now).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtils.dialogDismiss(dialog);
                    if(activity instanceof SplashScreen) {
                        ((SplashScreen)activity).handleDelayedStuff();
                    }

                }
            });
            dialog.findViewById(R.id.dont_ask).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ShineSharedPreferences.setUpdateSkip(activity, true);
                    DialogUtils.dialogDismiss(dialog);
                    if(activity instanceof SplashScreen) {
                        ((SplashScreen)activity).handleDelayedStuff();
                    }
                }
            });
            dialog.findViewById(R.id.rate_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent update_app = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("market://details?id=com.net.shine"));
                        if(activity instanceof SplashScreen){
                            ((SplashScreen)activity).startActivityFromSuper(update_app);
                        } else {
                            activity.startActivity(update_app);
                        }
                    } catch (Exception e) {
                        Intent update_app = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(URLConfig.APP_GOOGLE_PLAY_LINK));
                        if(activity instanceof SplashScreen){
                            ((SplashScreen)activity).startActivityFromSuper(update_app);
                        } else {
                            activity.startActivity(update_app);
                        }
                        e.printStackTrace();
                    }
//                    DialogUtils.dialogDismiss(dialog);
//                    if(activity instanceof SplashScreen) {
//                        ((SplashScreen)activity).handleDelayedStuff();
//                    }
                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    System.exit(0);
                    //Exit whole app here...not just activity
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }


    }

