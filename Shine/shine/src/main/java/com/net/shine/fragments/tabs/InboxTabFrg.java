package com.net.shine.fragments.tabs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.net.shine.fragments.JobAlertFragment;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ujjawal-work on 22/08/16.
 */
public class InboxTabFrg extends TwoTabFrg{

    public static final int ALERT_MAIL_TAB = 0;
    public static final int RECRUITER_MAIL_TAB = 1;

    public static InboxTabFrg newInstance(Bundle args) {
        InboxTabFrg fragment = new InboxTabFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity.setTitle("Messages");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        setHasOptionsMenu(true);
        mActivity.showSelectedNavButton(mActivity.MAIL_OPTION);
        if (getArguments() != null) {
            currentSelected = getArguments().getInt(SELECTED_TAB, ALERT_MAIL_TAB);
        }
        else {
            currentSelected = ALERT_MAIL_TAB;
        }
    }

    @Override
    protected void PageSelected(int position) {
        super.PageSelected(position);

        Log.d(">>>>> ","pageSelected "+position);
        if(position==0) {
            ManualScreenTracker.manualTracker("Inbox-JobAlerts");
        }else if(position==1) {
            ManualScreenTracker.manualTracker("Inbox-RecruiterMail");
        }
    }

    @Override
    public List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<>();
        JobAlertFragment f1 = JobAlertFragment.newInstance(true);
        JobAlertFragment f2 = JobAlertFragment.newInstance(false);
        fList.add(f1);
        fList.add(f2);
        return fList;
    }

    @Override
    public ArrayList<String> getNames() {
        SharedPreferences pref = mActivity.getSharedPreferences(
                ShineSharedPreferences.PREF_NAME, 0);
        int jobAlerts = pref
                .getInt(ShineSharedPreferences.UNREAD_JOB_MAIL_COUNT, 0);
        int recruiterMails = pref
                .getInt(ShineSharedPreferences.UNREAD_RECRUITER_MAIL_COUNT, 0);
        ArrayList<String> tabName = new ArrayList<>();
        tabName.add("Job Alerts " + "(" + jobAlerts + ")");
        tabName.add("Recruiter Mail " + "(" + recruiterMails + ")");
        return tabName;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Messages");
        mActivity.showNavBar();
        mActivity.hideBackButton();
        mActivity.setActionBarVisible(true);

        mActivity.showSelectedNavButton(mActivity.MAIL_OPTION);
    }
}
