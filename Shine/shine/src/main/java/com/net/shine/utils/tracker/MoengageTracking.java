package com.net.shine.utils.tracker;

/**
 * Created by Deepak on 19/09/16.
 */


import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;
import com.moe.pushlibrary.utils.MoEHelperConstants;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.db.ShineSharedPreferences;

public class MoengageTracking {




    public static void moengageEventTrack(String category, String label, String event,
                                                  Context mActivity, PayloadBuilder builder)
    {
        try {
            builder.putAttrString("Label",label);
            builder.putAttrString("Category", category);

        } catch (Exception e) {
            e.printStackTrace();
        }
        MoEHelper.getInstance(mActivity).trackEvent(event, builder.build());


    }


    public static  void setLoginAttribute(Activity mActivity){
        try {
            UserStatusModel model = ShineSharedPreferences.getUserInfo(mActivity);
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);
            if(model!=null){

                Log.d(">>>>>Moengage","logged_in");

                MoEHelper.getInstance(mActivity).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID,
                        ShineSharedPreferences.getCandidateId(mActivity));
                MoEHelper.getInstance(mActivity).setUserAttribute("Loggedin_status",true);

                mFirebaseAnalytics.setUserProperty("unique_id", ShineSharedPreferences.getCandidateId(mActivity));
                mFirebaseAnalytics.setUserProperty("Loggedin_status","true");

            }
            else {
                Log.d(">>>>>Moengage","loggedout");

                MoEHelper.getInstance(mActivity).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_UNIQUE_ID,
                        ShineSharedPreferences.getGAID(mActivity));
                MoEHelper.getInstance(mActivity).setUserAttribute("Loggedin_status",false);

                mFirebaseAnalytics.setUserProperty("Loggedin_status", "false");
                mFirebaseAnalytics.setUserProperty("unique_id", ShineSharedPreferences.getGAID(mActivity));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setProfileAttributes(Activity mActivity){
        try {

            UserStatusModel model = ShineSharedPreferences.getUserInfo(mActivity);
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);

            if(model!=null){
                Log.d(">>>>>Moengage","profile"+model.full_name);
                Log.d(">>>>>Moengage","profile"+model.has_jobs);
                Log.d(">>>>>Moengage","profile"+model.has_education);
                Log.d(">>>>>Moengage","profile"+model.has_skills);
                Log.d(">>>>>Moengage","profile"+!model.is_resume_mid_out);
                Log.d(">>>>>Moengage","profile"+model.experience_in_years);


                MoEHelper.getInstance(mActivity).setUserAttribute(MoEHelperConstants.USER_ATTRIBUTE_USER_FIRST_NAME, model.full_name);
                MoEHelper.getInstance(mActivity).setUserAttribute("has_employment", model.has_jobs);
                MoEHelper.getInstance(mActivity).setUserAttribute("has_education", model.has_education);
                MoEHelper.getInstance(mActivity).setUserAttribute("has_skills",model.has_skills);
                MoEHelper.getInstance(mActivity).setUserAttribute("profile_completed",
                        model.has_jobs && model.has_education && model.has_skills);
                MoEHelper.getInstance(mActivity).setUserAttribute("resume_uploaded", !model.is_resume_mid_out);
                MoEHelper.getInstance(mActivity).setUserAttribute("work_experience", model.experience_in_years);



                mFirebaseAnalytics.setUserProperty("Name", model.full_name);
                mFirebaseAnalytics.setUserProperty("has_employment", String.valueOf(model.has_jobs));
                mFirebaseAnalytics.setUserProperty("has_education", String.valueOf(model.has_education));
                mFirebaseAnalytics.setUserProperty("has_skills",String.valueOf(model.has_skills));
                mFirebaseAnalytics.setUserProperty("profile_completed",
                        String.valueOf(model.has_jobs && model.has_education && model.has_skills));
                mFirebaseAnalytics.setUserProperty("resume_uploaded", String.valueOf(!model.is_resume_mid_out));
                mFirebaseAnalytics.setUserProperty("work_experience", String.valueOf(model.experience_in_years));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setSkillCount(Activity mActivity, int skill_count){
        try {
            Log.d(">>>>>Moengage","skillCount"+skill_count);
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);


            MoEHelper.getInstance(mActivity).setUserAttribute("skills_count",skill_count);

            mFirebaseAnalytics.setUserProperty("skills_count",String.valueOf(skill_count));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void setSalaryAttribute(Activity mActivity, Double salary){
        try {
            Log.d(">>>>>Moengage","salary"+salary);

            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);


            MoEHelper.getInstance(mActivity).setUserAttribute("salary", salary);
            mFirebaseAnalytics.setUserProperty("salary", String.valueOf(salary));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setRecruiterContactedCount(Activity mActivity, int recruiter_count){
        try {
            Log.d(">>>>>Moengage","whoviewed"+recruiter_count);
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);


            MoEHelper.getInstance(mActivity).setUserAttribute("recruiter_contact_count", recruiter_count);
            mFirebaseAnalytics.setUserProperty("recruiter_contact_count", String.valueOf(recruiter_count));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setSaveJobExist(Activity mActivity, boolean exist){
        try {
            Log.d(">>>>>Moengage","saveJob"+exist);
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);


            MoEHelper.getInstance(mActivity).setUserAttribute("saved_jobs_exist",exist);
            mFirebaseAnalytics.setUserProperty("saved_jobs_exist",String.valueOf(exist));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void setSearchAttributes(Activity mActivity, SimpleSearchVO searchVO){
        try {
            if (searchVO!=null) {

                Log.d(">>>>>Moengage","search"+searchVO);
                FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(mActivity);


                MoEHelper.getInstance(mActivity).setUserAttribute("keyword",searchVO.getKeyword());
                MoEHelper.getInstance(mActivity).setUserAttribute("location",searchVO.getLocation());

                mFirebaseAnalytics.setUserProperty("keyword",searchVO.getKeyword());
                mFirebaseAnalytics.setUserProperty("location",searchVO.getLocation());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}