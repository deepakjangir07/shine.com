package com.net.shine.adapters;

/**
 * Created by Deepak on 14/02/17.
 */

import android.os.Bundle;
import android.view.View;

import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.utils.ShineCommons;

import java.util.ArrayList;



public class SuggestedJobsAdapter extends JobsListRecyclerAdapter {



    public SuggestedJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list,"SuggestedJobs");
    }

    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(URLConfig.INDEX, viewHolder.getAdapterPosition());
                bundle.putString(URLConfig.JOB_TYPE, "search_job");
                bundle.putSerializable(JobDetailParentFrg.JOB_LIST, mList);
                BaseFragment fragment = new JobDetailParentFrg();
                fragment.setArguments(bundle);
                mActivity.singleInstanceReplaceFragment(fragment);

                ShineCommons.trackShineEvents("ProfileViews","Job Detail- JobsFound",mActivity);
            }
        });
    }



}
