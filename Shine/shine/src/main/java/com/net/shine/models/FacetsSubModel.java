package com.net.shine.models;

import java.io.Serializable;

public class FacetsSubModel implements Serializable {

	private String key = "";
	private String value = "";
	private int number;

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}

	

}
