package com.net.shine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.net.shine.R;
import com.net.shine.fragments.SocialApplyFragment;
import com.net.shine.models.ActionBarState;
import com.net.shine.utils.auth.SocialAuthUtils;

/**
 * Created by ujjawal-work on 23/08/16.
 */
public class SocialApplyFormActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setLeftDrawer(DRAWER_NO_SELECTION);
        if(savedInstanceState==null){
            setBaseFrg(SocialApplyFragment.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle("Complete form and apply");
        //syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.social_apply_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if(getIntent().getExtras()!=null && menu!=null) {
            MenuItem item = menu.findItem(R.id.icon_social_apply);
            if(item==null)
                return true;
            item.setVisible(true);
            int auth_type = getIntent().getExtras().getInt("auth_type", 0);
            switch (auth_type){
                case SocialAuthUtils.LINKEDIN_AUTH:
                    item.setIcon(R.drawable.linkedin_menu);
                    break;
                case SocialAuthUtils.GOOGLE_AUTH:
                    item.setIcon(R.drawable.google_menu);
                    break;
                case SocialAuthUtils.FACEBOOK_AUTH:
                    item.setIcon(R.drawable.facebook_menu);
                    break;
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(gDriveCompat!=null){
            gDriveCompat.onActivityResult(requestCode, resultCode, data);
        }

    }


    public SocialApplyFormActivity.GoogleResolutionOnResult gDriveCompat;


    public interface GoogleResolutionOnResult {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

}
