package com.net.shine.interfaces;


import com.net.shine.models.DiscoverModel;

import java.util.HashMap;

public interface GetConnectionListener {
	
	void getFriends(HashMap<String, DiscoverModel.Company> result);
}
