package com.net.shine.utils.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.net.shine.MyApplication;
import com.net.shine.config.URLConfig;
import com.net.shine.models.SimpleSearchVO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShineSqliteDataBase {

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_USER_KEYWORD = "userkeyword";
    public static final String COLUMN_SEARCH_CRITERIA = "search_criteria";

    private final String RECENT_SEARCH_TABLE_NAME = "recent_search";
    private String CREATE_RECENT_SEARCH_TABLE;
    private String SELECT_RECENT_SEARCH_QUERY;
    private String CUSTOM_JOB_TABLE_NAME = "custom_job_alert";
    private String CREATE_CUSTOM_JOB_TABLE;

    private String CREATE_LOOKUPS_FUNCTIONALAREA;
    private String CREATE_LOOKUPS_SKILL;
    private String CREATE_LOOKUPS_INDUSTRY;
    private String CREATE_LOOKUPS_TEAMSIZEMANAGED;
    private String CREATE_LOOKUPS_ANNUALSALARY;
    private String CREATE_LOOKUPS_SHIFTTYPE;
    private String CREATE_LOOKUPS_EDUQUALIFICATION_LEVEL;
    private String CREATE_LOOKUPS_EDUCATIONSTREAM;
    private String CREATE_LOOKUPS_EDU_INSTITUTE;
    private String CREATE_LOOKUPS_CITY;
    private String CREATE_LOOKUPS_EXPERIENCE;
    private String CREATE_LOOKUPS_COURSETYPE;

    private String CREATE_LOOKUPS_VERSION;

    public static final String CREATE_LOOKUP_VERSION_TABLE = "lookup_version_table";

    public static final String CREATE_LOOKUP_FUNCTIONALAREA_TABLE = "lookup_functionalarea_table";

    public static final String CREATE_LOOKUP_SKILL_TABLE = "lookup_skill_table";

    public static final String CREATE_LOOKUP_INDUSTRY_TABLE = "lookup_industry_table";

    public static final String CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE = "lookup_teamsizemanaged_table";
    public static final String CREATE_LOOKUP_ANNUALSALARY_TABLE = "lookup_annualsalary_table";

    public static final String CREATE_LOOKUP_SHIFTTYPE_TABLE = "lookup_shifttype_table";

    public static final String CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE = "lookup_edu_qualification_level_table";
    public static final String CREATE_LOOKUP_EDUSTREAM_TABLE = "lookup_edustream_table";
    public static final String CREATE_LOOKUP_EDU_INSTITUTE_TABLE = "lookup_edu_institute_table";
    public static final String CREATE_LOOKUP_CITY_TABLE = "lookup_city_table";
    public static final String CREATE_LOOKUP_EXPERIENCE = "lookup_experience_table";
    public static final String CREATE_LOOKUP_COURSETYPE_TABLE = "lookup_coursetype_table";

    // intialize Columns

    private String COLUMN_LOOKUP_ID = "lookup_id";

    private String COLUMN_LOOKUP_PID = "pid";

    private String COLUMN_LOOKUP_PDESC = "pdesc";

    private String COLUMN_LOOKUP_CID = "cid";

    private String COLUMN_LOOKUP_CDESC = "cdesc";

    private String COLUMN_LOOKUP_VERSION = "version";

    private String COLUMN_LOOKUP_TABLE_NAME = "tablename";
    private String COLUMN_LOOKUPS_URL = "lookup_url";
    private String CREATE_SAVE_SEARCH_TABLE;
    private String TABLE_SAVED_SEARCHES = "saved";

    private final static String DATA_BASE_NAME = "shine";
    private final static int VERSION_ID = 2;
    public static SQLiteOpenHelper dbHelper;

    public ShineSqliteDataBase(Context context) {

        dbHelper = new ShineDatabaseHelper(context.getApplicationContext(), DATA_BASE_NAME,
                VERSION_ID);

    }

    private class ShineDatabaseHelper extends SQLiteOpenHelper {

        public ShineDatabaseHelper(Context context, String dbName, int version) {
            super(context, dbName, null, version);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {

            Log.d("DEBUG::DB", "OnCreateCalled");

            createTables(db);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
            Log.d("DEBUG::DB", "OnUpgradeCalled");
            try {
                dropTables(db);
                onCreate(db);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public static void dropTable(String tablename) {
        SQLiteDatabase db = null;
        Log.d("DEBUG::DB", "Dropping Table: " + tablename);
        db = dbHelper.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + tablename);

    }

    public boolean addRecentSearch(SimpleSearchVO sVO) {
        SELECT_RECENT_SEARCH_QUERY = "SELECT * FROM "
                + RECENT_SEARCH_TABLE_NAME + " ORDER BY s_date DESC";

        boolean returValue = false;
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            String str1 = sVO.getMinExp() + sVO.getFuncArea()
                    + sVO.getIndusType() + sVO.getKeyword() + sVO.getLocation()
                    + sVO.getMinExp() + sVO.getMinSal();

            System.out.println("Data :" + str1);
            db = dbHelper.getWritableDatabase();

            cursor = db.rawQuery(SELECT_RECENT_SEARCH_QUERY, null);
            boolean updateFlag = false;
            int recordCounter = 0;
            int recordId = 0;
            while (cursor.moveToNext()) {
                recordId = cursor.getInt(0);
                Object obj = deserializeNews(cursor.getBlob(2));
                SimpleSearchVO sObj = (SimpleSearchVO) obj;
                String str2 = sObj.getMinExp() + sObj.getFuncArea()
                        + sObj.getIndusType() + sObj.getKeyword()
                        + sObj.getLocation() + sObj.getMinExp()
                        + sObj.getMinSal();
                if (str1.equalsIgnoreCase(str2)) {
                    updateFlag = true;
                    break;
                } else if (sVO.getKeyword().equalsIgnoreCase(sObj.getKeyword())) {
                    updateFlag = true;
                    break;
                }


                Log.d("RECENT_DB", "str 1 " + str1);


                Log.d("RECENT_DB", "str 2 " + str2);

                recordCounter++;

            }

            cursor.close();

            if (updateFlag) {
                // Update date and get it at top
                ContentValues newValues = new ContentValues();
                newValues.put("s_date", System.currentTimeMillis());
                newValues.put("s_obj", serializeNews(sVO));
                db.update(RECENT_SEARCH_TABLE_NAME, newValues, "record_id=?",
                        new String[]{"" + recordId});

                Log.d("RECENT_DB", "update record id " + recordId);
            } else if (recordCounter > 6) {
                // Update oldest item and get it on top of the list by date
                ContentValues newValues = new ContentValues();
                newValues.put("s_date", System.currentTimeMillis());
                newValues.put("s_obj", serializeNews(sVO));
                db.update(RECENT_SEARCH_TABLE_NAME, newValues, "record_id=?",
                        new String[]{"" + recordId});

                Log.d("RECENT_DB", "Add record id after 7 " + recordId);

                Log.d("RECENT_DB", "Add svo after 7 " + sVO);

            } else {
                // Fire insert query.....
                ContentValues newValues = new ContentValues();
                newValues.put("s_date", System.currentTimeMillis());
                newValues.put("s_obj", serializeNews(sVO));
                long n = db.insert(RECENT_SEARCH_TABLE_NAME, null, newValues);

                Log.d("RECENT_DB", "Add new record id " + n);

                Log.d("RECENT_DB", "Add  new svo " + sVO);
            }
            returValue = true;

        } catch (Exception e) {
            deleteRecords(RECENT_SEARCH_TABLE_NAME);
            e.printStackTrace();

        }
        return returValue;
    }

    public List<SimpleSearchVO> getRecentSearchList() {

        SELECT_RECENT_SEARCH_QUERY = "SELECT * FROM "
                + RECENT_SEARCH_TABLE_NAME + " ORDER BY s_date DESC";
        SQLiteDatabase db = null;
        Cursor dataCount = null;
        ArrayList<SimpleSearchVO> list = new ArrayList<SimpleSearchVO>();
        try {
            db = dbHelper.getWritableDatabase();
            dataCount = db.rawQuery(SELECT_RECENT_SEARCH_QUERY, null);

            while (dataCount.moveToNext()) {
                Object obj = deserializeNews(dataCount.getBlob(2));
                list.add((SimpleSearchVO) obj);
            }
        } catch (Exception e) {
            deleteRecords(RECENT_SEARCH_TABLE_NAME);
            e.printStackTrace();
        }
        return list;
    }

    public static byte[] serializeNews(Object message) throws IOException {
        byte[] messageBytes = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream daos = new ObjectOutputStream(baos);
        daos.writeObject(message);
        daos.flush();
        messageBytes = baos.toByteArray();
        daos.close();
        baos.close();
        return messageBytes;
    }

    public static Object deserializeNews(byte[] messageBytes)
            throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(messageBytes);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object message = ois.readObject();
        ois.close();
        bais.close();
        return message;
    }

    @SuppressWarnings("deprecation")
    public int insertLookupValues(String table,
                                  ArrayList<HashMap<String, String>> arrmap) {
        SQLiteDatabase db = null;
        int n = 0;
        try {

            db = dbHelper.getWritableDatabase();


            InsertHelper helper = new InsertHelper(db, table);
            final int lookup_pid = helper.getColumnIndex(COLUMN_LOOKUP_PID);
            final int lookup_pdesc = helper.getColumnIndex(COLUMN_LOOKUP_PDESC);
            final int lookup_cid = helper.getColumnIndex(COLUMN_LOOKUP_CID);
            final int lookup_cdesc = helper.getColumnIndex(COLUMN_LOOKUP_CDESC);

            for (HashMap<String, String> map : arrmap) {

				/*
                 * ContentValues contentValues = new ContentValues();
				 * contentValues.put(COLUMN_LOOKUP_PID, map.get("pid"));
				 * contentValues.put(COLUMN_LOOKUP_PDESC, map.get("pdesc"));
				 * contentValues.put(COLUMN_LOOKUP_CID, map.get("cid"));
				 * contentValues.put(COLUMN_LOOKUP_CDESC, map.get("cdesc")); n =
				 * (int) db.insert(table, null, contentValues);
				 */
                try {
                    helper.prepareForInsert();
//					Log.d("mapSize", ""+arrmap.size());
                    helper.bind(lookup_pid, map.get("pid"));
                    helper.bind(lookup_pdesc, map.get("pdesc"));
                    helper.bind(lookup_cid, map.get("cid"));
                    helper.bind(lookup_cdesc, map.get("cdesc"));
                    helper.execute();
                    URLConfig.LOOKUP_INSERTION_FLAG = true;
                    n++;
                } catch (Exception e) {

                }

            }
            helper.close();
            URLConfig.LOOKUP_INSERTION_FLAG = false;
        } catch (Exception e) {
            e.printStackTrace();
//		} finally {
//			try {
//				if (db != null) {
//					db.close();
//					System.out.println("DB Closed in insert values "+n);
//				}
//
//			} catch (Exception e) {
//				// ignore...
//			}
        }

        return n;
    }

    public int insertLookupVersion(String tablename, int value, String url) {
        SQLiteDatabase db = null;
        int n = 0;
        try {

            db = dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_LOOKUP_TABLE_NAME, tablename);
            contentValues.put(COLUMN_LOOKUP_VERSION, value);
            contentValues.put(COLUMN_LOOKUPS_URL, url);
            n = (int) db.insert(CREATE_LOOKUP_VERSION_TABLE, null,
                    contentValues);
        } catch (Exception e) {
            e.printStackTrace();
//		} finally {
//			try {
//				if (db != null) {
//					db.close();
//				}
//
//			} catch (Exception e) {
//				// ignore...
//			}
        }
        return n;

    }

    public void updateLookupVersion(String tablename, int value) {
        SQLiteDatabase db = null;
        try {

            db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_LOOKUP_VERSION, value);

            String where = COLUMN_LOOKUP_TABLE_NAME + "=?";
            String[] whereArgs = {tablename};

            int k = db.update(CREATE_LOOKUP_VERSION_TABLE, cv, where,
                    whereArgs);
            android.util.Log.d("UPDATE", "table " + tablename + " value " + value
                    + " row  " + k);
        } catch (Exception e) {
            e.printStackTrace();
//		} finally {
//			try {
//				if (db != null) {
//					db.close();
//				}
//
//			} catch (Exception e) {
//				// ignore...
//			}
        }
    }

//	public static String[] getLookupsFromTable(String tableName) {
//		SQLiteDatabase db = null;
//		try {
//
//
//			db = dbHelper.getReadableDatabase();
//			Cursor crs = db.rawQuery("SELECT * FROM " + tableName, null);
//
//			String[] array = new String[crs.getCount()];
//			int i = 0;
//			while (crs.moveToNext()) {
//				String uname = crs.getString(crs.getColumnIndex("pdesc"));
//				array[i] = uname;
//				i++;
//			}
//			return array;
//		} catch (Exception e) {
//			e.printStackTrace();
////		} finally {
////			try {
////				if (db != null) {
////					db.close();
////				}
////
////			} catch (Exception e) {
////				// ignore...
////			}
//		}
//		return null;
//	}

    public static Cursor getCityList() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = dbHelper.getReadableDatabase();
            cursor = db.rawQuery(
                    "SELECT distinct * From lookup_city_table", null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return cursor;

    }

    public Cursor getExpLevelList() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = dbHelper.getReadableDatabase();
            cursor = db.rawQuery(
                    "SELECT distinct * From lookup_experience_table", null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return cursor;
    }

    public static Cursor getIndustryList() {
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db
                .rawQuery("SELECT distinct * from lookup_industry_table", null);
    }

    public static Cursor getQualificationLevelList() {
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db.rawQuery(
                "select distinct * from lookup_edu_qualification_level_table",
                null);
    }

//	public static Cursor getEducationStreamList() {
//		SQLiteDatabase db = null;
//		db = dbHelper.getReadableDatabase();
//		return db.rawQuery("select distinct * from lookup_edustream_table",
//				null);
//	}

    public static Cursor getEducationInstituteList() {

        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db.rawQuery("select distinct * from lookup_edu_institute_table",
                null);

    }

//	public static Cursor getSkillList() {
//
//		SQLiteDatabase db = null;
//		db = dbHelper.getReadableDatabase();
//		return db.rawQuery("select distinct * from lookup_skill_table", null);
//	}

    public static Cursor getCourseTypeList() {
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db.rawQuery("SELECT distinct * From lookup_coursetype_table",
                null);
    }

    public static Cursor getTeamSizeManagedList() {
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db.rawQuery(
                "Select distinct * from lookup_teamsizemanaged_table", null);
    }

    public static Cursor getAnnualSalaryList() {
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db.rawQuery("select distinct * From lookup_annualsalary_table",
                null);
    }

    public static Cursor getFunctionalAreaList() {

        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db
                .rawQuery(
                        "select distinct * From lookup_functionalarea_table",
                        null);
    }

//	public static Cursor getLookupVersionFromDB(String tableName) {
//		SQLiteDatabase db = null;
//		db = dbHelper.getReadableDatabase();
//		return db.rawQuery("select version from " + tableName, null);
//	}

    public Cursor getLookupVersionFromDatabase() {

//		Log.d("DEBUG ","IS TABLE EXISTS "+isTableExists());
        SQLiteDatabase db = null;
        db = dbHelper.getReadableDatabase();
        return db
                .rawQuery("select distinct * from lookup_version_table ", null);
    }


    public boolean isTableExists(String tableName) {
        SQLiteDatabase db = null;
        try {
            db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select  * from " + tableName,
                    null);
            return cursor != null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return false;
        }


    }


    public boolean isLookupTableFilledOrExists() {
        SQLiteDatabase db = null;


        try {
            db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select  * from lookup_version_table",
                    null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.close();
                    return true;
                }
                cursor.close();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return false;
        }
        return false;
    }

    public void deleteRecords(String tablename) {
        SQLiteDatabase db = null;
        try {
            db = dbHelper.getWritableDatabase();
            db.delete(tablename, null, null);
        } catch (Exception e) {
            e.printStackTrace();
//		} finally {
//			try {
//				if (db != null) {
//					db.close();
//				}
//
//			} catch (Exception e) {
//				// ignore...
//			}
        }
    }

    public boolean isDBdataValid(Context mcContext) {

        try {

            Log.d("DEBUG::LOOKUP", "Checking Database Integrity. " +
                    "Server Lookup: " + ShineSharedPreferences.getServerLookupVersion(mcContext));

            if (!isLookupTableFilledOrExists())
                return false;

            Cursor cursor = MyApplication.getDataBase()
                    .getLookupVersionFromDatabase();

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {

                        //get lookup version for each table
                        int lookup_version_db = cursor.getInt(cursor
                                .getColumnIndex("version"));
                        String tablename = cursor.getString(cursor
                                .getColumnIndex("tablename"));
//					String url = cursor.getString(cursor
//							.getColumnIndex("lookup_url"));

                        Log.d("DEBUG::LOOKUP", tablename + "DB Lookup version: " + lookup_version_db);


                        if (ShineSharedPreferences.getServerLookupVersion(mcContext) > lookup_version_db) {
                            Log.d("DEBUG::LOOKUP", tablename + "is not updated. Returning False");
                            return false;
                        }

                    }

                } else {
                    Log.d("DEBUG::LOOKUP", "Lookup Version Table is Empty. Returning False");
                    return false;
                }
                cursor.close();
            } else {
                Log.d("DEBUG::LOOKUP", "Lookup Version Table Does not exist. Returning False");
                return false;
            }


        } catch (Exception e) {
            Log.d("DEBUG::LOOKUP", "Exception: DB data corrupted. Returning False");
            dropAndCreateTables();
            e.printStackTrace();
            return false;
        }
        Log.d("DEBUG::LOOKUP", "DB data verified. Returning True");
        return true;

    }


    public boolean isDBdataEmpty() {

        try {

            Cursor cursor = MyApplication.getDataBase()
                    .getLookupVersionFromDatabase();

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    Log.d("DEBUG::LOOKUP", "Lookup Version Size:" + cursor.getCount() + ". Returning isDBEmpty False");
                    cursor.close();
                    return false;

                } else {
                    Log.d("DEBUG::LOOKUP", "Lookup Version Empty. Returning isDBEmpty True");
                    cursor.close();
                    return true;
                }

            } else {
                Log.d("DEBUG::LOOKUP", "DB Cursor is null. Returning isDBEmpty True");
            }

            return true;


        } catch (Exception e) {
            Log.d("DEBUG::LOOKUP", "Exception: DB data corrupted. Returning isDBEmpty True");
            e.printStackTrace();
            dropAndCreateTables();
            return true;
        }


    }

    private void dropAndCreateTables() {
//		mContext.deleteDatabase(DATA_BASE_NAME);
//		dbHelper = new ShineDatabaseHelper( mContext, DATA_BASE_NAME, VERSION_ID);

        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            dropTables(db);
            createTables(db);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void createTables(SQLiteDatabase db) {

        Log.d("DEBUG::DB", "Creating All Tables");

        if (db == null)
            db = dbHelper.getWritableDatabase();

        CREATE_RECENT_SEARCH_TABLE = "CREATE TABLE IF NOT EXISTS "
                + RECENT_SEARCH_TABLE_NAME
                + " (record_id INTEGER PRIMARY KEY, s_date long, s_obj BLOB)";

        CREATE_LOOKUPS_VERSION = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_VERSION_TABLE + " ( "
                + COLUMN_LOOKUP_TABLE_NAME + " varchar , "
                + COLUMN_LOOKUP_VERSION + " varchar, " + COLUMN_LOOKUPS_URL
                + " varchar )";

        CREATE_LOOKUPS_FUNCTIONALAREA = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_FUNCTIONALAREA_TABLE + " ( "
                + COLUMN_LOOKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_SKILL = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_SKILL_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";
        CREATE_LOOKUPS_INDUSTRY = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_INDUSTRY_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";
        CREATE_LOOKUPS_TEAMSIZEMANAGED = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE + " ( "
                + COLUMN_LOOKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";
        CREATE_LOOKUPS_ANNUALSALARY = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_ANNUALSALARY_TABLE + " ( "
                + COLUMN_LOOKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_SHIFTTYPE = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_SHIFTTYPE_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar  )";

        CREATE_LOOKUPS_EDUQUALIFICATION_LEVEL = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE
                + " ( "
                + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID
                + " varchar , "
                + COLUMN_LOOKUP_PDESC
                + " varchar, "
                + COLUMN_LOOKUP_CID
                + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_EDUCATIONSTREAM = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_EDUSTREAM_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_EDU_INSTITUTE = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_EDU_INSTITUTE_TABLE + " ( "
                + COLUMN_LOOKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar  )";

        CREATE_LOOKUPS_CITY = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_CITY_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_EXPERIENCE = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_EXPERIENCE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";

        CREATE_LOOKUPS_COURSETYPE = "CREATE TABLE IF NOT EXISTS "
                + CREATE_LOOKUP_COURSETYPE_TABLE + " ( " + COLUMN_LOOKUP_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                + COLUMN_LOOKUP_CDESC + " varchar )";


        db.execSQL(CREATE_RECENT_SEARCH_TABLE);
        db.execSQL(CREATE_LOOKUPS_VERSION);

        db.execSQL(CREATE_LOOKUPS_FUNCTIONALAREA);
        db.execSQL(CREATE_LOOKUPS_SKILL);
        db.execSQL(CREATE_LOOKUPS_INDUSTRY);
        db.execSQL(CREATE_LOOKUPS_ANNUALSALARY);
        db.execSQL(CREATE_LOOKUPS_TEAMSIZEMANAGED);
        db.execSQL(CREATE_LOOKUPS_SHIFTTYPE);
        db.execSQL(CREATE_LOOKUPS_EDUQUALIFICATION_LEVEL);
        db.execSQL(CREATE_LOOKUPS_EDUCATIONSTREAM);
        db.execSQL(CREATE_LOOKUPS_EDU_INSTITUTE);
        db.execSQL(CREATE_LOOKUPS_CITY);
        db.execSQL(CREATE_LOOKUPS_EXPERIENCE);
        db.execSQL(CREATE_LOOKUPS_COURSETYPE);
    }


    private void dropTables(SQLiteDatabase db) {

        Log.d("DEBUG::DB", "Dropping All Tables");
        if (db == null)
            db = dbHelper.getWritableDatabase();


        db.execSQL("DROP TABLE IF EXISTS " + RECENT_SEARCH_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_VERSION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_FUNCTIONALAREA_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_SKILL_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_INDUSTRY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_ANNUALSALARY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_SHIFTTYPE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_EDUSTREAM_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_EDU_INSTITUTE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_CITY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_EXPERIENCE);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_LOOKUP_COURSETYPE_TABLE);
    }


    public void createTable(String table) {
        Log.d("DEBUG::DB", "Creating Single Table: " + table);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        switch (table) {
            case RECENT_SEARCH_TABLE_NAME:
                CREATE_RECENT_SEARCH_TABLE = "CREATE TABLE IF NOT EXISTS "
                        + RECENT_SEARCH_TABLE_NAME
                        + " (record_id INTEGER PRIMARY KEY, s_date long, s_obj BLOB)";
                db.execSQL(CREATE_RECENT_SEARCH_TABLE);
                break;
            case CREATE_LOOKUP_VERSION_TABLE:
                CREATE_LOOKUPS_VERSION = "CREATE TABLE IF NOT EXISTS "
                        + CREATE_LOOKUP_VERSION_TABLE + " ( "
                        + COLUMN_LOOKUP_TABLE_NAME + " varchar , "
                        + COLUMN_LOOKUP_VERSION + " varchar, " + COLUMN_LOOKUPS_URL
                        + " varchar )";
                db.execSQL(CREATE_LOOKUPS_VERSION);
                break;
            default:
                String query = "CREATE TABLE IF NOT EXISTS "
                        + table + " ( "
                        + COLUMN_LOOKUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + COLUMN_LOOKUP_PID + " varchar , " + COLUMN_LOOKUP_PDESC
                        + " varchar, " + COLUMN_LOOKUP_CID + " varchar, "
                        + COLUMN_LOOKUP_CDESC + " varchar )";
                db.execSQL(query);
                break;


        }

    }


}
