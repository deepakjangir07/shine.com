package com.net.shine.utils.db;

import android.content.Context;
import android.util.Log;

import com.net.shine.R;
import com.net.shine.config.URLConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by gobletsky on 29/6/15.
 */

public class LookupInit {


    public static final int CURRENT_FILE_LOOKUP_VERSION = 4;

    public static  void LookupInitFromFile(Context context)
    {


        Log.d("DEBUG::LOOKUP", "LookupInitFromFile called");
        URLConfig.initializeNoticePeriodList(context, R.raw.notice_period);
        URLConfig.initializeExperienceMonthsLevelList(context,
                R.raw.exp_months_level_file);
        URLConfig.initializeSalaryLList(context,
                R.raw.annual_salary_l_file);
        URLConfig.initializeMonthNameList(context, R.raw.month_name);
        URLConfig.initializeSalaryTList(context,
                R.raw.annual_salary_t_file);
       // URLConfig
          //      .initializeQualiStreamList(context, R.raw.edu_stream_file);
        URLConfig.initializePassoutYearList();
        URLConfig.initializeCountyList(context, R.raw.country_file);
        URLConfig.initializSkillsList(context, R.raw.skills_level_file);



        parseJsonFromFile(context, R.raw.lookup_city, "lookup_city_table");
        parseJsonFromFile(context,R.raw.lookup_functionalarea,"lookup_functionalarea_table");
        parseJsonFromFile(context,R.raw.lookup_industry,"lookup_industry_table");
        parseJsonFromFile(context,R.raw.lookup_annualsalary,"lookup_annualsalary_table");
        parseJsonFromFile(context,R.raw.lookup_experience,"lookup_experience_table");
        parseJsonFromFile(context,R.raw.lookup_edu_qualification_nested,"lookup_edu_qualification_level_table");
        parseJsonFromFile(context,R.raw.lookup_teamsize,"lookup_teamsizemanaged_table");
        parseJsonFromFile(context,R.raw.lookup_coursetype,"lookup_coursetype_table");

        Log.d("DEBUG::LOOKUP", "LookupInitFromFile completed");

    }


    public static void LookupInitFromDB(Context context)
    {
        Log.d("DEBUG::LOOKUP", "LookupInitFromDB called");
        URLConfig.initializeCityList(context);
        URLConfig.initializeFunctionalAreaList(context);

        URLConfig.initializeInstituteList(context);
        URLConfig.initializCourseTypeList(context);

        URLConfig.initializeIndustryList(context);
        URLConfig.initializeExperienceLevelList(context);
        URLConfig.initializeNoticePeriodList(context, R.raw.notice_period);
        URLConfig.initializeExperienceMonthsLevelList(context,
                R.raw.exp_months_level_file);
        URLConfig.initializeSalaryList(context);
        URLConfig.initializeSalaryLList(context,
                R.raw.annual_salary_l_file);
        URLConfig.initializeMonthNameList(context, R.raw.month_name);

        URLConfig.initializeSalaryTList(context,
                R.raw.annual_salary_t_file);
        URLConfig.initializeTeamList(context);
        URLConfig.initializeEducationalQualificationList(context);
//        URLConfig
//                .initializeQualiStreamList(context, R.raw.edu_stream_file);
        URLConfig.initializePassoutYearList();

        URLConfig.initializeCountyList(context, R.raw.country_file);
        URLConfig.initializSkillsList(context, R.raw.skills_level_file);

        Log.d("DEBUG::LOOKUP", "LookupInitFromDB completed");



    }


    private static void parseJsonFromFile( Context context,int txt,String tableName)
    {


       String lookup= URLConfig.getFile(context, txt);

        Log.d("LOOKUP CHECK ","lookup "+lookup);

        try {
            JSONObject object=new JSONObject(lookup);
            lookup=object.getString("results");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        parseJson(tableName,lookup, context);

    }


        private static void parseJson(final String table, final String result1, Context context) {

        HashMap<String, String> map;
        final ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        try {
            JSONArray result = new JSONArray(result1);

            for (int i = 0; i < result.length(); i++) {
                map = new HashMap<>();

                ArrayList<Integer> cid_array=new ArrayList<>();
                JSONObject jobj = result.getJSONObject(i);
                String pid = jobj.getString("pid");
                String pdesc = jobj.getString("pdesc");

                JSONArray childarray = jobj.getJSONArray("child");
                if (childarray.length() > 0) {

                    for (int j = 0; j < childarray.length(); j++) {
                        HashMap<String, String> childmap = new HashMap<>();
                        JSONObject childobj = childarray.getJSONObject(j);
                        String cid = childobj.getString("cid");
                        String cdesc = childobj.getString("cdesc");

                        childmap.put("pid", pid);
                        childmap.put("pdesc", pdesc);
                        childmap.put("cid", cid);
                        childmap.put("cdesc", cdesc);

                        cid_array.add(Integer.parseInt(cid));

                        arrayList.add(childmap);

                    }
                } else {

                    map.put("pid", pid);
                    map.put("pdesc", pdesc);
                    arrayList.add(map);

                }

                URLConfig.CHILD_CID_MAP.put(Integer.parseInt(pid),cid_array);

            }



            try {


                switch (table)
                {
                    case ShineSqliteDataBase.CREATE_LOOKUP_CITY_TABLE:
                        URLConfig.initializeCityList(arrayList);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_FUNCTIONALAREA_TABLE:
                        URLConfig.initializeFunctionalAreaList(arrayList);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_EDU_INSTITUTE_TABLE:
                        URLConfig.initializeInstituteList(context);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_COURSETYPE_TABLE:
                        URLConfig.initializCourseTypeList(arrayList);
                        break;


                    case ShineSqliteDataBase.CREATE_LOOKUP_INDUSTRY_TABLE:
                        URLConfig.initializeIndustryList(arrayList);
                        break;


                    case ShineSqliteDataBase.CREATE_LOOKUP_EXPERIENCE:
                        URLConfig.initializeExperienceLevelList(arrayList);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_ANNUALSALARY_TABLE:
                        URLConfig.initializeSalaryList(arrayList);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_TEAMSIZEMANAGED_TABLE:
                        URLConfig.initializeTeamList(arrayList);
                        break;

                    case ShineSqliteDataBase.CREATE_LOOKUP_EDU_QUALIFICATION_LEVEL_TABLE:
                        URLConfig.initializeQualificationList(arrayList);
                        break;



                }







            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
