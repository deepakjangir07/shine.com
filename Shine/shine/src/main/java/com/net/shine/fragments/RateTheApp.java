package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by jaspreet on 19/11/15.
 */
public class RateTheApp extends BaseFragment implements View.OnClickListener,
        VolleyRequestCompleteListener {

    private EditText comments;
    private UserStatusModel userProfileVO;
    private LinearLayout popUp;
    private Dialog progressDialog;
    private TextInputLayout inputLayoutText;

    public static RateTheApp newInstance() {
        Bundle bundle = new Bundle();
        RateTheApp fragment = new RateTheApp();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rate_the_app, container, false);


        comments = (EditText) view.findViewById(R.id.feedback);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);
        TextView noContainer = (TextView) view.findViewById(R.id.nocontainer);
        TextView text = (TextView) view.findViewById(R.id.txt_star);
        text.setText(Html.fromHtml("Would you give the app" + "<b>" + " " + "5 stars" + "</b>") + " ?");
        popUp = (LinearLayout) view.findViewById(R.id.popup);
        TextView button = (TextView) view.findViewById(R.id.submit_btn);
        ImageView cross = (ImageView) view.findViewById(R.id.cross_button);
        inputLayoutText = (TextInputLayout) view.findViewById(R.id.input_layout_feedback);
        TextChangeListener.addListener(comments, inputLayoutText, mActivity);
        TextView yesContainer = (TextView) view.findViewById(R.id.yes_container);
        yesContainer.setOnClickListener(this);
        button.setOnClickListener(this);
        noContainer.setOnClickListener(this);
        cross.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.yes_container:
                popUp.setVisibility(View.GONE);

                ShineCommons.trackShineEvents("RateApp", "RateAppScreen", mActivity);
                Intent rate_app = new Intent(Intent.ACTION_VIEW, Uri.parse(URLConfig.APP_GOOGLE_PLAY_LINK));
                startActivity(rate_app);
                break;
            case R.id.nocontainer:
                if (popUp.getVisibility() == View.GONE) {
                    popUp.setVisibility(View.VISIBLE);
                } else {
                    popUp.setVisibility(View.GONE);
                }
                break;

            case R.id.cross_button:
                popUp.setVisibility(View.GONE);
                break;

            case R.id.submit_btn:
                ShineCommons.hideKeyboard(mActivity);


                if (comments.getText().toString().trim().length() > 0) {
                    progressDialog = DialogUtils.showCustomProgressDialog(mActivity, "Please wait");
                    progressDialog.setCancelable(false);

                    submitFeedback();
                } else {
                    inputLayoutText.setError("Write your feedback before sending.");

                }
                break;


            default:
                break;
        }
    }

    private void submitFeedback() {

        String mydate = java.text.DateFormat.getDateTimeInstance().format(
                Calendar.getInstance().getTime());
        String myDeviceModel = android.os.Build.MODEL;
        String myVersion = android.os.Build.VERSION.RELEASE;
        PackageInfo pInfo = null;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(
                    mActivity.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String feedback = comments.getText().toString();
        String subject = "Feedback-" + userProfileVO.full_name + "-" + mydate;


        String verNum = "";
        if (pInfo != null) {
            verNum = pInfo.versionName;
        }


        Type type = new TypeToken<EmptyModel>() {
        }.getType();
        VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this,
                URLConfig.FEEDBACK_URL, type);
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("candidate_id", userProfileVO.candidate_id);
        postParams.put("subject", subject);
        postParams.put("time", mydate);
        postParams.put("device", myDeviceModel);
        postParams.put("os_version", myVersion);
        postParams.put("app_version", verNum);
        postParams.put("feedback", feedback);
        request.setUsePostMethod(postParams);
        request.execute("feedbackFrag");

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        mActivity.setTitle("Rate the app");
        mActivity.setActionBarVisible(true);
        mActivity.showNavBar();
        mActivity.showBackButton();
        ManualScreenTracker.manualTracker("Rate the App");

    }

    @Override
    public void OnDataResponse(final Object object, final String tag) {

        try {

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        DialogUtils.dialogDismiss(progressDialog);
                        if (object != null) {
                            ShineCommons.trackShineEvents("FeedbackSend", "RateAppScreen", mActivity);
                            Toast.makeText(mActivity, "Thank you for your feedback. This will help us in serving you better", Toast.LENGTH_LONG).show();
                            if(popUp!=null)
                                popUp.setVisibility(View.GONE);
                            mActivity.onBackPressed();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    DialogUtils.dialogDismiss(progressDialog);
                    DialogUtils.showErrorToast(error);


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
