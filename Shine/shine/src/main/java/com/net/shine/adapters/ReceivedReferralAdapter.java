package com.net.shine.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.config.URLConfig;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.ReferralModel;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.enrcypt.ReferralCipher;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Deepak on 29-05-2015.
 */

public class ReceivedReferralAdapter extends BaseAdapter implements VolleyRequestCompleteListener {
    BaseActivity mActivity;
    private Activity mContext;
    private ArrayList<ReferralModel.RefrelResults> mList;
    private LayoutInflater mLayoutInflater = null;
    private Dialog alertDialog;
    private int mPosition;
    public ImageLoader.ImageContainer imageContainer;



    public ReceivedReferralAdapter(BaseActivity activity1, ArrayList<ReferralModel.RefrelResults> list) {
        mContext = activity1;
        mList = list;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.mActivity = activity1;


    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int pos) {
        return mList.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        LayoutInflater li = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = li.inflate(R.layout.received_ref_row, parent, false);


        TextView mCompanyName = (TextView) v
                .findViewById(R.id.company_name);
        final TextView mName = (TextView) v.findViewById(R.id.name);
        TextView mJobTitle = (TextView) v.findViewById(R.id.jobtitle);
        final ImageView mImg = (ImageView) v.findViewById(R.id.imageView1);
        TextView connected_via = (TextView) v.findViewById(R.id.connected_via);
        View accept = v.findViewById(R.id.accept);
        View ignore = v.findViewById(R.id.ignore);

        TextView job_exipred_tv = (TextView) v.findViewById(R.id.expired);
        LinearLayout expired_layout = (LinearLayout) v.findViewById(R.id.expired_layout);

        LinearLayout non_expired_layout = (LinearLayout) v.findViewById(R.id.accept_ignore_layout);

        ImageView icon_accepted = (ImageView) v.findViewById(R.id.icon_accepted);
        TextView mMsgtv = (TextView) v.findViewById(R.id.msg);
        ImageView msgIcon = (ImageView) v.findViewById(R.id.icon);
        final TextView viewMsgText = (TextView) v.findViewById(R.id.view_message_tv);


        mCompanyName.setText(mList.get(position).getCompany_name());
        mName.setText(mList.get(position).getCandidate_name());
        mJobTitle.setText(mList.get(position).getJob_title());

        int connected_mode = mList.get(position).getReferrals().get(0).getMode();


        if (connected_mode == 1) {
            connected_via.setText("Connected via Phonebook");
        } else if (connected_mode == 2) {
            connected_via.setText("Connected via Email");
        } else if (connected_mode == 3) {
            connected_via.setText("Connected via LinkedIn");
        } else if (connected_mode == 4) {
            if (mList.get(position).getReferrals().get(0).getOrganization() != null && !mList.get(position).getReferrals().get(0).getOrganization().equals("null")) {
                connected_via.setText("Worked together at " + mList.get(position).getReferrals().get(0).getOrganization());
            } else {
                connected_via.setText("Worked together");
            }
        } else if (connected_mode == 5) {
            if (mList.get(position).getReferrals().get(0).getOrganization() != null && !mList.get(position).getReferrals().get(0).getOrganization().equals("null")) {

                connected_via.setText("Studied together at " + mList.get(position).getReferrals().get(0).getOrganization());
            } else {
                connected_via.setText("Studied together");
            }
        }

        if (!TextUtils.isEmpty(mList.get(position).getReferrals().get(0).getReferrer_image_url())&&
                !(mList.get(position).getReferrals().get(0).getReferrer_image_url().equals("null"))) {
            try {

                Glide.with(mActivity).load(mList.get(position).getReferrals().get(0).getReferrer_image_url())
                        .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        ColorGenerator generator = ColorGenerator.MATERIAL;
                        int color = generator.getRandomColor();
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRect(mName.getText().toString().substring(0,1).toUpperCase(), color);
                        mImg.setImageDrawable(drawable);

                        return true;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                }).into(mImg);

        }
        catch (Exception e){
            e.printStackTrace();
        }
        }
        else{
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRect(mName.getText().toString().substring(0,1).toUpperCase(), color);
            mImg.setImageDrawable(drawable);
        }


        if (!mList.get(position).job_expired) {
            expired_layout.setVisibility(View.GONE);
            non_expired_layout.setVisibility(View.VISIBLE);
            mJobTitle.setTextColor(mActivity.getResources().getColor(R.color.linkcolor));
        } else {

            expired_layout.setVisibility(View.VISIBLE);
            non_expired_layout.setVisibility(View.GONE);
            mJobTitle.setTextColor(mActivity.getResources().getColor(R.color.black_555555));

        }
        if (mList.get(position).getReferrals().get(0).getStatus() == 1) {
            accept.setVisibility(View.VISIBLE);
            ignore.setVisibility(View.VISIBLE);
            viewMsgText.setVisibility(View.VISIBLE);
            msgIcon.setVisibility(View.VISIBLE);
            icon_accepted.setVisibility(View.GONE);
            mMsgtv.setVisibility(View.GONE);
        } else if (mList.get(position).getReferrals().get(0).getStatus() == 2) {
            accept.setVisibility(View.GONE);
            ignore.setVisibility(View.GONE);
            icon_accepted.setVisibility(View.VISIBLE);
            mMsgtv.setVisibility(View.VISIBLE);
            icon_accepted.setImageResource(R.drawable.green_tick_small);
            mMsgtv.setText("Request accepted by you");
            viewMsgText.setVisibility(View.GONE);
            msgIcon.setVisibility(View.GONE);

        } else if (mList.get(position).getReferrals().get(0).getStatus() == 3) {
            accept.setVisibility(View.GONE);
            ignore.setVisibility(View.GONE);
            viewMsgText.setVisibility(View.GONE);
            msgIcon.setVisibility(View.GONE);
            icon_accepted.setVisibility(View.VISIBLE);
            mMsgtv.setVisibility(View.VISIBLE);
            icon_accepted.setImageResource(R.drawable.ignore_cancel);
            mMsgtv.setText("Request ignored by you");
        }


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mode = mList.get(position).getReferrals().get(0).getMode();
                mPosition = position;
                if (mode == 3 || mode == 4 || mode == 5) {
                    hitAcceptApi(mList.get(position).getReferrals().get(0).getReferrer_corp_email(), position);
                } else {
                    alertDialog = DialogUtils.showEmailDialog(mActivity);
                    final EditText input = (EditText) alertDialog.findViewById(R.id.editText);
                    TextView ok = (TextView) alertDialog.findViewById(R.id.ok);
                    TextView cancel = (TextView) alertDialog.findViewById(R.id.cancel);

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String emailText = input.getText().toString().trim();
                            if (!Pattern.matches(ShineCommons.REGEX, emailText)) {

                                input.setTextColor(mContext.getResources().getColor(
                                        R.color.red));

                                DialogUtils.showErrorToast(mActivity.getResources().getString(R.string.email_invalid_address_msg));
                            } else {
                                alertDialog.dismiss();
                                hitAcceptApi(emailText, mPosition);

                            }

                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                if (alertDialog != null && alertDialog.isShowing()) {
                                    alertDialog.dismiss();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

                    // getEmailDialog(position);
                }
            }
        });
        ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPosition = position;

                alertDialog = DialogUtils.showCustomProgressDialog(mContext,
                        mContext.getString(R.string.plz_wait));

                String URL = URLConfig.ACCEPT_REJECT_URL.replace(URLConfig.REFERRAL_ID, mList.get(position).getReferrals().get(0).getReferral_id());


                Type type = new TypeToken<EmptyModel>() {
                }.getType();

                VolleyNetworkRequest req = new VolleyNetworkRequest(mActivity, ReceivedReferralAdapter.this, URL, type);

                HashMap<String, String> paramsMap = new HashMap<>();
                paramsMap.put("status", "3");
                paramsMap.put("candidate_id", mList.get(position).getCandidate_id());
                paramsMap.put("job_id", mList.get(position).getJob_id());
                req.setUsePutMethod(paramsMap);
                req.execute("IGNORE_REFERRAL");


            }
        });

        v.findViewById(R.id.requestAcceptLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if(mList.get(position).job_expired)
                    {
                        Toast.makeText(mActivity,"This job has been expired",Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (viewMsgText != null && viewMsgText.getVisibility() == View.VISIBLE) {
                        String URL = URLConfig.VIEW_MESSAGE_URL +
                                ReferralCipher.cipher(mList.get(position).getJob_id() + "/" + mList.get(position).getCandidate_id()
                                        + "/" + mList.get(position).getReferrals().get(0).getReferral_id(), ReferralCipher.ENC_MODE) + "/";
                        if(ShineCommons.appInstalledOrNot("com.android.chrome"))
                        {
                            System.out.println("-- package install");
                            ChromeCustomTabs.openCustomTab(mActivity,URL);
                        }
                        else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(getApplicationContext(),CustomWebView.class);
                            bundle.putString("shineurl", URL);
                            intent.putExtras(bundle);
                            mActivity.startActivity(intent);;

                        }

                        ShineCommons.trackShineEvents("ViewMessage", "ReceivedPage", mActivity);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }


    @Override
    public void OnDataResponse(Object object, String tag) {

        try {
            DialogUtils.dialogDismiss(alertDialog);

            if (tag.equals("ACCEPT_REFERRAL")) {


                mList.get(mPosition).getReferrals().get(0).setStatus(2);
                notifyDataSetChanged();

                ShineCommons.trackShineEvents("AcceptReferral", "ReceivedPage", mActivity);
            } else if (tag.equals("IGNORE_REFERRAL")) {
                mList.get(mPosition).getReferrals().get(0).setStatus(3);
                notifyDataSetChanged();

                ShineCommons.trackShineEvents("RejectReferral", "ReceivedPage", mActivity);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (alertDialog != null && alertDialog.isShowing()) {

                            alertDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    DialogUtils.showErrorToast(error);


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void hitAcceptApi(String email, int position)

    {
        alertDialog = DialogUtils.showCustomProgressDialog(mContext,
                mContext.getString(R.string.plz_wait));
        String URL = URLConfig.ACCEPT_REJECT_URL.replace(URLConfig.REFERRAL_ID, mList.get(position).getReferrals().get(0).getReferral_id());


        Type type = new TypeToken<EmptyModel>() {
        }.getType();

        VolleyNetworkRequest req = new VolleyNetworkRequest(mContext, ReceivedReferralAdapter.this,
                URL, type);
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put("status", "2");
        paramsMap.put("referrer_corp_email", email);
        paramsMap.put("candidate_id", mList.get(position).getCandidate_id());
        paramsMap.put("job_id", mList.get(position).getJob_id());
        req.setUsePutMethod(paramsMap);
        req.execute("ACCEPT_REFERRAL");


    }

    public void rebuildListObject(ArrayList itemList) {

        this.mList = itemList;
    }
}
