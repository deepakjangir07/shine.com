package com.net.shine.adapters;

/**
 * Created by Deepak on 14/02/17.
 */

import android.os.Bundle;
import android.view.View;

import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.ArrayList;


public class PostApplyFlowSimilarJobsAdapter extends JobsListRecyclerAdapter {

    public  PostApplyFlowSimilarJobsAdapter.closeBottomSheet closeBottomSheet;


    public PostApplyFlowSimilarJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list, PostApplyFlowSimilarJobsAdapter.closeBottomSheet mycloseBottomSheet){
        super(activity, list,"PostApplyFlowSimilarJobs");
        this.closeBottomSheet=mycloseBottomSheet;
    }

    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        viewHolder.title.setSingleLine();
        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(URLConfig.INDEX, viewHolder.getAdapterPosition());
                bundle.putString(URLConfig.JOB_TYPE, "search_job");
                bundle.putSerializable(JobDetailParentFrg.JOB_LIST, mList);
                BaseFragment fragment = new JobDetailParentFrg();
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);

                if(closeBottomSheet!=null) {
                        closeBottomSheet.onCloseAction();
                }


            }
        });


        viewHolder.applied_similar_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchVO sVO = new SimpleSearchVO();
                sVO.setJobId(mList.get(viewHolder.getAdapterPosition()).jobId);
                sVO.setFromSimilarJobs(true);
                sVO.setHideModifySearchButton(true);
                if (ShineSharedPreferences.getUserInfo(mActivity) != null) {
                    String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity));
                    sVO.setUrl(URL);

                } else {
                    sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                BaseFragment fragment = new SearchResultFrg();
                fragment.setArguments(bundle);
                mActivity.replaceFragmentWithBackStack(fragment);

                ShineCommons.trackShineEvents("View SimilarJobs","JobListing",mActivity);

                if(closeBottomSheet!=null) {
                    closeBottomSheet.onCloseAction();
                }
            }
        });
    }


    public interface closeBottomSheet
    {
        void onCloseAction();
    }

}
