package com.net.shine.interfaces;

public interface OTPCallback {

	void finalStep(String mobile, String otp);
}
