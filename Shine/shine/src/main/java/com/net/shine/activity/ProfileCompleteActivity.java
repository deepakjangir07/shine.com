package com.net.shine.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.ProfileCompleteFragment;
import com.net.shine.models.ActionBarState;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.Arrays;

/**
 * Created by Deepak on 11/01/17.
 */

public class ProfileCompleteActivity extends BaseActivity {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setLeftDrawer(DRAWER_JOBS);
        if(savedInstanceState==null){
            setBaseFrg(ProfileCompleteFragment.newInstance(getIntent().getExtras()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle(getString(R.string.register_complete));



        //syncDrawerState(ActionBarState.ACTION_BAR_STATE_BURGER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.similar_search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(this)))
        {
            final Dialog deleteDialog = DialogUtils.showAlertWithTwoButtons(this);
            TextView t = (TextView) deleteDialog
                    .findViewById(R.id.dialog_msg);
            t.setText("Are you sure you want to stop registration & exit the app");
            ((TextView)deleteDialog.findViewById(R.id.yes_btn)).setText("YES");
            deleteDialog.findViewById(R.id.yes_btn).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dialogDismiss(deleteDialog);
                            finish();
                        }
                    });
            deleteDialog.findViewById(R.id.cancel_exit_btn)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dialogDismiss(deleteDialog);
                        }
                    });
        }
        else
        {
            super.onBackPressed();
        }
    }
}
