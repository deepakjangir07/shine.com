package com.net.shine.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.components.PersonalDetailsEditFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class ErrorHandler {

    public static String getErrorResponse(VolleyError error, String tag) {
        String json = null;

        NetworkResponse response = error.networkResponse;
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 400:
                    json = new String(response.data);
                    Log.d("Error", json);

                    try {
                        JSONObject obj = new JSONObject(json);
                        if (obj.has("non_field_errors")) {
                            json = obj.getJSONArray("non_field_errors").get(0).toString();
                            Log.d("manish-errors",json+"");
                        }

                        else {

                            for (final Iterator<String> iter = obj.keys(); iter.hasNext();) {
                                final String key = iter.next();
                                Log.d("manish-key",key);
                                final Object value = obj.get(key);

                                try {

                                    final JSONArray jsonArray = (JSONArray) value;
                                    if(jsonArray.length()>0){
                                        for (int i=0;i<jsonArray.length();i++){
                                            try {
                                                JSONObject object = new JSONObject(jsonArray.get(i).toString());
                                                System.out.println(key);
                                                System.out.println(jsonArray.get(i).toString());

                                                if(object.has("non_field_errors")){
                                                    Log.d("manish_newerror",object.getJSONArray("non_field_errors").get(0).toString());
                                                    json = object.getJSONArray("non_field_errors").get(0).toString();
                                                    break;
                                                }
                                                else {
                                                    json = jsonArray.get(0).toString();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                json = jsonArray.get(0).toString();
                                            }
                                        }
                                    }
                                    else {

                                    }
                                    System.out.println(key);
                                    System.out.println(jsonArray.get(0).toString());

                                } catch (final Exception e) {
                                    e.printStackTrace();
                                    try {
                                        JSONObject jsonObject = new JSONObject(String.valueOf(value));
                                        if(jsonObject.has("non_field_errors")){
//                                            JSONArray newjsonArray = (JSONArray) jsonObject.get("non_field_errors");
                                            json = jsonObject.getJSONArray("non_field_errors").get(0).toString();
                                            Log.d("manish-errors_new",json+"");
                                        }
                                        else {

                                            try {
                                                for (final Iterator<String> iterator = jsonObject.keys(); iter.hasNext();) {
                                                    final String newkey = iterator.next();
                                                    final Object value1 = obj.get(newkey);
                                                    final JSONArray jsonArray1 = (JSONArray) value1;
                                                    json = jsonArray1.get(0).toString();


                                                }
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }

                                        }
//                                        final Object value1 = jsonObject.get(key);
//                                        final JSONArray jsonArray1 = (JSONArray) value1;
//                                        if(jsonArray1.length()>0){
//                                            json = jsonArray1.get(0).toString();
//                                        }
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }

                                }
                            }
                            Log.d("manish-errors2",json+"");
                        }
                    } catch (Exception e) {
                        json = URLConfig.PARSING_ERROR_MSG;
                    }

                    if (json != null) {
                        return json;
                    }
                    break;
                case 404:
                    //Not Found, Server Error

                    json = new String(response.data);
                    Log.d("Error", json);
                    try {
                        JSONObject obj = new JSONObject(json);
                        if (obj.has("details")) {
                            json = obj.getString("details");
                        } else if(obj.has("detail")){
                            json = obj.getString("detail");
                        }
                        else {
                            json = URLConfig.PARSING_ERROR_MSG;
                        }
                    } catch (Exception e) {
                        json = URLConfig.PARSING_ERROR_MSG;
                    }
                    if(tag.equals(PersonalDetailsEditFragment.PHONECHECK)){
                        json = "404: " + json;
                    }
                    if (json != null) {
                        return json;
                    }
                    break;


            }
            // Additional cases
        }

        if (error instanceof TimeoutError) {
            return "The connection has timed out. Please try again.";
        } else if (error instanceof NoConnectionError) {
            return "Please check your connection settings.";
        } else if (error instanceof AuthFailureError) {
            return "Authorisation Failure.";
        } else if (error instanceof ServerError) {
            return URLConfig.PARSING_ERROR_MSG;//"Server Failure.";
        } else if (error instanceof NetworkError) {
            return "Please check your connection settings.";//"Network Failure.";
        } else if (error instanceof ParseError) {
            return URLConfig.PARSING_ERROR_MSG;//"Parsing Failure.";
        }
        return URLConfig.PARSING_ERROR_MSG;//"Please check your connection setting.";
    }

//    public static String getErrorResponse(Exception exception) {
//        if (exception instanceof SocketTimeoutException) {
//            return "The connection has timed out. Please try again.";
//        } else if (exception instanceof ClientProtocolException) {
//            return "Some error has occured.";
//        } else if (exception instanceof UnknownHostException) {
//            return "Please check your connection settings.";
//        } else
//            return "Please check your connection settings.";
//    }
}
