package com.net.shine.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.AuthActivity;
import com.net.shine.activity.HomeActivity;
import com.net.shine.activity.ResumeUploadActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.dialogs.TermsAndConditionsDialog;
import com.net.shine.interfaces.LinkedInCallBack;
import com.net.shine.models.CheckEmailIdModel;
import com.net.shine.models.EmptyModel;
import com.net.shine.models.LinkedInAuthModel;
import com.net.shine.models.RegistrationModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.auth.ActivityResultHandler;
import com.net.shine.utils.auth.FacebookConstants;
import com.net.shine.utils.auth.GoogleConstants;
import com.net.shine.utils.auth.LinkedinConstants;
import com.net.shine.utils.auth.SocialAuthUtils;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;
import com.truecaller.android.sdk.ITrueCallback;
import com.truecaller.android.sdk.TrueClient;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

import static android.R.attr.type;

/**
 * Created by ujjawal-work.
 */

public class RegistrationFrg extends BaseFragment implements View.OnClickListener, SocialAuthUtils.AuthPrefillCallback, View.OnFocusChangeListener {


    public static RegistrationFrg newInstance(Bundle args) {
        RegistrationFrg fragment = new RegistrationFrg();
        fragment.setArguments(args);
        return fragment;
    }


    private EditText name;
    private EditText email;
    private EditText mobile, passwordField;
    private String mobile_number_trucaller="";
    private Dialog alertDialog;
    private ActivityResultHandler resultHandler;
    private TextView trueBtn,showField;
    private TrueClient mTrueClient;
    private TextView totalExpField, funcAreaField,
            industryField, cityField;
    private View rootView;
    private String profile_picture_url = "";
    private int user_type = 2;
    private boolean alertsCheck = true;
    private CheckBox alertBoxImg;
    private TextInputLayout inputLayoutEmail, inputLayoutName, inputLayoutPassword,
            inputLayoutMobile, inputLayoutCity, inputLayoutFunctionalArea,
            inputLayoutIndustry, inputLayoutExperience;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());

        rootView = inflater.inflate(R.layout.registration_view, container, false);


         Log.d("ActivityName ","name  "+getActivity());

        trueBtn=(TextView) rootView.findViewById(R.id.truecaller_btn);
        mTrueClient = new TrueClient(mActivity, new ITrueCallback() {
            @Override
            public void onSuccesProfileShared(@NonNull TrueProfile trueProfile) {
                try {
                    name.setText(trueProfile.firstName);
                    email.setText(trueProfile.email);

                    if(trueProfile.phoneNumber.contains("+91")) {
                        mobile.setText(trueProfile.phoneNumber.substring(3));
                        mobile_number_trucaller=trueProfile.phoneNumber.substring(3);
                    }
                    else
                    {
                        mobile.setText(trueProfile.phoneNumber);
                    }


                    Log.d("Truecaller","onSuccess "+trueProfile.phoneNumber);
                    ShineCommons.trackShineEvents("Truecaller","onSuccessProfileShared",mActivity);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailureProfileShared(@NonNull TrueError trueError) {


                Log.d("Truecaller","onFailed "+trueError.toString());
                ShineCommons.trackShineEvents("Truecaller","onFailureProfileShared",mActivity);
            }
        });

        if(getActivity() instanceof HomeActivity) {
            HomeActivity authActivity = (HomeActivity) getActivity();
            authActivity.callerCompat = new HomeActivity.TrueCallerCompat() {
                @Override
                public void onActivityResult(int requestCode, int resultCode, Intent data) {
                    if (mTrueClient!=null) {
                        mTrueClient.onActivityResult(requestCode, resultCode, data);
                    }
                }
            };
        }


        if(getActivity() instanceof AuthActivity) {
            AuthActivity authActivity = (AuthActivity) getActivity();
            authActivity.callerCompat = new AuthActivity.TrueCallerCompat() {
                @Override
                public void onActivityResult(int requestCode, int resultCode, Intent data) {
                    if (mTrueClient!=null) {
                        mTrueClient.onActivityResult(requestCode, resultCode, data);
                    }
                }
            };
        }

        trueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTrueClient.getTruecallerUserProfile(mActivity);
            }
        });


        rootView.findViewById(R.id.linkedin_btn).setOnClickListener(this);
        rootView.findViewById(R.id.google_btn).setOnClickListener(this);
        rootView.findViewById(R.id.facebook_btn).setOnClickListener(this);


        int citSelectedPos = -1;    //cannot be 0 as it refers to #TopCities
        int[] indSelectedPos = new int[0];
        int[] funSelectedPos = new int[0];

        try {
            if (cityField != null && cityField.getTag() != null)
                citSelectedPos = (int) cityField.getTag();
            if (industryField != null && industryField.getTag() != null)
                indSelectedPos = (int[]) industryField.getTag();
            if (funcAreaField != null && funcAreaField.getTag() != null)
                funSelectedPos = (int[]) funcAreaField.getTag();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            rootView.findViewById(R.id.registrationViewContainer).requestFocus();
            name = (EditText) rootView.findViewById(R.id.name);
            email = (EditText) rootView.findViewById(R.id.email);
            mobile = (EditText) rootView.findViewById(R.id.mobile_field);
            passwordField = (EditText) rootView.findViewById(R.id.password);
            passwordField.setTransformationMethod(new PasswordTransformationMethod());
            passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    ShineCommons.hideKeyboard(mActivity);
                    return true;
                }
            });

            inputLayoutEmail = (TextInputLayout) rootView.findViewById(R.id.input_layout_email);
            inputLayoutName = (TextInputLayout) rootView.findViewById(R.id.input_layout_name);
            inputLayoutPassword = (TextInputLayout) rootView.findViewById(R.id.input_layout_password);
            inputLayoutMobile = (TextInputLayout) rootView.findViewById(R.id.input_layout_mobile);
            inputLayoutCity = (TextInputLayout) rootView.findViewById(R.id.input_layout_city);
            inputLayoutFunctionalArea = (TextInputLayout) rootView.findViewById(R.id.input_layout_fun_area);
            inputLayoutIndustry = (TextInputLayout) rootView.findViewById(R.id.input_layout_industry);
            inputLayoutExperience = (TextInputLayout) rootView.findViewById(R.id.input_layout_exp);
            alertBoxImg = (CheckBox) rootView.findViewById(R.id.alert_check);
            alertBoxImg.setSelected(true);
            showField = (TextView) rootView.findViewById(R.id.show_hide);
            alertBoxImg.setChecked(true);
            showField.setOnClickListener(this);
            rootView.findViewById(R.id.alert_check_layout).setOnClickListener(this);
            rootView.findViewById(R.id.terms).setOnClickListener(this);
            rootView.findViewById(R.id.submit_btn).setOnClickListener(this);
            totalExpField = (TextView) rootView.findViewById(R.id.total_exp);
            cityField = (TextView) rootView.findViewById(R.id.city);
            cityField.setTag(citSelectedPos);
            cityField.setOnClickListener(this);
            cityField.setOnFocusChangeListener(this);
            funcAreaField = (TextView) rootView.findViewById(R.id.fun_area);
            funcAreaField.setTag(funSelectedPos);
            funcAreaField.setOnClickListener(this);
            funcAreaField.setOnFocusChangeListener(this);
            industryField = (TextView) rootView.findViewById(R.id.industry);
            industryField.setTag(indSelectedPos);
            industryField.setOnClickListener(this);
            industryField.setOnFocusChangeListener(this);


            TextChangeListener.addListener(name, inputLayoutName, mActivity);
            TextChangeListener.addListener(email, inputLayoutEmail, mActivity);
            TextChangeListener.addListener(mobile, inputLayoutMobile, mActivity);
            TextChangeListener.addListener(passwordField, inputLayoutPassword, mActivity);
            TextChangeListener.addListener(cityField, inputLayoutCity, mActivity);
            TextChangeListener.addListener(funcAreaField, inputLayoutFunctionalArea, mActivity);
            TextChangeListener.addListener(industryField, inputLayoutIndustry, mActivity);
            TextChangeListener.addListener(totalExpField, inputLayoutExperience, mActivity);


            name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String nameText = name.getText().toString().trim();
                        if (name.length() == 0 || !(Pattern.matches(ShineCommons.NAME_REGEX, nameText))) {
                            name.setTextColor(ContextCompat.getColor(getContext(),
                                    R.color.red));
                            inputLayoutName.setError(getResources().getString(
                                    R.string.name_invalid_msg));
                        }
                    }
                }
            });

            email.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if (!hasFocus) {
                        String emailText = email.getText().toString().trim();
                        if (!Pattern.matches(ShineCommons.REGEX, emailText)) {
                            email.setTextColor(ContextCompat.getColor(getContext(),
                                    R.color.red));
                            inputLayoutEmail.setError(getResources().getString(
                                    R.string.email_invalid_address_msg));
                        } else {
                            Type listType = new TypeToken<CheckEmailIdModel>() {}.getType();
                            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                                @Override
                                public void OnDataResponse(Object object, String tag) {
                                    final CheckEmailIdModel cModel = (CheckEmailIdModel) object;
                                    if (cModel.getExists()) {
                                        if (RegistrationFrg.this.isVisible()) {
                                            inputLayoutEmail.setError("Email ID already exists");
                                        }
                                    } else {
                                        inputLayoutEmail.setErrorEnabled(false);
                                    }
                                }
                                @Override
                                public void OnDataResponseError(String error, String tag) {
                                }
                            },
                                    URLConfig.EMAIL_ID_CHECK_URL.replace(URLConfig.EMAIL_ID, email
                                            .getText().toString().trim()),
                                    listType);

                            downloader.execute("emailIdExist");
                        }
                    }

                }
            });


            passwordField
                    .setOnFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {

                                String password = passwordField.getText()
                                        .toString().trim();
                                if (password.length() < 6
                                        || password.length() > 15) {
                                    passwordField.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                                    inputLayoutPassword.setError("Please enter valid password");

                                }
                            }

                        }
                    });

            mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String mobileText = mobile.getText().toString().trim();
                        if (mobileText.length() < 10) {
                            mobile.setTextColor(ContextCompat.getColor(getContext(),
                                    R.color.red));
                            inputLayoutMobile.setError("Enter your 10-digit Mobile No");
                        } else if ("0".equals("" + mobileText.charAt(0))) {
                            mobile.setTextColor(ContextCompat.getColor(getContext(),
                                    R.color.red));
                            inputLayoutMobile.setError("Mobile number should not start with 0");
                        } else {
                            boolean flag = false;
                            int n = mobileText.length();
                            for (int i = 0; i < n; i++) {
                                if (mobileText.charAt(i) < 48
                                        || mobileText.charAt(i) > 57) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag) {
                                mobile.setTextColor(ContextCompat.getColor(getContext(),
                                        R.color.red));
                                inputLayoutMobile.setError("" + R.string.MOBILE_NO_INVALID_CHAR_MSG);
                            }
                        }

                    }

                }
            });

            if(getArguments()!=null){
                if(getArguments().getSerializable("model")!=null){
                    setPrefillData((LinkedInAuthModel) getArguments().getSerializable("model"), type);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;

    }


    @Override
    public void onClick(View v) {

        try {
            switch (v.getId()) {

                case R.id.linkedin_btn: //LinkedIn Login
                    ShineCommons.trackShineEvents("LoginAttempt", "LinkedIn", mActivity);

                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomProgressDialog(getActivity(), "Please Wait...");
                    DialogUtils.hideDialog(alertDialog);
                    LinkedinConstants.LinkedInConnect(mActivity, new LinkedInCallBack() {
                        @Override
                        public void onLinkedInConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            if (TextUtils.isEmpty(accessToken)) {
                                ShineCommons.trackShineEvents("LinkedinLoginFail", "UserLogin", mActivity);
                            }
                            SocialAuthUtils.registerUser(accessToken, expiry, SocialAuthUtils.LINKEDIN_AUTH, getArguments(), mActivity, customDialog, RegistrationFrg.this);
                        }

                        @Override
                        public void onLinkedInSuccess() {
                        }

                        @Override
                        public void onLinkedInFailed() {
                        }
                    }, true, alertDialog);
                    break;
                case R.id.google_btn:   //Google Login
                    ShineCommons.trackShineEvents("LoginAttempt", "Google", mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    resultHandler = GoogleConstants.GoogleConnect(mActivity, this, new GoogleConstants.GoogleCallBack() {
                        @Override
                        public void onGoogleConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.registerUser(accessToken, expiry, SocialAuthUtils.GOOGLE_AUTH, getArguments(), mActivity, customDialog, RegistrationFrg.this);
                        }
                    });
                    break;
                case R.id.facebook_btn: //Facebook Login
                    ShineCommons.trackShineEvents("LoginAttempt", "Facebook", mActivity);
                    DialogUtils.dialogDismiss(alertDialog);
                    resultHandler = FacebookConstants.FacebookConnect(this, new FacebookConstants.FacebookCallBack() {
                        @Override
                        public void onFacebookConnected(final String accessToken, final String expiry, final Dialog customDialog) {
                            SocialAuthUtils.registerUser(accessToken, expiry, SocialAuthUtils.FACEBOOK_AUTH, getArguments(), mActivity, customDialog, RegistrationFrg.this);
                        }
                    });
                    break;
                case R.id.alert_check_layout:
                    if (alertsCheck) {
                        alertsCheck = false;
                        alertBoxImg.setChecked(false);
                    } else {
                        alertsCheck = true;
                        alertBoxImg.setChecked(true);
                    }
                    break;
                case R.id.ok_btn:
                    DialogUtils.dialogDismiss(alertDialog);
                    break;

                case R.id.submit_btn:
                    submitAllDetails();

                    break;

                case R.id.show_hide:
                    if (showField.getText().equals("Show")) {
                        showField.setText("Hide");
                        passwordField
                                .setTransformationMethod(HideReturnsTransformationMethod
                                        .getInstance());
                        passwordField
                                .setSelection(passwordField.getText().length());

                    } else if (showField.getText().equals("Hide")) {
                        showField.setText("Show");
                        passwordField
                                .setTransformationMethod(PasswordTransformationMethod
                                        .getInstance());
                        passwordField
                                .setSelection(passwordField.getText().length());

                    }
                    break;
                case R.id.terms:
                    TermsAndConditionsDialog termsDialog = new TermsAndConditionsDialog();
                    FragmentManager fm = getChildFragmentManager();
                    termsDialog.show(fm, "terms_check");
                    break;
                default:
                    showSpinner(v.getId());
                    ShineCommons.hideKeyboard(mActivity);
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("DEBUG::UJJ", "RegistrationFrg onActivityResult");
        if(resultHandler!=null)
            resultHandler.handleOnActivityResult(mActivity, requestCode, resultCode, data);
    }

    private void submitAllDetails() {
        try {

            JSONArray func_list = new JSONArray();
            JSONArray ind_list = new JSONArray();
            String exp_lookup = "";
            int smsAlert = 0;

            System.out.println("-- submit");
            boolean checkForToastAlert = false;
            String nameText = name.getText().toString().trim();
            String mobileText = mobile.getText().toString().trim();
            String totalExp = totalExpField.getText().toString().trim();
            String emailText = email.getText().toString().trim();
            String functionalArea = funcAreaField.getText().toString().trim();
            String industry = industryField.getText().toString().trim();
            String city = cityField.getText().toString().trim();
            String password = passwordField.getText().toString().trim();


            if (nameText.length() == 0 || !(Pattern.matches(ShineCommons.NAME_REGEX, nameText))) {
                checkForToastAlert = true;
                inputLayoutName.setError("Please enter valid name");
            }
            emailText = emailText.trim();
            if (emailText.trim().length() == 0) {
                checkForToastAlert = true;
                inputLayoutEmail.setError("Please enter valid email");
            }

            if (password.length() < 6 || password.length() > 15) {
                checkForToastAlert = true;
                inputLayoutPassword.setError("Please enter valid password");
            }

            if (mobileText.trim().length() == 0) {
                checkForToastAlert = true;
                inputLayoutMobile.setError("Please enter valid mobile number");
            }

            if (city.length() == 0) {
                checkForToastAlert = true;
                inputLayoutCity.setError("Please enter valid city");
            }


            if (functionalArea.length() == 0) {
                checkForToastAlert = true;
                inputLayoutFunctionalArea.setError("Please enter valid functional area");
            } else {
                String[] i_func = functionalArea.split(",");
                int[] func_area = new int[i_func.length];
                for (int i = 0; i < i_func.length; i++) {
                    if (!i_func[i].equals(""))
                        func_area[i] = Integer.parseInt(URLConfig.FUNCTIONA_AREA_MAP.get(i_func[i].trim()));
                }
                for (int aFunc_area : func_area) {
                    func_list.put(aFunc_area);
                }
            }


            if (industry.length() == 0) {
                checkForToastAlert = true;
                inputLayoutIndustry.setError("Please enter valid industry");
            } else {
                int[] indus;
                if (industry.equals("Any")) {
                    indus = new int[0];
                } else {
                    String[] i_ind = industry.split(",");
                    indus = new int[i_ind.length];
                    for (int i = 0; i < i_ind.length; i++) {
                        if (!i_ind[i].equals(""))
                            indus[i] = Integer.parseInt(URLConfig.INDUSTRY_MAP.get(i_ind[i].trim()));
                    }
                }
                for (int indu : indus) {
                    ind_list.put(indu);
                }
            }

            if (totalExp.length() == 0) {
                checkForToastAlert = true;
                inputLayoutExperience.setError("Please enter valid experience");
            } else {
                int exp = Integer.parseInt(totalExp);
                if (exp == 0)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("0 Yr");
                else if (exp == 1)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("1 Yr");
                else if (exp < 25)
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get(exp + " Yrs");
                else
                    exp_lookup = URLConfig.EXPERIENCE_MAP.get("> 25 Yrs");
                if (exp_lookup == null || exp_lookup.isEmpty()) {
                    inputLayoutExperience.setError("Please enter valid experience");
                }
            }

//            if(checkForToastAlert)
//                return;


            if (!Pattern.matches(ShineCommons.REGEX, emailText)) {
                email.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                inputLayoutEmail.setError("Please enter valid email");
                email.requestFocus();
                return;
            }


            if (password.length() < 6 || password.length() > 15) {
                passwordField
                        .setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                inputLayoutPassword.setError("Invalid password! password should be 6 to 15 characters long.");
                passwordField.requestFocus();
                return;
            }
            if (mobileText.length() < 10) {
                mobile.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                inputLayoutMobile.setError("Please enter  10 digit mobile number");
                mobile.requestFocus();
                return;
            } else if ("0".equals("" + mobileText.charAt(0))) {
                mobile.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                inputLayoutMobile.setError("Mobile number should not start with 0.");
                mobile.requestFocus();
                return;
            } else {
                boolean flag = false;
                int n = mobileText.length();
                for (int i = 0; i < n; i++) {
                    if (mobileText.charAt(i) < 48 || mobileText.charAt(i) > 57) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    mobile.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                    inputLayoutMobile.setError("Mobile number should be numeric");
                    mobile.requestFocus();
                    return;
                }
            }


            if (alertsCheck) {
                smsAlert = 1;
            }

            if(!checkForToastAlert) {
                alertDialog = DialogUtils.showCustomProgressDialog(mActivity,"Please wait...");

                Type listType = new TypeToken<RegistrationModel>() {
                }.getType();
                String url ="";
                if(!TextUtils.isEmpty(ShineSharedPreferences.getVendorId(mActivity))){
                 url= URLConfig.REGISTRATION_URL+"?vendorid="+ShineSharedPreferences.getVendorId(mActivity);}
                else {
                    url=URLConfig.REGISTRATION_URL;
                }
                Log.d("registraion url",url);

                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, new VolleyRequestCompleteListener() {
                    @Override
                    public void OnDataResponse(Object object, String tag) {
                        onRegistrationSuccess((RegistrationModel) object);
                    }

                    @Override
                    public void OnDataResponseError(String error, String tag) {
                        DialogUtils.dialogDismiss(alertDialog);
                        alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                        alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        DialogUtils.dialogDismiss(alertDialog);
                                    }
                                });
                    }
                }, url, listType);

                HashMap registerMap = new HashMap();

                registerMap.put("cell_phone", mobile.getText().toString().trim());
                registerMap.put("candidate_location", URLConfig.CITY_MAP.get(cityField.getText().toString().trim()));
                registerMap.put("name", name.getText().toString().trim());
                registerMap.put("country_code", "91");
                registerMap.put("industry", ind_list);
                registerMap.put("experience_in_months", "0");
                registerMap.put("experience_in_years", exp_lookup);
                registerMap.put("functional_area", func_list);
                registerMap.put("raw_password", passwordField.getText().toString().trim());
                registerMap.put("email", email.getText().toString().trim());
                registerMap.put("sms_alert_flag", smsAlert + "");
                registerMap.put("user_type", user_type);




                downloader.setUsePostMethod(registerMap);

                downloader.execute("mobiApiHitTleRegistration");
            }else {
                Log.d("Reg Page","Some validation error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void onRegistrationSuccess(RegistrationModel rModel){
        UserStatusModel profile = new UserStatusModel();
        Log.d("EXP ", "reg model " + rModel.toString());
        Log.d("EXP ", "exp " + rModel.experience_in_years);
        profile.mobile_no = (rModel.cell_phone);
        profile.full_name = (rModel.name);
        profile.email = (rModel.email);
        profile.candidate_id = (rModel.id);
        profile.experience_in_years = (rModel.experience_in_years);


        ShineSharedPreferences.setUserAccessToken(mActivity, rModel.access_token);
        ShineSharedPreferences.saveUserInfo(mActivity.getApplicationContext(), profile);

        if(!TextUtils.isEmpty(profile_picture_url))
        {
            try {
                ShineSharedPreferences.setLinkedinPictureUrl(
                        profile_picture_url, mActivity);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        ShineSharedPreferences.setCandidateId(mActivity, rModel.id);

        ShineSharedPreferences.setResumeMidout(mActivity, true);

        ShineSharedPreferences.setExpDashFlag(
                mActivity, false);
        try {
            String totalExp = totalExpField.getText().toString();
            if(!totalExp.isEmpty())
            {
                int exp = Integer.parseInt(totalExp);
                if (exp == 0) {
                    ShineSharedPreferences.setExpDashFlag(
                            mActivity, true);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }




        if(profile.mobile_no.equals(mobile_number_trucaller))
        {

            Log.d("---","trueCaller Hit");
            String getUrl = URLConfig.CELLPHONE_VERIFIED_TRUECALLER.replace(URLConfig.CANDIDATE_ID,
                    profile.candidate_id );
            Type type = new TypeToken<EmptyModel>(){}.getType();
            VolleyNetworkRequest downloader1 = new VolleyNetworkRequest(mActivity.getApplicationContext(), new VolleyRequestCompleteListener() {
                @Override
                public void OnDataResponse(Object object, String tag) {

                }

                @Override
                public void OnDataResponseError(String error, String tag) {

                }
            },
                    getUrl, type);

            HashMap verifyMap = new HashMap();
            downloader1.setUsePostMethod(verifyMap);
            downloader1.execute("TRUECALLER_MOBILE_VERIFY");
        }


        ShineSharedPreferences.setEduDashFlag(mActivity, false);

        ShineSharedPreferences.setSkillDashFlag(mActivity, false);
        //  ShineCommons.trackEvent("Logged In", "Konnect attempt", "Phonebook", 0l, mActivity);


        if(user_type==2)
        {
            ShineCommons.trackShineEvents("RegistrationSuccess","Shine",mActivity);
        }




        ShineCommons.sendContact();
        DialogUtils.dialogDismiss(alertDialog);
        if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(mActivity)))
        {
            Log.d("NON_SKIP_TEST ","IN Reg non skip true");
            Intent intent = new Intent(mActivity, ResumeUploadActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(getArguments());
            mActivity.startActivity(intent);
            mActivity.finish();

        }
        else {
            Intent intent = new Intent(mActivity, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(getArguments());
            mActivity.startActivity(intent);
            mActivity.finish();
        }
        MoengageTracking.setLoginAttribute(mActivity);
        MoengageTracking.setProfileAttributes(mActivity);
    }


    @Override
    public void setPrefillData(LinkedInAuthModel model, int type) {
        try {

            if(!TextUtils.isEmpty(model.prefill_details.profile_picture_url))
            {
                profile_picture_url = model.prefill_details.profile_picture_url;

            }

            switch (type){
                case SocialAuthUtils.LINKEDIN_AUTH:
                    user_type = 5;
                    break;
                case SocialAuthUtils.GOOGLE_AUTH:
                    user_type = 10;
                    break;
                case SocialAuthUtils.FACEBOOK_AUTH:
                    user_type = 13;
                    break;
            }


            rootView.findViewById(R.id.reg_link_btn).setVisibility(View.GONE);
            if(trueBtn!=null)
                trueBtn.setVisibility(View.GONE);
            if(model.prefill_details!=null){
                if(!TextUtils.isEmpty(model.prefill_details.email)) {
                    email.setText(model.prefill_details.email + "");
                    email.setEnabled(false);
                }
                if(!TextUtils.isEmpty(model.prefill_details.name)){
                    name.setText(model.prefill_details.name + "");
                }
                if(!TextUtils.isEmpty(model.prefill_details.cell_phone)){
                    mobile.setText(model.prefill_details.cell_phone + "");
                }
                if(!TextUtils.isEmpty(model.prefill_details.experience_in_years)){
                    totalExpField.setText(model.prefill_details.experience_in_years);
                }
                if(model.prefill_details.candidate_location!=0){
                    cityField.setTag(ShineCommons.setTagAndValue(
                            URLConfig.CITY_REVERSE_MAP,
                            URLConfig.CITY_LIST_EDIT, model.prefill_details.candidate_location,
                            cityField));
                }

            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            showSpinner(v.getId());
            ShineCommons.hideKeyboard(mActivity);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle("Register");
        mActivity.hideNavBar();
        mActivity.showBackButton();
        ManualScreenTracker.manualTracker("Registration");
    }

    private void showSpinner(int id) {
        try {
            switch (id) {
                case R.id.fun_area:
                    DialogUtils.showNewMultiSpinnerDialog(
                            getString(R.string.hint_func_area), funcAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST2, mActivity, true);
                    break;
                case R.id.industry:
                    DialogUtils.showNewSingleSpinnerDialog(
                            getString(R.string.hint_industry), industryField,
                            URLConfig.INDUSTRY_LIST2, mActivity, true);
                    break;
                case R.id.city:
                    DialogUtils.showNewMultiSpinnerDialogRadio(
                            getString(R.string.hint_city), cityField,
                            URLConfig.CITY_LIST_EDIT, mActivity);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
