package com.net.shine.adapters;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.CustomWebView;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailParentFrg;
import com.net.shine.fragments.KonnectFrag;
import com.net.shine.models.AdModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.ChromeCustomTabs;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.enrcypt.XOREncryption;

import java.net.URLEncoder;
import java.util.ArrayList;

public class MatchedJobsAdapter extends JobListRecyclerAdapterForAds {

    String add_link = "";

    private boolean is_impression_marked = false;


    @Override
    public void onBindViewHolder(final JobsViewHolder viewHolder, final int position) {
        super.onBindViewHolder(viewHolder, position);


        if (getItemViewType(position) == AD_ITEM) {
            final String admodel = ShineSharedPreferences.getCareerPlusAds(mActivity, ShineSharedPreferences.getCandidateId(mActivity));

            if (!TextUtils.isEmpty(admodel)) {


                viewHolder.imageView.setVisibility(View.VISIBLE);
                Gson gson = new Gson();

                final AdModel model = gson.fromJson(admodel, AdModel.class);

                if (model != null && model.results.size() > 0) {

                    if (URLConfig.AD >= model.results.size()) {

                        URLConfig.AD = 0;
                    }

                    try {
                        Glide.with(mActivity).load(model.results.get(URLConfig.AD).adPath).listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model1, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model1, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // imageView.setImageDrawable(resource);


                                try {
                                    String url = model.results.get(URLConfig.AD).impressionUrl;

                                    if (!TextUtils.isEmpty(url) && !is_impression_marked) {
                                        url = url.replace("App", "Android").replace("APP_SCREEN_NAME", "Matched_Job");

                                        ShineCommons.hitImpressionApi(mActivity, url);

                                        is_impression_marked = true;

//                                        ShineCommons.trackShineEvents("CareerPlusAdImpression", "Matched_Job", mActivity);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }
                        }).into(viewHolder.imageView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    add_link = model.results.get(URLConfig.AD).clickUrl;

//

                }
            } else {
                viewHolder.imageView.setVisibility(View.GONE);


            }

            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!TextUtils.isEmpty(add_link)) {
                        String appendUserData = "";
                        String encrypted_text = "";
                        String finalLink = "";

                        UserStatusModel user = ShineSharedPreferences.getUserInfo(mActivity);


                        String date_time_value = KonnectFrag.getDate(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");


                        if (add_link.contains("APP_SCREEN_NAME")) {
                            finalLink = add_link.replace("APP_SCREEN_NAME", "Matched_Job");

                        }

                        if (user != null) {
                            appendUserData = URLConfig.key + "|" + user.email + "|" + user.mobile_no + "|" + date_time_value;

                            encrypted_text = XOREncryption.encryptDecrypt(appendUserData);


                            Uri uri = Uri.parse(finalLink);
                            String paramValue = uri.getQueryParameter("next");
                            if(paramValue.contains("?")||paramValue.contains("3F")) {
                                finalLink = finalLink +  URLEncoder.encode("&ad_content="+encrypted_text);
                            }
                            else {
                                finalLink = finalLink  + URLEncoder.encode("?ad_content="+encrypted_text);
                            }
                        }

                        if (ShineCommons.appInstalledOrNot("com.android.chrome")) {
                            System.out.println("-- package install");
                            ChromeCustomTabs.openCustomTab(mActivity, finalLink);
                        } else {
                            Bundle bundle = new Bundle();
                            Intent intent = new Intent(mActivity, CustomWebView.class);
                            bundle.putString("shineurl", finalLink);
                            intent.putExtras(bundle);
                            mActivity.startActivity(intent);

                        }
                        ShineCommons.trackShineEvents("CareerPlusAdClick", "Matched_Job", mActivity);

                    }
                }
            });

        }


        viewHolder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (mList.get(viewHolder.getAdapterPosition()) == null) //this means it is title's position
                    {
                        return;
                    }

                    if (inActionMode()) {
                        onItemClickInActionMode(viewHolder.getAdapterPosition());
                        return;
                    }


                    //TODO: Don't create this list every time
                    ArrayList<SimpleSearchModel.Result> jList = new ArrayList<>();
                    int nullpos = Integer.MAX_VALUE;
                    for (int i = 0; i < mList.size(); i++) {
                        if (mList.get(i) != null) {
                            jList.add(mList.get(i));
                        } else {
                            nullpos = i;
                        }
                    }


                    int pos = viewHolder.getAdapterPosition();
                    if (jList != null && jList.size() > 4 && ShineSharedPreferences.isCareerPlusAds(mActivity)) {
                        jList.remove(3);
                        if (pos > 3)
                            pos = pos - 1;

                    }

                    Bundle bundle = new Bundle();
                    bundle.putInt(URLConfig.INDEX, pos);
                    bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jList);
                    BaseFragment fragment = new JobDetailParentFrg();
                    fragment.setArguments(bundle);
                    mActivity.singleInstanceReplaceFragment(fragment);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public MatchedJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list) {
        super(activity, list, "MatchedJobs");

        is_impression_marked = false;
    }


}
