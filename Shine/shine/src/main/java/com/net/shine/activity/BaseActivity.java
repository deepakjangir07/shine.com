package com.net.shine.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.reflect.TypeToken;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.moe.pushlibrary.MoEHelper;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.adapters.JobListRecyclerAdapterForAds;
import com.net.shine.adapters.JobsListRecyclerAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.CustomLogoutMessageFrag;
import com.net.shine.fragments.CustomLogoutProfileFrag;
import com.net.shine.fragments.LoginHomeFrg;
import com.net.shine.fragments.LogoutHomeFrg;
import com.net.shine.fragments.MoreFrag;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.fragments.tabs.BrowseJobsTabFrg;
import com.net.shine.fragments.tabs.InboxTabFrg;
import com.net.shine.fragments.tabs.JobsTabFrg;
import com.net.shine.models.EmptyModel;
import com.net.shine.services.ContactRetrieveService;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.MoengageTracking;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;

/**
 * Created by ujjawal-work on 12/08/16.
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener{


    public GoogleApiClient mGoogleApiClient;
    public static final String UPDATE_TYPE = "update_type";
    public static final String TYPE_FORCED = "type_forced";
    public static final String TYPE_OPTIONAL = "type_optional";
    public static final String UPDATE_BROADCAST_NOTI = "update_notification";
    public boolean selected = false;
    protected BottomNavigationViewEx mBottomNav;
    public ActionBar actionBar;
    private Toolbar toolbar;
    public static final int SEARCH_OPTION =0;
    public static final int JOBS_OPTION =1;
    public static final int MAIL_OPTION =2;
    public static final int PROFILE_OPTION =3;
    public static final int MORE_OPTION =4;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity_view);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN );
        setStatusBarGradiant(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        mBottomNav = (BottomNavigationViewEx) findViewById(R.id.bottom_navigation_bar);
        mBottomNav.enableAnimation(false);
        mBottomNav.enableItemShiftingMode(false);
        mBottomNav.enableShiftingMode(false);
        mBottomNav.setTextVisibility(false);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        try {
            if (actionBar != null) {
                Drawable d = ContextCompat.getDrawable(this, R.drawable.menu_view);
                actionBar.setBackgroundDrawable(d);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }




        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                BaseFragment currentFrag = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);

                if(JobListRecyclerAdapterForAds.actionMode!=null)
                    JobListRecyclerAdapterForAds.actionMode.finish();

                if(JobsListRecyclerAdapter.actionMode!=null)
                    JobsListRecyclerAdapter.actionMode.finish();

                switch (item.getItemId()){
                    case R.id.action_search:
                        if(ShineSharedPreferences.getUserInfo(getApplicationContext())==null) {
                            BaseFragment frag = new LogoutHomeFrg();
                            if(!(currentFrag instanceof LogoutHomeFrg))
                                singleInstanceReplaceFragment(frag);
                        }
                        else {
                            BaseFragment frag = new LoginHomeFrg();
                            if(!(currentFrag instanceof LoginHomeFrg))
                                singleInstanceReplaceFragment(frag);
                        }

                        break;

                    case R.id.action_jobs:
                        if(ShineSharedPreferences.getUserInfo(getApplicationContext())==null){
                            BaseFragment frag = new BrowseJobsTabFrg();
                            if(!(currentFrag instanceof BrowseJobsTabFrg))
                                singleInstanceReplaceFragment(frag);
                        }
                        else {
                            BaseFragment frag1 = new JobsTabFrg();
                            if (!(currentFrag instanceof JobsTabFrg))
                                singleInstanceReplaceFragment(frag1);
                        }


                        break;

                    case R.id.action_mail:

                        if(ShineSharedPreferences.getUserInfo(getApplicationContext())==null){
                            BaseFragment frag = new CustomLogoutMessageFrag();
                            if(!(currentFrag instanceof CustomLogoutMessageFrag))
                                singleInstanceReplaceFragment(frag);
                        }
                        else {

                            BaseFragment frag2 = new InboxTabFrg();
                            if (!(currentFrag instanceof InboxTabFrg))
                                singleInstanceReplaceFragment(frag2);
                        }

                        break;

                    case R.id.action_profile:

                        if(ShineSharedPreferences.getUserInfo(getApplicationContext())==null){
                            BaseFragment frag = new CustomLogoutProfileFrag();
                            if(!(currentFrag instanceof CustomLogoutProfileFrag))
                                singleInstanceReplaceFragment(frag);
                            else
                                singleInstanceReplaceFragment(frag);

                        }
                        else {
                            BaseFragment frag4 = new MyProfileFrg();
                            if (!(currentFrag instanceof MyProfileFrg))
                                singleInstanceReplaceFragment(frag4);
                        }

                        break;

                    case R.id.action_more:
                        BaseFragment frag5 = new MoreFrag();
                        if(!(currentFrag instanceof MoreFrag))
                            singleInstanceReplaceFragment(frag5);

                        break;
                }
                return true;
            }
        });

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.menu_view);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    public BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            Bundle args = getIntent().getExtras();
            if (args != null) {
                DialogUtils.showUpdateDialog(BaseActivity.this, args.getString(UPDATE_TYPE, ""));
            }
        }
    };




    public void hideNavBar(){
        if(mBottomNav!=null){
        mBottomNav.setVisibility(View.GONE);
        }
    }

    public void showNavBar(){
        if(mBottomNav!=null)
        mBottomNav.setVisibility(View.VISIBLE);
    }
    public void setActionBarVisible(boolean flag) {
        if (actionBar != null) {
            if (flag)
                actionBar.show();
            else
                actionBar.hide();
        }
    }
    public void showBackButton(){
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    public void hideBackButton(){
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }


    public void addFragmentWithBackStack(Fragment frg) {

        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.custom_fade_in,R.anim.custom_fade_out)
                .add(R.id.content_frame,frg)
                .addToBackStack(null)
                .commit();
    }


    public void replaceFragmentWithBackStack(Fragment frg) {

        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.custom_fade_in,R.anim.custom_fade_out)
                .replace(R.id.content_frame,frg)
                .addToBackStack(null)
                .commit();
    }


    public void setFragment(Fragment frg) {

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_frame,frg)
                .addToBackStack(null)
                .commit();
    }

    public void popone(){
        try {
            getSupportFragmentManager().popBackStack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void setBaseFrg(Fragment frg){
        getSupportFragmentManager().beginTransaction().add(R.id.content_frame,frg).commit();
    }

    public void replaceFragment(Fragment frg) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame,frg)
                .commit();
    }

    public void singleInstanceReplaceFragment (Fragment fragment){
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            Log.d("Mlist",backStateName);
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment,backStateName);
            ft.addToBackStack(backStateName);
            ft.commit();
            manager.executePendingTransactions();
        }


    }

    public void singleInstanceReplaceFragment(Fragment fragment,boolean forceReplace){
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped||forceReplace){ //fragment not in back stack, create it.
            Log.d("Mlist",backStateName);
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment,backStateName);
            ft.addToBackStack(backStateName);
            ft.commit();
            manager.executePendingTransactions();
        }


    }

    public void showSelectedNavButton(int position){
        try {
            mBottomNav.getMenu().getItem(position).setChecked(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        FragmentManager manager = getSupportFragmentManager();


        Log.d("BackPressed ","OnBackPressed");
        if(manager.getBackStackEntryCount() > 0) {
            super.onBackPressed();

            Log.d("BackPressed ","OnBackPressed inside IF "+manager.getBackStackEntryCount());

            BaseFragment currentFrag = (BaseFragment) manager.findFragmentById(R.id.content_frame);
            if(currentFrag instanceof LoginHomeFrg){
                mBottomNav.getMenu().getItem(SEARCH_OPTION).setChecked(true);
            }
            else if(currentFrag instanceof JobsTabFrg){
                mBottomNav.getMenu().getItem(JOBS_OPTION).setChecked(true);
            }
            else if(currentFrag instanceof InboxTabFrg){
                mBottomNav.getMenu().getItem(MAIL_OPTION).setChecked(true);
            }
            else if(currentFrag instanceof MyProfileFrg){
                mBottomNav.getMenu().getItem(PROFILE_OPTION).setChecked(true);
            }
            else if(currentFrag instanceof BrowseJobsTabFrg){
                mBottomNav.getMenu().getItem(JOBS_OPTION).setChecked(true);
            }
            else if (currentFrag instanceof LogoutHomeFrg){
                mBottomNav.getMenu().getItem(SEARCH_OPTION).setChecked(true);
            }

            else if (currentFrag instanceof CustomLogoutProfileFrag){
                mBottomNav.getMenu().getItem(PROFILE_OPTION).setChecked(true);
            }
            else if (currentFrag instanceof CustomLogoutMessageFrag){
                mBottomNav.getMenu().getItem(MAIL_OPTION).setChecked(true);
            }
            else if(currentFrag instanceof SearchResultFrg){
                mBottomNav.getMenu().getItem(SEARCH_OPTION).setChecked(true);
            }
            else{
                mBottomNav.getMenu().getItem(MORE_OPTION).setChecked(true);
            }


            if(currentFrag instanceof SearchResultFrg) {

                if(((SearchResultFrg) currentFrag).svo!=null&&((SearchResultFrg) currentFrag).svo.isFromSimilarJobs()) {
                    setTitle(getString(R.string.similiar_jobs));

                }
                else
                    if(((SearchResultFrg) currentFrag).fromDiscoverConnect)
                    {
                        setTitle("Jobs");
                    }
                    showNavBar();
                setActionBarVisible(true);
            }
        }


        else {

            BaseFragment currentFrag = (BaseFragment) manager.findFragmentById(R.id.content_frame);

            if(currentFrag instanceof SearchResultFrg) {

                if(((SearchResultFrg) currentFrag).svo!=null&&((SearchResultFrg) currentFrag).svo.isFromSimilarJobs()) {
                    setTitle(getString(R.string.similiar_jobs));

                }
                else
                if(((SearchResultFrg) currentFrag).fromDiscoverConnect)
                {
                    setTitle("Jobs");
                }
                showNavBar();
                setActionBarVisible(true);
            }
            super.onBackPressed();

            Log.d("BackPressed ","OnBackPressed inside ELSE");

        }
    }

    @Override
    public void onClick(View v) {

    }
    public void logout(String message){
        try {


            if (ShineSharedPreferences.getUserInfo(this) != null
                    && ShineSharedPreferences.getClientAccessToken(this) != null
                    && ShineSharedPreferences.getUserAccessToken(this) != null) {
                Type type = new TypeToken<EmptyModel>() {
                }.getType();
                VolleyNetworkRequest req = new VolleyNetworkRequest(this, new VolleyRequestCompleteListener() {
                    @Override
                    public void OnDataResponse(Object object, String tag) {
                        MoEHelper.getInstance(getApplicationContext()).logoutUser();
                        MyApplication.getDataBase().getRecentSearchList().clear();
                        MoengageTracking.setProfileAttributes(BaseActivity.this);
                        ShineCommons.trackShineEvents("Logout","success",BaseActivity.this);

                    }

                    @Override
                    public void OnDataResponseError(String error, String tag) {

                    }
                },
                        URLConfig.CLEAR_GCM_URL + "?shine_id=" + ShineSharedPreferences.getCandidateId(this), type);
                req.execute("clearGCM");
            } else {
                Log.d("Error:", "Unable to Send Clear Cache Hit");
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            stopService(new Intent(this, ContactRetrieveService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ShineSharedPreferences.signoutUser(this, message);
            URLConfig.jobAppliedSet.clear();
            ShineSharedPreferences.clearAlerts(this);
            MyApplication.getInstance().getRequestQueue().getCache().clear();
            DataCaching.removeAllObjects(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);



        finish();
    }


    public  void hitForRedirectURL(String url)
    {
        ShineCommons.hitJDpixelApi(BaseActivity.this,url);
    }


    public void deeplinkFromRedirectUrl(String url){

        Intent intent = new Intent(this, SplashScreen.class);
        intent.setData(Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }



    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(0, 0);
        try {
            if(updateReceiver!=null) {
                IntentFilter filter = new IntentFilter(UPDATE_BROADCAST_NOTI);
                registerReceiver(updateReceiver, filter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("DEBUG ","onResume Base Activity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if(updateReceiver!=null) {
                unregisterReceiver(updateReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
