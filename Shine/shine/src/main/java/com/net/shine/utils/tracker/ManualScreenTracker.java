package com.net.shine.utils.tracker;


import android.util.Log;

public class ManualScreenTracker {

	
	public static void manualTracker(String manualTrackerVal){
        try {

            Log.d(">>>>>trackingScreen",manualTrackerVal);

            EasyTracker.getTracker().sendView(manualTrackerVal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
