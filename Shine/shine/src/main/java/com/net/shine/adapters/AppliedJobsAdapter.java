package com.net.shine.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.activity.FriendPopupActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.JobDetailFrg;
import com.net.shine.fragments.SearchResultFrg;
import com.net.shine.models.DiscoverModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.SimpleSearchVO;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.views.BetterPopupWindow;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class AppliedJobsAdapter extends BaseAdapter {
    private BaseActivity mContext;
    private ArrayList<SimpleSearchModel.Result> mList;

    private static String mskill = "";

    public static String mLocations;
    public PopupJobcountClickListener callBackClikcListener;

    public AppliedJobsAdapter(BaseActivity activity, ArrayList<SimpleSearchModel.Result> list, PopupJobcountClickListener popupJobcountClickListener) {
        this.mContext = activity;
        this.mList = list;
        this.callBackClikcListener=popupJobcountClickListener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int postition) {
        return mList.get(postition);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View v = convertView;

        AppliedJobsViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.grid_table_row, viewGroup, false);
            viewHolder = new AppliedJobsViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (AppliedJobsViewHolder) v.getTag();
        }
        viewHolder.title.setText(mList.get(position).jobTitle);
        viewHolder.company.setText(mList.get(position).comp_name);

        viewHolder.similar_job_in_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleSearchVO sVO = new SimpleSearchVO();
                sVO.setJobId(""+mList.get(position).jobId);
                sVO.setFromSimilarJobs(true);
                sVO.setHideModifySearchButton(true);
                if (ShineSharedPreferences.getUserInfo(mContext) != null) {
                    String URL = URLConfig.SIMILAR_JOBS_URL_LOGIN.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mContext));
                    sVO.setUrl(URL);

                } else {
                    sVO.setUrl(URLConfig.SIMILAR_JOBS_URL_LOGOUT);
                }
                Bundle bundle = new Bundle();
                bundle.putSerializable(URLConfig.SEARCH_VO_KEY, sVO);
                BaseFragment fragment = new SearchResultFrg();
                fragment.setArguments(bundle);
                mContext.replaceFragmentWithBackStack(fragment);

            }
        });

        if(mList.get(position).job_skills==null || mList.get(position).job_skills.trim().isEmpty())
        {
            viewHolder.skills.setVisibility(View.GONE);

        }
        else
        {
            viewHolder.skills.setVisibility(View.VISIBLE);
        }

        if (mList.get(position).jJobType==2){

            viewHolder.walkin.setVisibility(View.VISIBLE);
            viewHolder.divider.setVisibility(View.VISIBLE);
            viewHolder.frndCountRow.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mList.get(position).getVenue())){
                viewHolder.venue.setVisibility(View.VISIBLE);
                viewHolder.venue.setText(Html.fromHtml("<b>Venue </b> " + mList.get(position).getVenue()));
            }

            else {
                viewHolder.venue.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(mList.get(position).getDate())){
                viewHolder.date.setVisibility(View.VISIBLE);
                viewHolder.date.setText(Html.fromHtml("<b>Date </b>"+mList.get(position).getDate()));
            }
            else {
                viewHolder.date.setVisibility(View.GONE);

            }
        } else {
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.venue.setVisibility(View.GONE);
            viewHolder.walkin.setVisibility(View.GONE);
            viewHolder.divider.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(mList.get(position).jRUrl)){
            viewHolder.frndCountRow.setVisibility(View.GONE);
        }

        DiscoverModel.Company mDiscoverFriendsModel = mList.get(position).frnd_model;
        if (mDiscoverFriendsModel != null && mDiscoverFriendsModel.data.friends.size() > 0) {
            setConnection(viewHolder, mDiscoverFriendsModel, mList.get(position));
        }
        else
            viewHolder.frndCountRow.setVisibility(View.GONE);

        showMoreInLocation(viewHolder, mList.get(position).job_loc_str, mList.get(position).jobExperience, mList.get(position).job_skills);


        return v;
    }

    public void setConnection(AppliedJobsViewHolder viewHolder,DiscoverModel.Company dfm, final SimpleSearchModel.Result searchResultVO) {
        {
            viewHolder.frndCountRow.setVisibility(View.VISIBLE);
            int friend_count = dfm.data.friends.size();
            if (friend_count > 1) {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#555555'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> + "
                                        + "<b>"
                                        + (dfm.data.friends
                                        .size() - 1)
                                        + "</b> connections "
                                        + "<font color='#555555'>work here</font>"));
            } else {
                viewHolder.frndCount
                        .setText(Html
                                .fromHtml("<font color='#2c84cc'>"
                                        + dfm.data.friends
                                        .get(0).name
                                        + "</font> "
                                        + "<font color='#555555'> works here</font>"));
            }
            viewHolder.frndCountRow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Bundle args = new Bundle();
                    args.putSerializable("svo", searchResultVO);
                    args.putInt("fromScreen", JobDetailFrg.FROM_APPLIED_JOBS_SCREEN);
                    Intent intent = new Intent(mContext, FriendPopupActivity.class);
                    intent.putExtras(args);
                    mContext.startActivity(intent);
                    ShineCommons.trackShineEvents("KonnectFriendList","AppliedJobs",(AppCompatActivity) mContext);



                }
            });
        }

    }

    public void rebuildListObject(ArrayList<SimpleSearchModel.Result> itemList) {

        this.mList = itemList;
    }

    public void showMoreInLocation(final AppliedJobsViewHolder viewHolder, final String location,
                                   final String exp, final String skill) {


        if (skill != null && !skill.isEmpty()) {

            viewHolder.skills.setText(skill);


        }

        if(exp==null || exp.equals("")){
            viewHolder.tv_experience.setVisibility(View.GONE);
        }
        else{
            viewHolder.tv_experience.setVisibility(View.VISIBLE);
            viewHolder.tv_experience.setText(exp+"  ");
        }

        final StringTokenizer st = new StringTokenizer(location, "/");
        if (st.countTokens() > 1) {
            final String s2;
            {
                s2 = st.nextToken()+"";
            }

            viewHolder.tv_location.setText(s2.toString());
            viewHolder.tv_more.setVisibility(View.VISIBLE);
            viewHolder.tv_more.setText(Html.fromHtml("<font color = '#333333'>"
                    +" & "
                    +"</font>"
                    +"<font color = '#2c84cc'>"
                    +(st.countTokens())
                    + " more"
                    +"</font>"));
        }
        else
        {
            viewHolder.tv_location.setText(location);
            viewHolder.tv_more.setVisibility(View.GONE);
        }


        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location_more = "";
                String[] arr_loc = location.split("/");
                for (int i = 0; i < arr_loc.length; i++) {
                    if (i == (arr_loc.length - 1)) {
                        location_more = location_more + arr_loc[i].trim();
                    } else {
                        location_more = location_more + arr_loc[i].trim()
                                + ", ";
                    }
                }
                mLocations = location_more;
                DemoPopupWindow demoPopupWindow = new DemoPopupWindow(viewHolder.tv_more);
                demoPopupWindow.showLikePopDownMenu();
            }
        });

    }

    private static class DemoPopupWindow extends BetterPopupWindow {

        public DemoPopupWindow(View anchor) {
            super(anchor);


        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView tv_locations = (TextView) root.findViewById(R.id.loc_txt);
            tv_locations.setText(mLocations);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }

    }
    private static class skillPopupWindow extends BetterPopupWindow {

        public skillPopupWindow(View anchor) {
            super(anchor);

        }

        @Override
        protected void onCreate() {
            // inflate layout
            LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ViewGroup root = (ViewGroup) inflater.inflate(
                    R.layout.location_dialog, null);

            TextView skills = (TextView) root.findViewById(R.id.loc_txt);
            skills.setText(mskill);

            // set the inlated view as what we want to display
            this.setContentView(root);
        }
    }
    private class AppliedJobsViewHolder {
        public View apply_job;
        public View applied_job;
        public TextView frndCount;
        public RelativeLayout frndCountRow;
        public TextView title;
        public TextView company;
        TextView tv_location;
        TextView skills;
        LinearLayout walkin;
        View divider;
        TextView tv_more;
        TextView date,venue;
        TextView tv_experience;
        TextView similar_job_in_list;



        public AppliedJobsViewHolder(View view) {

            walkin = (LinearLayout) view.findViewById(R.id.walkin);
            divider = view.findViewById(R.id.divider);
            apply_job = view.findViewById(R.id.apply_job);
            apply_job.setVisibility(View.GONE);
            applied_job = view.findViewById(R.id.applied_job);
            applied_job.setVisibility(View.VISIBLE);
            date = (TextView) view.findViewById(R.id.date);
            venue = (TextView) view.findViewById(R.id.venue);
            similar_job_in_list = (TextView) view.findViewById(R.id.applied_similar_btn_list);
            similar_job_in_list.setVisibility(View.VISIBLE);


            frndCount = (TextView) view
                    .findViewById(R.id.frndsCount);
            frndCountRow = (RelativeLayout) view
                    .findViewById(R.id.frnd_row);
            frndCountRow.setVisibility(View.GONE);


            title = (TextView) view.findViewById(R.id.title);
            company = (TextView) view.findViewById(R.id.company);

            skills = (TextView) view.findViewById(R.id.skills);
            tv_location = (TextView) view.findViewById(R.id.location);
            tv_more = (TextView) view.findViewById(R.id.more);
            tv_experience = (TextView) view
                    .findViewById(R.id.experience);


        }

}




    public interface PopupJobcountClickListener {

        void JobCountOnClick(DiscoverModel.Company dfm);
    }





}
