package com.net.shine.fragments.components;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.fragments.MyProfileFrg;
import com.net.shine.models.DesiredJobDetails;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.TextChangeListener;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.utils.tracker.ManualScreenTracker;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by gaurav on 9/7/15.
 */
public class DesiredJobDetailsEditFragment extends BaseFragment implements View.OnClickListener, View.OnFocusChangeListener, VolleyRequestCompleteListener {


    private DesiredJobDetails desiredModel;
    private View view;
    private EditText funAreaField;
    private EditText cityName;
    private EditText indusNameField;
    private LinearLayout desireDetails;
    private EditText shiftTypeField;
    private EditText jobTypeField;
    private EditText salaryField;
    private RadioButton rb_shift_no;
    private RadioButton rb_shift_yes;
    private RadioGroup rd_group_shift;
    private Dialog alertDialog;
    private final String EDIT_DESIRED_JOB_DETAILS = "edit_desired_job_details";
    TextInputLayout inputLayoutLocation, inputLayoutFunctionalArea, inputLayoutIndustry, inputLayoutSalary, inputLayoutJobType, inputLayoutShiftType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.desired_edit_comp, container, false);

        try {
            desireDetails = (LinearLayout) view.findViewById(R.id.desire_details);
            TextView tv_title = (TextView) view.findViewById(R.id.header_title);
            tv_title.setText("Desired job details");
            desireDetails.requestFocus();

            Bundle bundle = getArguments();

            Gson gson = new Gson();

            String json = bundle.getString(URLConfig.KEY_MODEL_NAME);
            System.out.println("DESIRED EDIT" + json);
            desiredModel = gson.fromJson(json, DesiredJobDetails.class);
            addUpdateDesired();
            TextChangeListener.addListener(funAreaField, inputLayoutFunctionalArea, mActivity);
            TextChangeListener.addListener(indusNameField, inputLayoutIndustry, mActivity);
            TextChangeListener.addListener(cityName, inputLayoutLocation, mActivity);
            TextChangeListener.addListener(salaryField, inputLayoutSalary, mActivity);
            TextChangeListener.addListener(jobTypeField, inputLayoutJobType, mActivity);
            TextChangeListener.addListener(shiftTypeField, inputLayoutShiftType, mActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            showSpinner(v.getId());
    }

    private void showSpinner(int id) {
        try {
            ShineCommons.hideKeyboard(mActivity);

            switch (id) {
                case R.id.city_name_des:

                    DialogUtils.showNewMultiSpinnerDialog(
                            getString(R.string.hint_select_city), cityName,
                            URLConfig.CITY_LIST, mActivity, false);

                    break;
                case R.id.industry_name_field_des:


                    DialogUtils.showNewSingleSpinnerDialog(
                            getString(R.string.hint_industry), indusNameField,
                            URLConfig.INDUSTRY_LIST2, mActivity, true);

                    break;
                case R.id.func_area_field_des:


                    DialogUtils.showNewMultiSpinnerDialog(
                            getString(R.string.hint_func_area), funAreaField,
                            URLConfig.FUNCTIONAL_AREA_LIST2, mActivity, true);


                    break;
                case R.id.shift_type_field:

                    DialogUtils.showNewSingleSpinnerDialog(
                            "Desired Shift Type", shiftTypeField,
                            URLConfig.SHIFT_TYPE_LIST, mActivity, false);

                    break;
                case R.id.annual_salary_field_des:
                    DialogUtils.showNewSingleSpinnerRadioDialog(
                            getString(R.string.hint_annual_salary), salaryField,
                            URLConfig.ANNUAL_SALARY_LIST, mActivity);
                    break;
                case R.id.job_type_field:
                    DialogUtils.showNewSingleSpinnerRadioDialog("Desired job type",
                            jobTypeField, URLConfig.JOB_TYPE_LIST, mActivity);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.update_btn:
                saveDesiredDetails();


                break;
            case R.id.cancel_btn:

                mActivity.popone();

                Bundle bundle = new Bundle();

                bundle.putInt(MyProfileFrg.SCROLL_TO, MyProfileFrg.SCROLL_TO_DESIRED);

                mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));
                break;
            case R.id.ok_btn:
                DialogUtils.dialogDismiss(alertDialog);
                break;
            default:
                showSpinner(v.getId());
                break;
        }
    }

    private void addUpdateDesired() {
        try {


            inputLayoutFunctionalArea = (TextInputLayout) view.findViewById(R.id.input_layout_fun_area);
            inputLayoutIndustry = (TextInputLayout) view.findViewById(R.id.input_layout_industry);
            inputLayoutJobType = (TextInputLayout) view.findViewById(R.id.input_layout_job_type);
            inputLayoutLocation = (TextInputLayout) view.findViewById(R.id.input_layout_cityname);
            inputLayoutSalary = (TextInputLayout) view.findViewById(R.id.input_layout_salary);
            inputLayoutShiftType = (TextInputLayout) view.findViewById(R.id.input_layout_title);
            funAreaField = (EditText) view
                    .findViewById(R.id.func_area_field_des);


            int[] i_func = desiredModel.getFunctionalArea();
            String func_text = "";
            int[] func_tag_index = new int[i_func.length];


            for (int i = 0; i < i_func.length; i++) {

                int index = ShineCommons.setTagAndValue(
                        URLConfig.FUNCTIONA_AREA_REVERSE_MAP, URLConfig.FUNCTIONAL_AREA_LIST2,
                        i_func[i], funAreaField);
                func_text = func_text + URLConfig.FUNCTIONAL_AREA_LIST2[index] + ",";
                func_tag_index[i] = index;

            }
            funAreaField.setTag(func_tag_index);
            if (func_text.length() > 0) {
                func_text = func_text.substring(0, func_text.length() - 1);
            }
            funAreaField.setText(func_text);
            funAreaField.setOnClickListener(this);
            funAreaField.setOnFocusChangeListener(this);

            cityName = (EditText) view.findViewById(R.id.city_name_des);
            int[] location = desiredModel.getLocation();
            if (location.length != 0) {
                String loc_text = "";
                int[] tag_index = new int[location.length];


                for (int i = 0; i < location.length; i++) {

                    int index = ShineCommons.setTagAndValue(URLConfig.CITY_REVERSE_MAP,
                            URLConfig.CITY_LIST, (location[i]),
                            cityName);

                    loc_text = loc_text + URLConfig.CITY_LIST[index] + ",";
                    tag_index[i] = index;

                }

                cityName.setTag(tag_index);
                loc_text = loc_text.substring(0, loc_text.length() - 1);
                cityName.setText(loc_text);
            } else {
                cityName.setText("Any");
                cityName.setTag(new int[0]);
            }
            cityName.setOnClickListener(this);
            cityName.setOnFocusChangeListener(this);

            indusNameField = (EditText) view
                    .findViewById(R.id.industry_name_field_des);
            int[] industry = desiredModel.getIndustry();
            if (industry.length != 0) {
                String indus_text = "";
                int[] indus_tag_index = new int[industry.length];

                List<String> excludedHashIndusList = new ArrayList<String>();
                String[] tempIndusList = URLConfig.INDUSTRY_LIST2;

                for (int i = 0; i < tempIndusList.length; i++) {
                    if (!tempIndusList[i].startsWith("#")) {
                        excludedHashIndusList.add(tempIndusList[i]);
                    }
                }
                final String[] excludedHashIndus = excludedHashIndusList
                        .toArray(new String[excludedHashIndusList.size()]);

                for (int i = 0; i < industry.length; i++) {

                    int index = ShineCommons.setTagAndValue(URLConfig.INDUSTRY_REVERSE_MAP,
                            excludedHashIndus, (industry[i]),
                            indusNameField);
                    indus_text = indus_text + excludedHashIndus[index] + ",";
                    indus_tag_index[i] = index;

                }
                indusNameField.setTag(indus_tag_index);
                indus_text = indus_text.substring(0, indus_text.length() - 1);
                indusNameField.setText(indus_text);
            } else {
                indusNameField.setText("Any");
                int[] indus_tag_index = new int[1];
                indusNameField.setTag(indus_tag_index);
            }

            indusNameField.setOnClickListener(this);
            indusNameField.setOnFocusChangeListener(this);

            salaryField = (EditText) view
                    .findViewById(R.id.annual_salary_field_des);
            int int_tag_sal = 0;
            if (desiredModel.getMinSalary().length != 0) {

                int_tag_sal = desiredModel.getMinSalary()[0];
            }


            //Although lookup mappings are exactly same as indexes in list,
            //we still better be sure
            int salindex = 0;
            String salary = URLConfig.ANNUAL_SALARY_REVRSE_MAP.get(int_tag_sal + "");
            if (salary != null) {
                for (int i = 0; i < URLConfig.ANNUAL_SALARY_LIST.length; i++) {
                    if (salary.equals(URLConfig.ANNUAL_SALARY_LIST[i])) {
                        salindex = i;
                    }
                }
            }
            salaryField.setTag(salindex);
            salaryField.setText(URLConfig.ANNUAL_SALARY_LIST[salindex]);

            salaryField.setOnClickListener(this);
            salaryField.setOnFocusChangeListener(this);


            jobTypeField = (EditText) view
                    .findViewById(R.id.job_type_field);

            //Although lookup mappings are exactly same as indexes in list,
            //we still better be sure
            int[] jobType = desiredModel.getJobType();
            int index = 0;
            if (jobType.length != 0) {
                for (int i = 0; i < URLConfig.jOB_TYPE_LIST_VAL.length; i++) {
                    if (("" + jobType[0]).equals(URLConfig.jOB_TYPE_LIST_VAL[i])) {
                        index = i;
                    }
                }
            }
            jobTypeField.setTag(index);
            jobTypeField.setText(URLConfig.JOB_TYPE_LIST[index]);

            jobTypeField.setOnClickListener(this);
            jobTypeField.setOnFocusChangeListener(this);

            shiftTypeField = (EditText) view
                    .findViewById(R.id.shift_type_field);

            shiftTypeField.setOnClickListener(this);
            shiftTypeField.setOnFocusChangeListener(this);

            rb_shift_no = (RadioButton) view
                    .findViewById(R.id.radioShiftNo);
            rb_shift_yes = (RadioButton) view
                    .findViewById(R.id.radioShiftYes);
            rd_group_shift = (RadioGroup) view
                    .findViewById(R.id.radioShift);

            final int[] shift_choice = desiredModel.getShiftType();

            // Log.e(":::TAG SHIFT RADIO", shift_choice);
            if (shift_choice.length == 0) {
                rd_group_shift.check(R.id.radioShiftNo);

                shiftTypeField.setVisibility(View.GONE);
                inputLayoutShiftType.setVisibility(View.GONE);
                shiftTypeField.setTag(new int[0]);

            } else {


//                int [] shiftType=new int[shift_choice.length];

                String shift_text = "";
                int[] shift_tag = new int[shift_choice.length];
                for (int i = 0; i < shift_choice.length; i++) {
                    shift_text = shift_text
                            + URLConfig.SHIFT_TYPE_LIST[shift_choice[i] - 1] + ", ";
                    shift_tag[i] = shift_choice[i] - 1;
                }
                shift_text = shift_text.substring(0, shift_text.length() - 2);
                shiftTypeField.setTag(shift_tag);
                shiftTypeField.setText(shift_text);
                shiftTypeField.setVisibility(View.VISIBLE);
                rd_group_shift.check(R.id.radioShiftYes);
            }

            rd_group_shift
                    .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(RadioGroup group,
                                                     int checkedId) {
                            // TODO Auto-generated method stub
                            switch (checkedId) {
                                case R.id.radioShiftNo:
                                    shiftTypeField.setVisibility(View.GONE);
                                    inputLayoutShiftType.setVisibility(View.GONE);
                                    break;
                                case R.id.radioShiftYes:
                                    shiftTypeField.setVisibility(View.VISIBLE);
                                    inputLayoutShiftType.setVisibility(View.VISIBLE);
                                    String shift_text = "";
                                    int[] shiftChoice = desiredModel.getShiftType();
                                    // Log.e("TAG VAL SHIFT ONCHANGE",
                                    // tag_val_shift);
                                    if (shiftChoice.length != 0) {
                                        shiftTypeField.setTag(shiftChoice);
                                        for (int i = 0; i < shiftChoice.length; i++) {
                                            shift_text = shift_text
                                                    + URLConfig.SHIFT_TYPE_LIST[shiftChoice[i] - 1]
                                                    + ", ";
                                        }
                                        shift_text = shift_text.substring(0,
                                                shift_text.length() - 2);
                                    } else {
                                        shiftTypeField.setTag(new int[0]);
                                    }

                                    shiftTypeField.setText(shift_text);
                                    shiftTypeField.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
            view.findViewById(R.id.update_btn)
                    .setOnClickListener(this);
            view.findViewById(R.id.cancel_btn).setOnClickListener(
                    this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveDesiredDetails() {
        try {

            String desired_func_area = "" + funAreaField.getText();
            desired_func_area = desired_func_area.trim();
            int[] func_area;
            if (desired_func_area.length() == 0) {
                func_area = new int[0];
                // showToastAlert(getString(R.string.func_area_empty_msg));
                inputLayoutFunctionalArea.setError("Please select desired functional area");
                return;
            } else {
                String[] i_func = desired_func_area.split(",");
                func_area = new int[i_func.length];
                for (int i = 0; i < i_func.length; i++) {
                    if (!i_func[i].equals(""))
                        func_area[i] = Integer.parseInt(URLConfig.FUNCTIONA_AREA_MAP.get(i_func[i].trim()));
                }
            }

            String desired_location = "" + cityName.getText();
            desired_location = desired_location.trim();
            int[] location;
            if (desired_location.equals("Any")) {
                desired_location = "0";
                location = new int[0];
            } else {

                String[] i_loc = desired_location.split(",");
                location = new int[i_loc.length];
                for (int i = 0; i < i_loc.length; i++) {
                    if (!i_loc[i].equals(""))
                        location[i] = Integer.parseInt(URLConfig.CITY_MAP.get(i_loc[i].trim()));
                }

            }

            String desired_indus = "" + indusNameField.getText();
            desired_indus = desired_indus.trim();
            int[] indus;
            if (desired_indus.equals("Any")) {
                desired_indus = "0";
                indus = new int[0];
            } else {

                String[] i_ind = desired_indus.split(",");
                indus = new int[i_ind.length];
                for (int i = 0; i < i_ind.length; i++) {

                    if (!i_ind[i].equals(""))
                        indus[i] = Integer.parseInt(URLConfig.INDUSTRY_MAP.get(i_ind[i].trim()));
                }


            }

            String desired_salary = "" + salaryField.getText();
            desired_salary = desired_salary.trim();
            int[] salary = new int[1];
            if (desired_salary.length() == 0) {
                // showToastAlert(getString(R.string.salary_empty_msg));
                inputLayoutSalary.setError("Please select annual salary");

                return;
            } else {

                salary[0] = Integer.parseInt(URLConfig.ANNUAL_SALARY_MAP
                        .get(desired_salary));
            }

            String desired_job_type = "" + jobTypeField.getText();
            desired_job_type = desired_job_type.trim();
            int[] job_type = new int[1];
            if (desired_job_type.length() == 0) {
                // showToastAlert(getString(R.string.desire_jobtype_empty_msg));
                inputLayoutJobType.setError("Please select a desired job type");
                return;
            } else {
                String desired_tag = jobTypeField.getTag().toString();
                System.out.println("TYPE" + desired_tag);
                job_type[0] = Integer.parseInt(URLConfig.jOB_TYPE_LIST_VAL[Integer
                        .parseInt(desired_tag)]);

            }

            String shiftType = "";
            int[] shiftTypeArray;
            int sel_id = rd_group_shift.getCheckedRadioButtonId();
            if (sel_id == R.id.radioShiftYes) {
                StringTokenizer st = new StringTokenizer(shiftTypeField.getText()
                        .toString(), ",");
                String[] arrShifts = shiftTypeField.getText()
                        .toString().split(",");
                shiftTypeArray = new int[arrShifts.length];
                if (shiftTypeArray.length == 0) {
                    //Toast.makeText(mActivity, "Please select your desired shift type", Toast.LENGTH_SHORT).show();
                    inputLayoutShiftType.setError("Please select your desired shift type");
//                    shiftTypeField.getBackground().setColorFilter(Color.parseColor("#fab7b7"), PorterDuff.Mode.SRC_IN);
                    return;
                }

                for (int i = 0; i < arrShifts.length; i++) {
//                    Log.d("DEBUG::SHIFT", arrShifts[i]);
                    shiftTypeArray[i] = Integer.parseInt(URLConfig.SHIFT_TYPE_REVERSE_MAP.get(arrShifts[i].trim())) + 1;

                }
            } else {
                shiftTypeArray = new int[0];
            }
            if (shiftType.equals("-1")) {
                shiftType = "";
            } else if (shiftType.equals("0")) {
                showToastAlert(getString(R.string.shift_type_empty_msg));
                return;
            }

            UserStatusModel userProfileVO = ShineSharedPreferences
                    .getUserInfo(mActivity);

            alertDialog = DialogUtils.showCustomProgressDialog(mActivity,
                    getString(R.string.plz_wait));


            Type listType = new TypeToken<DesiredJobDetails>() {
            }.getType();


            VolleyNetworkRequest request = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.MY_DESIRED_JOB_DETAIL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id), listType);

            HashMap desiredMap = new HashMap();

            if (location != null && location.length > 0) {
                JSONArray list = new JSONArray();
                for (int i = 0; i < location.length; i++) {
                    list.put(location[i]);
                }

                desiredMap.put("candidate_location", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("candidate_location", blank);
            }

            if (func_area != null && func_area.length > 0) {
                JSONArray list = new JSONArray();
                for (int i = 0; i < func_area.length; i++) {
                    list.put(func_area[i]);
                }
                desiredMap.put("functional_area", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("functional_area", blank);
            }

            if (indus != null && indus.length > 0) {
                JSONArray list = new JSONArray();
                for (int i = 0; i < indus.length; i++) {
                    list.put(indus[i]);
                }

                desiredMap.put("industry", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("industry", blank);
            }

            if (salary != null && salary.length > 0) {
                JSONArray list = new JSONArray();

                for (int i = 0; i < salary.length; i++) {
                    list.put(salary[i]);
                }

                desiredMap.put("minimum_salary", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("minimum_salary", blank);
            }

            if (salary != null && salary.length > 0) {
                JSONArray list1 = new JSONArray();

                for (int i = 0; i < salary.length; i++) {
                    list1.put(salary[i]);
                }

                desiredMap.put("minimum_salary", list1);
                desiredMap.put("maximum_salary", list1);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("minimum_salary", blank);
                desiredMap.put("maximum_salary", blank);
            }


            if (job_type != null && job_type.length > 0) {
                JSONArray list = new JSONArray();

                for (int i = 0; i < job_type.length; i++) {
                    list.put(job_type[i]);
                }
                desiredMap.put("job_type", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("job_type", blank);
            }

            if (shiftTypeArray != null && shiftTypeArray.length > 0) {
                JSONArray list = new JSONArray();

                for (int i = 0; i < shiftTypeArray.length; i++) {
                    list.put(shiftTypeArray[i]);
                }

                desiredMap.put("shift_type", list);
            } else {
                JSONArray blank = new JSONArray();
                desiredMap.put("shift_type", blank);
            }


            request.setUsePostMethod(desiredMap);

            request.execute(EDIT_DESIRED_JOB_DETAILS);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToastAlert(String msg) {
        DialogUtils.showErrorToast(msg);
    }

    @Override
    public void onResume() {
        super.onResume();
        desireDetails.requestFocus();
        mActivity.setTitle(getString(R.string.title_edit_profile));
        mActivity.setActionBarVisible(false);
        //mActivity.updateLeftDrawer()();
        ManualScreenTracker.manualTracker("Edit-DesiredJobDetails");
    }

    @Override
    public void onStop() {
        mActivity.setActionBarVisible(true);
        super.onStop();
    }

    @Override
    public void OnDataResponse(final Object object, final String tag) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DialogUtils.dialogDismiss(alertDialog);
                if (object != null) {
                    {

                        Toast.makeText(mActivity, "Desired job details updated successfully", Toast.LENGTH_SHORT).show();
                        ShineCommons.trackShineEvents("Edit", "DesiredJobs", mActivity);

                        //mActivity.updateLeftDrawer()();


                        DesiredJobDetails desiredJobDetails = (DesiredJobDetails) object;
                        MyApplication.getInstance().getRequestQueue().getCache().clear();
                        DataCaching.getInstance().cacheData(DataCaching.DESIRED_DETAILS_SECTION,
                                "" + DataCaching.CACHE_TIME_LIMIT, desiredJobDetails, mActivity);
                        mActivity.popone();

                        Bundle bundle = new Bundle();

                        bundle.putInt(MyProfileFrg.SCROLL_TO, MyProfileFrg.SCROLL_TO_DESIRED);

                        mActivity.replaceFragment(new MyProfileFrg().newInstance(bundle));


//                            mActivity.setFragment(new MyProfileFrg().newInstance(new Bundle()));


                    }
                }

            }
        });

    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity,
                            error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            DesiredJobDetailsEditFragment.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
