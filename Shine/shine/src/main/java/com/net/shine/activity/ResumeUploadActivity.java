package com.net.shine.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.net.shine.R;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.ResumeUploadFrg;
import com.net.shine.models.ActionBarState;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.db.ShineSharedPreferences;

import java.util.Arrays;

/**
 * Created by manishpoddar on 09/01/17.
 */

public class ResumeUploadActivity extends BaseActivity {


    private  boolean nonSkipResume=false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setLeftDrawer(DRAWER_NO_SELECTION);
        if(savedInstanceState==null){
            if(getIntent().getExtras()!=null)
                setBaseFrg(ResumeUploadFrg.newInstance(getIntent().getExtras()));
            else
                setBaseFrg(ResumeUploadFrg.newInstance(new Bundle()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitle("Upload Resume");
        //syncDrawerState(ActionBarState.ACTION_BAR_STATE_UP);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(gDriveCompat!=null){
            gDriveCompat.onActivityResult(requestCode, resultCode, data);
        }

    }


    public ResumeUploadActivity.GoogleResolutionOnResult gDriveCompat;


    public interface GoogleResolutionOnResult {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    @Override
    public void onBackPressed() {



        if(Arrays.asList(URLConfig.NON_SKIP_VENDOR_IDS).contains(ShineSharedPreferences.getVendorId(this)))
        {
            final Dialog deleteDialog = DialogUtils.showAlertWithTwoButtons(this);
            TextView t = (TextView) deleteDialog
                    .findViewById(R.id.dialog_msg);
            t.setText("Are you sure you want to stop registration & exit the app?");
            ((TextView)deleteDialog.findViewById(R.id.yes_btn)).setText("YES");
                    deleteDialog.findViewById(R.id.yes_btn).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dialogDismiss(deleteDialog);
                             finish();
                        }
                    });
            deleteDialog.findViewById(R.id.cancel_exit_btn)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DialogUtils.dialogDismiss(deleteDialog);
                        }
                    });
        }
        else
        {
            super.onBackPressed();

        }
    }
}
