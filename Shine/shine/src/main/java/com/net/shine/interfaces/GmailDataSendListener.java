package com.net.shine.interfaces;

/**
 * Created by F2722 on 04-06-2015.
 */
public interface GmailDataSendListener {
    void onSuccessGmail();
    void onFailedGmail();
}
