package com.net.shine.fragments.components;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.activity.BaseActivity;
import com.net.shine.config.URLConfig;
import com.net.shine.fragments.BaseFragment;
import com.net.shine.models.EducationDetailModel;
import com.net.shine.utils.DataCaching;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class EducationDetailComp implements VolleyRequestCompleteListener,
        OnClickListener {

    String deleteEduId = null;
    Dialog alertDialog;
    private Activity mActivity;
    private View view;
    private List<EducationDetailModel> eduList = new ArrayList<>();


    public EducationDetailComp(Activity mActivity, View view) {
        this.mActivity = mActivity;
        this.view = view;
    }

    public void showEducationDetails(List<EducationDetailModel> eduList) {

        this.eduList = eduList;

        TextView add_btn = (TextView) view
                .findViewById(R.id.edu_add_btn);
        add_btn.setOnClickListener(this);


        LinearLayout educationDetailView = (LinearLayout) view
                .findViewById(R.id.education_detail_view);
        educationDetailView.removeAllViews();

        LayoutInflater inflater = mActivity.getLayoutInflater();

        int len = eduList.size();
        for (int i = 0; i < len; i++) {


            View edu_sub_comp_view = inflater.inflate(R.layout.edu_profile_detail_sub_comp, null);
            EducationDetailModel pModel = eduList.get(i);

            Log.d("pModel ","pModel "+pModel.toString());

            String eduTitle = "" + URLConfig.EDUCATIONAL_QUALIFICATION_PARENT_REVERSE_MAP.get(pModel.educationNameIndex);

            TextView eduTitle_tv = (TextView) edu_sub_comp_view.findViewById(R.id.eduTitle);
            eduTitle_tv.setText(eduTitle);
            ImageView img_edit = (ImageView) edu_sub_comp_view.findViewById(R.id.edit_edu);
            img_edit.setVisibility(View.VISIBLE);
            img_edit.setTag(i);
            img_edit.setOnClickListener(this);
            if (eduList.size() > 1) {
                ImageView img_delete = (ImageView) edu_sub_comp_view
                        .findViewById(R.id.delete_edu);
                img_delete.setVisibility(View.VISIBLE);
                img_delete.setTag(i);
                img_delete.setOnClickListener(this);
            }

            TextView edu_stream_tv = (TextView) edu_sub_comp_view.findViewById(R.id.edu_stream_tv);

//
            String str = URLConfig.EDUCATIONAL_QUALIFICATION_CHILD_REVERSE_MAP.get(pModel.educationStreamIndex);

//             if(TextUtils.isEmpty(str))
//              {
//              str=URLConfig.TEN_TWO_EDUCATION_CHOICES_REVERSE.get(""+pModel.educationStreamIndex);
//               }


            if(!TextUtils.isEmpty(pModel.other_specialization))
                str=str+" ("+pModel.other_specialization+")";

            edu_stream_tv.setText(str);
//
            TextView edu_institute_tv = (TextView) edu_sub_comp_view.findViewById(R.id.edu_institute_tv);
            str = pModel.institueName + " (" + URLConfig.COURSE_TYPE_REVRSE_MAP.get(""
                    + pModel.courseTypeIndex) + " )";
            edu_institute_tv.setText(str);


            TextView pass_out_year_tv = (TextView) edu_sub_comp_view.findViewById(R.id.pass_out_year_tv);
            str = URLConfig.PASSOUT_YEAR_REVRSE_MAP.get("" + pModel.passoutYearIndex);
            if (str == null || "".equals(str)) {
                str = mActivity.getString(R.string.not_mentioned);
            }
            pass_out_year_tv.setText(str);

            educationDetailView.addView(edu_sub_comp_view);
            if (i != (len - 1)) {

                View oneLineView = inflater.inflate(
                        R.layout.one_line_view, null, false);

                educationDetailView.addView(oneLineView);
            }

        }


    }

    @Override
    public void onClick(View v) {
        try {

            switch (v.getId()) {

                case R.id.ok_btn:
                    DialogUtils.dialogDismiss(alertDialog);
                    break;
                case R.id.delete_edu:
                    final EducationDetailModel model2 = eduList.get((Integer) v.getTag());
                    final Dialog deleteDialog = DialogUtils.showAlertWithTwoButtons(mActivity);
                    TextView t = (TextView) deleteDialog
                            .findViewById(R.id.dialog_msg);
                    t.setText(mActivity.getResources().getString(R.string.delete_education));
                    deleteDialog.findViewById(R.id.yes_btn).setOnClickListener(
                            new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogUtils.dialogDismiss(deleteDialog);
                                    deleteEdu(model2.educationId);
                                }
                            });
                    deleteDialog.findViewById(R.id.cancel_exit_btn)
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    DialogUtils.dialogDismiss(deleteDialog);
                                }
                            });


                    break;
                case R.id.edit_edu:

                    EducationDetailModel model = eduList.get((Integer) v.getTag());
                    Bundle bundle = new Bundle();
                    Gson gson = new Gson();
                    String json = gson.toJson(model);
                    bundle.putString(URLConfig.KEY_MODEL_NAME, json);

                    BaseFragment frg = new EducationDetailsEditFragment();
                    frg.setArguments(bundle);
                    ((BaseActivity) mActivity).replaceFragmentWithBackStack(frg);

                    break;
                case R.id.edu_add_btn:


                    BaseFragment frg2 = new EducationDetailsEditFragment();
                    ((BaseActivity) mActivity).replaceFragmentWithBackStack(frg2);



                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteEdu(String eduId) {
        try {
            deleteEduId = eduId;

            Type listType = new TypeToken<EducationDetailModel>() {
            }.getType();


            VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, this,
                    URLConfig.MY_EDUCATIONS_URL.replace(URLConfig.CANDIDATE_ID, ShineSharedPreferences.getCandidateId(mActivity)) + eduId + "/", listType);
            downloader.setUseDeleteMethod();
            downloader.execute("deleteEducationDetails");
            alertDialog = DialogUtils.showCustomProgressDialog(mActivity, mActivity.getString(R.string.plz_wait));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void OnDataResponse(Object object, String tag) {

        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    DialogUtils.dialogDismiss(alertDialog);

                    if (deleteEduId != null) {
                        ShineCommons.trackShineEvents("Delete", "Education", mActivity);

                        for (int i = 0; i < eduList.size(); i++) {

                            if (eduList.get(i).educationId.equals(deleteEduId)) {
                                eduList.remove(i);
                                deleteEduId = null;
                                break;
                            }
                        }
                        DataCaching.getInstance().cacheData(DataCaching.EDUCATION_LIST_SECTION,
                                "" + DataCaching.CACHE_TIME_LIMIT, eduList, mActivity);
                        showEducationDetails(eduList);
                    }

                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    DialogUtils.dialogDismiss(alertDialog);
                    alertDialog = DialogUtils.showCustomAlertsNoTitle(mActivity, error);
                    alertDialog.findViewById(R.id.ok_btn).setOnClickListener(
                            EducationDetailComp.this);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
