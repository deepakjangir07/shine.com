package com.net.shine.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.R;
import com.net.shine.adapters.ReceivedReferralAdapter;
import com.net.shine.config.URLConfig;
import com.net.shine.models.ReferralModel;
import com.net.shine.models.SimpleSearchModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ReceiveReferralsFrg extends BaseFragment implements
        VolleyRequestCompleteListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {

    boolean loadMore = false;
    private View LoadingCmp, parentView;
    private ListView gridView;
    private int noofhits = 1, totalJobs;
    private int index;
    private ReceivedReferralAdapter adapter;
    private View errorLayout;
    private TextView count_tv;
    private boolean isDownloading;
    private ArrayList<ReferralModel.RefrelResults> jobList = new ArrayList<>();

    private UserStatusModel userProfileVO;
    private SwipeRefreshLayout swipeContainer;


    public static ReceiveReferralsFrg newInstance() {
        Bundle args = new Bundle();
        ReceiveReferralsFrg fragment = new ReceiveReferralsFrg();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        userProfileVO = ShineSharedPreferences.getUserInfo(mActivity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        parentView = inflater.inflate(R.layout.request_referral, container, false);


        swipeContainer = (SwipeRefreshLayout) parentView.findViewById(R.id.swipeContainer);
        ShineCommons.setSwipeRefeshColor(swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        gridView = (ListView) parentView.findViewById(R.id.list_view);
        count_tv = (TextView) parentView.findViewById(R.id.count_textview);
        errorLayout = parentView.findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        LoadingCmp = parentView.findViewById(R.id.loading_cmp);
        gridView.setOnScrollListener(this);
        jobList = new ArrayList<>();
        adapter = new ReceivedReferralAdapter(mActivity, jobList);
        gridView.setAdapter(adapter);
        LoadingCmp = parentView.findViewById(R.id.loading_cmp);

        downloadRequestRefarrals(1,
                parentView.findViewById(R.id.loading_cmp), false, false);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


                if (jobList.get(position).getJob_id() != null && !jobList.get(position).getJob_id().equals("null")) {
                    goToJDPage(position);

                }


            }
        });

        return parentView;

    }

    private void downloadRequestRefarrals(int pageNumber, View loadingView,
                                          boolean flag, boolean isRefresh) {
        try {

            LoadingCmp = loadingView;
            noofhits = pageNumber;
            loadMore = flag;
            isDownloading = true;
            if (!isRefresh)
                DialogUtils.showLoading(mActivity, getString(R.string.loading),
                        LoadingCmp);

            int noOfRecords = URLConfig.PER_PAGE_JOB_ALERT_MAIL;
            int numberOfResult;
            numberOfResult = noOfRecords;


            if (userProfileVO != null) {

                Type listType = new TypeToken<ReferralModel>() {
                }.getType();

                String URL = URLConfig.GET_REFERRAL_URL.replace(URLConfig.CANDIDATE_ID, userProfileVO.candidate_id);


                String secondPartUrl = "referral_type=received&page=" + noofhits + "&perpage=" + numberOfResult;


                VolleyNetworkRequest downloader = new VolleyNetworkRequest(mActivity, ReceiveReferralsFrg.this,
                        URL + secondPartUrl, listType);


                downloader.execute("ReceivedReferrals");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void showresult() {
        try {

            LoadingCmp.setVisibility(View.GONE);

            if (jobList.size() > 0) {
                adapter.rebuildListObject(jobList);
                adapter.notifyDataSetChanged();
                errorLayout.setVisibility(View.GONE);

            } else {

                gridView.setVisibility(View.GONE);
                count_tv.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);

                if (!ShineSharedPreferences.getLinkedinImported(mActivity) || !ShineSharedPreferences.getGoogleImported(mActivity)) {


                    BaseFragment konnectFrag = KonnectFrag.newInstance(5);
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.no_received_referral),
                            mActivity.getString(R.string.no_request_referral_msg2),
                            mActivity.getString(R.string.sync_now),
                            DialogUtils.ERR_NO_REFERRAL_RECEIVED,konnectFrag);


                } else {

                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.no_received_referral),
                            "",
                            "",
                            DialogUtils.ERR_NO_REFERRAL_RECEIVED,null);

                }



            }
            isDownloading = false;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        try {

            if (totalItemCount != 0) {
                int lastScreen = firstVisibleItem + visibleItemCount;

                if ((lastScreen == totalItemCount && !(loadMore))) {
                    loadMoreData();
                } else if (LoadingCmp.getTag() != null) {
                    if (LoadingCmp.getTag().equals("error")) {
                        parentView.findViewById(R.id.loading_grid)
                                .setVisibility(View.VISIBLE);
                        loadMore = false;
                    }
                }
            } else {
                if (parentView.findViewById(R.id.loading_grid) != null) {
                    parentView.findViewById(R.id.loading_grid)
                            .setVisibility(View.GONE);
                }
            }

            int topRowVerticalPosition =
                    (gridView == null || gridView.getChildCount() == 0) ?
                            0 : gridView.getChildAt(0).getTop();
            swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnDataResponse(Object object, String tag) {


        try {
            ReferralModel map = (ReferralModel) object;


            if (jobList == null || !loadMore)
                jobList = new ArrayList<>();

            LoadingCmp.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);


            if (map != null) {

                jobList.addAll(map.getResults());

                if (jobList != null && jobList.size() > 0) {

                    if (!map.getCount().equals(""))
                        totalJobs = Integer.parseInt(map.getCount());
                    else
                        totalJobs = 0;

                    count_tv.setVisibility(View.VISIBLE);

                    if (totalJobs > 1) {
                        count_tv.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs + "</font></b> connections have sent you referral requests.\n"));


                    } else {
                        count_tv.setText(Html.fromHtml("<b> <font color = '#ff9a09'>" + totalJobs + "</fomt> </b> connection has sent you referral requests. \n"));

                    }
                    gridView.setVisibility(View.VISIBLE);
                    showresult();

                } else {

                    errorLayout.setVisibility(View.VISIBLE);

                    BaseFragment konnectFrag = KonnectFrag.newInstance(5);
                    DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                            mActivity.getString(R.string.no_received_referral),
                            mActivity.getString(R.string.no_received_referral_msg2),
                            mActivity.getString(R.string.sync_now),
                            DialogUtils.ERR_NO_REFERRAL_RECEIVED,konnectFrag);

                }
                loadMore = false;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            if (!isDownloading) {


                if (adapter.getCount() == 0) {
                    gridView.setVisibility(View.GONE);
                    index = gridView.getFirstVisiblePosition();
                    gridView.post(new Runnable() {
                        @Override
                        public void run() {
                            gridView.setSelection(index);

                        }
                    });
                    jobList.clear();
                    adapter.notifyDataSetChanged();
                    downloadRequestRefarrals(1,
                            parentView.findViewById(R.id.loading_cmp), false, false);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void OnDataResponseError(final String error, String tag) {
        try {
            swipeContainer.setRefreshing(false);
            count_tv.setVisibility(View.GONE);
            gridView.setVisibility(View.GONE);
            LoadingCmp.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        DialogUtils.showErrorActionableMessage(mActivity,errorLayout,
                                mActivity.getString(R.string.technical_error),
                                mActivity.getString(R.string.technical_error2),
                                "",
                                DialogUtils.ERR_TECHNICAL,null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void loadMoreData() {
        try {

            int noofhits = (jobList.size() / URLConfig.PER_PAGE_JOB_ALERT_MAIL) + 1;
            int totalPage = totalJobs / URLConfig.PER_PAGE_JOB_ALERT_MAIL;

            int modtotalnoPages = totalPage % URLConfig.PER_PAGE_JOB_ALERT_MAIL;


            if (modtotalnoPages != 0) {
                totalPage = totalPage + 1;
            }

            if (noofhits <= totalPage && totalJobs > jobList.size()) {
                View view = parentView.findViewById(R.id.loading_grid);

                downloadRequestRefarrals(noofhits, view, true, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToJDPage(int position) {

        if(jobList.get(position).job_expired)
        {
            Toast.makeText(mActivity,"This job has been expired",Toast.LENGTH_SHORT).show();
        }
        else {

            ArrayList<SimpleSearchModel.Result> jobLists = new ArrayList<>();
            SimpleSearchModel.Result searchResultVO = new SimpleSearchModel.Result();
            searchResultVO.jobId = (jobList.get(position).getJob_id());
            jobLists.add(searchResultVO);
            Bundle bundle = new Bundle();
            bundle.putInt(URLConfig.INDEX, position);
            bundle.putString(URLConfig.JOB_TYPE, "search_job");
            bundle.putSerializable(JobDetailParentFrg.JOB_LIST, jobLists);
            BaseFragment fragment = new JobDetailParentFrg();
            fragment.setArguments(bundle);
            mActivity.singleInstanceReplaceFragment(fragment);
        }
    }

    @Override
    public void onRefresh() {
        downloadRequestRefarrals(1,
                parentView.findViewById(R.id.loading_cmp), false, true);
        errorLayout.setVisibility(View.GONE);
    }
}
