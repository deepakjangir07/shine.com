package com.net.shine.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.net.shine.R;

/**
 * Created by manishpoddar on 09/12/16.
 */

public class MyWidgetIntentReceiver extends BroadcastReceiver {
    private static int clickCount = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("com.net.shine.intent.action.CHANGE_PICTURE")){
            String aa=  intent.getStringExtra("title");
            System.out.println("--Title--"+aa);
//            updateWidgetPictureAndButtonListener(context);
        }
    }

//    private void updateWidgetPictureAndButtonListener(Context context) {
//        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_xml);
//        remoteViews.setImageViewResource(R.id.widget_image, getImageToSet());
//
//        //REMEMBER TO ALWAYS REFRESH YOUR BUTTON CLICK LISTENERS!!!
//        remoteViews.setOnClickPendingIntent(R.id.widget_button, WidgetProvider.buildButtonPendingIntent(context));
//
//        WidgetProvider.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
//    }

    private int getImageToSet() {
        clickCount++;
        return clickCount % 2 == 0 ? R.drawable.ic_launcher : R.drawable.ic_launcher;
    }
}

