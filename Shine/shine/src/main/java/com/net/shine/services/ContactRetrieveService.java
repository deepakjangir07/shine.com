package com.net.shine.services;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.net.shine.MyApplication;
import com.net.shine.config.URLConfig;
import com.net.shine.models.DumpContactsModel;
import com.net.shine.models.UserStatusModel;
import com.net.shine.utils.DialogUtils;
import com.net.shine.utils.ShineCommons;
import com.net.shine.utils.db.ShineSharedPreferences;
import com.net.shine.volley.VolleyNetworkRequest;
import com.net.shine.volley.VolleyRequestCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import contactretrieve.Contact;
import contactretrieve.ContactAPI;
import contactretrieve.Email;
import contactretrieve.Organization;
import contactretrieve.Phone;

public class ContactRetrieveService extends IntentService implements VolleyRequestCompleteListener {


	private Context mContext;


    public ContactRetrieveService() {
        super("ContactRetrieveService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = getApplicationContext();
        return super.onStartCommand(intent,flags,startId);
    }



	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

    @Override
    public void onDestroy() {

        Log.d("m_cooker", "Contact Service: OnDestroy Called");
        super.onDestroy();
    }


    boolean isUserAction = false;
    boolean isSyncing = false;

    @Override
    protected void onHandleIntent(Intent intent) {

        if(intent==null)
            return;

        isUserAction = intent.getBooleanExtra("isUserAction", false);

        Log.d("m_cooker", "Contact Service: OnHandleIntent Called");

        if(isUserAction)
        {
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("CONTACT_SYNC_STARTED"));
        }
        if(isSyncing)
        {
            return;
        }
        isSyncing = true;

        ShineSharedPreferences.savePhoneBookReadProgress(this, true);
        ContactAPI cpp;
        cpp = new ContactAPI();
        cpp.setCr(getContentResolver());
        if(ContextCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
        {
            //No need to extra ERROR_MSG as this condition will only occur in case of Background Sync
            if(isUserAction)
            {
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("CONTACT_SYNC_FAILED"));
                MyApplication.isPhonebookSyncing = false;
            }
            Log.d("m_cooker", "No Permission to read Contacts!! :(");
            isSyncing = false;
            return;
        }

        Log.d("m_cooker", "Contact Service: Getting Contacts");
        ArrayList<Contact> cl = cpp.newContactList();
        Log.d("m_cooker", "Contact Service: Fetched Contacts:" + cl.size());
        JSONObject result = toJSon(cl);


        if (result!=null) {

            try {

                if ((result.getJSONArray("contacts").length() != 0)) {

                    postContacts(result);
                } else {
                    if(isUserAction)
                    {
                        Intent i  = new Intent("CONTACT_SYNC_FAILED");
                        i.putExtra("ERROR_MSG", "No contacts to Sync");
                        LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
                        MyApplication.isPhonebookSyncing = false;
                    }
                    isSyncing = false;

                }
            } catch (Exception e) {
                isSyncing = false;
                e.printStackTrace();
            }
        }
            else
            {
                isSyncing = false;
                if(isUserAction) {
                    Intent i = new Intent("CONTACT_SYNC_FAILED");
                    i.putExtra("ERROR_MSG", "No contacts to Sync");
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
                    MyApplication.isPhonebookSyncing = false;
                }
                ShineSharedPreferences.savePhoneBookReadProgress(mContext,
                        false);

            }
    }





		public JSONObject toJSon(ArrayList<Contact> contacts) {

			try {

				if(contacts.size()==0)
					return null;

				JSONObject jsonMain = new JSONObject();
				JSONArray jsonArr = new JSONArray();
				for (int i = 0; i < contacts.size(); i++) {
					JSONObject jsonObj = new JSONObject();
					Contact c = contacts.get(i);
					// DisplayName
					jsonObj.put("n", c.getDisplayName());
                    jsonObj.put("lid", c.getLinkedinID());
                    jsonObj.put("lt", c.getLinkedinTitle());

                    JSONArray jsonArrEmail = new JSONArray();
					ArrayList<Email> Email = c.getEmail();
					for (int j = 0; j < Email.size(); j++) {
						// //system.out.println("---Email Num " + j + ": "+
						// Email.get(j).getAddress());

						JSONObject pnObj = new JSONObject();
						pnObj.put("e", Email.get(j).getAddress()); // Email:

						jsonArrEmail.put(pnObj);
					}
					jsonObj.put("es", jsonArrEmail);

					Organization org = c.getOrganization();

					jsonObj.put("ot", org.getTitle()); // Organization Title
					jsonObj.put("on", org.getOrganization()); // Organization
																// Name

					ArrayList<Phone> ph = c.getPhone();
					JSONArray jsonArrPhone = new JSONArray();

					if (ph != null) {
						for (int j = 0; j < ph.size(); j++) {

							JSONObject pnObj = new JSONObject();
							pnObj.put("pn", ph.get(j).getNumber()); // Phone
																	// Number
							jsonArrPhone.put(pnObj);
						}
						jsonObj.put("pns", jsonArrPhone); // Contact Ph Nums
					}

					jsonArr.put(jsonObj);
				}
				jsonMain.put("contacts", jsonArr);
				return jsonMain;
			}

			catch (JSONException ex) {
				ex.printStackTrace();
			}

			return null;

		}

    private void postContacts(JSONObject result) {

        String ShineUserID = "";
        String AppVersion;
        String androidOS;
        UserStatusModel userProfileVO;

        userProfileVO = ShineSharedPreferences
                .getUserInfo(ContactRetrieveService.this);

        if (userProfileVO != null) {
            ShineUserID = userProfileVO.candidate_id;

        }
        try {
            AppVersion = getApplicationContext().getPackageManager().getPackageInfo(
                    getApplicationContext().getPackageName(), 0).versionName;
        } catch (Exception e) {
            AppVersion = "";
        }
        try {
            androidOS = Build.VERSION.RELEASE;
        } catch (Exception e) {
            androidOS = "";
        }
        Log.e("DEBUG :","PhoneBook DATA "+result);

        Type type = new TypeToken<DumpContactsModel>(){}.getType();

        VolleyNetworkRequest request = new VolleyNetworkRequest(getApplicationContext(), this,URLConfig.KONNECT_PHONEBOOK_URL,type);
        HashMap<String, Object> paramsMap = new HashMap<>();
//        paramsMap.put("uid",uniqueID);
        paramsMap.put("shine_id",ShineUserID);
        paramsMap.put("data",result);
        paramsMap.put("source", "phonebook (app version: " + AppVersion + "; android: "
                    + androidOS + ";)");

        if(isUserAction)
            paramsMap.put("force",true);


        request.setUsePostMethod(paramsMap);
        request.execute("DumpContactData");

        Log.d("m_cooker", "Contact Service: Dumping Contacts");


    }
    @Override
    public void OnDataResponse(Object object, String tag) {

        try {

            DumpContactsModel model = (DumpContactsModel) object;

            Log.d("m_cooker", "Contact Service: Dumping Contacts Successful");

       final UserStatusModel userProfileVO = ShineSharedPreferences
                .getUserInfo(ContactRetrieveService.this);

                        if (userProfileVO != null) {
                            ShineSharedPreferences.savePhoneBookReadProgress(
                                    getApplicationContext(), false);
                            ShineSharedPreferences.savePhoneBookDelivered(
                                    getApplicationContext(), true);
                            ShineSharedPreferences.setPhonebookSyncStatus(getApplicationContext(), true);



                            ShineCommons.trackShineEvents("Import Contact Success","Phonebook", getApplicationContext());



                            // TODO: Ujjawal - Fix Duplicate Keys
                            ShineSharedPreferences.updateUserPhSyncTime(
                                    getApplicationContext(), model.next_phonebook_sync_on);
                           // fetch new sync time and set alarm
                            ShineSharedPreferences.saveSyncPhTime(getApplicationContext(),
                                    userProfileVO.candidate_id, model.next_phonebook_sync_on);
                            //setPhBookAlarm();

                            if(isUserAction)
                            {
                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent("CONTACT_SYNC_SUCCESS"));
                                Toast.makeText(mContext, "Your Phonebook friends are now ready to refer you for " +
                                        "jobs in their companies, start applying now.", Toast.LENGTH_LONG).show();
                                DialogUtils.showNotification(mContext,"Your Phonebook friends are now ready to refer you for jobs in" +
                                        " their companies, start applying now");
                                ShineSharedPreferences.savePhonebookImported(mContext, true);
                                ShineSharedPreferences.updateSynctimePhonebook(mContext, System.currentTimeMillis());
                                MyApplication.isPhonebookSyncing = false;
                            }
                            isSyncing = false;

                        }


                } catch (Exception e) {
            isSyncing = false;
                    ShineSharedPreferences.savePhoneBookReadProgress(getApplicationContext(),
                            false);
                    e.printStackTrace();
                }

    }

    @Override
    public void OnDataResponseError(String error, String tag) {

        Log.d("m_cooker", "Contact Service: Error Dumping Contacts " + error);
        if(isUserAction)
        {
            Intent i  = new Intent("CONTACT_SYNC_FAILED");
            i.putExtra("ERROR_MSG", error);
            MyApplication.isPhonebookSyncing = false;
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
        }
        isSyncing = false;


    }



}
