package contactretrieve;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class ContactAPI  {

	private ContentResolver cr;

	public void setCr(ContentResolver cr) {
		this.cr = cr;
	}

	public ArrayList<Contact> newContactList() {
		ArrayList<Contact> contacts = new ArrayList<>();
		String id;

		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);
		if (cur!=null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                Contact c = new Contact();
                id = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts._ID));
                c.setId(id);
                c.setDisplayName(cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    c.setPhone(this.getPhoneNumbers(id));
                }
                c.setEmail(this.getEmailAddresses(id));
                // c.setNotes(this.getContactNotes(id));
                // c.setAddresses(this.getContactAddresses(id));
                // c.setImAddresses(this.getIM(id));

                c.setOrganization(this.getContactOrg(id));

                try {
                    String[] linkedIn = this.getLinkedInID(id);
                    c.setLinkedinID(linkedIn[0]);
                    c.setLinkedinTitle(linkedIn[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                contacts.add(c);
            }
            cur.close();
        }
		return (contacts);
	}


	private String[] getLinkedInID(String id)
	{

		String where = ContactsContract.Data.CONTACT_ID + " = ?" + " AND "
				+ ContactsContract.CommonDataKinds.Website.TYPE + " = ?";
		String[] whereParameters = new String[]{id, "LinkedIn Profile"};
		Cursor pCur = cr.query(ContactsContract.Data.CONTENT_URI, null,
				where, whereParameters, null);
		String arr[] = new String[2];
		arr[0] = "";
		arr[1] = "";
		if (pCur!=null)
		{
            if(pCur.moveToNext()) {
                arr[0] = pCur
                        .getString(pCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Website.URL));
                arr[1] = pCur
                        .getString(pCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Website.LABEL));
            }
            pCur.close();
		}

		return arr;
	}


	private ArrayList<Phone> getPhoneNumbers(String id) {
		ArrayList<Phone> phones = new ArrayList<>();

		Cursor pCur = this.cr.query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
				new String[] { id }, null);
        if(pCur!=null) {
            while (pCur.moveToNext()) {
                phones.add(new Phone(
                        pCur.getString(pCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)),
                        pCur.getString(pCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))));

            }
            pCur.close();
        }
		return (phones);
	}

	private ArrayList<Email> getEmailAddresses(String id) {
		ArrayList<Email> emails = new ArrayList<>();

		Cursor emailCur = this.cr.query(
				ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
				ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
				new String[] { id }, null);
        if(emailCur!=null) {
            while (emailCur.moveToNext()) {
                // This would allow you get several email addresses
                Email e = new Email(
                        emailCur.getString(emailCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)),
                        emailCur.getString(emailCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE)));
                emails.add(e);
            }
            emailCur.close();
        }

		return (emails);
	}

	private Organization getContactOrg(String id) {
		Organization org = new Organization();
		String where = ContactsContract.Data.CONTACT_ID + " = ? AND "
				+ ContactsContract.Data.MIMETYPE + " = ?";
		String[] whereParameters = new String[] { id,
				ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE };

		Cursor orgCur = this.cr.query(ContactsContract.Data.CONTENT_URI, null,
				where, whereParameters, null);

        if(orgCur!=null) {
            if (orgCur.moveToFirst()) {
                String orgName = orgCur
                        .getString(orgCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
                String title = orgCur
                        .getString(orgCur
                                .getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                if (orgName != null && orgName.length() > 0) {
                    org.setOrganization(orgName);

                }
                if (title != null && title.length() > 0) {
                    org.setTitle(title);
                }
            }
            orgCur.close();
        }
		return (org);
	}

}