package contactretrieve;

import java.util.ArrayList;


public class Contact {

    public String getLinkedinID() {
        return linkedinID;
    }

    public void setLinkedinID(String linkedinID) {
        this.linkedinID = linkedinID;
    }

    private String linkedinID;

    public String getLinkedinTitle() {
        return linkedinTitle;
    }

    public void setLinkedinTitle(String linkedinTitle) {
        this.linkedinTitle = linkedinTitle;
    }

    private String linkedinTitle;



private String id;
 private String displayName;
 private ArrayList<Phone> phone;
 private ArrayList<Email> email;
 private ArrayList<String> notes;
 private ArrayList<Address> addresses = new ArrayList<Address>();
 private ArrayList<IM> imAddresses;
 private Organization organization;
public String getId() {
    return id;
}
public void setId(String id) {
    this.id = id;
}
public String getDisplayName() {
    return displayName;
}
public void setDisplayName(String displayName) {
    this.displayName = displayName;
}
public ArrayList<Phone> getPhone() {
    return phone;
}
public void setPhone(ArrayList<Phone> phone) {
    this.phone = phone;
}
public void addPhone(Phone phone) {
     this.phone.add(phone);
 }
public ArrayList<Email> getEmail() {
    return email;
}
public void setEmail(ArrayList<Email> email) {
    this.email = email;
}
public void addEmail(Email email) {
     this.email.add(email);
 }
public ArrayList<String> getNotes() {
    return notes;
}
public void setNotes(ArrayList<String> notes) {
    this.notes = notes;
}
public void AddNotes(String notes){
    this.notes.add(notes);
}
public ArrayList<Address> getAddresses() {
    return addresses;
}
public void setAddresses(ArrayList<Address> addresses) {
    this.addresses = addresses;
}
public void addAddress(Address address) {
     this.addresses.add(address);
 }
public ArrayList<IM> getImAddresses() {
    return imAddresses;
}
public void setImAddresses(ArrayList<IM> imAddresses) {
    this.imAddresses = imAddresses;
}
public void addImAddresses(IM imAddr) {
     this.imAddresses.add(imAddr);
 }
public Organization getOrganization() {
    return organization;
}
public void setOrganization(Organization organization) {
    this.organization = organization;
}
}